---
aliases:
- ../../fulllog_applications-18.04.1
hidden: true
title: KDE Applications 18.04.1 Full Log Page
type: fulllog
version: 18.04.1
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>TransactionSequence: fix rollback not finishing properly. <a href='http://commits.kde.org/akonadi/f14a6e997629af96f83153eae33a0abd18366403'>Commit.</a> </li>
<li>Akonadi tests: make checkTestIsIsolated work in release mode too. <a href='http://commits.kde.org/akonadi/f7104b0ccb1c8e72d59e31aae5635e35e811e6b7'>Commit.</a> </li>
<li>Fix session not reconnecting on Qt >= 5.10. <a href='http://commits.kde.org/akonadi/d1be68ccee21b916dc80546e25ab1c1cff3e975f'>Commit.</a> </li>
<li>Warning--. <a href='http://commits.kde.org/akonadi/72ec4f2fefccfa9ef13028651e910399879432a3'>Commit.</a> </li>
<li>Fix handling of cut collections. <a href='http://commits.kde.org/akonadi/c10f8522c9359d21e6d5f8a0ca7f115732c89ec4'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Hide]</a></h3>
<ul id='ulakonadi-calendar' style='display: block'>
<li>Binary serializer code now supports FreeBusy too, so we can remove "default" statement. <a href='http://commits.kde.org/akonadi-calendar/262aaf3e030846c0372c0edc0364d47bc1d5e459'>Commit.</a> </li>
<li>Always show an incidence if we are the organizer. <a href='http://commits.kde.org/akonadi-calendar/86625a0ebcd5d0bf2e13edb05cea7acfd5121a82'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352214'>#352214</a></li>
</ul>
<h3><a name='akonadi-import-wizard' href='https://cgit.kde.org/akonadi-import-wizard.git'>akonadi-import-wizard</a> <a href='#akonadi-import-wizard' onclick='toggle("ulakonadi-import-wizard", this)'>[Hide]</a></h3>
<ul id='ulakonadi-import-wizard' style='display: block'>
<li>Fix version. <a href='http://commits.kde.org/akonadi-import-wizard/d466b550b57e01c65b5656b0b018503b7e645934'>Commit.</a> </li>
</ul>
<h3><a name='akonadiconsole' href='https://cgit.kde.org/akonadiconsole.git'>akonadiconsole</a> <a href='#akonadiconsole' onclick='toggle("ulakonadiconsole", this)'>[Hide]</a></h3>
<ul id='ulakonadiconsole' style='display: block'>
<li>Akonadiconsole: support incidences serialized in the binary format. <a href='http://commits.kde.org/akonadiconsole/83722b685e6e519d1fd3db6c15655dd716291dc8'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Fix going to prev/next article with keyboard when using load full website. <a href='http://commits.kde.org/akregator/420dc9ab048bab6afa91f4dc5effb7033530b98c'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Libzipplugin: Add missing <memory> header. <a href='http://commits.kde.org/ark/1cec28b4f9ba43cf6b176db9cdf9ac8b3bbcc1d1'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix maxima LaTeX output for matrics. <a href='http://commits.kde.org/cantor/42687f6542db78b75584fe28d130f7720119863f'>Commit.</a> </li>
<li>Small fix for Sage backend. <a href='http://commits.kde.org/cantor/ab092625ec4c74553f5209addb5e095604729c85'>Commit.</a> </li>
<li>Improve octave login. <a href='http://commits.kde.org/cantor/0b7bf4991a1abfaea181cbe9629402df9795b72b'>Commit.</a> </li>
<li>Make the path of the currently opened project file available in the sage session. <a href='http://commits.kde.org/cantor/211c00896d3f64e3761832d3ab2e72cf28cbfc0d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358975'>#358975</a></li>
<li>Avoid octave backend crushing, when we try to kill ended process. <a href='http://commits.kde.org/cantor/1ffa0816e1daacfadd7723bba75cead53492b7fb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372074'>#372074</a></li>
<li>Add unit tests for lua backends. <a href='http://commits.kde.org/cantor/4b16e2adbd59d68e8c3b569f423db1d990f0d34b'>Commit.</a> </li>
<li>Don't crash with Lua when fetching completion without login being done. <a href='http://commits.kde.org/cantor/adf60aacb716476cf1faeed900b913f94300da0c'>Commit.</a> </li>
<li>Improved handling of multi-line commands in Lua. See D12074 for further details and the remaining open issues. <a href='http://commits.kde.org/cantor/df5360cf4ad988a4a508d6607ebcf272f950d8b2'>Commit.</a> </li>
<li>Improve Julia cmake build system. <a href='http://commits.kde.org/cantor/87bccf4abec7ae3eb7b8a264e112fd44333ffbb0'>Commit.</a> </li>
<li>Show error message in case the user tries to open a non valid Cantor project file. <a href='http://commits.kde.org/cantor/5f2a5eb6e3689f6471681068aed6a39bdf22bd3e'>Commit.</a> </li>
<li>Added some debug output to measure the time it took to load a saved worksheet. <a href='http://commits.kde.org/cantor/373b621e6afbbe0a9dd864b2b1746b9d5774c3a0'>Commit.</a> </li>
<li>Fix segfault in key settings menu when worksheet not open. <a href='http://commits.kde.org/cantor/869f237fd34f762480ba71a0d56c3892c3423554'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Don't filter out duplicated entries from places panel. <a href='http://commits.kde.org/dolphin/0c05b992c0dbfb2b0651344413310d0b860c2fa7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393528'>#393528</a></li>
<li>Update window title after closing split view. <a href='http://commits.kde.org/dolphin/78c8b36dd84a94916835d702ea4a9e8eda65e8ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385111'>#385111</a></li>
<li>Add failing test case for bug #385111. <a href='http://commits.kde.org/dolphin/74c2d4ea2d256183dc1f869a78ee5b1b79888330'>Commit.</a> See bug <a href='https://bugs.kde.org/385111'>#385111</a></li>
<li>Fix inconsistent preview spacing and icon wiggly-ness when toggling previews in Icon mode. <a href='http://commits.kde.org/dolphin/e15d9f266b662cdbed1a9f07b9686fd79a31d929'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393306'>#393306</a></li>
</ul>
<h3><a name='granatier' href='https://cgit.kde.org/granatier.git'>granatier</a> <a href='#granatier' onclick='toggle("ulgranatier", this)'>[Hide]</a></h3>
<ul id='ulgranatier' style='display: block'>
<li>Backport fix to make random arena mode work again. <a href='http://commits.kde.org/granatier/49e01ce2a94b8bacc944c1bb9b188d89bf7f6d42'>Commit.</a> </li>
</ul>
<h3><a name='grantleetheme' href='https://cgit.kde.org/grantleetheme.git'>grantleetheme</a> <a href='#grantleetheme' onclick='toggle("ulgrantleetheme", this)'>[Hide]</a></h3>
<ul id='ulgrantleetheme' style='display: block'>
<li>Support deleting ThemeManager after actiongroup and menu, to fix crash in kmail. <a href='http://commits.kde.org/grantleetheme/7992eada7a19e54fab997c6ff6b91e858cf2b00e'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix custom folder icons using an absolute path entry. <a href='http://commits.kde.org/gwenview/dd0d608a27711cdd1a64a5cb43f01513c212b0cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/315983'>#315983</a></li>
<li>Fix "Save As" in failed-save message box. <a href='http://commits.kde.org/gwenview/1e54480edc524525c0b821c5dbcf816eeea2a021'>Commit.</a> </li>
<li>Remove duplicate overwrite confirmation dialog. <a href='http://commits.kde.org/gwenview/e9cb5260937b2e7462a7a914af5bce19e2e3b4cc'>Commit.</a> </li>
<li>Fix duplicate and markup displaying failed-save message box. <a href='http://commits.kde.org/gwenview/5e7fe7535f9d5fb2de44fe408fe8cf5b0f7937b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393170'>#393170</a></li>
<li>Fix View Mode session restore. <a href='http://commits.kde.org/gwenview/cd18339fe959d8f9df3c4962b50df4f8c4755e4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393176'>#393176</a></li>
<li>Update view offset in higher zoom levels after image size changed. <a href='http://commits.kde.org/gwenview/dafe25a8623aba9822994965601351603ebcb0a9'>Commit.</a> </li>
<li>Center image after edit operation changed size. <a href='http://commits.kde.org/gwenview/b0111d431b15c125327f943818ae9df9802a5488'>Commit.</a> </li>
<li>Fix reloading of SVG images. <a href='http://commits.kde.org/gwenview/f11f5a43b4a65a1a9f2893dc577b94b7af94a37b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359736'>#359736</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Systray: Ensure quit from systray also saves config changes. <a href='http://commits.kde.org/juk/30cd501f866dbd06dacc20fe6ea7eff3dab46154'>Commit.</a> </li>
<li>Simplify shutdown handling. <a href='http://commits.kde.org/juk/a951caf3e9dd54f56fa1a32d4dc8bbbdfc269252'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388663'>#388663</a></li>
<li>Don't try to start in docked mode without the systray. <a href='http://commits.kde.org/juk/1eed47304c46ccba081a8306a3c95151ff8ffd66'>Commit.</a> </li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix build with KIO < 5.33. <a href='http://commits.kde.org/k3b/783bf568780cc4b2468afa091478071cc782db64'>Commit.</a> </li>
<li>Fix pixmap scaling on HiDPI screens. <a href='http://commits.kde.org/k3b/683aee875d173c3183252e2b002648239bc3193a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390081'>#390081</a></li>
</ul>
<h3><a name='kaddressbook' href='https://cgit.kde.org/kaddressbook.git'>kaddressbook</a> <a href='#kaddressbook' onclick='toggle("ulkaddressbook", this)'>[Hide]</a></h3>
<ul id='ulkaddressbook' style='display: block'>
<li>Fix manager leak. <a href='http://commits.kde.org/kaddressbook/384e1814043de1497ab74981fe3e6edac0ac7f58'>Commit.</a> </li>
</ul>
<h3><a name='katomic' href='https://cgit.kde.org/katomic.git'>katomic</a> <a href='#katomic' onclick='toggle("ulkatomic", this)'>[Hide]</a></h3>
<ul id='ulkatomic' style='display: block'>
<li>Fix crash on going to previous atom. <a href='http://commits.kde.org/katomic/7561b4304d1594f592bed6239921d7f347a746ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366396'>#366396</a></li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Hide]</a></h3>
<ul id='ulkdebugsettings' style='display: block'>
<li>Add category name as tooltip. <a href='http://commits.kde.org/kdebugsettings/85a9fb9058b50bfcc1461e7ca06ba4913881b5ff'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Update monitor safezone overlay. <a href='http://commits.kde.org/kdenlive/d7c51af4d84edbdbd44b25f2012af895ec5346a5'>Commit.</a> </li>
<li>Update appdata version. <a href='http://commits.kde.org/kdenlive/3850e34f9d311f559d690113af96a0b7b17f82c8'>Commit.</a> </li>
<li>Improve titler default background color. <a href='http://commits.kde.org/kdenlive/dd28daf9aa453d73f9cf621170640de018c3b802'>Commit.</a> </li>
<li>Fix build with Qt 5.11_beta3 (dropping qt5_use_modules). <a href='http://commits.kde.org/kdenlive/c595018a420127fb1cd2af6b676554f9960c7f79'>Commit.</a> </li>
<li>Only check color theme on first run, not each time the Wizard is called. <a href='http://commits.kde.org/kdenlive/ddc1dba82a833d1d5d58e8a9387733b02cd04b88'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388274'>#388274</a></li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix PIM Events Plugin metadata for ConfigUi. <a href='http://commits.kde.org/kdepim-addons/a330ac185b46d487de134efc4792246444c2c1e2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393678'>#393678</a></li>
<li>Fix crash on close. <a href='http://commits.kde.org/kdepim-addons/e0542356666fdc73cb08f2f82fe1a9865d4c3aa2'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-addons/4015e47d8d65cb5764babb5e21884657c969909b'>Commit.</a> </li>
<li>Use attachment code directly. <a href='http://commits.kde.org/kdepim-addons/d3985ceaa2cc1dd0787e0471e1b110200612c1b9'>Commit.</a> </li>
<li>Remove it. <a href='http://commits.kde.org/kdepim-addons/a29836cdacd40bfc63557885d84e5b047c8405e3'>Commit.</a> </li>
<li>Fix GrantleeTheme::ThemeManager being leaked. <a href='http://commits.kde.org/kdepim-addons/f3da346dcecd7d8ff83582a24b0bcc4ce01d4302'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383070'>#383070</a></li>
<li>Disable it for release version. <a href='http://commits.kde.org/kdepim-addons/dd7bb3a887b2b3d1263f37827dd4caee0d4d0d6e'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>IMAP resource: fix data loss on connection lost, depending on the current task. <a href='http://commits.kde.org/kdepim-runtime/743e0cb230d6e8c6c00437ccce203a7942f89d09'>Commit.</a> </li>
<li>IMAP: Collection removal should be replayed on error. <a href='http://commits.kde.org/kdepim-runtime/990052941b36aaa741616d52d1df48f2499833f8'>Commit.</a> </li>
<li>IMAP Resource: handle disconnection before initial "OK" from server. <a href='http://commits.kde.org/kdepim-runtime/e1ed624d87d1975ff48e0cbf1a936233bd326a12'>Commit.</a> </li>
<li>Fix IMAP resource stuck if connection lost during Login. <a href='http://commits.kde.org/kdepim-runtime/a0a2f6a824cfba9729af9adfc625a054a023f409'>Commit.</a> </li>
<li>Fix a bunch of typos in comments. <a href='http://commits.kde.org/kdepim-runtime/64240a38af55bb4de08e94b740e94a107ffcd285'>Commit.</a> </li>
<li>Fix version. <a href='http://commits.kde.org/kdepim-runtime/0b033fef54be77f73f137248d20c9e9d05d6272e'>Commit.</a> </li>
</ul>
<h3><a name='kfind' href='https://cgit.kde.org/kfind.git'>kfind</a> <a href='#kfind' onclick='toggle("ulkfind", this)'>[Hide]</a></h3>
<ul id='ulkfind' style='display: block'>
<li>Use the correct log category in the categories file. <a href='http://commits.kde.org/kfind/25bcc3f40372c2dbb89a34c7090c5df5e9de54cb'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Resolve KGpg crashing when setting trust level to ultimate. <a href='http://commits.kde.org/kgpg/68fc9165947c054423ac600b11462294d56fa06e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373909'>#373909</a>. Fixes bug <a href='https://bugs.kde.org/391499'>#391499</a></li>
</ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Hide]</a></h3>
<ul id='ulkimap' style='display: block'>
<li>Session: emit connectionLost when getting disconnected before the initial OK. <a href='http://commits.kde.org/kimap/1b79b4be07588a959af7caf3866005ba5e5128d3'>Commit.</a> </li>
<li>IMAP fake server: provide more output in case the scenario is incomplete. <a href='http://commits.kde.org/kimap/b185289830fdce70e86d5a1dec955fdaf12b446d'>Commit.</a> </li>
<li>KIMAP fake server: add support for disconnect before response. <a href='http://commits.kde.org/kimap/c5424b187c4a801e06a914a19d0c3c03ef785797'>Commit.</a> </li>
<li>Fix ctest with CTEST_PARALLEL_LEVEL being set. <a href='http://commits.kde.org/kimap/f5456fad70ed2faf60c8480fe53a6d40ff1be910'>Commit.</a> </li>
</ul>
<h3><a name='kldap' href='https://cgit.kde.org/kldap.git'>kldap</a> <a href='#kldap' onclick='toggle("ulkldap", this)'>[Hide]</a></h3>
<ul id='ulkldap' style='display: block'>
<li>LdapUrl: remove ctor code which made the QUrl invalid. <a href='http://commits.kde.org/kldap/76f496735b20d833d391cf6a4f45d10e1923b1ed'>Commit.</a> </li>
<li>LdapUrl: fix LDAP queries broken with non-ascii search strings. <a href='http://commits.kde.org/kldap/aef21d5c5f91603f46459318d6f1f8557e740b64'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Hide]</a></h3>
<ul id='ulkleopatra' style='display: block'>
<li>Fix runtime capability of apply-profile. <a href='http://commits.kde.org/kleopatra/4716fc0dd345440f494deec1e8bea973ed1911c5'>Commit.</a> </li>
<li>Change License of versioninfo.rc.in to GPL. <a href='http://commits.kde.org/kleopatra/b4a33b333e1c1dfa84a0b6cf3b193c06124c6496'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Set focus to viewer when we use esc. <a href='http://commits.kde.org/kmail/9b6c92eee014686fd735204f6de7810c06763543'>Commit.</a> </li>
<li>Distinguish between settings and explicit overrides for external content. <a href='http://commits.kde.org/kmail/88558f6273650a03d2828027e04116564ca18f20'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Fix build with Qt 5.11_beta3 (dropping qt5_use_modules). <a href='http://commits.kde.org/konsole/11c3c06a45c3b981043609c43f99a5fd62e361f6'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Hide]</a></h3>
<ul id='ulkopete' style='display: block'>
<li>Oscar: include buffer.h. <a href='http://commits.kde.org/kopete/b1f4fa1401cba2e359e5a4b3ea2bafd119fca62b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393372'>#393372</a></li>
</ul>
<h3><a name='ksmtp' href='https://cgit.kde.org/ksmtp.git'>ksmtp</a> <a href='#ksmtp' onclick='toggle("ulksmtp", this)'>[Hide]</a></h3>
<ul id='ulksmtp' style='display: block'>
<li>Partial revert, restore API in the stable branch. <a href='http://commits.kde.org/ksmtp/98956d3bcd497db23046881304aada099b5eb3e2'>Commit.</a> </li>
<li>KSMTP: Fix sending emails where a line starts with a dot. <a href='http://commits.kde.org/ksmtp/988e0649860952ed05501502a62af3ccc75afc8f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392049'>#392049</a></li>
<li>KSMTP: remove dependency on KMime, by removing setMessage(). <a href='http://commits.kde.org/ksmtp/93a7777ae7dc618c26094f73a2c9e034a2a6f678'>Commit.</a> </li>
<li>Increase SMTP session timeout to 1 minute. <a href='http://commits.kde.org/ksmtp/f53cf6e95741266fa948efc8056db7ba37c4df7c'>Commit.</a> </li>
</ul>
<h3><a name='ksystemlog' href='https://cgit.kde.org/ksystemlog.git'>ksystemlog</a> <a href='#ksystemlog' onclick='toggle("ulksystemlog", this)'>[Hide]</a></h3>
<ul id='ulksystemlog' style='display: block'>
<li>Fix build with Qt 5.11_beta3 (dropping qt5_use_modules). <a href='http://commits.kde.org/ksystemlog/7802ff363c5f47241c40d19e34d377d122081b5e'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Remove duplicate margin. <a href='http://commits.kde.org/mailcommon/0584cbaf40b33f068620e2f40470c76a864eae4d'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Now when we use ESC we force focus on viewer. <a href='http://commits.kde.org/messagelib/cdd80e5d49b9d2741c3f556ca1efa7320976a9fa'>Commit.</a> </li>
<li>Use >=. <a href='http://commits.kde.org/messagelib/0639c033a1665bf7f144d40900f2bf54ed64ea81'>Commit.</a> </li>
<li>Fix Bug 393688 - DND Attachments from one existing message to new message. <a href='http://commits.kde.org/messagelib/21d0b28183d8aa2e3a4f0017c5c453347a3a50da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393688'>#393688</a></li>
<li>Distinguish between settings and explicit override for external content. <a href='http://commits.kde.org/messagelib/0bb1c5b12745b801f1aa4d6c630911845409e8ee'>Commit.</a> </li>
<li>Load external references in encrypted emails only on explicit request. <a href='http://commits.kde.org/messagelib/5a57b15ed2ab77fb0e6c193432e3736b5666a0b9'>Commit.</a> </li>
<li>Remove unused variable. <a href='http://commits.kde.org/messagelib/de5b5d266b022ed91db7dca48f774fdcf92bca53'>Commit.</a> </li>
<li>Clean up css. <a href='http://commits.kde.org/messagelib/357ec92d1eb5af08539127825404bdde614b3619'>Commit.</a> </li>
<li>Fix expand/collapse addresslist. <a href='http://commits.kde.org/messagelib/707461db2e72b6d93a8f13e1aa6d884a77895bd1'>Commit.</a> </li>
<li>Fix size. <a href='http://commits.kde.org/messagelib/f5febd630c973b0deb4aea54457b1a782b77c015'>Commit.</a> </li>
<li>Cache value. <a href='http://commits.kde.org/messagelib/c62a30a594b21e6684fd486c2f23d080cf74e889'>Commit.</a> </li>
<li>Use directly css. Remove script for show full address. I know that some autotests are broken. <a href='http://commits.kde.org/messagelib/73564e655b8aa58726bb29eb7ca6e99fee440e00'>Commit.</a> </li>
<li>Clean up. <a href='http://commits.kde.org/messagelib/8f300457fbfb1d857e30df850f0e34f63699734e'>Commit.</a> </li>
<li>Cache number of identity. <a href='http://commits.kde.org/messagelib/c4e1ede8b22ca756fec11472608a2aebe7dc8071'>Commit.</a> </li>
<li>Cache string. <a href='http://commits.kde.org/messagelib/67f50b0e9b494360c1a776b5d5cdef9a3222e09b'>Commit.</a> </li>
<li>Add comment. <a href='http://commits.kde.org/messagelib/b492802f048ec9aa4a93c9898ae0456268147180'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/e4104f70338c0bd01b57d9c1f354ca1f84fa4db8'>Commit.</a> </li>
<li>Add comment. <a href='http://commits.kde.org/messagelib/d2e078c5443a1d9ee66b2828841d3e5c6d0b218a'>Commit.</a> </li>
<li>Initialize value. <a href='http://commits.kde.org/messagelib/b10de033d24de25f8f3848101c1066e37a45cb3f'>Commit.</a> </li>
<li>Warning--. <a href='http://commits.kde.org/messagelib/938394cd5d507f6350f4932eb20a83df9e621749'>Commit.</a> </li>
<li>Minor optimization. <a href='http://commits.kde.org/messagelib/d8a0f92908a2f9ef3aef495fe863797f23b1ea13'>Commit.</a> </li>
<li>Don't use injection code for attachment. <a href='http://commits.kde.org/messagelib/f80ef00562eed01aeb5c6997af170d5cd77da9f5'>Commit.</a> </li>
<li>Return attachmentHtml directly. <a href='http://commits.kde.org/messagelib/e0dc9697108674e4c87f02989b88b3ab4ee87a0c'>Commit.</a> </li>
<li>Remove show/hide method. <a href='http://commits.kde.org/messagelib/4c2dabfbf93bcef8dab7b72c7ae38a6890f687f6'>Commit.</a> </li>
<li>Remove hide/show attachment as discussed this WE. <a href='http://commits.kde.org/messagelib/96e19c1700ed85da1136dc019680414fd612b948'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Don't crash if fopen fails. <a href='http://commits.kde.org/okular/13bdb97084d2747c24f90ce50413a8d85bb67c06'>Commit.</a> </li>
<li>Use toLocal8Bit instead of toUtf8. <a href='http://commits.kde.org/okular/991eb0ed31251429d566e882401eb4bf2ad57ba2'>Commit.</a> </li>
<li>Obey umask rules when saving new file. <a href='http://commits.kde.org/okular/c5592689874f41df393b44f3ba572acc751c5645'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392682'>#392682</a></li>
<li>Fix crash on exit when having edited a text area. <a href='http://commits.kde.org/okular/3cea7c9927f0d7ff32bcfc1fbacde229d98e24c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393334'>#393334</a></li>
<li>[Okular] Bug 387282: Highlighting of search results lost when rotating page. <a href='http://commits.kde.org/okular/6a2ed4f3144bb4c22d2aeb4d72968fb8de898b76'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387282'>#387282</a></li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Fix memory leaks. <a href='http://commits.kde.org/spectacle/d929ba42d01d7f2956f78d308d23119c40a7b1dd'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Merge in fixes from kmymoney related script variant. <a href='http://commits.kde.org/umbrello/15d569b14408ab80d16d2bd7d8b76f5ef5aa2844'>Commit.</a> </li>
<li>Add c++ import support for combinations of 'explicit', 'constexp' keywords in combination with constructors. <a href='http://commits.kde.org/umbrello/33d1e40db6b7888c1f08635dbffee32a33ed255c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392984'>#392984</a></li>
<li>Fix "c++ importer does not recognize 'explicit' keyword". <a href='http://commits.kde.org/umbrello/d54c4979cbffc557e9c8bc3658aba90c509d1f6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392984'>#392984</a></li>
</ul>