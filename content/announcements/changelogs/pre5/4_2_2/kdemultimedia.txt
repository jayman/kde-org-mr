------------------------------------------------------------------------
r935444 | scripty | 2009-03-05 07:51:01 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r935739 | mpyne | 2009-03-06 03:20:28 +0000 (Fri, 06 Mar 2009) | 3 lines

Backport fix for file renamer empty tag handling when "Ignore Empty Tags" is selected to
KDE 4.2.2.

------------------------------------------------------------------------
r935747 | mpyne | 2009-03-06 04:04:52 +0000 (Fri, 06 Mar 2009) | 6 lines

Backport file renamer dialog fixes for JuK to KDE 4.2.2.  Now the file renamer should only be
as broken as it was in KDE 3.5.10 as opposed to being completely useless.

I double-checked to make sure no localized strings were changed, if I screwed that up please
let me know and I will fix.

------------------------------------------------------------------------
r937025 | lueck | 2009-03-08 21:46:50 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r937054 | mpyne | 2009-03-08 22:32:49 +0000 (Sun, 08 Mar 2009) | 1 line

Backport question mark icon fix for audiocd to KDE 4.2.2.
------------------------------------------------------------------------
r937928 | aacid | 2009-03-10 20:00:05 +0000 (Tue, 10 Mar 2009) | 3 lines

Backport r937926.
Save the config when chaning the master channel, acked by Helio

------------------------------------------------------------------------
r939002 | lukas | 2009-03-13 14:50:06 +0000 (Fri, 13 Mar 2009) | 2 lines

add missing i18n()

------------------------------------------------------------------------
r939469 | lueck | 2009-03-14 22:39:10 +0000 (Sat, 14 Mar 2009) | 1 line

add missing files for message extraction, backport from trunk
------------------------------------------------------------------------
r939962 | scripty | 2009-03-16 07:54:43 +0000 (Mon, 16 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r940739 | scripty | 2009-03-18 08:18:44 +0000 (Wed, 18 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r942595 | scripty | 2009-03-22 07:43:15 +0000 (Sun, 22 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944118 | reed | 2009-03-25 04:27:24 +0000 (Wed, 25 Mar 2009) | 1 line

couldn't find vorbis headers in non-standard location
------------------------------------------------------------------------
r944174 | scripty | 2009-03-25 08:46:44 +0000 (Wed, 25 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
