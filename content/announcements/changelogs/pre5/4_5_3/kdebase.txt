------------------------------------------------------------------------
r1181444 | scripty | 2010-10-01 15:49:34 +1300 (Fri, 01 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1181770 | scripty | 2010-10-02 15:41:51 +1300 (Sat, 02 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1181847 | lueck | 2010-10-03 02:55:23 +1300 (Sun, 03 Oct 2010) | 1 line

nepomuksearch has a documentation
------------------------------------------------------------------------
r1181906 | hpereiradacosta | 2010-10-03 06:23:29 +1300 (Sun, 03 Oct 2010) | 3 lines

Fixed checks on minimum progressbar indicator size. 
BUG: 253037

------------------------------------------------------------------------
r1182031 | scripty | 2010-10-03 15:53:46 +1300 (Sun, 03 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1182391 | jriddell | 2010-10-05 01:03:37 +1300 (Tue, 05 Oct 2010) | 6 lines

backport 1182380.
https://bugs.launchpad.net/ubuntu/+source/kdebase-workspace/+bug/602389
dolphin wasn't able to show free space in places for
    removable devices.
patch by Alessandro Ghersi <alessandro-ghersi@kubuntu.org>

------------------------------------------------------------------------
r1182469 | mart | 2010-10-05 06:24:02 +1300 (Tue, 05 Oct 2010) | 2 lines

backport binding for QList<double>

------------------------------------------------------------------------
r1182520 | anschneider | 2010-10-05 09:12:18 +1300 (Tue, 05 Oct 2010) | 2 lines

kio_sftp: Fixed symlink handling of files.

------------------------------------------------------------------------
r1182543 | aseigo | 2010-10-05 10:42:04 +1300 (Tue, 05 Oct 2010) | 3 lines

convert the number first to a string, otherwise it doesn't append anything. noticed by Oliver oin the BR
CCBUG:156475

------------------------------------------------------------------------
r1182719 | jlayt | 2010-10-06 02:25:32 +1300 (Wed, 06 Oct 2010) | 7 lines

Fix Plasma Calendar to remember if user selects to not show holidays.

Not to be forward-ported to trunk as trunk will very soon work differently.

BUG: 250778


------------------------------------------------------------------------
r1182731 | jlayt | 2010-10-06 02:47:01 +1300 (Wed, 06 Oct 2010) | 3 lines

Plasma Calendar don't use or save default region if user selects not to display
holidays.

------------------------------------------------------------------------
r1182798 | freininghaus | 2010-10-06 06:56:37 +1300 (Wed, 06 Oct 2010) | 9 lines

Prevent that icons overlap in Details View when zooming.

The problem was that a maximum size was assigned to KFileItemDelegate
for displaying items without considering that icon zooming may change
the item height.

BUG: 234600
FIXED-IN: 4.5.3

------------------------------------------------------------------------
r1182818 | lueck | 2010-10-06 07:22:36 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1182819 | lueck | 2010-10-06 07:22:57 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1182820 | lueck | 2010-10-06 07:23:13 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1182844 | lueck | 2010-10-06 08:26:40 +1300 (Wed, 06 Oct 2010) | 1 line

missed these in the previous backport
------------------------------------------------------------------------
r1182933 | scripty | 2010-10-06 16:00:54 +1300 (Wed, 06 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1182952 | habacker | 2010-10-06 19:37:55 +1300 (Wed, 06 Oct 2010) | 1 line

ported back r1182950 and r1182951 from trunk
------------------------------------------------------------------------
r1183290 | lueck | 2010-10-07 09:28:44 +1300 (Thu, 07 Oct 2010) | 1 line

doc backport
------------------------------------------------------------------------
r1183341 | scripty | 2010-10-07 15:48:31 +1300 (Thu, 07 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1183361 | graesslin | 2010-10-07 20:50:20 +1300 (Thu, 07 Oct 2010) | 5 lines

Do not allow to move desktop windows in desktop grid.
BUG: 253481
FIXED-IN: 4.5.3


------------------------------------------------------------------------
r1183470 | aseigo | 2010-10-08 02:39:16 +1300 (Fri, 08 Oct 2010) | 2 lines

use non-generic so #s

------------------------------------------------------------------------
r1183599 | scripty | 2010-10-08 15:55:20 +1300 (Fri, 08 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1184018 | hpereiradacosta | 2010-10-09 19:13:45 +1300 (Sat, 09 Oct 2010) | 6 lines

backport r1184017
Revert commit r1182380 and backport r1182391
It is _not_ a correct patch.
It creates artifact (the scrollbar slider is now larger than the groove)
Besides it should not be necessary in trunk.

------------------------------------------------------------------------
r1184157 | ossi | 2010-10-09 23:14:13 +1300 (Sat, 09 Oct 2010) | 9 lines

don't crash on excess presses of <return>

a repeatedly pressed, bouncing or held down return key would cause the
last field (password) to be re-sent to the core, which would of course
cause a protocol error and subsequent shutdown.

CCBUG: 251987


------------------------------------------------------------------------
r1184158 | ossi | 2010-10-09 23:15:57 +1300 (Sat, 09 Oct 2010) | 9 lines

suppress d-bus startup

set a fake session address.
this avoids all kinds of kde infrastructure from being fired up.

BUG: 251987
FIXED-IN: 4.5.3


------------------------------------------------------------------------
r1184159 | ossi | 2010-10-09 23:16:45 +1300 (Sat, 09 Oct 2010) | 5 lines

silence uninitialized variable warning

seems gcc got more pedantic again ...


------------------------------------------------------------------------
r1184328 | scripty | 2010-10-10 15:38:16 +1300 (Sun, 10 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1184471 | alexmerry | 2010-10-11 01:06:40 +1300 (Mon, 11 Oct 2010) | 3 lines

Backport r1184470: Ignore MPRIS2 interfaces (which look confusingly like MPRIS1 interfaces)


------------------------------------------------------------------------
r1184989 | scripty | 2010-10-12 16:18:03 +1300 (Tue, 12 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1185296 | fredrik | 2010-10-13 10:01:29 +1300 (Wed, 13 Oct 2010) | 7 lines

Backport r1185295:

Fix the model being dynamically sorted when the view is set to unsorted.
Based on a patch by Jason Harvey <kdebgz.9.almoff@spamgourmet.com>.

CBUG: 227157

------------------------------------------------------------------------
r1185568 | mmrozowski | 2010-10-14 07:54:27 +1300 (Thu, 14 Oct 2010) | 1 line

Backport 1185504 - fix automagic libgps dependency by introducing WITH_libgps CMake option (macro_optional_find_package).
------------------------------------------------------------------------
r1186036 | scripty | 2010-10-15 15:53:51 +1300 (Fri, 15 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1186303 | hindenburg | 2010-10-16 07:54:43 +1300 (Sat, 16 Oct 2010) | 5 lines

When closing a tab, select the previously focused tab.

Backport
CCBUG: 240037

------------------------------------------------------------------------
r1186304 | hindenburg | 2010-10-16 07:55:10 +1300 (Sat, 16 Oct 2010) | 1 line

update version
------------------------------------------------------------------------
r1186317 | dfaure | 2010-10-16 09:03:12 +1300 (Sat, 16 Oct 2010) | 4 lines

'~' also denotes the beginning of a local path, like '/', so don't prepend http://www. in the completion offers.
FIXED-IN: 4.5.3
BUG: 90005

------------------------------------------------------------------------
r1186482 | ruberg | 2010-10-17 05:41:54 +1300 (Sun, 17 Oct 2010) | 3 lines

Backporting fixes from trunk. Applet is now sized correctly (exspecially in the panel) and uses standard theme colors


------------------------------------------------------------------------
r1186485 | ruberg | 2010-10-17 05:48:08 +1300 (Sun, 17 Oct 2010) | 3 lines

Added signal to repaint applet when theme changes


------------------------------------------------------------------------
r1186599 | scripty | 2010-10-17 15:58:50 +1300 (Sun, 17 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1186752 | lliehu | 2010-10-18 01:48:23 +1300 (Mon, 18 Oct 2010) | 2 lines

Enable translations in keyboard daemon (fix catalog name)

------------------------------------------------------------------------
r1186784 | lvsouza | 2010-10-18 03:20:09 +1300 (Mon, 18 Oct 2010) | 6 lines

backport r1186781 by lvsouza from trunk to the 4.5 branch:

I added udi() to NetworkInterface some months ago in order to
make the Modem Manager backend work, but I forgot to add this
to make wicd backend continue to link.

------------------------------------------------------------------------
r1186798 | pino | 2010-10-18 04:23:22 +1300 (Mon, 18 Oct 2010) | 4 lines

remove udi() implementation added by r1186784, as it is already implemented

CCMAIL: lamarque@gmail.com

------------------------------------------------------------------------
r1186955 | scripty | 2010-10-18 15:50:06 +1300 (Mon, 18 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1187708 | dfaure | 2010-10-20 21:45:33 +1300 (Wed, 20 Oct 2010) | 9 lines

Ctrl+Shift+R is already taken by "hard reload", so change the shortcut for closing a split view to Ctrl+Shift+W
  (by analogy to Ctrl+W for closing a tab), as suggested in 117300.
BUG: 117300
BUG: 184492
FIXED-IN: 4.5.3
--Thi

M    src/konqmainwindow.cpp

------------------------------------------------------------------------
r1187715 | lueck | 2010-10-20 22:13:01 +1300 (Wed, 20 Oct 2010) | 2 lines

fix wrong number of args, backport from trunk revision 1154547
BUG:182004
------------------------------------------------------------------------
r1188013 | scripty | 2010-10-21 15:53:42 +1300 (Thu, 21 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1188339 | scripty | 2010-10-22 15:53:38 +1300 (Fri, 22 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1188631 | dfaure | 2010-10-23 08:15:44 +1300 (Sat, 23 Oct 2010) | 2 lines

backport icon fixes r1188323 & r1188629

------------------------------------------------------------------------
r1188958 | ossi | 2010-10-24 03:03:48 +1300 (Sun, 24 Oct 2010) | 12 lines

exit on x error if parent process is gone

this condition most likely means that our parent forgot to kill us (or
crashed in the worst case) and we are now trying to render to an
unexisting window, which floods the session log with errors.

FIXED-IN: 4.5.3
BUG: 245147

the bug as a whole may or may not be fixed ...


------------------------------------------------------------------------
r1188967 | ossi | 2010-10-24 03:45:52 +1300 (Sun, 24 Oct 2010) | 10 lines

fix possible crash

XQueryTree() can fail, so the following XFree() would use an
uninitialized pointer.
no idea what side effects such a failure will have, but the other
code paths involving VRoot also silently ignore errors.

BUG: 245495
FIXED-IN: 4.5.3

------------------------------------------------------------------------
r1188997 | ossi | 2010-10-24 05:53:46 +1300 (Sun, 24 Oct 2010) | 9 lines

fix terminally broken type exclusion logic

if a screensaver had multiple types, they'd all have to be forbidden to
actually exclude it.

BUG: 242266
FIXED-IN: 4.5.3


------------------------------------------------------------------------
r1189359 | fredrik | 2010-10-25 09:10:03 +1300 (Mon, 25 Oct 2010) | 7 lines

Backport r1182198:

Only call glXBindTexImageEXT() when the contents of the pixmap has changed.

We don't need to do this every time we bind the texture to a GL context,
even with strict binding.

------------------------------------------------------------------------
r1189360 | fredrik | 2010-10-25 09:11:14 +1300 (Mon, 25 Oct 2010) | 10 lines

Backport r1183978:

Specify the texture target explicitly when creating a GLXPixmap.

Prefer the GL_TEXTURE_2D target if the framebuffer configuration indicates
that it's supported.

This fixes a performance problem with the r600g driver.
freedesktop bug 30483.

------------------------------------------------------------------------
r1189733 | fredrik | 2010-10-26 07:01:34 +1300 (Tue, 26 Oct 2010) | 4 lines

Backport 1189731:
Work around a bug in the GLSL compiler in Mesa 7.9.
CCBUG: 255230

------------------------------------------------------------------------
r1190060 | dfaure | 2010-10-27 08:52:33 +1300 (Wed, 27 Oct 2010) | 2 lines

First test with kio_nfs: nfs://hostname -> it asserted. Fixed.

------------------------------------------------------------------------
r1190095 | hpereiradacosta | 2010-10-27 11:25:12 +1300 (Wed, 27 Oct 2010) | 4 lines

Backport r1190093
Do not block events processed by animations event filter.
CCBUG 255357

------------------------------------------------------------------------
r1190138 | scripty | 2010-10-27 15:53:59 +1300 (Wed, 27 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1190514 | aspotashev | 2010-10-28 11:29:42 +1300 (Thu, 28 Oct 2010) | 2 lines

kioslave/fish: Update documentation date and software version

------------------------------------------------------------------------
r1190558 | scripty | 2010-10-28 15:48:53 +1300 (Thu, 28 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1190636 | mueller | 2010-10-29 00:48:04 +1300 (Fri, 29 Oct 2010) | 2 lines

bump versions

------------------------------------------------------------------------
r1190780 | rkcosta | 2010-10-29 10:35:17 +1300 (Fri, 29 Oct 2010) | 13 lines

Backport r1190779.

Properly quote the call to 'export -p'.

Even though bash (and dash) do not differ 'export' and 'export -p', that call
was clearly wrong as the -p was being passed as a parameter to /bin/sh, not
export.

Thanks to M. Warner Losh for bringing that up.

CCMAIL: imp@ixsystems.com
CCMAIL: kde-freebsd@kde.org

------------------------------------------------------------------------
r1190789 | orlovich | 2010-10-29 11:14:15 +1300 (Fri, 29 Oct 2010) | 4 lines

Workaround problems with XEmbed clipping w/Qt 4.7.0

BUG:247181

------------------------------------------------------------------------
