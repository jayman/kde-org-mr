2005-03-06 06:15 +0000 [r395190]  pletourn

	* konq-plugins/searchbar/searchbar.cpp: Encode search string even
	  when there is no default search engine BUG:94862

2005-03-07 07:23 +0000 [r395411]  benb

	* debian/lnkforward.1 (added), debian/orient.py.1, debian/rules,
	  debian/kdeaddons-kfile-plugins.install, debian/copyright,
	  debian/control, debian/konq-plugins.install, debian/jpegorient.1,
	  debian/kicker-applets.install, debian/changelog,
	  debian/kate-plugins.install, debian/atlantikdesigner.1,
	  debian/noatun-plugins.install, debian/source.lintian-overrides,
	  debian/kdeaddons-kfile-plugins.manpages (added): Sync with
	  KDE_3_3_BRANCH.

2005-03-07 18:24 +0000 [r395573]  metz

	* kicker-applets/mediacontrol/xmmsInterface.cpp,
	  kicker-applets/mediacontrol/xmmsInterface.h: Backported fix for
	  XMMS startup, somehow this was missing for XMMS but added for all
	  other players.

2005-03-08 01:04 +0000 [r395649]  benb

	* debian/rules, debian/patches (removed): Remove patch that is no
	  longer necessary.

2005-03-10 21:15 +0000 [r396469]  benb

	* debian/kate-plugins.install,
	  debian/kdeaddons-kfile-plugins.install, debian/control,
	  debian/kicker-applets.install, debian/changelog, debian/rules,
	  debian/konq-plugins.install, debian/kaddressbook-plugins.install,
	  debian/kdeaddons-doc-html.install: Updated build-deps and install
	  files.

2005-03-11 22:11 +0000 [r396821-396820]  benb

	* debian/atlantikdesigner.menu: Update menu files.

	* debian/control: Note new plugins in description.

2005-03-11 22:26 +0000 [r396829]  benb

	* debian/konq-plugins.menu, debian/rules,
	  debian/atlantikdesigner.menu, debian/ksig.override (removed),
	  debian/ksig.menu, debian/konq-plugins.override (removed),
	  debian/atlantikdesigner.override (removed): Remove the obsolete
	  kderemove tags from menu files.

2005-03-11 22:49 +0000 [r396836]  benb

	* debian/changelog: Note what we did.

2005-03-11 23:05 +0000 [r396839]  benb

	* debian/rules, debian/changelog: Re-enable rellinks plugin.

2005-03-12 18:44 +0000 [r397028]  lukas

	* kate/cppsymbolviewer/Makefile.am: user visible strings in *.h
	  files are generally not a good idea

2005-03-12 23:32 +0000 [r397093]  benb

	* debian/control: Update descriptions and dependencies.

2005-03-13 23:43 +0000 [r397425]  benb

	* debian/control: Update build-deps for kdepim 3.4.

2005-03-14 06:37 +0000 [r397467]  benb

	* debian/copyright: Finished license audit for kdeaddons 3.4. The
	  full text of the GFDL has also been added to the debian copyright
	  file.

2005-03-21 02:01 +0000 [r399388]  thiago

	* konq-plugins/arkplugin/arkplugin.cpp: Backporting the changes
	  between 1.14 and 1.15.

2005-03-25 23:04 +0000 [r400653]  benb

	* debian/changelog: Note what we did in 3_3_BRANCH.

2005-03-26 13:51 +0000 [r400752]  waba

	* konq-plugins/akregator/akregatorplugin.cpp: KIOSK: Don't crash
	  when editable_desktop_icons is disabled.

2005-04-07 17:13 +0000 [r403828]  charles

	* noatun-plugins/oblique/cmodule.cpp: backport: show schema

2005-04-15 09:00 +0000 [r405688]  dhaumann

	* kate/xmltools/plugin_katexmltools.cpp: backport: Do not
	  disconnect everything from charactersInteractivelyInserted when
	  activating a DTD. Note that this works best if along with an
	  update of kdelibs/kate/part, otherwise the word completion might
	  conflict with the xml completion. (done in branch, too)

2005-04-20 22:48 +0000 [r406840]  rytilahti

	* konq-plugins/akregator/konqfeedicon.cpp,
	  konq-plugins/akregator/akregatorplugin.cpp: backport my last
	  commit

2005-04-25 10:17 +0000 [r407728]  adrian

	* konq-plugins/domtreeviewer/domtreecommands.cpp: fix compile on
	  64bit with gcc 4

2005-04-25 13:51 +0000 [r407777]  benb

	* debian/vimpart.README.Debian, debian/control, debian/changelog:
	  Forwardport recent changes on KDE_3_3_BRANCH.

2005-04-27 16:23 +0000 [r408208]  ogoffart

	* konq-plugins/rellinks/plugin_rellinks.rc: Backport: Hide the
	  rellinks toolbar by default. This workaround a bug in Konqueror
	  which seems to show this toolbar while it shouldn't (on popups
	  for examples) BUG: 83514 BUG: 96229

2005-05-01 06:31 +0000 [r408947]  benb

	* debian/control, debian/changelog: Forwardport changes from
	  3_3_BRANCH.

2005-05-10 10:32 +0000 [r411876]  binner

	* kdeaddons.lsm: update lsm for release

2005-05-15 05:49 +0000 [r414016]  benb

	* debian/changelog, debian/rules: New 3.4 upload to experimental.
	  Merge changes from 3.3.x, disable CVS rules.

2005-05-22 08:58 +0000 [r416668]  coolo

	* kicker-applets/mediacontrol/mediacontrol.h: forward declare what
	  you later want as friend

