2005-03-08 14:32 +0000 [r395794]  mattr

	* parts/snippet/snippet_widget.cpp: Backport the fixes for bugs
	  100332 and 100406 (revision 1.25) Should be in KDevelop 3.2.1
	  CCBUGS: 100332, 100406

2005-03-08 20:11 +0000 [r395926-395924]  mattr

	* vcs/cvsservice/cvsprocesswidget.cpp: backport revision 1.19. This
	  fix should be in KDevelop 3.2.1 CCBUGS: 99590 Original log
	  message: If the line already contains tags (or any other text
	  surrounded by "<" and ">") we need to replace these delimiters
	  with their corresponding HTML code. That way the tags will be no
	  longer recognized by QTextEdit but displayed as plain text.
	  Besides that QTextEdit crashes in some special constellations if
	  we try to wrap or own tags around already existing ones.

	* vcs/cvsservice/cvsdiffpage.h, vcs/cvsservice/cvsdiffpage.cpp:
	  backport: cvsdiffpage.cpp - revision 1.9 cvsdifpage.h - revision
	  1.6 Should be in KDevelop 3.2.1 CCBUGS: 99590 Original log
	  message: Fixed program of some diff-lines beeing cutted of in the
	  middle.

2005-03-09 22:39 +0000 [r396235]  larkang

	* languages/cpp/app_templates/kofficepart/kopart_aboutdata.h,
	  languages/cpp/app_templates/kofficepart/kde-configure.in.in,
	  languages/cpp/app_templates/kofficepart/kopart.kdevtemplate:
	  Backport fixes for koffice template

2005-03-10 21:48 +0000 [r396485]  mueller

	* kdevelop.m4.in: unbreak configure checks (backport)

2005-03-12 03:59 +0000 [r396889]  mattr

	* src/mainwindowshare.cpp: Backport the fix for bug 101289. Should
	  be in KDevelop 3.2.1 CCBUGS: 101289

2005-03-12 04:48 +0000 [r396900-396898]  mattr

	* languages/cpp/app_templates/kscreensaver/kscreensaver.h: backport
	  warning fix about self inclusion

	* languages/cpp/debugger/dbgpsdlg.cpp: Backport the fix for 79986.
	  It should be in KDevelop 3.2.1 CCBUG: 79986

2005-03-24 12:51 +0000 [r400217]  rdale

	* ChangeLog, pics/toolbar/Makefile.am: * Added missing ruby
	  debugger icons installation

2005-04-01 17:17 +0000 [r402449]  marchand

	* vcs/subversion/subversion_fileinfo.cpp: backport KURL passing fix

2005-04-13 20:46 +0000 [r405435]  neundorf

	* buildtools/custommakefiles/customprojectpart.cpp,
	  buildtools/custommakefiles/customprojectpart.h: -also show
	  makefile targets defined in included sub-makefiles -speedup
	  parsing, QFile::readline() is much faster than
	  QTextStream::readline() (for 400 files 300 ms instead of 800 ms)
	  Alex

2005-04-13 21:14 +0000 [r405441]  marchand

	* kdevelop.m4.in, configure.in.in: backport compilation fix with
	  kdelibs 3.3 when subversion is not available

2005-04-21 10:34 +0000 [r406902]  rdale

	* languages/ruby/debugger/rdbcontroller.cpp: * Promoted compile fix
	  to the release branch

2005-04-22 00:43 +0000 [r407043]  mattr

	* languages/cpp/app_templates/kscreensaver/kscreensaver.h: backport
	  fix for kscreensaver template (makes it compile out of the box)

2005-04-26 19:07 +0000 [r408012]  dagerbo

	* parts/ctags2/ctags2_part.cpp: Backport. Fixes bug #99964 and bug
	  #104510

2005-05-08 08:58 +0000 [r410712]  binner

	* Makefile.am: fix for .svn/

2005-05-10 10:32 +0000 [r411876]  binner

	* kdevelop.lsm: update lsm for release

2005-05-10 10:42 +0000 [r411884]  binner

	* configure.in.in: Increasing versions listed in RELEASE-CHECKLIST

