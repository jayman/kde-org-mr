------------------------------------------------------------------------
r1216107 | scripty | 2011-01-22 01:03:40 +1300 (Sat, 22 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1216181 | mart | 2011-01-22 08:18:26 +1300 (Sat, 22 Jan 2011) | 2 lines

backport mapping to twitter api

------------------------------------------------------------------------
r1216188 | mart | 2011-01-22 08:47:22 +1300 (Sat, 22 Jan 2011) | 2 lines

backport in_reply_to_status_id rename

------------------------------------------------------------------------
r1216215 | aseigo | 2011-01-22 12:35:14 +1300 (Sat, 22 Jan 2011) | 3 lines

work around non-thread safety of kexiv
BUG:263825

------------------------------------------------------------------------
r1217531 | sebas | 2011-01-28 08:27:22 +1300 (Fri, 28 Jan 2011) | 18 lines

Usability and performance improvements webslice applet

In particular:
- Turn the lineedit into an editable combobox showing some of the more interesting elements (those with an id attached)
- Add a reload button to load a newly selected page, repopulate the elements dropdown after load is finished
- Show the full page zoomed out while configuring, and paint a mask highlighting the currently selected element over it
- Fix cyclic resizing problems that lead to huge performance problems while resizing
- No more fixed aspect ratio (#221235)
- Compress resize events per 100ms to make resizing smoother
- Fix picking up config changes by the slice
- Cache slice geometry, by basing the geometries on the original geometry of the elements, we get much more precise viewports, it also saves a lot of dicking around with the current zoomFactor
- (No string changes)

backported from trunk r1217392 - r1217400, OK'ed on plasma-devel

BUG:221235
FIXED-IN:4.6.1

------------------------------------------------------------------------
r1217585 | aseigo | 2011-01-28 12:23:43 +1300 (Fri, 28 Jan 2011) | 3 lines

bounds check pos
BUG:202918

------------------------------------------------------------------------
r1217763 | scripty | 2011-01-29 02:05:58 +1300 (Sat, 29 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1218211 | pino | 2011-02-01 22:46:41 +1300 (Tue, 01 Feb 2011) | 2 lines

remove unneeded include

------------------------------------------------------------------------
