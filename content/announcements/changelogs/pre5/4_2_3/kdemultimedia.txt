------------------------------------------------------------------------
r946320 | scripty | 2009-03-29 08:00:46 +0000 (Sun, 29 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r947694 | scripty | 2009-04-01 07:40:35 +0000 (Wed, 01 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r947969 | mpyne | 2009-04-01 20:22:16 +0000 (Wed, 01 Apr 2009) | 4 lines

Backport fix for JuK service menu to KDE 4.2.3.  HOWEVER, since the required DBus interface
was not available in KDE 4.2 the service menu is disabled rather than leaving a broken
service menu in place.

------------------------------------------------------------------------
r948986 | scripty | 2009-04-04 08:32:07 +0000 (Sat, 04 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r951399 | scripty | 2009-04-09 07:22:20 +0000 (Thu, 09 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r960744 | mpyne | 2009-04-29 00:09:20 +0000 (Wed, 29 Apr 2009) | 4 lines

Backport fix for a common JuK crasher from Jiri Palecek to KDE 4.2.3.

BUG:179776

------------------------------------------------------------------------
r960745 | mpyne | 2009-04-29 00:12:31 +0000 (Wed, 29 Apr 2009) | 1 line

Bump JuK version for KDE 4.2.3
------------------------------------------------------------------------
r961233 | aacid | 2009-04-29 17:56:17 +0000 (Wed, 29 Apr 2009) | 2 lines

remove dragonplayer.org references

------------------------------------------------------------------------
