------------------------------------------------------------------------
r945017 | dfaure | 2009-03-26 17:51:09 +0000 (Thu, 26 Mar 2009) | 2 lines

Backport unittest, just in case. Passes here.

------------------------------------------------------------------------
r945019 | dfaure | 2009-03-26 17:57:27 +0000 (Thu, 26 Mar 2009) | 4 lines

My previous patch assumed "Only accept this cookie". But the text can actually say "Only accept these cookies", if N are shown in the dialog;
so make the code actually do that -- accept all the cookies that were shown, and not other cookies that might have come in from the same host
while the dialog was up.

------------------------------------------------------------------------
r945023 | dfaure | 2009-03-26 18:03:09 +0000 (Thu, 26 Mar 2009) | 2 lines

Backport r938717: You can't assume that the webserver is sending you 1KB upfront. Sometimes yes, sometimes no...

------------------------------------------------------------------------
r945025 | dfaure | 2009-03-26 18:07:06 +0000 (Thu, 26 Mar 2009) | 2 lines

Backport fix for kglobalsettingstest

------------------------------------------------------------------------
r945028 | dfaure | 2009-03-26 18:08:51 +0000 (Thu, 26 Mar 2009) | 2 lines

Backport r915764 by skelly: disable invalid unittests.

------------------------------------------------------------------------
r945034 | dfaure | 2009-03-26 18:26:53 +0000 (Thu, 26 Mar 2009) | 4 lines

Fix crash in kdirmodeltest by backporting the actual fixes for it:
r927192: Fix non-working (and/or crashing) overwriting of a file (or symlink) with a directory. (BUG 151851)
r925530: Fix the fix for #164185: comparing mimetype pointers doesn't work (they always differ), compare their names instead.

------------------------------------------------------------------------
r945035 | dfaure | 2009-03-26 18:30:41 +0000 (Thu, 26 Mar 2009) | 2 lines

like in trunk: disable non-working unittest.

------------------------------------------------------------------------
r945181 | porten | 2009-03-27 03:16:53 +0000 (Fri, 27 Mar 2009) | 2 lines

Make the date string 'Mon Nov 24 04:25:03 -0800 2008' from bug #176511 parsable. Ugly.

------------------------------------------------------------------------
r945316 | zwabel | 2009-03-27 09:25:26 +0000 (Fri, 27 Mar 2009) | 1 line

Backport r945314: Workaround a bug in the Qt 4.5 raster paint engine
------------------------------------------------------------------------
r945358 | mlaurent | 2009-03-27 10:44:54 +0000 (Fri, 27 Mar 2009) | 3 lines

Backport:
When we cancel spellchecking we don't want to remove all html formatting

------------------------------------------------------------------------
r945526 | orlovich | 2009-03-27 16:01:03 +0000 (Fri, 27 Mar 2009) | 8 lines

Properly handle re-creation of KHTMLGlobal --- don't drop the old ID table.
Fixes crashes on exit when this occurs.

Dirk, any chance this could still be rolled into 4.2.2?

CCMAIL:dirk@kde.org
BUG:187853

------------------------------------------------------------------------
r945532 | orlovich | 2009-03-27 16:39:41 +0000 (Fri, 27 Mar 2009) | 2 lines

Erk, shouldn't be here. Thanks vtokarev.

------------------------------------------------------------------------
r945545 | darioandres | 2009-03-27 17:19:05 +0000 (Fri, 27 Mar 2009) | 7 lines

Backport to 4.2branch of:
SVN commit 945542 by darioandres:

Rename KDE Control Center -> SystemSettings on timeout message

CCBUG: 188098

------------------------------------------------------------------------
r945549 | darioandres | 2009-03-27 17:29:21 +0000 (Fri, 27 Mar 2009) | 4 lines

Reverting commit 945545 as it is not allowed because of stringfreeze

CCBUG: 188098

------------------------------------------------------------------------
r945604 | zwabel | 2009-03-27 19:39:49 +0000 (Fri, 27 Mar 2009) | 1 line

Backport: Workaround: Don't crash in the Qt 4.5 raster engine when adapting the height of the completion widget
------------------------------------------------------------------------
r945667 | ggarand | 2009-03-27 22:29:31 +0000 (Fri, 27 Mar 2009) | 3 lines

automatically merged revision 945661:
enable the clear button on lineedits, provided their are not
excessively styled (no background-image, no custom border.)
------------------------------------------------------------------------
r945668 | ggarand | 2009-03-27 22:30:10 +0000 (Fri, 27 Mar 2009) | 5 lines

automatically merged revision 945662:
remove wrong assert, triggered by the '/', gui-less find-as-you-type
feature.

BUG: 188276
------------------------------------------------------------------------
r945669 | ggarand | 2009-03-27 22:31:33 +0000 (Fri, 27 Mar 2009) | 5 lines

automatically merged revision 945663:
it seems we go through ill-known, transitory states where Qt painters are
inactivated, thus returning null paintEngine(). That's no reason to crash.

(spotted by SadEagle on some dpaste.com page)
------------------------------------------------------------------------
r945699 | dfaure | 2009-03-28 00:17:34 +0000 (Sat, 28 Mar 2009) | 2 lines

Backport crashfix r945698.

------------------------------------------------------------------------
r945869 | chehrlic | 2009-03-28 13:27:00 +0000 (Sat, 28 Mar 2009) | 1 line

win32 compile++, make it work on w2k again
------------------------------------------------------------------------
r945888 | cfeck | 2009-03-28 14:12:53 +0000 (Sat, 28 Mar 2009) | 4 lines

backport r945882

CCBUG: 165460

------------------------------------------------------------------------
r946095 | ossi | 2009-03-28 17:11:34 +0000 (Sat, 28 Mar 2009) | 2 lines

backport: don't dirty the config when overwriting with same value, take 2

------------------------------------------------------------------------
r946162 | orlovich | 2009-03-28 19:37:49 +0000 (Sat, 28 Mar 2009) | 14 lines

/Enormous/ speedup for rendering speed of large/text-heavy documents
- takes /half/ the time as before this on the test files I have -
e.g. some French dictionary file from Spart now loads in 12 seconds,
while it used to take 24 (and 26 in 4.2.1); rendering of MySQL
reference doc html takes 8 seconds and not 16.

Note: results are likely to be less significant on other sorts of workloads.

This change works by caching width information for fonts in a sparse 2-level
tree, avoiding /insanely/ expensive costs of doing that via Qt (testing
was done with Qt4.5, optimized build). Also a slight shrinkage to
InheritedStyleData, but probably not enough to matter.


------------------------------------------------------------------------
r946617 | pino | 2009-03-29 19:47:13 +0000 (Sun, 29 Mar 2009) | 8 lines

Backport SVN commit 946616 by pino:

Fix a couple of small regressions in the tooltip handling:
- show up at the cursor position
CCBUG: 171545
- tie to the area of the content it refers to, so it is properly hidden when leaving its area
CCBUG: 175063

------------------------------------------------------------------------
r946623 | mlaurent | 2009-03-29 20:10:44 +0000 (Sun, 29 Mar 2009) | 3 lines

Backport:
don't replace when text edit is readonly

------------------------------------------------------------------------
r946770 | gateau | 2009-03-30 10:35:36 +0000 (Mon, 30 Mar 2009) | 3 lines

Use Qt::Tool window flags for popups to avoid having them shown over screensaver.

CCBUG:179924
------------------------------------------------------------------------
r947002 | woebbe | 2009-03-30 17:55:38 +0000 (Mon, 30 Mar 2009) | 1 line

doc fix
------------------------------------------------------------------------
r947109 | sebsauer | 2009-03-30 22:18:08 +0000 (Mon, 30 Mar 2009) | 6 lines

backport r947108
Don't crash if the conversation failed.
BUG:171722
BUG:187812


------------------------------------------------------------------------
r947125 | dfaure | 2009-03-31 00:53:36 +0000 (Tue, 31 Mar 2009) | 2 lines

Backport fix for ~KFilterDev crash on 32-bit systems, #157706 / #188415.

------------------------------------------------------------------------
r947126 | ggarand | 2009-03-31 01:03:24 +0000 (Tue, 31 Mar 2009) | 4 lines


reverting this for now : it proved not ready for prime time and I didn't expected re-tagging to go thus far. :(


------------------------------------------------------------------------
r947251 | mleupold | 2009-03-31 08:11:17 +0000 (Tue, 31 Mar 2009) | 4 lines

Backport of r947119.

Port Qt's method of handling PageUp/PageDown in QTextEdit. This will prevent cursorPositionChanged to be emitted for every single line that's scrolled.

------------------------------------------------------------------------
r947556 | chehrlic | 2009-03-31 19:03:54 +0000 (Tue, 31 Mar 2009) | 3 lines

Use BringWindowToTop() to force an activation of a window on win32 - backport r947551 from trunk

CCBUG: 187353
------------------------------------------------------------------------
r947575 | rjarosz | 2009-03-31 19:47:18 +0000 (Tue, 31 Mar 2009) | 4 lines

Backport commit 947574.
If icon is bigger than requested icon size scale it down otherwise the icon will be cut off.


------------------------------------------------------------------------
r947656 | mattr | 2009-04-01 03:30:27 +0000 (Wed, 01 Apr 2009) | 5 lines

Backport patch for bug 174262.
This should be in KDE starting with KDE 4.2.3

BUG: 174262

------------------------------------------------------------------------
r948254 | ggarand | 2009-04-02 16:34:31 +0000 (Thu, 02 Apr 2009) | 3 lines

automatically merged revision 948248:
recalculate dirty table grid if needed before using it in adjacent cell accessors(change from WC)
thanks to spart for noticing it
------------------------------------------------------------------------
r948298 | dfaure | 2009-04-02 18:16:38 +0000 (Thu, 02 Apr 2009) | 3 lines

Backport crash when closing ark after it embedded katepart; due to order of global-static-objects deletion.
Was bug 188667

------------------------------------------------------------------------
r948318 | mart | 2009-04-02 19:08:49 +0000 (Thu, 02 Apr 2009) | 2 lines

backport fix for folderview during rename of files

------------------------------------------------------------------------
r948320 | mart | 2009-04-02 19:10:24 +0000 (Thu, 02 Apr 2009) | 4 lines

backport the fix to the last fix
Don't start the fade out animation if the applet handle is still under
the mouse

------------------------------------------------------------------------
r948342 | ggarand | 2009-04-02 20:03:04 +0000 (Thu, 02 Apr 2009) | 28 lines

reinstate r945667 and backport along the fixes making it work
for real (r948016).

--------------------------------------------------------------------------
r945667 | ggarand | 2009-03-27 23:29:31 +0100 (ven, 27 mar 2009) | 3 lines

automatically merged revision 945661:
enable the clear button on lineedits, provided their are not
excessively styled (no background-image, no custom border.)
--------------------------------------------------------------------------
SVN commit 948016 by ggarand:

fix remaining problems with the lineedit clear button:

.use 'textEdited' insted of 'textChanged' signal, so we don't have to
 block signals on setText, which in turn wouldn't allow the clear button
 to show up (#188374)

.force use of the arrow cursor when above the lineedit clear button,
 overriding CSS defined cursor (#188376).

.fix clear button sometimes not working, due to Enter/Leave events not
 being correctly forwarded.
--------------------------------------------------------------------------
CCBUG: 188374, 188376



------------------------------------------------------------------------
r948348 | ggarand | 2009-04-02 20:13:48 +0000 (Thu, 02 Apr 2009) | 6 lines

automatically merged revision 947498:
We want the pop up delay of web tooltips to be constant.

Qt's behaviour of having a first tooltip at 700ms and following ones at
20ms is undesirable on the web where lingering over elements isn't
necessarily a call for more information.
------------------------------------------------------------------------
r948350 | ggarand | 2009-04-02 20:15:33 +0000 (Thu, 02 Apr 2009) | 7 lines

automatically merged revision 947499:
rework replaced elements painting
-render outlines
-properly check the painting rectangle bounds
-check visibility in all painting phases (#155599)

BUG: 155599
------------------------------------------------------------------------
r948351 | ggarand | 2009-04-02 20:16:17 +0000 (Thu, 02 Apr 2009) | 5 lines

automatically merged revision 948013:
fix wheel events in subframes not being forwarded to widgets when the root
view has a scroll offset.

dom events need to be mapped, not the Qt events that are passed along.
------------------------------------------------------------------------
r948352 | ggarand | 2009-04-02 20:17:25 +0000 (Thu, 02 Apr 2009) | 7 lines

automatically merged revision 948014:
.factor out some CSS parser code (initial patch from mccope@googlemail.com,
 derived from code found in webcore)
.increase CSS buffer's security padding to 8 bytes to prevent buggy flex
 from reading/writing past the end in some situations.

BUG: 167318
------------------------------------------------------------------------
r948353 | ggarand | 2009-04-02 20:19:16 +0000 (Thu, 02 Apr 2009) | 7 lines

automatically merged revision 948015:
.fix vintage line-breaking bug
 surely I must be the first guy in a decade to really understand that silly
 code (whee, i'm so proud). Let's document, before this knowledge is lost
 again.

BUG: 49441
------------------------------------------------------------------------
r948444 | ggarand | 2009-04-03 02:00:26 +0000 (Fri, 03 Apr 2009) | 2 lines

automatically merged revision 948443:
fix framesets nested in iframe not being displayed.
------------------------------------------------------------------------
r948589 | ggarand | 2009-04-03 09:21:03 +0000 (Fri, 03 Apr 2009) | 3 lines

automatically merged revision 948588:
can't really use the textEdited signal after all - it doesn't get triggered
by completion, so some changes are lost.
------------------------------------------------------------------------
r948748 | jacopods | 2009-04-03 17:13:41 +0000 (Fri, 03 Apr 2009) | 3 lines

backported 948747 - ignore leading/following whitespaces
CCBUG: 188665

------------------------------------------------------------------------
r948757 | ggarand | 2009-04-03 17:23:58 +0000 (Fri, 03 Apr 2009) | 10 lines

automatically merged revision 948754:
fix calcMinMaxWidth broken logic for new lines
in preserveLF mode (white-space: pre, pre-wrap, pre-line).

This bug would prevent line-wraping in a lot of situations.

New lines could also affect non-pre codepath - so make the code tighter,
matching the actual comments!

BUG: 154143, 130181
------------------------------------------------------------------------
r948882 | orlovich | 2009-04-04 01:57:21 +0000 (Sat, 04 Apr 2009) | 14 lines

Make sure that iframes created with javascript: URLs enter the proper mode --- 
in particular HTML, not XHTML one. Us doing otherwise caused tinyMCE to 
get confused (due to a paragraph being <p> and not <P>) and remove & clone text node
being typed on, which caused the editting code to get really confused and blow up.
This, in particular, affected GSoC editor page. Now typing in should be fine 
(though I would still be careful with richtext there)

I do think the mode detection code needs rework, and the editing code will 
need heavy work, but that's for an another time.

CCBUG:188445
CCBUG:141523


------------------------------------------------------------------------
r949036 | ossi | 2009-04-04 11:15:49 +0000 (Sat, 04 Apr 2009) | 4 lines

backport: remove stale code

just so ...

------------------------------------------------------------------------
r949054 | ossi | 2009-04-04 12:48:51 +0000 (Sat, 04 Apr 2009) | 8 lines

re-introduce KDE_EXTRA_FSYNC for the poor XFS/Ext4/... users

setting this env variable to anything (before kde starts!) will avoid
getting those pesky 0-byte sized files after system crashes - at a
potentially non-trivial performance cost.

BUG: 187172

------------------------------------------------------------------------
r950360 | orlovich | 2009-04-06 22:12:21 +0000 (Mon, 06 Apr 2009) | 4 lines

Add missing mapping for insert key
BUG:188977


------------------------------------------------------------------------
r950465 | ahartmetz | 2009-04-07 08:58:06 +0000 (Tue, 07 Apr 2009) | 10 lines

Backport:

  r949948 | ahartmetz | 2009-04-06 12:07:06 +0200 (Mo, 06. Apr 2009) | 5 lines

  Don't loop if we get no auth request (that we understand) together with a
  401/407 status code.                                                     
  Some whitespace cleanup.                                                 
  BUG:187753


------------------------------------------------------------------------
r950876 | rkcosta | 2009-04-07 23:18:25 +0000 (Tue, 07 Apr 2009) | 2 lines

Backport commit 950846 with apidox clarifications for result and finished.

------------------------------------------------------------------------
r950983 | dfaure | 2009-04-08 10:54:36 +0000 (Wed, 08 Apr 2009) | 2 lines

Merging 950830, 950837 and 950977: Fix the call to KJob::kill when the Cancel button is pressed - KJob::EmitResult must be passed so that ReadOnlyPart can handle the termination correctly. Bugs 186722, 186980, 187538. Patch by Raphael Kubo da Costa.

------------------------------------------------------------------------
r951041 | dfaure | 2009-04-08 13:57:11 +0000 (Wed, 08 Apr 2009) | 6 lines

Backport 950875:
Fix two porting bugs (!)
 - no null termination needed on QByteArrays anymore
 - tempfilename must be retrieved before closing the tempfile
This makes server-push (e.g. webcams) work again.

------------------------------------------------------------------------
r951134 | dfaure | 2009-04-08 14:54:54 +0000 (Wed, 08 Apr 2009) | 4 lines

backport: Fix updating of images from the airport-nuernberg webcam. This url works in konq now:
http://www.airport-nuernberg.de/_/tools/webcam.html?_FRAME=64&refresh=2&datei=webcam-0-0.jpg&bildpaket=1&oldcam=oldcam&pid=12541
CCBUG: 123614

------------------------------------------------------------------------
r951145 | orlovich | 2009-04-08 15:01:52 +0000 (Wed, 08 Apr 2009) | 2 lines

Not needed anymore... 

------------------------------------------------------------------------
r951214 | orlovich | 2009-04-08 17:33:53 +0000 (Wed, 08 Apr 2009) | 4 lines

- Make it build with Qt4.4 again..
- Don't show the clear button in tr, we don't 
need any more spurious warnings...

------------------------------------------------------------------------
r951343 | orlovich | 2009-04-08 23:48:02 +0000 (Wed, 08 Apr 2009) | 7 lines

Make sure to clear next here, so we look for 
the next next on the next iteration properly. Fixes 
infinite loop in bidiNext on some websites

BUG:188830
BUG:189161

------------------------------------------------------------------------
r951351 | orlovich | 2009-04-09 00:39:25 +0000 (Thu, 09 Apr 2009) | 7 lines

Backport vtokarev's r949425

Set continuation to 0 when we actually removed it


BUG:189121

------------------------------------------------------------------------
r951367 | aseigo | 2009-04-09 04:21:08 +0000 (Thu, 09 Apr 2009) | 2 lines

revert backport

------------------------------------------------------------------------
r951532 | ahartmetz | 2009-04-09 15:06:54 +0000 (Thu, 09 Apr 2009) | 3 lines

Backport of revision 951352 from trunk (mostly sorting fixes)
CCBUG:186277

------------------------------------------------------------------------
r951666 | aacid | 2009-04-09 20:58:19 +0000 (Thu, 09 Apr 2009) | 5 lines

Backport r951665 | aacid | 2009-04-09 22:56:37 +0200 (Thu, 09 Apr 2009) | 3 lines

leak--
CID: 5789

------------------------------------------------------------------------
r951697 | aacid | 2009-04-09 21:40:08 +0000 (Thu, 09 Apr 2009) | 3 lines

Backport r951694
set m_server to NULL after deleting it, Coverity is reporting a double delete and it seems plausible to me

------------------------------------------------------------------------
r952346 | cfeck | 2009-04-11 15:14:48 +0000 (Sat, 11 Apr 2009) | 5 lines

Fix widget item delegates to respect the focused widget (backport r952343)

CCBUG: 186142


------------------------------------------------------------------------
r952798 | cfeck | 2009-04-12 15:12:50 +0000 (Sun, 12 Apr 2009) | 4 lines

Fix crash in KDatePicker selectYearClicked() (backport r952794)

CCBUG: 182325

------------------------------------------------------------------------
r953123 | zwabel | 2009-04-13 12:34:24 +0000 (Mon, 13 Apr 2009) | 3 lines

Backport r953120:
Update the height of the completion-list when switching between a partially expanded and a not partially expanded item

------------------------------------------------------------------------
r953247 | zwabel | 2009-04-13 14:02:51 +0000 (Mon, 13 Apr 2009) | 4 lines

Backport r953244:
- Only trigger view updates from range changes if the range actually affects the visible area
- Make sure updates are triggered only exactly as often as needed, by checking in the foreground thread whether the smart-ranges are still dirty

------------------------------------------------------------------------
r953307 | rjarosz | 2009-04-13 17:43:25 +0000 (Mon, 13 Apr 2009) | 4 lines

Backport commit 953306.
Use setFilterFixedString instead of setFilterRegExp.


------------------------------------------------------------------------
r953495 | scripty | 2009-04-14 07:25:40 +0000 (Tue, 14 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r954058 | cfeck | 2009-04-15 00:49:29 +0000 (Wed, 15 Apr 2009) | 4 lines

Fix annoying notification when canceling year popup (backport r954041)

This fixes a side effect of r952798.

------------------------------------------------------------------------
r954287 | mlaurent | 2009-04-15 16:42:07 +0000 (Wed, 15 Apr 2009) | 4 lines

Backport:
don't allow to undo/redo/cut/paste when lineedit is readonly
(bug found in kmail->property)

------------------------------------------------------------------------
r955809 | orlovich | 2009-04-18 15:26:28 +0000 (Sat, 18 Apr 2009) | 2 lines

More SVG GC... Further stuff will likely need to be trunk only, though...

------------------------------------------------------------------------
r955821 | orlovich | 2009-04-18 15:50:45 +0000 (Sat, 18 Apr 2009) | 3 lines

Don't crash when attempting attribute value normalization during removeAttribute
BUG: 188061

------------------------------------------------------------------------
r955832 | mlaurent | 2009-04-18 16:14:26 +0000 (Sat, 18 Apr 2009) | 3 lines

Backport:
fix potential mem leak

------------------------------------------------------------------------
r955904 | ossi | 2009-04-18 19:34:41 +0000 (Sat, 18 Apr 2009) | 4 lines

make kdeinit really not leak file descriptors to child processes

BUG: 180785

------------------------------------------------------------------------
r956037 | ossi | 2009-04-19 10:43:38 +0000 (Sun, 19 Apr 2009) | 6 lines

fix and simplify the child struct disposal

the previous patch exposed this problem ...

CCBUG: 180785

------------------------------------------------------------------------
r956226 | carewolf | 2009-04-19 15:38:39 +0000 (Sun, 19 Apr 2009) | 2 lines

virtual functions doesn't work unless the have the right types

------------------------------------------------------------------------
r956231 | carewolf | 2009-04-19 15:51:33 +0000 (Sun, 19 Apr 2009) | 2 lines

Reverting until more generally applied and tested

------------------------------------------------------------------------
r956274 | sedwards | 2009-04-19 18:12:39 +0000 (Sun, 19 Apr 2009) | 6 lines

Restore binary compatibility after commit 952798 . Protected Q_SLOT uncheckYearSelector() should not be removed.

CCBUG: 182325
CCMAIL: christoph@maxiom.de


------------------------------------------------------------------------
r956528 | scripty | 2009-04-20 07:40:52 +0000 (Mon, 20 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r956565 | segato | 2009-04-20 09:23:36 +0000 (Mon, 20 Apr 2009) | 3 lines

backport commit r956559
fix theme reloading 

------------------------------------------------------------------------
r956847 | bettio | 2009-04-20 19:11:50 +0000 (Mon, 20 Apr 2009) | 2 lines

Backported 956845: Added 2 missing update()s.

------------------------------------------------------------------------
r956969 | scripty | 2009-04-21 07:21:50 +0000 (Tue, 21 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r956998 | gateau | 2009-04-21 09:20:10 +0000 (Tue, 21 Apr 2009) | 6 lines

Do not let plasma popup appear over screensaver.

BUG: 179924


CCMAIL:aseigo@kde.org
------------------------------------------------------------------------
r957022 | dfaure | 2009-04-21 10:14:10 +0000 (Tue, 21 Apr 2009) | 2 lines

The X-KDE-Library was made available as the "Library" property, which was rather confusing. Make it also available as the X-KDE-Library property

------------------------------------------------------------------------
r957319 | dfaure | 2009-04-21 19:59:15 +0000 (Tue, 21 Apr 2009) | 3 lines

Fix crash when renaming a directory that is only in the cache and not held by any dirlister.
BUG: 188807

------------------------------------------------------------------------
r957379 | dfaure | 2009-04-21 21:56:32 +0000 (Tue, 21 Apr 2009) | 7 lines

Backport r907250 by sstjames:
"Make proper use of inotify's implicit "WatchFiles" ability.  Immediate effect: Dolphin etc should now report changes to file attributes, rather than just creations and deletions, when KDirWatch uses inotify as its backend.  Secondary effect: Assuming no bugs, we should consume at most the same number of inotify watches as before (and often much less)."
CCBUG: 154676

As a third effect, it fixes kdirlistertest being stuck in KDirListerTest::testRenameAndOverwrite() because
kdirwatch wasn't informing kdirlister of the newly recreated file.

------------------------------------------------------------------------
r957388 | dfaure | 2009-04-21 22:05:27 +0000 (Tue, 21 Apr 2009) | 5 lines

Prefer most-local-urls in the drag object, rather than kde- (and user-!) specific urls.
Fixes copying files from folderview to a filemanager running as root.
This fix will be in 4.2.3.
BUG: 184403

------------------------------------------------------------------------
r957714 | aacid | 2009-04-22 18:59:13 +0000 (Wed, 22 Apr 2009) | 3 lines

Backport r957574 and remove my hack since it's not needed with it
Acked by cullmann and dhaumann

------------------------------------------------------------------------
r957816 | jacopods | 2009-04-22 22:58:40 +0000 (Wed, 22 Apr 2009) | 3 lines

Backport the isValid() method along with 957813 and 957814
CCBUG: 188940

------------------------------------------------------------------------
r957857 | jacopods | 2009-04-23 02:14:43 +0000 (Thu, 23 Apr 2009) | 2 lines

Style corrected

------------------------------------------------------------------------
r958004 | zwabel | 2009-04-23 09:26:09 +0000 (Thu, 23 Apr 2009) | 3 lines

Backport r957998:
Do not start code-completion after the editor lost focus

------------------------------------------------------------------------
r958007 | zwabel | 2009-04-23 09:33:34 +0000 (Thu, 23 Apr 2009) | 1 line

Backport r958006: Use the full screen space at bottom for the completion widget
------------------------------------------------------------------------
r958045 | rdale | 2009-04-23 10:56:21 +0000 (Thu, 23 Apr 2009) | 3 lines

* Allow scripting PopupApplets to be created if the metadata.desktop for the
applet has a 'ServiceTypes=Plasma/PopupApplet' line

------------------------------------------------------------------------
r958236 | orlovich | 2009-04-23 15:37:45 +0000 (Thu, 23 Apr 2009) | 4 lines

Make sure we don't transfer focus on a cancelled mousedown
BUG:190255
BUG:190142

------------------------------------------------------------------------
r958312 | dfaure | 2009-04-23 19:05:16 +0000 (Thu, 23 Apr 2009) | 6 lines

Better fix for 184403 (copying from desktop:/ to a filemanager running as root), which doesn't break moving icons in folderview:
- revert to exporting both kinds of urls in kdirmodel.
- at decoding time, add an option for which kind of url is preferred.
This allows the code that is actually going to call KIO (i.e. KonqOperations, next commit), to use local urls, but not other code.
CCBUG: 184403

------------------------------------------------------------------------
r958920 | orlovich | 2009-04-25 01:13:17 +0000 (Sat, 25 Apr 2009) | 5 lines

Parse doctypes here, too... Fixes 14 failures on newer domts version.

Really, XML tokenizer cleanup may be a nice project for someone who knows a lot about XML 
and wants to learn KHTML stuff..

------------------------------------------------------------------------
r958981 | pfeiffer | 2009-04-25 11:00:14 +0000 (Sat, 25 Apr 2009) | 2 lines

always return KFileMetaInfo object, not only when previews are enabled

------------------------------------------------------------------------
r959136 | orlovich | 2009-04-25 16:41:53 +0000 (Sat, 25 Apr 2009) | 20 lines

Go ahead and remove restrictions on what elements can be altered
with innerHTML, to match Mozilla's embrace-and-extend of this IE 
extension. We do not take advantage of the context yet, but I am not 
sure we have to do context-aware reconstruction here anyway;
and the weather is way too nice for me to try to make sense of HTML5's
spaghetti code. 

(And whitespace issue with this are a separate report)

Makes google suggest on google.com front page work --- thanks to 
Ariya for mentionning it exists.

Makes the 'other systems' download wizard for flash player on adobe.com 
work --- thanks to 'bokey' on IRC for bringing it up...

Makes stuff show up on mail-archives.apache.org

BUG:168479
BUG:137408

------------------------------------------------------------------------
r959157 | orlovich | 2009-04-25 17:00:47 +0000 (Sat, 25 Apr 2009) | 3 lines

Fix parsing of <meta charset="utf-8">, e.g. on http://www.google.com/intl/en/privacy.html 
(yey for sloppy keypresses finding bugs!)

------------------------------------------------------------------------
r959163 | mrybczyn | 2009-04-25 17:11:03 +0000 (Sat, 25 Apr 2009) | 2 lines

Added entry for Marble

------------------------------------------------------------------------
r959187 | mrybczyn | 2009-04-25 17:29:53 +0000 (Sat, 25 Apr 2009) | 2 lines

Adding underLGPL

------------------------------------------------------------------------
r959204 | orlovich | 2009-04-25 17:48:09 +0000 (Sat, 25 Apr 2009) | 10 lines

Don't walk object literals for declarations; they can't possibly 
be there, and we may run out of stack space doing this.

I think I'll probably switch over how we do declarations at some point, though 
--- since we now track the active function context in the grammar, we should 
be able to keep track of declarations at the time, avoiding extra AST walks.


CCBUG:190609

------------------------------------------------------------------------
r959239 | orlovich | 2009-04-25 19:17:02 +0000 (Sat, 25 Apr 2009) | 3 lines

Expand out combobox popups to fix content.
Fixes them being too narrow on travelocity.ca, as reported by Chani

------------------------------------------------------------------------
r959312 | cfeck | 2009-04-25 23:45:13 +0000 (Sat, 25 Apr 2009) | 4 lines

Do not insert random widget into the widgetInIndex hash (backport r959305)

CCBUG: 190024

------------------------------------------------------------------------
r959326 | orlovich | 2009-04-26 01:37:53 +0000 (Sun, 26 Apr 2009) | 4 lines

Apply patch by Janusz Lewandowski (lew21st at host gmail tld com) which implements 
two ES5/JS1.8 Array functions. Thanks!


------------------------------------------------------------------------
r959567 | pfeiffer | 2009-04-26 15:00:01 +0000 (Sun, 26 Apr 2009) | 3 lines

fix broken completion (completeListDirty must be set to true whenever the list
of items changes)

------------------------------------------------------------------------
r959668 | mrybczyn | 2009-04-26 20:13:25 +0000 (Sun, 26 Apr 2009) | 2 lines

Adding some entities from 3.5

------------------------------------------------------------------------
r959681 | dfaure | 2009-04-26 20:34:18 +0000 (Sun, 26 Apr 2009) | 2 lines

backport r959680: my wife's kword-kde3 registers to DCOP, not to DBUS, so don't migrate that key.

------------------------------------------------------------------------
r959691 | mpyne | 2009-04-26 20:37:35 +0000 (Sun, 26 Apr 2009) | 4 lines

Backport revert of auto-default button change to KDE 4.2.

CCBUG:190631

------------------------------------------------------------------------
r959694 | mrybczyn | 2009-04-26 20:44:34 +0000 (Sun, 26 Apr 2009) | 2 lines

Evn more entities from 3.5 plus some additions

------------------------------------------------------------------------
r959712 | orlovich | 2009-04-26 21:28:06 +0000 (Sun, 26 Apr 2009) | 8 lines

Cache and coalesce StringImp's for sufficiently short string literals.
This cuts heap usage by about 48% on vandenoever's heap demo; 
also seems to be about ~2% speedup on SunSpider --- perhaps, it's noisy;
but with a clear 1.11x or so on string-unpack-code which is fairly 
realistic piece of code, too 

CCBUG:190609

------------------------------------------------------------------------
r959713 | orlovich | 2009-04-26 21:29:00 +0000 (Sun, 26 Apr 2009) | 2 lines

Need to commit this too... 

------------------------------------------------------------------------
r959737 | cfeck | 2009-04-27 00:27:52 +0000 (Mon, 27 Apr 2009) | 6 lines

Fix broken menu layout with large menu fonts (backport r959736)

Will be in 4.2.3

CCBUG: 163713

------------------------------------------------------------------------
r959803 | cfeck | 2009-04-27 09:02:26 +0000 (Mon, 27 Apr 2009) | 6 lines

Fix PageView list delegate for selected items with QMotifStyle (backport r959802)

Will be in KDE 4.2.3

CCBUG: 164684

------------------------------------------------------------------------
r960538 | kbal | 2009-04-28 15:43:14 +0000 (Tue, 28 Apr 2009) | 1 line

backport of addition
------------------------------------------------------------------------
r960726 | pfeiffer | 2009-04-28 22:25:26 +0000 (Tue, 28 Apr 2009) | 3 lines

don't calculate previews when the preview was disabled by the user
(after being enabled once before)

------------------------------------------------------------------------
r960766 | kkofler | 2009-04-29 02:14:00 +0000 (Wed, 29 Apr 2009) | 4 lines

Backport revision 931194 by dfaure from trunk (fixes 1 of 2 kdelibs build issues with g++ 4.4 + glibc 2.9):

constify the retval, to fix compilation with gcc-4.4 (says spstarr). I checked and it still compiles
(e.g. kdegraphics/kgamma/kcmkgamma/xvidextwrap.cpp) with 4.3, the char* retval is constified.
------------------------------------------------------------------------
r960767 | kkofler | 2009-04-29 02:16:37 +0000 (Wed, 29 Apr 2009) | 7 lines

Fix crash when built with gcc 4.4 without -fno-strict-aliasing

Details included in the bug report.

CCBUG:189809

(backport revision 960321 by bero from trunk)
------------------------------------------------------------------------
r960777 | jacopods | 2009-04-29 04:02:44 +0000 (Wed, 29 Apr 2009) | 8 lines

Constify the isValid() method (backported around one week ago) 
Will fix in trunk on monday, but I have to do this before tagging (tomorrow) here in branch.  

Thanks to André for noticing that.

CCMAIL: Woebbeking@kde.org
CCMAIL: kde-core-devel@kde.org

------------------------------------------------------------------------
r961165 | orlovich | 2009-04-29 15:24:02 +0000 (Wed, 29 Apr 2009) | 9 lines

Fix a crash that can happen when closing last document part (which is much easier to trigger in 
akregator than konqueror) --- we need to mark the interned tables even if there are no 
interpreters alive, as interpreter destruction may preceed some GCs, including one 
that finally gets to the function body that's keeping the interned literal alive.

Thanks to Andras for report, and getting the VG data needed to fix it.

BUG:190966

------------------------------------------------------------------------
r961205 | orlovich | 2009-04-29 16:58:58 +0000 (Wed, 29 Apr 2009) | 3 lines

automatically merged revision 961195:
We already have preparsed class attribute for every DOM element (for
faster css selectors) no reason to do it here all over again
------------------------------------------------------------------------
r961328 | pfeiffer | 2009-04-29 22:23:50 +0000 (Wed, 29 Apr 2009) | 4 lines

- don't store instance-specific data in a static variable (it would get
  shared among all instances)
- avoid creating a view in setMode(); only if one is already available, update it

------------------------------------------------------------------------
r961357 | ggarand | 2009-04-30 00:13:18 +0000 (Thu, 30 Apr 2009) | 14 lines

automatically merged revision 961352:
rework form widgets padding for increased compatibility, fixing several
bugs in the process:

.change default box-sizing value for many widgets, depending
 on parse mode
.stop moving the padding from the inside of widgets to the outside
 when box-sizing changes. This doesn't match any engine behaviour
 and is couter-intuitive - this is now hard-coded for each widget type.
.fix calculation of box-sizing. We have to expose the internal
 padding during layout, so that padding is properly accounted for
 in width/height computation.
.fix intrinsic size of textarea/select with padding. Was botched by
 Qt's strange frameWidth behaviour.
------------------------------------------------------------------------
r961358 | ggarand | 2009-04-30 00:14:23 +0000 (Thu, 30 Apr 2009) | 5 lines

automatically merged revision 961353:
remove this $$ test, this is the initial context, and Bison won't
initialise it to zero ; so this is pointless and bogus.

diagnosed by Maksim
------------------------------------------------------------------------
r961360 | ggarand | 2009-04-30 00:15:25 +0000 (Thu, 30 Apr 2009) | 4 lines

automatically merged revision 961354:
enable the checking of @charset, that was never enabled for ill-know
reasons.
The check is really simplistic but will do for know.
------------------------------------------------------------------------
r961362 | ggarand | 2009-04-30 00:16:32 +0000 (Thu, 30 Apr 2009) | 5 lines

automatically merged revision 961356:
.fix crashes happening with the "/" gui less find-as-you-type
.harden the findbar code to prevent possible access to deleted bar

BUG: 189201
------------------------------------------------------------------------
r961373 | ggarand | 2009-04-30 02:01:23 +0000 (Thu, 30 Apr 2009) | 3 lines

compile with Qt 4.4 even..
kindly tesed by Maksim

------------------------------------------------------------------------
r961499 | scripty | 2009-04-30 08:02:13 +0000 (Thu, 30 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
