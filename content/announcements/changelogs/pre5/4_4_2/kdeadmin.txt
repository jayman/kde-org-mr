------------------------------------------------------------------------
r1098395 | lueck | 2010-03-04 05:52:51 +1300 (Thu, 04 Mar 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1098631 | cgiboudeaux | 2010-03-04 13:43:18 +1300 (Thu, 04 Mar 2010) | 4 lines

backport r1098628 from trunk to 4.4:
Explicitly link to KDE4_KIO_LIBS or the tests compilation will fail when using the gold linker.


------------------------------------------------------------------------
r1098670 | scripty | 2010-03-04 15:17:26 +1300 (Thu, 04 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1098973 | lbeltrame | 2010-03-05 09:35:27 +1300 (Fri, 05 Mar 2010) | 2 lines

Fix printer KCM throwing an AttributeError when trying to access it. Reported to me by Andrea Scarpino, patch by Augusto Erdosain. Thanks!

------------------------------------------------------------------------
r1099872 | lbeltrame | 2010-03-06 21:34:02 +1300 (Sat, 06 Mar 2010) | 4 lines

Fix reinit() by removing the reference to combobox_dict. I had already changed a bit but that one slipped through...

CCBUG: 219751

------------------------------------------------------------------------
