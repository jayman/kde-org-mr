2007-05-21 14:37 +0000 [r667004]  mkretz

	* branches/arts/1.5/arts/mcop/notification.cc,
	  branches/arts/1.5/arts/mcop/notification.h,
	  branches/arts/1.5/arts/qtmcop/qiomanager.cc,
	  branches/arts/1.5/arts/mcop/iomanager.cc: stop polling
	  NotificationManager for now notifications, instead handle
	  notifications on demand. This gets rid of the 50ms timer that was
	  burning energy unnecessarily. This change introduces a special
	  meaning to IOManager::addTimer(-1, 0) in order to get a new
	  virtual function without breaking BC. Additionally a previously
	  inlined function was moved to the .cpp file. The only place where
	  this breaks (that I know of) is in
	  kdemultimedia/arts/midi/audiotimer.cc. It only needs a recompile
	  of kdemultimedia to fix it, though.

2007-09-16 00:37 +0000 [r713015]  jriddell

	* branches/arts/1.5/arts/flow/audioiosgi.cc,
	  branches/arts/1.5/arts/mcop/md5auth.cc,
	  branches/arts/1.5/arts/mcop/unixserver.cc,
	  branches/arts/1.5/arts/flow/cpuinfo.cc,
	  branches/arts/1.5/arts/soundserver/artsplay.cc,
	  branches/arts/1.5/arts/soundserver/crashhandler.h,
	  branches/arts/1.5/arts/soundserver/artsshell.cc,
	  branches/arts/1.5/arts/soundserver/artscat.cc,
	  branches/arts/1.5/arts/flow/audiosubsys.cc,
	  branches/arts/1.5/arts/soundserver/artsrec.cc,
	  branches/arts/1.5/arts/mcop/dispatcher.cc,
	  branches/arts/1.5/arts/flow/audioiooss.cc,
	  branches/arts/1.5/arts/flow/audioiojack.cc,
	  branches/arts/1.5/arts/mcop/startupmanager.cc,
	  branches/arts/1.5/arts/flow/audioionull.cc,
	  branches/arts/1.5/arts/flow/audioioesd.cc,
	  branches/arts/1.5/arts/flow/audioiolibaudioio.cc,
	  branches/arts/1.5/arts/mcop/extensionloader.cc,
	  branches/arts/1.5/arts/flow/audioioaix.cc,
	  branches/arts/1.5/arts/flow/audioiocsl.cc,
	  branches/arts/1.5/arts/flow/audioionas.cc,
	  branches/arts/1.5/arts/soundserver/fileinputstream_impl.cc,
	  branches/arts/1.5/arts/mcop/tcpserver.cc,
	  branches/arts/1.5/arts/mcop/mcoputils.cc,
	  branches/arts/1.5/arts/flow/audioiosun.cc,
	  branches/arts/1.5/arts/flow/audioioossthreaded.cc,
	  branches/arts/1.5/arts/flow/convert.cc,
	  branches/arts/1.5/arts/soundserver/soundserverv2_impl.cc,
	  branches/arts/1.5/arts/artsc/artsdsp.c,
	  branches/arts/1.5/arts/flow/audioioalsa9.cc,
	  branches/arts/1.5/arts/mcop/objectmanager.cc,
	  branches/arts/1.5/arts/artsc/artscbackend.cc,
	  branches/arts/1.5/arts/mcop/iomanager.cc,
	  branches/arts/1.5/arts/flow/audioioalsa.cc,
	  branches/arts/1.5/arts/flow/audioiomas.cc: fixes for gcc 4.3

2007-09-28 12:22 +0000 [r718295]  jriddell

	* branches/arts/1.5/arts/mcop/debug.cc: sync typedefs to
	  glibconfig.h to fix compile with --enable-final

2007-10-04 12:25 +0000 [r721048]  jriddell

	* branches/arts/1.5/arts/flow/audioiosgi.cc,
	  branches/arts/1.5/arts/mcop/md5auth.cc,
	  branches/arts/1.5/arts/mcop/unixserver.cc,
	  branches/arts/1.5/arts/flow/cpuinfo.cc,
	  branches/arts/1.5/arts/soundserver/artsplay.cc,
	  branches/arts/1.5/arts/soundserver/crashhandler.h,
	  branches/arts/1.5/arts/soundserver/artscat.cc,
	  branches/arts/1.5/arts/soundserver/artsshell.cc,
	  branches/arts/1.5/arts/flow/audiosubsys.cc,
	  branches/arts/1.5/arts/soundserver/artsrec.cc,
	  branches/arts/1.5/arts/mcop/debug.cc,
	  branches/arts/1.5/arts/mcop/dispatcher.cc,
	  branches/arts/1.5/arts/flow/audioiooss.cc,
	  branches/arts/1.5/arts/flow/audioiojack.cc,
	  branches/arts/1.5/arts/mcop/startupmanager.cc,
	  branches/arts/1.5/arts/flow/audioionull.cc,
	  branches/arts/1.5/arts/flow/audioioesd.cc,
	  branches/arts/1.5/arts/flow/audioiolibaudioio.cc,
	  branches/arts/1.5/arts/mcop/extensionloader.cc,
	  branches/arts/1.5/arts/flow/audioioaix.cc,
	  branches/arts/1.5/arts/flow/audioiocsl.cc,
	  branches/arts/1.5/arts/flow/audioionas.cc,
	  branches/arts/1.5/arts/soundserver/fileinputstream_impl.cc,
	  branches/arts/1.5/arts/mcop/tcpserver.cc,
	  branches/arts/1.5/arts/mcop/mcoputils.cc,
	  branches/arts/1.5/arts/flow/audioiosun.cc,
	  branches/arts/1.5/arts/flow/audioioossthreaded.cc,
	  branches/arts/1.5/arts/flow/convert.cc,
	  branches/arts/1.5/arts/soundserver/soundserverv2_impl.cc,
	  branches/arts/1.5/arts/artsc/artsdsp.c,
	  branches/arts/1.5/arts/flow/audioioalsa9.cc,
	  branches/arts/1.5/arts/mcop/objectmanager.cc,
	  branches/arts/1.5/arts/artsc/artscbackend.cc,
	  branches/arts/1.5/arts/mcop/iomanager.cc,
	  branches/arts/1.5/arts/flow/audioioalsa.cc,
	  branches/arts/1.5/arts/flow/audioiomas.cc: revert my recent
	  changes, they cause build errors due to duplicated typedefs from
	  glibconfig.h (depending on mood of automake and 64/32bit system)
	  and I don't know how to fix it.

2007-10-08 11:05 +0000 [r722976-722957]  coolo

	* branches/arts/1.5/arts/arts.lsm: updating lsm

	* branches/arts/1.5/arts/configure.in.in: update for tag

