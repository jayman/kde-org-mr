2006-08-01 11:04 +0000 [r568488]  kling

	* branches/arts/1.5/arts/examples/x11commtest.cc,
	  branches/arts/1.5/arts/tests/testanyref.cc,
	  branches/arts/1.5/arts/examples/helloserver.cc,
	  branches/arts/1.5/arts/examples/referenceinfo.cc,
	  branches/arts/1.5/arts/tests/testifacerepo.cc: Unbreak the test
	  suite (header dependency problems.) BUG: 82208

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-09-03 16:21 +0000 [r580475]  kling

	* branches/arts/1.5/arts/flow/audioioalsa.cc: Default to big-endian
	  format on big-endian machines.. CCBUG: 132924

2006-10-02 11:07 +0000 [r591322]  coolo

	* branches/arts/1.5/arts/arts.lsm: updates for 3.5.5

2006-10-02 11:11 +0000 [r591340]  coolo

	* branches/arts/1.5/arts/configure.in.in: updates for 3.5.5

