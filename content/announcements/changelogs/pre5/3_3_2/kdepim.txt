Dir: kdepim
----------------------------
new version numbers
----------------------------
Backport: Don't use dcop to call a library method



Dir: kdepim/certmanager
----------------------------
From HEAD:
- Speed up CRL cache dumping by an insane 4-digit percentage by
  o using QTextEdit with LogText format and
  o only updating the output window every second
- kill the process when closing the window, so it doesn't continue to suck CPU
----------------------------
From HEAD: Use a fixed font for the gpgsm dump output window.
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)
----------------------------
Merge from HEAD (2/3): Code cleanups and bugfixes.



Dir: kdepim/certmanager/conf
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)



Dir: kdepim/certmanager/kwatchgnupg
----------------------------
From HEAD: Werner Koch says use utf-8, so we do.
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)



Dir: kdepim/certmanager/lib
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)
----------------------------
fix compile (gcc 4.0)
----------------------------
Bugfix: make sure the information about wrong key usage is passed through in a secure way.



Dir: kdepim/certmanager/lib/backends
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)



Dir: kdepim/certmanager/lib/backends/kpgp
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)



Dir: kdepim/certmanager/lib/backends/qgpgme
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)
----------------------------
Merge from HEAD (2/3): Code cleanups and bugfixes.



Dir: kdepim/certmanager/lib/kleo
----------------------------
fix compilation
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)
----------------------------
fix compile (gcc 4.0)
----------------------------
Merge from HEAD (2/3): Code cleanups and bugfixes.



Dir: kdepim/certmanager/lib/pics
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)



Dir: kdepim/certmanager/lib/tests
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)
----------------------------
Merge from HEAD (2/3): Code cleanups and bugfixes.



Dir: kdepim/certmanager/lib/ui
----------------------------
Merge from HEAD (1/3): reduce diff (no code changes)
----------------------------
Speed up selectKeys() from O(NM) to O(MlogN+N) (M=#selectedKeys, N=#keys) by using itemMap once more. And simplify the code _a lot_ :)
----------------------------
Backport "Fix mem leak" and "Update button when we add item"
----------------------------
Merge from HEAD (2/3): Code cleanups and bugfixes.



Dir: kdepim/debian
----------------------------
Commit kdepim 4:3.3.1-1 packaging.



Dir: kdepim/debian/patches
----------------------------
Commit kdepim 4:3.3.1-1 packaging.



Dir: kdepim/kaddressbook
----------------------------
Ask the user once before importing contacts from the ldap search dialog for
the target resource, and not for each contact. Lock and unlock the resource
properly. Ok'd by Tobias.
----------------------------
Fix dnd of a contact onto an addressee-lineedit:
 - kaddressbook should put the QTextDrag before the KURLDrag (which offers plain text too)
 - addressee-lineedit should only care about the KURLDrag if the protocol is mailto:
----------------------------
increased version
----------------------------
Backport from HEAD: Fix modal dialogs blocking other parts of kontact



Dir: kdepim/kaddressbook/editors
----------------------------
fix compile (gcc 4.0)



Dir: kdepim/kaddressbook/features
----------------------------
branches:  1.25.2.1.2;
Fixed "wrong item selected in combo when renaming a distr list"



Dir: kdepim/kalarm
----------------------------
Fix compile error
----------------------------
Ignore reminders when showing next alarm time in alarm list and system tray tooltip
----------------------------
Fix time-to-alarm column in main window not always updating every minute
----------------------------
Fix KAlarm button on message windows to makeit always display main window



Dir: kdepim/karm
----------------------------
Backport, don't show the window on login if it was hidden at logout.
----------------------------
fix compile (gcc 4.0)



Dir: kdepim/kdgantt
----------------------------
compile
----------------------------
fix compile (gcc 4.0)



Dir: kdepim/kfile-plugins/vcf
----------------------------
fix encoding



Dir: kdepim/kioslaves/sieve
----------------------------
Backport fix on 64_x86



Dir: kdepim/kmail
----------------------------
Update assert.
----------------------------
(backport)
There's a bug where KTipDialog appears, but is empty, on kontact start.  This is because it's not Kontact showing the tips, with a list of all the components' tips files, but KMail, which likes to show its tips when constructing its KMainWidget. Since KMail is running in Kontact, its appname is kontact, and kontact/tips does not exist -> empty tips dialog.

This moves KMail's startup tips to KMMainWin, which is only used when kmail is running standalone.
----------------------------
Better fix for "aborting during the initial connection phase says 'Aborting...' and never moves on".
The previous fix broke handling of "bad password".
----------------------------
Backport: Notice a manual change of the external editor command line. Patch by Francois Kritzinger.
----------------------------
Correctly end new-mail-check when a folder reports an error.
BUG: 92416
----------------------------
Thanks to WildFox who noted that I forgot to adjust the api docs
----------------------------
Backport from HEAD: Fix modal dialogs blocking other parts of kontact
----------------------------
readTemporaryMsg speedup fix for proko2 - 3.3-branch code not affected
----------------------------
make the bogoilter criteria working for newer versions
----------------------------
reverting my commit: should not go into 3.3 branch
----------------------------
Backport speed and crash fix from HEAD, since reactions there were
positive.
----------------------------
Backport of CVS commit by kloecker:

Fix PGP/MIME encrypting messages which are BCC'ed. The problem was that KMMessagePart contains a QByteArray which is explicitely shared. Therefore simply copying a KMMessagePart with the copy c'tor for getting a temporary copy isn't possible. Implementing and using a duplicate method fixes the problem.
BUG: 92412
----------------------------
Save 4 bytes per KMMsgBase.
----------------------------
Bugfix: Make suer we detect when a read message, which we just decrypted, was encrypted using a for-signing-only key. (aegypten issue #220
----------------------------
oops, I applied the wrong version of the patch - create dialog on stack.
----------------------------
Compile
----------------------------
i18nify two strings. (backport revision 1.72)
----------------------------
branches:  1.30.2.4.2;
Provide all reply methods and all forward methods in the separate reader window.
The previous toolbar button with a tooltip of just "Inline..." was confusing
(https://intevation.de/roundup/kolab/issue97)
----------------------------
Backpport: Fix searching when the header field in question is
the first one of a message.
----------------------------
Apply coolo's fix for my brown-paper-bag bug
----------------------------
Fixed "Folder Menu not updated after enabling Expiration for a Folder", approved by Ingo.
BUG: 92918
----------------------------
- config.h must be first
- rename IDS_HEADER, IDS_VERSION to IDS_SEARCH_* to avoid confusion with IDS_* from kmmsgdict.cpp
----------------------------
Revert incorrect fix for #[Bug 39537] attachments' temporary files are deleted too soon.
It used to work as long as you kept the reader window on that mail, but now it fails with
apps that go into the background (ark, kfmclient, korganizer).
Proper fix can only be done in HEAD.
----------------------------
Backport my contentsDragMoveEvent optimizations, revisions .cpp 1.329:1.334 + .h 1.87:1.88
Quite a lot changes, but it's mostly removing unnecessary stuff and should be safe.
----------------------------
Backport two fixes
----------------------------
fix compile (gcc 4.0)
----------------------------
From HEAD: Expunge did not tell the resources that something had changed. This is fixed now, and the slots have been put into one generic slot
----------------------------
Fix for "kstart --iconify --windowclass kmail kmail" as discussed with Seli.

The cases for which this code was added still work (typing kmail while kmail
is running activates it; kmail --check doesn't).
CCBUG: 73591
----------------------------
Let the expunged signal say where it came from. Required for my upcoming bugfix
----------------------------
Backport fix for missing filter actions after application switch in Kontact
----------------------------
Confusing - those methods are simply not used.
----------------------------
A faster method for retrieving a message without getMsg + unGetMsg (*)
It simply combines the code from getMsg and readMsg, without the mMsgList.set() call.

(*) Callgrind shows that unGetMsg takes time too, mostly due to strippedSubjectMD5(),
so let's avoid it altogether, as discussed with Till.
----------------------------
Backport: Don't use dcop to call a library method
----------------------------
Fix Aegypten issue #278: KMail looks up recipient key during autosave.
----------------------------
Add a serial number based DeleteMsgCommand which also works if the message
is opened in an external reader window.
----------------------------
Don't kill running mailchecks when cancelling the subscription dialog.
Approved by Carsten B. ("Subscription dialog cancelling" post on kmail-devel)
----------------------------
Backport: Make sure writeConfig is written after all changes.
----------------------------
Swap two lines and gain some performance when parsing big folders. Approved by Don.
----------------------------
Provide all reply methods and all forward methods in the separate reader window.
The previous toolbar button with a tooltip of just "Inline..." was confusing
(https://intevation.de/roundup/kolab/issue97)
----------------------------
Bugfix (aegypten #39): When pressing 'T' in the draft folder, to edit a previously saved mail before sending it, we now get preselected the Crypto Module which we had used when saving that mail.
----------------------------
Make KMail compile with --enable-final. Please port to HEAD, can't do it myself, but should be straightforward...
Patch as posted on kmail-devel
----------------------------
Fix broken order of folders in the folder combobox. Since we now allow subfolders below the system folders we can't simply move the system folders to the begin of the list. Therefore we now leave the system folders where they are. Additionally, the local folders are now listed before the IMAP folders which should have always been the case.
BUG:92710
----------------------------
Globally disablesigning and encryption of ical replies.
----------------------------
Put in a working email address
----------------------------
Backport fix mem leak
----------------------------
Bo correctly points out that if the folder is const, the storage should
be as well. Adjust.
----------------------------
increase version (sleepy maintainers, many bug reports, not in RELEASE-CHECKLIST)
----------------------------
Backport of:

CVS commit by tilladam:

When making a connection to an imap server fails, set an error condition
and clear it after a timeout, so all waiting folders are stopped, and a
new mailcheck can try again.

This is based on a patch by Waldo with most bits moved to online imap only
since they don't make sense for dimap and some debug changes of very little
consequence. Thanks, dude.



Dir: kdepim/kmail/interfaces
----------------------------
This code is interpreted differently depending on whether or not interfaces/htmlwriter.h was included before or after this file. That's bad, so fix it. Uncovered by my wrong fix of a --enable-final compile error.



Dir: kdepim/knode
----------------------------
Backport of #93312
clear searchline when changing group
----------------------------
Backport 41973:
- exclude ignored messages from unread count
CCBUG: 41973
----------------------------
backport from HEAD:
- scroll the reader win back after rot13 (#76797)
- dito for switching from/to fixed font
- insert spelling correction at correct position (#76396)
- formatting of 1-letter words (bold etc.) (#70726)
- disable formatting of signatures (#59448)
----------------------------
Backport 40266:
- don't close search dialog when it is minimised
CCBUG: 40266
----------------------------
increase version (sleepy maintainers, many bug reports, not in RELEASE-CHECKLIST)



Dir: kdepim/knotes
----------------------------
fixed #90744, #91702, #92596, #93499: save modified notes on quit, may
not have happened when the note still had focus. Also, the problem
with knotesnotes.ics should be gone now.

To #91702: I guess you were takling about modifying only one note, because
when changing to a second one the first one is saved immediately.

BUG: 90744, 91702, 92596, 93499

Will backport in a second.
----------------------------
Same here, time to update version.
----------------------------
Backported all the bugfixes from HEAD. Should make for an almost-bugfree
KNotes in KDE 3.3.x. Sorry for the lateness, have been sick :(

Coolo, is everything finished and done for 3.3.1 already or is there still
a chance to move the tag?

----------------------------
Put in a working email address



Dir: kdepim/kontact
----------------------------
try to extract all the messages



Dir: kdepim/kontact/interfaces
----------------------------
the weekly "unbreak compilation" commit



Dir: kdepim/kontact/plugins/kmail
----------------------------
Ingo's kmkernel commit makes this fix unnecessary again



Dir: kdepim/kontact/plugins/knotes
----------------------------
Backported Tobias' fix: make rename work better, i.e. no restart of Kontact
is needed anymore.
----------------------------
Fixed compilation



Dir: kdepim/kontact/src
----------------------------
Backport from HEAD: Fix modal dialogs blocking other parts of kontact
----------------------------
increase version (sleepy maintainers, many bug reports, not in RELEASE-CHECKLIST)



Dir: kdepim/korganizer
----------------------------
Fix compilation. I didn't notice that actionmanager.cpp was branched for proko2, and the cvs mail didn't tell that. Sorry
----------------------------
This would appear to be a very big change, but in fact it does absolutely nothing. It just removes an obscene amount of old kroupware code that is no longer used. There are no changes in this patch.
----------------------------
Backport of http://lists.kde.org/?l=kde-cvs&m=109509131620180&w=2
CVS commit by kainhofe:
Turn the Organizer of an incidence into a Person object (instead of using just a
QString). This has the advantage that it's now possible to have a name and the email
specified for the organizer.
----------------------------
Add the attendees to the listview in the order they appear in the ical.
Otherwise the first call to operator== fails, because it compares the
attendees in the wrong order.
----------------------------
Remove more old and unused code



Dir: kdepim/korganizer/Attic
----------------------------
Backport of http://lists.kde.org/?l=kde-cvs&m=109509131620180&w=2
CVS commit by kainhofe:
Turn the Organizer of an incidence into a Person object (instead of using just a
QString). This has the advantage that it's now possible to have a name and the email
specified for the organizer.



Dir: kdepim/korganizer
----------------------------
Prevent editing of read-only todos.
----------------------------
Use the old "paint into a pixmap and then bitblt" trick instead of doing
a complex painting operation on the live widget to completely remove the
flicker of the day matrix. Yeah!
----------------------------
Backport from HEAD: Fix modal dialogs blocking other parts of kontact
----------------------------
Backport of various bug fixes to the 3.3 branch. I can't remember the actual bug numbers...
----------------------------
Bugfix: The checkboxes for the weekdays were not shown, due to one of my stupid typos.
----------------------------
Add double-quotes around the name when reopening the dialog on a name with a comma in it,
needed since getNameAndMail removes it now. Code taken from addressee.cpp, maybe we need
a "make full email" function in email.cpp...
----------------------------
 Keep track of the uid of the last selected incidence and re-select that
 after clear or reload in fillAgenda(). This avoids relying on the incidence
 pointer kept in the mSelectedItem of the agenda, since that might already
 be invalid. Ok'd by Cornelius.
----------------------------
Backport of http://lists.kde.org/?l=kde-cvs&m=109509131620180&w=2
CVS commit by kainhofe:
Turn the Organizer of an incidence into a Person object (instead of using just a
QString). This has the advantage that it's now possible to have a name and the email
specified for the organizer.
----------------------------
From HEAD: fullEmails()
----------------------------
Backport: Don't use dcop to call a library method
----------------------------
This fixes multiple problems with the freebusy code:

- Add a person to the attendee list and don't touch him. Then the old code
  would never download the FB list. Now, it immediately tries to download
  the list, if the email address provided makes sense
- While editing the attendee name/email lineedit, for every single
  keystroke, an FB download job was fired. Now, a timer has been
  introduced so it's not tried before 5 seconds after the last change
- Related to this: If a download job is already running for an attendee,
  then it will no longer start a new one
----------------------------
the weekly "unbreak compilation" commit
----------------------------
Second half of the 541 fix: encode mailto URL correctly
----------------------------
Merge shome fixes from HEAD.
	* when the notes are loaded from imap resource, those have to be registrated
	on the manager. Now, knotes show the list of notes that are in the imap resource.
	* Fix due date wrong after moving todo in KOAgendaView
	* Don't add a todo when we press enter on the QuickLineEdit
	* Use the internal value to report the date, not use the text for this.
	When the text is changed, we updated the internal value.
----------------------------
Ported the proko2 changes of my last commit
----------------------------
Remove deleted items from the internal data structures.
----------------------------
Backport from proko2: Don't reset the rsvp flag
----------------------------
Load the message catalogues that main.cpp loads - for the case where we are
embedded into kontact.
https://intevation.de/roundup/kolab/issue535
----------------------------
Put in a working email address



Dir: kdepim/korganizer/Attic
----------------------------
Backport of http://lists.kde.org/?l=kde-cvs&m=109509131620180&w=2
CVS commit by kainhofe:
Turn the Organizer of an incidence into a Person object (instead of using just a
QString). This has the advantage that it's now possible to have a name and the email
specified for the organizer.



Dir: kdepim/korganizer
----------------------------
Fixed the two bugs mentionned in https://intevation.de/roundup/kolab/issue117.
koeventviewer: use name() instead of fullName() for the organizer. Where is this code in HEAD???
urihandler: there was a typo in the DCOP method name (the spaces were wrong). Better use DCOPRef anyway.
----------------------------
Make code a bit less confusing (ctor arg and then setText with a different value)
----------------------------
increase version (sleepy maintainers, many bug reports, not in RELEASE-CHECKLIST)



Dir: kdepim/korganizer/plugins/holidays
----------------------------
Backport of bug fixes for #84979 (crash on AMD64) and #89539 (typos in the New Zealand holidays file)



Dir: kdepim/korganizer/plugins/holidays/holidays/Attic
----------------------------
Backport of new Japanese Holidays file by Toyokiro Asukai

CBUG: 84987
----------------------------
Backport of bug fixes for #84979 (crash on AMD64) and #89539 (typos in the New Zealand holidays file)
----------------------------
Backport of fix in HEAD:
Sweden changes to winter time on the last sunday of october, too. This has been the case since 1996, as Josef Huber notices in Bug report 90496.



Dir: kdepim/korn
----------------------------
fix compile (gcc 4.0)



Dir: kdepim/kpilot/kpilot
----------------------------
Remove all traces of kroupware stuff. It was never supposed to go here in the first place, yet remained here for 1½ years
----------------------------
Backport fix for data loss on restore



Dir: kdepim/kpilot/lib
----------------------------
Backport fix for category duplication
----------------------------
increased version



Dir: kdepim/kresources/egroupware
----------------------------
Adjust to lib splitting in knotes.
----------------------------
fix compile (gcc 4.0)
----------------------------
the weekly "unbreak compilation" commit



Dir: kdepim/kresources/exchange
----------------------------
Backport to the 3.3 Branch of fix for Bug 88005 (times were sent to the server without being shifted to UTC. This was caused by the tzid not being correctly propagated to the uploader)

CCBUG: 88005



Dir: kdepim/kresources/imap/kcal
----------------------------
Put in a working email address



Dir: kdepim/kresources/imap/knotes
----------------------------
Adjust to lib splitting in knotes.
----------------------------
Merge shome fixes from HEAD.
	* when the notes are loaded from imap resource, those have to be registrated
	on the manager. Now, knotes show the list of notes that are in the imap resource.
	* Fix due date wrong after moving todo in KOAgendaView
	* Don't add a todo when we press enter on the QuickLineEdit
	* Use the internal value to report the date, not use the text for this.
	When the text is changed, we updated the internal value.
----------------------------
Put in a working email address



Dir: kdepim/kresources/imap/shared
----------------------------
Put in a working email address



Dir: kdepim/kresources/remote
----------------------------
insert needed catalog



Dir: kdepim/kresources/slox
----------------------------
Read accounts from correct server and use correct protocol.
----------------------------
insert needed catalog
----------------------------
not needed
----------------------------
Set read-only state according to write rights on the server.



Dir: kdepim/ktnef/ktnef
----------------------------
Put in a working email address



Dir: kdepim/ktnef/lib
----------------------------
Put in a working email address



Dir: kdepim/libical/src/libicalss/Attic
----------------------------
What's worse than no comments, is wrong comments.



Dir: kdepim/libkcal
----------------------------
Backport of various bug fixes to the 3.3 branch. I can't remember the actual bug numbers...
----------------------------
Fix compilation on clean system, patch by Bernhard Herzog
(ktnef headers are in kdepim/ktnef/ktnef)
----------------------------
Backport of http://lists.kde.org/?l=kde-cvs&m=109509131620180&w=2
CVS commit by kainhofe:
Turn the Organizer of an incidence into a Person object (instead of using just a
QString). This has the advantage that it's now possible to have a name and the email
specified for the organizer.
----------------------------
Backport from proko2: Don't reset the rsvp flag
----------------------------
fix compilation
----------------------------
Don't overwrite alarm descriptions read from ical, which makes operator==
fail after writeIncidence.



Dir: kdepim/libkdenetwork/gpgmepp
----------------------------
Add DecryptionResult::wrongKeyUsage(), up the version
----------------------------
Add Error::code() and Error::sourceID()
----------------------------
Make it default-constructible
----------------------------
From HEAD: Fix isNull() for FooResult classes. They previously ignored the errors set in them.
----------------------------
fix compile
----------------------------
unbreak compilation (gcc 4.0)
----------------------------
check for wrong_key_usage and define HAVE_GPGME_WRONG_KEY_USAGE if available.



Dir: kdepim/libkdepim
----------------------------
Fixed "LDAP completion only works once" by merging code from the proko2 branch.
Thanks to kuenne at rentec.com for spotting the problem, which reminded me that we fixed this in proko2 :/
BUG: 92874
----------------------------
Cancel LDAP search when cancelling the completion popup



Dir: kdepim/libkdepim/Attic
----------------------------
getNameAndMail("foo <bar>") should return email="bar", not email="bar>"



Dir: kdepim/libkdepim
----------------------------
Backport of fix from Andre Woebbeking:
fixed most annoying bug in KOrganizer ;-)

if I tried to change the start or end time of an event with the mouse
wheel the time jumped to 12:00.
Now it even works for all times :-)

BUG: 75323
----------------------------
Merge shome fixes from HEAD.
	* when the notes are loaded from imap resource, those have to be registrated
	on the manager. Now, knotes show the list of notes that are in the imap resource.
	* Fix due date wrong after moving todo in KOAgendaView
	* Don't add a todo when we press enter on the QuickLineEdit
	* Use the internal value to report the date, not use the text for this.
	When the text is changed, we updated the internal value.
----------------------------
Better fully-qualify the signals, to make sure moc gets it right.
----------------------------
fix compile
----------------------------
unbreak compilation (gcc 4.0)



Dir: kdepim/libkdepim/tests
----------------------------
Makefile.am and copyright, for the email.cpp test program



Dir: kdepim/libkpimexchange/core
----------------------------
Backport from HEAD:
Unbreak downloading from the Exchange server.
Just revert Cornelius' commit with the message "Make it work with OpenGroupware."

BUG: 87837
----------------------------
Backport to the 3.3 Branch of fix for Bug 88005 (times were sent to the server without being shifted to UTC. This was caused by the tzid not being correctly propagated to the uploader)

CCBUG: 88005



Dir: kdepim/mimelib
----------------------------
ranges in case statements are not supported by all C++ compilers (backport revision 1.13)



Dir: kdepim/plugins/kmail/bodypartformatter
----------------------------
Here too.
