2007-10-10 22:26 +0000 [r723858]  bando

	* branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/underGPL.docbook,
	  branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/underFDL.docbook,
	  branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/help-menu.docbook,
	  branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/install-intro.docbook:
	  update

2007-10-12 12:13 +0000 [r724472]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/kstartupinfo.cpp: Handle
	  properly X timestamps that are so high that they cannot be
	  represented properly as signed long, only unsigned. This fixes
	  focus e.g. when mailing a link from Konqueror. CCMAIL:
	  kde-packagers@kde.org Since such X timestamps happen right after
	  startup on both of my 10.3 machines, I suggest packagers consider
	  adding this patch to their 3.5.8 packages.

2007-10-12 12:16 +0000 [r724473]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/kstartupinfo.cpp: Bah, use right
	  index.

2007-10-12 14:55 +0000 [r724524]  bando

	* branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/underArtisticLicense.docbook,
	  branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/underBSDLicense.docbook,
	  branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/underX11License.docbook,
	  branches/KDE/3.5/kdelibs/kdoctools/customization/ja/entities/install-compile.docbook:
	  update

2007-10-13 09:13 +0000 [r724748]  dfaure

	* branches/KDE/3.5/kdelibs/kio/kio/kservice.cpp: Don't write out
	  empty UntranslatedGenericName for most kservices into ksycoca.
	  Reduces the size of ksycoca by 5%.

2007-10-14 02:49 +0000 [r724981]  porten

	* branches/KDE/3.5/kdelibs/khtml/ecma/kjs_navigator.cpp: fixed
	  property table mixup of MimeType objects

2007-10-17 04:11 +0000 [r726098-726097]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/http.cc: ** Revert
	  previous change to propagate received response header earlier as
	  it seems to break things. CC:150904

	* branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp:
	  Cosmetic and comment fix

2007-10-18 03:57 +0000 [r726571]  grossard

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/user.entities:
	  added a translator

2007-10-18 04:08 +0000 [r726574]  grossard

	* branches/KDE/3.5/kdelibs/kdoctools/customization/fr/user.entities:
	  added missing entity for a translator

2007-10-19 11:48 +0000 [r726988]  adawit

	* branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp:
	  Commented out debug statement

2007-10-20 20:49 +0000 [r727528]  aacid

	* branches/KDE/3.5/kdelibs/kdeui/kactionclasses.cpp: Revert r706570
	  which fixed a corner case in kaffeine and broke zoom settings on
	  kpdf, kghostview, kview and basically anything using editable
	  kselectations You'll have to find a fix that does not break all
	  the other applications using the same class as you Please next
	  time be extra careful when touching kdelibs code CCMAIL:
	  gustavo.boiko@kdemail.net BUG: 150944

2007-10-24 00:32 +0000 [r728712]  djarvie

	* branches/KDE/3.5/kdelibs/kdecore/kcmdlineargs.cpp: Fix crash
	  kcmdlineargs.cpp:405: int findOption(const KCmdLineOptions*,
	  QCString&, const char*&, const char*&, bool&): Assertion `result'
	  failed. which occurs when an option prefixed by '!' has a short
	  form defined, and the short form is specified in the command
	  line. Backport of commit 728697.

2007-10-24 03:56 +0000 [r728729]  orlovich

	* branches/KDE/3.5/kdelibs/khtml/ecma/kjs_html.cpp,
	  branches/KDE/3.5/kdelibs/khtml/ecma/kjs_html.h: Provide
	  body.focus, for comments on youtube. Reported by a developer
	  whose name is withheld for fear of the damage to their reputation
	  that the revelation of them reading youtube comments may bring.

2007-10-29 04:05 +0000 [r730518]  orlovich

	* branches/KDE/3.5/kdelibs/khtml/html/html_formimpl.cpp: Don't
	  miraculously not-crash on trying to get RenderWidget's widget
	  pointer out of a RenderFieldSet, but rather work properly for the
	  right reason.

2007-10-29 17:31 +0000 [r730796]  ossi

	* branches/KDE/3.5/kdelibs/kdecore/kconfigbase.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/kconfigbase.h: mark path entries
	  with [$e] on writeout

2007-10-30 10:58 +0000 [r730993]  hasso

	* branches/KDE/3.5/kdelibs/kinit/start_kdeinit_wrapper.c: Make
	  nonLinux case actually work. The patch from FreeBSD KDE ports
	  devel repository.

2007-11-02 11:23 +0000 [r731945]  dfaure

	* branches/KDE/3.5/kdelibs/kdecore/kurl.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/tests/kurltest.cpp: You cannot
	  messup prettyURL in ways that break its definition. Its
	  definition is KURL(u.prettyURL()) == u. The security issue has to
	  be fixed in a better way. BUG: 150973 CCMAIL: mueller@kde.org

2007-11-12 14:25 +0000 [r735731]  ereslibre

	* branches/KDE/3.5/kdelibs/khtml/khtml_part.cpp: Fix the problem of
	  no reseting the hits count. Thanks goes to Tais M. Hansen. This
	  bug is inherited from KHTMLPart from KDE 3.x series with bug
	  report http://bugs.kde.org/show_bug.cgi?id=118152 BUG: 118152

2007-11-14 10:45 +0000 [r736559]  mueller

	* branches/KDE/3.5/kdelibs/kio/misc/kpac/script.cpp: fix build

2007-12-03 19:40 +0000 [r744487]  vriezen

	* branches/KDE/3.5/kdelibs/khtml/java/org/kde/kjas/server/KJASProtocolHandler.java,
	  branches/KDE/3.5/kdelibs/khtml/java/kjavaappletviewer.cpp:
	  KJASProtocolHandler.getArg can't handle empty strings, so for
	  calling a member function from js we must add the number of
	  arguments as extra param. There's no way to see difference
	  between NULL and "" though. Note this needs regenerating the
	  kjava.jar BUG: 153362

2007-12-03 20:29 +0000 [r744503]  vriezen

	* branches/KDE/3.5/kdelibs/khtml/java/kjava.jar: Regenerate

2007-12-04 18:51 +0000 [r744863]  dakon

	* branches/KDE/3.5/kdelibs/kio/kio/karchive.cpp: Make KArchive
	  include hidden files in directories acked by dfaure CCBUG:129958
	  Backport of r744752

2007-12-11 07:51 +0000 [r747156]  vkrause

	* branches/KDE/3.5/kdelibs/kabc/scripts/addressee.src.cpp:
	  Correctly handle quotes in quoted names.

2007-12-12 14:28 +0000 [r747606]  lunakl

	* branches/KDE/3.5/kdelibs/kdeui/qxembed.cpp,
	  branches/KDE/3.5/kdelibs/kdeui/qxembed.h: Handle also the case
	  when a window to be embedded is not reparented into QXEmbed but
	  simply created there.

2007-12-20 13:07 +0000 [r750896]  lunakl

	* branches/KDE/3.5/kdelibs/kdeui/qxembed.cpp: Flash sucks.

2007-12-21 14:02 +0000 [r751253]  lunakl

	* branches/KDE/3.5/kdelibs/kdeui/qxembed.cpp: svnrevertlast, hack
	  for flash is elsewhere.

2008-01-04 19:08 +0000 [r757346]  lunakl

	* branches/KDE/3.5/kdelibs/kdeui/qxembed.cpp: Fix embedding started
	  from outside by reparenting inside to QXEmbed. CCBUG: 155001
	  CCBUG: 132138

2008-01-06 22:01 +0000 [r758100]  mkoller

	* branches/KDE/3.5/kdelibs/kabc/vcardformatplugin.cpp,
	  branches/KDE/3.5/kdelibs/kabc/vcardparser/testread.cpp,
	  branches/KDE/3.5/kdelibs/kabc/vcardparser/vcardparser.cpp: BUG:
	  98790 Correctly handle CHARSET attribute in vcard parsing and
	  handle given string as plain C-byte array

2008-01-06 22:04 +0000 [r758102]  mkoller

	* branches/KDE/3.5/kdelibs/kabc/vcardconverter.h: correct file
	  reading example to not do UTF8 conversion before parsing the
	  vcard

2008-01-10 16:01 +0000 [r759452]  mueller

	* branches/KDE/3.5/kdelibs/kdecore/kstandarddirs.cpp: fix findExe
	  (backport -r736144)

2008-01-14 16:44 +0000 [r761357]  orlovich

	* branches/KDE/3.5/kdelibs/khtml/ecma/kjs_traversal.cpp: Don't
	  crash on acid3 BUG:155451

2008-01-16 19:42 +0000 [r762308]  djarvie

	* branches/KDE/3.5/kdelibs/kdoctools/customization/entities/contributor.entities:
	  Update email address

2008-01-18 15:59 +0000 [r763109]  lunakl

	* branches/KDE/3.5/kdelibs/kdeui/qxembed.cpp: Cosmetic fix.

2008-01-25 17:06 +0000 [r766354]  lunakl

	* branches/KDE/3.5/kdelibs/kio/kio/krun.cpp: Fix bnc:#343359 - it
	  actually can cause trouble when using startup notification for
	  non-compliant apps, even if silent :-/.

2008-01-30 10:18 +0000 [r768533]  berendsen

	* branches/KDE/3.5/kdelibs/kate/data/lilypond.xml,
	  branches/KDE/3.5/kdelibs/kate/data/language.dtd,
	  branches/KDE/3.5/kdelibs/kate/tests/highlight.ly: Lilypond
	  highlighting backport, including update for backgroundColor and
	  selBackgroundColor in dtd

2008-02-08 14:17 +0000 [r772352]  jriddell

	* branches/KDE/3.5/kdelibs/kate/part/katesupercursor.cpp: patch
	  from Sergei Ivanov <svivanov@pdmi.ras.ru>, Closes
	  http://bugs.kde.org/show_bug.cgi?id=146300

2008-02-08 15:17 +0000 [r772369]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/kapplication.cpp: Avoid
	  launching kdeinit several times at once e.g. when launching
	  several KDE apps outside of KDE under heavy load.

2008-02-08 17:44 +0000 [r772413]  lunakl

	* branches/KDE/3.5/kdelibs/kio/kio/kuserprofile.cpp: // HACK
	  ksycoca may open the dummy db, in such case the first call to
	  ksycoca // in initStatic() leads to closing the dummy db and
	  clear() being called // in the middle of it, making s_lstProfiles
	  be NULL

2008-02-13 09:38 +0000 [r774438]  coolo

	* branches/KDE/3.5/kdelibs/kdeui/kdepackages.h: most of this is
	  KDE4, but who cares - it's in the checklist :)

2008-02-13 09:50 +0000 [r774454-774453]  coolo

	* branches/KDE/3.5/kdelibs/kdecore/kdeversion.h: 3.5.9

	* branches/KDE/3.5/kdelibs/README: 3.5.9

2008-02-13 09:53 +0000 [r774471]  coolo

	* branches/KDE/3.5/kdelibs/kdelibs.lsm: 3.5.9

