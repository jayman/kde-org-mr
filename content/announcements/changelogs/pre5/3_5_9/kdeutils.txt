2007-10-23 16:25 +0000 [r728537]  jriddell

	* branches/KDE/3.5/kdeutils/ark/zoo.cpp: fix from Anthony
	  Mercatante, ensure zoo available for uncompress too

2007-11-06 17:12 +0000 [r733550]  cartman

	* branches/KDE/3.5/kdeutils/ark/rar.cpp: Fix opening password
	  protected RAR archives, patch by Ozan Çağlayan
	  <ozan@pardus.org.tr>, thanks! BUG:FIXED:135099

2007-11-06 17:20 +0000 [r733552]  cartman

	* branches/KDE/3.5/kdeutils/ark/arch.h,
	  branches/KDE/3.5/kdeutils/ark/rar.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: Use
	  QString::fromLocal8Bit on console data to fix display issues on
	  Turkish locale

2007-11-06 17:32 +0000 [r733554]  cartman

	* branches/KDE/3.5/kdeutils/kfloppy/format.cpp: Fix KFloppy with
	  udev, patch found on Mandriva, Pardus, etc.

2007-11-26 09:12 +0000 [r741729]  coolo

	* branches/KDE/3.5/kdeutils/ark/compressedfile.cpp: compile with
	  glibc 2.7 and fix a braino

2007-11-28 07:24 +0000 [r742520]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/kgpg.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpginterface.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpginterface.h: Fix leak of
	  KArchive instance when encrypting folder This leads to leftover
	  temporary files besides the memory leak. BUG:150605 Port to trunk
	  will follow soon, there are currently some additional problems.

2007-12-05 18:10 +0000 [r745196]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/kgpg.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpginterface.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpginterface.h: rework r742520
	  This was totally overcomplicated

2007-12-19 13:44 +0000 [r750514]  mueller

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.h,
	  branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.h:
	  workaround build breakage with gmp 4.2

2008-01-02 17:48 +0000 [r756071]  djarvie

	* branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.h,
	  branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.cpp: Use
	  KMix master device to adjust volume and mute. It uses a newly
	  committed addition to KMix's DCOP interface which allows the
	  master device to be determined. Fixes commit 624936 which broke
	  things completely for laptops which don't use device 0 as the
	  master. This patch was contributed by Kelvie Wong. CCBUGS:134820

2008-02-02 18:34 +0000 [r770083]  fabo

	* branches/KDE/3.5/kdeutils/klaptopdaemon/portable.cpp: Fix
	  "Hibernate" and "Lock & Hibernate" missing menu in kernel >=
	  2.6.22 BUG: 148928

2008-02-04 09:40 +0000 [r770642]  woebbe

	* branches/KDE/3.5/kdeutils/klaptopdaemon/portable.cpp: compile

2008-02-06 12:57 +0000 [r771580]  jriddell

	* branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.h,
	  branches/KDE/3.5/kdeutils/kmilo/generic/README,
	  branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.cpp:
	  Patch from Matej Laitl strohel@gmail.com * kmilo generic plugin
	  is now configurable (no gui yet, one have to use kwriteconfig) -
	  one can set which kmix channels to mute and set volume on
	  (separately - needed for Intel HDA soundcards) and one can now
	  fine-tune "slow" and "fast" volume steps (bugs 149183, 92730) *
	  when channels are not configured, query kmix for master channel *
	  some cleanups (removal of dupe code) and new volume computation
	  algo changed a bit (see comments is patch) * unregister global
	  shortcuts in destructor (bug 75169) * support for controlling an
	  extra channel, for people with surround sound * README extended
	  to contain relevant information

2008-02-13 09:54 +0000 [r774477]  coolo

	* branches/KDE/3.5/kdeutils/kdeutils.lsm: 3.5.9

