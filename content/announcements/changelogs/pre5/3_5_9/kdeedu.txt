2007-10-17 12:49 +0000 [r726282]  annma

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/sv.keyboard,
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am: Swedish
	  keyboard thanks to tito

2007-10-17 13:02 +0000 [r726289-726286]  cartman

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/tr.q.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/tr.f.keyboard (added),
	  branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am: Add support
	  for Turkish Q/F keyboards

	* branches/KDE/3.5/kdeedu/ktouch/training/turkish.ktouch.xml
	  (added), branches/KDE/3.5/kdeedu/ktouch/training/Makefile.am: Add
	  Turkish training support

2007-10-30 06:09 +0000 [r730918]  harris

	* branches/KDE/3.5/kdeedu/libkdeedu/extdate/extdatetime.cpp: Fixing
	  bug #151302. Will close report with the commit to trunk.

2007-11-02 08:14 +0000 [r731897]  repinc

	* branches/KDE/3.5/kdeedu/doc/ktouch/index.docbook: Fixed typo BUG:
	  151722

2007-11-03 20:12 +0000 [r732419]  messmer

	* branches/KDE/3.5/kdeedu/kpercentage/kpercentage/main.cpp,
	  branches/KDE/3.5/kdeedu/kpercentage/kpercentage/kpercentage.cpp:
	  fixed copyright years save/restore window position

2007-11-05 19:41 +0000 [r733179]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/asia.kgm: backport
	  r733178 kgeography/trunk/KDE/kdeedu/kgeography/data/asia.kgm:
	  indonesia is asia, do not ignore it!

2007-11-14 07:19 +0000 [r736418]  annma

	* branches/KDE/3.5/kdeedu/klettres/klettres/klettresview.cpp,
	  branches/KDE/3.5/kdeedu/klettres/klettres/klettresview.h: fix
	  backspace bug, thanks a lot to Mark Bryan Yu! Bryan: I'll put it
	  in KDE 4 code, no problem! Warm thanks! CCMAIL=vafada@gmail.com

2007-11-17 08:46 +0000 [r737866]  annma

	* branches/KDE/3.5/kdeedu/khangman/khangman/khangmanview.cpp,
	  branches/KDE/3.5/kdeedu/khangman/README.packagers: reduce minimum
	  size for small devices tested on the Classmate PC BUG=137757

2007-11-17 18:17 +0000 [r738026]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/tools/planetviewer.cpp:
	  Fixing bug #143608 Planet viewer now ignores invalid dates. BUG:
	  143608

2007-11-20 06:22 +0000 [r738998]  mutlaqja

	* branches/KDE/3.5/kdeedu/kstars/kstars/indiproperty.cpp: Fixing
	  menu-to-command mapping for all INDI devices. This is a quick fix
	  for the problem. It was solved in KDE 4 branch using a completely
	  different approach. BUG:152453 CCMAIL:kstars-devel@kde.org

2007-11-20 09:15 +0000 [r739030]  pvicente

	* branches/KDE/3.5/kdeedu/kstars/kstars/kstarsdata.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/skymapdraw.cpp: CCMAIL:
	  kstars-devel@kde.org BUG fixed. This bug was not explicitely
	  declared in the KDE bug database. Fixed the custom catalogs
	  feature which was not working (all sources were drawn at the same
	  place). After this fix: - Stars are correctly drawn on the sky
	  map. - The size of the spot for type 0 objects (stars) is
	  proportional to the magnitude. - Names of the sources are shown -
	  The sources are painted according to the color selected in the
	  custom catalog dialog. This fix has only been tested with stars
	  (type 0 objects). No test tried with other kind of objects.

2007-11-25 14:24 +0000 [r741363]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/kstars.cpp: Fixing bug
	  #152869 Hidden Toolbars not persistent on restart. Also fixed the
	  same issue for the statusbar. I had thought that calling
	  setChecked() on the KToggleActions would trigger the connected
	  slotShowGUIItem(), but apparently not. Interestingly, the call to
	  slotGUIItem() *is* successfully made in the trunk codebase...so
	  no fix needed there. Thanks for the report! BUG: 152869 CCMAIL:
	  kstars-devel@kde.org

2007-12-13 09:34 +0000 [r747985]  annma

	* branches/KDE/3.5/kdeedu/ktouch/training/english.ktouch.xml:
	  backport of fix for 142118 that closes 149620 and makes the fix
	  avalaible for the next 3.5 release if any! BUG=149620

2007-12-13 19:41 +0000 [r748177]  annma

	* branches/KDE/3.5/kdeedu/ktouch/src/ktouchstatus.cpp,
	  branches/KDE/3.5/kdeedu/ktouch/src/ktouchtrainer.cpp: I think I
	  tackled it. Roberto if you have KTouch sources you can apply
	  manual ly my changes and try it. If not, I'll look at KTouch in
	  KDE 4 to apply the s ame changes if possible and have people test
	  it. CCBUG=152610

2007-12-22 05:29 +0000 [r751532]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/ksplanetbase.cpp: Fixing
	  bug in KSPlanetBase::setRearth() that caused the incorrect
	  distance to be computed. I needed to use the heliocentric
	  ecliptic coordinates, not the geocentric. This fixes Bug #147410
	  BUG: 147410 CCMAIL: kstars-devel@kde.org

2007-12-29 15:20 +0000 [r754321]  pino

	* branches/KDE/3.5/kdeedu/kig/misc/coordinate_system.cpp: Euclidean
	  grid: - extend its drawing so it covers the whole visible area -
	  use the same grid scale (the smallest one) for x and y steps
	  CCBUG: 154814

2007-12-29 16:51 +0000 [r754360]  pino

	* branches/KDE/3.5/kdeedu/kig/filters/exporter.cc: Pass the
	  coordinate of bottom-left corner of the text label, as XFig
	  wants. CCBUG: 153629

2008-01-02 18:27 +0000 [r756093]  pino

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/query-dialogs/MCQueryDlgForm.ui:
	  fixuifiles

2008-01-26 23:16 +0000 [r766928-766927]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/skymap.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/ksmoon.cpp: Fixing crash
	  condition in SkyMap::refract(). The array index could sometimes
	  go out of bounds, leading to a segfault. Will port to trunk and
	  4.0 branch BUG: 133505 CCMAIL: kstars-devel@kde.org

	* branches/KDE/3.5/kdeedu/kstars/kstars/ksmoon.cpp: The KSMoon
	  changes should not have been part of the previous commit.
	  Reverting. CCBUG: 133505 CCMAIL: kstars-devel@kde.org

2008-01-27 20:51 +0000 [r767344]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/kstarsdata.h,
	  branches/KDE/3.5/kdeedu/kstars/kstars/tools/observinglist.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/kstarsdata.cpp: Backporting
	  fix for bug #147742 to 3.5 branch. In addition, I added a fix
	  that allows you to save stars with only a genetive name (i.e.,
	  "sigma Orionis"). Prior to this fix, the greek letter was
	  represented as a "?" in the observing list file, and could not be
	  parsed when reopening the list. Now, the greek letters are saved
	  as ascii-letter words instead of the actual greek letter. Will
	  forward-port this genetive name fix to 4.0 and trunk as well.
	  CCBUG: 147742 CCMAIL: kstars-devel@kde.org

2008-01-28 01:05 +0000 [r767412]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/main.cpp: Bump version
	  number to 1.2.9 (for KDE 3.5.9)

2008-01-29 15:06 +0000 [r768150]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/kstarsdata.cpp: Applying
	  patch to fix bug #156845 (Custom catalog objects not reloaded in
	  observing list). Thanks for the patch, Carl! No need to
	  forward-port this fix; custom objects already get reloaded in
	  observing lists in 4.x. BUG: 156845 CCMAIL: kstars-devel@kde.org

2008-01-30 00:44 +0000 [r768417]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/data/ngcic03.dat:
	  Backporting patch from Akarsh to fix object type for NGC 1975 and
	  NGC 1977, which are both nebulae.

2008-02-06 21:08 +0000 [r771746]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/flags/manitoba.png:
	  backport r771732 **************** fix manitoba flag, all
	  triangles of the union jack must be blue

2008-02-06 21:17 +0000 [r771756-771754]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/canada.png: Backport
	  r771752 **************** Fix The Belcher Islands in Hudson Bay
	  are shown belonging to Quebec and Akimiski Island in James Bay is
	  shown belonging to Ontario when they belong to Nunavut

	* branches/KDE/3.5/kdeedu/kgeography/src/main.cpp: increase version
	  number for 3.5.9 release

2008-02-13 09:53 +0000 [r774468]  coolo

	* branches/KDE/3.5/kdeedu/kdeedu.lsm: 3.5.9

