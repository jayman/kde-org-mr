2008-06-25 16:51 +0000 [r824342]  anagl

	* branches/KDE/3.5/kdeadmin/kuser/kuser.desktop,
	  branches/KDE/3.5/kdeadmin/lilo-config/kde/lilo.desktop,
	  branches/KDE/3.5/kdeadmin/kdat/kdat.desktop,
	  branches/KDE/3.5/kdeadmin/kcron/kcron.desktop,
	  branches/KDE/3.5/kdeadmin/kpackage/kpackage.desktop,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kcm_knetworkconfmodule.desktop,
	  branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.desktop,
	  branches/KDE/3.5/kdeadmin/ksysv/ksysv.desktop: Desktop validation
	  fixes: remove deprecated entries for Encoding and fix Categories.

2008-06-25 17:11 +0000 [r824381]  anagl

	* branches/KDE/3.5/kdeadmin/lilo-config/kde/lilo.desktop: Settings
	  fits better than System.

2008-06-25 17:29 +0000 [r824408]  anagl

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in:
	  Fix typo in Ubuntu release name. I can not believe, *I* am fixing
	  this.

2008-06-25 20:42 +0000 [r824461]  anagl

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  Add support to KDE3's knetworkconf for Debian Lenny 5.0. When the
	  platform is not recognised it will be set by default to
	  "testing".

2008-06-29 22:38 +0000 [r826170]  anagl

	* branches/KDE/3.5/kdeadmin/kfile-plugins/rpm/kfile_rpm.desktop,
	  branches/KDE/3.5/kdeadmin/ksysv/x-ksysv-log.desktop,
	  branches/KDE/3.5/kdeadmin/ksysv/x-ksysv.desktop,
	  branches/KDE/3.5/kdeadmin/kfile-plugins/deb/kfile_deb.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-08-19 19:47 +0000 [r849593]  coolo

	* branches/KDE/3.5/kdeadmin/kdeadmin.lsm: update for 3.5.10

2008-08-21 04:38 +0000 [r850245]  gorlik

	* branches/KDE/3.5/kdeadmin/kdat/kdat.h,
	  branches/KDE/3.5/kdeadmin/kdat/OptionsDlgWidget.ui: BUG: 95625
	  fix the 99 bytes block size limit bug

