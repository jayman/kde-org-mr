2008-05-23 02:24 +0000 [r811447]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/juk.cpp: Forgot to backport
	  this to KDE 3.5, which is what bug 144427 was reported against.
	  The bug report was so useful that I want to reward the reporter
	  so if you're still using 3.5, it should crash less now.
	  CCBUG:144427

2008-06-29 22:51 +0000 [r826180]  anagl

	* branches/KDE/3.5/kdemultimedia/kappfinder-data/djplay.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/vkeybd.desktop,
	  branches/KDE/3.5/kdemultimedia/arts/builder/artsbuilder.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/qsynth.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/avi/kfile_avi.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/mpc/kfile_mpc.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/sid/kfile_sid.desktop,
	  branches/KDE/3.5/kdemultimedia/juk/juk.desktop,
	  branches/KDE/3.5/kdemultimedia/arts/tools/artscontrol.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/freqtweak.desktop,
	  branches/KDE/3.5/kdemultimedia/libkcddb/kcmcddb/libkcddb.desktop,
	  branches/KDE/3.5/kdemultimedia/arts/builder/x-artsbuilder.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/qjackctl.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/theora/kfile_theora.desktop,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.desktop,
	  branches/KDE/3.5/kdemultimedia/krec/mp3_export/krec_exportmp3.desktop,
	  branches/KDE/3.5/kdemultimedia/arts/tools/artscontrolapplet.desktop,
	  branches/KDE/3.5/kdemultimedia/krec/krec_exportwave.desktop,
	  branches/KDE/3.5/kdemultimedia/kmix/restore_kmix_volumes.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/au/kfile_au.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/flac/kfile_flac.desktop,
	  branches/KDE/3.5/kdemultimedia/krec/kcm_krec.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/mpeg/kfile_mpeg.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/muse.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/freebirth.desktop,
	  branches/KDE/3.5/kdemultimedia/kmid/x-karaoke.desktop,
	  branches/KDE/3.5/kdemultimedia/xine_artsplugin/tools/thumbnail/videothumbnail.desktop,
	  branches/KDE/3.5/kdemultimedia/noatun/noatun.desktop,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixctrl_restore.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/galan.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/wav/kfile_wav.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/amsynth.desktop,
	  branches/KDE/3.5/kdemultimedia/kscd/xmcd.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/rezound.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/jack-rack.desktop,
	  branches/KDE/3.5/kdemultimedia/kmix/kmix.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/mp3/kfile_mp3.desktop,
	  branches/KDE/3.5/kdemultimedia/kaboodle/kaboodle_component.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/ecamegapedal.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/meterbridge.desktop,
	  branches/KDE/3.5/kdemultimedia/krec/ogg_export/krec_exportogg.desktop,
	  branches/KDE/3.5/kdemultimedia/krec/kcm_krec_files.desktop,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixapplet.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/ardour.desktop,
	  branches/KDE/3.5/kdemultimedia/kaboodle/kaboodleengine.desktop,
	  branches/KDE/3.5/kdemultimedia/kmid/audiomidi.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/jamin.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/zynaddsubfx.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/m3u/kfile_m3u.desktop,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/audiocd.desktop,
	  branches/KDE/3.5/kdemultimedia/kaboodle/kaboodle.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/mixxx.desktop,
	  branches/KDE/3.5/kdemultimedia/kscd/kscd.desktop,
	  branches/KDE/3.5/kdemultimedia/krec/krec.desktop,
	  branches/KDE/3.5/kdemultimedia/kmid/kmid.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/hydrogen.desktop,
	  branches/KDE/3.5/kdemultimedia/kappfinder-data/ams.desktop,
	  branches/KDE/3.5/kdemultimedia/kfile-plugins/ogg/kfile_ogg.desktop,
	  branches/KDE/3.5/kdemultimedia/noatun/modules/winskin/mimetypes/interface/x-winamp-skin.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-07-06 20:54 +0000 [r828869]  anagl

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/audiocd.desktop,
	  branches/KDE/3.5/kdemultimedia/libkcddb/kcmcddb/libkcddb.desktop:
	  Desktop validation fixes: fix Categories.

2008-07-20 10:25 +0000 [r835252]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/mixer_oss4.cpp (added),
	  branches/KDE/3.5/kdemultimedia/kmix/kmix-platforms.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer_oss4.h (added): Add
	  OpenSoundSystem V4 support. This is neccesary for newer
	  soundcards in combination with disabled ALSA support. CCBUGS:
	  166591

2008-08-11 14:05 +0000 [r845341]  bminisini

	* branches/KDE/3.5/kdemultimedia/kmix/kmixapplet.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/viewapplet.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mdwslider.cpp: Support for
	  transparency in the mixer applet.

2008-08-19 19:48 +0000 [r849602]  coolo

	* branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm: update for
	  3.5.10

