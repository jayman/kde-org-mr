------------------------------------------------------------------------
r1008115 | dfaure | 2009-08-06 22:23:40 +0000 (Thu, 06 Aug 2009) | 2 lines

Backport: fix unit tests, patch by Richard Lärkäng

------------------------------------------------------------------------
r1014170 | rkcosta | 2009-08-21 18:51:44 +0000 (Fri, 21 Aug 2009) | 7 lines

Backport 1014167.

Replace disabled screensaver code to KNotificationRestrictions with DBUS call to
screensaver.

CCBUG: 168460

------------------------------------------------------------------------
r1014326 | anagl | 2009-08-22 13:20:27 +0000 (Sat, 22 Aug 2009) | 3 lines

Backporting r1014322:
Include also systems providing a FreeBSD kernel.

------------------------------------------------------------------------
r1014635 | anagl | 2009-08-23 12:43:26 +0000 (Sun, 23 Aug 2009) | 4 lines

Backporting r1014630:
Fix for systems with a kfreebsd kernel.


------------------------------------------------------------------------
r1015270 | scripty | 2009-08-25 03:36:08 +0000 (Tue, 25 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
