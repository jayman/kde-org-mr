------------------------------------------------------------------------
r1004115 | mart | 2009-07-29 11:42:21 +0000 (Wed, 29 Jul 2009) | 2 lines

backport the missing cmake file

------------------------------------------------------------------------
r1004184 | lunakl | 2009-07-29 14:21:58 +0000 (Wed, 29 Jul 2009) | 3 lines

backport r1004183


------------------------------------------------------------------------
r1004277 | aseigo | 2009-07-29 19:02:03 +0000 (Wed, 29 Jul 2009) | 5 lines

only change the urls to local paths when trashing (satisfies the comment in the
code); otherwise stick with the full url
BUG:201650


------------------------------------------------------------------------
r1004281 | ppenz | 2009-07-29 19:17:20 +0000 (Wed, 29 Jul 2009) | 1 line

Backport of SVN commit 1004276 by mjansen: Fix memory leak. QMenu::addAction(QMenu*) does not take ownership.
------------------------------------------------------------------------
r1004334 | mueller | 2009-07-29 21:26:12 +0000 (Wed, 29 Jul 2009) | 2 lines

fix soprano min version

------------------------------------------------------------------------
r1004336 | mueller | 2009-07-29 21:26:36 +0000 (Wed, 29 Jul 2009) | 2 lines

fix version number

------------------------------------------------------------------------
r1004353 | cgiboudeaux | 2009-07-29 22:10:01 +0000 (Wed, 29 Jul 2009) | 1 line

Also fix the typo in the 4.3 branch
------------------------------------------------------------------------
r1004389 | scripty | 2009-07-30 03:11:50 +0000 (Thu, 30 Jul 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1004438 | trueg | 2009-07-30 07:28:56 +0000 (Thu, 30 Jul 2009) | 5 lines

Backport:
Fixed behaviour of hidden folders checkbox:
* If it is unchecked when saving, remove all hidden dirs from the folders lists
* If one of the lists contains a hidden folder on load, check the hidden folders checkbox

------------------------------------------------------------------------
r1004452 | graesslin | 2009-07-30 08:16:02 +0000 (Thu, 30 Jul 2009) | 3 lines

Backport r1004451: Ensure there is another repaint when wobbly windows ends.
CCBUG: 201244

------------------------------------------------------------------------
r1004455 | wstephens | 2009-07-30 08:23:23 +0000 (Thu, 30 Jul 2009) | 4 lines

Backport r1004453 - undefined virtual method breaking 4.3 with NM 0.6

CCMAIL: dmueller@suse.de

------------------------------------------------------------------------
r1004746 | aseigo | 2009-07-30 19:29:13 +0000 (Thu, 30 Jul 2009) | 5 lines

reset the interface after setting the location-based layout
patch by Felix Geyer
CCMAIL:debfx-kde@fobos.de
CCBUG:201998

------------------------------------------------------------------------
r1004812 | aseigo | 2009-07-30 23:41:13 +0000 (Thu, 30 Jul 2009) | 3 lines

only show the toolbox on the very first time plasma is run by that user
BUG:201984

------------------------------------------------------------------------
r1004911 | scripty | 2009-07-31 03:16:16 +0000 (Fri, 31 Jul 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1004932 | wstephens | 2009-07-31 05:19:51 +0000 (Fri, 31 Jul 2009) | 2 lines

merge r1004931 - missing class qualifier on method

------------------------------------------------------------------------
r1005232 | cfeck | 2009-07-31 15:40:03 +0000 (Fri, 31 Jul 2009) | 2 lines

Fix preview margins (backport r1005231)

------------------------------------------------------------------------
r1005249 | winterz | 2009-07-31 17:01:02 +0000 (Fri, 31 Jul 2009) | 4 lines

backport SVN commit 1005130 by tilladam:

Implementation of a timezone daemon for Windows that watches the registry and emits a dbus signal when the current timezone changes. This requires some refactoring of shared implementation into a base class for the unix and Windows implementations. Reviewed by David Jarvie.

------------------------------------------------------------------------
r1005250 | winterz | 2009-07-31 17:01:33 +0000 (Fri, 31 Jul 2009) | 4 lines

backport SVN commit 1005136 by sengels:

fix fromWCharArray

------------------------------------------------------------------------
r1005265 | mart | 2009-07-31 17:44:55 +0000 (Fri, 31 Jul 2009) | 2 lines

backport a size fix for different resolutions

------------------------------------------------------------------------
r1005268 | mart | 2009-07-31 17:50:34 +0000 (Fri, 31 Jul 2009) | 2 lines

backport fields alignent fix

------------------------------------------------------------------------
r1005436 | scripty | 2009-08-01 03:05:34 +0000 (Sat, 01 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1005654 | segato | 2009-08-01 16:56:18 +0000 (Sat, 01 Aug 2009) | 3 lines

backport commit r1005652
fix compilation

------------------------------------------------------------------------
r1005694 | ppenz | 2009-08-01 19:39:34 +0000 (Sat, 01 Aug 2009) | 5 lines

Backport of SVN commit 1005693: Assure that the panels don't shrink when the view is split and the zoomslide and spaceinfo is shown in the statusbar.

KDE 4.3.0 has been tagged already I think, so the patch will be part of KDE 4.3.1.

CCBUG: 202147
------------------------------------------------------------------------
r1005824 | mleupold | 2009-08-02 10:12:00 +0000 (Sun, 02 Aug 2009) | 2 lines

Backport of r1005822:
Insert correct locale for Singaporese times (12h no leading 0).
------------------------------------------------------------------------
r1005994 | aseigo | 2009-08-02 19:08:17 +0000 (Sun, 02 Aug 2009) | 2 lines

protect access to task() as that can be null

------------------------------------------------------------------------
r1006084 | aseigo | 2009-08-03 00:20:42 +0000 (Mon, 03 Aug 2009) | 6 lines

backport SVN commit 1005669 by mkoller:

escape the window title text in tooltip so that HTML tags inside it are not
rendered as HTML.


------------------------------------------------------------------------
r1006336 | trueg | 2009-08-03 14:08:13 +0000 (Mon, 03 Aug 2009) | 1 line

Backported improved clucene query analyzer instead of using the plain whitespace one. This fixes for example querying for filenames.
------------------------------------------------------------------------
r1006755 | mueller | 2009-08-04 10:06:05 +0000 (Tue, 04 Aug 2009) | 2 lines

change default to oxygen-air

------------------------------------------------------------------------
r1006947 | jacopods | 2009-08-04 19:10:55 +0000 (Tue, 04 Aug 2009) | 2 lines

SVN_SILENT: minor ws cleanup

------------------------------------------------------------------------
r1006951 | jacopods | 2009-08-04 19:16:47 +0000 (Tue, 04 Aug 2009) | 3 lines

Make the notifier background transparent so that it looks good as a standalone plasmoid (don't know why it ever worked as a 
popupapplet)

------------------------------------------------------------------------
r1007304 | aseigo | 2009-08-05 13:26:32 +0000 (Wed, 05 Aug 2009) | 6 lines

backport SVN commit 1006608 by peterpan:

Don't change the size of panel when we change the plasma theme or enable/disable window effects.

CCBUG:184905

------------------------------------------------------------------------
r1007361 | mart | 2009-08-05 16:15:50 +0000 (Wed, 05 Aug 2009) | 2 lines

backport of 995768 use ExpandFlag for vertical panels too

------------------------------------------------------------------------
r1007364 | lunakl | 2009-08-05 16:21:00 +0000 (Wed, 05 Aug 2009) | 9 lines

Backport:
Remove aborting if there's any unknown X error during kwin
startup. It dates so back in history that the KWM commit
message is completely useless and I see no point in that,
it can just needlessly make kwin abort because of a harmless
X error e.g. in libGL. Real problems should be handled
explicitly wherever they happen.


------------------------------------------------------------------------
r1007497 | dfaure | 2009-08-05 20:52:13 +0000 (Wed, 05 Aug 2009) | 2 lines

backport: cleanup

------------------------------------------------------------------------
r1007535 | aseigo | 2009-08-05 21:59:18 +0000 (Wed, 05 Aug 2009) | 8 lines

backport r1001470 :

* Add interfaces correctly
* Fix regexp so it won't match for dataTotal
BUG: 198885
CCBUG: 196712


------------------------------------------------------------------------
r1007554 | aseigo | 2009-08-05 23:10:21 +0000 (Wed, 05 Aug 2009) | 3 lines

backport  r1007552:  don't assert, check the member. it can be 0.
BUG:199894

------------------------------------------------------------------------
r1007739 | bcooksley | 2009-08-06 08:44:15 +0000 (Thu, 06 Aug 2009) | 3 lines

Backport of commit 1007731.
Use the correct foreground colour in tooltips.
CCBUG: 202119
------------------------------------------------------------------------
r1007982 | trueg | 2009-08-06 16:23:31 +0000 (Thu, 06 Aug 2009) | 1 line

Backport: do not compile index-related things in case Soprano-index is not available
------------------------------------------------------------------------
r1008050 | cfeck | 2009-08-06 19:04:03 +0000 (Thu, 06 Aug 2009) | 4 lines

Do not draw twice (backport r1008049)

CCBUG: 202701

------------------------------------------------------------------------
r1008101 | aseigo | 2009-08-06 21:46:55 +0000 (Thu, 06 Aug 2009) | 8 lines

backport r1008100:

* when our screen changes, update the working area
* when we update our working area, check to make sure that the screen actually exists currently: it may be a screen that USED to exist but doesn't anymore (b
ut is still associated with it in case that screen reappears!)

BUG:187319

------------------------------------------------------------------------
r1008123 | aseigo | 2009-08-06 22:50:04 +0000 (Thu, 06 Aug 2009) | 2 lines

these signals do not exist in 4.3

------------------------------------------------------------------------
r1008146 | dfaure | 2009-08-06 23:50:48 +0000 (Thu, 06 Aug 2009) | 2 lines

backport fix for unittest

------------------------------------------------------------------------
r1008154 | aseigo | 2009-08-07 00:35:18 +0000 (Fri, 07 Aug 2009) | 2 lines

backport translation fixes

------------------------------------------------------------------------
r1008188 | scripty | 2009-08-07 03:03:56 +0000 (Fri, 07 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1008512 | rdale | 2009-08-07 17:22:46 +0000 (Fri, 07 Aug 2009) | 4 lines

* Fixed typo in the type signature of the initExtenderItem() slot. Thanks to
David Palacio for reporting the bug


------------------------------------------------------------------------
r1008538 | mpyne | 2009-08-07 19:00:30 +0000 (Fri, 07 Aug 2009) | 4 lines

Backport fix for NTP setting save location to KDE 4.3.1.

Thanks to Sian Cao for the patch.

------------------------------------------------------------------------
r1008608 | darioandres | 2009-08-08 01:12:29 +0000 (Sat, 08 Aug 2009) | 6 lines

Backport to 4.3:
- On the Send page of the assistant, leave the "Show contents of report" button visible
  until the report is properly sent, so we can allow the reporter to check the report/backtrace
  even during the sending process (which may be slow & infinite due bugzilla stress/fail)


------------------------------------------------------------------------
r1008700 | scripty | 2009-08-08 03:20:15 +0000 (Sat, 08 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1008861 | cfeck | 2009-08-08 14:18:10 +0000 (Sat, 08 Aug 2009) | 4 lines

Fix crash after drag selecting languages (backport r1008860)

CCBUG: 201578

------------------------------------------------------------------------
r1008903 | alake | 2009-08-08 16:12:00 +0000 (Sat, 08 Aug 2009) | 3 lines

Reduce minimum bottom border size to look proportional to sides when size >= Normal.  
Bottom corner rounding is unaffected.

------------------------------------------------------------------------
r1009031 | huynhhuu | 2009-08-08 23:16:35 +0000 (Sat, 08 Aug 2009) | 8 lines

Backport of r1009001:
Increase the clickable area without changing the visible border
(which essentially means that the shadow can be used to resize the window)

Backport of r1009021:
Don't enlarge the clickable area in non-composited environments with no shadows


------------------------------------------------------------------------
r1009062 | scripty | 2009-08-09 04:16:56 +0000 (Sun, 09 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1009293 | mdawson | 2009-08-09 16:12:19 +0000 (Sun, 09 Aug 2009) | 2 lines

Fix compile error in kwin's logout effect when opengl is disabled.

------------------------------------------------------------------------
r1009770 | darioandres | 2009-08-10 20:17:10 +0000 (Mon, 10 Aug 2009) | 12 lines

Fixed for 4.4 by svn rev. 1009763
Backport to 4.3branch:

In some situation, the Popup of the folderview may be still showing an invalid(non-existant)
folder (case described on https://bugs.kde.org/show_bug.cgi?id=198499#c7)
Right-clicking the popup will crash Plasma as it is trying to read a field from an invalid
KFileItem)

- Add a check for KFileItem(rootItem)::isNull and avoid showing the menu in that case

BUG: 198499

------------------------------------------------------------------------
r1010069 | wstephens | 2009-08-11 15:06:28 +0000 (Tue, 11 Aug 2009) | 4 lines

Merge
r1010068 - robustness fixes in backend
r1010067 - enum misspecification

------------------------------------------------------------------------
r1010248 | segato | 2009-08-12 00:01:09 +0000 (Wed, 12 Aug 2009) | 3 lines

backport r1010247
add app icon

------------------------------------------------------------------------
r1010331 | trueg | 2009-08-12 09:39:19 +0000 (Wed, 12 Aug 2009) | 1 line

backport: nicer naming scheme for duplicate names
------------------------------------------------------------------------
r1010396 | wstephens | 2009-08-12 11:27:25 +0000 (Wed, 12 Aug 2009) | 5 lines

Backport network interface bitrate reporting fixes:
1010394
1010395


------------------------------------------------------------------------
r1010416 | lukas | 2009-08-12 12:08:30 +0000 (Wed, 12 Aug 2009) | 8 lines

unbreak fish protocol by reverting r946444 (467 votes :)

Confirmed to work by #fedora-kde and dfaure in Bugzilla.

https://bugzilla.redhat.com/show_bug.cgi?id=516416

BUG: 189235

------------------------------------------------------------------------
r1010639 | scripty | 2009-08-13 03:16:08 +0000 (Thu, 13 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1010949 | trueg | 2009-08-13 16:30:45 +0000 (Thu, 13 Aug 2009) | 1 line

backport: create a real KComponentData instance
------------------------------------------------------------------------
r1010960 | trueg | 2009-08-13 16:44:09 +0000 (Thu, 13 Aug 2009) | 5 lines

Backport of the change from a local event loop in the main thread to usage of QWaitCondition. This change
is interwoven with my last commit which is, thus, also backported. But since it sort of "fixes" the handling
of pimo things that link to files it can be seen as a bugfix. :)
In any case, it will improve the searching experience in 4.3.1 a lot.

------------------------------------------------------------------------
r1011004 | freininghaus | 2009-08-13 18:56:42 +0000 (Thu, 13 Aug 2009) | 9 lines

Do not warn the user about closing Dolphin windows with multiple tabs
if Dolphin is closed by the session manager, i.e., if the user logs
out. The fix is inspired by the way this issue is handled in
Konqueror.

Fix will be in KDE 4.3.1.

CCBUG: 201803

------------------------------------------------------------------------
r1011059 | ppenz | 2009-08-13 20:51:42 +0000 (Thu, 13 Aug 2009) | 4 lines

Backport of SVN commit 1011008: Fix possible crash when opening Dolphin with an enabled Terminal. Thanks to
Dario Andres for the analyses!

CCBUG: 202953
------------------------------------------------------------------------
r1011075 | ppenz | 2009-08-13 21:40:26 +0000 (Thu, 13 Aug 2009) | 3 lines

Backport of SVN commit 1011074: Respect the maximum file size when creating thumbnails for directories.

CCBUG: 202960
------------------------------------------------------------------------
r1011079 | mpyne | 2009-08-13 21:54:48 +0000 (Thu, 13 Aug 2009) | 3 lines

Backport fix for incorrect loop in the KDED KCM to KDE 4.3.1.
Caught by Jaroslaw Staniek.

------------------------------------------------------------------------
r1011082 | mpyne | 2009-08-13 22:03:40 +0000 (Thu, 13 Aug 2009) | 6 lines

Backport fix for default handling in launch KCM to 4.3.1.

Issue noted by Jaroslaw Staniek (trunk commit was "fix pesky
warnings" since I didn't realize at first that the comparison
was actually buggy as well)

------------------------------------------------------------------------
r1011222 | scripty | 2009-08-14 03:20:16 +0000 (Fri, 14 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011341 | trueg | 2009-08-14 11:21:36 +0000 (Fri, 14 Aug 2009) | 1 line

Backport: show all results, never cut them due to their low score. Our scoring is very bad ATM so this can be seen as a bugfix
------------------------------------------------------------------------
r1011483 | ppenz | 2009-08-14 19:21:24 +0000 (Fri, 14 Aug 2009) | 5 lines

Backport of SVN commit 1011482: Assure that the thumbnails of directory previews don't exceed the cache size.
Thanks to Brendon Higgins for the detailed analyses.

BUG: 203512
CCBUG: 202960
------------------------------------------------------------------------
r1011496 | ppenz | 2009-08-14 20:38:06 +0000 (Fri, 14 Aug 2009) | 9 lines

Backport of SVN commit 1011495:

- Fixed issue that the items inside the "Other" category are sorted in a wrong
manner.

- Removed redundant code.

CCBUG: 173027

------------------------------------------------------------------------
r1011559 | scripty | 2009-08-15 03:04:50 +0000 (Sat, 15 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011799 | mjansen | 2009-08-15 21:04:30 +0000 (Sat, 15 Aug 2009) | 3 lines

Enable actions added over dbus automatically.

CCBUG: 170455
------------------------------------------------------------------------
r1011848 | scripty | 2009-08-16 03:14:25 +0000 (Sun, 16 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011958 | mjansen | 2009-08-16 13:10:41 +0000 (Sun, 16 Aug 2009) | 1 line

Fix an dbus related crash.
------------------------------------------------------------------------
r1011959 | mjansen | 2009-08-16 13:10:44 +0000 (Sun, 16 Aug 2009) | 3 lines

Fix typo in assertion :(. Now i can see the problem.

CCBUG: 202684
------------------------------------------------------------------------
r1011960 | mjansen | 2009-08-16 13:10:46 +0000 (Sun, 16 Aug 2009) | 8 lines

BUG: 202684
BUG: 203543

The bug was introduced by the reactivation of accelerator manager.

  "Plasma Workspace" != "&Plasma Workspace" :(

The export/import works again.
------------------------------------------------------------------------
r1012079 | darioandres | 2009-08-16 18:48:01 +0000 (Sun, 16 Aug 2009) | 5 lines

Backport to 4.3 of:
- Fix a bug when searching for duplicates: if we use the date for today as the end limit, Bugzilla
  will not retrieve the bug reports submitted today. We should use the "Now" word.


------------------------------------------------------------------------
r1012114 | huftis | 2009-08-16 20:25:48 +0000 (Sun, 16 Aug 2009) | 1 line

Write information about which weather provider each string comes from, and greatly simplify and generalise the extraction code.
------------------------------------------------------------------------
r1012232 | huftis | 2009-08-17 08:01:42 +0000 (Mon, 17 Aug 2009) | 1 line

Removed file removal and empty lines.
------------------------------------------------------------------------
r1012484 | dfaure | 2009-08-17 17:58:59 +0000 (Mon, 17 Aug 2009) | 7 lines

Repair old profiles without tabs, so that they create a tabwidget. Otherwise it gets
created automatically, but without any view in it, and nothing works
(debug build: konq asserts, release build: konq just shows an empty unusable gray window).

With unit test. Fix will be in 4.3.1.
BUG: 201366

------------------------------------------------------------------------
r1012549 | mart | 2009-08-17 20:10:25 +0000 (Mon, 17 Aug 2009) | 4 lines

don't try to alter the size if the icon isn't square, wouldn't look
better anyways
BUG:200178

------------------------------------------------------------------------
r1012788 | scripty | 2009-08-18 03:20:02 +0000 (Tue, 18 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1012916 | huftis | 2009-08-18 11:48:19 +0000 (Tue, 18 Aug 2009) | 1 line

Add line numbers to source references.
------------------------------------------------------------------------
r1013271 | pdamsten | 2009-08-19 12:32:30 +0000 (Wed, 19 Aug 2009) | 2 lines

Fix layout issues.
BUG:200397
------------------------------------------------------------------------
r1013297 | graesslin | 2009-08-19 13:41:09 +0000 (Wed, 19 Aug 2009) | 3 lines

Backport rev 1004096: Copy decoration pixmaps to Deleted. By that we see the decoration also when having a fade out animation.
CCBUG: 201780

------------------------------------------------------------------------
r1013298 | graesslin | 2009-08-19 13:42:35 +0000 (Wed, 19 Aug 2009) | 3 lines

Backport rev 1004121: Reimplement addRepaintFull() in Client and add the padding to the repaint region.
CCBUG: 201596

------------------------------------------------------------------------
r1013299 | graesslin | 2009-08-19 13:44:02 +0000 (Wed, 19 Aug 2009) | 3 lines

Backport rev 1004690: When we have a shaded window we have to create the four decoration quads manually. For each deco pixmap there has to be one quad of same size.
CCBUG: 195593

------------------------------------------------------------------------
r1013300 | graesslin | 2009-08-19 13:45:10 +0000 (Wed, 19 Aug 2009) | 1 line

Backport rev 1006788: Reset Repaints on a Toplevel has to include the decoration shadows if used.
------------------------------------------------------------------------
r1013370 | trueg | 2009-08-19 18:50:50 +0000 (Wed, 19 Aug 2009) | 1 line

Backport: do nothing if nepomuk is disabled
------------------------------------------------------------------------
r1013434 | darioandres | 2009-08-19 20:51:20 +0000 (Wed, 19 Aug 2009) | 3 lines

Added "kpilotDaemon" and "akonadi_gcal_resource" to mappings file


------------------------------------------------------------------------
r1013728 | freininghaus | 2009-08-20 15:48:23 +0000 (Thu, 20 Aug 2009) | 5 lines

Use KApplication::sessionSaving() to determine if Dolphin is closed by
the session manager - there's no need to reinvent the wheel ;-)

Thanks to Lubos Lunak for the hint.

------------------------------------------------------------------------
r1013730 | pino | 2009-08-20 15:49:05 +0000 (Thu, 20 Aug 2009) | 2 lines

be sure that the workspace subdir exists as well, before deciding kworkspace is part of the sources being compiled or an external package to search

------------------------------------------------------------------------
r1013764 | trueg | 2009-08-20 18:14:37 +0000 (Thu, 20 Aug 2009) | 1 line

Backport: Fixed moving metadata for moved folders: nie:url is an actual url, not only a path
------------------------------------------------------------------------
r1013901 | graesslin | 2009-08-21 07:08:01 +0000 (Fri, 21 Aug 2009) | 3 lines

Backport r1013898: Only show cross cursor in desktop grid when clicking on a movable window.
CCBUG: 204571

------------------------------------------------------------------------
r1013913 | ogoffart | 2009-08-21 07:47:45 +0000 (Fri, 21 Aug 2009) | 7 lines

Backport r1013912.

Do not try to load the sound file if we are not going to play a sound. 
And close the Notification right away



------------------------------------------------------------------------
r1014151 | trueg | 2009-08-21 17:05:42 +0000 (Fri, 21 Aug 2009) | 1 line

backport: fixed regression from last fix
------------------------------------------------------------------------
r1014152 | trueg | 2009-08-21 17:09:10 +0000 (Fri, 21 Aug 2009) | 1 line

backport: fixed regression from last fix properly
------------------------------------------------------------------------
r1014241 | scripty | 2009-08-22 03:25:42 +0000 (Sat, 22 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1014371 | anagl | 2009-08-22 16:19:09 +0000 (Sat, 22 Aug 2009) | 4 lines

Backporting r1014370:
Include also systems with a FreeBSD kernel.
CCMAIL: Petr.Salinger@seznam.cz

------------------------------------------------------------------------
r1014431 | mart | 2009-08-22 20:08:08 +0000 (Sat, 22 Aug 2009) | 2 lines

backport fix for 203058

------------------------------------------------------------------------
r1014493 | scripty | 2009-08-23 03:26:53 +0000 (Sun, 23 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1014751 | trueg | 2009-08-23 18:08:30 +0000 (Sun, 23 Aug 2009) | 2 lines

Backport: fixed deleteEntries query which fixes removal of metadata for removed and changed files.

------------------------------------------------------------------------
r1014779 | mart | 2009-08-23 19:05:03 +0000 (Sun, 23 Aug 2009) | 2 lines

backport the color role fix

------------------------------------------------------------------------
r1014784 | mart | 2009-08-23 19:16:41 +0000 (Sun, 23 Aug 2009) | 2 lines

backport the ligher lines

------------------------------------------------------------------------
r1014785 | darioandres | 2009-08-23 19:18:05 +0000 (Sun, 23 Aug 2009) | 7 lines

Backport to 4.3 of:

- Update the text of the icon (if we are not in a panel) when setUrl is called, on:
  - .desktop file updated
  - icon initialization (removed duplicate call)


------------------------------------------------------------------------
r1014811 | aacid | 2009-08-23 20:55:38 +0000 (Sun, 23 Aug 2009) | 2 lines

fix grec shortcut

------------------------------------------------------------------------
r1014867 | scripty | 2009-08-24 03:31:44 +0000 (Mon, 24 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1014912 | vlanz | 2009-08-24 08:17:14 +0000 (Mon, 24 Aug 2009) | 5 lines

backport to 4.3 branch

CCBUG: 204745


------------------------------------------------------------------------
r1015067 | segato | 2009-08-24 12:26:37 +0000 (Mon, 24 Aug 2009) | 4 lines

backport commit r1015063
disable edit/add/remove buttons if the user doesn't have permission to edit the emoticon theme
CCBUG:201488

------------------------------------------------------------------------
r1015296 | aseigo | 2009-08-25 07:58:42 +0000 (Tue, 25 Aug 2009) | 6 lines

pay attention to multi-monitor issues; should really be using the screen geometry calls in Corona or Kephal if no Corona dependency is wanted, but this is at least an improvement.

thanks to Michal for working on debugging this
CCMAIL:michalodstrcil@gmail.com
CCBUG:189282

------------------------------------------------------------------------
r1015363 | cfeck | 2009-08-25 11:16:12 +0000 (Tue, 25 Aug 2009) | 4 lines

Use QPainter to render preview (backport r1015361)

CCBUG: 203842

------------------------------------------------------------------------
r1015367 | carewolf | 2009-08-25 11:36:30 +0000 (Tue, 25 Aug 2009) | 2 lines

Hint the pkgconfig found paths to find_path and find_library

------------------------------------------------------------------------
r1015384 | aseigo | 2009-08-25 12:10:24 +0000 (Tue, 25 Aug 2009) | 3 lines

make Add Widget the default
BUG:205072

------------------------------------------------------------------------
r1015421 | mart | 2009-08-25 13:50:07 +0000 (Tue, 25 Aug 2009) | 2 lines

backport the fix of dynamic margins broken with revision 990711

------------------------------------------------------------------------
r1015441 | pdamsten | 2009-08-25 14:24:54 +0000 (Tue, 25 Aug 2009) | 2 lines

Backport graph color fix.
CCBUG: 200283
------------------------------------------------------------------------
r1015507 | lueck | 2009-08-25 17:52:00 +0000 (Tue, 25 Aug 2009) | 3 lines

load the translations from dolphin catalog, so the kcm's are translated
no matter if launched by dolphin, konqueror or via kcmshell4
CCBUG:204859
------------------------------------------------------------------------
r1015539 | tdickenson | 2009-08-25 19:46:53 +0000 (Tue, 25 Aug 2009) | 1 line

Backport. Constrain the middle of a window following a step-change (such as maximize or restore) so that any simulation asymetry will not cause the window to drift off-center. This previously led to problems with the wobblyness set to maximum. BUG: 198559
------------------------------------------------------------------------
r1015550 | mart | 2009-08-25 20:40:21 +0000 (Tue, 25 Aug 2009) | 2 lines

backport scrollbar fix

------------------------------------------------------------------------
r1015639 | scripty | 2009-08-26 02:50:42 +0000 (Wed, 26 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1015801 | luebking | 2009-08-26 11:13:08 +0000 (Wed, 26 Aug 2009) | 3 lines

backport 1002997, make opacity rules actually do something
CCBUG: 167138

------------------------------------------------------------------------
r1015945 | aacid | 2009-08-26 16:46:26 +0000 (Wed, 26 Aug 2009) | 5 lines

Backport r1015943 | aacid | 2009-08-26 18:44:54 +0200 (Wed, 26 Aug 2009) | 3 lines

Honor Charset key in web shortcuts
acked by dfaure

------------------------------------------------------------------------
r1016089 | fredrik | 2009-08-27 00:06:39 +0000 (Thu, 27 Aug 2009) | 1 line

Backport r1016088.
------------------------------------------------------------------------
r1016198 | scripty | 2009-08-27 03:36:05 +0000 (Thu, 27 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1016224 | huynhhuu | 2009-08-27 07:52:46 +0000 (Thu, 27 Aug 2009) | 2 lines

Revert my latest backport (see r1009031) as it has side-effects (e.g. regarding window snapping)

------------------------------------------------------------------------
r1016318 | dfaure | 2009-08-27 14:17:45 +0000 (Thu, 27 Aug 2009) | 2 lines

Backport: fix crash when doing "Close all tabs". #183486.

------------------------------------------------------------------------
r1016350 | dfaure | 2009-08-27 15:20:51 +0000 (Thu, 27 Aug 2009) | 3 lines

respect the user's wish and don't append .desktop to the filename. Fix will be in 4.3.1
BUG: 197840

------------------------------------------------------------------------
r1016410 | aseigo | 2009-08-27 19:15:48 +0000 (Thu, 27 Aug 2009) | 2 lines

only activate if the mouse up happens in the item

------------------------------------------------------------------------
r1016413 | aseigo | 2009-08-27 19:17:22 +0000 (Thu, 27 Aug 2009) | 2 lines

paint the subtext beneath the title, even if it wraps

------------------------------------------------------------------------
r1016449 | mart | 2009-08-27 20:24:37 +0000 (Thu, 27 Aug 2009) | 2 lines

backport of the (potential) fix for bug 202587

------------------------------------------------------------------------
r1016500 | dfaure | 2009-08-27 23:01:40 +0000 (Thu, 27 Aug 2009) | 2 lines

backport: fix memory leak in KonqClosedWindowsManager

------------------------------------------------------------------------
r1016503 | jacopods | 2009-08-27 23:10:21 +0000 (Thu, 27 Aug 2009) | 2 lines

Fadeout effect is now sharper

------------------------------------------------------------------------
