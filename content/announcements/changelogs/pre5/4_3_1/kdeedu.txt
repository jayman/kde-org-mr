------------------------------------------------------------------------
r1004236 | rahn | 2009-07-29 17:02:45 +0000 (Wed, 29 Jul 2009) | 5 lines

- Adjusting variable names
- Adding a missing icon
- Adding a link to the map site to MarbleQt.


------------------------------------------------------------------------
r1004269 | sengels | 2009-07-29 18:31:53 +0000 (Wed, 29 Jul 2009) | 1 line

fix nsi installer - attention it needs some more editing before being usable
------------------------------------------------------------------------
r1004392 | scripty | 2009-07-30 03:13:24 +0000 (Thu, 30 Jul 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1004706 | ariya | 2009-07-30 18:49:44 +0000 (Thu, 30 Jul 2009) | 2 lines

Fix potential divide-by-zero, backported from r1004705.

------------------------------------------------------------------------
r1005821 | cniehaus | 2009-08-02 09:37:47 +0000 (Sun, 02 Aug 2009) | 6 lines

Backport 1005817

"Make the code show random elements, not always Bromine."



------------------------------------------------------------------------
r1006347 | sengels | 2009-08-03 14:31:24 +0000 (Mon, 03 Aug 2009) | 1 line

correct the define
------------------------------------------------------------------------
r1006573 | mueller | 2009-08-03 22:37:57 +0000 (Mon, 03 Aug 2009) | 2 lines

compile fixes for freebsd/netbsd

------------------------------------------------------------------------
r1006583 | segato | 2009-08-03 23:05:36 +0000 (Mon, 03 Aug 2009) | 3 lines

backport r986538
use namespace std with mingw, this works both with gcc 3 and 4

------------------------------------------------------------------------
r1006868 | lueck | 2009-08-04 16:20:13 +0000 (Tue, 04 Aug 2009) | 1 line

add missing i18n call to make the string translatable
------------------------------------------------------------------------
r1007387 | gladhorn | 2009-08-05 17:35:53 +0000 (Wed, 05 Aug 2009) | 1 line

backport qDeleteAll(m_allTestEntries);
------------------------------------------------------------------------
r1010443 | kbal | 2009-08-12 13:32:02 +0000 (Wed, 12 Aug 2009) | 3 lines

Change tr() to i18n() to make translatable. It adds two new strings to the stable branch.
Torsten will fix the rest.
CCMAIL: kde-i18n-doc@kde.org
------------------------------------------------------------------------
r1010457 | kbal | 2009-08-12 14:45:32 +0000 (Wed, 12 Aug 2009) | 3 lines

Fix extracting of Qt messages and tr()->i18n() in marble_part.
This will add two times three (identical) messages in marble.pot en marble_qt.pot.
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r1010941 | gladhorn | 2009-08-13 16:23:17 +0000 (Thu, 13 Aug 2009) | 2 lines

backport -r 1010937 removing first languages crashes

------------------------------------------------------------------------
r1011613 | habacker | 2009-08-15 09:36:35 +0000 (Sat, 15 Aug 2009) | 1 line

backported r1011607 from trunk: win32 fix: for local files toLocalFile() has to be used, see http://doc.trolltech.com/4.5/qurl.html#toLocalFile
------------------------------------------------------------------------
r1012436 | whiting | 2009-08-17 17:07:18 +0000 (Mon, 17 Aug 2009) | 2 lines

BUG: 203783
backport bugfix to 4.3 branch
------------------------------------------------------------------------
r1012792 | scripty | 2009-08-18 03:21:46 +0000 (Tue, 18 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013451 | jmhoffmann | 2009-08-19 22:50:49 +0000 (Wed, 19 Aug 2009) | 3 lines

Backport fix from svn revision 1013450:
Add user agent also in QNAM network plugin.

------------------------------------------------------------------------
r1013703 | nielsslot | 2009-08-20 13:58:43 +0000 (Thu, 20 Aug 2009) | 3 lines

Backport revision 1013702.
Disable the execute console action when running a program

------------------------------------------------------------------------
r1015334 | gladhorn | 2009-08-25 10:26:30 +0000 (Tue, 25 Aug 2009) | 2 lines

backport 1015333 - mkdir in py would fail because of wrong import

------------------------------------------------------------------------
r1015504 | jmhoffmann | 2009-08-25 17:42:29 +0000 (Tue, 25 Aug 2009) | 4 lines

Fix crash in case no network plugin was found,
fixed in trunk already with svn revision 1005467.
BUG: 201995

------------------------------------------------------------------------
r1015555 | jmhoffmann | 2009-08-25 20:53:59 +0000 (Tue, 25 Aug 2009) | 2 lines

Set version to "0.8.1".

------------------------------------------------------------------------
r1015558 | jmhoffmann | 2009-08-25 21:12:27 +0000 (Tue, 25 Aug 2009) | 3 lines

Set user agent string to something more reasonable, also in preparation
for different use cases like "bulk download" or "viewing".

------------------------------------------------------------------------
r1016052 | jmhoffmann | 2009-08-26 21:33:04 +0000 (Wed, 26 Aug 2009) | 2 lines

Bump MARBLE_VERSION for KDE 4.3.1.

------------------------------------------------------------------------
r1016077 | rahn | 2009-08-26 22:16:25 +0000 (Wed, 26 Aug 2009) | 3 lines

Preliminary fix for Bug #202375


------------------------------------------------------------------------
r1016469 | hedlund | 2009-08-27 21:13:44 +0000 (Thu, 27 Aug 2009) | 2 lines

Backport. Fix checking answer in multiple choice.

------------------------------------------------------------------------
