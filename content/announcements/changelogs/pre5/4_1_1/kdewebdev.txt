------------------------------------------------------------------------
r838626 | mojo | 2008-07-28 13:01:47 +0200 (Mon, 28 Jul 2008) | 1 line

backport #167475: Send configured user agent in HTTP header.
------------------------------------------------------------------------
r838663 | mojo | 2008-07-28 14:34:23 +0200 (Mon, 28 Jul 2008) | 1 line

Backport: Load schedule configurations even if they have an empty name
------------------------------------------------------------------------
r838734 | mojo | 2008-07-28 18:32:33 +0200 (Mon, 28 Jul 2008) | 2 lines

Backport:
Adjust to the latest change in revision 838725
------------------------------------------------------------------------
r839460 | scripty | 2008-07-30 07:35:48 +0200 (Wed, 30 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r840363 | mueller | 2008-08-01 00:52:44 +0200 (Fri, 01 Aug 2008) | 1 line

fix link reduction fallout
------------------------------------------------------------------------
r840416 | scripty | 2008-08-01 07:03:40 +0200 (Fri, 01 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r843998 | mojo | 2008-08-08 13:11:11 +0200 (Fri, 08 Aug 2008) | 6 lines

Backport:

- BUG: don't let the search engine jobs run (in new thread) if the search was already cancelled
- BUG: check status bar reference to avoid crash on exit


------------------------------------------------------------------------
r844663 | scripty | 2008-08-10 07:04:47 +0200 (Sun, 10 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r846108 | mojo | 2008-08-13 02:27:33 +0200 (Wed, 13 Aug 2008) | 4 lines

Backport: 

Send referer in HTTP header

------------------------------------------------------------------------
