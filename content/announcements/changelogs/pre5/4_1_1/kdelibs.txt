------------------------------------------------------------------------
r836885 | mueller | 2008-07-23 10:46:44 +0200 (Wed, 23 Jul 2008) | 2 lines

update version number

------------------------------------------------------------------------
r837147 | neundorf | 2008-07-24 00:00:12 +0200 (Thu, 24 Jul 2008) | 6 lines

-make the reduced link interface also available under Windows, sync with trunk

Alex



------------------------------------------------------------------------
r837228 | lunakl | 2008-07-24 11:42:49 +0200 (Thu, 24 Jul 2008) | 4 lines

The KDE_FULL_SESSION property is always on root window of screen 0.
BUG: 166598


------------------------------------------------------------------------
r837460 | mueller | 2008-07-24 20:33:05 +0200 (Thu, 24 Jul 2008) | 1 line

link kparts for ktexteditor
------------------------------------------------------------------------
r837850 | uwolfer | 2008-07-25 23:17:20 +0200 (Fri, 25 Jul 2008) | 5 lines

Backport:
SVN commit 837849 by uwolfer:

Replace assert with retern, this is a case which happens quite often in KGet when downloads get deleted.
Okay'ed by maelcum.
------------------------------------------------------------------------
r837857 | uwolfer | 2008-07-25 23:39:26 +0200 (Fri, 25 Jul 2008) | 4 lines

Backport:
SVN commit 837856 by uwolfer:

better fix suggested by mjansen
------------------------------------------------------------------------
r838117 | ereslibre | 2008-07-26 21:20:45 +0200 (Sat, 26 Jul 2008) | 2 lines

Backport of fix

------------------------------------------------------------------------
r838208 | ossi | 2008-07-27 10:01:35 +0200 (Sun, 27 Jul 2008) | 5 lines

backport: mark KConfig c'tor taking a backend name as internal, as it is
kind of half-baked.
a bit late for 4.1, but it should not hurt given that noone can write
backends due to the backend api headers not being installed.

------------------------------------------------------------------------
r838499 | segato | 2008-07-28 02:01:08 +0200 (Mon, 28 Jul 2008) | 3 lines

backport of commit 838497
only add emoticons with a non empty text

------------------------------------------------------------------------
r838505 | orlovich | 2008-07-28 02:51:37 +0200 (Mon, 28 Jul 2008) | 10 lines

Fix a bunch of issues with sub-cursor link type indicator:
1) Don't make it all black --- seems like Qt doesn't like texture brushes anymore.
2) Fix mask setting --- default mask constructions cuts away all pixels with non-0xff alpha.
And no, this isn't less efficient --- ::mask recomputes all the time anyway.
3) Actually change the icon when needed. As a bonus, we don't bug kiconloader all the time while the cursor is wiggling 
over a link

Also, be evil, and sanify the variable name. 
BUG: 167512  

------------------------------------------------------------------------
r838695 | ahartmetz | 2008-07-28 16:33:10 +0200 (Mon, 28 Jul 2008) | 7 lines

Forwardport of r838686 from trunk

Suppress artifacts when scrolling quickly (or just scrolling, depending on
your particular setup) in a view with open extenders. Extenders sometimes
stayed in the viewport when their items were gone.


------------------------------------------------------------------------
r838712 | orlovich | 2008-07-28 17:31:00 +0200 (Mon, 28 Jul 2008) | 6 lines

Don't crash on navigating away when a suppressed popup 
comes from a child frame which in turn got navigated away,
deleting the part the m_suppressedPopupOriginParts list is referring to.

BUG:167514

------------------------------------------------------------------------
r839036 | ervin | 2008-07-29 12:20:06 +0200 (Tue, 29 Jul 2008) | 3 lines

Oh, this one was hard to spot. If anyone wondered why the configure
dialog tree in kontact was messy... that was why. :-)

------------------------------------------------------------------------
r839199 | ervin | 2008-07-29 15:48:22 +0200 (Tue, 29 Jul 2008) | 5 lines

Allow to mark plugins as "immutable" and force KSettings::Dialog to
respect that. This way it's now possible to have components the user
can't disable (for instance in Kontact it doesn't make sense to disable
the mail component).

------------------------------------------------------------------------
r839227 | mkretz | 2008-07-29 17:14:21 +0200 (Tue, 29 Jul 2008) | 2 lines

backport: when using cmake 2.6.0 make sure it's using automoc 0.9.84 or later

------------------------------------------------------------------------
r839453 | scripty | 2008-07-30 07:33:10 +0200 (Wed, 30 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r839700 | orlovich | 2008-07-30 18:02:49 +0200 (Wed, 30 Jul 2008) | 7 lines

Now that the HTML parser adds doctype nodes, it can no longer be sure that the
<html> will be the first child of #document, so use ->documentElement() instead.
Restores recovery from mis-structured head/body, w/o which we could even lose 
<style> or <title> elements in some cases of malformed input
BUG:166450
BUG:167717 

------------------------------------------------------------------------
r839899 | scripty | 2008-07-31 06:50:00 +0200 (Thu, 31 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r839978 | ervin | 2008-07-31 11:35:37 +0200 (Thu, 31 Jul 2008) | 4 lines

Allow to list components which have no settings in KSettings::Dialog.
For instance this way we'll be able disable the knotes component of
kontact again.

------------------------------------------------------------------------
r840047 | ervin | 2008-07-31 12:02:01 +0200 (Thu, 31 Jul 2008) | 2 lines

Missing constness.

------------------------------------------------------------------------
r840288 | mueller | 2008-07-31 22:14:23 +0200 (Thu, 31 Jul 2008) | 1 line

merge link interface lib definitions
------------------------------------------------------------------------
r840377 | ereslibre | 2008-08-01 02:14:54 +0200 (Fri, 01 Aug 2008) | 4 lines

When the tagging was done, the problematic patch did get into. This is a backport of the fix for this problem. This commit has been into 4.2 (trunk) for several days now, so I am backporting it.

BUG: 167826

------------------------------------------------------------------------
r840379 | ereslibre | 2008-08-01 02:18:28 +0200 (Fri, 01 Aug 2008) | 2 lines

Ooops, forgot to commit here too

------------------------------------------------------------------------
r840463 | habacker | 2008-08-01 10:29:42 +0200 (Fri, 01 Aug 2008) | 1 line

merged win32 fixes from trunk
------------------------------------------------------------------------
r840622 | lueck | 2008-08-01 13:32:24 +0200 (Fri, 01 Aug 2008) | 1 line

backport from trunk
------------------------------------------------------------------------
r840916 | ossi | 2008-08-02 10:17:07 +0200 (Sat, 02 Aug 2008) | 2 lines

backport 840913: don't break on trivial case

------------------------------------------------------------------------
r841067 | ilic | 2008-08-02 16:42:56 +0200 (Sat, 02 Aug 2008) | 1 line

Update standard help texts.
------------------------------------------------------------------------
r841089 | orlovich | 2008-08-02 17:57:48 +0200 (Sat, 02 Aug 2008) | 3 lines

Handle text-overflow here
BUG:168094

------------------------------------------------------------------------
r841092 | orlovich | 2008-08-02 18:02:00 +0200 (Sat, 02 Aug 2008) | 2 lines

Let's keep this in alphabetical order..

------------------------------------------------------------------------
r841120 | lueck | 2008-08-02 19:07:27 +0200 (Sat, 02 Aug 2008) | 1 line

add 'Switch Application Language' item to the help menu in the docs
------------------------------------------------------------------------
r841276 | ilic | 2008-08-03 00:46:55 +0200 (Sun, 03 Aug 2008) | 3 lines

Allow languages to have number formatting according to system locale,
instead of to the language of current message. (bport: 841274)

------------------------------------------------------------------------
r841286 | ilic | 2008-08-03 02:08:17 +0200 (Sun, 03 Aug 2008) | 1 line

Handle transliterated languages also when collecting semantic patterns. (bport: 841285)
------------------------------------------------------------------------
r841305 | scripty | 2008-08-03 06:48:06 +0200 (Sun, 03 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r841613 | orlovich | 2008-08-03 19:05:27 +0200 (Sun, 03 Aug 2008) | 10 lines

Multiple fixes for encoding detection:
1) Update the encoding type when re-setting to the same codec. 
This is relevant if we have a default that matches an explicitly specified encoding 
which we should trust. Fixes the frame in that Greek banking site that kfm-devel was e-mailed about
2) HTTP encoding is preferable over meta! (#158663 comment #7, part of #156774)
3) Always use the entire buffer for autodetection --- fixes intermittent failures in encoding detection (part of #156774)

CCBUG: 158663
BUG:   156774

------------------------------------------------------------------------
r841625 | orlovich | 2008-08-03 19:40:31 +0200 (Sun, 03 Aug 2008) | 3 lines

Don't mess up unicode in javascript: URIs; fromPercentEncoding likes utf-8, so feed it some.
Fixes some of the trees on that Greek banking website.

------------------------------------------------------------------------
r841962 | mutz | 2008-08-04 12:50:43 +0200 (Mon, 04 Aug 2008) | 3 lines

Only emit changed(false) on first show.
Fixes the Apply button in KCMultiDialog being grayed out in this situation:
Change Setting on Page A -> Page B -> Page A => Apply button grayed out.
------------------------------------------------------------------------
r842007 | ervin | 2008-08-04 15:15:04 +0200 (Mon, 04 Aug 2008) | 3 lines

Expand the items first, before computing the width we need. Otherwise
the view won't be wide enough for all the content.

------------------------------------------------------------------------
r842176 | tmcguire | 2008-08-04 22:29:55 +0200 (Mon, 04 Aug 2008) | 10 lines

Backport r842134 by tmcguire to the 4.1 branch:

Make sure KGlobal::deref() is always called, even when s subclass overrides
closeEvent().

This fixes KMail not exiting when, for example, the composer was opened.

CCBUG: 167745


------------------------------------------------------------------------
r842272 | scripty | 2008-08-05 06:52:43 +0200 (Tue, 05 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r842307 | apaku | 2008-08-05 10:11:56 +0200 (Tue, 05 Aug 2008) | 3 lines

Backport r842305:
Fix my earlier mistake, Debugfull should be enabled for anything greater 2.5 (i.e. 2.6.0 and onwards)

------------------------------------------------------------------------
r842350 | osterfeld | 2008-08-05 11:47:58 +0200 (Tue, 05 Aug 2008) | 1 line

typo (backport of 842348)
------------------------------------------------------------------------
r842384 | lunakl | 2008-08-05 12:01:56 +0200 (Tue, 05 Aug 2008) | 3 lines

Avoid kruler crash when reading out of the screen.


------------------------------------------------------------------------
r842620 | ianjo | 2008-08-05 18:43:35 +0200 (Tue, 05 Aug 2008) | 5 lines

Backport fix from trunk revision 837374 to branch:
Use a KSqueezedTextLabel instead of a QLabel for showing the current url,
so that the dialog won't become very wide and unresizable (on www.gmail.com for example).


------------------------------------------------------------------------
r843224 | pino | 2008-08-06 20:58:53 +0200 (Wed, 06 Aug 2008) | 8 lines

Backport SVN commit 843223 by pino:

Use the right cmake variable for the data directory, so the policy file points to the correct directory.
The mismatch may happen in case a particular datadir is set, eg by some distro.

This may fix bug #161825.
CCBUG: 161825

------------------------------------------------------------------------
r843313 | aacid | 2008-08-07 00:38:19 +0200 (Thu, 07 Aug 2008) | 4 lines

Backport r838837
Add some kuit markers
Acked by translators

------------------------------------------------------------------------
r843320 | orlovich | 2008-08-07 01:10:36 +0200 (Thu, 07 Aug 2008) | 5 lines

Proper parsing and RenderStyle construction for clip:rect(..  auto ...).
RenderBox part still needs fixing, but lets get this into SVN so I don't 
lose it.
CCBUG:166830

------------------------------------------------------------------------
r843323 | orlovich | 2008-08-07 01:19:44 +0200 (Thu, 07 Aug 2008) | 12 lines

Fix the deterministic leak on websites such as google maps (and some other ones)
which also causes an assertion failure on exit.

There is still a more subtle one where the conservative collection keeps an object alive for 
a while. Happens e.g. for #164126. That one needs more thought...

CCBUG:156172
CCBUG:161289
CCBUG:151453
BUG:167354


------------------------------------------------------------------------
r843330 | orlovich | 2008-08-07 01:30:19 +0200 (Thu, 07 Aug 2008) | 4 lines

Remove some pointless refcount thrashing in some hot methods.
This is theoretically a bit of a speedup, but I am really 
doing this because it makes serious leak debugging easier.

------------------------------------------------------------------------
r843791 | orlovich | 2008-08-08 00:02:27 +0200 (Fri, 08 Aug 2008) | 8 lines

- Add ability to rewrite attributes on their parsing.
This required making sure that if we have an AttrImpl it still gets passed down 
to parseAttribute, hence the changes in the overloads for that
- Use that ability to fix border=00 on tables, as suggested by Germain.
It seems to not always restyle properly when border attributed changed dynamically, though,
hence keeping this open.
CCBUG:167567

------------------------------------------------------------------------
r844383 | ereslibre | 2008-08-09 17:13:50 +0200 (Sat, 09 Aug 2008) | 4 lines

Backport of crash fix.

CCBUG: 168038

------------------------------------------------------------------------
r844825 | ereslibre | 2008-08-10 18:14:48 +0200 (Sun, 10 Aug 2008) | 2 lines

Backport of commit: Set the text to the drag object so you can drop the url. Contributed by Alex Fiestas (alex@eyeos.org).

------------------------------------------------------------------------
r844830 | dfaure | 2008-08-10 18:48:42 +0200 (Sun, 10 Aug 2008) | 3 lines

A cookie with empty domain and empty host (leftover from kde3 in the cookies file) would crash on loading, as reported by Kevin Kofler.
CCMAIL: kevin.kofler@chello.at

------------------------------------------------------------------------
r844907 | ereslibre | 2008-08-10 23:40:35 +0200 (Sun, 10 Aug 2008) | 2 lines

Backport of fix of misusage

------------------------------------------------------------------------
r845064 | dfaure | 2008-08-11 10:05:32 +0200 (Mon, 11 Aug 2008) | 2 lines

Fix crash when skipping a source URL in the direct-renaming phase (#157601)

------------------------------------------------------------------------
r845252 | dhaumann | 2008-08-11 13:06:38 +0200 (Mon, 11 Aug 2008) | 2 lines

backport: clearing the cache now works again. makes toggling dynamic word wrap work again.

------------------------------------------------------------------------
r845293 | mlaurent | 2008-08-11 14:40:40 +0200 (Mon, 11 Aug 2008) | 3 lines

Backport:
Don't hardcode color (bug reported by darktears)

------------------------------------------------------------------------
r845330 | dfaure | 2008-08-11 15:41:10 +0200 (Mon, 11 Aug 2008) | 3 lines

more output in the error message when automoc4config.cmake isn't found.
Too bad this doesn't apply to trunk anymore...

------------------------------------------------------------------------
r845369 | alexmerry | 2008-08-11 16:59:35 +0200 (Mon, 11 Aug 2008) | 3 lines

Backport r845368: respect work path when running applications in Konsole


------------------------------------------------------------------------
r845385 | ereslibre | 2008-08-11 17:23:50 +0200 (Mon, 11 Aug 2008) | 2 lines

Backport of rendering fix (highlight/selected row) was being cut off.

------------------------------------------------------------------------
r845792 | gladhorn | 2008-08-12 14:01:57 +0200 (Tue, 12 Aug 2008) | 2 lines

Backport r845780 to let kdiroperator lists take up the whole space available

------------------------------------------------------------------------
r846526 | orlovich | 2008-08-13 18:32:28 +0200 (Wed, 13 Aug 2008) | 6 lines

Fix handling of borders in clip:; particularly important 
if the length is auto. RTL handling left as following my reading
of the spec and not other browsers' behavior.

BUG:166830

------------------------------------------------------------------------
r846622 | orlovich | 2008-08-13 21:29:29 +0200 (Wed, 13 Aug 2008) | 3 lines

Streamline global puts for a ~5% speedup on hosted SunSpider, 
and for one more change competing for kjs_window.cpp

------------------------------------------------------------------------
r846672 | orlovich | 2008-08-13 22:42:31 +0200 (Wed, 13 Aug 2008) | 7 lines

Add support for making typeof() of calleable things not be 
"function" but "object" in KJS, and use it for the array-like
things that support the IE call notation in KHTML.

(Actually, one is still unchanged, but I am in process of removing it)
BUG: 139981

------------------------------------------------------------------------
r846709 | rpedersen | 2008-08-13 23:45:22 +0200 (Wed, 13 Aug 2008) | 2 lines

backport 846708

------------------------------------------------------------------------
r846733 | orlovich | 2008-08-14 01:57:45 +0200 (Thu, 14 Aug 2008) | 10 lines

Don't dispatch mouse events to disabled widgets. 
The current behavior matches FF since no event is produced, not even 
at capture or bubble spots.

Also, don't propagate the double-click of disabled widgets up;
but to we even want to do it for enabled ones? That code seems 
prehistoric and may be responsible for #165580

Still need to fix the palette of disabled buttons, though.

------------------------------------------------------------------------
r847605 | ilic | 2008-08-15 20:43:16 +0200 (Fri, 15 Aug 2008) | 1 line

Fix message extraction. (bport: 847604)
------------------------------------------------------------------------
r847753 | dfaure | 2008-08-16 11:18:52 +0200 (Sat, 16 Aug 2008) | 2 lines

SVN_SILENT --noise

------------------------------------------------------------------------
r847754 | dfaure | 2008-08-16 11:19:52 +0200 (Sat, 16 Aug 2008) | 3 lines

Don't insert empty strings into "sharedPaths" (when /etc/exports contains empty lines)
--noise

------------------------------------------------------------------------
r847758 | dfaure | 2008-08-16 11:24:02 +0200 (Sat, 16 Aug 2008) | 4 lines

Rewrite QTest::kWaitForSignal so that it doesn't return false even when the signal was emitted, due to a timeout after that (e.g. due to a gdb breakpoint). With unit test.

Add KGlobal::ref() in qtest_kde.h to fix konqhtmltest eventloops returning too early because the kmainwindow refcount stuff called qApp->quit after each window being created+closed.

------------------------------------------------------------------------
r847763 | dfaure | 2008-08-16 11:28:07 +0200 (Sat, 16 Aug 2008) | 2 lines

port to non-deprecated API (KPluginFactory)

------------------------------------------------------------------------
r848078 | orlovich | 2008-08-16 22:57:19 +0200 (Sat, 16 Aug 2008) | 5 lines

Make KJS just append to global object's scope chain 
and not reset it again; the reset broke KJSApi.

Do the proper breaking in KHTML instead.

------------------------------------------------------------------------
r848108 | gladhorn | 2008-08-17 01:36:18 +0200 (Sun, 17 Aug 2008) | 1 line

backport r848107 - missing i18n for standardactions
------------------------------------------------------------------------
r848170 | apaku | 2008-08-17 10:18:03 +0200 (Sun, 17 Aug 2008) | 2 lines

Add documentation for KDE_MIN_VERSION variable

------------------------------------------------------------------------
r848802 | lunakl | 2008-08-18 15:31:43 +0200 (Mon, 18 Aug 2008) | 3 lines

Replace XSetTransientForHint() with KWindowSystem::setMainWindow().


------------------------------------------------------------------------
r848852 | bjacob | 2008-08-18 17:04:57 +0200 (Mon, 18 Aug 2008) | 4 lines

backport FindEigen2.cmake. Since some apps like KOffice depend currently on 
4.1 libs, i've been told this was useful to backport.


------------------------------------------------------------------------
r848997 | staniek | 2008-08-18 22:55:08 +0200 (Mon, 18 Aug 2008) | 2 lines

missing initialization of curCounter

------------------------------------------------------------------------
r849396 | bjacob | 2008-08-19 15:35:25 +0200 (Tue, 19 Aug 2008) | 3 lines

SVN_SILENT backport minor fix


------------------------------------------------------------------------
r849438 | ervin | 2008-08-19 17:05:05 +0200 (Tue, 19 Aug 2008) | 3 lines

Be more defensive, this causes crashes on Windows, in some situations.
(Backporting revision 849212 by Till)

------------------------------------------------------------------------
r849439 | ervin | 2008-08-19 17:06:53 +0200 (Tue, 19 Aug 2008) | 4 lines

This moc file is already included by automoc. With this in here again, things
won't link, at least on OSX.
(Backporting revision 849437 by Till)

------------------------------------------------------------------------
r849454 | ervin | 2008-08-19 17:37:02 +0200 (Tue, 19 Aug 2008) | 3 lines

Initialized merge tracking via "svnmerge" with revisions "1-849447" from 
svn+ssh://svn.kde.org/home/kde/branches/kdepim/enterprise4/kdelibs-4.1-branch

------------------------------------------------------------------------
r849490 | ahartmetz | 2008-08-19 19:07:50 +0200 (Tue, 19 Aug 2008) | 5 lines

Backport of commit 849485 from trunk:
Revert commit 849437 by tilladam to fix the build (on dashboard, it seems to
work here). Coordinated with Till who will fix it again for his environment,
later.

------------------------------------------------------------------------
r849529 | ahartmetz | 2008-08-19 20:17:18 +0200 (Tue, 19 Aug 2008) | 3 lines

Forward port of commits 848992 and 849008 from trunk to fix SSL proxying if
the proxy requires authentication.

------------------------------------------------------------------------
r849659 | ilic | 2008-08-19 23:39:33 +0200 (Tue, 19 Aug 2008) | 1 line

The vertical scroll bar now reports width/height normally. (bport: 849658)
------------------------------------------------------------------------
r849755 | mueller | 2008-08-20 05:09:45 +0200 (Wed, 20 Aug 2008) | 2 lines

backport minimal part for fixing the kmail crashes (bug 165540)

------------------------------------------------------------------------
r849794 | grossard | 2008-08-20 09:48:10 +0200 (Wed, 20 Aug 2008) | 2 lines

added entity for translator Stanislas Zeller

------------------------------------------------------------------------
r849842 | huynhhuu | 2008-08-20 12:20:29 +0200 (Wed, 20 Aug 2008) | 5 lines

Backport of r849373:
"Fix content rect and label margins for vertical tabs
CCBUG: 168742"


------------------------------------------------------------------------
r850151 | mkretz | 2008-08-20 23:05:29 +0200 (Wed, 20 Aug 2008) | 5 lines

backport: I didn't realize HalAudioInterface needed the fix for handling the
incompatibility in HAL, too. Now, in order to find out what type of soundcard it
is, don't look at the direct parent device object but the parent of the parent
instead (for newer HAL only). Finally my USB device is detected as such again.

------------------------------------------------------------------------
r850486 | orlovich | 2008-08-21 18:40:31 +0200 (Thu, 21 Aug 2008) | 6 lines

Emulate a quirk of mozilla's content-type parsing and completely ignore content-type 
headers without a /, even for purposes of mimetype validation in strict mode.
Makes www.monosit.ro show up fine (I won't say "fixes it", since The Real Fix(tm)
would be for the server to be configured properly)
BUG:169523

------------------------------------------------------------------------
r850532 | ilic | 2008-08-21 19:50:10 +0200 (Thu, 21 Aug 2008) | 1 line

Disambiguate +/-/= as calculator ops (prevent clash with Qt's shortcut separator). (bport: 850530)
------------------------------------------------------------------------
r850881 | dfaure | 2008-08-22 15:24:16 +0200 (Fri, 22 Aug 2008) | 2 lines

Backport r850876. Fix symlink handling in FileProtocol::rename. BUG 169547.

------------------------------------------------------------------------
r850884 | dfaure | 2008-08-22 15:26:16 +0200 (Fri, 22 Aug 2008) | 2 lines

more friendly for our windows colleagues

------------------------------------------------------------------------
r850949 | orlovich | 2008-08-22 18:21:07 +0200 (Fri, 22 Aug 2008) | 3 lines

Fix problems with disabled palette generation, in particular 
filling in all the foreground entries, and fixing the guard in the fallback case...

------------------------------------------------------------------------
r850972 | orlovich | 2008-08-22 19:37:56 +0200 (Fri, 22 Aug 2008) | 5 lines

Backport bugfixes from trunk:
r846699 by Matthias Grimrath --- fixes extra - in parsed comments
r842096 by Vir --- fixes assert-fail on parsing of some SSL-related commands


------------------------------------------------------------------------
r851099 | orlovich | 2008-08-22 23:36:29 +0200 (Fri, 22 Aug 2008) | 6 lines

Fix a bunch of bugs with focus of form elements:
1) Don't allow a disabled/readonly focus element to be focused 
2) Cleaer focus from an element when it gets disabled
3) readonly things shouldn't be focusable either
BUG: 159682

------------------------------------------------------------------------
r851443 | orlovich | 2008-08-23 19:07:01 +0200 (Sat, 23 Aug 2008) | 3 lines

Fix this in the right spot: only strip it for comments, not 
other stuff that (inexplicably) goes through parseComment.

------------------------------------------------------------------------
r851467 | orlovich | 2008-08-23 20:15:09 +0200 (Sat, 23 Aug 2008) | 2 lines

Revise this 0-advance workaround to work again w/Qt4.1

------------------------------------------------------------------------
r851476 | orlovich | 2008-08-23 20:44:28 +0200 (Sat, 23 Aug 2008) | 9 lines

Make the window.frames behavior more Mozilla-like --- frames just returns 
the Window again, which meanthe properties of the window are available there, too.
This kills the FrameArray class.
Fixes #168017, and renders #164348 moot, as Window is robust against these 
cases while FrameArray wasn't

BUG:168017
BUG:164348

------------------------------------------------------------------------
r851489 | orlovich | 2008-08-23 22:32:08 +0200 (Sat, 23 Aug 2008) | 2 lines

Add missing prototypes and some missing DontEnum's.

------------------------------------------------------------------------
r851496 | orlovich | 2008-08-23 22:44:11 +0200 (Sat, 23 Aug 2008) | 6 lines

Make some fix writeable, to make other browsers better.
Fixes Hixie's skeleton demo and nasaimages.org

BUG:168127
BUG:166099

------------------------------------------------------------------------
r851523 | orlovich | 2008-08-24 00:30:56 +0200 (Sun, 24 Aug 2008) | 5 lines

Cancels any  pending redirections on form submit.
Fixes regression in forms/form_checkbox.html and #159932

BUG:159932

------------------------------------------------------------------------
r851845 | orlovich | 2008-08-24 20:53:40 +0200 (Sun, 24 Aug 2008) | 4 lines

Noticed when looking at #169722 -- the rico framework treats as as IE, using.attachEvent(!)
because hasFeature("CSS", "2.0") returns false. Since we do support DOM2 CSS, return true here, and
make that thing work.

------------------------------------------------------------------------
r851859 | orlovich | 2008-08-24 21:11:47 +0200 (Sun, 24 Aug 2008) | 3 lines

Fix a couple of cases where background-position: parsing accepted invalid settings.
BUG: 169612

------------------------------------------------------------------------
r851879 | porten | 2008-08-24 22:24:42 +0200 (Sun, 24 Aug 2008) | 3 lines

Merged revision 851878:
Fixed rgba() string representation. Instead of #rrggbb
use rgb() as Firefox does.
------------------------------------------------------------------------
r851943 | porten | 2008-08-25 01:40:19 +0200 (Mon, 25 Aug 2008) | 6 lines

Merged revision 851942:
Omit calculation of return value in internal removeProperty()
calls where it's not needed.

Speeds up the repeated backgroundColor setting in
http://www.hixie.ch/tests/adhoc/perf/video/001.html by about 8%.
------------------------------------------------------------------------
r851949 | porten | 2008-08-25 02:49:32 +0200 (Mon, 25 Aug 2008) | 2 lines

Merged revision 851948:
Save some cpu cycles (1-2% in the first skeleton video) by avoiding some QString constructions.
------------------------------------------------------------------------
r851964 | orlovich | 2008-08-25 04:25:00 +0200 (Mon, 25 Aug 2008) | 4 lines

Protect the document so we don't crash on parse errors with the DOMParser object.
Fixes #169727 and winterz's bank.
BUG:169727

------------------------------------------------------------------------
r852000 | scripty | 2008-08-25 07:24:52 +0200 (Mon, 25 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r852185 | sengels | 2008-08-25 15:38:41 +0200 (Mon, 25 Aug 2008) | 1 line

backport windows compile fix
------------------------------------------------------------------------
r852209 | sengels | 2008-08-25 16:33:12 +0200 (Mon, 25 Aug 2008) | 2 lines

fix linker error
this should break under linux too - I just wonder why it doesn't do it
------------------------------------------------------------------------
r852217 | harris | 2008-08-25 17:03:58 +0200 (Mon, 25 Aug 2008) | 17 lines

backporting fix from trunk (r851444): 

Improved "smart" placement of text labels for plotted points that have 
been named.  The previous method used a low-resolution "gridding" of the plot 
region into a 100x100 array (regardless of the plot widget's size) to serve as 
a mask to identify regions which already contain a plot element.  Now we use an 
actual image for the mask, matched to the size of the widget.  

In addition, we use a "downhill simplex" algorithm to search for an 
optimal  label position near the target point such that the label doesn't overlap with 
other plot elements.  Before we used a brute-force search.  

With the new method, the Solar System tool in KStars takes 85-95 msec to 
draw itself (compared to 250 ms with the old code, and 75-85 msec with no 
point labels).


------------------------------------------------------------------------
r852292 | dfaure | 2008-08-25 19:56:43 +0200 (Mon, 25 Aug 2008) | 2 lines

Fix docu

------------------------------------------------------------------------
r852461 | dfaure | 2008-08-25 23:40:37 +0200 (Mon, 25 Aug 2008) | 2 lines

Don't set an invalid kcomponentdata, set the main one back. Otherwise people hit asserts when using KGlobal::activeComponentData().

------------------------------------------------------------------------
r852471 | orlovich | 2008-08-25 23:53:58 +0200 (Mon, 25 Aug 2008) | 5 lines

- Make sure the "create <html>" element recovery thing isn't confused by DTD nodes
- Allow construction of empty <head> things w/o <html>, doing proper  recovery and 
making the <html> as above.
- Properly reset documentElement when doing document.open()

------------------------------------------------------------------------
r852517 | orlovich | 2008-08-26 01:00:17 +0200 (Tue, 26 Aug 2008) | 4 lines

Make sourceIndex work --- neither assert-fail in release builds, 
nor always return 0 on non-empty documents with debug builds,
nor loop infinitely on empty documents.

------------------------------------------------------------------------
r852565 | ereslibre | 2008-08-26 02:41:37 +0200 (Tue, 26 Aug 2008) | 4 lines

Backport all toolbar fixes. PLEASE REVIEW. This has been a crazy cherry picking of commits from trunk. I double checked, and I believe everything is fine.

CCMAIL: kde-core-devel@kde.org

------------------------------------------------------------------------
r852579 | orlovich | 2008-08-26 04:07:15 +0200 (Tue, 26 Aug 2008) | 9 lines

Make sure to have breaks jump to before the EndForIn instruction so that 
the for in stack entry isn't leaked. Fixes explosive memory consumption on 
 http://www.chron.com/ stories. 
 
(Perhaps following olliej's approach and using GCable objects for the context 
 would be a good idea, since it'd simplify all of this, and lighten ExecState)

BUG: 165847

------------------------------------------------------------------------
r852585 | orlovich | 2008-08-26 04:31:01 +0200 (Tue, 26 Aug 2008) | 2 lines

Return out when handling designMode put, so we don't warn about it being unhandled

------------------------------------------------------------------------
r852590 | orlovich | 2008-08-26 05:17:13 +0200 (Tue, 26 Aug 2008) | 3 lines

Fix the handling of the FontSize editting command --- it accepts the same 
numeric constants as <font size=""> does. All hail magic numbers!

------------------------------------------------------------------------
r852593 | orlovich | 2008-08-26 05:38:51 +0200 (Tue, 26 Aug 2008) | 5 lines

Backport ad filtering improvements --- 
mostly huge (~7.6x) performance improvements thanks to vtokarev's ultra-cool RK implementation,
some compatibility improvements (e.g. understanding @@ whitelist directives) and a bit of 
refactoring so we don't clutter khtml_settings.cc with computer science.

------------------------------------------------------------------------
r852600 | orlovich | 2008-08-26 06:24:15 +0200 (Tue, 26 Aug 2008) | 2 lines

Fix keycode info for ctrl+foo and similar keys.

------------------------------------------------------------------------
r852716 | orlovich | 2008-08-26 14:47:37 +0200 (Tue, 26 Aug 2008) | 2 lines

This is case insensitive...

------------------------------------------------------------------------
r852727 | bram | 2008-08-26 15:15:21 +0200 (Tue, 26 Aug 2008) | 1 line

Fix compilation
------------------------------------------------------------------------
r852920 | orlovich | 2008-08-26 23:14:04 +0200 (Tue, 26 Aug 2008) | 10 lines

Fix the debugger window keeping konqueror open, by refcounting it and destroying it 
when all KJSProxy's are gone. Also fix debugger disabling not taking effect, and 
many cases of debugger enabling in the middle of execution crashing by syncing 
things at session boundaries (imperfectly for enabling, though)

Most of the credit goes to Hugh Daschbach for masterfully analyzing the issue.

CCBUG:157685
BUG:169881

------------------------------------------------------------------------
r853061 | scripty | 2008-08-27 08:04:00 +0200 (Wed, 27 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r853394 | orlovich | 2008-08-27 18:57:36 +0200 (Wed, 27 Aug 2008) | 12 lines

Make sure to update layout, and not just CSS/RenderStyle when querying image width/height 
because, well, we do need the size. 

Fixes image data not being up-to-date on load, (#129307)
Fixes infinite loop with some forum software (#169844, #166781, most likely #165668, presuming fixed 
since no non-bt info available)

BUG:129307
BUG:169844
BUG:166781
BUG:165668

------------------------------------------------------------------------
r853405 | orlovich | 2008-08-27 19:19:12 +0200 (Wed, 27 Aug 2008) | 6 lines

Backport r853395 by vtokarev, fixing #155597:

do the same changes as in r691143 but for objects to avoid random hardly reproducible crashes on toggling content editable state



------------------------------------------------------------------------
r853417 | orlovich | 2008-08-27 19:39:05 +0200 (Wed, 27 Aug 2008) | 5 lines

Properly handle box-sizing for getComputedStyle, fixing crash on 
djangobook.com

BUG:169617

------------------------------------------------------------------------
r853478 | winterz | 2008-08-27 21:20:33 +0200 (Wed, 27 Aug 2008) | 5 lines

backport SVN commit 852242 by zack:

test apostrophes


------------------------------------------------------------------------
r853482 | winterz | 2008-08-27 21:22:46 +0200 (Wed, 27 Aug 2008) | 4 lines

backport SVN commit 852267 by zack:

after replacing a word, restart before that word

------------------------------------------------------------------------
r853487 | winterz | 2008-08-27 21:24:30 +0200 (Wed, 27 Aug 2008) | 2 lines

unused variable--

------------------------------------------------------------------------
r853490 | winterz | 2008-08-27 21:30:21 +0200 (Wed, 27 Aug 2008) | 4 lines

backport SVN commit 852281 by zack:

by default finish, not cancel

------------------------------------------------------------------------
r853492 | winterz | 2008-08-27 21:39:17 +0200 (Wed, 27 Aug 2008) | 4 lines

backport SVN commit 845866 by tmcguire:

warning--

------------------------------------------------------------------------
r853500 | winterz | 2008-08-27 21:46:49 +0200 (Wed, 27 Aug 2008) | 5 lines

backport SVN commit 852365 by zack:

underline using a squiggly line instead of red color
also be a little smarter about auto-disabling

------------------------------------------------------------------------
r853505 | winterz | 2008-08-27 21:55:07 +0200 (Wed, 27 Aug 2008) | 5 lines

SVN commit 853319 by zack:

remove never used files
qt now has this functionality

------------------------------------------------------------------------
r853566 | lueck | 2008-08-27 23:56:51 +0200 (Wed, 27 Aug 2008) | 1 line

backport from trunk
------------------------------------------------------------------------
r853739 | mueller | 2008-08-28 10:22:41 +0200 (Thu, 28 Aug 2008) | 2 lines

KDE 4.1.1

------------------------------------------------------------------------
