2006-01-30 17:27 +0000 [r503870]  helio

	* branches/arts/1.5/arts/configure.in.in: - Fix glib detection for
	  older pkg-config

2006-02-03 19:20 +0000 [r505373]  jriddell

	* branches/arts/1.5/arts/debian (removed): Remove debian directory,
	  now at http://svn.debian.org/wsvn/pkg-kde/trunk/packages/arts
	  svn://svn.debian.org/pkg-kde/trunk/packages/arts

2006-03-15 17:10 +0000 [r518909]  lunakl

	* branches/arts/1.5/arts/mcop/object.cc: Avoid accessing deleted
	  memory.

2006-03-17 21:26 +0000 [r519759]  coolo

	* branches/arts/1.5/arts/configure.in.in: tagging 3.5.2

2006-03-17 21:34 +0000 [r519774]  coolo

	* branches/arts/1.5/arts/arts.lsm: tagging 3.5.2

