2008-03-01 19:49 +0000 [r780996]  mueller

	* branches/KDE/4.0/kdebindings/kalyptus/kalyptus,
	  branches/KDE/4.0/kdebindings/kalyptus/kalyptusCxxToSmoke.pm:
	  backport parts of r776653 to fix build with Qt 4.3.4

2008-03-01 21:16 +0000 [r781017]  kkofler

	* branches/KDE/4.0/kdebindings/python/pykde4/sip/kdecore/kservice.sip:
	  Remove no longer existing protected function
	  KService::accessServiceTypes from PyKDE4. (The function was
	  always marked internal, so it shouldn't have been in the binding
	  in the first place.) (backport rev 781014 from trunk) Dirk, this
	  is required to fix building PyKDE4 against kdelibs 4.0.2, so it
	  should probably be included in a rerolled kdebindings-4.0.2.
	  CCMAIL: mueller@kde.org

2008-03-01 21:40 +0000 [r781022]  mueller

	* branches/KDE/4.0/kdebindings/kalyptus/kalyptus,
	  branches/KDE/4.0/kdebindings/kalyptus/kalyptusCxxToSmoke.pm:
	  merge r777019, this should help with Qt 4.3.4 as well

2008-03-02 11:11 +0000 [r781229]  mueller

	* branches/KDE/4.0/kdebindings/kalyptus/kalyptus: backport parts of
	  r780626 | rdale | 2008-02-29 17:33:47 +0100 (Fri, 29 Feb 2008) |
	  5 lines to fix build against Qt 4.4

2008-03-02 18:46 +0000 [r781383]  arnorehn

	* branches/KDE/4.0/kdebindings/csharp/qyoto/src/qyoto.cpp,
	  branches/KDE/4.0/kdebindings/csharp/qyoto/ChangeLog: * Backported
	  bugfixes from kdebindings-smoke2 CCMAIL: kde-bindings@kde.org

2008-03-07 07:27 +0000 [r783138]  sebsauer

	* branches/KDE/4.0/kdebindings/python/krosspython/pythoninterpreter.cpp:
	  backport r783137 disable the stdin-hack which is needed for
	  LiquidWeather :-/

2008-03-24 17:31 +0000 [r789594]  rdale

	* branches/KDE/4.0/kdebindings/kalyptus/kalyptusCxxToSmoke.pm: *
	  Attempt to make the smoke lib build with both Qt 4.3.x and Qt
	  4.4.x

