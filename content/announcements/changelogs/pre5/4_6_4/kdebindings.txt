------------------------------------------------------------------------
r1232112 | lbeltrame | 2011-05-16 06:41:35 +1200 (Mon, 16 May 2011) | 9 lines

Fix pykdeuic4 parsing with newer PyQt (*again*). In newer versions text is
always wrapped in functions (_fromUtf8) so quotes should not be put around 
i18n() calls as overridden by PyKDE4 or all one would get would be syntax 
errors.
This is only a patch over the wound. Ultimately pykdeuic4 needs to be fixed
of refactored for good.



------------------------------------------------------------------------
r1233431 | pino | 2011-05-25 00:54:40 +1200 (Wed, 25 May 2011) | 4 lines

Include also PHONON_EXPORT_DEPRECATED as defined, based on phonon commit 6e3b693b

Backport of commit e5a62c49e8ee77b2ce06d75dedb5f1d9edddc7e6 in smokeqt by Raymond Wooninck <tittiatcoke@gmail.com>

------------------------------------------------------------------------
