---
aliases:
- ../../fulllog_releases-21.07.90
- ../releases/21.07.90
title: KDE Gear 21.07.90 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ Add parent to menu. [Commit.](http://commits.kde.org/akregator/b36b5ccb08b90ecebdb6fe018cee1edde811040a) 
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Refactor timetable printing utility functions. [Commit.](http://commits.kde.org/calendarsupport/c9c0b93f74303980ccc22d9b6be8df8d23e233c3) 
+ Insert a parent class for CalPrintDay and CalPrintWeek config options. [Commit.](http://commits.kde.org/calendarsupport/e7e7e388d05d3bc083b8cd99543609a252d8b489) 
+ Obey Use Colors option when printing tags. [Commit.](http://commits.kde.org/calendarsupport/3bf4c90b37e67e4c734aa54b0fc7f51c2b4cbba3) 
+ Combine doLoadConfig()/loadConfig(), doSaveConfig()/saveConfig(). [Commit.](http://commits.kde.org/calendarsupport/a72fc7ecfc1043e4abce12bbdc11945cdc44a750) 
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ [R] properly show the results of R's "help commands" appropos(), vignette(), demos() and help.search(). [Commit.](http://commits.kde.org/cantor/938a020eec059287d501ca4315b86579aee7a367) 
+ Also show the internal help in R when the user is requesting it via help(). [Commit.](http://commits.kde.org/cantor/1b7c9c1090d0029a1afa8509233571f6ef4de586) 
+ Fixed multiple issues reported by Coverity. [Commit.](http://commits.kde.org/cantor/4c45c9dff0354c8163e78fdc1763faddd4b63d62) 
+ Fixed a couple of issues reported by cppcheck. [Commit.](http://commits.kde.org/cantor/7c7a31103ae1d5e8b08987059105f0d63d0182eb) 
+ When using KSharedConfig, specify explicitely 'cantrorc' so we also propely read the documentation settings in LabPlot. [Commit.](http://commits.kde.org/cantor/d300c2fcdd541df13eff65eafdda8238232bd71d) 
+ Open the default URL for the online documentation in case no local documentation is installed. [Commit.](http://commits.kde.org/cantor/11dd741df61fbb5af1ac069a12e93b29386701bb) 
+ Fixed the layout in qalculate settings widget to look the same like the settings widgets for all other backends,. [Commit.](http://commits.kde.org/cantor/28c1b3959dc41250b3f142d14c2f28a16a593cd5) 
+ Removed the setting "local documentation". [Commit.](http://commits.kde.org/cantor/7db0d041463ce6f1905cd0df6f4afa0175c1da0a) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Add break; to KItemListRoleEditor::keyPressEvent cases. [Commit.](http://commits.kde.org/dolphin/674a4a91dac8698bdd9aef611e2707d4fb389bf1) 
+ [PlacesPanel] Remove horizontal scrollbar. [Commit.](http://commits.kde.org/dolphin/90699c2c2dc6927c8294965041e45c6b986dceff) Fixes bug [#301758](https://bugs.kde.org/301758)
+ Fix placeholder label text within an empty folder in Trash. [Commit.](http://commits.kde.org/dolphin/3fe971e17467e30ba59ca42a53931a273fd7e78b) Fixes bug [#439952](https://bugs.kde.org/439952)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Make KIO optional on Android and required everywhere else. [Commit.](http://commits.kde.org/elisa/ab48bc4f78902e02f9f1c59e5dba3e90452efba9) 
+ Avoid fetching lyrics twice for tracks not in database from playlist. [Commit.](http://commits.kde.org/elisa/f5170ed659f29d87ebd13a9da2fdd6d308979d5f) 
+ Remove hard dependency on DBus for powermanagement code. [Commit.](http://commits.kde.org/elisa/18c5bfa7f17a940693ae756cb9a35f26dfbcffdd) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Simplify sorting the Todo List view. [Commit.](http://commits.kde.org/eventviews/247662e333c0fc2d0de1ab0ad2c172979e4e8d7d) 
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Add catalog definition for Dolphin context menu items. [Commit.](http://commits.kde.org/gwenview/709e4ef5029f10f14711faee16d7f61e671a90cf) Fixes bug [#439931](https://bugs.kde.org/439931)
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Fix possible abort when creating incidences with tags. [Commit.](http://commits.kde.org/incidenceeditor/70aa7ba42904adc50b7c06aeed3e827dada3a047) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Fix build against stable branch kitinerary. [Commit.](http://commits.kde.org/itinerary/5ddec58339a7e7717fde71527c2416ba675c5438) 
+ Fix ReservationHelper::equals always returning false. [Commit.](http://commits.kde.org/itinerary/6e85129b8d7af1b76c2ec836fd47d9955c357443) 
+ Remember which map locations we have already cached. [Commit.](http://commits.kde.org/itinerary/26a03e5cc4964ab9dba2ba600e6ddcf89c63d1d7) 
+ Request fewer transfer journeys. [Commit.](http://commits.kde.org/itinerary/5bdbda52813f51ec799643f5e77a317ad5682203) 
+ Improve platform change notification check. [Commit.](http://commits.kde.org/itinerary/30001e57ad0b982399a1f0bb1715e4b43fbebc8d) 
+ Show the arrival location if we have no departure. [Commit.](http://commits.kde.org/itinerary/f171b89b56d7d9b108df8ff0023dd601514047a8) 
+ Fix trip group delegate content hiding/unhiding. [Commit.](http://commits.kde.org/itinerary/76b7642e21bea3b6fc5551afa07aeb1ac683bd10) 
+ Also write intermediate stop waypoints to the GPX trip export. [Commit.](http://commits.kde.org/itinerary/9d094009b627db88a481b83d2ae51f75c8867745) 
+ Don't write invalid GPX time elements. [Commit.](http://commits.kde.org/itinerary/ba7cdf478814d86d8b7e03998e5bce1ebb3d5201) 
+ Fix strict weak order violation for timeline element comparison. [Commit.](http://commits.kde.org/itinerary/6c2f3486eebc9675a838954630a2375e2d8f194c) 
+ Allow to also import health certificates from PDFs. [Commit.](http://commits.kde.org/itinerary/470feea214a41ec81d41be00e46bb0f5ddf91392) 
+ Fix checks for non-set date/time values. [Commit.](http://commits.kde.org/itinerary/3092eeb3174347056aecdff08230ff64892b3c12) 
{{< /details >}}
{{< details id="kaddressbook" title="kaddressbook" link="https://commits.kde.org/kaddressbook" >}}
+ Add parent to qmenu. [Commit.](http://commits.kde.org/kaddressbook/617e4254f3541853d91f60d12e37f5d0762e8836) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Update changelog. [Commit.](http://commits.kde.org/kalarm/636be1ef9980fcca269d4d684a2b50f27c769551) 
+ Update changelog. [Commit.](http://commits.kde.org/kalarm/deee90c70cd571cdc540df31556720236204f5e8) 
+ Don't display a 'time to' for alarms whose trigger time has passed. [Commit.](http://commits.kde.org/kalarm/0cebca8501786656acf3839060196553caa17d28) 
+ Fix clazy warning. [Commit.](http://commits.kde.org/kalarm/6ace88b90455eee05411d527364d464da5d0cf27) 
+ Fix alarm time showing blank in alarm list in right-to-left mode. [Commit.](http://commits.kde.org/kalarm/e3ca79e0a8a031b56b6ea1a5e55288709ac22f62) 
+ In right-to-left mode, select correct date for mouse click position. [Commit.](http://commits.kde.org/kalarm/32ea1d3fdb6351138e6bd96f47f56bfe0959c976) 
+ Layout fixes for right-to-left direction. [Commit.](http://commits.kde.org/kalarm/265de2a8228f18b005233341510ad09acd73c29d) 
+ Disable modal alarm messages config setting on Wayland. [Commit.](http://commits.kde.org/kalarm/b361ef6f4415e7db094e6e20e3882f973a0e8bc3) 
+ Show hamburger context menu at correct position on Wayland. [Commit.](http://commits.kde.org/kalarm/d2949e639a4654c6b4d8a1e3cb9ad7ea26ef8bf5) 
+ Fix description in changelog. [Commit.](http://commits.kde.org/kalarm/4f2a149bd3e175a06198f3a2a7478929e368d011) 
+ Bug 439853: Fix crash at startup on multiple screen system using Wayland. [Commit.](http://commits.kde.org/kalarm/a063257b4bb673f3ab7d85a15a1c926f9c768696) 
+ Show context menu at correct position on Wayland. [Commit.](http://commits.kde.org/kalarm/b821cd4ff58e33c2008411aad701e9600f95504e) 
{{< /details >}}
{{< details id="kalarmcal" title="kalarmcal" link="https://commits.kde.org/kalarmcal" >}}
+ Fix deletion of events with reminders after the main event. [Commit.](http://commits.kde.org/kalarmcal/71ea41a44b4519eb59c72eca00d83ba56b2c3ede) Fixes bug [#440200](https://bugs.kde.org/440200)
+ Fix events with reminder alarms not triggering. [Commit.](http://commits.kde.org/kalarmcal/1942c057d18c4480f7861b98d930f76499f03819) Fixes bug [#440200](https://bugs.kde.org/440200)
{{< /details >}}
{{< details id="kamoso" title="kamoso" link="https://commits.kde.org/kamoso" >}}
+ Revert the removal of FindOpenGL2. [Commit.](http://commits.kde.org/kamoso/a414f7bfca1919891947cd7da6c4481c9f1a7d98) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ LocHistory: When limiting size, also rewind currentLocation. [Commit.](http://commits.kde.org/kate/0919ddefab783346780a346b37dffc2bb454d967) 
+ Location history improvements. [Commit.](http://commits.kde.org/kate/e7241670e1c270732d827f1513a4d6dd2f3301df) 
+ Handle nullptr return of screenAt(). [Commit.](http://commits.kde.org/kate/4e985e363da761bbb63b63a2741cf65e3c323281) Fixes bug [#439804](https://bugs.kde.org/439804)
{{< /details >}}
{{< details id="kbackup" title="kbackup" link="https://commits.kde.org/kbackup" >}}
+ Tell cmake our version. [Commit.](http://commits.kde.org/kbackup/38a3f73cb7803240235fe2fda4813a653843e8a2) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Set breeze as fallback icon theme for all executables. [Commit.](http://commits.kde.org/kdeconnect-kde/7f87a4f197dfe0d11909caf28deee8df9c42532e) 
+ Add missing KF5WindowSystem dependency. [Commit.](http://commits.kde.org/kdeconnect-kde/eaec8888ada5492953eab2ee89f593d1c432ec03) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix timeremap widget not enabled in some cases. [Commit.](http://commits.kde.org/kdenlive/cdf7db3506f7b93ffe327c4ca56feb5a03a0c9b6) 
+ Ensure markers are properly sorted in Clip Properties dialog, enable F2 rename. [Commit.](http://commits.kde.org/kdenlive/dd3ca408963995279b4f8312f8a952a5337d0343) 
+ Fix position and zoom effect and affine composition broken with switch to MLT7. [Commit.](http://commits.kde.org/kdenlive/69434fb89fb3efa151d0e28fb4f00726ddeed4ea) 
+ Fix audio thumbs missing on clip reload (like when changing autorotate value). [Commit.](http://commits.kde.org/kdenlive/9b828c4b57aa0154344fa410da8416ac19590448) 
+ Move remap marker before clip name, fix qml warning. [Commit.](http://commits.kde.org/kdenlive/c055ffa769c217a6e87a90f3756a4a5258d5c4f1) 
+ TImeline guides: add delimiter and highlight active one. [Commit.](http://commits.kde.org/kdenlive/71ba58f48e1fe785d4f1bed858d55f78ce46ca5f) 
+ Disable time remap for color or image clips and clips with speed effect. [Commit.](http://commits.kde.org/kdenlive/39494a12556b2f11e3339ab6ed20b5fba48c1f74) 
+ Various fixes for timeremap. [Commit.](http://commits.kde.org/kdenlive/b3a4ae10a9f5bf7c1bfa32566a85ea1e0ecd8223) 
+ Timeremap: don't seek on drag start (caused delay), add snap to start/end of clip. [Commit.](http://commits.kde.org/kdenlive/e0765a832f2f64d0d78ece89801e47e011fd84cc) 
+ Fix timeremap keyframe grab zone. [Commit.](http://commits.kde.org/kdenlive/470b7f345f9907867e145a46de95726c68d405b7) 
+ Removing a remap effect now restores input duration. [Commit.](http://commits.kde.org/kdenlive/a50a24f1dd224b16f3794510b1977d1d2cf85f9c) 
+ Fix mix direction lost on save / change track, fix clip offset on vertical move while deleting start mix. [Commit.](http://commits.kde.org/kdenlive/b4c0505cdfe934cf9fb61069f49fc8db498b77fd) 
+ Fix timeremap undo/redo to resize clip in one pass. [Commit.](http://commits.kde.org/kdenlive/66b95243bfb9866c20804b654dfd5e5998a609b8) 
+ Small update for timeremap ui. [Commit.](http://commits.kde.org/kdenlive/636cd23f544b14d0f2ee61320e0ea8a35f3d592b) 
+ Use KDE_INSTALL_QTQCHDIR to install QCH documentation. [Commit.](http://commits.kde.org/kdenlive/70b4260e497ff5c97cc45c37da8c1b77cf390442) 
+ Fix various mix move issue and tests. [Commit.](http://commits.kde.org/kdenlive/f7f3fc215e38f02bf533cf0e6326954d3d2c98d8) 
+ Fix various time remap display glitches, only resize timeline clip on mouse release. [Commit.](http://commits.kde.org/kdenlive/058b8e882e958835bd0690b09321c11d08eb2d3a) 
+ Make time remap a checkable option in the context menu so it can easily be removed. [Commit.](http://commits.kde.org/kdenlive/fbbac11593897dfeda8455ae12ce6c445675ea39) 
+ Fix crash on mix group move, fix mix sometimes refusing to be created. [Commit.](http://commits.kde.org/kdenlive/f6048bd147b6fc86ab2a0eb7429ae6c2d1bad398) 
+ Time remap: display negative speed where it makes sense. [Commit.](http://commits.kde.org/kdenlive/1bb25c612c90aa0022472986260201a7c4d24246) 
+ Fix possible crashes in timeremap. [Commit.](http://commits.kde.org/kdenlive/affc281fbe26ccec6debe8b070a00670f341e9a6) 
+ Time remap: fix expanding clip creating extra keyframes. [Commit.](http://commits.kde.org/kdenlive/511997cbfee41e475eab31b7fd61edcd2354bd36) 
+ Fix another grouped mix move crash. [Commit.](http://commits.kde.org/kdenlive/dfc4097e4eb163910dca026c3695d423bee88e8c) 
+ Fix crash on grouped mix deletion. [Commit.](http://commits.kde.org/kdenlive/84a008368b68a31169dda9043778e703fb6dc17d) 
+ Properly set default label for guides. [Commit.](http://commits.kde.org/kdenlive/f65254d7f8e8c766bf5c18fb3f1bc411937a28da) 
+ Fix various timeremap inconsistencies. [Commit.](http://commits.kde.org/kdenlive/17b0bc047ff81522906a33a28df6652c0138ec2a) 
+ Various timeremap UI and workflow fixes. [Commit.](http://commits.kde.org/kdenlive/14c98d92ba0c8f639fd3249f73f0dfe54a139155) 
+ Fix various mix move issues. [Commit.](http://commits.kde.org/kdenlive/b2e2abb2fb0f4cbc8df90718984218a408c42a9d) 
+ Show edit dialog on guide creation via "Add/Remove Guide". [Commit.](http://commits.kde.org/kdenlive/c435bd427c4fd0d5cf4f959939fcc2291f7cc6b5) 
+ Fix: always enable marker actions to make them usable in clip monitor. [Commit.](http://commits.kde.org/kdenlive/eb3dec0346995933d547129628ef94bddd797f51) 
+ Fix cannot move grouped clips with mix. [Commit.](http://commits.kde.org/kdenlive/9c198b201f89f5ee6fdc87aad6f5dc68473ea154) 
+ Drop custom combobox stylesheet causing unreadable text. Thanks to Martin Sandsmark for the hint. [Commit.](http://commits.kde.org/kdenlive/465e6dbb790baef47ecc5e8061546737422096fc) Fixes bug [#428755](https://bugs.kde.org/428755)
+ Revert "Fix clips with mix cannot be move onto another track.". [Commit.](http://commits.kde.org/kdenlive/dd4d8d37bb308bcead917eb43deed4d705c1740a) 
+ Fix clips with mix cannot be move onto another track. [Commit.](http://commits.kde.org/kdenlive/2b4fcacce33e69f1aa85c9b18d4beb5b8bbdd141) 
+ Remove seek checkboxes in timeremap, add button to center keyframe at cursor position. [Commit.](http://commits.kde.org/kdenlive/af4a81f6f9e0df3fdc7641a28609435ca00a0065) 
+ Don't stop preview render when editing outside of the preview zone. [Commit.](http://commits.kde.org/kdenlive/ec313d5dc4d6b58978aff7065ee97da3c1e6f850) 
+ Fix reverse wipes. [Commit.](http://commits.kde.org/kdenlive/cec9664648f7abb82542dbe19d8d28e484be426d) 
+ Mix alignment: remember and adjust resize accordingly. [Commit.](http://commits.kde.org/kdenlive/6f7df8c3753ac855293b8d875a5b6aa06dbe2d7d) 
+ Hide composition list on audio mix. [Commit.](http://commits.kde.org/kdenlive/5869fd57c55b6e15736878cb26c242e07fd19e0e) 
+ Fix crash dragging multiple audio streams clip in timeline with locked tracks. [Commit.](http://commits.kde.org/kdenlive/6c37857ead305a43f330517983c3c040dd67e696) Fixes bug [#439849](https://bugs.kde.org/439849)
+ Expose mix duration and align buttons. [Commit.](http://commits.kde.org/kdenlive/d061e92d1e59b8159342a703c557d51c06f84d68) 
+ Fix crash on multiple items deletion. [Commit.](http://commits.kde.org/kdenlive/06181f0e831b4816681b7d183c913e57f1f35e5e) 
+ Timeremap: expose frame blending param. [Commit.](http://commits.kde.org/kdenlive/8421430b7d4d970bc9ae632d8d6e2c6b2719b5da) 
+ Time remap: fix keyframe timecode editing, add pitch correction. [Commit.](http://commits.kde.org/kdenlive/202c2c1ccff26c70469c5124bdbf1118e76f0a73) 
+ Upgrade document version and fix wipe params for MLT 7 when opening older project file. [Commit.](http://commits.kde.org/kdenlive/59a699783e2affd4357e82d57fb96dc86f9c2e0e) 
+ Fix crash on remap clip selection, add button to delete remap effect. [Commit.](http://commits.kde.org/kdenlive/0c87cda1281221e217b781e5d1e397a6261397da) 
+ Timeremap: make it work with clip not starting at 0 and adjust view on clip resize. [Commit.](http://commits.kde.org/kdenlive/4df1f4242070dfdc9dd7b5e2af9b7e4225f2790a) 
+ Fix moving clip group before another clip broken. [Commit.](http://commits.kde.org/kdenlive/8ac4374b1a00b044320a294caeee38df9270b6d2) 
+ Time remap: fix monitor focus and seeking issues. [Commit.](http://commits.kde.org/kdenlive/5993f95ce882282b0789dbcc4c9c8d15d5be8f96) 
+ Remap: fix editing "speed before" broke, correctly clear remap widget on clip deletion. [Commit.](http://commits.kde.org/kdenlive/134fdd58a1cae2d22870c8cb6bf89f37a6e8ce4b) 
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Fix UI elements being used before initialization. [Commit.](http://commits.kde.org/kdepim-runtime/8f7520ed17f96b7eaddc50ec314eca30509d1e6b) Fixes bug [#439991](https://bugs.kde.org/439991)
{{< /details >}}
{{< details id="kdf" title="kdf" link="https://commits.kde.org/kdf" >}}
+ Tell cmake our version. [Commit.](http://commits.kde.org/kdf/bd19f55e62168fd1b3955c3caf66daf56d872bd2) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ The isspace/isctrl/etc functions require unsigned char as input. [Commit.](http://commits.kde.org/kitinerary/a100e721a5aebb3caea9fbbc39edae7bae762e69) 
+ Remove unneeded MSVC utf-8 definition. [Commit.](http://commits.kde.org/kitinerary/421759f71a50ed96b1754d5858bcaeda117f2878) 
+ Add event reservation extractor for ticketmaster.de. [Commit.](http://commits.kde.org/kitinerary/a7687588679401ac9800f53efe4e93e7329d0f34) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Add parent to menu. [Commit.](http://commits.kde.org/kmail/76fb41023484689e46d15c8d152a61ea6c61e0a9) 
{{< /details >}}
{{< details id="kmplot" title="kmplot" link="https://commits.kde.org/kmplot" >}}
+ Give kmplot an automatically increasing version. [Commit.](http://commits.kde.org/kmplot/8d6aa573a3cef77dfa6c4b554229d8458a7734aa) 
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Tell cmake our version. [Commit.](http://commits.kde.org/konqueror/feaf6bba52fa964394126e072ea73df4ec33ef21) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Resize lines when clearing to end of line. [Commit.](http://commits.kde.org/konsole/7e0d300c2d08e8429b0e1997d07ce543c186767d) Fixes bug [#432669](https://bugs.kde.org/432669)
+ Fix one crash in the sshmanager plugin. [Commit.](http://commits.kde.org/konsole/94e4285da0acee4ff94ccb54583eaafda40a8958) Fixes bug [#439551](https://bugs.kde.org/439551)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Bind keyboard shortcuts in the Search dialog's results. [Commit.](http://commits.kde.org/korganizer/48485e4fbdc7b70a30972bb3ac2d529b987746aa) Fixes bug [#315894](https://bugs.kde.org/315894)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Propagate static timezones to intermediate stops as well. [Commit.](http://commits.kde.org/kpublictransport/5df7b94e69638ba23e938684956ab53c147f4d19) 
+ Add missing timezone for VRR. [Commit.](http://commits.kde.org/kpublictransport/d477266865aa4716b44dce17d3ebf4de1bacac08) 
+ Propagate merged line data to intermediate stops. [Commit.](http://commits.kde.org/kpublictransport/cb2e4a7ab4490107c346d087991511de45e46440) 
+ The isspace/isctrl/etc functions require unsigned char as input. [Commit.](http://commits.kde.org/kpublictransport/2d19050ab6fb4909d1c54865d3d9802e0fffb565) 
+ Properly handle empty path sections in Hafas responses. [Commit.](http://commits.kde.org/kpublictransport/94b9cc30024a54ce7f1425ceb3240bc6d64fcf3b) 
{{< /details >}}
{{< details id="kross-interpreters" title="kross-interpreters" link="https://commits.kde.org/kross-interpreters" >}}
+ Remove rb_set_safe_level call. [Commit.](http://commits.kde.org/kross-interpreters/473116aa9977b64f5a47fdeda1efc62aa640c5c8) 
{{< /details >}}
{{< details id="libkomparediff2" title="libkomparediff2" link="https://commits.kde.org/libkomparediff2" >}}
+ Set correct file mode when saving changes (#402363). [Commit.](http://commits.kde.org/libkomparediff2/99b1d14756279c6cb3cb4df6027585130d17205e) Fixes bug [#402363](https://bugs.kde.org/402363)
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Fix color channels being swapped. [Commit.](http://commits.kde.org/libksane/de59b9121ea62db5a49b1d4477becf17eb86c2de) 
+ Correctly load option values during construction. [Commit.](http://commits.kde.org/libksane/7695b538852a6f10a6f3567b1ec0bf40e610964c) 
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ Fix Bug 440041 - Sieve editor does not show the line numbers correctly. [Commit.](http://commits.kde.org/libksieve/71fb1356afc64bde878911cfb3c2e8258e0b91a5) Fixes bug [#440041](https://bugs.kde.org/440041)
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix QStringView porting mistake. [Commit.](http://commits.kde.org/messagelib/e9a229bc061876bd82f7b8278d96c7accee914e9) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix Page Up/Down scrolling when scrollbars are turned off. [Commit.](http://commits.kde.org/okular/c399a11054fffafb86938077a0591c05a4a3511c) Fixes bug [#421822](https://bugs.kde.org/421822)
{{< /details >}}
{{< details id="rocs" title="rocs" link="https://commits.kde.org/rocs" >}}
+ Set the project version also at the cmake level. [Commit.](http://commits.kde.org/rocs/415fd1c51672790628a1ecaf26083aacae3dacb2) 
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Tell cmake our version. [Commit.](http://commits.kde.org/yakuake/95b57b435a4ada4d7478347f974ba9bca8ecf23f) 
{{< /details >}}
