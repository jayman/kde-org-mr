---
aliases:
- ../../fulllog_releases-22.07.90
title: KDE Gear 22.07.90 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Fix crash when using QString as KConfigGroup name, use C Char array from start. [Commit.](http://commits.kde.org/akonadi-calendar/b2fc63fe688b0fb946021547ff560cd6d6760437) 
+ Condense occurrenceForAlarm early returns and add check for null incidence ptr. [Commit.](http://commits.kde.org/akonadi-calendar/5d4950eb5f70514c9b7e59066d5f3cb450a67517) 
{{< /details >}}
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ Drop unused KF5Service dependency. [Commit.](http://commits.kde.org/akregator/c4f5687ec0967b31af411c56818ae83c2f0c4ae7) 
+ Drop unused QGpgme dependency. [Commit.](http://commits.kde.org/akregator/712175e11bc6fb71938f67ee0426660c6fe6aebc) 
{{< /details >}}
{{< details id="analitza" title="analitza" link="https://commits.kde.org/analitza" >}}
+ Fix typo breaking analitzatest. [Commit.](http://commits.kde.org/analitza/dc589ee7256230edb04428f374b099fb229e842a) 
+ Use Config mode for finding Eigen3. [Commit.](http://commits.kde.org/analitza/a2d20b525451b281c1e09d2f0b55a3153d2b8990) 
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Pass parent widget to add dialog. [Commit.](http://commits.kde.org/ark/5d90a6b412cef088410bf7c10c9673e501c9d5f0) 
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Use the Akonadi::TagCache from Akonadi. [Commit.](http://commits.kde.org/calendarsupport/8c6a9467eeb8c7396a6deb2785472f63be2fe61c) 
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Squash commit of multiple fixes that will go into 22.08. [Commit.](http://commits.kde.org/cantor/e73ab99b8127b693fdc7300beada0d826781af85) Fixes bug [#456650](https://bugs.kde.org/456650)
{{< /details >}}
{{< details id="falkon" title="falkon" link="https://commits.kde.org/falkon" >}}
+ Remove duplicate releases tag. [Commit.](http://commits.kde.org/falkon/65e51dc54de31230066981c3c4d23efb193a0d6f) 
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Drop unused KF5IconThemes dependency. [Commit.](http://commits.kde.org/filelight/867363efe774a402997e1c7f7ac97584ffa531f6) 
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Include exiv2.hpp and KImageAnnotator outside of #ifdef HAVE_TIFF. [Commit.](http://commits.kde.org/gwenview/21f51b259643dfd17635fb6dce61662449883e53) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Only show certificate issue time when present. [Commit.](http://commits.kde.org/itinerary/00fef1c5be779e00e225b33bf10ec1830f5f187a) 
+ Integrate kirigami-addons upstream fixes. [Commit.](http://commits.kde.org/itinerary/e61b291dd8a8d158227ee1565f8bc28951d1b2c1) 
+ Propagate ticket and document data also to newly generated elements. [Commit.](http://commits.kde.org/itinerary/b069d40fb89dc497068e1954cf03a7618aaa801e) 
+ Fix scrolling of the vehicle layout view with new Kirigami. [Commit.](http://commits.kde.org/itinerary/5e4b7280dc6b2471c3ae9b80462b653bb7ef2ecb) 
+ Also import health certificates from pkpass files. [Commit.](http://commits.kde.org/itinerary/a432079ba6bea2fd61b412bdc88f4c2177b83f0d) 
+ Sync with kirigami-addons upstream. [Commit.](http://commits.kde.org/itinerary/63f341fef1383d3bd01f72506a6ec7e3d479fc65) 
{{< /details >}}
{{< details id="juk" title="juk" link="https://commits.kde.org/juk" >}}
+ Player: Fix bug where moving to next track would play twice in rapid succession. [Commit.](http://commits.kde.org/juk/0fb22f61357f73ec989dfec4c49612e2511a65d3) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Bug 456845: Set volume correctly for repeated audio alarms. [Commit.](http://commits.kde.org/kalarm/45f4df183ebaf94bbdf42652d8fc843056e26abe) 
+ Fix CI failure. [Commit.](http://commits.kde.org/kalarm/e7bb0d258df81245d4c4342f5052857ff6dc1498) 
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Remove qml debugging. [Commit.](http://commits.kde.org/kalendar/3b6a46747995e6fc37e4b00a9a0f2a402a9e0ed6) 
{{< /details >}}
{{< details id="kalzium" title="kalzium" link="https://commits.kde.org/kalzium" >}}
+ Use Config mode for finding Eigen3. [Commit.](http://commits.kde.org/kalzium/c4d77ecb6308846f5534510d62fe2ba934dc7b69) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Revert "Filter out binary files in folder based projects + untracked git". [Commit.](http://commits.kde.org/kate/3595b960557f9cb37f0a678d4e4a3ad0d19ef547) 
+ Konsole plugin: use QUrl::toDisplayString() in error message. [Commit.](http://commits.kde.org/kate/2f3586fc2febcbd23c76487758ebbfb9d71a5db0) 
+ Konsole plugin: quote args before passing them to the shell. [Commit.](http://commits.kde.org/kate/a59494709cc4e8e3415fa7ad88c9e505c89e9547) See bug [#456570](https://bugs.kde.org/456570)
+ LSP: Fix hand cursor remaining in some cases. [Commit.](http://commits.kde.org/kate/327810a53a72f435a26b4f64007154e52975f179) 
+ Tabbar: DND improvements. [Commit.](http://commits.kde.org/kate/75e4505f5e613ce2ad999e429b82a4f3dc8f0063) Fixes bug [#456844](https://bugs.kde.org/456844)
+ Tabbar: Provide a drop action when starting drag. [Commit.](http://commits.kde.org/kate/7a83673aeb5b1ca8c47de7398ecb2d7eb8c8d5ef) 
+ Konsole: Wrap file name into quotes. [Commit.](http://commits.kde.org/kate/4a10f068ea243efdc64fd6df2e8e149f2aa813f1) Fixes bug [#456570](https://bugs.kde.org/456570)
+ SymbolView: Fix filtering not working. [Commit.](http://commits.kde.org/kate/f7b35107d4475c2104cf78492e18be606c5c17d4) Fixes bug [#387723](https://bugs.kde.org/387723)
+ Fix connect statement. [Commit.](http://commits.kde.org/kate/eb593437202e3b3f895aa87a2eb241524dba3fa7) 
+ GitWidget: Create ContextMenu for index at pos. [Commit.](http://commits.kde.org/kate/aef9d7ce4f34b8464ae9f1c2357de48a47353a46) 
+ ExternalTools: Fix checkExec. [Commit.](http://commits.kde.org/kate/c3014eb7074906970fcde1325bcf60644fd85a89) 
+ Katesql: Include &lt;QActionGroup&gt; to fix the Qt6 build. [Commit.](http://commits.kde.org/kate/1fcb62b2c491b0914910321038354d4b185b4705) 
+ Katesql: Port from Qt:SystemLocaleDate to QLocale. [Commit.](http://commits.kde.org/kate/5e2b46a5508d0820df362ac96a97f9dd269ff607) 
+ Restore find_package call for Qt{5,6}Sql. [Commit.](http://commits.kde.org/kate/bd727bc37eb41890c5b8ca149f8142eb34c849a7) 
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Show header in qtc6. [Commit.](http://commits.kde.org/kcalutils/f832b6370f3cc07ef63b8a219b721feb3c602e5e) 
{{< /details >}}
{{< details id="kdenetwork-filesharing" title="kdenetwork-filesharing" link="https://commits.kde.org/kdenetwork-filesharing" >}}
+ Adapt to using Kirigami.ScrollablePage a bit better. [Commit.](http://commits.kde.org/kdenetwork-filesharing/b45de09daa6a3324e11e7d03ddf5c01351f0bf26) 
+ Don't require that the samba shares group has "samba" in it. [Commit.](http://commits.kde.org/kdenetwork-filesharing/147ecc6931a162f7c9093dd35047b01fd3c38054) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Revert "Fix possible crash on profile switch, based on a contribution from Ivan Sudakov.". [Commit.](http://commits.kde.org/kdenlive/b9c9e4b423b3ef9c4b10de7935abfc558994acc9) 
+ Revert "Try to fix project profile corruption.". [Commit.](http://commits.kde.org/kdenlive/1fe9b7de4c92fbfe76be7dbb5328e7fee5c55e4a) 
+ Fix ambiguous widget name. [Commit.](http://commits.kde.org/kdenlive/19073c3fb1528e14fb8815e93fb6407f2cb618e3) 
+ Fix compilation with KF5 5.86. [Commit.](http://commits.kde.org/kdenlive/9018c1f8147b7983e22d587be02bf46eecba8cb1) 
+ Update kdenliveeffectscategory.rc adding typewriter to Stylize. [Commit.](http://commits.kde.org/kdenlive/dc05232b93fc72f24c1c18bf060829f6b5836cea) 
+ Clearify UI representation of mixes. [Commit.](http://commits.kde.org/kdenlive/10416866100707c17bbc8688e39a3167f3228a71) 
+ Try to fix project profile corruption. [Commit.](http://commits.kde.org/kdenlive/f8f8000f2bf3b1d0aef60a6b82bcf24ec6f2d8bf) 
+ Fix possible crash on profile switch, based on a contribution from Ivan Sudakov. [Commit.](http://commits.kde.org/kdenlive/db4ac94bc4880205230dff0de37ba61ad0c1f4f2) 
+ Added xml UI for the avfilter CMakeLists.txt. [Commit.](http://commits.kde.org/kdenlive/9e4283b048818caf9cc4c059fbbcd7eabd62dee6) 
+ Added xml UI for the avfilter.shear. [Commit.](http://commits.kde.org/kdenlive/5802b7032fede504f086812b0176fe52b4bd8071) 
+ Added xml UI for the avfilter.scroll. [Commit.](http://commits.kde.org/kdenlive/32887b8ca893628b7c52a0c37fcbfb85f571bb3a) 
+ Added xml UI for the avfilter.photosensitivity. [Commit.](http://commits.kde.org/kdenlive/320f98c1a6c7213ad1807cf290cc487d50f7d006) 
+ Added xml UI for the avfilter.monochrome. [Commit.](http://commits.kde.org/kdenlive/a8cc17becd68d4fd4b67106eba0e63502a5ed62c) 
+ Added xml UI for the avfilter.median. [Commit.](http://commits.kde.org/kdenlive/9d8ae43ea3116cb3ebf31ce32bd230092d27944c) 
+ Added xml UI for the avfilter.kirsch. [Commit.](http://commits.kde.org/kdenlive/755a27d6edbb42068a48b376e9aae0b64c204505) 
+ Added xml UI for the avfilter.exposure. [Commit.](http://commits.kde.org/kdenlive/c793da5e69d8ad931060f9c270e917213579f923) 
+ Added xml UI for the avfilter.epx. [Commit.](http://commits.kde.org/kdenlive/62d3c0ea68724ccddc7e9fcb5d947e5be954ce5f) 
+ Added xml UI for the avfilter.colortemperature. [Commit.](http://commits.kde.org/kdenlive/e853f18b4ae475c91729cc46a3fa84be9e1f0874) 
+ Added xml UI for the avfilter.colorize. [Commit.](http://commits.kde.org/kdenlive/e8b40071ba3e8bf8191237cc92899eefc976fc0a) 
+ Added xml UI for the avfilter.colorcorrect. [Commit.](http://commits.kde.org/kdenlive/b199dd7be6bb8b7b0cdae7cf3da2645741c0e32a) 
+ Added xml UI for the avfilter.colorcontrast. [Commit.](http://commits.kde.org/kdenlive/216976eb78577fad648bcb4bee57088582b1193d) 
+ Added xml UI for the avfilter.chromanr. [Commit.](http://commits.kde.org/kdenlive/87477110816c8b92b4660dd02f23162a47176685) 
+ Added xml UI for the avfilter.cas. [Commit.](http://commits.kde.org/kdenlive/38908560ed065dec2cd6869f2da17c55b0eea39e) 
+ Added xml UI for the avfilter.bilateral. [Commit.](http://commits.kde.org/kdenlive/f6b4c6fdab65e70da570c07ee997c85470f4aa7c) 
+ Update kdenliveeffectscategory.rc. [Commit.](http://commits.kde.org/kdenlive/5203c16e0d41653777f0aedc2c5625c847081377) 
+ Updated blacklisted_effects.txt. [Commit.](http://commits.kde.org/kdenlive/3080019c86f825e9e26f95e12ba9149fffb750e6) 
+ Updated CMakeLists.txt for frei0r effects. [Commit.](http://commits.kde.org/kdenlive/5cfb69b2f6926afc7d5fb2ede88a75089de70893) 
+ Added xml interface for the frei0r_bigsh0t_eq_to_stereo. [Commit.](http://commits.kde.org/kdenlive/25adfd245614a08e7cb4e3fd3eb32110b3110f49) 
+ Fix crash in proxy test dialog. [Commit.](http://commits.kde.org/kdenlive/6a0ff188e5f1c421963ee198f52c2d21b0162acf) 
+ Fix proxy extension not changed when setting changed, fix proxies not rebuilt on param change. [Commit.](http://commits.kde.org/kdenlive/db9c2de73c6589ff16ba6d47c2a1b542416cf3b1) 
+ Fix project cache folder not created on new document, causing thumbs to be recreated on opening. [Commit.](http://commits.kde.org/kdenlive/38f68854393ec21656acc10cf557c8f3a7ab2dad) 
+ Fix proxy aborting on unknown stream type. [Commit.](http://commits.kde.org/kdenlive/58c4b3f4a0bcafd5b5c960cefa965b1468d1b9f8) 
+ Fix proxy resize with nvenc. [Commit.](http://commits.kde.org/kdenlive/976089217a97e0164b1fdd1566b53f82109dc59f) 
+ Fix cast to double moved outside division. [Commit.](http://commits.kde.org/kdenlive/b3e783329477205cfbcd5d570bd31497e2dd6933) 
+ Fix wrong use of useSourceProfile. [Commit.](http://commits.kde.org/kdenlive/3e34893605df4a4a787e471a7e04d6bced201e88) 
+ Extract frame: fix incorrect handling of sar!= 1 profiles and incorrect use of useSourceProfile. [Commit.](http://commits.kde.org/kdenlive/0b4af095c1bf115a124505c4b4261e56d97599f1) 
+ Render preset edit: allow specifying a file extension (for example mkv for matroska format). [Commit.](http://commits.kde.org/kdenlive/a51b140a16fde34118d8ae5969a7a64e14d4d007) 
+ Edit render profile: make most parameters optionnal, allow editing parameters text. [Commit.](http://commits.kde.org/kdenlive/364d2f2f3ceefd27d82dac7e3196be3cd564e644) 
+ Only allow one selected render profile. [Commit.](http://commits.kde.org/kdenlive/90aaecd6e9d083615966e3efe816b714a018757e) 
+ Fix incorrect shortcut sequence, spotted by Eugen. [Commit.](http://commits.kde.org/kdenlive/19d6503f23b63b5ec173be91d0028279bd1ad4fe) 
+ Don't show monitor ruler duration tooltip if no zone is set. [Commit.](http://commits.kde.org/kdenlive/a3afe0edba0e9a07ea2f78a2fc65d8abc22d7f2a) 
+ Fix compile warning. [Commit.](http://commits.kde.org/kdenlive/3fee49e33d38d7853c93f76794accedf95e6d881) 
+ Fix filtering TreeItem lists by non-ASCII strings. [Commit.](http://commits.kde.org/kdenlive/9648cb12294da4295654d4e7abc03e1d7b0b2903) Fixes bug [#432699](https://bugs.kde.org/432699)
+ Add test for non-ascii list filtering (bug 432699). [Commit.](http://commits.kde.org/kdenlive/07ad0cf79e33ea41ac60bae1abb2f16d4eda7245) 
+ Test histogram handling RGB/BGR. [Commit.](http://commits.kde.org/kdenlive/1b0a6e3e45955b2798164c99d48edd064b439d88) 
+ Use QImage::pixel() in rgbparadegenerator.cpp. [Commit.](http://commits.kde.org/kdenlive/23d98aa7e66aa1628ff92c31c19018c9413dbe7d) 
+ Use QImage::pixel() in waveform. [Commit.](http://commits.kde.org/kdenlive/faf73f361eab4086086996db1e00b89ac7803b7f) 
+ Test waveform RGB/BGR handling. [Commit.](http://commits.kde.org/kdenlive/25b33e15aa085f7f76e4dd3b3ae8f925f34378f2) 
+ Change vectorscope to use QImage::pixel(). [Commit.](http://commits.kde.org/kdenlive/e0f68346d015180a0ccce2fd740bb3598f359a4a) Fixes bug [#453149](https://bugs.kde.org/453149)
+ Test vectorscope switching red and blue. [Commit.](http://commits.kde.org/kdenlive/9eca9f074c28203a8ebdbf8ea928af1adcc96f24) 
+ Fix proxy incorrectly scaled to 200px width when creating new project. [Commit.](http://commits.kde.org/kdenlive/2c1f5ee7b2cee806d481bd422a47aee6de8abcc0) 
+ When proxy clip is deleted, ensure the proxy context menu action is unchecked. [Commit.](http://commits.kde.org/kdenlive/fe68811fb40ff9d512dfecfa60910a5180fecb04) 
+ Fix vaapi proxy encoding profile, switch prores to use proxy quality. [Commit.](http://commits.kde.org/kdenlive/050dd0971c42c1124f4245dcd458309d73de4f5f) See bug [#436358](https://bugs.kde.org/436358)
+ Ensure monitor is paused when extracting a frame. [Commit.](http://commits.kde.org/kdenlive/7c5cf283b7c0cdfea14fdfb0598237e88aca2002) 
+ Ensure dropped frames timer stops when playing stops. [Commit.](http://commits.kde.org/kdenlive/e9fc92ecf85e443664e9eba9c0d7f10b8003e2c3) 
+ Fix extract frame for playlist clips. [Commit.](http://commits.kde.org/kdenlive/96ecfe35ee7b9196c8c9a3dbdbaea73a3e6d10ff) 
+ Extract frame: process in another frame so we don't block the UI, make sure effects are applied. [Commit.](http://commits.kde.org/kdenlive/26bcbb6fb920b86ea92970da87d0502816d84ca8) 
+ Fix document folder incorrectly set on loading project with "Use parent folder as project folder". [Commit.](http://commits.kde.org/kdenlive/4ad3de1bc2dfac3194878362fdf6ff4887c7f4a9) 
+ Render last frame. [Commit.](http://commits.kde.org/kdenlive/e525ae5e93061f4a5c21a25d10482e5272249f28) 
+ Don't crash loading project with incorrect subtrack count. [Commit.](http://commits.kde.org/kdenlive/46dcfd32b090d22926ace916e426cf298861fa40) 
+ Export guides: remember last used format, add reset button to restore default settings. [Commit.](http://commits.kde.org/kdenlive/13ae5abeba02eea6c15905adcff2aa188cd8071f) 
+ Export guides: allow using HH:MM:SS:FF timecode for export. [Commit.](http://commits.kde.org/kdenlive/bfbbaec2b161f93e4da417c440ccad0b15fcd17f) 
+ Fix timeline audio record broken after pause/play. [Commit.](http://commits.kde.org/kdenlive/c2db6fe127adc34f1075660cf3de8a3e34f6bc03) 
+ Fix timeline duration offset of -1 frame. [Commit.](http://commits.kde.org/kdenlive/bac847b699b6fb5705c8a3eb92af08a1bb65ac07) 
+ Fix possible crashes on invalid track position. [Commit.](http://commits.kde.org/kdenlive/df1b839548806f2c95edea3d5e7e8a479fce3a1b) 
+ Fix clip selected when not ready on duplicate, leading to incorrect display in clip monitor. [Commit.](http://commits.kde.org/kdenlive/63548341b42ba433c54d42006e6386af30d29ef1) 
+ Don't add a keyframe on double click unselected clip. [Commit.](http://commits.kde.org/kdenlive/1908bbea5f848dd6110c128fe9fd37d1ff8a32ff) 
+ When copying effect with keyframes, don't copy keyframes that are past the clip end. [Commit.](http://commits.kde.org/kdenlive/ee14310617d255beaa0eae7b3a43ae17c9bbcf3f) 
+ Fix possible crash on opening shortcuts dialog. [Commit.](http://commits.kde.org/kdenlive/9e11b4d636cb6f902f399c6627bc1a0d963299bc) 
+ Fix crash in debug mode when dragging a composition into timeline. [Commit.](http://commits.kde.org/kdenlive/0ff5f95e08d212ad3af37c0256ffd9c3d9479212) 
+ Manually register newer mime types for older OSes. [Commit.](http://commits.kde.org/kdenlive/64428376edc96cde1fc7ec2564635569bd403d3c) 
+ Always inform user if a file write fails. [Commit.](http://commits.kde.org/kdenlive/44935f201bc46d07bc32cb79b5db92b4847541f4) 
+ Fix changing cursor position when trying to resize effect zoombar. [Commit.](http://commits.kde.org/kdenlive/e2046ae1fc43ec058b08b577433432acd5e74d27) 
+ Fix save effect stack broken if there is only 1 effect in the stack. [Commit.](http://commits.kde.org/kdenlive/c4c5cbd626242ce4a8dfc9df7d094e3e097d3a7a) 
+ Fix Insert Zone to Bin out point off by 1. [Commit.](http://commits.kde.org/kdenlive/d38f885b9115ad53e9ebf61def968480109f604a) Fixes bug [#455883](https://bugs.kde.org/455883)
+ Fix editing clips in external apps on Mac. [Commit.](http://commits.kde.org/kdenlive/3ae19d7a8d8ab83bc6cbe51892303ce418109335) 
+ When aborting document load, open a new blank document. [Commit.](http://commits.kde.org/kdenlive/86980c457d4d022f7566df375b1b54349d743a3e) 
+ Fix compilation. [Commit.](http://commits.kde.org/kdenlive/0b8ac5a34b6b6cec3504e2d9c536d7e5c5d486a9) 
+ Show proxy and metadata tabs on new project creation and correctly set their values to the document. [Commit.](http://commits.kde.org/kdenlive/9b882121e3d3e4a20d1eba8676556111ee44c591) 
+ When source clip and proxy are both missing, propose to recreate proxy (or reuse existing on in case of LRV). [Commit.](http://commits.kde.org/kdenlive/7730d39bd924bdbe57def3ffd7fbf32cf4330c5f) See bug [#456185](https://bugs.kde.org/456185)
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ Remove usage of npw disfunctional KNewStuff upload feature. [Commit.](http://commits.kde.org/kdevelop/bfedc087f6dd3693c1a989b87ed8c800cfc58aeb) 
{{< /details >}}
{{< details id="khelpcenter" title="khelpcenter" link="https://commits.kde.org/khelpcenter" >}}
+ Fix most systemsettings and kinfocenter KCM not being shown. [Commit.](http://commits.kde.org/khelpcenter/ebd1c31c477c4a5a22fb59837f153684a857e050) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Support more image formats. [Commit.](http://commits.kde.org/kio-extras/81a71588fbc590e0bd2a27e74cd7eb2cf910d5c4) 
{{< /details >}}
{{< details id="kio-gdrive" title="kio-gdrive" link="https://commits.kde.org/kio-gdrive" >}}
+ Fully port away from Q_FOREACH. [Commit.](http://commits.kde.org/kio-gdrive/3d86169c6ba7d40ea35a45c4a0b71e95f54d11f1) Fixes bug [#456975](https://bugs.kde.org/456975)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Handle all DB language variants for ical events. [Commit.](http://commits.kde.org/kitinerary/d4a977b86f19c5766c00152458e8777830ad36a6) 
+ Extract full itenerary from Deutsche Bahn ical events. [Commit.](http://commits.kde.org/kitinerary/54a0be21dba8801b4412de39a504efd0e11cf027) 
+ Handle alternative MAV barcode format. [Commit.](http://commits.kde.org/kitinerary/c736c837675c4a57d4855985c104f8236f0c8401) 
+ Fix filter pattern matching against null bytes in binary content. [Commit.](http://commits.kde.org/kitinerary/8dec9ce21e34e5abd52f7e1894b42fd5f8a530b5) 
+ Improve booking.com address parsing. [Commit.](http://commits.kde.org/kitinerary/e69593ecc8140fead32136a8e57ffe3ed3735f7c) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Add FIXME comment. [Commit.](http://commits.kde.org/kmail/0a492e5c994310f4412455844cb22d01a888289c) 
+ Hide warning when we change mdn. [Commit.](http://commits.kde.org/kmail/09b81b8b5f47f910e47166d7e1b3b861fb2d05aa) 
+ Set response. [Commit.](http://commits.kde.org/kmail/335c285f95e7560e6f3f0c48c239a72f1a0d728c) 
+ Use finished signal. [Commit.](http://commits.kde.org/kmail/9ec809269ae035234182bf29d3cbb2991d2f81f4) 
+ Fix assert when using an empty tab to load a folder. [Commit.](http://commits.kde.org/kmail/72bca354fa3bf265024e4e8ddeb711ade9e2c68b) 
+ Continue to implement mdn job. [Commit.](http://commits.kde.org/kmail/1ead22c417421e88a47835267253e93a0008512b) 
+ Fix show headers in qtc6. [Commit.](http://commits.kde.org/kmail/9dd413c7f2f94f0f85f06ba3585aed56ec6c0127) 
{{< /details >}}
{{< details id="kmines" title="kmines" link="https://commits.kde.org/kmines" >}}
+ Use KGameClock::FlexibleHourMinSec to show hour when needed. [Commit.](http://commits.kde.org/kmines/b6c99ae9dbfea9fae9feb904f631914e3ada3a2b) 
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Use the correct path for the Cookies KCM for KIO >= 5.95. [Commit.](http://commits.kde.org/konqueror/760b1a760368b4c80fd9de35a07f17b37a8567b0) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Add missing copyright/license. [Commit.](http://commits.kde.org/konsole/889503e10d13b20009fe92a7892fa1a827a12419) 
+ Add some comments to VT state machine. [Commit.](http://commits.kde.org/konsole/4ad061547115ce5f869de1ea918e6d415d04c672) 
+ Fix three coverity issues. [Commit.](http://commits.kde.org/konsole/cf226215226251e09f60d156092da182bca1a2f9) 
+ Increase test wait to avoid failures after 416941b7. [Commit.](http://commits.kde.org/konsole/23ff7d5affc67e6ce62ec35308418656dd3b864c) 
+ Revert "Improvements to Session::getDynamicTitle()". [Commit.](http://commits.kde.org/konsole/80f33b011f3cb2e0bcc76b01cb5de163e56ae2f5) 
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Drop unused KF5Service dependency. [Commit.](http://commits.kde.org/korganizer/a776751f15198867fed73ad62841955b514182d5) 
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Show headers in qtc6. [Commit.](http://commits.kde.org/kpimtextedit/9be3f195152e8d6131f1bba17be295b444ad1803) 
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Add MAV UIC code for HU coverage areas. [Commit.](http://commits.kde.org/kpublictransport/346285ce091dd178b496957e17bcc820e5ab1077) 
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ MainWindow: Remove workarounds for QX11EmbedContainer. [Commit.](http://commits.kde.org/krdc/17a78f278bbbca49cd5bf12badfacca2171a9387) Fixes bug [#436650](https://bugs.kde.org/436650)
{{< /details >}}
{{< details id="ktorrent" title="ktorrent" link="https://commits.kde.org/ktorrent" >}}
+ Explicitly set progress bar to horizontal mode. [Commit.](http://commits.kde.org/ktorrent/e24e964aa2b40547d51240c626e98e8a4f38dc22) Fixes bug [#455451](https://bugs.kde.org/455451). Fixes bug [#455367](https://bugs.kde.org/455367)
{{< /details >}}
{{< details id="kwordquiz" title="kwordquiz" link="https://commits.kde.org/kwordquiz" >}}
+ Allow users to still open a file even if it's locked. [Commit.](http://commits.kde.org/kwordquiz/4d28a1a04c1897016228dd51e3bf24a3a8854037) 
+ Fix crash if opening a file fails. [Commit.](http://commits.kde.org/kwordquiz/3b3d851d709c81514f78d11559fba35207041c53) Fixes bug [#456710](https://bugs.kde.org/456710)
+ Initialize variables. [Commit.](http://commits.kde.org/kwordquiz/67fcffb974fe52c5b99d8de226af15f103c1b782) 
{{< /details >}}
{{< details id="libkdepim" title="libkdepim" link="https://commits.kde.org/libkdepim" >}}
+ Fix show headers in qtc6. [Commit.](http://commits.kde.org/libkdepim/21707be7c76aa47d6d583f88d4fb086a77a52646) 
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ Show headers in qtc6. [Commit.](http://commits.kde.org/libksieve/fa3381eb9e26f53f585bfbbd0a3a2b1a4d910544) 
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Add KMime::MDN::SendingMode settings. [Commit.](http://commits.kde.org/mailcommon/0724ce4a650977d444405409629335f1c70d8b3f) 
+ Fix dependancy. [Commit.](http://commits.kde.org/mailcommon/7445e15954672f781308b2c50f3b9455a9cb9312) 
+ Port code. [Commit.](http://commits.kde.org/mailcommon/a225275ed8b758621545c3fcec38cfdaf4ffcf79) 
+ Use directly modifyItem. [Commit.](http://commits.kde.org/mailcommon/55a3d630bc20888d8b9296999c14153413c2a69c) 
+ Fix mem leak + add signal finished. [Commit.](http://commits.kde.org/mailcommon/ac50406f5caf1f468140f70d761e0bc1ec272f34) 
+ Remove unused signal. [Commit.](http://commits.kde.org/mailcommon/ce338dc5ec5571e5866a7e1ceee544490a0bb6ba) 
+ Show headers in qtc6. [Commit.](http://commits.kde.org/mailcommon/a0f27187c7ab42a5fe1ff77ee396d5a62dac9ea2) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Move as static method. [Commit.](http://commits.kde.org/messagelib/76ff453517da3ee924be5f0c7424c32273c0eaaf) 
+ Add missing find_dependency calls. [Commit.](http://commits.kde.org/messagelib/3edc93673f94604c203be1c899dffb25575985db) 
+ Show headers in qtc6. [Commit.](http://commits.kde.org/messagelib/7f9ef58e41b020e1782a2b4136f707a7ac95fc79) 
+ Compile warning--. [Commit.](http://commits.kde.org/messagelib/6d68155199690412307d6beb58b19f0a5dad57ea) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Clang-tidy-14. [Commit.](http://commits.kde.org/okular/0a571b84df7e599382ed808262459abb9450652d) 
{{< /details >}}
{{< details id="parley" title="parley" link="https://commits.kde.org/parley" >}}
+ Drop unused kcmutils dependency. [Commit.](http://commits.kde.org/parley/af80b45a1c7e8f1aa12226696f80911209ef6110) 
{{< /details >}}
{{< details id="pim-data-exporter" title="pim-data-exporter" link="https://commits.kde.org/pim-data-exporter" >}}
+ Disable for the moment. [Commit.](http://commits.kde.org/pim-data-exporter/409b56daf3c31cca899e263c72662ba8f7749955) 
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" link="https://commits.kde.org/pim-sieve-editor" >}}
+ Show headers in qtc6. [Commit.](http://commits.kde.org/pim-sieve-editor/32923a06b8ab16ece01c2d08891e4262fb173e0c) 
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Fix Bug 456578 recipient auto-completion does not work in master git snapshots. [Commit.](http://commits.kde.org/pimcommon/8dbb1ce882023f2c106179b6fcc43980928c5db9) Fixes bug [#456578](https://bugs.kde.org/456578)
+ Show header in qtc6. [Commit.](http://commits.kde.org/pimcommon/d27df42fc976c92b0c62f5f4dea20cd43cd646f9) 
{{< /details >}}
{{< details id="skanpage" title="skanpage" link="https://commits.kde.org/skanpage" >}}
+ Make save location field read-only. [Commit.](http://commits.kde.org/skanpage/a02d66853ecd74a68db007bb942ae5c5b98fdc59) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Using ESC while in annotation mode will close annotator instead of closing spectacle. [Commit.](http://commits.kde.org/spectacle/dd1d7baecd7a3d9356992151e57f1c380e9500c8) Fixes bug [#456823](https://bugs.kde.org/456823)
{{< /details >}}
{{< details id="step" title="step" link="https://commits.kde.org/step" >}}
+ Help finding GSL header. [Commit.](http://commits.kde.org/step/fc0fb386ffa22f96f7f4d70a584b077cd4181e9a) 
+ Fix missing __PRETTY_FUNCTION__ for MSVC. [Commit.](http://commits.kde.org/step/36f43d088e244b90dd20bf8832923fc25c1fae7b) 
{{< /details >}}
