---
aliases:
- ../../fulllog_releases-21.08.3
title: KDE Gear 21.08.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Cleaup AddJob on destruction. [Commit.](http://commits.kde.org/ark/b82fcaa37abac01547c28e948f4f1e253b8115ea) Fixes bug [#442774](https://bugs.kde.org/442774)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Remove min Android SDK version from Manifest. [Commit.](http://commits.kde.org/elisa/3eaca584015ab0190d16cd2a5eb7ddde26bbd693) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Include iso-codes data files needed by KF::I18n 5.88. [Commit.](http://commits.kde.org/itinerary/2f9739ff9118fe0369add65f3d9392e40ca3ea43) 
+ [EventDelegate] Hide location name if there is none. [Commit.](http://commits.kde.org/itinerary/3d04869e5f2c148f1b03ced179ee8faab88d07a4) 
+ [Delegates] Use preferredWidth instead of width. [Commit.](http://commits.kde.org/itinerary/addd131b5a7c35f55f2579e908d9cc664b126152) 
+ Prevent map panning in the location picker switching to the previous page. [Commit.](http://commits.kde.org/itinerary/b0dcd6066487a04a74caca650794909611be2b6d) 
+ For departureCountry check departureLocation. [Commit.](http://commits.kde.org/itinerary/9ead4270165b79adc6d65263891fc11dd67a6c12) 
+ Don't let checkbox labels force the minimum page width. [Commit.](http://commits.kde.org/itinerary/d5099a87943e32ed00ebe91d0153c484b295a6f2) Fixes bug [#443200](https://bugs.kde.org/443200)
+ Add 21.08.2 changelog. [Commit.](http://commits.kde.org/itinerary/b308dbab58e659b2d9f0fb41afc547318c4d4199) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix display errors in time edit field arrows. [Commit.](http://commits.kde.org/kalarm/f01b106bdae3291bc854d69b8891309b0e3d371b) 
+ Update change log. [Commit.](http://commits.kde.org/kalarm/0282a1c25bc237ae8e59cbfa9adb23ab8ba45451) 
+ Fix formatting using Fusion application style. [Commit.](http://commits.kde.org/kalarm/44e8014095afcadbe5ef2f2950273fd0550bf2e8) 
+ Remove debug code. [Commit.](http://commits.kde.org/kalarm/9db0a4e3851db8ccb72f57f6fe0def9f775cf05d) 
+ Fix formatting of times in alarm list, when "am"/"pm" are translated. [Commit.](http://commits.kde.org/kalarm/a437719639eab8d4060bd1f1baada03059db35b0) 
+ Use translated forms of "am" and "pm" when displaying times. [Commit.](http://commits.kde.org/kalarm/3a5927dddf9dc38101e99c1c90b2cac961647f93) 
+ Fix build. [Commit.](http://commits.kde.org/kalarm/e59b9c7e2e6791f1a0f95fa3b998a2fc7ea000e2) 
+ Bug 443062: Allow stepping hour in time edit fields, in Breeze app style. [Commit.](http://commits.kde.org/kalarm/873e617e490f7aab4b5631c1c5ab704ddfee0eeb) 
+ Bug 443062: Make time edit fields work for Breeze application style. [Commit.](http://commits.kde.org/kalarm/b6060e197ea4e76db788a39acf13098db5d9a303) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ [handler] Enable highdpi pixmaps. [Commit.](http://commits.kde.org/kdeconnect-kde/08a9e3b25f33cda01abd649c2f2938cb80ecf5a1) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Update catch.hpp. [Commit.](http://commits.kde.org/kdenlive/adc86cfe0f2df232106530e5ceef576a93a856b4) See bug [#440867](https://bugs.kde.org/440867)
+ Timeline clip drop: add id to each drag operation to avoid incorrectly interpreting a new drag operation as the continuation of a previous one. [Commit.](http://commits.kde.org/kdenlive/a70cccc9508b996c5122ccd15db2ef5a49c25700) 
+ Fix muting audio master broken. [Commit.](http://commits.kde.org/kdenlive/abfea9bbd158675dd89c553561b34c989d5e917f) 
+ Fix various mix resize/align issues. [Commit.](http://commits.kde.org/kdenlive/d9234c1cc931c27a2c7e99793293a99b6cb96a15) 
+ Fix proxy clips not archived when requested. [Commit.](http://commits.kde.org/kdenlive/783204cc14e0887afae90f3a623e88cf92fd59d2) 
+ Fix wipe and slide transition incorrect behavior on resize, and incorrectly detecting "reverse" state. [Commit.](http://commits.kde.org/kdenlive/231b29cd20a83bf8df182fa09ddd19023aea6643) 
+ Fix same track transition if one clip has no frame at its end. [Commit.](http://commits.kde.org/kdenlive/b3552c983b3d190104ee71992535402bc54d745a) 
+ Fix crash and incorrect resize with same track transitions. [Commit.](http://commits.kde.org/kdenlive/761b20a919150be751b610a9f8a21cf94b9356f2) 
+ Fix mix cut position lost on paste. [Commit.](http://commits.kde.org/kdenlive/15074cb7dc133f48082053799e604fa570d1985b) 
+ Fix one cause of crash related to multiple keyframes move. [Commit.](http://commits.kde.org/kdenlive/3fdcfe16c76216fd16c77c6604d7ec753b13b0a6) 
+ Fix proxying of playlist clips. [Commit.](http://commits.kde.org/kdenlive/c22dbb8ab8e51d30b17303dc80f23c470cfa736f) 
+ When a clip job creates an mlt playlist, check if the file is already in project to avoid double insertion. [Commit.](http://commits.kde.org/kdenlive/307f55aad74780072adb65402400238bdc5bdd52) 
+ Fix clip with mix cannot be moved back in place. [Commit.](http://commits.kde.org/kdenlive/d906e739b5c05b42d50063e5259a56933223b09e) 
+ Fix loop mode broken on add effect. [Commit.](http://commits.kde.org/kdenlive/f1132ba3f5a4ea38b007668d0995a9676b2ddd13) 
+ Fix replacing AV clip with playlist clip broken. [Commit.](http://commits.kde.org/kdenlive/7f5645d1255f8b50959e44f88504ba30c212cd6e) 
+ Fix export frame broken for title clips. [Commit.](http://commits.kde.org/kdenlive/d2937a31692501cb96390b725515e0877c731462) 
+ Fix bin thumbnail hover seek not reset when leaving thumb area. [Commit.](http://commits.kde.org/kdenlive/45da0f7588d33a3cb10bb6a6a5abec27b8b7ac04) 
+ Project bin:when hover seek is enabled, restore thumb after seeking, set thumb with shift+seek. [Commit.](http://commits.kde.org/kdenlive/7bffc9fab3d8d0fa49b6cd2023e75f1d4ff96c40) 
+ Fix "adjustcenter" asset param in case where the frame size is empty. [Commit.](http://commits.kde.org/kdenlive/273f1238b06761c0e31edf27c93cab780b25dad3) 
+ Fix crash loading project with incorrectly detected same track transition. [Commit.](http://commits.kde.org/kdenlive/00cad95df068669b7bdbb13ff9544f96b946de7c) 
+ Fix install path of frei0r effect UI's. [Commit.](http://commits.kde.org/kdenlive/853762e6a9c664e0d457139d3156ea40061b2df7) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Man kio: fix crash due to bad memory pointer on REQ_ps handling. [Commit.](http://commits.kde.org/kio-extras/809863617a3fb76ce43f11dec541d4b94f63925a) Fixes bug [#443983](https://bugs.kde.org/443983)
+ Kio_filenamesearch: fix crash due to KCoreDirLister changes. [Commit.](http://commits.kde.org/kio-extras/c3d52ebecb7f4890e8a412930bce9c74fc9e2f1f) Fixes bug [#438187](https://bugs.kde.org/438187)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Handle English language MÁV ticket date formats. [Commit.](http://commits.kde.org/kitinerary/7b96a680b019b6408666a42663bc8177d2e5801a) Fixes bug [#444550](https://bugs.kde.org/444550)
+ Fix erroneous merging of train trips with the same line on the same day. [Commit.](http://commits.kde.org/kitinerary/963c7a7c3cf9e602fefefe27375aff0eb3255e44) 
+ Convert location info to the right type when necessary. [Commit.](http://commits.kde.org/kitinerary/2a6a1f8e2f69e52a3da9d0902386b5e94899eeb1) 
+ Add gomus.de extractor script. [Commit.](http://commits.kde.org/kitinerary/55d15b27ec3a573d84bf1f35a337944831307cfe) 
+ Use pkpass expirationDate field to guess a suitable end time for events. [Commit.](http://commits.kde.org/kitinerary/2256ddc635483597030c0b6cf9dc8808d745c4b4) 
+ Extract boarding group from Eurowings pkpass. [Commit.](http://commits.kde.org/kitinerary/be98aaae20b4ceffb030b26d772a8a5baffca8a6) 
+ Extract Cancel URL from Lufthansa pkpass. [Commit.](http://commits.kde.org/kitinerary/09c03384e9831a79f0b2a7eb556484c6d7997c30) 
+ Adapt unit tests to extractor improvements in 6597c1bdef25. [Commit.](http://commits.kde.org/kitinerary/5ae2266b9b3cd6b847d67ce3a6fb69af9aef16bc) 
+ Explicitly apply airport timezone when creating fake 23:59:59 time. [Commit.](http://commits.kde.org/kitinerary/e3d1daf4c3fc899f682eca002137e7eabc7d152d) 
+ Extract proper real name from Lufthansa and EuroWings pkpass. [Commit.](http://commits.kde.org/kitinerary/38c5ccee6feb5a0314d19ffb2154f1cf9595bb8f) 
+ Support German Eurowings booking confirmation. [Commit.](http://commits.kde.org/kitinerary/6966d67ca6978f98f8bc074feb57fb4a509bc13a) 
{{< /details >}}
{{< details id="kmahjongg" title="kmahjongg" link="https://commits.kde.org/kmahjongg" >}}
+ Don't generate random game numbers bigger than INT_MAX. [Commit.](http://commits.kde.org/kmahjongg/21f71613273f38454dc9644883f9e4b112af8d67) Fixes bug [#444188](https://bugs.kde.org/444188)
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Remove ASSERT here. [Commit.](http://commits.kde.org/kmail/3fe15552375aca94215bf39101529c27589b907f) 
{{< /details >}}
{{< details id="kmix" title="kmix" link="https://commits.kde.org/kmix" >}}
+ Fix crash by adding missing initialize part for KToggleAction. [Commit.](http://commits.kde.org/kmix/ade58189892f8dc0009d65387cfd6b307a5a0311) 
{{< /details >}}
{{< details id="kontact" title="kontact" link="https://commits.kde.org/kontact" >}}
+ Kcmkontact: Fix writing of configured startup module. [Commit.](http://commits.kde.org/kontact/03c663e2a8cdfa2caac2202192ff82bdf79e84ac) Fixes bug [#444170](https://bugs.kde.org/444170)
+ Use show/hide sidebar action. [Commit.](http://commits.kde.org/kontact/a90e0d9d43eee5c4d609b0cd2308991480fe2961) 
{{< /details >}}
{{< details id="kopeninghours" title="kopeninghours" link="https://commits.kde.org/kopeninghours" >}}
+ Fix evaluating single time open end ranges. [Commit.](http://commits.kde.org/kopeninghours/ee2b2104766e007ef68e08b95fe4e6124237b950) 
+ Don't turn 20:00-26:00 into 20:00-02:00. [Commit.](http://commits.kde.org/kopeninghours/e76f2e9fb2dec35950c141556d752e2140620866) 
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Revert "Bind keyboard shortcuts in the Search dialog's results". [Commit.](http://commits.kde.org/korganizer/436e107da1836ebe54538a94e00cb727ca7d2c4d) 
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Update SNCB certificate. [Commit.](http://commits.kde.org/kpublictransport/b945ff76069642c5b22f11b5eb0259087696dc90) 
+ Update OTP Ulm rental vehicle configuration. [Commit.](http://commits.kde.org/kpublictransport/b64e42f750d6ea3d759fcd4c45885d05c201e349) 
+ Pass along floating rented vehicle data correctly. [Commit.](http://commits.kde.org/kpublictransport/3d88fae8a6979f646b349a2b40409a58dbce8f38) 
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Naughty-list pixma network backend option polling. [Commit.](http://commits.kde.org/libksane/2bdbe999808a225f1cfa10f10a4fcf09e45444e4) Fixes bug [#429260](https://bugs.kde.org/429260)
{{< /details >}}
{{< details id="marble" title="marble" link="https://commits.kde.org/marble" >}}
+ Fix build with gpsd 3.23.1. [Commit.](http://commits.kde.org/marble/7211ed5afc1c2ff7334c421b9555888ac9850d46) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Make sure that index is valid. [Commit.](http://commits.kde.org/messagelib/766e3d796e552abdbfdb253a286a95de7e9ae089) 
+ Fix Bug 419978 - KMail missing line break in blockquote when showing as HTML mail. [Commit.](http://commits.kde.org/messagelib/6cc49743f3f9521e3a0f75083d9fd6c01e019250) Fixes bug [#419978](https://bugs.kde.org/419978)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Relax the check for non square DPI. [Commit.](http://commits.kde.org/okular/2002f4e7e94af8fce1b6e6d95b95cb3905166391) Fixes bug [#444168](https://bugs.kde.org/444168)
+ Make Quick Annotations button open the full toolbar when no Quick Annotations are configured. [Commit.](http://commits.kde.org/okular/99209e29f78bb8e530e5bdf626e07a871dad3b1b) 
+ CI: add the deb-src of testing since we're based on testing not unstable. [Commit.](http://commits.kde.org/okular/f9ca87c8a15897b40a5c33821e96a1e5343cc00a) 
+ Fix bookmark menu actions missing after switching tabs. [Commit.](http://commits.kde.org/okular/9e2d256a6f214f7230f8575f8a61570592d35428) Fixes bug [#442668](https://bugs.kde.org/442668)
+ Util.printd: Don't crash if we get an unexpected oDate argument. [Commit.](http://commits.kde.org/okular/913560eb78078ddd2b7e543d4cb26925c0d432e1) Fixes bug [#443255](https://bugs.kde.org/443255)
{{< /details >}}
{{< details id="print-manager" title="print-manager" link="https://commits.kde.org/print-manager" >}}
+ Remove applet list item animations. [Commit.](http://commits.kde.org/print-manager/f2a7e45238bbb937447987ab94c44ef49fdeccda) 
{{< /details >}}
