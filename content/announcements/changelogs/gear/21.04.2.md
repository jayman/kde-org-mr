---
aliases:
- ../../fulllog_releases-21.04.2
- ../releases/21.04.2
title: KDE Gear 21.04.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="artikulate" title="artikulate" link="https://commits.kde.org/artikulate" >}}
+ Fix invalid index on resource creation. [Commit.](http://commits.kde.org/artikulate/d4aaf42d02fab0aaf27ee7346f924d269eeac5c9) Fixes bug [#436510](https://bugs.kde.org/436510)
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Improve the printed to-do list. [Commit.](http://commits.kde.org/calendarsupport/24c4ece049ed2f63353d678f8f6854e6035fb9e0) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix crash on Wayland when closing the mainwindow while another dialog is open. [Commit.](http://commits.kde.org/dolphin/2f3d8c5c9664f66ff7922ad7fd612f65182c75d0) 
+ [src/settings/contextmenu/contextmenusettingspage] Fix Crash because of nullptr. [Commit.](http://commits.kde.org/dolphin/b5e276d115c55241b0b193ee184f3bd206da8d33) Fixes bug [#437539](https://bugs.kde.org/437539)
+ Search/facetswidget: Check for protocol before trying to fetch tags. [Commit.](http://commits.kde.org/dolphin/29636baff0f0cf8d769aa75b1c458b96cf83b828) Fixes bug [#435586](https://bugs.kde.org/435586)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix fields behavior when creating a radio. [Commit.](http://commits.kde.org/elisa/8bc7b2bdb132b7e27f41585ec8dc0d07be701a1f) 
+ Properly update album when removing one of its track. [Commit.](http://commits.kde.org/elisa/e2d32dd9171a6c0ea77f183dbf4b64c8bfb8b1a0) 
+ Elisa has a wonderful site, let's point to it. [Commit.](http://commits.kde.org/elisa/51689394b0454f377de70efd5abfdedbb36fa7d4) 
+ No need to emit trackModified when a track starts playing. [Commit.](http://commits.kde.org/elisa/7b8a7e3d6bbd402fc0461667d174417b63a8a338) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Fix corner cases in the Agenda and Month views. [Commit.](http://commits.kde.org/eventviews/4a74c17bae654b237272d1555fc0630bb98af3c3) 
+ Show recurring to-dos on DTDUE in the month view, like plain to-dos. [Commit.](http://commits.kde.org/eventviews/1c9c5afa245e0cb5a266a6f1180cae15ea0e9e90) 
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Simplify the code that saves incidences. [Commit.](http://commits.kde.org/incidenceeditor/c65c7939bf2de96333c9b303245d7e207a0ccac6) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Fix application version display in tarball builds. [Commit.](http://commits.kde.org/itinerary/d71cfd125a8f44896ac7321aac3c3faf9f9992e1) 
{{< /details >}}
{{< details id="juk" title="juk" link="https://commits.kde.org/juk" >}}
+ Add missing Qt5Concurrent to target_link_libraries. [Commit.](http://commits.kde.org/juk/3f25d7206db1e939270914b473c87a4e2780b732) 
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Don't ask if the user wants to overwrite two times. [Commit.](http://commits.kde.org/k3b/121c9e9e0ba7d0bd87bdd944c04961675f7a6d97) 
+ Fix showing of dialog the user can't interact with when saving. [Commit.](http://commits.kde.org/k3b/2a4b66e3a417559599d3bc61ce6497e17ec98472) Fixes bug [#437574](https://bugs.kde.org/437574)
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Bug 437676: In audio alarm edit dialog, don't show file name in encoded format. [Commit.](http://commits.kde.org/kalarm/725f7388fdd0feedf1e5891667e64380c62df0fd) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Prevent crash in git-blame for HUGE git commits. [Commit.](http://commits.kde.org/kate/b1b868d9fa09efcc89868cbe91d4a01d1a7aa0e5) Fixes bug [#437683](https://bugs.kde.org/437683)
+ Fix crash when turning on "bg shading" for Documents plugin. [Commit.](http://commits.kde.org/kate/1b0a87416031eb68e4d2acda25e0f69d7ed15360) 
+ Deactivate Kate command bar if KXMLGui command bar is there. [Commit.](http://commits.kde.org/kate/f75ce95498badcb47eae532bf21ec1f05c57dc4e) 
{{< /details >}}
{{< details id="kblocks" title="kblocks" link="https://commits.kde.org/kblocks" >}}
+ Extract all messages. [Commit.](http://commits.kde.org/kblocks/8c81ca4c21263afa6dcb92559828dea2044ed9d8) 
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Add "end time" to item viewer's display of events. [Commit.](http://commits.kde.org/kcalutils/808c49660f4143273c73ee28502615cb87c51898) Fixes bug [#438082](https://bugs.kde.org/438082)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ [SMS App] Fix message sending area from always having a white background. [Commit.](http://commits.kde.org/kdeconnect-kde/bd838d60d568a72eed16b1a2c3a50527c71bd101) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Remove duplicate line from last cherry-pick. [Commit.](http://commits.kde.org/kdenlive/e2da06720b9116199e6ef26ea006592f9b8734ac) 
+ Hopefully proper patch to solve "white" rendering issues. [Commit.](http://commits.kde.org/kdenlive/fcd82a3fdeddae478280ee098555449c8ecf46fa) 
+ Fix resize clip end does not allow touching next clip. [Commit.](http://commits.kde.org/kdenlive/e4c9bebe80056b0e633e175af40d2d54bb798f0a) 
+ Fix clip thumbs disappearing on timeline resize. [Commit.](http://commits.kde.org/kdenlive/f4a0821916263d41909aac83a680adb77e1d4dd0) 
+ Fix timeline thumbnails not saved with project. [Commit.](http://commits.kde.org/kdenlive/2144251aec50f46700e84a006105f962783bae89) 
+ Don't discard subtitle files on project fps change. [Commit.](http://commits.kde.org/kdenlive/e0e7d0ba71b98af286e14cfc75754541e5f4c338) 
+ Update guides position on project's fps change. [Commit.](http://commits.kde.org/kdenlive/42fdaec38aa3149ed8542849c3c3813d676ce5c0) 
+ Fix delete selected clips not working on project opening. [Commit.](http://commits.kde.org/kdenlive/e5ed85c026a3172d29a7a4dc28cd9e4ac952e167) 
+ Fix Chroma Key: Advanced edge mode normal was reset to hard. [Commit.](http://commits.kde.org/kdenlive/22c506e5c6545b850a1df3cc5eac75723d279e53) 
+ Fix various frei0r effects losing parameter settings:. [Commit.](http://commits.kde.org/kdenlive/082258a9580486056f54111481cd5f4d700e9a0d) 
+ Next try to fix keyframe view positon for mixes. [Commit.](http://commits.kde.org/kdenlive/b043a164dd4bce812dd6ae7dcd382cc276270941) 
+ Revert "Fix keyframeview position in mixes". [Commit.](http://commits.kde.org/kdenlive/39c32a8d8a7bed57a93bf3c9a45f2a1b23ef708f) 
+ Fix keyframeview position in mixes. [Commit.](http://commits.kde.org/kdenlive/0884e2475e72759ead696f1be98ac5c323b2b62c) 
+ Make effects keyframable: scratchlines, tcolor, lumaliftgaingamma. [Commit.](http://commits.kde.org/kdenlive/a93b4c33384f8c25c6f0b02ed301765832ef6b77) See bug [#393668](https://bugs.kde.org/393668)
+ Make effects keyframable: charcoal, dust, oldfilm, threshold.xml. [Commit.](http://commits.kde.org/kdenlive/0c838d06b00bd705ccd7f0d6542d63a7b6d95b76) See bug [#393668](https://bugs.kde.org/393668)
+ Make glitch0r effect keyframable. [Commit.](http://commits.kde.org/kdenlive/e02dec093867bda48bbe60159aece6bf84cbf314) See bug [#393668](https://bugs.kde.org/393668)
+ Fix profile repository not properly refreshed after change. [Commit.](http://commits.kde.org/kdenlive/97f6d0267601af947cf2396031d4bc0db35f858f) 
+ Fix marker monitor overlayer flickers on hover. [Commit.](http://commits.kde.org/kdenlive/873b2bee3976534d910bb06250c909fef5ff98b8) 
+ Ensure timeline zoombar right handle is always visible. [Commit.](http://commits.kde.org/kdenlive/441fc496f4c7cb6a7f91131624ca75f131b2ab14) 
+ Fix issue with duplicated title clips. [Commit.](http://commits.kde.org/kdenlive/45b9448697de91cd355aaa61773f2aa18980fa2d) 
+ Fix effect sliders on right to left (rtl) layouts. [Commit.](http://commits.kde.org/kdenlive/06a5cb12d557b1b22d854aea6365847165a37c88) Fixes bug [#434981](https://bugs.kde.org/434981)
+ Fix alignment of statusbar message label. [Commit.](http://commits.kde.org/kdenlive/db00e238c0a91f153a7fb1d66d5ff01ee89e73b9) Fixes bug [#437113](https://bugs.kde.org/437113)
+ Fix crash using filter with missing MLT metadata (vidstab in MLT 6.26.1). [Commit.](http://commits.kde.org/kdenlive/cf64f52756069cb13421760ce85ce49a7b18e3a3) 
+ Try to fix wrongly set color in titler. [Commit.](http://commits.kde.org/kdenlive/94e1d5b03e996e9ca5142eac0db5b8e3a33cbee3) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Only show the event address if there's anything substantial to show. [Commit.](http://commits.kde.org/kdepim-addons/0d76beb223bf4c703a56167b0534c17ea4a95cc7) 
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Make the EWS Configuration dialog fit on small screens. [Commit.](http://commits.kde.org/kdepim-runtime/e1cf5d691264f9435ea4b29b0277b9368194b909) Fixes bug [#436841](https://bugs.kde.org/436841)
+ Adjust the size of the iCal Calendar File properties dialog. [Commit.](http://commits.kde.org/kdepim-runtime/d2390ef45dcd2800ee6721a56b039331fc512839) Fixes bug [#435871](https://bugs.kde.org/435871). Fixes bug [#436448](https://bugs.kde.org/436448)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Thumbnail: Check shm size before writing to it. [Commit.](http://commits.kde.org/kio-extras/a288a7ba4283b2102a4602aa105072f33bc25645) See bug [#430862](https://bugs.kde.org/430862)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Fix station name comparison for unbound train tickets. [Commit.](http://commits.kde.org/kitinerary/db26073820646481981598fefc9e2d0839d384b1) 
+ Fix single digit day date parsing in oui.sncf confirmation emails. [Commit.](http://commits.kde.org/kitinerary/8b6f51373be797deab6f8cc28108b2d44ff796cc) Fixes bug [#437854](https://bugs.kde.org/437854)
+ Improve handling of multi-leg and return tickets in Oui.sncf summary emails. [Commit.](http://commits.kde.org/kitinerary/947babff7907c6cbe2ed099b1d91cb763f1d7f64) Fixes bug [#436976](https://bugs.kde.org/436976)
+ Fix off by one date in certain SNCF tickets. [Commit.](http://commits.kde.org/kitinerary/279f704a38100c233989710b51ea3c8cdca1e2fd) 
+ Apply the same departure day handling we have for flights also for trains. [Commit.](http://commits.kde.org/kitinerary/102d8aff8867049e233b79023478460835533a41) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Allow to load eml file. [Commit.](http://commits.kde.org/kmail/cd2311624056824351baef63ec2bcb1e10db698a) 
{{< /details >}}
{{< details id="kolf" title="kolf" link="https://commits.kde.org/kolf" >}}
+ Fix saving courses and games. [Commit.](http://commits.kde.org/kolf/37caba7321b62e29fb8642ff696d707683426b12) Fixes bug [#437318](https://bugs.kde.org/437318)
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ KonqCombo: Use new QComboBox signal and new-style connect. [Commit.](http://commits.kde.org/konqueror/f4a3fec5a229fea59eba15b8644a06a9e16e91a0) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Allow /bin/sh as a profile command. [Commit.](http://commits.kde.org/konsole/34ad14c8cee4ab352481bf2ba190df9f8b744d29) Fixes bug [#436242](https://bugs.kde.org/436242)
+ Fix alpha channel of scrollbar colors. [Commit.](http://commits.kde.org/konsole/585f9a561a07ec1cf5c20af37229667a3009362a) Fixes bug [#437223](https://bugs.kde.org/437223)
+ Don't override profile icon in "New Tab" menu. [Commit.](http://commits.kde.org/konsole/c29e734845950446f9d27e007f3b8486d7f5c0b1) Fixes bug [#437200](https://bugs.kde.org/437200)
+ Fix crash when using a color scheme with random colors. [Commit.](http://commits.kde.org/konsole/b246ac57ee8000e15e71d833701fbcab0b780f20) Fixes bug [#434892](https://bugs.kde.org/434892)
+ Fix type of randomSeed parameter. [Commit.](http://commits.kde.org/konsole/4177f5c01568b31f3d5c1ff20cde3841eec97f08) See bug [#434892](https://bugs.kde.org/434892)
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Use KPasswordLineEdit for all password edit fields. [Commit.](http://commits.kde.org/konversation/4aee005a6cc3f6431a63eb1938f44d552ea1d8c2) 
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Fix Bug 436880 - Kmail ignores right-to-left text direction. [Commit.](http://commits.kde.org/kpimtextedit/4c58a536926315c7528ee7bb4595d3374ffef4d0) Fixes bug [#436880](https://bugs.kde.org/436880)
+ Add autotest for Bug 436880. [Commit.](http://commits.kde.org/kpimtextedit/5313b6732772f2a38ad322523fe49d768eb6410a) 
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Fix parsing of numeric GBFS station ids. [Commit.](http://commits.kde.org/kpublictransport/d174917abf4f36c4e8de7b4b5a4482a88d05273e) 
{{< /details >}}
{{< details id="ktorrent" title="ktorrent" link="https://commits.kde.org/ktorrent" >}}
+ Do not attempt to download non-existing GeoIp data. [Commit.](http://commits.kde.org/ktorrent/ca128f13009c527339e28e305218ba01ea206f18) See bug [#403054](https://bugs.kde.org/403054)
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ Add Boost::headers to link libraries. [Commit.](http://commits.kde.org/libkleo/55aa004b8091018790445a6431284dd71755e42e) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix bug 206269 "An escaping problem also occurs with '&' in subject or. [Commit.](http://commits.kde.org/messagelib/6c4fd41ee3ffc5227f805d65091775d556486aef) Fixes bug [#206269](https://bugs.kde.org/206269)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ FormLineEdit: Don't run focusin action when gaining focus because of window management. [Commit.](http://commits.kde.org/okular/e457ceb73146790c5d4a9914182433ab4002815e) Fixes bug [#436990](https://bugs.kde.org/436990)
+ FormLineEdit: Don't run focusout/formatfield actions when losing focus because of window management. [Commit.](http://commits.kde.org/okular/b518c01f1c66a30ab2c32758c5267acecc96294a) Fixes bug [#435833](https://bugs.kde.org/435833)
+ FormLineEdit: Move editing=false up in the focus out event. [Commit.](http://commits.kde.org/okular/812adaa26d951d4bdb0262b2375752eab054a5f8) 
+ Mark items with name in toolsQuick as default. [Commit.](http://commits.kde.org/okular/92f4ade00cc540288307ba4b828101e979188936) 
+ Unbox alert() parameters when they are in an object. [Commit.](http://commits.kde.org/okular/81344f8aa8244197f4292210c4f0b31d871a3d28) 
+ Second attempt at fixing the windows/craft build. [Commit.](http://commits.kde.org/okular/99788b907e2fbaa56070b4f36b61ace9bd6a86b9) 
+ Fix spectre includes. [Commit.](http://commits.kde.org/okular/108e568d67cf2c67c620951240bfb5177dd7450c) 
+ Fix loading some png files inside zip comicbook files. [Commit.](http://commits.kde.org/okular/f31152d389fc5ce348f836c749d66e71a66f3949) Fixes bug [#436086](https://bugs.kde.org/436086)
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Platforms: Introduce PlatformKWinWayland2. [Commit.](http://commits.kde.org/spectacle/777038952edfe8b1eff0a2a732fd3d9d8171fc43) Fixes bug [#437652](https://bugs.kde.org/437652)
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Umbrello/version.h umbrello/main.cpp : Update copyright year. [Commit.](http://commits.kde.org/umbrello/7e15f5090f08abe98b69ee962580f8358a1200d7) 
+ Lib/cppparser/lexer.{h,cpp} followup to commit 7d3eb05,. [Commit.](http://commits.kde.org/umbrello/3041141b10361aedb3c8c06c9ba36b5d25ac6f9a) See bug [#338649](https://bugs.kde.org/338649)
{{< /details >}}
