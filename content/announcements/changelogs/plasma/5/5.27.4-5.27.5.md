---
title: Plasma 5.27.5 complete changelog
version: 5.27.5
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Applet: introduce a brief animation for section height estimates. [Commit.](http://commits.kde.org/bluedevil/1956411588dbed54acb672d89ca003d8806684c4) Fixes bug [#438610](https://bugs.kde.org/438610)
+ Applet: Fix Toolbar padding in RTL mode. [Commit.](http://commits.kde.org/bluedevil/a0a78aee96c8b69500ea59f4876fa50f09b8c7e4) 
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ ProgressBar: Stop emitting polish requests when item becomes invisible. [Commit.](http://commits.kde.org/breeze/47ba564392d42991931491740d02bb1aed8409b6) Fixes bug [#468903](https://bugs.kde.org/468903)
+ ToolButton: Fix subcontrol menu buttons outline in RTL mode. [Commit.](http://commits.kde.org/breeze/affec5c8704834bea4fd13b9d0d6ddf3c99fa37b) 
+ Remove unused methods. [Commit.](http://commits.kde.org/breeze/273e271374f12085be97e48a910be50971771283) 
+ ToolButton: Fix text & icon position in textUnderIcon RTL mode. [Commit.](http://commits.kde.org/breeze/ad94ebd85cd0972f9efc735be5094a7c3ef506a8) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ 🍒gtk3/titlebutton: reduce right margin of {max/minimize} button. [Commit.](http://commits.kde.org/breeze-gtk/177ba74a2d9ca7d6d64c55c1975cb545e68cfac2) See bug [#468203](https://bugs.kde.org/468203)
+ Gtk4/windowcontrols: use updated SVG assets. [Commit.](http://commits.kde.org/breeze-gtk/10e9ef98971c101295ccc4bad075c12c84431520) 
+ Assets: resize viewports of titlebutton SVGs. [Commit.](http://commits.kde.org/breeze-gtk/56216f86f3e15231a303b3abad70c46ed14535ad) See bug [#468203](https://bugs.kde.org/468203)
+ Gtk3: restore old icon size for titlebutton. [Commit.](http://commits.kde.org/breeze-gtk/c5e757a198321208c6b1e99313652577c263ecc0) Fixes bug [#468203](https://bugs.kde.org/468203)
+ Gtk3: remove invalid icon size property. [Commit.](http://commits.kde.org/breeze-gtk/fcecf770fa7b85a66f723d4cc799b539faedaf91) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Fix margins around distro upgrade message. [Commit.](http://commits.kde.org/discover/a8a1ef30d77aa328ac4f1a3c0414a6262c99f680) Fixes bug [#469163](https://bugs.kde.org/469163)
+ Rpm-ostree: Do not offer resources when the search doesn't pertain. [Commit.](http://commits.kde.org/discover/1756b3a8ac7b66fd1c93a823e6dde262d1f2a9ec) Fixes bug [#468162](https://bugs.kde.org/468162)
+ Updates page: fix missing clip causing items to paint over logging scrollview. [Commit.](http://commits.kde.org/discover/6586a4715a4be9f8b5aeedbd0b3aae057ef54f15) 
+ Updates ui: Simplify bottom toolbar logic. [Commit.](http://commits.kde.org/discover/2e134322179048f746cb8b730c6f30a44cd3892d) Fixes bug [#468459](https://bugs.kde.org/468459)
+ Flatpak: Do not crash if for any reason we lack a ref's source. [Commit.](http://commits.kde.org/discover/2bed299745d7c56c53191e46a8affe89f19c0491) Fixes bug [#467827](https://bugs.kde.org/467827)
+ PackageKitNotifier: Do not refresh database if update is in progress. [Commit.](http://commits.kde.org/discover/163a1190687c1a1b9b087e1c249dc7a0944202c6) 
+ Fwupd: Set user agent, and allow following redirects. [Commit.](http://commits.kde.org/discover/39ac7a6b82b3dc20f3c44ef11246d72545897f2d) 
+ Allow building against libmarkdown3. [Commit.](http://commits.kde.org/discover/82b30ac0d9a80f1b54b2010b935eb322c49f4cb7) 
+ Always clear offline updates file after a repair operation. [Commit.](http://commits.kde.org/discover/b18ebb137e49ccde3be49239bfaa8d88ace6a0c4) Fixes bug [#451753](https://bugs.kde.org/451753). Fixes bug [#450973](https://bugs.kde.org/450973). See bug [#467638](https://bugs.kde.org/467638)
+ ApplicationPage: Fix layout for content rating dialog. [Commit.](http://commits.kde.org/discover/f780c6ceaf3da7e3610d384088504364118b2f82) Fixes bug [#466815](https://bugs.kde.org/466815)
+ Flatpak: Report latest available version. [Commit.](http://commits.kde.org/discover/f68e0d42c001f9716550e062f9e55213aac92d28) Fixes bug [#465864](https://bugs.kde.org/465864). Fixes bug [#448521](https://bugs.kde.org/448521)
{{< /details >}}

{{< details title="Flatpak Permissions" href="https://commits.kde.org/flatpak-kcm" >}}
+ Sync model and helper files from master branch. [Commit.](http://commits.kde.org/flatpak-kcm/dd21c509e452a6d263cc92ea1b987143d73735fd) 
+ UI: Greatly refactor prompt dialogs. [Commit.](http://commits.kde.org/flatpak-kcm/58ec5dbe1c4c5f1fcfa8c0a1bcd6828b0bbe71ca) 
+ FlatpakPermissionModel: Fix runtime error about unknown method parameter type. [Commit.](http://commits.kde.org/flatpak-kcm/19a826577f7ec051f22b104e65a8f7d4885d4a15) 
+ FlatpakPermissionModel: Update "settings changed" state when saving non-default entries. [Commit.](http://commits.kde.org/flatpak-kcm/99e17cea2258906516f0ecd58c2be233e412cd25) 
+ FlatpakPermissionModel: Enable loading and adding environment variables. [Commit.](http://commits.kde.org/flatpak-kcm/29e236f97becfd699e8a38fd4101d28428e4a3dc) Fixes bug [#465502](https://bugs.kde.org/465502)
+ Use new validators to prevent users from entering duplicate or invalid entries. [Commit.](http://commits.kde.org/flatpak-kcm/b18954e153c7ae5301a177f87f93836acdef6e49) 
+ Add validators for names of custom entries, expose permissionExists. [Commit.](http://commits.kde.org/flatpak-kcm/6ab21a5038570aae49fa9fd3e6430d67497766de) 
+ Fix loading default filesystem with home/ or ~/ prefix. [Commit.](http://commits.kde.org/flatpak-kcm/447b3e1e7e15e06175d32e5c7f1e47a38bfa1b0c) 
+ Defer creation of non-standard FlatpakPermission Filesystems entries. [Commit.](http://commits.kde.org/flatpak-kcm/85f1278fbb0ea27e4a489e946852b2b5002a94e2) 
+ FlatpakReference: Fix enabling 'Apply' button by subscribing to more model signals. [Commit.](http://commits.kde.org/flatpak-kcm/54405bf0a4e0895b0ea2d549f74f7a07313c63da) 
+ Remove unused stuff and outdated comments. [Commit.](http://commits.kde.org/flatpak-kcm/fa01c4e6423e634b9c69026eea424c07c41ae232) 
+ Add myself to copyright file headers. [Commit.](http://commits.kde.org/flatpak-kcm/fafe65f5150c55ad6c850f1a4c66c75c894607c6) 
+ Add KCM.SettingHighlighter inside permissions page. [Commit.](http://commits.kde.org/flatpak-kcm/3ca1f7f41b8d2b1d068d6963938dfdb01bd21ce0) 
+ QML: Add some blank lines for readability. [Commit.](http://commits.kde.org/flatpak-kcm/5b264489459c1cb2334ea6a830459235eb92d9a3) 
+ Rename fields and methods without abbreviations. [Commit.](http://commits.kde.org/flatpak-kcm/5f53f2ac30e0b46ca0b99565b3f1f9d9ea6813f6) 
+ Rename needsSaveChanged signals to settingsChanged. [Commit.](http://commits.kde.org/flatpak-kcm/34e8bc1a510e02bfec3206a40e918864904422ab) 
+ FlatpakPermissionModel: Fix empty rows appearing after Reset. [Commit.](http://commits.kde.org/flatpak-kcm/f21949a74c5e54e1c0c40893f4541adb6b7670f8) Fixes bug [#468244](https://bugs.kde.org/468244)
+ FlatpakPermissionModel: Remove IsEnvironment model role. [Commit.](http://commits.kde.org/flatpak-kcm/362abdc298653070f7d6a2c3571eeec9bfe3d72a) 
+ QML: Bump import version of org.kde.kcm. [Commit.](http://commits.kde.org/flatpak-kcm/49d4a25aa411cf834290213a3579a76b66d47cc8) 
+ QML: Refer to ListView in delegates through attached property, remove id. [Commit.](http://commits.kde.org/flatpak-kcm/40e94c34707c9c290c62a74e15c6c2acfc81b11f) 
+ QML: Simplify bindings, remove needless component IDs. [Commit.](http://commits.kde.org/flatpak-kcm/a400cfe916ddf360631fd2530bb409488a8b4458) 
+ QML: Drop all custom properties from delegate, require model object instead. [Commit.](http://commits.kde.org/flatpak-kcm/9c5678247972bcb9f4a33e082f7493f741747a20) 
+ FlatpakPermissionModel: Factor out some permission loading code. [Commit.](http://commits.kde.org/flatpak-kcm/f86c49e433fd3fac4708fa630a34741b8c09a067) 
+ FlatpakPermissionModel: Rewrite override data management, drop manual string puzzle. [Commit.](http://commits.kde.org/flatpak-kcm/cb22c22cd679f8d4b9f9cefe86e0e6d556f9f478) 
+ FlatpakPermissionModel: Fix description of Session D-Bus rows. [Commit.](http://commits.kde.org/flatpak-kcm/eb345acbcc63bc2d157134d98ee241a14819d5ac) 
+ FlatpakPermissionModel: Add section type as a predicate to find entries. [Commit.](http://commits.kde.org/flatpak-kcm/5338163b677989d18aabde95fefef7766f19c72a) 
+ FlatpakPermissionModel: Fix removing dummy rows when inserting real ones. [Commit.](http://commits.kde.org/flatpak-kcm/1be53ba425266fdbbd5ebbd85f4dc9a3784cd9de) 
+ PolicyChoicesModel: Mark all constructors as explicit. [Commit.](http://commits.kde.org/flatpak-kcm/d8b1314d7363c6bbd2e045a17a85e9004516d41a) 
+ FlatpakPermissionModel: Port reading simple values to use the new parser. [Commit.](http://commits.kde.org/flatpak-kcm/e774bb6ddef039f0e39048a59001734971392a60) 
+ Move helpers definitions to the top. [Commit.](http://commits.kde.org/flatpak-kcm/f14a9b84502c141f2229bc1093f3a701dd000893) 
+ Add test for overriding with the same enabled state as in defaults. [Commit.](http://commits.kde.org/flatpak-kcm/ee1608c4c88d3406b1674b14ad1e3024ac21b98a) 
+ FlatpakPermissionModel: Port fetching list categories to readXdgListEntry. [Commit.](http://commits.kde.org/flatpak-kcm/ee67e54139b5822aa39dd86ee2d419ec376f6f14) 
+ FlatpakPermissionModel: Parse 'simple' values groups while loading overrides. [Commit.](http://commits.kde.org/flatpak-kcm/fd80187cdfa4120138130b1818c6cfc4b1694b0a) 
+ FlatpakPermissionModel: Expose CanBeDisabled role for convenience. [Commit.](http://commits.kde.org/flatpak-kcm/b69aaa6da797d0ad606cb6d31a732ade34b591ac) 
+ Add FlatpakSimpleEntry parser, serializer and tests. [Commit.](http://commits.kde.org/flatpak-kcm/dfdb6d3108a888f369dddd8ee7d5ff9a90b9e68d) 
+ Tests: Fix off-by-one error while iterating over model rows. [Commit.](http://commits.kde.org/flatpak-kcm/7d8f82d21713ed329c0d6b10449ff7e2357ab0d0) 
+ Fix code style, stop turning clang-format off. [Commit.](http://commits.kde.org/flatpak-kcm/b2a06611e9989a5076aae840e0a10c99113ec446) 
+ Fix application names in test data. [Commit.](http://commits.kde.org/flatpak-kcm/06d9fdea9fbb44a90dc471d636a4088b8f5bd12f) 
+ FlatpakPermissionModel: Store values of Filesystem entries as AccessMode enum. [Commit.](http://commits.kde.org/flatpak-kcm/130c72a265a4906ac35c18f4d898e49b01d4bfb1) 
+ Fix off-by-one error in tests. [Commit.](http://commits.kde.org/flatpak-kcm/9ce0cf51a49d929ac66ef49e14dcd1e12deed18e) 
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Add kinfo. [Commit.](http://commits.kde.org/kde-cli-tools/c48699eb6088bebc8b877d9e00e92552355c2369) Fixes bug [#403077](https://bugs.kde.org/403077)
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Kded: add explicit waiting time before setting colors. [Commit.](http://commits.kde.org/kde-gtk-config/90b423429dde264117c729d5455e581ebb09a109) See bug [#421745](https://bugs.kde.org/421745)
+ Kded: don't update text scale on kdeglobals change events. [Commit.](http://commits.kde.org/kde-gtk-config/52eb9d01b94eeece64044c5159d965eec7f39079) See bug [#468203](https://bugs.kde.org/468203)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Applets/colorpicker: disable pick button when compositing is disabled. [Commit.](http://commits.kde.org/kdeplasma-addons/852c0663803a64aa7fb93ca296260c58e8b2d6ac) Fixes bug [#466230](https://bugs.kde.org/466230)
+ ProfilesModel: Adjust to kate using different folder for storing sessions than konsole. [Commit.](http://commits.kde.org/kdeplasma-addons/6c0b511bd9df98ee896ef7402d9a1bb23ddaab5f) Fixes bug [#466824](https://bugs.kde.org/466824)
+ Runners/datetime: fix test failure. [Commit.](http://commits.kde.org/kdeplasma-addons/db8597e91eeb013df4ba88603df39c5eeeaed252) 
+ Applets/colorpicker: Use native relative positioning Menu API. [Commit.](http://commits.kde.org/kdeplasma-addons/ff4872e51f91bee436f8f8322b03b062b5225684) 
+ Applets/colorpicker: Open context menu on Menu key press. [Commit.](http://commits.kde.org/kdeplasma-addons/abaf0521d643fe062c516982c1837be676f90a66) 
+ Applets/colorpicker: Fix copying color via keyboard. [Commit.](http://commits.kde.org/kdeplasma-addons/c9872227e5623939576e5d321ea8b7056e60e9c1) 
+ Applets/colorpicker: Factor out logic from signal handler. [Commit.](http://commits.kde.org/kdeplasma-addons/0439bea72b37f7137533840dc40fe2b49f136feb) 
+ Applets/colorpicker: Only reset global current index from a real current item. [Commit.](http://commits.kde.org/kdeplasma-addons/386942d332d1d5c0d0738f6c9fa43593f9257a38) 
+ Applets/colorpicker: Fix QML/JS code style, add explicit signal arguments. [Commit.](http://commits.kde.org/kdeplasma-addons/565ea91d69a5346ccc122ed878a350ec86fad5ee) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ About-distro: add a dump mode to print to cli. [Commit.](http://commits.kde.org/kinfocenter/34838dc61b4448791745a53df677f75710e3ca03) Fixes bug [#403077](https://bugs.kde.org/403077)
+ Nics: reset model on update. [Commit.](http://commits.kde.org/kinfocenter/3c0a6936ed4386cf11af65f69d0438d89b88d830) Fixes bug [#468026](https://bugs.kde.org/468026)
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Item: hide the cursor when we start getting null positions. [Commit.](http://commits.kde.org/kpipewire/0c14b94df5d251e05170bdd9ac38714c951b7583) 
+ Item: Make sure we have a texture at all times. [Commit.](http://commits.kde.org/kpipewire/3e28f4b516c20b7e04cc5a78502515a406cdfdb0) 
+ Do not process corrupt frames. [Commit.](http://commits.kde.org/kpipewire/b27e1a43792498c22e8d529beff1a585a8df8263) 
+ Record: Do not crash when closing while recording. [Commit.](http://commits.kde.org/kpipewire/ebfd3755b4fa6b5a51fcbefa04f870ab6b77ead7) Fixes bug [#467593](https://bugs.kde.org/467593)
+ Expose the stream size in PipewireSourceItem. [Commit.](http://commits.kde.org/kpipewire/c531d6388ba7a57fe21cd25628de68163c974e84) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Yet more QRect right() fixes. [Commit.](http://commits.kde.org/kscreen/fee471e09bcbfcbd6f6cab05b605a0916162fa43) Fixes bug [#455394](https://bugs.kde.org/455394)
+ Don't stumble over nullptrs if outputs changed during saving. [Commit.](http://commits.kde.org/kscreen/8af1cfac332f6f7c4e6db40c851dd5ac719236f1) Fixes bug [#466960](https://bugs.kde.org/466960)
+ Kcm: notify changes in kcmfonts when global scale changes. [Commit.](http://commits.kde.org/kscreen/e9384150d8e41dd9c869f5f502e02c70a5c6f002) See bug [#468203](https://bugs.kde.org/468203)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Greeter: track various properties persistently. [Commit.](http://commits.kde.org/kscreenlocker/161f9d4c5151e4f3871e670531883bcb6a198d1f) Fixes bug [#456210](https://bugs.kde.org/456210)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Wayland: Avoid pointer warp if constraint region is invalid. [Commit.](http://commits.kde.org/kwin/d7033a3c83134f383fbbd8e005ff6a10c79e48d4) Fixes bug [#457021](https://bugs.kde.org/457021)
+ Cmake: Correct gbm version check. [Commit.](http://commits.kde.org/kwin/a6b2f10d7954003449e8bed361cbdcb96bbc0a63) 
+ Kwineffects: Initialize m_reason. [Commit.](http://commits.kde.org/kwin/8bafe31b44b92285ca098d291ea8b98b978ef254) 
+ Screencast: Disable screencasting when a window closes. [Commit.](http://commits.kde.org/kwin/709b3db17efc214bb6ae7e7ddf37e9697b287bc5) Fixes bug [#469055](https://bugs.kde.org/469055)
+ Screencast: Do not push frames when not in a streaming state. [Commit.](http://commits.kde.org/kwin/109d7ad06dd5bbc0e9ade7fc686de407b126067e) 
+ Use non-rotated physical size of an output when required. [Commit.](http://commits.kde.org/kwin/9f46f605184976fe025c75b3a5452b4b0ed4c23c) 
+ Ci: Pass --repeat until-pass to ctest. [Commit.](http://commits.kde.org/kwin/58decc33d81323074dbc000d6caefd2d80445cc6) 
+ Platformsupport/scenes/opengl: filter out external formats properly. [Commit.](http://commits.kde.org/kwin/fca6655717722e36ae4dbb23043136db72078429) 
+ Effects/screenshot: Fix potentially leaking screenshot fds to child processes. [Commit.](http://commits.kde.org/kwin/7cdfc8b7bc0fde944e4338fe50fd07334e282396) 
+ Backends/drm: fix buffer orientation check for direct scanout. [Commit.](http://commits.kde.org/kwin/5e0349cd99be7c8728f93ba7cf0e2cf590756971) See bug [#467138](https://bugs.kde.org/467138)
+ Plugins/screencast: Provide absolute timestamps. [Commit.](http://commits.kde.org/kwin/8798ac3c063a45dae2c9e7f18d5e0c8b9d252e47) 
+ Don't create Plasma activation feedback if StartupNotify is false. [Commit.](http://commits.kde.org/kwin/f6f8bac96927ca21b827081a3c775569e093d72a) 
+ Effects/screenshot: Provide screenshot scale information. [Commit.](http://commits.kde.org/kwin/bfc4cfddf06447b2892022b10574b7fbbe52dfee) 
+ Effects/screenshot: Provide information about captured window or screen. [Commit.](http://commits.kde.org/kwin/4587bb3165f25d7d65ba453f2dd3358d002a6b45) 
+ Effects/screenshot: Introduce CaptureWorkspace. [Commit.](http://commits.kde.org/kwin/42d93fe88448104283fec8c17bae68ba58d88f79) 
+ Screencast: Base the frame skippin on max_framerate. [Commit.](http://commits.kde.org/kwin/e125f48ec1d64bd2ea8bbf1a3edd0439fc9b5276) 
+ Screencast: Ensure we respect the negotiated framerate. [Commit.](http://commits.kde.org/kwin/23806d31e23d2ef2bc8344114feb6d27364cdc01) 
+ Screencast: Offer the real framerate range we have available. [Commit.](http://commits.kde.org/kwin/7fc2146b09358245cf9a4eff9e841bd3e2359b06) 
+ Blacklist Spectacle for all window open/close effects. [Commit.](http://commits.kde.org/kwin/b090a7cc9e3b2a70e88c47b5fb6add8599bc7c0d) Fixes bug [#467890](https://bugs.kde.org/467890). Fixes bug [#463105](https://bugs.kde.org/463105). See bug [#465784](https://bugs.kde.org/465784)
+ Backends/drm: set the scaling mode to none. [Commit.](http://commits.kde.org/kwin/bbc3e10c589a01dfe8933fbe910412c3cd2dbac8) Fixes bug [#468235](https://bugs.kde.org/468235)
+ Screencast: Still set the size to 0 for cursor-only frames. [Commit.](http://commits.kde.org/kwin/c8977dea7055f51cff6051a7a3ccbe986057e3df) 
+ Screencast: Improve how we communicate that a frame has just cursor info. [Commit.](http://commits.kde.org/kwin/73b654991bd3d0edf6ee86dca00ed1b27f439959) 
+ Kcms/rules: fix invalid tooltip visible condition in rule items. [Commit.](http://commits.kde.org/kwin/7ec7fac90990c02dfb27d9e5402fe6c4bafc2816) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Xrandr: always try to update primary. [Commit.](http://commits.kde.org/libkscreen/49cc50e629d55d258218bbe5e0abb0c6e93951e3) See bug [#465396](https://bugs.kde.org/465396)
+ Cleanup dpms object in destructor. [Commit.](http://commits.kde.org/libkscreen/771d68f6dd9d5ecd0a867280aae02fec1fae3fbe) 
+ Some GPUs will report a virtual edid even there is no monitor connected to. [Commit.](http://commits.kde.org/libkscreen/282cb301f5961d6519bb5a295155f25f96421c85) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Formatter: Don't try to prefix units that we don't have a prefixed version of. [Commit.](http://commits.kde.org/libksysguard/7f4ddc21426a2f288033ea6f16ff80cb0e2e7b71) 
+ Formatter: Format UnitWattHour in formatValue. [Commit.](http://commits.kde.org/libksysguard/bbe1f55ab27984474a8481bca29a82219e3e583e) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [kcm/keys] Fix loading service data. [Commit.](http://commits.kde.org/plasma-desktop/791dd8fbc3bd1ed36d801fda2b1b1800f9d943c1) 
+ Desktoppackage: Fix ScrollBar overlapping configuration category delegates. [Commit.](http://commits.kde.org/plasma-desktop/1ada2d490734bbda36f106e52c21242b90c0bf91) 
+ Desktoppackage: Fix visual overlap of items in applet configuration view. [Commit.](http://commits.kde.org/plasma-desktop/6be051f6bacd5361ab99511c851156280fcaa711) 
+ Landingpage: Port "Most Used Pages" grid to Kirigami.SizeGroup. [Commit.](http://commits.kde.org/plasma-desktop/80fd9eef76be234b72bf3ea5e33e15870f636867) 
+ Landingpage: Fix broken binding in "Most Used Pages" grid. [Commit.](http://commits.kde.org/plasma-desktop/c7662ea126c8e28546c1b22bb53b03fbecbb028b) See bug [#468712](https://bugs.kde.org/468712)
+ Landingpage: Set implicit size, so that kcmshell doesn't open too small. [Commit.](http://commits.kde.org/plasma-desktop/7b23dbbc60f747b16bd0f3769e77cb037e879dab) 
+ Kcms/keys: Make adding a script file work even if its path has spaces in the name. [Commit.](http://commits.kde.org/plasma-desktop/49b56e7adb1e582da4b253c2ed491a11d1cc5696) 
+ Kcms/keys: Make Add Command work with percent characters in commands. [Commit.](http://commits.kde.org/plasma-desktop/2ac3bf70b7edbdacdad98aa1a7e7786227ab7fb2) 
+ Applets/taskmanager: also check title contains "-". [Commit.](http://commits.kde.org/plasma-desktop/df39f05070cd74000050d746814ed65851129820) See bug [#462760](https://bugs.kde.org/462760)
+ Applets/taskmanager: fix tooltip text with custom window title. [Commit.](http://commits.kde.org/plasma-desktop/626167b3c7be11afa35dd0d5ecbdfdf37a1ef897) Fixes bug [#462760](https://bugs.kde.org/462760)
+ Automounter: don't track ignored devices. [Commit.](http://commits.kde.org/plasma-desktop/b7f8e895a60f858f816374861419cc1998953aff) Fixes bug [#468410](https://bugs.kde.org/468410)
+ Applets/taskmanager: don't show Unpin menu item while app is launching. [Commit.](http://commits.kde.org/plasma-desktop/40514ae41d79a8584b243bae8d9fe249fc3f5966) Fixes bug [#468668](https://bugs.kde.org/468668)
+ KCM/mouse: enable compatibility with x11-libinput 1.3. [Commit.](http://commits.kde.org/plasma-desktop/2a1ea4d02bc3fd90bf63099ba6752eb23808dff7) Fixes bug [#468217](https://bugs.kde.org/468217)
+ Folder View: fix keyboard shortcuts handling. [Commit.](http://commits.kde.org/plasma-desktop/1cceee9a9ef59696e067a4129cc702e31f51bcd1) Fixes bug [#436362](https://bugs.kde.org/436362)
+ Applets/taskmanager: show Activity icons in relevant context menu items. [Commit.](http://commits.kde.org/plasma-desktop/56e25cce0954970600c4128a236ba4895f8c5e7a) Fixes bug [#419225](https://bugs.kde.org/419225)
+ Emojier: enable asynchronous loading in Instantiator. [Commit.](http://commits.kde.org/plasma-desktop/24cce71ecc3167641f41fafe4da1a55ae1ece44d) Fixes bug [#468328](https://bugs.kde.org/468328)
+ CompactApplet: show tabbar for Breeze Twilight. [Commit.](http://commits.kde.org/plasma-desktop/16ef7f64c55825240b224883eccf3eaa1ba2de37) Fixes bug [#468313](https://bugs.kde.org/468313)
+ Kcms/landingpage: add accessible name to animation speed slider. [Commit.](http://commits.kde.org/plasma-desktop/35331fe9fbb4cfe3d9492081ff05b4203ec52db9) 
+ Kcms/landingpage: use explanation label as accessible description. [Commit.](http://commits.kde.org/plasma-desktop/54edd07dd790160c61bb960eba04ece9297f0359) 
+ Applets/taskmanager: also match row index before skipping updating bindings. [Commit.](http://commits.kde.org/plasma-desktop/63d1f25e2a2c0ec70802f624e37bdac009faaa9a) 
+ Applets/taskmanager: reset `parentTask` after a task was moved. [Commit.](http://commits.kde.org/plasma-desktop/7195637d184c146e177feb28fe904cbf98a0a87e) See bug [#467709](https://bugs.kde.org/467709). See bug [#452187](https://bugs.kde.org/452187)
+ Applets/taskmanager: Improve scrolling usability. [Commit.](http://commits.kde.org/plasma-desktop/d44a3d9b934bc29c76f442bc7f6056727cbe3921) 
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Wayland: Only set decoration palette once for each change. [Commit.](http://commits.kde.org/plasma-integration/2b04981039bbd0fd2ee8303d0d4539208f651b0a) Fixes bug [#468408](https://bugs.kde.org/468408)
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Use nm_conn_wireguard_import to import WireGuard files. [Commit.](http://commits.kde.org/plasma-nm/0237be91fd8a714efa3ef9d88c7ac157fcff488b) Fixes bug [#420066](https://bugs.kde.org/420066). Fixes bug [#468365](https://bugs.kde.org/468365). Fixes bug [#452952](https://bugs.kde.org/452952). Fixes bug [#423973](https://bugs.kde.org/423973). Fixes bug [#427222](https://bugs.kde.org/427222)
+ Persist imported VPN connections on disk. [Commit.](http://commits.kde.org/plasma-nm/d795e92bd12705d0ce506c4ac900b0ccb7bdc69b) Fixes bug [#468666](https://bugs.kde.org/468666)
{{< /details >}}

{{< details title="Plasma Remotecontrollers" href="https://commits.kde.org/plasma-remotecontrollers" >}}
+ Update org.kde.plasma.remotecontrollers.CEC.xml. [Commit.](http://commits.kde.org/plasma-remotecontrollers/c0be18b9ddc0ab7057cddd9e59fb38d15e924809) 
+ Remove unused signals. [Commit.](http://commits.kde.org/plasma-remotecontrollers/3feaf12455d8a549386c3c2c65110137e395d8e0) 
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Cuttlefish: Set window decoration icon. [Commit.](http://commits.kde.org/plasma-sdk/f21c59df59d8bb261edbe189159ce9f15772e2bb) Fixes bug [#409956](https://bugs.kde.org/409956)
+ IconGrid: Fix JavaScript/QML code style, use strict equality operator. [Commit.](http://commits.kde.org/plasma-sdk/1f07a46ea0e91ecb2c142923026e869eb6cadb51) 
+ IconGrid: Redo highlight style similar to Plasma Breeze. [Commit.](http://commits.kde.org/plasma-sdk/c40c9ebd1a319d7a5a93c5a2d70ee5f61c7cf7b8) 
+ IconGrid: Fix sizing issues. [Commit.](http://commits.kde.org/plasma-sdk/17e62469f305236385f0917fd880f508bb7f1e1b) 
+ Add icontheme when creating new global theme. [Commit.](http://commits.kde.org/plasma-sdk/8d0522a470ef998ae7716427954d76f3ba60a645) Fixes bug [#465817](https://bugs.kde.org/465817)
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Make runCommand() with only one argument work as intended. [Commit.](http://commits.kde.org/plasma-welcome/7ce4f35895600c5b00b9441a42940d189cddadb2) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fix Klipper empty prevention on wayland. [Commit.](http://commits.kde.org/plasma-workspace/16d7f44deac7621318be63438d0f8b8923d165df) Fixes bug [#468422](https://bugs.kde.org/468422)
+ Don't read default wallpaper multiple times. [Commit.](http://commits.kde.org/plasma-workspace/78ee1e36ad0d07177ffc882b67c9be65eeb47cfb) 
+ Users kcm: hack around indexing confusion in swipeview. [Commit.](http://commits.kde.org/plasma-workspace/0a913be612e3492576f502b4f1edc90a4858b95d) Fixes bug [#439081](https://bugs.kde.org/439081)
+ Krunner: Fix query with clipboard contents on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/c1465ef57452d0f3878b757bd2a628f0de48fb36) Fixes bug [#451747](https://bugs.kde.org/451747)
+ Applets/notification: fix history view vertical lines. [Commit.](http://commits.kde.org/plasma-workspace/b0056e32d0ade98051eeae9eae77175d402a3d22) Fixes bug [#416386](https://bugs.kde.org/416386)
+ Applets/notifications: Set text color to ensure visibility with all color themes. [Commit.](http://commits.kde.org/plasma-workspace/9c5331c3f2eb5b9808951afe14e0696d7277d122) 
+ Kcms/nightcolor: fix wrong height for timings view. [Commit.](http://commits.kde.org/plasma-workspace/cdd268a3b8b96dc0ef16ab0bec176c5785a6d215) Fixes bug [#468747](https://bugs.kde.org/468747)
+ Applets/kicker,kcm/fonts,lookandfeel: Guard adding autotest subdirectory with BUILD_TESTING. [Commit.](http://commits.kde.org/plasma-workspace/b4624325f71705f0b1232d72b23117fd07219305) 
+ Applets/notifications: Handle the case of the finish date being in the future. [Commit.](http://commits.kde.org/plasma-workspace/ab9576d68fe623f7c1e002460e2bfce2ff14c220) Fixes bug [#464803](https://bugs.kde.org/464803)
+ Applets/systemtray: add workaround for QTBUG-59044. [Commit.](http://commits.kde.org/plasma-workspace/c27fe83854032fe78ba8e02c78029bced2501e9d) 
+ Applets/devicenotifier: don't show "Mount" action for MTP devices. [Commit.](http://commits.kde.org/plasma-workspace/3cf0b77173c2b77a5cbc214d2b418638105a95a6) Fixes bug [#446278](https://bugs.kde.org/446278)
+ Applets/digital-clock: Fix broken binding in the list of calendar plugins. [Commit.](http://commits.kde.org/plasma-workspace/28e6cab939007c54be4ea2b33b4ed7f4aa57bfa8) See bug [#468712](https://bugs.kde.org/468712)
+ Reset "show password" status on lock and login screens when fading in. [Commit.](http://commits.kde.org/plasma-workspace/00576ef1499a319791b217fedd234f2ed6e92ed8) Fixes bug [#449034](https://bugs.kde.org/449034)
+ Kcms/autostart: Correctly handle spaces in a script file path. [Commit.](http://commits.kde.org/plasma-workspace/36775c708106b3d2c0cf553d3e11ea6a16852dc6) 
+ Applets/digital-clock: Fix sizing and layouts of CalendarView full representation. [Commit.](http://commits.kde.org/plasma-workspace/af80597dec2cd8fdc29385e3c872dabd5791b361) 
+ Kcms/style: Add i18n support for percent value. [Commit.](http://commits.kde.org/plasma-workspace/dc8759034bfe8db2d499fa9bb0374e90d65f5ac4) 
+ Outputorderwatcher: rely on QMap for ordering of outputs. [Commit.](http://commits.kde.org/plasma-workspace/9bea46b09e5d407697dc2cb660e72e0d538d482f) 
+ Screenpool: don't race with orderwatcher. [Commit.](http://commits.kde.org/plasma-workspace/acbc7234937027a6cf98b173517136d4bdbdfe94) 
+ Improve dbus session-local hack. [Commit.](http://commits.kde.org/plasma-workspace/5e2f90db2dfda013a84833e871ee5f6517e52d2f) 
+ Applets/notifications: Fix NotificationItem label alignment when in group. [Commit.](http://commits.kde.org/plasma-workspace/effb4986130e88f64f345f265c4d6dd5706041cf) 
+ Calendar: don't anchor daydelegate contentItem. [Commit.](http://commits.kde.org/plasma-workspace/fe4dfe3bac92869b05a12fd0f903629df1b74629) Fixes bug [#463080](https://bugs.kde.org/463080)
+ Applets/notifications: Fix NotificationHeader spacing and padding. [Commit.](http://commits.kde.org/plasma-workspace/c8f10f7f100fc7c15582904991508c6f979fc4a9) 
+ Dataengines/mpris2: don't recreate PlayerControl for the same container. [Commit.](http://commits.kde.org/plasma-workspace/402ebdbde1348b7e5a5e4c4ed7b6fed366fc80b6) See bug [#465454](https://bugs.kde.org/465454)
+ Libtaskmanager: add activityIcon getter. [Commit.](http://commits.kde.org/plasma-workspace/e3f688661309e732baa540c9bbb19476307f267e) See bug [#419225](https://bugs.kde.org/419225)
+ Outputorderwatcher: ignore outputs without crtc. [Commit.](http://commits.kde.org/plasma-workspace/951b72c1ed24c9c236883ca47bfe6a25883eca7f) Fixes bug [#466362](https://bugs.kde.org/466362)
+ Outputorderwatcher: don't leak replies. [Commit.](http://commits.kde.org/plasma-workspace/364c617a1654b5bd0dc36379c9ab7056b21fe066) See bug [#466362](https://bugs.kde.org/466362)
+ Kcms/icons: find larger icons and fall back to svg if there is none. [Commit.](http://commits.kde.org/plasma-workspace/c10830cb0a1630e424bbcd5f364a32c2ae6569f4) 
+ Applets/panelspacer: Use height values for vertical panel in spacers. [Commit.](http://commits.kde.org/plasma-workspace/c88d93ce9019ba47a5591c3bc44caef9973b3088) Fixes bug [#467416](https://bugs.kde.org/467416)
+ Kcms/colors: remove custom change notifier for `accentColorFromWallpaper`. [Commit.](http://commits.kde.org/plasma-workspace/5f484533a1e7805d8953739815116371416f1b55) 
+ Libtaskmanager: fix flaky test. [Commit.](http://commits.kde.org/plasma-workspace/9325e0506303aaa983eb20ab844f6b2fe0790eda) 
+ Runners/calculator: set timeout to prevent allocating huge memory. [Commit.](http://commits.kde.org/plasma-workspace/3f3bea079bb55ec70ce248d1238cd0e7e38e5261) Fixes bug [#468084](https://bugs.kde.org/468084)
+ Libtaskmanager: fix flaky test. [Commit.](http://commits.kde.org/plasma-workspace/bf30eadde58810721f552a9d41ce91ca74f84337) 
+ Lookandfeel/osd: Explicitely add the translation domain. [Commit.](http://commits.kde.org/plasma-workspace/4467cf391b136c0692843973f48c88833241d3a0) Fixes bug [#468013](https://bugs.kde.org/468013)
+ Appmenu: fix crash when there is no matched menu item. [Commit.](http://commits.kde.org/plasma-workspace/9d8d1de29ac314058f5baa6bb7dc85fc2f0f10f1) Fixes bug [#467979](https://bugs.kde.org/467979)
{{< /details >}}

{{< details title="polkit-kde-agent-1" href="https://commits.kde.org/polkit-kde-agent-1" >}}
+ Force extreme focus protection. [Commit.](http://commits.kde.org/polkit-kde-agent-1/239ec8537b88f57ec1366b4121d14a4655581ac9) Fixes bug [#312325](https://bugs.kde.org/312325)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Fix for screen backlight not working on Planet Computers Astro Slide PDA. [Commit.](http://commits.kde.org/powerdevil/f8fb7b1e0b7b969d2d5dca2ef9290417f468460b) 
+ Fix and consolidate discharge rate and remaining time calculation. [Commit.](http://commits.kde.org/powerdevil/f6664808e34650512867fee817f940ab27b0097c) See bug [#434432](https://bugs.kde.org/434432)
+ Remove unused "BatteryState" from backend interface. [Commit.](http://commits.kde.org/powerdevil/d1d4807178e73dbdad54d8d78afa7e243f6489d9) 
+ Fix possible race between DeviceAdded signal and device enumeration. [Commit.](http://commits.kde.org/powerdevil/dd6a011dffc084b3583bcd52d495b13e2af428d7) 
+ Remove support for UPower < 0.99.0. [Commit.](http://commits.kde.org/powerdevil/344ae969f30a68222c0709d002c1869dbb825185) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Sync cursor size too. [Commit.](http://commits.kde.org/sddm-kcm/0a7e6f596d7496c0a3385f55378363ec51377ab2) Fixes bug [#467326](https://bugs.kde.org/467326)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ [notifications] Set proper value for x-kde-xdgTokenAppId. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/4c4544f37c299d451e5e580a93d2fed54f176bcb) 
+ Screenshot: fix flameshot #3164 by using native resolution. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/be08ee0afcf033e306c026ce1e8892699ec4d798) 
+ Screencast: Don't try to screencast nullptr. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/58313fac8188163c9445bb9a212405412258ef5e) See bug [#467622](https://bugs.kde.org/467622)
{{< /details >}}

