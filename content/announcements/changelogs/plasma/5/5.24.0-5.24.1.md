---
title: Plasma 5.24.1 complete changelog
version: 5.24.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Kded/devicemonitor: Check BlueDevilDaemon exists when calling login1PrepareForSleep. [Commit.](http://commits.kde.org/bluedevil/038b7e303f7c1881e53ccd1db4d04e5d1ca3987b) Fixes bug [#450195](https://bugs.kde.org/450195)
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Fix "Blue Ocean" styling of QCommandLinkButtons. [Commit.](http://commits.kde.org/breeze/648d80ae381ddbd9c258aeed396f51c5a39e8ad8) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Assets: uncircle the close button's idle state. [Commit.](http://commits.kde.org/breeze-gtk/285585f751af96f7febcf979898ab5d59e96751a) Fixes bug [#449876](https://bugs.kde.org/449876)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Notifier: Don't trigger updates while busy. [Commit.](http://commits.kde.org/discover/f8638e857c346c8293ec1e47c535219eb3f7961c) 
+ Notifier: Update lastUnattendedTrigger before checking for updates again. [Commit.](http://commits.kde.org/discover/556ab9b99dcc06ee427342350763c6ebf9377d09) 
+ Notifier: Actually save LastUnattendedTrigger. [Commit.](http://commits.kde.org/discover/eb81046825606fa6ed7973dc3b5eb3413c6a355c) 
+ Notifier: Use KIdleTime properly. [Commit.](http://commits.kde.org/discover/7b918b965acd39cf2bcc8f82b7f2bf9bf1db2704) 
+ Use C++17. [Commit.](http://commits.kde.org/discover/db151e2aef3f448682a0e1df15937cafa18211a5) 
+ Don't display critical packages/apps in the UI. [Commit.](http://commits.kde.org/discover/4e6748b1fc21bbd1f983adb78dcac95b7174706f) See bug [#449260](https://bugs.kde.org/449260)
+ Update: Ensure we never have two update processes running in parallel. [Commit.](http://commits.kde.org/discover/ad9ad39cf2e63a087dbe430b3bb2f93d7e1eae95) 
+ Implement Kirigami.CheckableListItem properly. [Commit.](http://commits.kde.org/discover/3674bc6bd436b5e0bb7f82171a6d47e62b442a37) Fixes bug [#449766](https://bugs.kde.org/449766)
+ Fix unattended update interval check. [Commit.](http://commits.kde.org/discover/3907841afc6d46ea6c243327cddf0d59d5694b7d) 
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Plasma-open-settings: systemsettings5 is now systemsettings. [Commit.](http://commits.kde.org/kde-cli-tools/288f999b4f7dd8f6d40ce6c36acb3c84db3f3045) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ [applets/notes] Fix drag-and-drop focus stealing. [Commit.](http://commits.kde.org/kdeplasma-addons/e6fc3c0015dc18ea0c901480c1fe5db86b0a2396) Fixes bug [#449711](https://bugs.kde.org/449711)
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Search in /usr/local/sbin:/usr/sbin:/sbin as fallback. [Commit.](http://commits.kde.org/kinfocenter/c735a7481b6200d7a03f533476ed9f0fdb53fde9) Fixes bug [#449792](https://bugs.kde.org/449792)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ KCM: Workaround unknown Qt issue that causes the revert dialog to be invisible. [Commit.](http://commits.kde.org/kscreen/3fb6eb435b752981721e152c39c7d0fbf589042e) Fixes bug [#449560](https://bugs.kde.org/449560)
+ [kcm] Only enable revert action when revert sheet is open. [Commit.](http://commits.kde.org/kscreen/2a22b96d46537d02b3e3fef83167115a9ba0bf8d) Fixes bug [#449931](https://bugs.kde.org/449931)
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ CPU Plugin: Prevent integer overflow of total usage. [Commit.](http://commits.kde.org/ksystemstats/99670f3a2ec04037da341e5ae11cbc481c799f35) Fixes bug [#448626](https://bugs.kde.org/448626)
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Add missing errno.h include. [Commit.](http://commits.kde.org/kwayland-server/b53d673aa77c9f06080b438e0f019a6513fe489f) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: fix legacy dpms. [Commit.](http://commits.kde.org/kwin/c461149d5516a1dcf896984e2b0adfd60cea9953) 
+ Utils/serviceutils: compare executablePath against canonical path of exec fields in .desktops. [Commit.](http://commits.kde.org/kwin/22f59e0a06b0927a78101a3a505ac12cb8824d90) 
+ X11: Fix shrinking window size after toggling no border multiple times. [Commit.](http://commits.kde.org/kwin/d4068754c91f17f01e430f60bfed8975c7170977) Fixes bug [#449988](https://bugs.kde.org/449988)
+ Wayland: Fix maximize mode propagation to decoration. [Commit.](http://commits.kde.org/kwin/a43496bcc4795cbe462cd72198e62b0a8923f589) Fixes bug [#450053](https://bugs.kde.org/450053)
+ Effects/overview: Add translation domain. [Commit.](http://commits.kde.org/kwin/f8d228c6023a64605c40f9d8bbad4ec3996894ec) 
+ Effects: Extract messages from QML files. [Commit.](http://commits.kde.org/kwin/193ac8eb4544ff15ac02dd496bcc9ef872db9bdb) 
+ Inputmethod: Also open the inputm method panel with a pen. [Commit.](http://commits.kde.org/kwin/7626dbb1bb4dd5e437f060b3d64da316d15987a6) Fixes bug [#449888](https://bugs.kde.org/449888)
+ Implement services in our QPA. [Commit.](http://commits.kde.org/kwin/966ac0320bc743c4ee707786a61c748ef40af2a3) Fixes bug [#446144](https://bugs.kde.org/446144)
+ Don't force QT_QPA_PLATFORM=wayland. [Commit.](http://commits.kde.org/kwin/f05a7c0e51c452b3850cb9e21d5b92701526366e) Fixes bug [#450000](https://bugs.kde.org/450000)
+ Schedule workspace repaint when window leaves current desktop. [Commit.](http://commits.kde.org/kwin/ee65fb3d2fca84a706d17c9fa6d41641fedbd757) Fixes bug [#444172](https://bugs.kde.org/444172)
+ Initialize the KSldApp after we connect. [Commit.](http://commits.kde.org/kwin/2b217ce78c82a00c8b261184a8623df02635726b) 
+ Fix loading of effect if plugin defined X-KDE-Library. [Commit.](http://commits.kde.org/kwin/79fadcc6082d20dca71f99321779f34b21f9481c) Fixes bug [#449881](https://bugs.kde.org/449881)
+ Wayland: Fix mispositioned decoration tooltips. [Commit.](http://commits.kde.org/kwin/f065d74af6ebbf749a9513da372218b2d8df8718) Fixes bug [#432860](https://bugs.kde.org/432860)
+ Make openGL context current when recording frame triggered by cursor move. [Commit.](http://commits.kde.org/kwin/8f2ba3ef13ecdd6c93ba1ab537d4841bedbef495) Fixes bug [#448162](https://bugs.kde.org/448162)
+ Effects/DesktopGrid: use delta, not absolute time for MotionManager. [Commit.](http://commits.kde.org/kwin/78c483ab55b3b8e93580726cae6821f3a8f7703d) Fixes bug [#443971](https://bugs.kde.org/443971)
+ Fix window decoration quads in OpenGL scene. [Commit.](http://commits.kde.org/kwin/86da8e9e369523939a3e7a1ad2eade4ee2457105) 
+ Wayland: Make the launcher not restart kwin_wayland if it crashes at shutdown. [Commit.](http://commits.kde.org/kwin/dcdad5ce901e814023c4b81e8b11c769b323b39c) 
+ Add missing errno.h include. [Commit.](http://commits.kde.org/kwin/12d52f3a091dca2ffbddc28a2cb04594f900ee5b) 
+ Avoid mixing current and next state. [Commit.](http://commits.kde.org/kwin/733469d4e7648cd1df0862c8879ac364594935da) Fixes bug [#449541](https://bugs.kde.org/449541)
+ Effects/overview: Hide selection rect during dnd. [Commit.](http://commits.kde.org/kwin/665db3ad160235828202086a9e7eb79ae5424571) Fixes bug [#448573](https://bugs.kde.org/448573)
+ Effects/overview: Don't show minimized windows in desktop thumbnails. [Commit.](http://commits.kde.org/kwin/59154b710fa86d729125b0955c92b9664b14b43c) Fixes bug [#448850](https://bugs.kde.org/448850)
+ Fix mouse pointer disappearing after using zoom effect on X11. [Commit.](http://commits.kde.org/kwin/43caf2539357a1f9d5b2668a8eff151517377a7e) Fixes bug [#448537](https://bugs.kde.org/448537)
+ Effects/fallapart: Avoid animating windows while there's a fullscreen effect. [Commit.](http://commits.kde.org/kwin/75462970f344acaab5b4c661cd9a415744545935) Fixes bug [#449844](https://bugs.kde.org/449844)
+ Effects/scale: Make it not grab open and close roles. [Commit.](http://commits.kde.org/kwin/fa142dd15cb16975e24dd9bd8fb00b7d4da375e7) Fixes bug [#449832](https://bugs.kde.org/449832)
+ Backends/drm: ignore modifier env var if modifiers are not supported. [Commit.](http://commits.kde.org/kwin/9f95798fc46322314c18fdf74fdf7a2e51c917f2) 
+ Kcm/screenedge: Fix default value for TabBox.BorderActivate option. [Commit.](http://commits.kde.org/kwin/7130ebe9b3ee398254c594bdf45eca61dd2cb4f1) Fixes bug [#449720](https://bugs.kde.org/449720)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Fix showing icons in KSysguard. [Commit.](http://commits.kde.org/libksysguard/dc96519996ee4592220ee8364db6420233b1be94) 
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Avoid sorting old results based on new query input string. [Commit.](http://commits.kde.org/milou/a458e4c7a7b02bf533f463d1be19138e195d8d22) See bug [#427672](https://bugs.kde.org/427672)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Attica-kde: Allow it to use HTTP2. [Commit.](http://commits.kde.org/plasma-desktop/dbac39f42a1d4221d0deb9cdbfb12b9a0aa699c7) 
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Gracefully handle invalid color scheme setting. [Commit.](http://commits.kde.org/plasma-integration/96c85d1c0be8fc437554c1462c427bb576018e81) Fixes bug [#449613](https://bugs.kde.org/449613)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Mmplugin: Use NM autoconnect instead of wwanEnabled for mobile data. [Commit.](http://commits.kde.org/plasma-mobile/f7beb52016ac8548d33ccc0ff9ecfd1a6fdc896f) 
+ Revert "Bump KF5 requirement to 5.90". [Commit.](http://commits.kde.org/plasma-mobile/8aff6e857b73dbd6e04baba621f76dfa9c5ff5cf) 
+ Ci: Add networkmanager-qt. [Commit.](http://commits.kde.org/plasma-mobile/04eb1478a13422f428b0512c06cfa3b82285a3dc) 
+ Bump KF5 requirement to 5.90. [Commit.](http://commits.kde.org/plasma-mobile/35a73ba6bbeaaab461d002b5f3aa5ff528a10303) 
+ Quicksettings: Make mobile data quick setting use NM API directly. [Commit.](http://commits.kde.org/plasma-mobile/9b6ac61802825b0de4e9696fbe2fd511513fedab) 
+ Taskswitcher: Ensure window is in maximized state when activating. [Commit.](http://commits.kde.org/plasma-mobile/e4ad5220294153b5cec260316bd8d04c99cf6989) 
+ Taskswitcher: Remove global variable calls. [Commit.](http://commits.kde.org/plasma-mobile/e1d50b5fbfc1d4c80764acd0c9d0e7bb3a8c86a1) 
+ Mmplugin: Fix SIM being reported as locked when no sim is inserted. [Commit.](http://commits.kde.org/plasma-mobile/cfa7fa4fcf3620f8e61a72ae6f54b031302ed39a) 
+ Applets/activities & applets/krunner: Remove from repo. [Commit.](http://commits.kde.org/plasma-mobile/79bf2344c7c5d2513d0cd34198b976e4e287380b) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Move to runtime check of valid font DPI. [Commit.](http://commits.kde.org/plasma-workspace/6c2d9d4a66db35ab208f194f6a37c7d0bc634d11) Fixes bug [#449918](https://bugs.kde.org/449918)
+ Applets/battery: adjust show/hide conditions for charge limits. [Commit.](http://commits.kde.org/plasma-workspace/ef454e9d77fd48c07f810e9ab6cc0c7812f30ce7) Fixes bug [#435931](https://bugs.kde.org/435931)
+ Applets/systemtray: fix SNI context menu usage without libappindicator. [Commit.](http://commits.kde.org/plasma-workspace/b53fec3714c68ae6191f3c2207e7b7a06b53d9f5) Fixes bug [#449870](https://bugs.kde.org/449870)
+ Applets/notifications: Increase implicit size of standalone popup. [Commit.](http://commits.kde.org/plasma-workspace/843c5ae6df074dd0eb27fa773c3221b49555b4ff) Fixes bug [#448383](https://bugs.kde.org/448383)
+ Applets-digital/clock: Add workaround for QTBUG-83890. [Commit.](http://commits.kde.org/plasma-workspace/ca73442edaab24fab42fd0580dc08ca58daac0a3) Fixes bug [#448387](https://bugs.kde.org/448387)
+ Clean up "empty" mimeType containg only whitespaces. [Commit.](http://commits.kde.org/plasma-workspace/0ce1ae9ba706efac8b7066778634578eae16011c) 
+ Dataengines/apps: Use KIO::ApplicationLauncherJob for starting KService. [Commit.](http://commits.kde.org/plasma-workspace/ff46d9fd0efac8e0af7f558da553a2cb76e70cd2) 
+ [containmentactions/applauncher] Use ApplicationLauncherJob instead of. [Commit.](http://commits.kde.org/plasma-workspace/d208c478a94979738d00d7985e8ab164d9f38160) Fixes bug [#449900](https://bugs.kde.org/449900)
+ Desktop: Still show services with nodisplay=true set. [Commit.](http://commits.kde.org/plasma-workspace/9302030f6263df78a97273ca4364b317d9d8b4a3) Fixes bug [#449243](https://bugs.kde.org/449243)
+ Appstream runner: De-duplicate results from multiple sources. [Commit.](http://commits.kde.org/plasma-workspace/a9926cae23244c97c5066cf37372c7420b285099) Fixes bug [#448619](https://bugs.kde.org/448619)
+ Fix ksplash always using default theme. [Commit.](http://commits.kde.org/plasma-workspace/12f8228d487820817d77bdb5cf56da117dfee35a) Fixes bug [#446966](https://bugs.kde.org/446966)
+ Kcms/users: Fix missing template arguments before reply in FingerprintModel. [Commit.](http://commits.kde.org/plasma-workspace/b37ab685674e8ab495d54405b81b473b1e6b9d2a) 
+ De-duplicate 'Uninstall or manage addons' option in the context menu of Kickoff. [Commit.](http://commits.kde.org/plasma-workspace/31a4cdd835d99abb85899b3f1e6b30fb3fac8dc0) Fixes bug [#448564](https://bugs.kde.org/448564)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Support hardware with only one charging threshold, not both. [Commit.](http://commits.kde.org/powerdevil/29154c13a186f5d13191db4bca1984ba2a21b839) Fixes bug [#449997](https://bugs.kde.org/449997)
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Use correct DBus signal for listening for font changes. [Commit.](http://commits.kde.org/qqc2-breeze-style/ba704971527d7d07b34e0399dd3d0e80c5e72a29) 
+ Avoid needlessly reading font settings. [Commit.](http://commits.kde.org/qqc2-breeze-style/adf5b6e0c91a322746ac6bfceb037696f175c0a5) 
+ Use raw pointer instead of QPointer to track watchers. [Commit.](http://commits.kde.org/qqc2-breeze-style/e2d6f088464f8e4c87984b0a9c373f867aa8870c) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Fixup! app/SettingsBase: Only load current view on startup. [Commit.](http://commits.kde.org/systemsettings/e3aec25d07b002d65395a8c96e8912bf98b3f303) 
+ App/SettingsBase: Show the main window before QML component is loaded. [Commit.](http://commits.kde.org/systemsettings/5af70dfb694ef4b3396ec6b4afee5dc58ff96fe6) 
+ App/SettingsBase: Only load current view on startup. [Commit.](http://commits.kde.org/systemsettings/6627e89e8f9515d73135c4b8e9dd190b073ec2ff) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ [screencast] Fall back to monitor if no type is given. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/b46f565e3fe8405ef7dbdb684ebb18a4b6f5fe19) Fixes bug [#450206](https://bugs.kde.org/450206)
{{< /details >}}

