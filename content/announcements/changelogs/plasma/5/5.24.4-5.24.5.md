---
title: Plasma 5.24.5 complete changelog
version: 5.24.5
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Add Windows CI. [Commit.](http://commits.kde.org/breeze/0532fb9003027c4b8aac7bc5c5927cd5d9b0b2a9) 
+ Disable decoration on Windows and mac. [Commit.](http://commits.kde.org/breeze/95114a2bb52fedc9202dc7ad5659b769fe62d271) 
+ Add missing kcoreaddons dep. [Commit.](http://commits.kde.org/breeze/5e3bb6b46a994ff12965c7424472c8a4f87ab052) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: Fix state change emits. [Commit.](http://commits.kde.org/discover/1c93cc29032ab84834e9c12ec44df1df85811fbd) Fixes bug [#451111](https://bugs.kde.org/451111)
+ Flatpak: Improve stability of different sources integration. [Commit.](http://commits.kde.org/discover/a8ac52085d9adfbac6885b24e9b5dc83aabf6231) 
+ Flatpak: Centralise remote integration in FlatpakBackend. [Commit.](http://commits.kde.org/discover/2ad3446a4abada3ba39c1ae1a6c57bef6328d8e8) Fixes bug [#443745](https://bugs.kde.org/443745)
+ Pk: Consider multiple package ids for one upgradeable resource. [Commit.](http://commits.kde.org/discover/222122cb962780bde91a97852b6a32a7172285ae) Fixes bug [#444600](https://bugs.kde.org/444600)
+ Don't use the appdata version in the installed version string if empty. [Commit.](http://commits.kde.org/discover/6e26b05f17c57765e859661324de2de0fb4b6a36) 
+ Libdiscover: Fix Discover doesn't show license or description of local package. [Commit.](http://commits.kde.org/discover/4ef7939e7efdf47ace28810f1f2d13fd4068abe9) Fixes bug [#452150](https://bugs.kde.org/452150)
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Fix processor invocation. [Commit.](http://commits.kde.org/drkonqi/23ad5938542deb6bbcd6dd2a57d832016f00a23f) 
+ Fix the instance filter. [Commit.](http://commits.kde.org/drkonqi/7a666f3f496f5e660da939884ef42c9e0742fcf7) 
+ Instance ids have 2 hyphens not 3. [Commit.](http://commits.kde.org/drkonqi/448797086e201bf7add7a8f18a9d796e1b9e3a8a) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Remove unneeded check for comic Dataengine being valid. [Commit.](http://commits.kde.org/kdeplasma-addons/27710b95a3fd53019b4b053cbdf720354fd939d2) Fixes bug [#452596](https://bugs.kde.org/452596)
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Outputconfigurationinterface: don't crash if mode is invalid. [Commit.](http://commits.kde.org/kwayland-server/90e2d2ba41618a53dfaacee7c0d2545cc02a9c40) Fixes bug [#453042](https://bugs.kde.org/453042)
+ Fix race in wp_drm_lease_v1. [Commit.](http://commits.kde.org/kwayland-server/dc09ce85f00b3a790e2817888067c3826280dd8e) See bug [#452435](https://bugs.kde.org/452435)
+ Simplify code that announces available modes and current mode. [Commit.](http://commits.kde.org/kwayland-server/40364d2ee670a5a74318ef96c643762293ca95f0) Fixes bug [#452318](https://bugs.kde.org/452318)
+ Guard subsurface parent access. [Commit.](http://commits.kde.org/kwayland-server/6dcf73adaafeaa40c05e22df5f1c10af88df362b) Fixes bug [#452044](https://bugs.kde.org/452044)
+ Fix layer shell reset. [Commit.](http://commits.kde.org/kwayland-server/97df88c6c8af925382bb1f59b7c6ad75f28142a0) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: reduce "max bpc" to what is actually used. [Commit.](http://commits.kde.org/kwin/ba43e3d662e32a1e3cfced759a13b7ca1e1f7650) Fixes bug [#449906](https://bugs.kde.org/449906)
+ Fix unlocking wayland sessions. [Commit.](http://commits.kde.org/kwin/ee625b2d8acad2ba81693107740a12d8a03863c0) Fixes bug [#447705](https://bugs.kde.org/447705)
+ Effects/kscreen: don't use xcb on Wayland. [Commit.](http://commits.kde.org/kwin/7a26f93cb579cf6b1cbc1e32790f93be70013157) Fixes bug [#450564](https://bugs.kde.org/450564)
+ AbstractClient: Fix the current VD being always added to the plasma interface. [Commit.](http://commits.kde.org/kwin/c1d7919ad7cc8a7e55f5b93ec84de63547bc53b9) Fixes bug [#452171](https://bugs.kde.org/452171)
+ Do not send overlay geometry to text input. [Commit.](http://commits.kde.org/kwin/a35e3075a7aa4616c8930ccd3869dc4cca5d287e) 
+ Backends/drm: don't permanently disable VRR when the test commit fails. [Commit.](http://commits.kde.org/kwin/a5404234dbd26e170acb97789599fbcd705cf6cb) 
+ Backends/drm: fetch immutable blobs in DrmProperty. [Commit.](http://commits.kde.org/kwin/82c2324b5675ea2d5e3b962f270bda1d186e7326) See bug [#449285](https://bugs.kde.org/449285)
+ Waylandserver: move LockScreenPresentationWatcher to the correct place. [Commit.](http://commits.kde.org/kwin/388402c3b1b37d9f72597aae8d6670e8c0edeb14) See bug [#452334](https://bugs.kde.org/452334)
+ Xdgactivation: Demand attention when a process fails to resolve its token. [Commit.](http://commits.kde.org/kwin/5390a4978f3a3903ede15ecd94a750b37931fbd2) 
+ Waylandserver: only signal lockScreenShown once it has actually been shown. [Commit.](http://commits.kde.org/kwin/9d38f57d84fb9a6f2c4e60f7051f685842f34e0f) 
+ Add a way to ignore devices for tablet mode. [Commit.](http://commits.kde.org/kwin/7f2891ed0a89b9d7918a82d6dbdd259d1c1fa533) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/taskmanager: Disable `reuseItems` to avoid a crash. [Commit.](http://commits.kde.org/plasma-desktop/a23fb82fcac7d4af4e981a9c84780cb25a82e88a) See bug [#452660](https://bugs.kde.org/452660)
+ Containments/desktop/folder: remove stray qDebugs. [Commit.](http://commits.kde.org/plasma-desktop/7bdff088f699d76b1917a3c9bd389766fbf65412) 
+ Folder View: save desktop containment icon positions on a per-resolution basis. [Commit.](http://commits.kde.org/plasma-desktop/8f85c4658adfdf7a01c591afd79baa9eed8b79dd) Fixes bug [#360478](https://bugs.kde.org/360478). Fixes bug [#354802](https://bugs.kde.org/354802)
+ Applets/taskmanager: Update tooltip bindings when activating from keyboard. [Commit.](http://commits.kde.org/plasma-desktop/7ae51e545c44e885e98d35c78ab0df7cb369e779) Fixes bug [#452187](https://bugs.kde.org/452187)
+ Applets/taskmanager: manually set hover: true on the tooltip highlight. [Commit.](http://commits.kde.org/plasma-desktop/c32585ab16411a2f90b1ebc56c59d48a6ed5b301) 
+ Folder View: Make popup dialog wide enough for one more grid cell. [Commit.](http://commits.kde.org/plasma-desktop/c0405586a7411d995a02411979058c2335285737) Fixes bug [#417539](https://bugs.kde.org/417539)
{{< /details >}}

{{< details title="Plasma Nano" href="https://commits.kde.org/plasma-nano" >}}
+ Remove duplicate entry in desktop file (Resolves #5). [Commit.](http://commits.kde.org/plasma-nano/b54adbec6547491f2563d9e23272cc07e00df63a) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Don't use forceActiveFocus to focus the applet toolbar. [Commit.](http://commits.kde.org/plasma-nm/4272f75d698a6bf3bc3814b0862629b07a82c206) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ SpeakerTest: Fix subwoofer test. [Commit.](http://commits.kde.org/plasma-pa/183a26d9a668e1613605e708d613bceb2ffa3396) Fixes bug [#445523](https://bugs.kde.org/445523)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Revert "[Global Menu] Respect toplevel menu action visibility". [Commit.](http://commits.kde.org/plasma-workspace/536d695a08daafab4b5b0396120bcf394a531d18) 
+ [lookandfeel] Fix collapsed width of a volume/brightness OSD on a lock screen. [Commit.](http://commits.kde.org/plasma-workspace/b6d5966ce31adaec1ec96499b59d348d5d4743f4) Fixes bug [#446185](https://bugs.kde.org/446185)
+ Xdgactivation: Make sure we don't call setStartupId with an x11 token. [Commit.](http://commits.kde.org/plasma-workspace/3f4c5a22367732405a8cf16a9a96683eb2a7a827) 
+ Convert old-style Favorites resources in KActivities DB. [Commit.](http://commits.kde.org/plasma-workspace/06602886d624fc8aa9f1f39c6f002793c4b96e10) Fixes bug [#385814](https://bugs.kde.org/385814)
+ [Global Menu] Respect toplevel menu action visibility. [Commit.](http://commits.kde.org/plasma-workspace/0b938e5ffb5a8a299c9bbca5d621a8fc8f737feb) Fixes bug [#443505](https://bugs.kde.org/443505)
+ Revert "widgetexplorer: Delete newStuffDialog on closed". [Commit.](http://commits.kde.org/plasma-workspace/b309aff8c8818e7c60006089b9cd2f1eeae12702) See bug [#452865](https://bugs.kde.org/452865)
+ Revert "widgetexplorer: Replace `d->newStuffDialog` with `WidgetExplorerPrivate::newStuffDialog`". [Commit.](http://commits.kde.org/plasma-workspace/e881c89da00ce8851e28de56eebabbbe1e204174) 
+ Widgetexplorer: Replace `d->newStuffDialog` with `WidgetExplorerPrivate::newStuffDialog`. [Commit.](http://commits.kde.org/plasma-workspace/943a575a169b27859436c3b0fcdf210dd7fa258a) 
+ Widgetexplorer: Delete newStuffDialog on closed. [Commit.](http://commits.kde.org/plasma-workspace/ee1cd1ab77119cb9c57af38b297bcad1326daf09) Fixes bug [#452865](https://bugs.kde.org/452865)
+ Runners/baloo: Add missing category "text" to file search results. [Commit.](http://commits.kde.org/plasma-workspace/87f06d81878b604d671aaee783e61c166bdbfe83) 
+ Applets/digital-clock: Fix date drift. [Commit.](http://commits.kde.org/plasma-workspace/ab86bd37adeed851e460aa7ffd74c1fba0a99b12) Fixes bug [#452554](https://bugs.kde.org/452554)
+ Systemtray: Fix race in DBusServiceObserver. [Commit.](http://commits.kde.org/plasma-workspace/757b5e8c27a43a42d6a28c8dda66be08bf231b6a) Fixes bug [#422111](https://bugs.kde.org/422111)
+ Systemdialog: also consider buttonbox for dimensions. [Commit.](http://commits.kde.org/plasma-workspace/f4b7cbb63295abf579f688b6f248ccf662ea2114) 
+ Fix not working applet with same compact/full representation when hiding. [Commit.](http://commits.kde.org/plasma-workspace/d70c7c47eb9b1b47af1261595cb53263dd52fee8) 
+ Shell/scripting: Consider current activity in `desktopForScreen`. [Commit.](http://commits.kde.org/plasma-workspace/35d27fb286e3de548ca76fc97a047a5bc22a245f) Fixes bug [#452561](https://bugs.kde.org/452561)
+ Set a sane minimum size. [Commit.](http://commits.kde.org/plasma-workspace/3ba5839864aceca8cb7fa4abb4a0808777c0e85f) 
+ SystemDialog: re-add removed public properties. [Commit.](http://commits.kde.org/plasma-workspace/5e4344da09f8a26b6b3064ac8dc45d62590bf3c2) 
+ Kcm/colors: don't dull accent colour on dark themes in colorsapplicator. [Commit.](http://commits.kde.org/plasma-workspace/edf4b01cfad748441a31c8358866a3aa0ce36d6b) Fixes bug [#442820](https://bugs.kde.org/442820)
+ Wallpapers/image: Use onActivated instead of onCurrentIndexChanged. [Commit.](http://commits.kde.org/plasma-workspace/0f1c6bd9ed83ff9009ae105baf7c895b90ee659a) 
+ Appmenu: Use existing menu in compact represenation. [Commit.](http://commits.kde.org/plasma-workspace/f77565c9eebd538fdb6dbb262e53f5d9668fdd14) Fixes bug [#438467](https://bugs.kde.org/438467)
+ Change the text color when appmenu is selected or hovered. [Commit.](http://commits.kde.org/plasma-workspace/a1153e6ab35be7ad2dc99b04ae3d045a452e17c2) 
+ Don't use forceActiveFocus to set search field focus. [Commit.](http://commits.kde.org/plasma-workspace/bddd526c371ec65e87463c91e602c3428eec1e72) 
+ Applets/clipboard: Don't forward input to filter if it's disabled. [Commit.](http://commits.kde.org/plasma-workspace/109687ecc49460eec082d619cf947cfe60cd1e8f) See bug [#450989](https://bugs.kde.org/450989)
+ Klipper/autotests: Add SPDX header. [Commit.](http://commits.kde.org/plasma-workspace/87de7749dfb95ca67416d82858f4c32636f79f2c) 
+ Avoid memory leaks by misusing HistoryItem::mimeData(). [Commit.](http://commits.kde.org/plasma-workspace/d5b53effa7b602c53081d396d4a41bd77f317d99) 
+ A better fix for BUG 431673. [Commit.](http://commits.kde.org/plasma-workspace/8f44e84654ad2de932e843d0fa1e48be859f841e) 
+ Revert "Trim very long text strings in Klipper history view". [Commit.](http://commits.kde.org/plasma-workspace/970f9538ae976a416239de7500531cddec7bfd8d) 
+ Revert "[klipper] Use full text for DBus return values". [Commit.](http://commits.kde.org/plasma-workspace/e9e0a759bf477e2c5febeed22503431e3db821cf) 
+ Revert "Fix Klipper Actions content truncation". [Commit.](http://commits.kde.org/plasma-workspace/109cfac21248177aa30ca7a5ee53b76109eff28b) 
+ Revert "klipper: Add FullTextRole to get untruncated text". [Commit.](http://commits.kde.org/plasma-workspace/5f8e051cb81f84cfc58b891dfecae7a552ac84bd) 
+ Revert "klipper: Add `testTrimmedText`". [Commit.](http://commits.kde.org/plasma-workspace/efdcea7071b4b387c592534d05e62ea73131430c) 
+ Revert "applets/clipboard: Use FullTextRole in EditPage". [Commit.](http://commits.kde.org/plasma-workspace/007101257b2f8630b99c999a1098ac0ffff922b7) 
+ Revert "applets/clipboard: Use FullTextRole in SortFilterModel". [Commit.](http://commits.kde.org/plasma-workspace/031255d5dce48cdd7805d059331e4007829fe273) 
+ Revert "applets/clipboard: Generate QR code from full text". [Commit.](http://commits.kde.org/plasma-workspace/73019e83a4924e401635dee46fddf7078930ad7e) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Fix saving file dialog view options. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/e6d796d1461311d587ea08ebba0d2f0a2458b526) 
{{< /details >}}

