---
title: Plasma 5.27.8 complete changelog
version: 5.27.8
hidden: true
plasma: true
type: fulllog
---
{{< details title="Aura Browser" href="https://commits.kde.org/aura-browser" >}}
+ Remove (unused) VS Code launch/setting files. [Commit.](http://commits.kde.org/aura-browser/13c559ced4ce97633f805ed9675ac4008722dd1b) 
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Decoration: remove extra 1px padding. [Commit.](http://commits.kde.org/breeze/7a2136641d82747043ae5d5065d114dca07e9d84) 
+ Toolarea: Fix separator position on hi dpi. [Commit.](http://commits.kde.org/breeze/fa349da1ff7ec0d09821302edfa1d87bdde77e53) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ SteamOS: Use new currentBuildID in addition to currentVersion. [Commit.](http://commits.kde.org/discover/a19a44e7b88cb074da76a5fa2b3034f53b860874) 
+ Flatpak: Check if error is null before trying to log a message. [Commit.](http://commits.kde.org/discover/58060d0b0b391a4d69cc3d825307e19e3cf168e9) Fixes bug [#473497](https://bugs.kde.org/473497)
+ Flatpak: Do not crash if we failed to get unused refs. [Commit.](http://commits.kde.org/discover/5dc1b54121db0bbbb602233aa8cb7426b141678f) 
{{< /details >}}

{{< details title="kactivitymanagerd" href="https://commits.kde.org/kactivitymanagerd" >}}
+ Plugins/sqlite: Skipping insert/update when m_blockAll or m_whatToRemember==NoApplications. [Commit.](http://commits.kde.org/kactivitymanagerd/257acb4cdebd0aa10c50d547e919b451cbf7151e) Fixes bug [#397487](https://bugs.kde.org/397487)
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Set GTK scale-factor value as a uint. [Commit.](http://commits.kde.org/kde-gtk-config/bfb0896d3c88ee6f57eab0fd9e84010da304ea81) Fixes bug [#472305](https://bugs.kde.org/472305)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Thumbnail_grid: use MouseArea as delegate root. [Commit.](http://commits.kde.org/kdeplasma-addons/e90d4e1334883c738add38c30d5713cc3253f991) 
+ Thumbnail_grid: allow screen reader to announce window name when pressing Alt+Tab. [Commit.](http://commits.kde.org/kdeplasma-addons/14fb6ca6e39fe276089baf2b832832a087efb444) See bug [#472643](https://bugs.kde.org/472643)
+ Applets/userswitcher: Set an explicitly compact width. [Commit.](http://commits.kde.org/kdeplasma-addons/e95e585fb64ab206bb9c4d136dcd44bc843b2439) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kded/device: Write configuration when UPower is not available. [Commit.](http://commits.kde.org/kscreen/40849e570a001deb2d59d6c02a452a50cdb9660e) Fixes bug [#474099](https://bugs.kde.org/474099)
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ NvidiaSmiProcess: Fix missing data for GPU 2+. [Commit.](http://commits.kde.org/ksystemstats/ddce29082e9955331c4ba0e1561cc66fe54dce6c) Fixes bug [#473424](https://bugs.kde.org/473424)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ XdgToplevelWindow: Consider app responsive only if sent ping was received. [Commit.](http://commits.kde.org/kwin/991580d9b7473955c8591ecc42f7230fea4c6935) 
+ Wayland: Fix high-resolution scroll wheel discrete step calculation. [Commit.](http://commits.kde.org/kwin/80acfe344c6c55533d561f214c08d18e21a0c8e0) 
+ Wayland: Refactor the high-resolution scroll wheel step accumulator. [Commit.](http://commits.kde.org/kwin/f40814c308bd36278bc21e5220f92b2bd8ca1b67) 
+ Wayland: Send data device selections to data control on bind even if null. [Commit.](http://commits.kde.org/kwin/e8769ea8aaabe162213f9292ef51aa453f74d1f2) See bug [#459389](https://bugs.kde.org/459389)
+ Backends/drm: check explicit gpu paths for symlinks. [Commit.](http://commits.kde.org/kwin/306ea7a392e7278dd93e79f3479a8e380cad925a) 
+ Effects/screenshot: Fix screen screenshot infinite loop on X11. [Commit.](http://commits.kde.org/kwin/9b889bfd97f5e91edb073655b867f0966f965342) 
+ Input: add special handling for tabbox modifiers. [Commit.](http://commits.kde.org/kwin/98bdba20ffa38f1d7c132afb8fdfe2cf68d7f31a) Fixes bug [#473099](https://bugs.kde.org/473099)
+ Backends/drm: don't assume we never get new subpixel types. [Commit.](http://commits.kde.org/kwin/e4b2811f78691c2b727cdb61d533a29ddd802028) See bug [#472340](https://bugs.kde.org/472340)
+ Internalwindow: don't crash on pointer leave when m_handle is nullptr. [Commit.](http://commits.kde.org/kwin/59307e2678c194d61ba70130039a856490c4de67) Fixes bug [#472922](https://bugs.kde.org/472922)
+ Backends/drm: allow modesets with atomic tests if a modeset is already pending. [Commit.](http://commits.kde.org/kwin/c580f8ed516c6c93d1e0a91cd96837744badd25e) Fixes bug [#461657](https://bugs.kde.org/461657)
+ Input: use modifiersRelevantForGlobalShortcuts for tabbox events. [Commit.](http://commits.kde.org/kwin/6ea5ae9c0f37daedbf1573cf0733ced19745710e) Fixes bug [#453918](https://bugs.kde.org/453918)
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Backends/xrandr: make sure `XRandRCrtc::update` is always called. [Commit.](http://commits.kde.org/libkscreen/984b78ef3eb121d8c965c5285984b345d8241232) See bug [#472280](https://bugs.kde.org/472280)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Sensors: Correctly handle the return value of QCollator::compare. [Commit.](http://commits.kde.org/libksysguard/bf2685a3628f213e930743676a6d713e630c6a59) Fixes bug [#461070](https://bugs.kde.org/461070)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fix touchpad kded crash on X11. [Commit.](http://commits.kde.org/plasma-desktop/38b4e328f9a961114c506fcf72ef6deccd71368c) Fixes bug [#426937](https://bugs.kde.org/426937)
+ XlibBackend: port to modern signal-slot. [Commit.](http://commits.kde.org/plasma-desktop/1476b602bcab5d181d0b48b1f40a3b39923517e9) See bug [#473262](https://bugs.kde.org/473262). See bug [#426937](https://bugs.kde.org/426937)
+ XlibNotifications: port to modern signal-slot. [Commit.](http://commits.kde.org/plasma-desktop/a01d42f13dd966dfc84719d421fa357b75436d97) Fixes bug [#426937](https://bugs.kde.org/426937). See bug [#473262](https://bugs.kde.org/473262)
+ RecentFiles kcm: whattoremember: Make the value visible in the UI match the value saved. [Commit.](http://commits.kde.org/plasma-desktop/b57bf61d8d18cc4b7319b1af5514a8466761d321) Fixes bug [#397487](https://bugs.kde.org/397487)
+ PositionerTest: make sure folder model is ready before moving test. [Commit.](http://commits.kde.org/plasma-desktop/6f60632bab5bb3bd35c8faae855a16e402513fdc) 
+ FolderView: fix missing Kirigami import. [Commit.](http://commits.kde.org/plasma-desktop/75da641788d32c3f9a9f6a3f767a094c07b31b51) 
+ FolderViewLayer: use fixed interval. [Commit.](http://commits.kde.org/plasma-desktop/f539918693b8bc50cab91db265afaf16b610216b) 
+ ScreenMapper: remove handling for config before Plasma 5.25. [Commit.](http://commits.kde.org/plasma-desktop/6754146c813e3c39e3054c63f43dec6052971da5) 
+ Folder: modernize iteration. [Commit.](http://commits.kde.org/plasma-desktop/4fe101f20a4d9dea8920fa3a10aa173cf0312cff) 
+ Folder: use qset instead of qvector to improve performance. [Commit.](http://commits.kde.org/plasma-desktop/b765ea5afe663b44beda5f4cb1a90cdb6f481846) 
+ Desktop: don't save positions immediately. [Commit.](http://commits.kde.org/plasma-desktop/5be5954d885eff5c94f394ee87ec773a65b49407) 
+ Kcms/access Fix unintentional config changes. [Commit.](http://commits.kde.org/plasma-desktop/a0b14f69a3813f13feed2248aac93c1797ac8add) 
+ Folderview: cap amount of screen mappings we hold. [Commit.](http://commits.kde.org/plasma-desktop/9ed48f1def75f3ce46853ddb212b7cb7247e3a7c) Fixes bug [#469445](https://bugs.kde.org/469445)
+ Applets/taskmanager: update deprecation message for DragHelper. [Commit.](http://commits.kde.org/plasma-desktop/83df4fb1abf61326606d57e8815a418520886f51) 
+ Applets/pager: fix window/screen size/position when using Qt scaling. [Commit.](http://commits.kde.org/plasma-desktop/879492370e9b8193dc76b59a266fbdc410a7c45a) Fixes bug [#446627](https://bugs.kde.org/446627)
+ Panel: fix applet not returning focus after pressing applet shortcut. [Commit.](http://commits.kde.org/plasma-desktop/d9f39abdb402a81465dca79c791a229778e97efb) Fixes bug [#472909](https://bugs.kde.org/472909)
+ Migrate missing key handling/accessibility features from default CompactRepresentation. [Commit.](http://commits.kde.org/plasma-desktop/3eb3f64a9a1506c1868dc02c0dfb19d6212317dc) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Applet: show placeholder message in tooltip when there is no device. [Commit.](http://commits.kde.org/plasma-pa/bf27b7b4fb2746ab1a1ff85cd4b95350c49a1201) Fixes bug [#469778](https://bugs.kde.org/469778)
{{< /details >}}

{{< details title="Plasma Remotecontrollers" href="https://commits.kde.org/plasma-remotecontrollers" >}}
+ Do not take 100% CPU when a device gets disconnected. [Commit.](http://commits.kde.org/plasma-remotecontrollers/12052062a8b3f7ca53189a96b12742e1445d3201) 
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Remove wrongly-generated translations. [Commit.](http://commits.kde.org/plasma-sdk/7f5e0485d757aad112349f831f144852a08baabc) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Hide heightHelper BasicListItem. [Commit.](http://commits.kde.org/plasma-systemmonitor/e6b3dbd3fa370fb94652a208158d7ed13752a6bf) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ WindowSwitcher: move accessible properties to delegate root. [Commit.](http://commits.kde.org/plasma-workspace/6fc087180a843ed0f01f0b77a7c029cdaec15acd) See bug [#472643](https://bugs.kde.org/472643)
+ Krunner: make debut on X11 faster. [Commit.](http://commits.kde.org/plasma-workspace/433a4365b77a9400280effcd3cce3702bc2f577e) 
+ 🍒PanelView: fix panel overlapping windows when Qt scaling is enabled. [Commit.](http://commits.kde.org/plasma-workspace/61bc13fcc05af721ccc5303ffd99c1882d65a7ca) Fixes bug [#456453](https://bugs.kde.org/456453)
+ Sessionmanagement: fix broken hybrid suspend. [Commit.](http://commits.kde.org/plasma-workspace/cce9e16023e05f06b889777a4041d6421c351e81) 
+ Plasmawindowed: support `PLASMA_ENABLE_QML_DEBUG`. [Commit.](http://commits.kde.org/plasma-workspace/956cdcc5cd621d9336ac7aadf92bd43c0ce3ef22) 
+ Fixup! shellcorona: never iterate on in-destruction iteratables. [Commit.](http://commits.kde.org/plasma-workspace/7e6a316dbc0cddabeca767eca450e94542f1da9d) 
+ Shellcorona: never iterate on in-destruction iteratables. [Commit.](http://commits.kde.org/plasma-workspace/4a8b452c7b41934e01917059aa0a5a33c669d68e) 
+ Wallpapers/image: don't add files/folders to KDirWatch again in proxy model. [Commit.](http://commits.kde.org/plasma-workspace/1c23150b14776e8d1d0d84c2a5a44c3205968d78) Fixes bug [#473798](https://bugs.kde.org/473798)
+ Wallpapers/image: move some shared code to `AbstractImageListModel::load`. [Commit.](http://commits.kde.org/plasma-workspace/4fd1ecf690dccc341f8f57c2a11f5b6c342460ec) 
+ Widgetexplorer: Fix category filtering. [Commit.](http://commits.kde.org/plasma-workspace/02914fabb1e5019ae8ee7d1bdcf7c78e05bba1c6) Fixes bug [#473035](https://bugs.kde.org/473035)
+ Containmentlayoutmanager: Guard m_contentItem in setEditMode. [Commit.](http://commits.kde.org/plasma-workspace/8a559b6a2399123c9e2893d4cb46706909619155) 
+ Backport systemtraytest from master to fix flaky test. [Commit.](http://commits.kde.org/plasma-workspace/805b62653a7372192cb8728e247d856fa516cfca) 
+ Applets/kicker: deprecate KickerCompatTriangleMouseFilter. [Commit.](http://commits.kde.org/plasma-workspace/c3550e7f32ba4b919c73c4631eaba17e86d27486) 
+ Applets/kicker: deprecate DragHelper. [Commit.](http://commits.kde.org/plasma-workspace/cfe5040d3ba21d5b3c7c708d26c86afd5e763f49) 
+ Applets/kicker: deprecate Kicker.WindowSystem. [Commit.](http://commits.kde.org/plasma-workspace/ead7879a228f7a979bbf621bd94a19bea02a61a6) 
+ Applets/kicker: deprecate DashboardWindow. [Commit.](http://commits.kde.org/plasma-workspace/fe6dacac380f58d067a507885bdc7b8883be66df) 
+ Lookandfeel: make splash load faster. [Commit.](http://commits.kde.org/plasma-workspace/03a58e06c694349ee14806b1cb3529e7d0fa7655) 
+ Wallpapers/image: fix random order update logic. [Commit.](http://commits.kde.org/plasma-workspace/f26f7b1ada1b6d8c8943d6f457ca68449951dd87) See bug [#473088](https://bugs.kde.org/473088)
+ Wallpapers/image: move `sort(0)` to `SlideFilterModel::setSortingMode`. [Commit.](http://commits.kde.org/plasma-workspace/b027e20911cc13d21ae7cb458e2f71728733db39) 
+ TriangleMouseFilter: also check optional position has value. [Commit.](http://commits.kde.org/plasma-workspace/88780c843d1dbd63d18edbeecca17fddfe24a004) See bug [#473432](https://bugs.kde.org/473432)
+ TriangleMouseFilter: check intercepted item still exists in reset timer. [Commit.](http://commits.kde.org/plasma-workspace/dc146e7164a5dcb3a6ed4829398db69378111f0e) Fixes bug [#473432](https://bugs.kde.org/473432)
+ Wallpapers/image: don't add/remove a file if its parent folder is in KDirWatch. [Commit.](http://commits.kde.org/plasma-workspace/784ade6bf7b89d752137f41188e1059f7e2674ba) 
+ Wallpapers/image: fix slideshow not starting for few images or superfast computers. [Commit.](http://commits.kde.org/plasma-workspace/8eb5fbcc6263c6e761ccbcfa11919830be34a891) See bug [#473088](https://bugs.kde.org/473088)
+ Wallpapers/image: update random order before sorting. [Commit.](http://commits.kde.org/plasma-workspace/3163c60f9012d13909566489eeb7ebc6d685e6f8) 
+ Wallpapers/image: use removeDir to remove folders from KDirWatch. [Commit.](http://commits.kde.org/plasma-workspace/17a8894acee6240d374078c209cb4acc9de3c9c7) 
+ Wallpapers/image: fix potential invalid slide index in `nextSlide`. [Commit.](http://commits.kde.org/plasma-workspace/bb17444cbab7d76b18c9c4bf2294d3e87426fc6b) See bug [#473088](https://bugs.kde.org/473088)
+ Applets/notifications: Add icon to "Copy Link Address" context menu item. [Commit.](http://commits.kde.org/plasma-workspace/d985e45b182e966bb508ac57d2195c7c252c0f36) 
+ Region_language KCM: Guard glibc-related code with ifdefs. [Commit.](http://commits.kde.org/plasma-workspace/0d53975b637620cc7619a67df3397fd43f710eba) 
+ Rename REGION_LANG_GENERATE_LOCALE to REGION_LANG_GENERATE_LOCALE_HELPER. [Commit.](http://commits.kde.org/plasma-workspace/6fff5ebf2eb9ec2def4f7b5f0569b48c36061844) 
+ PanelView: clear previous focus when status changes to ActiveStatus. [Commit.](http://commits.kde.org/plasma-workspace/9c7fbe7922dc51d733cc26c109b4b27d9e944184) 
+ PanelView: also restore previous window when status changes to `ActiveStatus`. [Commit.](http://commits.kde.org/plasma-workspace/8cc5c74cf862e1ab52ca5678d0b432cc5d3a338c) 
+ Shell: avoid potential crash when previous window is gone before returning focus. [Commit.](http://commits.kde.org/plasma-workspace/fc01a7f837d06ee9e92d02f13acb79c2b06e9e3c) 
+ OSD: Fix size calculation for progress value. [Commit.](http://commits.kde.org/plasma-workspace/94b2c3d1d4a72d70d487513954601c2cf723e673) Fixes bug [#469576](https://bugs.kde.org/469576)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Profiledefaults: don't automatically suspend by default if running in a virtual machine. [Commit.](http://commits.kde.org/powerdevil/cccb8a70a35cd8dd74ccb153b00438f44484ef01) 
+ Fix the error overlay shown on other kcms. [Commit.](http://commits.kde.org/powerdevil/23b42a9b17bf15860a8d6c07cf50f378240983a9) Fixes bug [#424531](https://bugs.kde.org/424531)
+ Dimdisplay: only dim the screen at configured dim time. [Commit.](http://commits.kde.org/powerdevil/dc4a286037a802f3c18512e761b89082740aa1e9) Fixes bug [#304696](https://bugs.kde.org/304696)
+ Print error with higher logging severity when backend fails to load. [Commit.](http://commits.kde.org/powerdevil/aac426ab83dd914285877527cb120c8c1982d9b9) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Remove soft hyphens in keywords used for searching. [Commit.](http://commits.kde.org/systemsettings/054713702216bf54bd0420c6f742aa031ed48ab6) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Fix data type for accent color. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/9282a8f9802ef1938eb78259b3c8190ac9654f55) 
+ Settings: provide accent-color settings. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/455ceca14bf7a385abb819d5827a1b61f4e48913) 
+ Install kde-portals.conf in datadir. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/70c022549b56796a0460a1f9de9f208e9ef50119) 
+ Install a portals.conf file for the plasma session. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/be106d505274b9f8455d8aec6d43274e2307f7f1) 
+ RemoteDesktop: Fix relativity of absolute pointer motion. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/323b7ebd544eba9a3945e77cc9afbc7bb1942275) 
+ RemoteDesktop: Use QScreen for starting multiple streams. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/4dc89b08af602bc4aab803f8fd2e949675b10d99) 
+ Add a method using QScreen to create a screencasting stream. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/c630237c9bd961e924422788d93c4b6c7a57932d) 
{{< /details >}}

