---
aliases:
- /announcements/plasma-5.6.1-5.6.2-changelog
hidden: true
plasma: true
title: Plasma 5.6.2 Complete Changelog
type: fulllog
version: 5.6.2
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Fix isQtQuickControl check with Qt 5.7. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e335a81c191c974a516d3dae5fe8d1b6a28d6068'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127524'>#127524</a>

### <a name='kscreenlocker' href='http://quickgit.kde.org/?p=kscreenlocker.git'>KScreenlocker</a>

- Workaround problems with Qt::QueuedConnection. <a href='http://quickgit.kde.org/?p=kscreenlocker.git&amp;a=commit&amp;h=f0d2746615eb823770d14af1ca857918e3e7c399'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361008'>#361008</a>. Fixes bug <a href='https://bugs.kde.org/361007'>#361007</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- [autotests] Use DMZ-White as cursor theme. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=09dd9153d8f656706fec0585171ec59aaa244e59'>Commit.</a>

### <a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a>

- Do not unconditionally enable logging. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=649dda5e8c8f683f1650b4cf609fe094d22e3290'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126630'>#126630</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Add isQtQuickControl function and make it work with Qt 5.7. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=f59ae8992d18718d596fd332389b3fe98ff21a10'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127533'>#127533</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Fix typo in c9e6dd77591511185ac3161474d1ca1a0c1f52db. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5ded42275f40d488c4c2403ca0cbf780ad645f85'>Commit.</a>
- Break if xcb_connection has error in record event handling. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c9e6dd77591511185ac3161474d1ca1a0c1f52db'>Commit.</a>
- Revert "TaskManager: Force grouping for Icons-Only Task Manager". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=20da0a071d817b3f4e415a486ecb9c821b651648'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361172'>#361172</a>
- Avoid dead loop for xcb_poll_for_reply if xcb connection is dead. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=afd38b228960c2f5bf9a810237e04b875447301c'>Commit.</a>
- Guard round this delegate getting deleted during menu exec. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b7faa415a404ac4715a7fe4eecf23709d7e18de1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360711'>#360711</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Update tooltip when connection name gets changed. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=d5d1aa64a3929d00ad8b4fb97f8855b1ba69dccb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361348'>#361348</a>
- Improve tooltip displaying current active connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=4217dbed985314534900eb5e0629b3cf6649b465'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360668'>#360668</a>
- Editor: fix indexing for 802-1x authentication. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=a9982adb4d94a20a85744521d21bebbe8ca22865'>Commit.</a>

### <a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a>

- Fix bindings in KCM MuteButton. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=1b58de13f0fdaa65482dbb5f9242b6321edf971d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127487'>#127487</a>
- KCM: Add ScrollView to PulseView. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=554382f062f663661bba9a015a32181b3aed0886'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127477'>#127477</a>
- KCM: Fix icon size in StreamListItem. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=8ae0a7b954e31ceea98cba667b3891dc3be95709'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127466'>#127466</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- [weather bbcukmet] Readd bbcukmet ion to build & install. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cf5cca38788407f80d07080fb72e55308a02f0f1'>Commit.</a>
- [weather bbcukmet] Update Credit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0057f920d14d3664d9ea03d33916c4a0d83dbc8c'>Commit.</a>
- [weather bbcukmet] Fix for crash bug #332392 and error handling improvements. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7c66c48d7f3929917d8b8fbad9bfc19ca980347b'>Commit.</a>
- [weather bbcukmet] Trivial fix for the "clear sky" typo. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=00d6c8f9483495539725b160ed1d41016c19dba2'>Commit.</a>
- [weather bbcukmet] Handle cases where min. or max. temperatures are not reported. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=68b767046e9b185a38232b634559200c3832ea88'>Commit.</a>
- [weather bbcukmet] Update to BBC's new json-based search and modified xml. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=84fe5785bd1520f17a801cfe2e263c8ba872b273'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/330773'>#330773</a>
- [Weather] Remove no longer used custom DataEngineConsumer class. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f03482f6073fa14ee3fc6e9353a7be34a468f1dc'>Commit.</a>
- [Weather dataengine] Do not install CamelCase forward header Ion for now. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=75476af33fe295e84cdcde20043032b2fc381b1c'>Commit.</a>
- Don't crash on launchers with sorting disabled. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=55180f86b1b78206decea94c4d332bd8b2efe7ba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361166'>#361166</a>
- [calendar] Fix calendar applet not clearing selection when hiding. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d3beb0b647a543436d3d23ab82b39a2f98a384be'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127456'>#127456</a>. Fixes bug <a href='https://bugs.kde.org/360683'>#360683</a>