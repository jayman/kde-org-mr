---
aliases:
- /announcements/plasma-5.12.5-5.12.6-changelog
hidden: true
plasma: true
title: Plasma 5.12.6 Complete Changelog
type: fulllog
version: 5.12.6
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

<li>Trigger a refresh when appstream data isn't available when on aptcc. <a href='https://commits.kde.org/discover/7bae1c831f0cb63a643949eab626da38cc7105f8'>Commit.</a> </li>
<li>Don't catch unneeded value in lambda. <a href='https://commits.kde.org/discover/c0b69142bbe01861b07a6d736af114913dbc51c2'>Commit.</a> </li>
<li>Fix warning: use the right API to get the component desktop id. <a href='https://commits.kde.org/discover/9ce57430962027d76f9622990afccdbf21f0520e'>Commit.</a> </li>
<li>Improve how progress is processed when installing one application. <a href='https://commits.kde.org/discover/bc71b6874dd9c42af03a42cd9ead38d8f918e7c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391058'>#391058</a></li>
<li>PK: Don't show an error message when simulating. <a href='https://commits.kde.org/discover/09bf743ac3456e3d3e9c9a17e9353dfc12d30004'>Commit.</a> </li>
<li>Fix warnings. <a href='https://commits.kde.org/discover/5e08b2f5337e3732f7d2d052874313f164559974'>Commit.</a> </li>
<li>Properly remove the items from the FeaturedModel. <a href='https://commits.kde.org/discover/d20d2a8296e07db89b8662822a3402f4ba04b0af'>Commit.</a> </li>
<li>Flatpak: Make sure we release every time we aquire. <a href='https://commits.kde.org/discover/538489ec680dc08d20fc00b1dc21763f5d891eac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394558'>#394558</a></li>
<li>Fix there being more security updates than total updates in notifier. <a href='https://commits.kde.org/discover/9577e7d1d333d43d899e6eca76bfb309483eb29a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392056'>#392056</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13596'>D13596</a></li>
<li>ASAN: Specify buffer size. <a href='https://commits.kde.org/discover/17c0b578920f01a84d1fd0bc12c47ffff87e850a'>Commit.</a> </li>
<li>ASAN: Fix possible division by 0. <a href='https://commits.kde.org/discover/24d0bdc6e3271e0a6f68481626fd14fc057d97a4'>Commit.</a> </li>
<li>Fix warning, add missing interface. <a href='https://commits.kde.org/discover/7bebf11ad9bdc1a8bccf1e800c28b1cacb333e43'>Commit.</a> </li>
<li>Fix build with newer flatpak. <a href='https://commits.kde.org/discover/e95083f4a3a3cc64be9c98913a42759bce2716ee'>Commit.</a> </li>
<li>PK: Make sure we don't use very outdated package dbs. <a href='https://commits.kde.org/discover/3ff7ecb2a554f39048c18155a01a3c406e8acc98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390224'>#390224</a></li>
<li>Flatpak: fix early queries. <a href='https://commits.kde.org/discover/925464323fa1811943848f911ca251e865b57d54'>Commit.</a> </li>
<li>Flatpak: fix fetching status. <a href='https://commits.kde.org/discover/cbd396269366b0bca8ef7444d85faf98a84a3fc3'>Commit.</a> </li>
<li>Fix looking for popular applications by appstream id. <a href='https://commits.kde.org/discover/40a9fd6548fdb8dbc6ec4442eaa071c81d5be1ca'>Commit.</a> </li>
<li>Reduce importance of debug output. <a href='https://commits.kde.org/discover/97a0543091bd5567bb12d9b29a65b974b7c94ae9'>Commit.</a> </li>
<li>Flatpak: Add the .desktop postfix when creating the installed resources. <a href='https://commits.kde.org/discover/98842a40e1a85ad9bf6a523f684de9ce8f5e46b7'>Commit.</a> </li>
<li>Make sure we always remove duplicates before inserting. <a href='https://commits.kde.org/discover/71cea8baad0b6f1a7433b04ecffc812c1eaf1422'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393484'>#393484</a></li>
<li>Show something on the updates page after leaving it to navigate the app. <a href='https://commits.kde.org/discover/fcec1090e2f4237e9b8c3cfe175c95e77539cd68'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393593'>#393593</a></li>
<li>Disable updates checkboxes when performing updates. <a href='https://commits.kde.org/discover/5374247f47485f06ca3b1912bb534360c3f54790'>Commit.</a> </li>
<li>Prevent the system from logging out if transactions are pending completion. <a href='https://commits.kde.org/discover/d8c69ba2d61342b0a66748489d0c21972056b74a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13269'>D13269</a></li>
<li>Make sure we don't end up with a stale transaction. <a href='https://commits.kde.org/discover/a421a5c040cffccc50371cf3b652929548d05dc2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394737'>#394737</a></li>
<li>Fix dependency counting. <a href='https://commits.kde.org/discover/6d63fd3581cb39be8ef4eccfb3b697751daaacc5'>Commit.</a> </li>
<li>Simplify logic. <a href='https://commits.kde.org/discover/898cddd9e8ccdd108778c45e0d3d4dd300d1acf3'>Commit.</a> </li>
<li>Limit how often we refresh the size of the updates set. <a href='https://commits.kde.org/discover/c01a0cee7ac60f7ec26e0710df6bbb1bcfddd889'>Commit.</a> </li>
<li>PK: Re-use package instances when refreshing. <a href='https://commits.kde.org/discover/c66e4647b00bab4bd823f99da4bec6149167aae1'>Commit.</a> </li>
<li>Don't randomly remove the category on some delegates. <a href='https://commits.kde.org/discover/13eb5dd32a1296d6a495137fb7cf51ebbdb5544e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394669'>#394669</a></li>
<li>AppStream: Include some missing categories. <a href='https://commits.kde.org/discover/9ac3f08ab3f26a5037c0e77c5097ceab2c2cd4fc'>Commit.</a> </li>
<li>Abort transaction on PackageKit errors. <a href='https://commits.kde.org/discover/c0277f806a6eab8e9850e4d022ebc0419dd8efc1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394327'>#394327</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13119'>D13119</a></li>
<li>Adapt test to changes in the backend. <a href='https://commits.kde.org/discover/256233dec7c2b498257aea3de808a06eacc6041e'>Commit.</a> </li>
<li>Fix UpdateAndInstall test. <a href='https://commits.kde.org/discover/f09e3ed1934f47bd5c105f8b78ad752d749885a2'>Commit.</a> </li>
<li>Fix dummy test. <a href='https://commits.kde.org/discover/88257be7ee9bd59cab3c3d26e1b97d2ae662fdf8'>Commit.</a> </li>
<li>Properly check if we should be checking for a resource's reviews. <a href='https://commits.kde.org/discover/358a03b887781d4d49c14d91f483ca0077d03235'>Commit.</a> </li>
<li>Remove unnecessary complexity in InstallApplicationButton. <a href='https://commits.kde.org/discover/37e166aed1967f3a1276e73f3a9258b02d536bb7'>Commit.</a> </li>
<li>Adapt if the application object is destroyed. <a href='https://commits.kde.org/discover/c62613f1175c0fc12a6f5af6f08a84dc1ce3e136'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390647'>#390647</a></li>
<li>Guard against calling a null object. <a href='https://commits.kde.org/discover/0c4e121d78df65694694c0cee0c6feed0cb14da7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394543'>#394543</a></li>
<li>Report when progress changes unconditionally. <a href='https://commits.kde.org/discover/fd4f6382136fb23f8338f332f8354c4d5a15739e'>Commit.</a> </li>
<li>Resolve all packages that have been modified by the transaction. <a href='https://commits.kde.org/discover/82f69cffc8ff52fb567de70b81e4f3a06f865525'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394388'>#394388</a></li>
<li>PackageKit: Try harder to find the application desktop file. <a href='https://commits.kde.org/discover/def5203424618f0f29e25c2a9c66e15decb007c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394270'>#394270</a></li>
<li>Snap: make it possible to launch installed applications. <a href='https://commits.kde.org/discover/d9d711ff42a67a2df24317af317e6722f89bd822'>Commit.</a> </li>
<li>Fix crash. <a href='https://commits.kde.org/discover/6ee190414604f471327fc9613c96f246047d6ac2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392971'>#392971</a></li>
<li>Fix crash when rendering a delegate that extends an unexisting component. <a href='https://commits.kde.org/discover/f2d87ea7f3fb0e2ef34667694064796a3c5f43ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394048'>#394048</a></li>
<li>Don't try to guess the url location. <a href='https://commits.kde.org/discover/5d6593633f026f6f204fa712282fe6f2ef5adc9c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393987'>#393987</a></li>
<li>Fix warning. <a href='https://commits.kde.org/discover/eae21be8b2028f6adc755be4f8534d44ee604be9'>Commit.</a> </li>
<li>Fix status of flatpak transactions. <a href='https://commits.kde.org/discover/0aca22d85f8f8f45957e79a7c6ce4f3787cce724'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393909'>#393909</a></li>
<li>Hide launch button while uninstalling. <a href='https://commits.kde.org/discover/f5ad3ff0ce40cb5bdefcc8e5ca21b9b85c2062ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393910'>#393910</a></li>
<li>Fix filtering. <a href='https://commits.kde.org/discover/b4ee04e89df4b68ce7b1ce329fef019a2d305f0f'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/discover/4487b4f74296a017de81c8db7a712b0898c8641f'>Commit.</a> </li>
<li>Remove unneeded, redundant interface. <a href='https://commits.kde.org/discover/33d0fa4de7dfa224e87d76865d8c704e43ad45cd'>Commit.</a> </li>
<li>Fix warning. <a href='https://commits.kde.org/discover/bf9fe2adeaa039015783e384e77b9af5ef846983'>Commit.</a> </li>
<li>Fix warnings. <a href='https://commits.kde.org/discover/39f104324858d06b96023d604043378306961472'>Commit.</a> </li>
<li>Use a nicer delegate for the missing backends items in the sources. <a href='https://commits.kde.org/discover/a122b93badc2abc18f1b1af8092a5d0b07c5ca6a'>Commit.</a> </li>
<li>Dummy: fix remove sources. <a href='https://commits.kde.org/discover/67410504cab72896fd2e602be7f6fd90e52a85b7'>Commit.</a> </li>
<li>Have flatpak packagename include the version. <a href='https://commits.kde.org/discover/656ee2a80bdd6e79c23bf932ae18ccca4e3def96'>Commit.</a> </li>
<li>De-duplicate redundant updates. <a href='https://commits.kde.org/discover/2fc8f0c31e764693396d2fe4e764e4b8c7c14cd1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389108'>#389108</a></li>
<li>Fix updates count when listing resources that point to the same package. <a href='https://commits.kde.org/discover/36643a92cd3095d356f846935c67e37db6c45350'>Commit.</a> </li>
<li>Notifier: Don't crash if we failed to check the remote. <a href='https://commits.kde.org/discover/34a91690be72aa32b425e51d71571f4d7edee502'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393614'>#393614</a></li>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

<li>[Timer applet] Fix double speed countdown & commands run multiple times. <a href='https://commits.kde.org/kdeplasma-addons/6ce58880151dad9e0adb3607087de12bca65a6cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381173'>#381173</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13065'>D13065</a></li>

### <a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a>

<li>Remove verbose debugging statement. <a href='https://commits.kde.org/khotkeys/c5743089667d29e32191409b600e5640fbad173f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13435'>D13435</a></li>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

<li>Update kinfocenter docbook to 5.12. <a href='https://commits.kde.org/kinfocenter/e6759f109bacf36c0159d71461b226975cc6ad86'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11864'>D11864</a></li>

### <a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a>

<li>Fix leak of pipe FDs in MD RAID code. <a href='https://commits.kde.org/ksysguard/d0287c1dea39f5d3b8993ddfc38e483a048a4333'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378268'>#378268</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13664'>D13664</a></li>

### <a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a>

<li>Avoid giving an stderr to kwallet. <a href='https://commits.kde.org/kwallet-pam/8da1a47035fc92bc1496059583772bc4bd6e8ba6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393856'>#393856</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12702'>D12702</a></li>
<li>Move socket creation to unprivileged codepath. <a href='https://commits.kde.org/kwallet-pam/01d4143fda5bddb6dca37b23304dc239a5fb38b5'>Commit.</a> </li>
<li>Move salt creation to an unprivileged process. <a href='https://commits.kde.org/kwallet-pam/2134dec85ce19d6378d03cddfae9e5e464cb24c0'>Commit.</a> </li>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

<li>Update kwindecoration docbook. <a href='https://commits.kde.org/kwin/06fb902e14cc2fd4419857f6dfc007d62adaf556'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11453'>D11453</a></li>
<li>Do not save kwinrulesrc on every window opening/closing. <a href='https://commits.kde.org/kwin/9a02ed4d360ffa18c3c406ab0eb1d01ccc9c0901'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393911'>#393911</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12749'>D12749</a></li>
<li>Update seat's timestamp after waking up screen through double tap. <a href='https://commits.kde.org/kwin/69afe4d266ffbde364913d31fbcbc00769b4b390'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392754'>#392754</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12668'>D12668</a></li>
<li>Use XdgShell Unstable V6 in nested wayland platform. <a href='https://commits.kde.org/kwin/f28d44e5b2a900b42dd823de67d95ed0f21a2685'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11806'>D11806</a></li>
<li>Do not unset cursor image when cursor enters a surface. <a href='https://commits.kde.org/kwin/e3250460cc62800d706d02eda78e866efb12da55'>Commit.</a> See bug <a href='https://bugs.kde.org/393639'>#393639</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12631'>D12631</a></li>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

<li>[ProcessModel] Center more column headings. <a href='https://commits.kde.org/libksysguard/7a580e83e1b2d21cd5deacb2cbba86130e1de669'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394467'>#394467</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13031'>D13031</a></li>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

<li>[SourcesModel] Check changed file name before reloading configuration. <a href='https://commits.kde.org/milou/c0fa968c08fcd4b37ae4dcb68321b86d035ad9ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12870'>D12870</a></li>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

<li>Fix tooltip woes. <a href='https://commits.kde.org/plasma-desktop/1e218b405bee4d75a5d26a4c28da2724d967953a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382571'>#382571</a>. Fixes bug <a href='https://bugs.kde.org/385947'>#385947</a>. Fixes bug <a href='https://bugs.kde.org/389469'>#389469</a>. Fixes bug <a href='https://bugs.kde.org/388749'>#388749</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13602'>D13602</a></li>
<li>[kded kcm] Fix estimating dbusModuleName of kded plugins. <a href='https://commits.kde.org/plasma-desktop/6f491642e9b940d1f084b491408398a1df28027e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13623'>D13623</a></li>
<li>Fix accident. <a href='https://commits.kde.org/plasma-desktop/adbc35b0d82d332ac4826ddfe34d73bee04fd8a0'>Commit.</a> </li>
<li>Swap Trash for Delete action when only one is enabled and the Shift modifier is pressed. <a href='https://commits.kde.org/plasma-desktop/6f3a486330d02cd1feb10969ab56113a09f3e0af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395155'>#395155</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13508'>D13508</a></li>
<li>Fix Recent Applications sorting in Kicker and Dashboard. <a href='https://commits.kde.org/plasma-desktop/eda4d2645fa8113104721c6237e5d11724a35038'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13273'>D13273</a></li>
<li>Update kfontview docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/e62cde200798e7001e8fa7a1d308fe6470f90370'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11832'>D11832</a></li>
<li>Doc: use the exact text label of a GUI item. <a href='https://commits.kde.org/plasma-desktop/379235bd15f8b06069034f7b7ef8469fed190db4'>Commit.</a> </li>
<li>Update translations docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/cb21d756f98908ccd04cf1e4ddc37565fb7b6aba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11812'>D11812</a></li>
<li>Update colors docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/91f881a1d6570f7945f07180bc70e308b21c951d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org//D11813'>/D11813</a></li>
<li>Update kcmsmserver docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/b77559c56734f53bbcf75cedb1ad4aa821ff6052'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11817'>D11817</a></li>
<li>Update fontinst docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/5ab23426db7c61b79043cf5716ce32d376bfc214'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11831'>D11831</a></li>
<li>Fix minor typos. <a href='https://commits.kde.org/plasma-desktop/88eb65bd45e6bcfcf79ea79a62746594ec4d0e13'>Commit.</a> </li>
<li>Update systemsettings fonts docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/b7925de6507a86e55d894f26253aae3f3511fc91'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11659'>D11659</a></li>
<li>Update systemsettings cursortheme docbook. <a href='https://commits.kde.org/plasma-desktop/560de171b518766c5affc43a7f747f2d2c074d6e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11660'>D11660</a></li>
<li>Update desktop theme docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/5838eeb5d2e8bad27594340c7e4c9a1da85c2b95'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11686'>D11686</a></li>
<li>Update autostart docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/69f1e6586aefd0bc2fb097e36a02c32834355517'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11687'>D11687</a></li>
<li>Update mouse docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/50676f0bf7f66d34f95d5083bbe761a38fb21476'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11688'>D11688</a></li>
<li>Update keys docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/b35d8880cdf90f445c5242ca78cf4453286cb1f5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11690'>D11690</a></li>
<li>Update kcmnotify docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/24141c8d44e446ff6105050325961041ebd19e41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11693'>D11693</a></li>
<li>Update launchfeedback docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/6591361e2c04154d52ed457263354c3983889caa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11711'>D11711</a></li>
<li>Update clock docbook to 5.12. <a href='https://commits.kde.org/plasma-desktop/a84ab014e5f95626516075a98f1e4f1110b02a02'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11695'>D11695</a></li>
<li>Update kcmstyle docbook. <a href='https://commits.kde.org/plasma-desktop/6a62f6460e6b18f48b84f11fe30eea047c744346'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11455'>D11455</a></li>
<li>[Baloo KCM] Prevent duplicate paths in the config file. <a href='https://commits.kde.org/plasma-desktop/18674aa9e9f15213e45bd8d73e82b4bdecf686fd'>Commit.</a> </li>
<li>Some spacing. <a href='https://commits.kde.org/plasma-desktop/21e9c644e3dce7931a8e91095406424a06bb3797'>Commit.</a> </li>
<li>[Task Manager] Fix URL comparison. <a href='https://commits.kde.org/plasma-desktop/f7516196cfa49904a4b9daf337090c8f2c78ab20'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13069'>D13069</a></li>
<li>Always set applet title, even when label is disabled. <a href='https://commits.kde.org/plasma-desktop/28dc6b88538ebb9606db8678c40a2040d9d870f9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394007'>#394007</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12775'>D12775</a></li>
<li>Fix submenus not updating when switching between categories of the same size. <a href='https://commits.kde.org/plasma-desktop/c0cd1f8d49e6d63f1988d4749c366a0bef177790'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394013'>#394013</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12774'>D12774</a></li>
<li>Fix avatar picture aliasing and stretching in kickoff. <a href='https://commits.kde.org/plasma-desktop/9da4da7ea7f71dcf6da2b6b263f7a636029c4a25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369327'>#369327</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12469'>D12469</a></li>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

<li>Don't manipulate foreign windows. <a href='https://commits.kde.org/plasma-integration/6288079d936b4068fabd982dce6ff45375ac22a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13062'>D13062</a></li>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

<li>Unquote remote when importing OpenVPN connections. <a href='https://commits.kde.org/plasma-nm/46c1dcbeb0c9f8828e40be8c3b4d1288b3f34f53'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393657'>#393657</a></li>
<li>Use ScrollView from QtQuickControls in the main connection view. <a href='https://commits.kde.org/plasma-nm/7f44a45ffcf3f18309ff455152cd9b1e6834a6bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393794'>#393794</a></li>
<li>Check for device not being null. <a href='https://commits.kde.org/plasma-nm/37a924609d79e9fb87ef9ad4234ab335aa4db362'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393707'>#393707</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12716'>D12716</a></li>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

<li>[Slideshow Wallpaper] Ignore hidden files and folders. <a href='https://commits.kde.org/plasma-workspace/b39522e9e925c1dccc3759284846b53650d18eb8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394882'>#394882</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13260'>D13260</a></li>
<li>Weigh matching services by relating data used in query to their menuids. <a href='https://commits.kde.org/plasma-workspace/664b90afcfd82fdbfec8be617228dc699c27a101'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13073'>D13073</a></li>
<li>Add mappings for VirtualBox. <a href='https://commits.kde.org/plasma-workspace/b2206d16c155e3eb0b390c7ae177ee71b79d0362'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13060'>D13060</a></li>
<li>Remove legacy Wine hack-around from rules rc. <a href='https://commits.kde.org/plasma-workspace/30654e2158c454de664d54073544ae2ca295ce2f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393787'>#393787</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13058'>D13058</a></li>
<li>[Service Runner] Do startsWith check case-insensitive. <a href='https://commits.kde.org/plasma-workspace/b998e68d1128a246d6d9371909ead63cc95dbfd6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394202'>#394202</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12927'>D12927</a></li>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

<li>Update systemsettings docbook to 5.12. <a href='https://commits.kde.org/systemsettings/b3b57edf86f3c12cf5f6ff51a4ab9e55027a970e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11607'>D11607</a></li>

### <a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a>

<li>Split replaceAccount from addAccountToCache. <a href='https://commits.kde.org/user-manager/ff88e24e4380a341f70f9b005acbce2ae9afa60a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/336994'>#336994</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12837'>D12837</a></li>
<li>Enable anti-aliasing for user avatars. <a href='https://commits.kde.org/user-manager/da05675b4292af42e6757284e60700a8bb25856d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12468'>D12468</a></li>