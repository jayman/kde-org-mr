---
title: Plasma 5.24.2 complete changelog
version: 5.24.2
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ [wizard] Don't set minimum size. [Commit.](http://commits.kde.org/bluedevil/4816c46f0aa749153c9044d4c1ed55fb7192d3e1) 
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Kstyle: make MDI title buttons legible. [Commit.](http://commits.kde.org/breeze/594a5157e7cafe9627d79686d5e09ee6a0357d93) Fixes bug [#438964](https://bugs.kde.org/438964)
+ Make sure the size of the menu title is big enough. [Commit.](http://commits.kde.org/breeze/9ccec8c9e09471e787b3a65e8e0cda0e46d1266c) Fixes bug [#450333](https://bugs.kde.org/450333)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Enable -DQT_NO_URL_CAST_FROM_STRING and fix compilation. [Commit.](http://commits.kde.org/discover/9476b7b24f78e9b4ab9c6e6c7dd970e7396fb70f) 
+ UpdatesPage: add elision property for size label. [Commit.](http://commits.kde.org/discover/3b2bda2c5b47a669b846e0a7b613643511da74f9) Fixes bug [#450339](https://bugs.kde.org/450339)
{{< /details >}}

{{< details title="KDE Window Decoration Library" href="https://commits.kde.org/kdecoration" >}}
+ Hide tooltip when pressing button. [Commit.](http://commits.kde.org/kdecoration/0bc7ab1aa2ed5690893834cc39b339200e6a7d8b) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ KCM: fix crash when editing disabled display output's refresh rate. [Commit.](http://commits.kde.org/kscreen/59231e0456d2a8b423c26b94c5840178cebda23d) Fixes bug [#450265](https://bugs.kde.org/450265)
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Seat: Do not assert on corner touch cases. [Commit.](http://commits.kde.org/kwayland-server/a9f7072f7510e543d47b84ef69be99e58f2fea63) Fixes bug [#450338](https://bugs.kde.org/450338)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: block input with the placeholder output. [Commit.](http://commits.kde.org/kwin/5511c1c84948b24bea2e527f27bfc59f50cb1b1c) 
+ Effects/blur: Avoid shrinking unrelated opaque regions. [Commit.](http://commits.kde.org/kwin/00a8de6c011954f379fe6f0542bdc9c502de45f3) 
+ Effects/contrast: Remove paint area tracking. [Commit.](http://commits.kde.org/kwin/5e448f063cfff0a15068a018323653a0e164908d) 
+ Backends/drm: ignore enabled state of outputs for the lifetime of surfaces. [Commit.](http://commits.kde.org/kwin/29bc1173c70b2a31753c791aa60fb33ee320440a) Fixes bug [#450501](https://bugs.kde.org/450501). See bug [#450358](https://bugs.kde.org/450358)
+ Backends/drm: fix overscan. [Commit.](http://commits.kde.org/kwin/5897605759775942aecedc4405a84061eb47ee2f) 
+ Inputmethod: Fix read setting. [Commit.](http://commits.kde.org/kwin/c7988e8c8335bd3ee922b77eb9f53c8274310ce2) Fixes bug [#450430](https://bugs.kde.org/450430)
+ Use the minimum of workspace area size and panel size for input method placement. [Commit.](http://commits.kde.org/kwin/7e25e20097fe8d140f7408b76fbf374fe5f33690) 
+ Check lockscreen status for fullscreen effects. [Commit.](http://commits.kde.org/kwin/39153cf77aac120476402b21c9fdd357ec1d40ce) Fixes bug [#450331](https://bugs.kde.org/450331)
+ Xwayland: Guard  against offers arriving after leaving surface. [Commit.](http://commits.kde.org/kwin/70766a4416bb421dfda18939541723890789345f) Fixes bug [#449644](https://bugs.kde.org/449644)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applet/pager: Divide window size by devicePixelRatio on X11 when it's >1. [Commit.](http://commits.kde.org/plasma-desktop/78b1d26134647d890ee55e1ad7607cbd77e538be) Fixes bug [#446627](https://bugs.kde.org/446627)
+ Applet/pager: Properly adjust the icon size. [Commit.](http://commits.kde.org/plasma-desktop/5fc9525a868ead27ceed619046ebb90758c04de7) 
+ Fix build. [Commit.](http://commits.kde.org/plasma-desktop/196299ab655f0a29d3b45541459a5f326b82ce1d) 
+ Only trust the expiration date if it's less than 24 hours. [Commit.](http://commits.kde.org/plasma-desktop/1842af5da5d2f73415c94bdfb3de0cc5a89fa6c2) 
+ Fix missing variable name. [Commit.](http://commits.kde.org/plasma-desktop/c4aecfa6475a9db4f1ef539e5e6af6b11a16536f) 
+ Add a granular cache preference thing to attica-kde. [Commit.](http://commits.kde.org/plasma-desktop/122f59d0a9f99abfa155be5ea8889927a8dfc4bb) 
+ Taskmanager: Hide unneeded scrollbar. [Commit.](http://commits.kde.org/plasma-desktop/89751cb71edb7e05ba7059a724c22056e7a18413) Fixes bug [#450463](https://bugs.kde.org/450463)
+ Emoji Selector: Use a more appropriate icon for the Symbols page. [Commit.](http://commits.kde.org/plasma-desktop/e903d9e291246428bdab93c83b58ab4b0624a999) Fixes bug [#450380](https://bugs.kde.org/450380)
+ Applets/minimizeall: make active indicator line touch panel edges. [Commit.](http://commits.kde.org/plasma-desktop/b56519113f50fc707853fa2c5456b5e15789744a) Fixes bug [#444810](https://bugs.kde.org/444810)
+ Applets/showdesktop: Give it an active indicator line. [Commit.](http://commits.kde.org/plasma-desktop/95acde4cb3edf1312b8153fde4e6376f3f375b4e) Fixes bug [#447998](https://bugs.kde.org/447998)
+ Foldermodel: hide paste action when right-clicking on files. [Commit.](http://commits.kde.org/plasma-desktop/542e9a249d1839750ee67695742a8cb7c38edcca) Fixes bug [#448913](https://bugs.kde.org/448913)
+ Folder View: hide additional settings not relevant to list view. [Commit.](http://commits.kde.org/plasma-desktop/4ca5fcb41de9ab0831a161ac42b2f0217ccc107e) Fixes bug [#450063](https://bugs.kde.org/450063)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Actiondrawer: Darken landscape drawer background to be easier to read. [Commit.](http://commits.kde.org/plasma-mobile/68440f04bab51895710dad9be99b113001a0cce0) 
+ Quicksettings: Ensure mobile data quick setting says not available if not available. [Commit.](http://commits.kde.org/plasma-mobile/e1b90618e3a4dabe0aa82e248e59143e62b66200) 
+ Ensure we have a default wallpaper. [Commit.](http://commits.kde.org/plasma-mobile/6322b1ac9c30b9fd30d492d5a9f42425ce83616e) 
+ Homescreen: Fix app drawer not interactable in empty space. [Commit.](http://commits.kde.org/plasma-mobile/bb1e21b15d2ec7454a2d25f43f36842cba291b46) 
+ Taskpanel: Fix close action. [Commit.](http://commits.kde.org/plasma-mobile/3420067e6f1587af4cb77e2668d6262f696e3232) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Kded: don't show "connection activated" notifications on launch/login. [Commit.](http://commits.kde.org/plasma-nm/622f53da6184dc90d5834a9e75fb1cb1c3af5986) Fixes bug [#399973](https://bugs.kde.org/399973)
+ Don't use warning icon in notification when the user disconnects a VPN. [Commit.](http://commits.kde.org/plasma-nm/c53e97839cb49c83b818064e88ada5d836202791) 
+ Use error icon for missing VPN plugin notification. [Commit.](http://commits.kde.org/plasma-nm/c74a4e07f5ee5a2a87a08d937137fed1e741e641) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Applets/clipboard: Focus on the search field for the first time opening the applet. [Commit.](http://commits.kde.org/plasma-workspace/33df8fc7964153b899de96ceacd00edce885b560) See bug [#448811](https://bugs.kde.org/448811)
+ Applets/clipboard: Make Up/Down arrow keys work in ClipboardPage. [Commit.](http://commits.kde.org/plasma-workspace/9e0baec1dfaf81d4a4d3346d28a3f8ff0d56329b) Fixes bug [#448811](https://bugs.kde.org/448811). Fixes bug [#450040](https://bugs.kde.org/450040)
+ Applets/panelspacer: Fix optimal size calculation. [Commit.](http://commits.kde.org/plasma-workspace/b382f61c914072732cabd9b5ae0d6e7b12fc20a9) Fixes bug [#431668](https://bugs.kde.org/431668)
+ Fix overdraw on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/4fa725efa9b060d8368d300d9edfe18110c2f2ff) 
+ PanelView: Do not crash when the state is transitioning. [Commit.](http://commits.kde.org/plasma-workspace/3520cd5a6251d568af7a1b286aa041df27851dfe) Fixes bug [#373461](https://bugs.kde.org/373461)
+ Fix launch kscreen kcm in font kcm. [Commit.](http://commits.kde.org/plasma-workspace/bd99299a1a07361b905436c3723e254317f0ee3a) 
+ Don't install two copies of kcm_fontinst. [Commit.](http://commits.kde.org/plasma-workspace/5cea29ed6ac0cb789520421325bb547bd2e8b614) 
+ Lock/login themes: stop adjusting shadows based on color scheme. [Commit.](http://commits.kde.org/plasma-workspace/a31e4ce4ec4c0a7083b9bdfa962dafec4ac8f784) Fixes bug [#449985](https://bugs.kde.org/449985)
+ Top-align lock/login/logout screen action buttons. [Commit.](http://commits.kde.org/plasma-workspace/b4d5e587f5e676eb54eb4d97342df878811d58e8) Fixes bug [#450238](https://bugs.kde.org/450238)
+ [kcms/user] Set interactive auth flag for more calls. [Commit.](http://commits.kde.org/plasma-workspace/2b9ea88c9d3fe7b7530c20e0a6fe1bf70d0accde) Fixes bug [#450122](https://bugs.kde.org/450122)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Sidebar: Load placeHolderWidget only when needed. [Commit.](http://commits.kde.org/systemsettings/f2909a903e125d904ae3e49f372634c2fc39db9d) See bug [#449853](https://bugs.kde.org/449853)
{{< /details >}}

