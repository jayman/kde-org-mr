---
date: 2022-05-12
appCount: 120
image: true
layout: gear
---
+ dolphin: The terminal panel will keep in sync with quick folder changes now, [Commit](http://commits.kde.org/dolphin/e70e12e3bdf3ce4e9cca4c8f003655ea10b21d7e), [#391380](https://bugs.kde.org/391380), [#416690](https://bugs.kde.org/416690)
+ kate: Fix crash on session restore, [Commit](http://commits.kde.org/kate/3414b6269a616a0dee4237f66e77772669ee59ff), [#453152](https://bugs.kde.org/453152)
+ kalendar: Fix 'next week' button in the week's view [Commit](http://commits.kde.org/kalendar/aba5daa965e4ed61ec1ee728abd6963bbdc7e093)
