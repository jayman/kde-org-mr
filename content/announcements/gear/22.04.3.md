---
date: 2022-07-07
appCount: 120
image: true
layout: gear
---
+ elisa: Fix the seek slider with newer versions of qqc2-desktop-style [Commit](https://invent.kde.org/multimedia/elisa/-/commit/b3d081d14a6da1e18ac220867eb7961b8a017837), [#455339](https://bugs.kde.org/show_bug.cgi?id=455339)
+ konsole: Better recognition for URIs, e.g. [Commit](http://commits.kde.org/konsole/e925acc1ca60ef1116fa84cc35328d5accad23c5), [#455166](https://bugs.kde.org/455166)
+ korganizer:  Fix a crash when completing a to-do in the summary view [Commit](https://invent.kde.org/pim/korganizer/-/commit/c4e58599b3f3eeece0d94d5cee9259be904cdbe7) [#454536](https://bugs.kde.org/454536)
