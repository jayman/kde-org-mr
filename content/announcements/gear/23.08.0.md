---
description: KDE Gear ⚙️ 23.08 adds features and improvements to dozens of your favorite tools.
authors:
  - SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2023-08-24
hero_image: hero.png
images:
  - /announcements/gear/23.08.0/Hero.webp
layout: gear
noContainer: true
scssFiles:
- /scss/gear-23-08.scss
outro_img:
  link: many_apps.png
  alt: Screenshots of many applications
title: "KDE Gear ☀️ 23.08"
subtitle: We got you covered 🏖️
merkuro:
  - url: https://cdn.kde.org/screenshots/merkuro/calendar-month.png
    name: Merkuro month view
  - url: https://cdn.kde.org/screenshots/merkuro/calendar-week.png
    name: Merkuro week view
  - url: https://cdn.kde.org/screenshots/merkuro/contact.png
    name: Merkuro contact app
tokodon:
  - url: tokodon-admin1.png
    name: "List of users"
  - url: tokodon-admin2.png
    name: "Account information for a specific user"
  - url: tokodon-admin3.png
    name: "List of blocked instances"
kwordquiz:
  - url: https://cdn.kde.org/screenshots/kwordquiz/home.png
    name: Homepage
  - url: https://cdn.kde.org/screenshots/kwordquiz/flashcard.png
    name: Flashcard mode
  - url: https://cdn.kde.org/screenshots/kwordquiz/question-response.png
    name: Question response mode
  - url: https://cdn.kde.org/screenshots/kwordquiz/multiple-choice.png
    name: Multiple choice mode
  - url: https://cdn.kde.org/screenshots/kwordquiz/editor.png
    name: Editor
okular:
  - url: Okular_signing_01.png 
    name: "Adding metadata to a signature"
  - url: Okular_signing_02.png 
    name: "Signature with background image and metadata"
itinerary:
  - url: kde-itinerary-timeline.png  
    name: "Itinerary's new revamped timeline"
  - url: kde-itinerary-matrix-location-sharing_c.png  
    name: "Share locations with your friends on Matrix"
draft: false
---

{{< container class="intro" >}}

We create software for people, and the KDE Gear releases are the result of that. Every four months we publish new updates of a large number of KDE apps and software libraries. We create new programs to meet more of your needs, implement more features so you can adapt to an ever-changing digital world, and make our software faster, more efficient, more reliable. We also port it to more platforms so you can run it on more devices: your laptop, your game console, your phone, anywhere.

{{< announcements/donorbox >}}

Read on to find out what's new in KDE Gear 23.08:

{{< /container >}}

{{< section class="dolphin py-5" >}}

{{< figure src="https://apps.kde.org/app-icons/org.kde.dolphin.svg" height="96" width=96 >}}

## [Dolphin](https://apps.kde.org/dolphin/)

No system is complete without a reliable file explorer. Dolphin lets you navigate your folders and files, move and copy things around, connect to servers, and in general manage everything in your storage local and remote.

Dolphin 23.08 tweaks what it shows and how it shows it to give you a better idea of what you are looking at and what you can do with it. Dolphin hides temporary and backup files, uncluttering your view and stopping you from accidentally tampering with them. Other smaller details, like showing the progress of the calculation of the size of an item as it is happening, and the information of a selected file in the information panel (instead of showing the information of every file the cursor rolls over) help you get a clearer idea of what is going on in each moment.

{{< video src="Dolphin_filesize.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1000" >}}

Dolphin has supported tabs and split views for a long time. In 23.08 you can open a duplicate of a tab just by double-clicking on it, and, when you are in split view mode, there are new context menu items and keyboard shortcuts to let you quickly move or copy items from one view to the other.

{{< /section >}}

{{< section class="okular py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.okular.svg)

## [Okular](https://apps.kde.org/okular/)

Okular, KDE's document viewer, reinforces its digital PDF signing features by allowing signees to add various pieces of metadata, such as "reason" and "location" to the signature, and also to optionally add a background image behind it. To more easily access the "Digitally Sign" action we have placed it at the top level of its hamburger menu.

{{< screenshots name="okular" >}}

Other changes include an option that allows you to choose the default scaling mode when printing PDFs, and you can copy an annotation’s text to the clipboard using the context menu for the annotation’s entry in the annotations sidebar.

{{< /section >}}

{{< container >}}

{{< figure src="org.kde.kalendar.svg" height="96" width=96 >}}

## [Merkuro](https://apps.kde.org/merkuro.calendar/)

We renamed Kalendar to Merkuro since the application not only lets you manage your events and tasks any more, but also your contacts.

And soon we'll be adding email!

Apart from renaming and revamping its looks, Merkuro splits the application in two, so you can have just calendaring, only contacts, or both, depending on your needs.

{{< screenshots name="merkuro" >}}

{{< /container >}}

{{< diagonal-box color="purple" logo="https://apps.kde.org/app-icons/org.kde.itinerary.svg" >}}

## [Itinerary](https://apps.kde.org/itinerary/)

Itinerary is KDE's travel assistant. Apart from reading and importing data from confirmation emails and SMS sent by airline companies, Itinerary can now import online railway tickets using the booking reference and passenger name, and grab booking details directly from the operator's website. Note that this is currently only available for Deutsche Bahn and SNCF.

We have also improved the extraction of data from documents from the following transport companies: Aegean Air, Air Asia, ATPI, B&B Hotels, Best Western, České dráhy, Deutsche Bahn, European Sleeper, Eventbrite, FlixBus, Gepard Express, Grimaldi Lines, MÁV, ÖBB, Ouigo, Qatar Airways, SNCB, SNCF, Sunny Cars, Vueling, and Westbahn. We have also increased the number of types of documents you can add to a reservation to include passes or program membership cards and email messages.

We have redesigned the pages to query connections and they now display the graphical line identifier (if available) and you can select which type of public transport you would prefer to use. In a similar vein, Itinerary’s timeline has been revamped, making more entries editable.

{{< screenshots name="itinerary" >}}

{{< /diagonal-box >}}

{{< container >}}

{{< figure src="https://apps.kde.org/app-icons/org.kde.skanpage.svg" height="96" width=96 >}}

## [Skanpage](https://apps.kde.org/skanpage/)

Skanpage, KDE's scanning utility, now lets you re-order multi-page scans using drag-and-drop, and offers more adjustment options, like brightness, contrast, gamma, and color balance.

{{< video src="skanpage_edited.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1920" >}}

The preview has also been improved and lets you select multiple specific parts of the image to scan, or automatically split the area into two pages. This is especially useful when scanning books.

{{< /container >}}

{{< section class="kate py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.kate.svg)

## [Kate](https://apps.kde.org/kate/)

KDE's Advanced Text Editor continues on its road to becoming an all round IDE for developers, and has added support for the GLSL language and Godot's game design engine in its LSP client, and includes a QML language server option when using Qt 6.

Another developer-friendly feature is that the debugger plugin has received interactive GDB navigation buttons in its toolview, a configuration window, and a clearer name.

{{< /section >}}

{{< section class="neochat py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.neochat.svg)

## [NeoChat](https://apps.kde.org/neochat)

NeoChat is a chat app that lets you take full advantage of the Matrix network.

![NeoChat](https://cdn.kde.org/screenshots/neochat/application.png)

Apart from a visual overhaul, NeoChat can now display location events and also a map with the location of all the users currently broadcasting their location using Itineray's Matrix integration. Great for locating where your friends are.

![NeoChat map](neochat-map.png)

{{< /section >}}

{{< section class="tokodon py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.tokodon.svg)

## [Tokodon](https://apps.kde.org/tokodon/)

Tokodon is an app that helps you post to Mastodon. This is another app that has received a visual overhaul and the timeline is now much smoother.

![Tokodon homepage with the main timeline](https://cdn.kde.org/screenshots/tokodon/tokodon-desktop.png)

More importantly, if you run your own Mastodon instance, you can now manage and moderate your instance directly from within Tokodon. Version 23.08 gives you the tools to manage users and list the instances you want to (or not) federate with.

{{< screenshots name="tokodon" >}}

Other new features let you share posts through other apps, save your access token securely in KWallet, view all blocked and muted accounts, and pin your status to your profile.

It's also possible to browse the trending tags and posts of your instance.

![Tokodon list of trending tags](tokodon-trending.png)

We have also upgraded the multimedia framework, making the video player much more reliable.

{{< /section >}}

{{< section class="elisa py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.elisa.svg)

## [Elisa](https://apps.kde.org/elisa)

Elisa is KDE's elegant and feature-rich music player.

The version of Elise being released today does a better job at finding cover files and introduces a smooth transition when switching from one album cover to another.

Another nice addition is that Elisa Shuffle and Repeat settings are controllable via MPRIS. This  means you can control these features from the Media Player widget, or, indeed from KDE Connect on your phone.

{{< video src="eliza_shuffle.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1920" >}}

{{< /section >}}

{{< section class="kwordquiz py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.kwordquiz.svg)

## [Kwordquiz](https://apps.kde.org/kwordquiz)

KWordQuiz, the flashcard based educational app, was ported to QML and redesigned from the ground up.

{{< screenshots name="kwordquiz" >}}

{{< /section >}}

{{< section class="gwenview py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.gwenview.svg)

## [Gwenview](https://apps.kde.org/gwenview)

Gwenview improves the transition from image to image, as well as the usability of the zoom bar, and cleans up toolbars and options.

{{< video src="gwenview_transition.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1280" fig_class="shadow" vid_class="small-radius" >}}

{{< /section >}}

{{< section class="spectacle py-5" >}}

![](https://apps.kde.org/app-icons/org.kde.spectacle.svg)

## [Spectacle](https://apps.kde.org/spectacle)

Spectacle, the powerful screenshot utility, lets you easily select annotations as it shows an outline when you hover the cursor over them.

{{< video src="spectacle_annotations_edited.mp4" loop="true" muted="true" autoplay="true" controls="false" width="1280" >}}

{{< /section >}}

{{< container >}}

## ...And also this

* **[Partition Manager](https://apps.kde.org/partitionmanager)**, the tool for creating and modifying partitions on disks, gets a new icon which distinguishes it from Filelight.

* **[Konqueror](https://apps.kde.org/konqueror)**, KDE's classic web browser/file explorer, restores the last tabs and windows on restart and you can change the browser identification.

* **[Konsole](https://apps.kde.org/konsole)**, KDE's feature-rich terminal emulator, can send you a notification once a long-running task finishes.

## In Memoriam

![Daniel Gutiérrez left us in July.](dani01.jpg )

KDE Gear ⚙️ 23.08 is dedicated to our good friend [Dani Gutiérrez Porset](https://dot.kde.org/2012/10/18/akademy-2013-bilbao). Dani was a generous and militant Free Software activist that organised many KDE-related events, including the [Akademy of 2013](https://web.archive.org/web/20170211153031/http://www.danitxu.com/blog/2013/07/27/akademy-2013-game-over/).

Dani left us for thee great big server in the sky on the 16th of July. Goian bego.

---

{{< announcements/gear_major_outro >}}

{{< /container >}}
