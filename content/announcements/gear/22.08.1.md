---
date: 2022-09-08
appCount: 120
image: true
layout: gear
---
+ kmail: Searching for messages does work again ([Commit](http://commits.kde.org/akonadi/2acf92c81c4317950760e22c4fc1c5e216de6012), fixes bugs [#458202](https://bugs.kde.org/458202) and [#458245](https://bugs.kde.org/458245)).
+ kate:  Fix a crash when there are no search results ([Commit](http://commits.kde.org/kate/6b29b6ba03b9e01b03c5af8dd71c36a62bbb3344))
+ krdc:  Access dates are displayed properly now ([Commit](http://commits.kde.org/krdc/f4824d49759ba1cf1711e432ea4fe42a7359d380), fixes bug [#458587](https://bugs.kde.org/458587))
