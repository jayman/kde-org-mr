---
aliases:
- ../../kde-frameworks-5.81.0
qtversion: 5.14
date: 2021-04-10
layout: framework
libCount: 83
---


### Baloo

* [SearchStore] Explicitly narrow timestamps for range query
* Add now mandatory args parameter to QProcess::startDetached()
* [MetadataMover] Update filename terms when moving/renaming file (bug 433116)

### BluezQt

* Fix unity compile support

### Breeze Icons

* Added branches with leaves to Kmymoney icon
* Add a few symlinks for "configure" and "help-donate" (bug 435150)
* Add KMyMoney Pie-Chart Icon
* Link svn-* icons to new vcs-* icons
* Add vcs-* icons for Kate
* Make lock icon filled status consistent (bug 244542)
* Remove 22 brightness icons in 16 size folder
* Fix location of brightness icons
* Add high-brightness and low-brightness icons

### Extra CMake Modules

* ECMGenerateExportHeader: do sanity check for version argument values
* Fix warning about wayland-scanner code arg

### KActivities

* Activate activity manager asynchronously

### KAuth

* Un-overload HelperProxy::progressStep() signal

### KCMUtils

* Add loaded signal to KCModuleData to handle delayed loading

### KCompletion

* Un-overload KLineEdit::returnPressed(const QString &) signal => returnKeyPressed
* Un-overload KCompletionBox::activated(const QString &) signal => textActivated
* Un-overload KComboBox::returnPressed(const QString &) signal, by deprecating returnPressed()

### KConfig

* Relicense file to LGPL-2.0-or-later
* kconfig_compiler: Explicitly open input file for reading
* kconfig_compiler: change how paramString() creates strings

### KConfigWidgets

* Introduce KHamburgermenu (bug 368421)

### KCoreAddons

* Enable Unicode support in QRegularExpression where needed

### KCrash

* Document why we close FDs

### KDED

* Make kded shut down cleanly during systemd session teardown

### KDELibs 4 Support

* KComponentData: add a link to the KF5 porting notes

### KFileMetaData

* Enable Unicode support in QRegularExpression where needed

### KGlobalAccel

* Only use unistd/getuid when available
* Don't let kglobalaccel run if KDE_SESSION_UID mismatches

### KHolidays #

* Make it compile with unity cmake support
* Handle negative years in easter and pascha calculations (bug 434027)

### KIconThemes

* Revert "avoid race condition on loading the plugin"
* Revert "add private header to avoid extern in .cpp file"
* Don't register our engine per default
* More robust handling of missing global KDE themes
* Ensure qrc + QDir::searchPaths work for icons (bug 434451)
* More robust and complete setup of the scaled test environment
* Produce output with the request devicePixelRatio
* Properly render non-square icons
* Remove the assumption that SVG icons are squares in icon loading
* Retain non-square icon sizes in KIconEngine::actualSize()
* Revert icon size behavior changes
* Align handling of non-square icons with how Qt behaves

### KIO

* FileCopyJob: fix regression when copying a data: URL
* Handle errors during xattr copy in a more robust way
* Port ktelnetservice away from kdeinitexec
* Handle .theme files correctly (bug 435176)
* Remove KCoreDirListerCache::validUrl, let the job emit error instead
* PreviewJob: Initialize cachesSize with 0, only pass size > 0 to shmget, improve createThumbnail (bug 430862)
* KNewFileMenu: use destination side to stat destination (bug 429541)
* MimeTypeFinderJob: don't put job on hold for local files
* FileCopyJob: port to the async AskUserActionInterface
* Fix crash in ApplicationLauncherJob(service) when service is null
* Don't try to get mimetypes on empty urls
* Fix appending file extensions in KFileWidget

### Kirigami

* Add a humanMoment unit to Kirigami.Units
* Make the luma conversion of a color api accessible
* Auto fire SearchField's accepted, with optional extra delay (bug 435084)
* Make globaltoolbar colorset customizable
* Lower duration to change color for ActionButton
* Fix focus handling in OverlaySheet to be managed as one FocusScope (bug 431295)
* BasicListItem: partially silence binding loop
* [FormLayout] Use layout boundaries on twin layout hints (bug 434383)
* Make SwipeNavigator only allow the active page to be focused
* Consider Ubuntu Touch to be mobile
* [controls/Avatar]: Get rid of 'both point size and pixel size' set warning
* Remove link to deprecated ApplicationHeader
* the visible part should always at least be as tall as item (bug 433815)
* Fix potential crash in SizeGroup (bug 434079)
* turn contentItemParent into a FocusScope (bug 433991)

### KJobWidgets

* Introduce KUiServerV2JobTracker

### KNewStuff

* qtquickengine: Do not forward intermediate states
* quickengine: Emit entryEvent signal with enum which is exposed to QML
* Create a NewStuff.Action component, add NewStuff.Settings global
* Less risk of infinite spinner on uninstalling KPackage based things (bug 434371)

### KNotification

* Relicense files to LGPL-2.0-or-later
* Don't close resident notifications when action is invoked
* Implement inline replies on Android
* Add an inline reply notification to the example
* Add KNotificationReplyAction for using inline-reply Notification API

### KParts

* Add a new signal to replace the now deprecated completed(bool)

### KRunner

* Deprecate concept of delayed runners & related methods
* Deprecate methods to remove matches in RunnerContext

### KService

* Deprecate KPluginInfo::fromKPartsInstanceName, completely unused

### KTextEditor

* Don't warn about unsaved changes when closing if blank and unsaved (bug 391208)
* Use QPalette::highlight for the scrollbar minimap slider (bug 434690)
* Avoid gaps in indentation line drawing
* Don't use F9 & F10 as shortcuts
* Use Okular's QScroller settings
* Add basic touchscreen support
* [Vimode] Improve sentence text object
* [Vimode] Fix paragraph text object in visual mode
* Restrict horizontal range of cursor to avoid unintentionally wrapping (bug 423253)
* Turn on line numbers & modification markers
* Update remove-trailing-spaces modeline
* Add option to keep spaces to the left of cursor when saving (bug 433455)
* Remove unneeded options setting
* Search: Enable Unicode support in QRegularExpression
* [Vimode] Show search wrapepd hint for # and * motions
* [Vimode] Use ViewPrivate::showSearchWrappedHint() for consistency;
* Move showSearchWrappedHint() to KTextEditor::ViewPrivate
* [Vimode] Only display "Search wrapped" message for n/N motions
* Ensure we use unicode regex where needed
* Fix spellcheck word detection for non-ASCII (bug 433673)
* Fix auto-completion for non ASCII words (bug 433672)

### KTextWidgets

* Deprecate the KFind::highlight(int, int, int) signal
* Deprecate the KFind::highlight(QString &, int, int) signal
* Enable Unicode support in QRegularExpression where needed

### KWallet Framework

* Un-overload OrgKdeKWalletInterface::walletClosed(int) signal

### KWayland

* Bump required PlasmaWaylandProtocols
* Fix DTD check errors and a typo
* Add the activity management protocol client implementation

### KWindowSystem

* Add MediaPause key to mapping (bug 403636)

### Plasma Framework

* Deprecate AppletScript::description()
* [widgets/arrows] Fix viewBox size and naturalSize in SvgItems
* Add a humanMoment unit to the various Units
* Add PageIndicator
* [pluginloader] Add methods to list containments using KPluginMetaData
* Deprecate PluginLoader::listEngineInfoByCategory
* Mark Plasma Style as internal
* Add notes about classes that will hopefully be dropped in KF6
* [lineedit.svg] Remove empty space around borders and use 3px corner radius
* [PC3 TextField] set on placeholder text
* [PC3 TextField] mirrored -> control.mirrored
* [PC3] Refactor TextField
* Make compositing-off margins in the panel same size of compositing on
* widgets>lineedit.svg: remove double ring
* Change ContrastEffect check to AdaptiveTransparency in A.T. check (bug 434200)
* Revert recent theme changes (bug 434202)
* Port to singleton Units

### QQC2StyleBridge

* Respect highlighted property (bug 384989)
* Fix size of toolbuttons
* [DialogButtonBox] Improve implicit size behavior

### Syntax Highlighting

* LaTeX: allow Math env within another Math env
* java: add assert and var keywords
* cmake.xml: Updates for CMake 3.20
* PHP: add preg_last_error_msg() function and classes that was previously a resource
* Js: numeric separator ; fix Octal ; add new classes
* Try to fix Qt 5.14 based compile
* Make it possible to compile with Qt 6
* Fix #5 and add Path style with alternate value (${xx:-/path/})
* Python: add match and case keywords (Python 3.10)
* Bump rust.xml to version 12
* Do not spellcheck in Rust code
* Markdown: add folding in sections
* Fix BracketMatching color in Breeze Dark theme
* Added more checking and number support
* Added proper escaping support with error handling
* Yet another DetectSpaces at MultiLineText
* Removed Variable context; not needed
* Added Error displaying for Placeables
* Included ##Comments
* Optimisations (DetectIdentifier, etc.)
* Fixed Attribute handling
* Added support for identifiers
* Multiline text is now working
* Added support for fluent language files
* Add `findloc` Fortran function

### ThreadWeaver

* exception.h: fix export header include to work in namespace-prefixed include

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
