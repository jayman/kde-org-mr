---
qtversion: 5.15.2
date: 2023-03-11
layout: framework
libCount: 83
---


### Baloo

* extactor: add KAboutData

### Breeze Icons

* Add draw-number

### Extra CMake Modules

* Load translations for application-specific language also on Win and Mac (bug 464694)
* ECMGenerateExportHeader: fix duplicated addition of deprecation macros code
* Find wayland.xml from wayland-scanner.pc

### KConfig

* Don't include screen connector names in screen position/size data (bug 460260)
* Fix multimonitor window size restoration (bug 460260)
* Sort connector names for multi-screen size/position keys (bug 460260)

### KConfigWidgets

* KConfigDialogManager: Fix logs formatting

### KCoreAddons

* Deprecate KPluginMetaData::initialPreference
* Convert BugReportUrl in desktoptojson (bug 464600)
* exportUrlsToPortal: stop fusing remote urls (bug 457529)
* Show deprecation warning about desktoptojson tool

### KDeclarative

* Guard nullable property access, and bind instead of assigning once
* AbstractKCM: Rewrite padding expressions to be more readable
* Add import aliases, bump internal import versions as needed
* Drop unused QML imports
* [managedconfigmodule] Fix deprecation comments
* [configmodule] Deprecate constructor without metadata
* [configmodule] Deprecate setAboutData

### KDocTools

* Install version header

### KFileMetaData

* Mobi extractor: only extract what is asked (bug 465006)

### KGlobalAccel

* Skip reloading global registry settings instead of asserting

### KHolidays #

* Add holidays for Dominican Rebublic (bug 324683)
* Kf5 add cuba holidays (bug 461282)
* holidayregion variable 'regionCode' shadows outer function

### KI18n

* KI18nLocaleData target: add include dir for version header to interface
* Load translations for application-specific language also on Win and Mac (bug 464694)

### KIconThemes

* Properly mark panel icon group as deprecated
* Deprecate KIconLoader overloads in KIconButton and KIconDialog

### KIdleTime

* wayland: Guard wayland object destructors (bug 465801)

### KIO

* DeleteOrTrashJob: when trashing a file in trash:/ delete it instead (bug 459545)
* Set bug report URL for Windows Shares KCM (bug 464600)
* OpenFileManagerWindowJob: fix opening multiple instances under Wayland [KF5] (bug 463931)
* Add missing URLs in KCMs for reporting bugs (bug 464600)
* kshorturifilter: return directly if cmd is empty
* [kprocessrunner] Use aliased desktop file name for xdg-activation

### Kirigami

* Dialog: Don't let user interact with footer during transitions
* For styling and recoloring, use `down` property instead of `pressed`
* Fix mistyping of Kirigami.Settings.isMobile

### KItemModels

* KDescendantProxyModel: Do not remove indexes from mapping before announcing the removal

### KNewStuff

* DownloadItemsSheet: Fix scrolling (bug 448800)

### KPackage Framework

* Check pluginId contains '/' before using it as package type (bug 449727)

### KPeople

* Install version header

### KRunner

* KF5KRunnerMacros: Add compat code and warning for in KF6 renamed configure_krunner_test macro

### KService

* Fix deprecation ifdef
* Deprecate KService::serviceTypes and KService::hasServiceType
* application: Add X-SnapInstanceName
* Add method to query supported protocols for a service

### KTextEditor

* Improve cstyle performance (bug 466531)
* Improve performance of rendering spaces with dyn wrap disabled (bug 465841)
* documentSaveCopyAs: Use async job api (bug 466571)
* Optimize rendering spaces with dyn wrapping (bug 465841)

### KWindowSystem

* Remove extra semicolon
* Deprecated KWindowSystem::allowExternalProcessWindowActivation
* [kstartupinfo] Deprecate setWindowStartupId
* [kstartupinfo] Deprecate KStartupInfo::currentStartupIdEnv
* [kstartupinfo] Fix API docs for currentStartupIdEnv

### NetworkManagerQt

* settings: fix -Wlto-type-mismatch in NetworkManager::checkVersion decl

### Prison

* KPrisonScanner target: add include dir for version header to interface

### Purpose

* Place Purpose::Menu headers into C++ namespace subdir, w/ compat headers

### QQC2StyleBridge

* ProgressBar: Pause indeterminate animation when invisible
* Added flat combobox without outline unless hovered
* TextField: Fix password-protection code from affecting normal text fields (bug 453828)
* Drawer: Fix RTL by copying sizing code from upstream Default style
* Drawer: Use simpler sizing expressions from upstream Default style
* Don't check for selectByMouse on a non-existent root for TextArea
* use again the palette coming from Kirigami.Theme (bug 465054)
* Only enable TextArea context menu when able to select by mouse

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
