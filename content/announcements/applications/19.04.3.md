---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE Ships Applications 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE Ships KDE Applications 19.04.3
version: 19.04.3
---

{{% i18n_date %}}

Today KDE released the third stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Over sixty recorded bugfixes include improvements to Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular, Umbrello, among others.

Improvements include:

- Konqueror and Kontact no longer crash on exit with QtWebEngine 5.13
- Cutting groups with compositions no longer crashes the Kdenlive video editor
- The Python importer in Umbrello UML designer now handles parameters with default arguments
