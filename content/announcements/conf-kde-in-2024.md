---
description: conf.kde.in 2024 will be hosted on 2-4 February at COEP Technological University, Pune
authors:
  - SPDX-FileCopyrightText: 2023 Shah Bhushan <bshah@kde.org>
  - SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2023 Aniqa Khokhar <aniqa.khokhar@kde.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2023-12-18
title: "conf.kde.in 2024 in Pune, Maharashtra"
draft: false
---

conf.kde.in 2024 will be hosted at COEP Technological University, Pune.

conf.kde.in aims to attract new KDE Community members, as well as seasoned developers. The contents of the conference provide updates on what is going on in the KDE Community and teaches newcomers how to start making meaningful contributions.

This event attracts speakers from all over India. It provides students with an excellent opportunity to interact with established open-source contributors, as well as developers from various industries working on open-source projects in the fields of automotive, embedded, mobile, and more.

conf.kde.in was started in 2011 at the R V College of Engineering, Bangalore by a group of Indian KDE contributors. Since then we have hosted six different editions, each in different universities and venues:

- 2013, KDE Meetup, DA-IICT, Gandhinagar
- conf.kde.in 2014, DA-IICT, Gandhinagar
- conf.kde.in 2015, Amrita University, Kerala
- conf.kde.in 2016, LNMIIT, Jaipur
- conf.kde.in 2017, IIT Guwahati
- conf.kde.in 2020, Maharaja Agrasen Institute of Technology, Delhi

Past events have been successful in attracting Indian students to mentoring programs such as Google Summer of Code (GSoC), Season of KDE, and Google Code-In. These programs have often successfully helped students become lifelong contributors to open-source communities, including KDE.

## Call for papers

We are asking for talk proposals on topics relevant to the KDE community and technology, as well as workshops or presentations targeted towards new contributors.

Interesting topics include (but are not restricted to):

- New people and organisations that are discovering KDE
- Working towards giving people more digital freedom and autonomy with KDE
- New technological developments in KDE
- Guides on how to participate for new, intermediate and expert users
- Showcases of projects by students participating in KDE mentorship programs
- Anything else that might interest the audience

Have you got something interesting to present? Let us know about it! If you know of someone else who should present, encourage them to do so too.

Please submit your papers at the [following link](https://conf.kde.org/event/7/abstracts/)

![](/announcements/cki2024/cki2024.jpeg)

## [COEP Technological University](https://www.coep.org.in)

COEP Technological University is a unitary public university of the Government of Maharashtra, situated on the banks of Mula river, Pune, Maharashtra, India. Established in 1854, it is the 3rd oldest engineering institute in India.

College of Engineering, Pune (COEP), chartered in 1854 is a nationally respected leader in technical education. The institute is distinguished by its commitment to finding solutions to the great predicaments of the day through advanced technology. The institute has a rich history and dedication to the pursuit of excellence. COEP offers a unique learning experience across a spectrum of academic and social experiences. With a firm footing in truth and humanity, the institute gives you an understanding of both technical developments and the ethics that go with it. The curriculum is designed to enhance your academic experience through opportunities like internships, study abroad programmes and research facilities. The hallmark of COEP education is its strong and widespread alumni network, support of the industry and the camaraderie that the institute shares with several foreign universities.

On 23 June 2022, the Government of Maharashtra issued a notification regarding the conversion of the college into an independent technological university.

## [CoFSUG](https://foss.coep.org.in/cofsug)

The COEP's Free Software Users Group (CoFSUG) is a volunteer group at the COEP Technological University that promotes the use and development of free and open source software. CoFSUG runs the FOSS Lab, FOSS Server, COEP Moodle, COEP LDAP Server, and COEP Wiki. The group carries out activities like installation festivals to teach GNU/Linux, workshops on Linux administration, Python, Drupal, and FOSS technologies, promoting software development under the GNU GPLv3 license, and Freedom Fridays to spread FOSS philosophy. It has in the past hosted the Fedora Users and Developers Conference 2011, Android app development workshops, spoken tutorials program from IIT Bombay, and summer coding projects for students on FOSS contributions. CoFSUG organizes FLOSSMEET, a flagship event to create awareness and encourages the use of free and open-source software. Previous editions hosted talks on technologies such as Docker, Kubernetes, open-source licenses, and OSINT, exploring RUST to name a few.

Overall, CoFSUG aims to advance the use of free and open source solutions in academia through hands-on training, student projects, and evangelism about the FOSS philosophy.
