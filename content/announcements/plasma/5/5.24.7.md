---
date: 2022-10-14
changelog: 5.24.6-5.24.7
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Fix appstream runner results appearing before apps/kcms. [Commit.](http://commits.kde.org/plasma-workspace/bfdd84f3cb5f939ceb76dcea679c0a8c7b4d0eca) Fixes bug [#457600](https://bugs.kde.org/457600)
+ [dataengines/weather] Check if jobs failed. [Commit.](http://commits.kde.org/plasma-workspace/c27b15ed78ad0bce35e3aa7dc82d5f16555bbbc8) 
+ [FIX] Unable to remove manually added wallpaper. [Commit.](http://commits.kde.org/plasma-workspace/ac6f1c858e1e6fa1f0a97f52769585d38ea098f3) Fixes bug [#457019](https://bugs.kde.org/457019)
+ plasmashell (Desktop Containment) "Can't remember desktop widget positions after reboot" [major,FIXED] [#413645](https://bugs.kde.org/show_bug.cgi?id=413645)
+ kwin (activities) "Restarting kwin_x11 forgets activities assigned to windows" [normal,FIXED] [#438312](https://bugs.kde.org/show_bug.cgi?id=438312)
