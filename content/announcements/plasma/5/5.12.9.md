---
aliases:
- ../../plasma-5.12.9
changelog: 5.12.8-5.12.9
date: 2019-09-10
layout: plasma
youtube: xha6DJ_v1E4
figure:
  src: /announcements/plasma/5/5.12.0/plasma-5.12.png
  class: text-center mt-4
asBugfix: true
---

- Make the Trashcan applet use the same shadow settings as desktop icons. <a href="https://commits.kde.org/plasma-desktop/83ef545a35abcaab51e1fd02accf89d26a3ba95c">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D21545">D21545</a>
- [Folder View] Improve label crispness. <a href="https://commits.kde.org/plasma-desktop/05e59e1c77c4a83590b0cd906ecfb698ae5ca3b4">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D20407">D20407</a>
- Media controls on the lock screen are now properly translated. <a href="https://commits.kde.org/plasma-workspace/588aa6be2984038f91167db9ab5bd1f62b9f47e5">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D21947">D21947</a>
