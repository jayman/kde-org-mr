---
aliases:
- ../../plasma-5.9.2
changelog: 5.9.1-5.9.2
date: 2017-02-14
layout: plasma
youtube: lm0sqqVcotA
figure:
  src: /announcements/plasma/5/5.9.0/plasma-5.9.png
  class: text-center mt-4
asBugfix: true
---

+ Fix crash in Screen Locker KCM on teardown. <a href="https://commits.kde.org/kscreenlocker/51c4d6c8db8298dbd471fa91de6cb97bf8b4287a">Commit.</a> See bug <a href="https://bugs.kde.org/373628">#373628</a>
+ Fix Discover Appstream Support: make sure we don't show warnings unless it's absolutely necessary. <a href="https://commits.kde.org/discover/d79cb42b7d77dd0c6704a3a40d85d06105de55b4">Commit.</a>
