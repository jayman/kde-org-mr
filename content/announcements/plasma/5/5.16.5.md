---
aliases:
- ../../plasma-5.16.5
changelog: 5.16.4-5.16.5
date: 2019-09-03
layout: plasma
youtube: T-29hJUxoFQ
figure:
  src: /announcements/plasma/5/5.16.0/plasma-5.16.png
  class: text-center mt-4
asBugfix: true
---

- [weather] [envcan] Add additional current condition icon mappings. <a href="https://commits.kde.org/plasma-workspace/3b61c8c689fe2e656e25f927d956a6d8c558b836">Commit.</a>
- [Notifications] Group only same origin and show it in heading. <a href="https://commits.kde.org/plasma-workspace/ee787241bfff581c851777340c1afdc0e46f7812">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D23583">D23583</a>
- Volume Control: Fix speaker test not showing sinks/buttons. <a href="https://commits.kde.org/plasma-pa/097879580833b745bae0dc663df692d573cf6808">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D23620">D23620</a>
