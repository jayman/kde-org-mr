---
title: "Donation received - Thank you!"
---

**Thank you for your donation! You are making KDE possible with your generous contribution!**

### Badges

As a token of our appreciation,  you can download your "I Donated" badge, display it on your social media accounts, blog posts or website, and help us spread the word!

<div class="d-flex justify-content-center my-5">
<figure class="mx-4"><a href="badge_katie.png"><img class="img-fluid" alt="Konqi &amp;quot;I Donated&amp;quot; badge." title="Katie &amp;quot;I Donated&amp;quot; badge." src="badge_katie.png"></a></figure>
<figure class="mx-4"><a href="badge_konqi.png"><img class="img-fluid" alt="Konqi &amp;quot;I Donated&amp;quot; badge." title="Konqi &amp;quot;I Donated&amp;quot; badge." src="badge_konqi.png"></a></figure>
</div>

### Holiday Cards

Print and [make your own Xmas card](Konqi_Kard_01.pdf) with our PDF design:

[![Holidays card.](gift_card_crop.jpg)](Konqi_Kard_01.pdf)

### Or Make Your own

Download our Katie and Konqi Holiday Special designs and make your own digital or physical cards to send to friends and family.

<div class="d-flex justify-content-center my-5">
<a href="katie-christmas.zip">
<figure>
  <img src="katie-christmas-22_transparent_150.png" class="img-fluid" alt="Kristmas Katie." title="Kristmas Katie.">
</figure>
</a>
<a href="konqi-christmas.zip">
<figure><a href="konqi-christmas.zip">
  <img src="konqi-christmas-22_transparent_150.png" class="img-fluid" alt="Kristmas Konqi." title="Kristmas Konqi.">
</a></figure>
</a>
</div>

Click on the design you would like (or grab both!) and you will get two high resolution images, one with and one without a background, so you can send them as is, or set them in the wintery landscape of your choice.





