<ul>
<!-- CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  <ul type="disc">
    <li>
      conectiva:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/Conectiva/conectiva/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- PLD LINUX -->
<li>
  <a href="http://www.pld-linux.org">PLD Linux Distribution</a>:
    <ul type="disc">
        <li><a href="ftp://ftp.alpha.ac.pld-linux.org/dists/ac/PLD/alpha/PLD/RPMS/">Alpha AXP 64 bit</a></li>
	<li><a href="ftp://ftp.athlon.ac.pld-linux.org/dists/ac/PLD/athlon/PLD/RPMS/">Amd Athlon</a></li>
	<li><a href="ftp://ftp.amd64.ac.pld-linux.org/dists/ac/PLD/amd64/PLD/RPMS/">Amd x86-64</a></li>
	<li><a href="ftp://ftp.i386.ac.pld-linux.org/dists/ac/PLD/i386/PLD/RPMS/">Intel i386</a></li>
	<li><a href="ftp://ftp.i586.ac.pld-linux.org/dists/ac/PLD/i586/PLD/RPMS/">Intel i586</a></li>
	<li><a href="ftp://ftp.i686.ac.pld-linux.org/dists/ac/PLD/i686/PLD/RPMS/">Intel i686</a></li>
	<li><a href="ftp://ftp.ppc.ac.pld-linux.org/dists/ac/PLD/ppc/PLD/RPMS/">PowerPC 32bit (601 or newer)</a></li>
	<li><a href="ftp://ftp.sparc.ac.pld-linux.org/dists/ac/PLD/sparc/PLD/RPMS/">Sun Sparc 32bit</a></li>
    </ul>
  <p />
</li>

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/RedHat/Fedora3/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      Fedora 3: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/RedHat/Fedora3/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- KDE-RedHat -->
<li>
 <a href="http://kde-redhat.sourceforge.net/">KDE-RedHat (unofficial) Packages</a>:
 <ul type="disc">
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/kde-redhat/all/">all platforms</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/kde-redhat/redhat/7.3/">Red Hat 7.3</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/kde-redhat/redhat/9/">Red Hat 9</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/kde-redhat/fedora/2/">Fedora Core 2</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/kde-redhat/fedora/3/">Fedora Core 3</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/kde-redhat/redhat/enterprise/3.0/">Red Hat Enterprise 3</a>
 </li>
 </ul>
 <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/Slackware/10.0/README">README</a>)
   :
   <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/Slackware/noarch/">Language packages</a>
    </li>
     <li>
       10.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/contrib/Slackware/10.0/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/README">README</a>)
      :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/ix86/9.2/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/x86_64/9.2/">AMD x86-64</a>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/ix86/9.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/x86_64/9.0/">AMD x86-64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/SuSE/ix86/8.2/">Intel i586</a>
    </li>
  </ul>
  <p />
</li>

<!-- YOPER LINUX -->
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      V2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.2/Yoper/">Intel i686 rpm</a>
    </li>
  </ul>
  <p />
</li>

</ul>
