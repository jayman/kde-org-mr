<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/arts-1.2.2.tar.bz2">arts-1.2.2</a></td>
   <td align="right">956kB</td>
   <td><tt>83ca7e7a33c55de34e12bfc360190795</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdeaccessibility-3.2.2.tar.bz2">kdeaccessibility-3.2.2</a></td>
   <td align="right">1.7MB</td>
   <td><tt>26179a0315123a72949a5981f5d4beb3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdeaddons-3.2.2.tar.bz2">kdeaddons-3.2.2</a></td>
   <td align="right">1.7MB</td>
   <td><tt>294edc0b59a23ae5a9e3a3664e677b2f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdeadmin-3.2.2.tar.bz2">kdeadmin-3.2.2</a></td>
   <td align="right">1.9MB</td>
   <td><tt>4a39ce0d9abdd2dccb4531466ee7f229</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdeartwork-3.2.2.tar.bz2">kdeartwork-3.2.2</a></td>
   <td align="right">16MB</td>
   <td><tt>cc2b1f2116c998387ea4ea998720934a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdebase-3.2.2.tar.bz2">kdebase-3.2.2</a></td>
   <td align="right">16MB</td>
   <td><tt>81a348e01625f77beeb53755cb28ba85</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdebindings-3.2.2.tar.bz2">kdebindings-3.2.2</a></td>
   <td align="right">10MB</td>
   <td><tt>5c00277c009ea97e7ca70c613f5fc87b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdeedu-3.2.2.tar.bz2">kdeedu-3.2.2</a></td>
   <td align="right">21MB</td>
   <td><tt>97174178360396ea50e69097979b8319</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdegames-3.2.2.tar.bz2">kdegames-3.2.2</a></td>
   <td align="right">9.2MB</td>
   <td><tt>714f8a591964c11af5bdb228cee750e6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdegraphics-3.2.2.tar.bz2">kdegraphics-3.2.2</a></td>
   <td align="right">5.9MB</td>
   <td><tt>d74038c154a5ecd924a50b57d4c79c43</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kde-i18n-3.2.2.tar.bz2">kde-i18n-3.2.2</a></td>
   <td align="right">141MB</td>
   <td><tt>3de328fcce5fb1f90b9489e4f36fa33d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdelibs-3.2.2.tar.bz2">kdelibs-3.2.2</a></td>
   <td align="right">12MB</td>
   <td><tt>76c656fb4ec7f1ca073f52fad2b8898b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdemultimedia-3.2.2.tar.bz2">kdemultimedia-3.2.2</a></td>
   <td align="right">5.1MB</td>
   <td><tt>062249563cbf66d77e61b41e5126f806</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdenetwork-3.2.2.tar.bz2">kdenetwork-3.2.2</a></td>
   <td align="right">6.3MB</td>
   <td><tt>405ade1938cd74e3e9643f25814ee81d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdepim-3.2.2.tar.bz2">kdepim-3.2.2</a></td>
   <td align="right">7.8MB</td>
   <td><tt>10249b56cbc4c67dc4093b9f968604b9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdesdk-3.2.2.tar.bz2">kdesdk-3.2.2</a></td>
   <td align="right">4.2MB</td>
   <td><tt>dd3c690444fef79de620c26abfaafd37</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdetoys-3.2.2.tar.bz2">kdetoys-3.2.2</a></td>
   <td align="right">2.7MB</td>
   <td><tt>8db947ef275b7eb255a4448baca419d9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdeutils-3.2.2.tar.bz2">kdeutils-3.2.2</a></td>
   <td align="right">2.9MB</td>
   <td><tt>94eee311b04f91aa083d1f8a8620faca</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/kdevelop-3.0.3.tar.bz2">kdevelop-3.0.3</a></td>
   <td align="right">6.1MB</td>
   <td><tt>c362e32f793f30ba4cedcdc0a914328d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.2/src/quanta-3.2.2.tar.bz2">quanta-3.2.2</a></td>
   <td align="right">3.8MB</td>
   <td><tt>2cdd52e44839e4456c8809a73bf96493</tt></td>
</tr>

</table>
