<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdeaccessibility-3.91.0.tar.bz2">kdeaccessibility-3.91.0</a></td><td align="right">7.4MB</td><td><tt>2cd5c668a0839e41eb26548d5fb150c2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdeaddons-3.91.0.tar.bz2">kdeaddons-3.91.0</a></td><td align="right">757KB</td><td><tt>f262a3be08360e2129eaabe3b2e00616</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdeadmin-3.91.0.tar.bz2">kdeadmin-3.91.0</a></td><td align="right">1.4MB</td><td><tt>d516147dd8bc3449a82ebe862ec7f1a8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdeartwork-3.91.0.tar.bz2">kdeartwork-3.91.0</a></td><td align="right">44MB</td><td><tt>182b2e70c085264df1fbe619de1d1b33</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdebase-3.91.0.tar.bz2">kdebase-3.91.0</a></td><td align="right">21MB</td><td><tt>396ec322d0e8a157a1278f19577ec44b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdeedu-3.91.0.tar.bz2">kdeedu-3.91.0</a></td><td align="right">37MB</td><td><tt>0b604050a2401d240f17b9b88e8b09c8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdegames-3.91.0.tar.bz2">kdegames-3.91.0</a></td><td align="right">21MB</td><td><tt>0fd198f273d2994a507ba142f4742430</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdegraphics-3.91.0.tar.bz2">kdegraphics-3.91.0</a></td><td align="right">2.5MB</td><td><tt>659331919abc209c3e731a904d6a8d0b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdelibs-3.91.0.tar.bz2">kdelibs-3.91.0</a></td><td align="right">52MB</td><td><tt>856b3eadb90b9c9a675412ea1c68e209</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdemultimedia-3.91.0.tar.bz2">kdemultimedia-3.91.0</a></td><td align="right">4.0MB</td><td><tt>b0daa314dd4b0cf90930e6680ee8ae90</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdenetwork-3.91.0.tar.bz2">kdenetwork-3.91.0</a></td><td align="right">6.8MB</td><td><tt>39ce2610aacf22ee66d36cb27ecddb73</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdepim-3.91.0.tar.bz2">kdepim-3.91.0</a></td><td align="right">12MB</td><td><tt>09e65391c7b8366ce9e94fa3c60569d7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdepimlibs-3.91.0.tar.bz2">kdepimlibs-3.91.0</a></td><td align="right">1.5MB</td><td><tt>978712ededae818f2b9225897684b752</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdesdk-3.91.0.tar.bz2">kdesdk-3.91.0</a></td><td align="right">5.2MB</td><td><tt>634c63e003030ed74dce4b777330f57c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdetoys-3.91.0.tar.bz2">kdetoys-3.91.0</a></td><td align="right">2.2MB</td><td><tt>1a950cae9f577a9945495202b801d333</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.91.0/src/kdeutils-3.91.0.tar.bz2">kdeutils-3.91.0</a></td><td align="right">2.1MB</td><td><tt>4facc48e10010efea5f376b72eb4ee0d</tt></td></tr>
</table>
