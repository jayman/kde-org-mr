-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1



KDE Security Advisory: VCF file information reader vulnerability
Original Release Date: 2004-01-14
URL: http://www.kde.org/info/security/advisory-20040114-1.txt

0. References


1. Systems affected:

        All versions of kdepim as distributed with KDE versions 3.1.0
        through 3.1.4 inclusive. 


2. Overview:

        The KDE team has found a buffer overflow in the file 
        information reader of VCF files. 

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2003-0988 to this issue.


3. Impact:

        A carefully crafted .VCF file potentially enables local attackers
        to compromise the privacy of a victim's data or execute
        arbitrary commands with the victim's privileges. 

        By default, file information reading is disabled for remote files.
        However, if previews are enabled for remote files, remote
        attackers may be able to compromise the victim's account.


4. Solution:

        As a workaround, remove the kfile_vcf.desktop file.

        Users of KDE 3.1.x are advised to upgrade to KDE 3.1.5. A patch for
        KDE 3.1.4 is available for users who are unable to upgrade to 
        KDE 3.1.5.


5. Patch:

        A patch for KDE 3.1.4 is available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

        26469366cc393e50ff80d6dca8c74c58  post-3.1.4-kdepim-kfile-plugins.diff


6. Time line and credits:

        15/12/2003 KDE developer Dirk Mueller discovers vulnerability.
        15/12/2003 Patches for the vulnerability are applied to CVS and
                   release preparations for KDE 3.1.5 are started.

        14/01/2004 Public advisory.


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.3 (GNU/Linux)

iD8DBQFABUiwvsXr+iuy1UoRAmf2AKC4JiwDwfDXGME6SZkTF8sVqginEgCgisjC
MLH9/a8f1cFs0iJ2ebdiShM=
=Uoit
-----END PGP SIGNATURE-----
