KDE Project Security Advisory
=============================

Title:          krfb: multiple security issues in libvncserver.
Risk Rating:    Medium
CVE:            CVE-2014-6055 and others
Platforms:      All
Versions:       krfb < 4.14.2
Author:         Martin Sandsmark <martin.sandsmark@kde.org>
Date:           23 September 2014

Overview
========

krfb 4.14 embeds libvncserver which has had several security issues.

For future versions krfb instead depends on a system-installed
libvncserver, but for 4.14 the bundled version needs to be updated.

Impact
======

Several remotely exploitable security issues have been uncovered in
libvncserver, some of which might allow a remote authenticated user
code execution or application crashes.

Workaround
==========

None.

Solution
========

Upgrade krfb to 4.14.2 once released, unbundle libvncserver or apply the following patches:
http://quickgit.kde.org/?p=krfb.git&a=commit&h=d931eafccf3140d740ac61e876dce72a23ade7f4
http://quickgit.kde.org/?p=krfb.git&a=commit&h=126a746dd7bee35840083e9bec7a52935a010346
http://quickgit.kde.org/?p=krfb.git&a=commit&h=2e211579455fd832fb21322482c005b6a85aa1bf
http://quickgit.kde.org/?p=krfb.git&a=commit&h=857c2b411ed806ef806116407612a2d2a40fab9c

Unbundle libvncserver: http://quickgit.kde.org/?p=krfb.git&a=commit&h=1c85dc7d85570c9e3a5fcc57572feb04e57fe6db

Credits
=======

Thanks to Daniele Bianco from oCERT for notifying Tim Jansen about the issues.
Thanks to Tim Jansen for notifying the KDE security team.
Thanks to Nicolas Ruff for fixing libvncserver.
Thanks to Johannes Huber for creating the patch to unbundle libvncserver.
