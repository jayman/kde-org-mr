KDE Project Security Advisory
=============================

Title:          kio: Information Leak when accessing https when using a malicious PAC file
Risk Rating:    Medium
CVE:            CVE-2017-6410
Versions:       kio < 5.32, kdelibs < 4.14.30
Date:           28 February 2017


Overview
========
Using a malicious PAC file, and then using exfiltration methods in the PAC
function FindProxyForURL() enables the attacker to expose full https URLs.

This is a security issue since https URLs may contain sensitive
information in the URL authentication part (user:password@host), and in the
path and the query (e.g. access tokens).

This attack can be carried out remotely (over the LAN) since proxy settings
allow “Detect Proxy Configuration Automatically”.
This setting uses WPAD to retrieve the PAC file, and an attacker who has access
to the victim’s LAN can interfere with the WPAD protocols (DHCP/DNS+HTTP)
and inject his/her own malicious PAC instead of the legitimate one.

Solution
========
Update to kio >= 5.32 and kdelibs >= 4.14.30 (when released)

Or apply the following patches:
    kio: https://commits.kde.org/kio/f9d0cb47cf94e209f6171ac0e8d774e68156a6e4
kdelibs: https://commits.kde.org/kdelibs/1804c2fde7bf4e432c6cf5bb8cce5701c7010559

Credits
=======
Thanks to Safebreach Labs researchers Itzik Kotler, Yonatan Fridburg
and Amit Klein.
