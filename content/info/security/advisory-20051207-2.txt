-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory [UPDATED]: kpdf/xpdf multiple integer overflows
Original Release Date: 2005-12-07
Final Release Date:    2006-01-03
URL: http://www.kde.org/info/security/advisory-20051207-2.txt

0. References
        CAN-2005-3191
        CAN-2005-3192
        CAN-2005-3193
        CVE-2005-3624
        CVE-2005-3625
        CVE-2005-3626
        CVE-2005-3627
        CESA-2005-003


1. Systems affected:

        KDE 3.2.0 up to including KDE 3.5.0
	KOffice 1.3.0 up to including KOffice 1.4.2


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        multiple integer overflow vulnerabilities that allow specially
	crafted pdf files, when opened, to overflow heap allocated
	buffers and execute arbitrary code.

        The patches announced in the KDE security advisory 20051207-1
        were incomplete. This re-issued advisory contains updated patches
        that correct issues with the original upstream patch that were
        found and fixed by Ludwig Nussel, Martin Pitt and Chris Evans.
        The previous patches and advisory have been removed.

3. Impact:

        Remotely supplied pdf files can be used to execute arbitrary
	code on the client machine.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.5.0 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        17ea076e986be5e26a4feea3cd264f7e  post-3.5.0-kdegraphics-CAN-2005-3193.diff

        Patch for KDE 3.4.3 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        e8dde74416769d4589dcca25072aea3e  post-3.4.3-kdegraphics-CAN-2005-3193.diff

        Patch for KDE 3.3.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        fe38b0728e5e2b000eb04e037536f330  post-3.3.2-kdegraphics-CAN-2005-3193.diff

        Patch for KDE 3.2.3 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        51ae90242b7e65ba34c704b38c91cfbe  post-3.2.3-kdegraphics-CAN-2005-3193.diff

        Patch for KOffice 1.3.0 and newer is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        939b41e59cfb5f738e9b6fcfff4faf48  post-1.3-koffice-CAN-2005-3193.diff


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFDur+QvsXr+iuy1UoRAqo7AJ9lJ4Qvf8UB/JmTu+oHg5MitghNlwCdGh6+
Dp6GvaxQhptTyiQipPpEVCY=
=Ov7G
-----END PGP SIGNATURE-----
