<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdeaccessibility-4.5.90.tar.bz2">kdeaccessibility-4.5.90</a></td><td align="right">5.0MB</td><td><tt>e9f729f71f0cecbc0391869a163af35a378a95c3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdeadmin-4.5.90.tar.bz2">kdeadmin-4.5.90</a></td><td align="right">776KB</td><td><tt>c478884db928074d264222abfc8b98fa4a1173fd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdeartwork-4.5.90.tar.bz2">kdeartwork-4.5.90</a></td><td align="right">106MB</td><td><tt>ca08c62e6124b146b40037a7da9a5ad94607ba75</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdebase-4.5.90.tar.bz2">kdebase-4.5.90</a></td><td align="right">2.6MB</td><td><tt>7c21bda15a77c1e1cd2b5e6296046d2ab92bf545</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdebase-runtime-4.5.90.tar.bz2">kdebase-runtime-4.5.90</a></td><td align="right">5.6MB</td><td><tt>e285958332e986abda63436e10189afa7367171f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdebase-workspace-4.5.90.tar.bz2">kdebase-workspace-4.5.90</a></td><td align="right">73MB</td><td><tt>075c5a7eb4e1fc7c4ec86d089f0ba8b524369daa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdebindings-4.5.90.tar.bz2">kdebindings-4.5.90</a></td><td align="right">6.8MB</td><td><tt>721ea1ba951255701f7137bdf2c122fca61c6b55</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdeedu-4.5.90.tar.bz2">kdeedu-4.5.90</a></td><td align="right">69MB</td><td><tt>9d0ce559b8f3bd4ce4646cfb415d3ac78d63e746</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdegames-4.5.90.tar.bz2">kdegames-4.5.90</a></td><td align="right">57MB</td><td><tt>5570801747d390678981a1121ff633a1ed0cc8ad</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdegraphics-4.5.90.tar.bz2">kdegraphics-4.5.90</a></td><td align="right">4.9MB</td><td><tt>ea0b1da956bffd14ea93258b5bd5e938e81503f8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdelibs-4.5.90.tar.bz2">kdelibs-4.5.90</a></td><td align="right">14MB</td><td><tt>6fb6695de4642e5861ace2ca7d08d571ab2c2ac7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdemultimedia-4.5.90.tar.bz2">kdemultimedia-4.5.90</a></td><td align="right">1.6MB</td><td><tt>2c44102f69427365cde2cdffe7ec146a17726a7a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdenetwork-4.5.90.tar.bz2">kdenetwork-4.5.90</a></td><td align="right">8.5MB</td><td><tt>3b744e52b884d46c4237f27f260ad65ff4dd5973</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdepimlibs-4.5.90.tar.bz2">kdepimlibs-4.5.90</a></td><td align="right">3.1MB</td><td><tt>8bf4035cdc456680b25cdf152ab147c30644a853</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdeplasma-addons-4.5.90.tar.bz2">kdeplasma-addons-4.5.90</a></td><td align="right">1.8MB</td><td><tt>59eaefd008b1e181f84852257a29f309f1d44fcb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdesdk-4.5.90.tar.bz2">kdesdk-4.5.90</a></td><td align="right">5.9MB</td><td><tt>fa3833519228f38a0b7a43cdbc2568371d24ca3f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdetoys-4.5.90.tar.bz2">kdetoys-4.5.90</a></td><td align="right">396KB</td><td><tt>d45dab058309e2e0af0eca9863d0c836f3e38a40</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdeutils-4.5.90.tar.bz2">kdeutils-4.5.90</a></td><td align="right">3.6MB</td><td><tt>b241341a0016732c84fcafb3a7a4746ef82faac6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/kdewebdev-4.5.90.tar.bz2">kdewebdev-4.5.90</a></td><td align="right">2.2MB</td><td><tt>05d4859c39d8bb86f4f29edee71dace1bf7ce143</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.5.90/src/oxygen-icons-4.5.90.tar.bz2">oxygen-icons-4.5.90</a></td><td align="right">271MB</td><td><tt>fef6ae77057212b74a4e33ceb67f36185ccbe0df</tt></td></tr>
</table>
