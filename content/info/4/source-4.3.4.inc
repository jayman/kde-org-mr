<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdeaccessibility-4.3.4.tar.bz2">kdeaccessibility-4.3.4</a></td><td align="right">6.3MB</td><td><tt>96aa150b8a5c368b6bb36476ed5fb3e3b3c30547</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdeadmin-4.3.4.tar.bz2">kdeadmin-4.3.4</a></td><td align="right">1.8MB</td><td><tt>8f61aeb2ff9d51712d72cd77dad837c8902b6a5d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdeartwork-4.3.4.tar.bz2">kdeartwork-4.3.4</a></td><td align="right">63MB</td><td><tt>7a095932bad297f22b23765ce6ee17bfd2438bf2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdebase-4.3.4.tar.bz2">kdebase-4.3.4</a></td><td align="right">4.0MB</td><td><tt>7f8637367177c93916dfc2e6a2f6323a5e91c6ce</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdebase-runtime-4.3.4.tar.bz2">kdebase-runtime-4.3.4</a></td><td align="right">7.0MB</td><td><tt>871d23457c4a2676704722e2e3b7194d447904ee</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdebase-workspace-4.3.4.tar.bz2">kdebase-workspace-4.3.4</a></td><td align="right">60MB</td><td><tt>5b43447139d22247d5bc2deee8e3a944447f0bbf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdebindings-4.3.4.tar.bz2">kdebindings-4.3.4</a></td><td align="right">4.7MB</td><td><tt>0c7bf45bd3d81bf6aa1d2b085c17b4ce925c656b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdeedu-4.3.4.tar.bz2">kdeedu-4.3.4</a></td><td align="right">56MB</td><td><tt>ee646d57db11b761d8da33fc03c596c8f531eb9d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdegames-4.3.4.tar.bz2">kdegames-4.3.4</a></td><td align="right">49MB</td><td><tt>33ea8ec476b1557a55c90c071bd462e5ceb7c52b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdegraphics-4.3.4.tar.bz2">kdegraphics-4.3.4</a></td><td align="right">3.5MB</td><td><tt>bb32171b0d562d5698d4f0526be2b069e99f7448</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdelibs-4.3.4.tar.bz2">kdelibs-4.3.4</a></td><td align="right">11MB</td><td><tt>1af2d185c88898b71f36b57f033e3a6d9839ab3d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdelibs-experimental-4.3.4.tar.bz2">kdelibs-experimental-4.3.4</a></td><td align="right">28KB</td><td><tt>43e19c44c3cdc1049c9ee9aca2e2f83a48ffe8bd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdemultimedia-4.3.4.tar.bz2">kdemultimedia-4.3.4</a></td><td align="right">1.6MB</td><td><tt>cf8f2696267ec682ee48fbb0f6171d9e9f7aa4b3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdenetwork-4.3.4.tar.bz2">kdenetwork-4.3.4</a></td><td align="right">7.1MB</td><td><tt>e066c3642d254e1e8213ef511546e960c5658596</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdepim-4.3.4.tar.bz2">kdepim-4.3.4</a></td><td align="right">11MB</td><td><tt>3ddc34c2b0c96c0273431631ccf47a8b9b4289b3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdepimlibs-4.3.4.tar.bz2">kdepimlibs-4.3.4</a></td><td align="right">1.7MB</td><td><tt>4f7cd55999e82f504e3766b6f360ae58b984d5cb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdepim-runtime-4.3.4.tar.bz2">kdepim-runtime-4.3.4</a></td><td align="right">732KB</td><td><tt>bef70088aec4355f5bb8c0375082388eaa3d4b26</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdeplasma-addons-4.3.4.tar.bz2">kdeplasma-addons-4.3.4</a></td><td align="right">1.4MB</td><td><tt>0b7af5db24fd194dd5fbbe1690d4ea62f597b891</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdesdk-4.3.4.tar.bz2">kdesdk-4.3.4</a></td><td align="right">5.4MB</td><td><tt>42b6209908f8baadc0add46153649ebdfa9a977a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdetoys-4.3.4.tar.bz2">kdetoys-4.3.4</a></td><td align="right">1.3MB</td><td><tt>96f6121990590acd3fb5b8cae25ef98b5572733c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdeutils-4.3.4.tar.bz2">kdeutils-4.3.4</a></td><td align="right">2.5MB</td><td><tt>2d5e26055e364af2df7459cdbc3aebdc3a8abdea</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/kdewebdev-4.3.4.tar.bz2">kdewebdev-4.3.4</a></td><td align="right">2.5MB</td><td><tt>697f0557c616497a6f80bbcac472ad6ddd687ae8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.3.4/src/oxygen-icons-4.3.4.tar.bz2">oxygen-icons-4.3.4</a></td><td align="right">120MB</td><td><tt>9905f6b5a47db85c05f7387a75b6ae0e0fdd756e</tt></td></tr>
</table>
