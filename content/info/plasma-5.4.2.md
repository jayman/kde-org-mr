---
version: "5.4.2"
title: "Plasma 5.4.2 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.4_Errata
    name: 5.4 Errata
type: info/plasma5
---

This is a bugfix release of Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the <a
href="/announcements/plasma-5.4.2">Plasma 5.4.2 announcement</a>.
