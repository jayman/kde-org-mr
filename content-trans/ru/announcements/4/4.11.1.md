---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.1.
title: KDE выпускает сентябрьские обновления для среды Plasma, приложений и платформы
  KDE
---
3 сентября 2013 года. Сегодня KDE выпустила обновления для среды, приложений и платформы разработки KDE. Это первые обновления из серии ежемесячных стабилизирующих обновлений для версии 4.11. Как было объявлено при выходе этой версии, среда KDE 4.11 будет поддерживаться и обновляться в течение следующих двух лет. Так как этот выпуск содержит только исправления ошибок, установить его будет приятно и безопасно для всех.

Зафиксировано более 70 исправлений ошибок в менеджере окон KWin, файловом менеджере Dolphin и других приложениях. Пользователи заметят, что среда Plasma стартует быстрее, прокрутка в Dolphin стала плавнее, а ряд приложений и инструментов потребляют меньше памяти. Кроме того, исправлена ошибка с перетаскиванием приложений в переключатель рабочих столов, исправлена подсветка и цветовое выделение в Kate, уничтожено множество мелких «жуков» в игре Kmahjongg. Наконец, улучшена стабильность и как обычно обновлены переводы.

A more complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.1, you can also browse the Git logs.

Скачать исходный код или установочные пакеты вы можете на странице <a href='/info/4/4.11.1'>информации о версии 4.11.1</a>. Если вы хотите узнать больше о версии 4.11 среды, приложений и платформы KDE, прочитайте <a href='/announcements/4.11/'>замечания к выпуску 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Новый агент «Отправить позже» в Kontact'е` width="600px">}}

Всё программное обеспечение KDE, включая библиотеки и приложения, предоставляется бесплатно по лицензиям Open Source. Его можно загрузить в виде исходного кода или в двоичных пакетах с <a href='http://download.kde.org/stable/4.11.1'>download.kde.org</a>, а можно получить в составе <a href='/distributions'>популярных GNU/Linux и UNIX-систем</a>, выпускаемых сегодня.
