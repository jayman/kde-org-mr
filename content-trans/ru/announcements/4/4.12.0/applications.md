---
date: '2013-12-18'
hidden: true
title: KDE Applications 4.12 делают большой шаг вперёд в области управления персональной
  информацией и получают развитие в целом
---
Сообщество KDE с радостью сообщает о выходе очередных важных обновлений KDE Applications, включающих новые функции и исправления ошибок. Этот выпуск отличается значительными улучшениями в пакете KDE PIM, принёсшими оптимизацию производительности и много новых возможностей. В Kate включена интеграция модулей на языке Python, добавлена начальная поддержка макросов Vim. Кроме того, много новшеств появилось в играх и обучающих приложениях.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

В Kate, самом мощном графическом редакторе текста для Linux, получил развитие механизм автодополнения; в результате были реализованы: <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>улучшенный код сопоставления, обработка аббревиатур и частичное сопоставление в классах</a>. Например, новая реализация предложит для введённой строки «QualIdent» дополнение «QualifiedIdentifier». В Kate также появилась <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>начальная поддержка макросов Vim</a>. А больше всего радует, что эти изменения проявятся в KDevelop и других приложениях, построенных на технологии Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Просмотрщик документов Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>теперь учитывает физические поля принтера</a>, воспроизводит аудио и видео в epub, предлагает улучшенный поиск и поддерживает больше преобразований, в том числе заданные в метаданных Exif. В средстве построения UML-диаграмм Umbrello появились <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>разные стили отображения связей</a> и <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>визуальная индикация для документированных элементов</a>.

Страж приватности KGpg теперь показывает больше информации, а хранитель паролей KWalletManager может <a href='http://www.rusu.info/wp/?p=248'>хранить данные в формате GPG</a>. В Konsole появилась новая функция: Ctrl+щелчок открывает адрес, выведенный в консоли. Кроме того, теперь <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>в предупреждении о выходе выводится список процессов</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit adds the ability to <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatically scale content to match desktop resolution</a>. File manager Dolphin introduced a number of performance improvements in sorting and showing files, reducing memory usage and speeding things up. KRDC introduced automatic reconnecting in VNC and KDialog now provides access to 'detailedsorry' and 'detailederror' message boxes for more informative console scripts. Kopete updated its OTR plugin and the Jabber protocol has support for XEP-0264: File Transfer Thumbnails. Besides these features the main focus was on cleaning code up and fixing compile warnings.

### Игры и обучающие программы

Была проведена работа над играми KDE. KReversi <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>переписана на QML и Qt Quick</a>, в результате её интерфейс стал красивее и плавнее. KNetWalk также была <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>портирована на Qt Quick</a>, что дало тот же положительный эффект и возможность задавать произвольную ширину и высоту поля. В Konquest появился новый компьютерный игрок «Becai».

In the Educational applications there have been some major changes. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduces custom lesson support and several new courses</a>; KStars has a new, more accurate <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>alignment module for telescopes</a>, find a <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>youtube video here</a> of the new features. Cantor, which offers an easy and powerful UI for a variety of mathematical backends, now has backends <a href='http://blog.filipesaraiva.info/?p=1171'>for Python2 and Scilab</a>. Read more about the powerful Scilab backend <a href='http://blog.filipesaraiva.info/?p=1159'>here</a>. Marble adds integration with ownCloud (settings are available in Preferences) and adds overlay rendering support. KAlgebra makes it possible to export 3D plots to PDF, giving a great way of sharing your work. Last but not least, many bugs have been fixed in the various KDE Education applications.

### Почта, календарь, личная информация

Много нового было сделано в наборе приложений KDE для работы с почтой, календарями и другой личной информацией.

Starting with email client KMail, there is now <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>AdBlock support</a> (when HTML is enabled) and improved scam detection support by extending shortened URLs. A new Akonadi Agent named FolderArchiveAgent allows users to archive read emails in specific folders and the GUI of the Send Later functionality has been cleaned up. KMail also benefits from improved Sieve filter support. Sieve allows for server-side filtering of emails and you can now <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>create and modify the filters on the servers</a> and <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convert existing KMail filters to server filters</a>. KMail's mbox support <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>has also been improved</a>.

In other applications, several changes make work easier and more enjoyable. A new tool is introduced, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>the ContactThemeEditor</a>, which allows for creating KAddressBook Grantlee themes for displaying contacts. The addressbook can now also show previews before printing data. KNotes has seen some <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>serious work on solving bugs</a>. Blogging tool Blogilo can now deal with translations and there are a wide variety of fixes and improvements all over the KDE PIM applications.

Benefiting all applications, the underlying KDE PIM data cache has <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>seen much work on performance, stability and scalability</a>, fixing <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>support for PostgreSQL with the latest Qt 4.8.5</a>. And there is a new command line tool, the calendarjanitor which can scan all calendar data for buggy incidences and adds a debug dialog for search. Some very special hugs go to Laurent Montel for the work he is doing on KDE PIM features!

#### Установка KDE Applications

Программное обеспечение KDE, включая все приложения и библиотеки, распространяется бесплатно по лицензиям Open Source. Программные продукты KDE работают на разных аппаратных платформах и процессорах, включая ARM и x86, в разных операционных системах и окружениях рабочего стола с любыми оконными менеджерами. Помимо пакетов для Linux и других операционных систем на базе UNIX, вы можете найти версии KDE-приложений для Microsoft Windows на сайте <a href='http://windows.kde.org'>KDE на Windows</a> и версии для Apple Mac OS X на сайте <a href='http://mac.kde.org/'>KDE на Mac</a>. Кроме этого, в Интернете можно найти экспериментальные версии приложений KDE для различных мобильных платформ, включая MeeGo, MS Windows Mobile и Symbian, но в данный момент они не поддерживаются. <a href='http://plasma-active.org'>Plasma Active</a> — новая платформа, разрабатываемая для широкого спектра мобильных устройств, таких как планшетные компьютеры и телефоны.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Пакеты

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Местонахождение пакетов

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Community Wiki</a>.

The complete source code for 4.12.0 may be <a href='/info/4/4.12.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.12.0 are available from the <a href='/info/4/4.12.0#binary'>4.12.0 Info Page</a>.

#### Системные требования

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
