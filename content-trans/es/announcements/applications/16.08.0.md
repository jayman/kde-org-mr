---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE lanza las Aplicaciones de KDE 16.08.0
layout: application
title: KDE lanza las Aplicaciones de KDE 16.08.0
version: 16.08.0
---
Hoy, 18 de agosto de 2016, KDE presenta las Aplicaciones 16.08 con un impresionante conjunto de actualizaciones relacionadas con una mayor facilidad de acceso, la introducción de funcionalidades de gran utilidad y la solución de problemas menores. Todo ello contribuye a que ahora las Aplicaciones de KDE estén un paso más cerca de ofrecer la configuración perfecta para su dispositivo.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> y KDiskFree se han migrado a KDE Frameworks 5 y esperamos con ilusión sus comentarios sobre las funcionalidades más recientes introducidas en esta versión.

En el continuo esfuerzo de dividir las bibliotecas de la suite Kontact para hacerlas más fáciles de usar por terceras partes, el «tarball» de kdepimlibs se ha dividido en akonadi-contacts, akonadi-mime y akonadi-notes.

Hemos discontinuado los siguientes paquete: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu y mplayerthumbs. Esto nos ayudará a centrarnos en el resto del código.

### Manteniendo el Kontacto

<a href='https://userbase.kde.org/Kontact'>La suite Kontact</a> ha tenido la típica ronda de limpieza, corrección de errores y optimizaciones en este lanzamiento. Cabe destacar el uso de QtWebEngine en diversos componentes, lo que permite usar un motor de visualización HTML más moderno. También hemos mejorado el uso de VCard4, además de añadir nuevos complementos que pueden generar avisos si se cumplen algunas condiciones al enviar un correo electrónico, como verificar que desea permitir el envío de mensajes con cierta identidad, comprobar si está enviando mensajes en texto sin formato, etc.

### Nueva versión de Marble

<a href='https://marble.kde.org/'>Marble</a> 2.0 forma parte de las Aplicaciones de KDE 16.08 e incluye más de 450 cambios en el código fuente que incluyen mejoras en la navegación, visualización y renderización vectorial experimental de datos de OpenStreetMap.

### Más archivado

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> puede extraer ahora archivos AppImage y .xar, así como comprobar la integridad de archivos comprimidos zip, 7z y rar. También puede añadir y editar comentarios en los archivos comprimidos rar.

### Mejoras de la terminal

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> contiene mejoras relacionadas con la visualización de tipos de letra y con la accesibilidad.

### ¡Y aún más!

<a href='https://kate-editor.org'>Kate</a> contiene pestañas movibles. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Más información...</a>

<a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a> contiene mapas de provincias y regiones de Burkina Faso.

### Agresivo control de plagas

Se han resuelto más de 120 errores en aplicaciones, que incluyen la suite Kontact, Ark, Cantor, Dolphin, KCalc y Kdenlive, entre otras.

### Registro de cambios completo
