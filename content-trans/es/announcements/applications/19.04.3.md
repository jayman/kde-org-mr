---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE lanza las Aplicaciones 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE lanza las Aplicaciones de KDE 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

KDE ha lanzado hoy la tercera actualización de estabilización para las <a href='../19.04.0'>Aplicaciones de KDE 19.04</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 60 correcciones de errores registradas se incluyen mejoras en Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Konqueror y Kontact ya no fallan al salir con QtWebEngine 5.13.
- El corte de grupos con composiciones ya no hace que falle el editor de vídeo Kdenlive.
- El importador de Python del diseñador UML Umbrello gestiona ahora parámetros con argumentos predeterminados.
