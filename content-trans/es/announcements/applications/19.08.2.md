---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE lanza las Aplicaciones 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE lanza las Aplicaciones 19.08.2
version: 19.08.2
---
{{% i18n_date %}}

KDE ha lanzado hoy la segunda actualización de estabilización para las <a href='../19.08.0'>Aplicaciones 19.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de veinte correcciones de errores registradas, se incluyen mejoras en Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize y Spectacle, entre otras aplicaciones.

Las mejoras incluyen:

- Se ha mejorado el uso de pantallas de alta densidad en Konsole y en otras aplicaciones.
- El cambio entre distintas búsquedas en Dolphin actualiza ahora los parámetros de búsqueda de forma correcta.
- KMail vuelve a poder guardar mensajes de forma directa en carpetas remotas.
