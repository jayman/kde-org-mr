---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE lanza las Aplicaciones 19.08.1.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE lanza las Aplicaciones 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

KDE ha lanzado hoy la primera actualización de estabilización para las <a href='../19.08.0'>Aplicaciones 19.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kontact, Dolphin, Kdenlive, Konsole y Step, entre otras aplicaciones.

Las mejoras incluyen:

- Se han corregido varias regresiones en la gestión de pestañas de Konsole.
- Dolphin vuelve a iniciarse de forma correcta en el modo de vista dividida.
- El borrado de un cuerpo blando en el simulador de física Step ya no bloquea el programa.
