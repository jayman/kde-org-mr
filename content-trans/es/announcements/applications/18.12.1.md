---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE lanza las Aplicaciones 18.12.1.
layout: application
major_version: '18.12'
title: KDE lanza las Aplicaciones de KDE 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

KDE ha lanzado hoy la primera actualización de estabilización para las <a href='../18.12.0'>Aplicaciones 18.12</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole y Okular, entre otras aplicaciones.

Las mejoras incluyen:

- Akregator funciona ahora con WebEngine de Qt 5.11 o posterior.
- Se ha corregido la ordenación de columnas en el reproductor multimedia JuK.
- Konsole vuelve a mostrar correctamente los caracteres de dibujo de cuadros.
