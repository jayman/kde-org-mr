---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE lanza las Aplicaciones de KDE 17.04.2
layout: application
title: KDE lanza las Aplicaciones de KDE 17.04.2
version: 17.04.2
---
Hoy, 8 de junio de 2017, KDE ha lanzado la segunda actualización de estabilización para las <a href='../17.04.0'>Aplicaciones 17.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 15 correcciones de errores registradas, se incluyen mejoras en kdepim, ark, dolphin, gwenview y kdenlive, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.33 que contará con asistencia a largo plazo.
