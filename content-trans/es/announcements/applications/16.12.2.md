---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE lanza las Aplicaciones de KDE 16.12.2
layout: application
title: KDE lanza las Aplicaciones de KDE 16.12.2
version: 16.12.2
---
Hoy, 9 de febrero de 2017, KDE ha lanzado la segunda actualización de estabilización para las <a href='../16.12.0'>Aplicaciones de KDE 16.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kdepim, Dolphin, Kate, Kdenlive, KTouch y Okular, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.29 que contará con asistencia a largo plazo.
