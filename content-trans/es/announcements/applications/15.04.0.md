---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: KDE lanza las Aplicaciones 15.04.
layout: application
title: KDE lanza las Aplicaciones de KDE 15.04.0
version: 15.04.0
---
Hoy, 15 de abril de 2015, KDE ha lanzado las Aplicaciones de KDE 15.04. Con esta versión se ha pasado un total de 72 aplicaciones a <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. El equipo se está esforzando en proporcionar a los usuarios un escritorio y unas aplicaciones de la mejor calidad, así que contamos con ustedes para que nos envíen sus comentarios.

Con esta versión se amplía la lista de aplicaciones basadas en KDE Frameworks 5, entre las que se incluyen <a href='https://www.kde.org/applications/education/khangman/'>KHangMan</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> y <a href='https://games.kde.org/'>algunos juegos de KDE</a>.

Kdenlive es uno de las aplicaciones de edición de vídeo no lineal disponibles actualmente. Recientemente ha salido de su periodo de <a href='https://community.kde.org/Incubator'>incubación</a> para convertirse en un proyecto oficial de KDE y se ha pasado a KDE Frameworks 5. El equipo que está detrás de esta obra maestra decidió que Kdenlive se lanzaría junto con las aplicaciones de KDE. Algunas de las nuevas funcionalidades son el guardado automático de proyectos nuevos y la estabilización de los clips fijos.

KDE Telepathy es la herramienta para mensajería instantánea. Se ha portado a KDE Frameworks 5 y a Qt5 y es un nuevo miembro de los lanzamientos de las Aplicaciones de KDE. Está prácticamente terminado, aunque todavía carece de una interfaz de usuario para llamadas de audio y vídeo.

Cuando es posible, KDE usa tecnologías existentes, como se ha hecho con el nuevo KAccounts, que también se usa en SailfishOS y en Unity, de Canonical. Entre las Aplicaciones de KDE, solo se usa por ahora en KDE Telepathy, aunque en el futuro se usará ampliamente por aplicaciones como Kontact y Akonadi.

En el <a href='https://edu.kde.org/'>módulo educativo de KDE</a>, Cantor incorpora varias funciones nuevas relacionadas con Python: un nuevo motor para Python 3 y nuevas categorías para «Obtener novedades». A Rocs se le ha dado la vuelta por completo: el núcleo sobre la teoría de grafos se ha reescrito, se ha eliminado la separación de estructuras de datos y se ha introducido un documento de grafos más general como entidad central de grafos; también se ha revisado la API para crear scripts de algoritmos de grafos, que ahora proporciona una única API unificada. KHangMan se ha adaptado a QtQuick y ha recibido una capa de pintura fresca durante el proceso. Finalmente, Kanagram posee un nuevo modo para dos jugadores, sus letras son ahora botones que se pueden pulsar y también se pueden escribir con el teclado como antes.

Aparte de las típicas correcciones de errores, <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> contiene esta vez algunas mejoras de usabilidad y de estabilidad. Además, la función de búsqueda se puede limitar ahora por categorías: clase, interfaz, paquete, operaciones o atributos.
