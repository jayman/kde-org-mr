---
aliases:
- ../../kde-frameworks-5.60.0
date: 2019-07-13
layout: framework
libCount: 70
---
### General

- Qt &gt;= 5.11 is now required, now that Qt 5.13 has been released.

### Baloo

- [QueryTest] Test if independent phrases are really independent
- [TermGenerator] Insert an empty position between independent terms
- [QueryTest] Restructure tests to allow easier extension
- [TermGenerator] Leave single term phrases out of the PositionDB
- [TermGenerator] Do Term truncation prior to UTF-8 conversion
- [PostingIterator] Move positions() method to VectorPositionInfoIterator
- [TermGenerator] Use UTF-8 ByteArray for termList
- [WriteTransactionTest] Clear mixup of QString and QByteArray
- [experimental/BalooDB] Fix trivial 0 / nullptr warning
- [PositionDbTest] Fix trivial memleak in test
- [PendingFileQueueTest] Verify create + delete do not emit extra events
- [PendingFileQueueTest] Verify delete + create actually works
- [PendingFileQueue] Avoid delete + create / create + delete race
- [PendingFileQueueTest] Use synthetic timer events to speedup test
- [XAttrIndexer] Update DocumentTime when XAttrs are updated
- [PendingFileQueueTest] Shorten timeouts, verify tracking time
- [PendingFileQueue] Use more accurate calculation of remaining time
- [ModifiedFileIndexer] Use correct mimetype for folders, delay until needed
- [NewFileIndexer] Omit symlinks from the index
- [ModifiedFileIndexer] Avoid shadowing XAttr changes by content changes
- [NewFileIndexer] Use correct mimetype for folders, check excludeFolders
- [UnindexedFileIndexer] Pick up comment, tags and rating changes
- [UnindexedFileIndexer] Omitir las comprobaciones de «filetime» en los archivos nuevos.
- [DocumentUrlDB] Avoid manipulation of the whole tree on trivial rename
- [DocumentUrlDB] Catch invalid URLs early
- [DocumentUrlDB] Remove unused 'rename' method
- [balooctl] Streamline indexer control commands
- [Transaction] Replace template for functor with std::function
- [FirstRunIndexer] Use correct mimetype for folders
- Move invariant IndexingLevel out of the loop
- [BasicIndexingJob] Skip lookup of baloo document type for directories
- [FileIndexScheduler] Ensure indexer is not run in suspended state
- [PowerStateMonitor] Be conservative when determining power state
- [FileIndexScheduler] Stop the indexer when quit() is called via DBus
- Avoid container detach in a few places
- Do not try to append to QLatin1String
- Disable valgrind detection when compiling with MSVC
- [FilteredDirIterator] Combine all suffixes into one large RegExp
- [FilteredDirIterator] Avoid RegExp overhead for exact matches
- [UnindexedFileIterator] Retrasar la determinación del tipo MIME hasta que sea necesario.
- [UnindexedFileIndexer] No intentar añadir archivos no existentes al índice.
- Detect valgrind, avoid database removal when using valgrind
- [UnindexedFileIndexer] Loop optimizations (avoid detach, invariants)
- Delay running UnindexedFileIndexer and IndexCleaner
- [FileIndexScheduler] Add new state for Idle on battery
- [FileIndexScheduler] Postpone housekeeping tasks while on battery
- [FileIndexScheduler] Avoid emitting state changes multiple times
- [balooctl] Clarificar y extender la salida de estado.

### BluezQt

- Add MediaTransport API
- Add LE Advertising and GATT APIs

### Iconos Brisa

- Add id="current-color-scheme" to collapse-all icons (bug 409546)
- Se han añadido iconos de cuota de disco (error 389311).
- Symlink install to edit-download
- Change joystick settings icon to game controller (bug 406679)
- Add edit-select-text, make 16px draw-text like 22px
- Update KBruch icon
- Se han añadido los iconos «help-donate-[moneda]».
- Make Breeze Dark use same Kolourpaint icon as Breeze
- Se han añadido iconos de notificaciones de 22 píxeles.

### KActivitiesStats

- Fix a crash in KactivityTestApp when Result has strings with non-ASCII

### KArchive

- Do not crash if the inner file wants to be bigger than QByteArray max size

### KCoreAddons

- KPluginMetaData: usar «Q_DECLARE_METATYPE».

### KDeclarative

- [GridDelegate] Fix gaps in corners of thumbnailArea highlight
- Deshacerse de «blockSignals».
- [KCM GridDelegate] Silence warning
- [KCM GridDelegate] Take into account implicitCellHeight for inner delegate height
- Corregir el icono de «GridDelegate».
- Fix fragile comparison to i18n("None") and describe behavior in docs (bug 407999)

### KDE WebKit

- Downgrade KDEWebKit from Tier 3 to Porting Aids

### KDocTools

- Actualización user.entities para pt-BR.

### KFileMetaData

- Fix extracting of some properties to match what was written (bug 408532)
- Use debugging category in taglib extractor/writer
- Formatear el valor de exposición de las fotos (error 343273).
- Corregir el nombre de la propiedad.
- Eliminar el prefijo «photo» de cada nombre de propiedad exif (error 343273).
- Cambiar el nombre de las propiedades «ImageMake» e «ImageModel» (fallo 343273).
- [UserMetaData] Add method to query which attributes are set
- Formatear la longitud focal como milímetros.
- Format photo exposure time as rational when applicable (bug 343273)
- Activar «usermetadatawritertest» para todos los tipos de UNIX, no solo para Linux.
- Formatear los valores de apertura como números F (error 343273).

### Complementos KDE GUI

- KModifierKeyInfo: estamos compartiendo la implementación interna.
- Remove double look-ups
- Move to runtime the decision to use x11 or not

### KHolidays

- Update UK Early May bank holiday for 2020 (bug 409189)
- Fix ISO code for Hesse / Germany

### KImageFormats

- QImage::byteCount -&gt; QImage::sizeInByes

### KIO

- Fix KFileItemTest::testIconNameForUrl test to reflect different icon name
- Fix i18n number-of-arguments error in knewfilemenu warning message
- [ftp] Fix wrong access time in Ftp::ftpCopyGet() (bug 374420)
- [CopyJob] Batch reporting processed amount
- [CopyJob] Report results after finishing copy (bug 407656)
- Move redundant logic in KIO::iconNameForUrl() into KFileItem::iconName() (bug 356045)
- Instalar «KFileCustomDialog».
- [Places panel] Don't show Root by default
- Downgrade "Could not change permissions" dialog box to a qWarning
- O_PATH is only available on linux. To prevent the compiler from throwing an error
- Show feedback inline when creating new files or folders
- Auth Support: Drop privileges if target is not owned by root
- [copyjob] Only set modification time if the kio-slave provided it (bug 374420)
- Cancel privilege operation for read-only target with the current user as owner
- Se ha añadido «KProtocolInfo::defaultMimetype».
- Always save view settings when switching from one view mode to another
- Restore exclusive group for sorting menu items
- Dolphin-style view modes in the file dialog (bug 86838)
- kio_ftp: se ha mejorado la gestión de errores cuando falla la copia a FTP.
- kioexec: change the scary debug messages for delayed deletion

### Kirigami

- [ActionTextField] Make action glow on press
- Permitir el uso del modo de texto y de la posición.
- mouseover effect for breadcrumb on desktop
- enforce a minimum height of 2 gridunits
- Set SwipeListItem implicitHeight to be the maximum of content and actions
- Ocultar ayuda emergente al pulsar «PrivateActionToolButton».
- Remove accidentally slipped back traces of cmake code for Plasma style
- ColumnView::itemAt
- force breeze-internal if no theme is specified
- correct navigation on left pinned page
- keep track of the space covered by pinned pages
- show a separator when in left sidebar mode
- in single column mode, pin has no effect
- first semi working prototype of pinning

### KConfigWidgets

- [KUiServerJobTracker] Contemplar el cambio de propietario.

### KNewStuff

- [kmoretools] Añadir iconos para las acciones de descargar e instalar.

### KNotification

- No buscar «phonon» en Android.

### KParts

- Add profile support interface for TerminalInterface

### KRunner

- No retrasar indefinidamente la emisión de «matchesChanged».

### KService

- Añadir «X-Flatpak-RenamedFrom» como clave reconocida.

### KTextEditor

- Corregir el centrado de la línea a la que ir (error 408418).
- Fix bookmark icon display on icon border with low dpi
- Fix action "Show Icon Border" to toggle border again
- Fix empty pages in print preview and lines printed twice (bug 348598)
- Eliminar cabecera que ya no se usa.
- Se ha corregido la velocidad de desplazamiento hacia abajo automático (error 408874).
- Add default variables for variables interface
- Make automatic spellcheck work after reloading a document (bug 408291)
- raise default line length limit to 10000
- WIP:Disable highlighting after 512 characters on a line
- KateModeMenuList: mover a «QListView».

### KWayland

- Incluir una descripción.
- Proof of concept of a wayland protocol to allow the keystate dataengine to work

### KWidgetsAddons

- KPasswordLineEdit now correctly inherits its QLineEdit's focusPolicy (bug 398275)
- Sustituir el botón «Detalles» con «KCollapsibleGroupBox».

### Framework de Plasma

- [Svg] Fix porting error from QRegExp::exactMatch
- ContainmentAction: Fix loading from KPlugin
- [TabBar] Eliminar márgenes exteriores.
- Applet, DataEngine and containment listing methods inPlasma::PluginLoader no longer filters the plugins with X-KDE-ParentAppprovided when empty string is passed
- Pinchar en el calendario vuelve a funcionar.
- Se han añadido iconos de cuota de disco (error 403506).
- Make Plasma::Svg::elementRect a bit leaner
- Automatically set version of desktopthemes packages to KF5_VERSION
- Don't notify about changing to the same state it was at
- Fix the alignment of the label of the toolbutton
- [PlasmaComponents3] Centrar el texto del botón verticalmente también.

### Purpose

- Cambiar el tamaño inicial del diálogo de configuración.
- Se han mejorado los iconos y el texto de los botones del diálogo de tareas.
- Se ha corregido la traducción de «actiondisplay».
- Don't show error message if sharing is cancelled by the user
- Se ha corregido una advertencia al leer metadatos de los complementos.
- Las páginas de configuración se han rediseñado.
- ECMPackageConfigHelpers -&gt; CMakePackageConfigHelpers
- phabricator: Fix fallthrough in switch

### QQC2StyleBridge

- Show shortcut in menu item when specified (bug 405541)
- Añadir MenuSeparator.
- Fix ToolButton remaining in a pressed state after press
- [ToolButton] Pass custom icon size to StyleItem
- Respetar la política de visibilidad (error 407014).

### Solid

- [Fstab] Select appropriate icon for home or root directory
- [Fstab] Show mounted "overlay" filesystems
- [UDev Backend] Narrow device queried for

### Resaltado de sintaxis

- Fortran: se ha cambiado la licencia a MIT.
- Se ha mejorado el resaltado de sintaxis de formato fijo de Fortran.
- Fortran: implementar formatos libres y fijos.
- Fix CMake COMMAND nested paren highlighting
- Add more keywords and also support rr in gdb highlighter
- Detectar líneas de comentarios anticipadamente en el resaltado de sintaxis de GDB.
- AppArmor: actualizar sintaxis.
- Julia: update syntax and add constants keywords (bug 403901)
- CMake: Highlight the standard CMake environment variables
- Se ha añadido la definición de sintaxis para la compilación «ninja».
- CMake: Permitir el uso de funcionalidades de 3.15.
- Jam: diversas mejora y correcciones.
- Lua: update for Lua54 and end of function as Keyword rather than Control
- C++: Actualizar a C++20.
- debchangelog: Añadir a Eoan Ermine.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
