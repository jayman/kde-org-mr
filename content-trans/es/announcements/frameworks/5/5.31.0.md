---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### General

Muchos módulos tienen ahora vínculos con Python.

### Attica

- permitir el uso de display_name en las categorías

### Iconos Brisa

- demasiados cambios en los iconos para listarlos aquí

### Módulos CMake adicionales

- Activar -Wsuggest-override para g++ &gt;= 5.0.0
- Pasar -fno-operator-names cuando esté permitido
- ecm_add_app_icon: ignorar archivos SVG de forma silenciosa cuando no estén permitidos
- Asociaciones: muchas correcciones y mejoras

### Integración con Frameworks

- Permitir el uso de algunas de las preguntas de KNSCore que usan notificaciones

### KArchive

- Se ha corregido KCompressionDevice (búsqueda) para que funcione con Qt &gt;= 5.7

### KAuth

- Se han actualizado muchos ejemplos, y se han descartado los desfasados

### KConfig

- Se ha corregido el enlazado en Windows: no enlazar «kentrymaptest» con «KConfigCore»

### KConfigWidgets

- No hacer nada en ShowMenubarActionFilter::updateAction si no hay barras de menú

### KCoreAddons

- Se ha corregido el fallo 363427: caracteres inseguros se analizan de forma incorrecta como parate de un URL (fallo 363427).
- kformat: se ha hecho posible que se traduzcan bien las fechas relativas (fallo 335106)
- KAboutData: Documentar que la dirección de correo para errores también puede ser un URL.

### KDeclarative

- [Diálogo de iconos] definir correctamente el grupo de iconos
- [QuickViewSharedEngine] usar «setSize» en lugar de «setWidth/setHeight»

### Soporte de KDELibs 4

- Sincronizar el «KDE4Defaults.cmake» de kdelibs
- Se ha corregido la comprobación HAVE_TRUNC de cmake

### KEmoticons

- KEmoticons: usar DBus para notificar a los procesos en ejecución de los cambios realizados en KCM
- KEmoticons: importantes mejoras de rendimiento

### KIconThemes

- KIconEngine: centrar icono en el rectángulo solicitado

### KIO

- Añadir KUrlRequester::setMimeTypeFilters
- Se ha corregido el análisis del listado de directorios de un servidor ftp (fallo 375610)
- Preservar el grupo y el propietario durante la copia de archivos (fallo 103331)
- KRun: desaconsejar el uso de «runUrl()» en favor de «runUrl()» con «RunFlags»
- kssl: asegurar que se ha creado el directorio de certificados del usuario antes de usarlo (fallo 342958)

### KItemViews

- Aplicar el filtro al proxy inmediatamente

### KNewStuff

- Hacer posible que se puedan adoptar recursos, principalmente para las preferencias globales del sistema.
- No fallar al mover al directorio temporal durante la instalación.
- Desaconsejar el uso de la clase «security»
- No producir un bloqueo al ejecutar la orden de postinstalación (error 375287).
- [KNS] tener en cuenta el tipo de distribución
- No preguntar si estamos obteniendo el archivo de «/tmp».

### KNotification

- Se han vuelto a añadir notificaciones de registro a los archivos (fallo 363138)
- Marcar las notificaciones no persistentes como efímeras.
- Permitir el uso de «acciones predeterminadas»

### Framework KPackage

- No generar «appdata» si está marcado como «NoDisplay»
- Corregir el listado cuando la ruta solicitada es absoluta
- Se ha corregido el manejo de archivos comprimidos con una carpeta en su interior (fallo 374782)

### KTextEditor

- Se ha corregido la representación del minimapa en los entornos HiDPI

### KWidgetsAddons

- Se han añadido métodos para ocultar la acción de revelar la contraseña
- KToolTipWidget: No apropiarse del widget de contenido.
- KToolTipWidget: Ocultar inmediatamente si se destruye el contenido.
- Corregir la redefinición del foco en «KCollapsibleGroupBox»
- Se ha corregido la advertencia cuando se destruye un KPixmapSequenceWidget
- Instalar también encabezados de reenvío de «CamelCase» de las clases para los encabezados multiclase.
- KFontRequester: encontrar la coincidencia más próxima para un tipo de letra ausente (fallo 286260)

### KWindowSystem

- Permitir que la tecla Tabulador se pueda modificar con Mayúsculas (error 368581).

### KXMLGUI

- Notificador de fallos: permitir un URL (no solo una dirección de correo electrónico) para informes personalizados
- Omitir los accesos rápidos vacíos en la comprobación de ambigüedad.

### Framework de Plasma

- [Interfaz de contenedores] No se necesita «values()», ya que «contains()» busca claves.
- Diálogo: Ocultar cuando el foco cambia a «ConfigView» con «hideOnWindowDeactivate».
- [Menú de PlasmaComponents] Añadir la propiedad «maximumWidth».
- Falta un icono al conectarse a «openvpn» usando una red bluetooth (error 366165).
- Asegurarse de que se muestra el «ListItem» activado al situar el cursor sobre él.
- Hacer que todas las alturas del encabezado del calendario sean uniformes (error 375318).
- Corregir el estilo de color en el icono de red de Plasma (error 373172).
- Definir «wrapMode» a «Text.WrapAnywhere» (error 375141).
- Actualizar el icono de kalarm (fallo 362631)
- Reenviar correctamente el estado de las miniaplicaciones al contenedor (error 372062).
- Usar «KPlugin» para cargar complementos del calendario.
- Usar el color de resaltado para el texto seleccionado (error 374140).
- [Elemento de icono] Redondear el tamaño en el que queremos cargar un mapa de píxeles.
- La propiedad «portrait» no es relevante cuando no existe texto (error 374815).
- Se han corregido las propiedades «renderType» de varios componentes.

### Solid

- Se ha solucionado provisionalmente el error de la obtención de la propiedad «DBus» (error 345871).
- Tratar la ausencia de frase de contraseña como un error «Solid::UserCanceled».

### Sonnet

- Se ha añadido el archivo de datos de trigramas del griego.
- Se ha corregido un bloqueo en la generación de trigramas y se ha expuesto la constante «MAXGRAMS» en el archivo de cabecera.
- Buscar el «libhunspell.so» no versionado; debería estar más preparado para el futuro.

### Resaltado de sintaxis

- Resaltado sintáctico de C++: Actualizar a Qt 5.8.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
