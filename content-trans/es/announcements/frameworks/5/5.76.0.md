---
aliases:
- ../../kde-frameworks-5.76.0
date: 2020-11-07
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

* Dividir las palabras en CJK por los caracteres de puntuación, optimizar el código.
* Se ha refactorizado el código para preparar la integración de ICU.

### Iconos Brisa

* Añadir el icono de 48 píxeles «dialog-warning».
* Cambiar el estilo de «media-repeat-single» para que use el número 1.
* Se han añadido más archivos ignorados de git.
* Comprobar si existe el archivo antes de eliminarlo.
* Eliminar siempre en primer lugar el archivo de destino antes de generar enlaces simbólicos.
* Añadir iconos de modo color para Okular.
* Se han añadido iconos para «task-complete» (fallo 397996).
* Se ha añadido el icono «network-limited».
* Copiar el enlace simbólico de «kup» de 32 píxeles en «apps/48» para corregir el fallo de la comprobación de escalabilidad.
* Se han añadido iconos para «meeting-organizer» (fallo 397996).
* Se ha añadido el icono «fingerprint».
* Se han añadido los iconos «task-recurring» y «appointment-recurring» (fallo 392533).
* Desactivar temporalmente la la generación de iconos en Windows.
* Enlace simbólico de «kup.svg» a «preferences-system-backup.svg».

### Módulos CMake adicionales

* Hacer que «androiddeployqt» encuentre las bibliotecas y los complementos QML sin instalación.
* find-modules/FindReuseTool.cmake: Corregir la búsqueda de la herramienta de reutilización.
* Mejorar las opciones de formateo predeterminado.
* Incluir opción para usar LLVM para los usuarios con Qt < 5.14.
* Añadir la versión mínima que faltaba para el parámetro de RENAME.
* Documentar cuándo se ha añadido «FindGradle».
* Se ha añadido «FindGradle» de «KNotification».

### KAuth

* Convertir el nombre del motor a mayúsculas antes.
* Añadir función auxiliar para obtener el UID del llamante.

### KCalendarCore

* Elevar la ambigüedad en «ICalFormat::toString()» durante las pruebas.
* Añadir la serialización de la propiedad COLOR de RFC7986.
* Hacer que «MemoryCalendar::rawEvents(QDate, QDate)» funcione con límites abiertos.

### KCMUtils

* Portar de «QStandardPaths::DataLocation» a «QStandardPaths::AppDataLocation».
* Añadir soporte de espacios de nombres a la macro «KCModuleData» de CMake.
* Marcar como obsoleta «KSettings::PluginPage».
* Eliminar referencia a cabecera no definida.
* Mover «KCMUtilsGenerateModuleData» al lugar correcto.
* Empujar todas las subpáginas precreadas de los KCM.
* Se ha añadido una función de CMake para generar datos de módulo básicos.
* Mejorar la legibilidad de QML en línea en «kcmoduleqml.cpp».
* Altura de cabecera correcta con un texto de relleno.
* [kcmoduleqml] Corregir el margen superior para los módulos KCM en QML.

### KConfig

* Encontrar la dependencia de Qt5DBus que faltaba.
* kconf_update: Permitir la repetición de pruebas en «--testmode» ignorando «kconf_updaterc».

### KConfigWidgets

* Cambiar «http:» por «https:».

### KContacts

* Se ha corregido el fallo 428276: KContacts no se puede usar en un proyecto de qmake.

### KCoreAddons

* KJob: Añadir «setProgressUnit()» para escoger cómo se calculan los porcentajes.
* Se ha corregido una fuga de memoria potencial en KAboutData::registerPluginData.
* Dividir «suggestName()»; el método de división no comprueba si el archivo existe.
* KAboutData: Marcar como obsoletas «pluginData()» y «registerPluginData()».
* No salir del bucle de eventos en «KJobTest::slotResult()».
* Usar una sobrecarga de «singleShot()» basada en «functor» en «TestJob::start()».

### KDeclarative

* [abstractkcm] Definir un relleno explícitamente.
* [simplekcm] Eliminar el manejo de relleno personalizado.
* [kcmcontrols] Eliminar código duplicado.
* Añadir fuente para «KDeclarativeMouseEvent».
* Cambiar el padre de las hojas superpuestas a la raíz.
* Hacer que «GridViewKCM» y «ScrollVieKCM» hereden de «AbstractKCM».
* Se ha añadido un método «getter» a «subPages».

### KDocTools

* Se ha corregido el formato XML en «contributor.entities».
* Actualización coreana: Reformatear los archivos HTML de GPL, FDL y añadir LGPL.

### KFileMetaData

* [ExtractionResult] Restaurar la compatibilidad binaria.
* [TaglibWriter|Extractor] Eliminar el tipo mime en bruto de «speex».
* [TaglibWriter] Abrir en modo lectura-escritura también en Windows.
* [Extractor|WriterCollection] Excluir del filtro los archivos que no sean bibliotecas.
* [EmbeddedImageData] Intentar parchear la estupidez de MSVC.
* [ExtractionResult] Marcar como obsoleto «ExtractEverything».
* [EmbeddedImageData] Leer la imagen real del campo de pruebas solo una vez.
* [EmbeddedImageData] Eliminar la implementación privada para escribir la portada.
* [EmbeddedImageData] Mover la implementación de escritura al complemento de escritura de «taglib».
* [EmbeddedImageData] Eliminar la implementación privada de extracción de portada.
* [EmbeddedImageData] Mover la implementación al complemento de extracción de «taglib».

### KGlobalAccel

* Activación de DBus de «systemd».

### KIconThemes

* Mantener las proporciones al agrandar con escalado.

### KIdleTime

* Desaconsejar el uso de la señal con un único argumento «KIdleTime::timeoutReached(int identifier)».

### KImageFormats

* Añadir soporte para los archivos PSD de 16 bits por canal con compresión RLE.
* Devolver que no está soportado al leer archivos PSD de 16 bits comprimidos con RLE.
* Funcionalidad: Añadir soporte para el formato PSD con profundidad de color de 16 bits.

### KIO

* Comparar condicionalmente con QUrl en lugar de con «/» en Windows para «mkpathjob».
* KDirModel: Dos correcciones de errores para «QAbstractItemModelTester».
* CopyJob: Incluir los archivos omitidos en el cálculo del progreso al cambiar de nombre.
* CopyJob: no contar los archivos omitidos en la notificación (error 417034).
* En los diálogos de archivos, seleccionar un directorio existente al intentar crearlo.
* CopyJob: Corregir el número total de archivos/directorios en el diálogo de avance (al mover).
* Hacer que «FileJob::write()» se comporte de forma consistente.
* Soporte de «xattrs» en copiar/mover de KIO.
* CopyJob: No contar los tamaños de los directorios en el tamaño total.
* KNewFileMenu: Se ha corregido un fallo al usar «m_text» en lugar de «m_lineEdit->text()».
* FileWidget: Mostrar la vista previa del archivo seleccionado al abandonar el cursor el elemento (error 418655).
* Exponer el campo de ayuda de contexto del usuario en «kpasswdserver»
* KNewFileMenu: Usar «NameFinderJob» para obtener el nombre «Nueva carpeta».
* Introducir «NameFinderJob» que sugiere nuevos nombres «Nueva carpeta».
* No definir explícitamente líneas «Exec» para módulos KCM (error 398803).
* KNewFileMenu: Dividir el código de creación de diálogo en un método separado.
* KNewFileMenu: Comprobar si el archivo no existe ya con demora para una mejor usabilidad.
* [PreviewJob] Asignar suficiente memoria para el segmento SHM (error 427865).
* Usar un mecanismo de versiones para añadir los nuevos lugares a los usuarios existentes.
* Añadir marcadores de imágenes, música y vídeos (error 427876).
* kfilewidget: Mantener el texto en el cuadro «Name» al navegar (error 418711).
* Manejar los módulos KCM en «OpenUrlJob» con la API de «KService».
* Canonicalizar las rutas de los archivos al obtener y crear miniaturas.
* KFilePlacesItem: Ocultar los montajes «sshfs» de KDEConnect.
* OpenFileManagerWindowJob: Escoger correctamente la ventana de la tarea principal.
* Evitar el sondeo inútil de imágenes de miniatura no existentes.
* [ERROR] Se ha corregido la regresión al seleccionar archivos que contienen `#`.
* KFileWidget: Hacer que los botones de ampliación de iconos salten al tamaño estándar más cercano.
* Situar «minimumkeepsize» realmente en el KCM de preferencias de red (error 419987).
* KDirOperator: Simplificar la lógica del deslizador de ampliación de iconos.
* UDSEntry: Documentar el formato de hora esperado para las claves de tiempo.
* kurlnavigatortest: Eliminar el «desktop:», necesita «desktop.protocol» para funcionar.
* KFilePlacesViewTest: No mostrar una ventana, no es necesario.
* OpenFileManagerWindowJob: Corregir fallo al recurrir a la estrategia de «KRun» (error 426282).
* Palabras clave de Internet: Corregir fallo y las pruebas fallidas si el delimitador es un espacio.
* Preferir los «bangs» de DuckDuckGo sobre otros delimitadores.
* KFilePlacesModel: Ignorar los lugares ocultos al calcular «closestItem» (error 426690).
* SlaveBase: Documentar el comportamiento de «ERR_FILE_ALREADY_EXIST» con «copy()».
* kio_trash: Corregir la lógica cuando no se ha definido ningún límite de tamaño (error 426704).
* En los diálogos de archivos, al crear un directorio que ya existe debe seleccionarlo.
* KFileItemActions: Añadir propiedad para contar mín/máx de URL.

### Kirigami

* [avatar]: Hacer que los números sean nombres no válidos.
* [avatar]: Exponer la propiedad «cache» de la imagen.
* Definir también «maximumWidth» para los iconos del cajón global (error 428658).
* Hacer que el acceso rápido para salir sea una acción y exponerla como propiedad de solo lectura.
* Usar cursores de mano en «ListItemDragHandle» (error 421544).
* [controles/avatar]: Permitir nombres en CJK para las iniciales.
* Mejorar el aspecto de «FormLayout» en la plataforma móvil.
* Se han corregido los menús en «contextualActions».
* No alterar «Item» en el código que se llama desde el destructor de «Item» (error 428481).
* No modificar el otro esquema de «reversetwins».
* Situar/quitar el foco en la hoja solapada al abrir/cerrar.
* Arrastrar las ventanas solo por la barra de herramientas global cuando se pulsan y se arrastran.
* Cerrar «OverlaySheet» al pulsar la tecla «Esc».
* Página: Hacer que funcionen las propiedades «padding», «horizontalPadding» y «verticalPadding».
* ApplicationItem: Usar la propiedad «background».
* AbstractApplicationItem: Se han añadido las propiedades y el comportamiento que faltaban de «ApplicationWindow» de QQC2.
* Limitar la anchura de los elementos a la de la disposición.
* Se ha corregido que no se mostrara el botón «Atrás» en las cabeceras de las páginas por capas en la plataforma móvil.
* Silenciar la advertencias sobre el bucle de enlace «marcable» en «ActionToolBar».
* Intercambiar el orden de las columnas en las disposiciones de idiomas que se leen de derecha a izquierda.
* Solución alternativa para asegurar que «ungrabmouse» se llama cada vez.
* Comprobar la existencia de «startSystemMove».
* Se ha corregido el separador en las disposiciones reflejadas.
* Arrastrar ventanas al pulsar sobre áreas vacías.
* No desplazar al arrastrar con el ratón.
* Se han corregido los casos en los que la respuesta es «null».
* Se ha corregido la refactorización defectuosa de «Forward/BackButton.qml».
* Asegurar que el icono vacío está preparado y no se pinta como el icono anterior.
* Restringir la altura del botón «Atrás/Adelante» en «PageRowGlobalToolBarUI».
* Silenciar la salida no deseada en la consola de «ContextDrawer».
* Silenciar la salida no deseada en la consola de «ApplicationHeader».
* Silenciar la salida no deseada en la consola del botón «Atrás/Adelante».
* Impedir el arrastre del ratón al arrastrar una «OverlaySheet».
* No usar el prefijo «lib» cuando se compila para Windows.
* Corregir la gestión de alineación de «twinformlayouts».
* Se ha mejorado la legibilidad de QML integrado en el código C++.

### KItemModels

* KRearrangeColumnsProxyModel: Se ha corregido un fallo cuando no hay modelo fuente.
* KRearrangeColumnsProxyModel: Solo la columna 0 tiene hijos.

### KNewStuff

* Se ha corregido la lógica errónea introducida en «e1917b6a».
* Se ha corregido un fallo de doble borrado en «kpackagejob» (error 427910).
* Marcar como obsoleto «Button::setButtonText()» y corregir la documentación de la API.
* Posponer todas las escrituras de caché en disco hasta que exista un segundo de tranquilidad.
* Se ha corregido un fallo cuando la lista de archivos instalados está vacía.

### KNotification

* Marcar «KNotification::activated()» como obsoleto.
* Aplicar algunas comprobaciones de saneamiento a las teclas de acciones (fallo 427717).
* Usar «FindGradle» desde ECM.
* Se ha corregido la condición para usar DBus.
* Corrección: Se ha activado la bandeja del sistema heredada en plataformas sin DBus.
* Reescribir «notifybysnore» para proporcionar un soporte más fiable para Windows.
* Se han añadido comentarios para describir el campo «DesktopEntry» en el archivo «notifyrc».

### Framework KPackage

* Hacer que la advertencia «sin metadatos» solo sea para propósitos de depuración.

### KPty

* Eliminar el soporte de AIX, Tru64, Solaris e Irix.

### KRunner

* Desaconsejar el uso de los métodos obsoletos de «RunnerSyntax».
* Marcar como obsoletos «ignoreTypes» y «RunnerContext::Type».
* No definir el tipo para el archivo o el directorio si no existen (error 342876).
* Se ha actualizado el encargado como se ha discutido en la lista de distribución.
* Marcar como obsoleto el constructor no usado de «RunnerManager».
* Desaconsejar el uso de la funcionalidad de categorías.
* Eliminar una comprobación innecesaria cuando el lanzador está suspendido.
* Marcar como obsoletos los métodos «defaultSyntax» y «setDefaultSyntax».
* Limpiar el antiguo uso de «RunnerSyntax».

### KService

* Permitir que se pueda usar «NotShowIn=KDE apps», que se lista en «mimeapps.list» (error 427469).
* Escribir un valor al que recurrir para las líneas «Exec» de los KCM con el ejecutable adecuado (error 398803).

### KTextEditor

* [EmulatedCommandBar::switchToMode] No hacer nada cuando los modos anterior y nuevo son el mismo (error 368130).
* KateModeMenuList: Se han eliminado los márgenes especiales para Windows.
* Se ha corregido una fuga de memoria en «KateMessageLayout».
* Intentar evitar el borrado de estilos personalizados de resaltado que no se han tocado en ningún momento (error 427654).

### KWayland

* Proporcionar métodos de conveniencia alrededor de «wl_data_offet_accept()».
* Marcar enumeradores dentro de «Q_OBJECT» con «Q_ENUM».

### KWidgetsAddons

* Nuevo «setUsernameContextHelp» en «KPasswordDialog».
* KFontRequester: Eliminar la ahora redundante función auxiliar «nearestExistingFont».

### KWindowSystem

* xcb: Se ha corregido la detección del tamaño de la pantalla para alta densidad de puntos.

### NetworkManagerQt

* Añadir un enumerador y declaraciones para permitir el paso de capacidades en el proceso de registro para «NetworkManager».

### Framework de Plasma

* Componente «BasicPlasmoidHeading».
* Mostrar siempre los botones «ExpandableListitem», no solo al situar el cursor encima (fallo 428624).
* [PlasmoidHeading]: Definir correctamente el tamaño implícito.
* Fijar los colores de la cabecera de Brisa oscuro y de Brisa claro (error 427864).
* Se ha unificado el aspecto de los iconos de batería para 32 y 22 píxeles.
* Se han añadido sugerencias de márgenes a «toolbar.svg» y se ha refactorizado la barra de herramientas PC3.
* Se han añadido «AbstractButton» y «Pane» a PC3.
* Permitir el uso de grupos de acciones exclusivas en las acciones de contexto.
* Se ha vuelto a corregir la rotación del indicador de ocupado incluso cuando no está visible.
* Se ha corregido que los colores no se apliquen en el icono del selector de tareas móvil.
* Se han añadido iconos para el selector de tareas móvil y para cerrar aplicaciones (para el panel de tareas).
* Mejor menú en «PlasmaComponents3».
* Se han eliminado anclajes innecesarios en «ComboBox.contentItem».
* Redondear la posición del asa del deslizador.
* [ExpandableListItem] Cargar la vista expandida bajo demanda.
* Se ha añadido «PlasmaCore.ColorScope.inherit: false», que faltaba.
* Definir «colorGroup» de «PlasmoidHeading» en el elemento raíz.
* [ExpandableListItem] Hacer que el texto con color sea 100% opaco (fallo 427171).
* Indicador de ocupado: No rotar cuando sea invisible (fallo 426746).
* «ComboBox3.contentItem» debe ser un «QQuickTextInput» para corregir la completación automática (fallo 424076).
* FrameSvg: No reiniciar la caché al cambiar el tamaño.
* Conmutar los plasmoides cuando se activa el acceso rápido (fallo 400278).
* TextField 3: Se ha añadido una importación que faltaba.
* Se han corregido los ID en el icono «plasmavault_error».
* PC3: se ha corregido el color de la etiqueta «TabButton».
* Usar un «hint» en lugar de un «bool».
* Permitir que los plasmoides puedan ignorar los márgenes.

### Purpose

* Se ha añadido una descripción al proveedor de youtube para «kaccounts».

### QQC2StyleBridge

* Se ha corregido el bucle de enlace de «contentWidth» de la barra de herramientas.
* Hacer referencia a la etiqueta del acceso rápido directamente por su identificador en lugar de implícitamente.
* «ComboBox.contentItem» debe ser un «QQuickTextInput» para corregir la completación automática (fallo 425865).
* Simplificar las cláusulas condicionales en las conexiones.
* Se han corregido advertencias de conexiones en «ComboBox».
* Añadir el uso de iconos «qrc» para «StyleItem» (error 427449).
* Indicar correctamente el estado del foco en «ToolButton».
* Se ha añadido «TextFieldContextMenu» para los menús de contexto del botón derecho en «TextField» y «TextArea».
* Definir el color de fondo de la vista de desplazamiento de las listas desplegables.

### Solid

* Permitir el uso de «sshfs» en el motor «fstab».
* CMake: Usar «pkg_search_module» al buscar «plist».
* Se ha corregido el motor «imobiledevice»: Comprobar la versión de la API para «DEVICE_PAIRED».
* Se ha corregido la compilación del motor «imobiledevice».
* Se ha añadido el motor «Solid» usando «libimobiledevice» para encontrar dispositivos iOS.
* Usar QHash para mapear cuando no se necesita un orden.

### Sonnet

* Usar la sintaxis moderna de conexiones «signal-slot».

### Resaltado de sintaxis

* Falta la función «compact».
* Descomentar la comprobación y añadir comentario sobre por qué esto ya no funciona aquí.
* Falta el valor «position:sticky».
* Se ha corregido la generación de «php/*» para el resaltado de los nuevos comentarios.
* Funcionalidad: Sustituir las alertas con sintaxis de comentarios especiales y eliminar «modelines».
* Funcionalidad: Se ha añadido `comments.xml` como sintaxis paraguas para varios tipos de comentarios.
* Corrección: La sintaxis de CMake marca ahora `1` y `0` como valores lógicos especiales.
* Mejora: Incluir reglas de «modelines» en los archivos donde se han añadido alertas.
* Mejora: Se han añadido más valores booleanos a `cmake.xml`.
* Temas solarizados: Se ha mejorado el separador.
* Mejora: Actualizaciones para CMake 3.19.
* Se ha implementado el uso de archivos de unidades de «systemd».
* debchangelog: Se ha añadido Hirsute Hippo.
* Funcionalidad: Permitir múltiples opciones `-s` en la herramienta `kateschema2theme`.
* Mejora: Se han añadido diversas pruebas al conversor.
* Se han movido más guiones de utilidades a un lugar mejor.
* Se ha movido el guion «update-kate-editor-org.pl» a un mejor lugar.
* kateschema2theme: Se ha añadido una herramienta en Python para convertir antiguos archivos de esquemas.
* Se ha disminuido la opacidad del separador de los temas Brisa y Drácula.
* Se ha actualizado «README» con la sección «Archivos de temas de color».
* Corrección: Usar `KDE_INSTALL_DATADIR` al instalar archivos de sintaxis.
* Se ha corregido la renderización de «--syntax-trace=region» con gráficos múltiples en el mismo desplazamiento.
* Se han corregido algunos problemas del intérprete «fish».
* Se ha sustituido «StringDetect» por «DetectChar/Detect2Chars».
* Se han sustituido algunas «RegExpr» por «StringDetect».
* Se ha sustituido RegExpr="." + lookAhead por «fallthroughContext».
* Se ha sustituido \s* con «DetectSpaces».

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
