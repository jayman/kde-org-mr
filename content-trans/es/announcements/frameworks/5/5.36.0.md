---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Todas las frameworks: Opción para compilar e instalar el archivo QCH con la «dox» de la API pública.

### Baloo

- Usar FindInotify.cmake para decidir si «inotify» está disponible.

### Iconos Brisa

- No depender de bash si no es necesario, y no validar los iconos por omisión.

### Módulos CMake adicionales

- FindQHelpGenerator: evitar la selección de la versión de Qt4.
- ECMAddQch: provocar un fallo severo si las herramientas necesarias no están presentes, para evitar sorpresas.
- Descartar la dependencia de perl para ecm_add_qch (no es necesaria o no se usa).
- Explorar toda la carpeta de instalación para buscar dependencias de qml.
- Nuevo: ECMAddQch, para generar archivos qch y de etiquetas de doxygen.
- Se ha corregido «KDEInstallDirsTest.relative_or_absolute_usr» para evitar el uso de rutas de Qt.

### KAuth

- Comprobar el estado de error tras cada uso de «PolKitAuthority».

### KBookmarks

- Emitir errores cuando «keditbookmarks» está ausente (fallo 303830).

### KConfig

- Corrección para CMake 3.9.

### KCoreAddons

- Usar FindInotify.cmake para decidir si «inotify» está disponible.

### KDeclarative

- KKeySequenceItem: hacer que se pueda guardar «Ctrl+Num+1» como acceso rápido.
- Empezar a arrastrar cuando se pulsa y se mantiene la pulsación en los eventos táctiles (fallo 368698).
- No confiar en que «QQuickWindow» entregue «QEvent::Ungrab» como «mouseUngrabEvent» (porque ya no se hace así en Qt 5.8+) (fallo 380354).

### Soporte de KDELibs 4

- Buscar «KEmoticons», que es una dependencia para «config.cmake.in» de CMake (fallo 381839).

### KFileMetaData

- Se ha añadido un extractor que usa «qtmultimedia».

### KI18n

- Asegurar que se genera el objetivo «tsfiles».

### KIconThemes

- Más detalles sobre el despliegue de temas de iconos en Mac y MSWin.
- Cambiar el tamaño de los iconos del panel por omisión a 48.

### KIO

- [KNewFileMenu] Ocultar el menú «Enlazar a dispositivo» si debe estar vacío (fallo 381479).
- Usar «KIO::rename» en lugar de «KIO::moveAs» en «setData» (fallo 380898).
- Se ha corregido la posición del menú desplegable en Wayland.
- KUrlRequester: Definir la señal «NOTIFY» a «textChanged()» para la propiedad de texto.
- [KOpenWithDialog] Nombre de archivo en codificado en HTML.
- KCoreDirLister::cachedItemForUrl: No se crea la caché si no existe.
- Se usa «data» como nombre de archivo cuando se copian URL de datos (fallo 379093).

### KNewStuff

- Se ha corregido la detección de errores errónea para la falta de archivos «knsrc».
- Se expone y se usa la variable de tamaño de página del motor.
- Ahora se puede usar «QXmlStreamReader» para leer un archivo de registro de KNS.

### Framework KPackage

- Se ha añadido «kpackage-genericqml.desktop».

### KTextEditor

- Se ha corregido el pico de uso máximo de la CPU tras mostrar la barra de órdenes de «vi» (fallo 376504).
- Se ha corregido el arrastre irregular de la barra de desplazamiento cuando está activado el minimapa.
- Saltar a la posición de la barra de desplazamiento sobre la que se ha pulsado cuando está activado el minimapa (fallo 368589).

### KWidgetsAddons

- Se ha actualizado «kcharselect-data» a Unicode 10.0.

### KXMLGUI

- KKeySequenceWidget: Ahora es posible usar «Ctrl+Num+1» como acceso rápido (fallo 183458).
- Revertir «Al construir jerarquías de menús, hacer que los menús tengan como padres a sus contenedores».
- Revertir «usar padre efímero directamente».

### NetworkManagerQt

- WiredSetting: Las propiedades «wake on lan» se han adaptado a NM 1.0.6.
- WiredSetting: La propiedad «metered» se ha adaptado a NM 1.0.6.
- Añadir nuevas propiedades a muchas clases de preferencias.
- Dispositivo: Añadir estadísticas del dispositivo.
- Añadir el dispositivo de túnel Ip.
- WiredDevice: Se ha añadido información sobre la versión de NM solicitada para la propiedad «s390SubChannels».
- TeamDevice: Se ha añadido una nueva propiedad de configuración (desde NM 1.4.0).
- Dispositivo cableado: Añadir la propiedad «s390SubChannels».
- Actualizar introspecciones (NM 1.8.0).

### Framework de Plasma

- Se asegura que el tamaño es definitivo tras «showEvent».
- Se han corregido los márgenes y el esquema de color del icono de VLC en la bandeja del sistema.
- Se ha establecido que los contenedores tengan el foco dentro de la vista (fallo 381124).
- Generar la clave antigua antes de de actualizar «enabledborders» (fallo 378508).
- Mostrar también el botón para mostrar la contraseña cuando no hay ningún texto (fallo 378277).
- Emitir «usedPrefixChanged» cuando el prefijo está vacío.

### Solid

- CMake: construir el motor «udisks2» en FreeBSD solo cuando está activado.

### Resaltado de sintaxis

- Resaltar los archivos .julius como JavaScript.
- Haskell: Se han añadido las directivas del compilador del lenguaje como palabras claves.
- CMake: OR/AND no se resaltan tras una expresión entre () (fallo 360656).
- Makefile: Se han eliminado las entradas de palabras claves no válidas en «makefile.xml».
- Indexador: Mejores informes de errores.
- Actualización de la versión de archivo de sintaxis HTML.
- Se han añadido modificadores angulares en los atributos HTML.
- Se han actualizado los datos de referencia de las pruebas siguiendo los cambios del envío de código anterior.
- Fallo 376979: Los paréntesis angulares en comentarios de doxygen rompen el resaltado de sintaxis.

### ThreadWeaver

- Se ha proporcionado una solución provisional para el fallo del compilador MSVC2017.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
