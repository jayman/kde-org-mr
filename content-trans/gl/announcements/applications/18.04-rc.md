---
aliases:
- ../announce-applications-18.04-rc
date: 2018-04-06
description: KDE Ships Applications 18.04 Release Candidate.
layout: application
release: applications-18.03.90
title: KDE publica a candidata a versión final da versión 18.04 das aplicacións de
  KDE
---
6 de abril de 2018. Hoxe KDE publicou a candidata a versión final da nova versión das súas aplicacións. Coa desautorización temporal de dependencias e funcionalidades novas, agora o equipo de KDE centrase en solucionar fallos e pulir funcionalidades.

Check the <a href='https://community.kde.org/Applications/18.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release

Hai que probar ben a versión 18.04 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera fallo canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a candidata a versión final <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.
