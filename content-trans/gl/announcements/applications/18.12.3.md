---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE Ships Applications 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE Ships KDE Applications 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de vinte de correccións de erros inclúen melloras en, entre outros, Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize e Umbrello.

Entre as melloras están:

- Corrixiuse a carga de arquivos .tar.zstd en Ark
- Dolphin xa non quebra ao deter unha actividade de Plasma
- Cambiar de partición xa non quebra Filelight
