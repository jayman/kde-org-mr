---
aliases:
- ../announce-applications-15.08-rc
date: 2015-08-06
description: KDE Ships Applications 15.08 Release Candidate.
layout: application
release: applications-15.07.90
title: KDE publica a candidata a versión final da versión 15.08 das aplicacións de
  KDE
---
6 de agosto de 2015. Hoxe KDE publicou a candidata a versión final da nova versión das súas aplicacións. Coa desautorización temporal de dependencias e funcionalidades novas, agora o equipo de KDE centrase en solucionar fallos e pulir funcionalidades.

Debido ao gran número de aplicativos que agora se basean na versión 5 das infraestruturas de KDE, hai que probar ben a versión 15.08 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera fallo canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a candidata a versión <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.
