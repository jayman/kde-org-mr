---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE Ships Applications 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE publica a versión 19.04.1 das aplicacións de KDE
version: 19.04.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de vinte correccións de erros inclúen melloras en, entre outros, Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle e Umbrello.

Entre as melloras están:

- Etiquetar ficheiros no escritorio xa non recorta o nome da etiqueta
- Corrixiuse unha quebra no complemento de compartición de texto de KMail
- Corrixíronse varias regresións no editor de vídeo Kdenlive
