---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Comprobar correctamente se un URL corresponde a un ficheiro local

### Baloo

- Correccións de compilación para Windows

### Iconas de Breeze

- Moitas accións e iconas de aplicación novas.
- Indicar as extensións ofrecidas como por cambio en kiconthemes

### Módulos adicionais de CMake

- Despregue de Android: permitir proxectos sen cousas en share ou lib/qml (fallo 362578)
- Activa KDE_INSTALL_USE_QT_SYS_PATHS se CMAKE_INSTALL_PREFIX ten o prefixo de Qt 5
- ecm_qt_declare_logging_category: mellorar a mensaxe de erro ao usar sen incluír

### Integración de infraestruturas

- Retirar o complemento platformtheme, dado que está en plasma-integration

### KCoreAddons

- Permitir desactivar o uso de inotify en KDirWatch
- Corrixir KAboutData::applicationData() para inicializar desde os metadatos actuais de Q*Application
- Aclarar que KRandom non se recomenda para usos criptográficos

### KDBusAddons

- KDBusService: converter «-» en «_» nas rutas dos obxectos

### KDeclarative

- Non quebrar cando falta o contexto de openGL

### Compatibilidade coa versión 4 de KDELibs

- Fornecer un MAXPATHLEN de reserva se non hai un definido
- Corrixir KDateTime::isValid() para valores de ClockTime (fallo 336738)

### KDocTools

- Engadíronse aplicacións de entidade

### KFileMetaData

- Fusionar a rama «externalextractors»s
- Corrixíronse complementos e probas externos
- Engadir a posibilidade de complementos externos de escritor
- Engadiuse compatibilidade co complemento de escritor
- Engadir a posibilidade de complementos de extractor externos

### KHTML

- Engadir toString a Uint8ArrayConstructor e amigos
- Fusionar varias correccións relacionadas con Coverity
- Usar QCache::insert correctamente
- Corrixir algunhas fugas de memoria
- Facer unha comprobación preliminar da análise de fontes web de CSS, evitar unha potencial fuga de memoria
- dom: Engadir prioridades de etiqueta para a etiqueta «comment» (comentario)

### KI18n

- libgettext: corrixir un potencial uso tras liberación usando compiladores distintos de g++

### KIconThemes

- Usar un contedor axeitado para unha matriz interna de punteiros
- Engadir unha oportunidade para reducir accesos innecesarios ao disco, introduce KDE-Extensions
- Evitar algúns accesos a disco

### KIO

- kurlnavigatortoolbutton.cpp - usar buttonWidth en paintEvent()
- Menú de novo ficheiro: evitar duplicados (p. ex. entre .qrc e ficheiros do sistema) (fallo 355390)
- Corrixir unha mensaxe de erro no inicio do KCM de cookies
- Retirar kmailservice5, chegados a este punto non pode facer ningún ben (fallo 354151)
- Corrixir KFileItem::refresh() para ligazóns simbólicas. Definíanse o tamaño, tipo de ficheiro e permisos incorrectos
- Corrixir unha regresión en KFileItem: refresh() perdería o tipo de ficheiro, de xeito que un directorio se convertese nun ficheiro (fallo 353195)
- Definir o texto no trebello QCheckbox en vez de usar unha etiqueta aparte (fallo 245580)
- Non activar o trebello de permisos de acl se non somos o dono do ficheiro (fallo 245580)
- Corrixir unha barra inclinada dobre nos resultados de KUriFilter cando se define un filtro de nome
- KUrlRequester: engadir o sinal textEdited (pasado desde QLineEdit)

### KItemModels

- Corrixir a sintaxe de modelo para a xeración de casos de proba
- Corrixir a ligazón con Qt 5.4 (#endif colocado incorrectamente)

### KParts

- Corrixir a disposición do diálogo BrowserOpenOrSaveQuestion

### KPeople

- Engadir unha comprobación da validez de PersonData

### KRunner

- Corrixir metainfo.yaml: KRunner nin é unha asistencia de migración nin está obsoleto

### KService

- Retirar unha lonxitude de cadea máxima demasiado estrita da base de datos de KSycoca

### KTextEditor

- Use proper char syntax '"' instead of '"'
- doxygen.xml: usar o estilo predeterminado dsAnnotation tamén para «etiquetas personalizadas» (menos cores definidas a man)
- Engadir unha opción para mostrar o número de palabras
- Mellorouse o contraste da cor principal para o salientado de buscar e substituír
- Corrixir a quebra ao pechar Kate mediante D-Bus mentres o diálogo de impresión está aberto (fallo #356813) (fallo 356813)
- Cursor::isValid(): engadir unha nota sobre isValidTextPosition()
- Engadir API {Cursor, Range}::{toString, static fromString}

### KUnitConversion

- Informar ao cliente en caso de non coñecer o tipo de conversión
- Engadir a divisa ILS (novo shekel) (fallo 336016)

### Infraestrutura de KWallet

- desactivar a restauración da sesión de kwalletd5

### KWidgetsAddons

- KNewPasswordWidget: retirar o consello de tamaño do espazador, que provocaba espazo sempre baleiro na disposición
- KNewPasswordWidget: corrixir QPalette cando o trebello está desactivado

### KWindowSystem

- Corrixir a xeración do complemento de ruta a XCB

### Infraestrutura de Plasma

- [QuickTheme] Corrixir as propiedades
- highlight e highlightedText do grupo de cores axeitado
- ConfigModel: Non intentar resolver as rutas de orixe baleiras do paquete
- [calendario] Só mostrar a marca de eventos na grade de días, non no mes ou no ano
- declarativeimports/core/windowthumbnail.h - corrixir o aviso -Wreorder
- cargar de novo o tema de iconas correctamente
- Escribir sempre o nome do tema en plasmarc, tamén cando se escolle o tema predeterminado
- [Calendario] Engadir unha marca aos días que conteñan un evento
- engadir cores de texto positiva, neutral e negativa
- ScrollArea: Corrixir un aviso cando contentItem non é escintilante
- Engadir unha propiedade e un método para aliñar o menú coa esquina do seu pai visual
- Permitir definir a anchura mínima en Menu
- Manter a orde na lista ordenada de elementos
- Estender a API para permitir colocar os elementos do menú durante unha inserción procedemental
- asociar a cor de highlightedText (texto salientado) en Plasma::Theme
- Corrixir a retirada do application/urls asociado para Plasma::Applets
- Non expoñer símbolos da clase privada DataEngineManager
- engadir un elemento «event» no SVG do calendario
- SortFilterModel: Invalidar o filtro ao cambiar a retrochamada de filtro

### Sonnet

- Instalar a ferramenta parsetrigrams para compilación cruzada
- hunspell: cargar e almacenar un dicionario persoal
- Compatibilidade con hunspell 1.4
- configwidget: notificar sobre configuracións cambiadas cando se actualizasen palabras ignoradas
- configuración: non gardar inmediatamente a configuración ao actualizar a lista para ignorar
- configwidget: corrixir a garda cando se actualizan as palabras ignoradas
- Corrixir o problema de non poder gardar a palabra para ignorar (fallo 355973)

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
