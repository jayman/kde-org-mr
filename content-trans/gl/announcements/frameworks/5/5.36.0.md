---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
All frameworks: Option to build &amp; install QCH file with the public API dox

### Baloo

- Usar FindInotify.cmake para decidir se inotify está dispoñíbel

### Iconas de Breeze

- Non depender de Bash innecesariamente e non validar iconas de maneira predeterminada

### Módulos adicionais de CMake

- FindQHelpGenerator: evitar coller a versión de Qt 4
- ECMAddQch: fallar duramente se as ferramentas necesarias non están presentes, para evitar sorpresas
- Deixar de depender de perl para ecm_add_qch, non se usa
- Examinar o cartafol de instalación completo en busca de dependencias de QML
- Novo: ECMAddQch, para xerar os ficheiros de etiquetas de qch e doxygen
- Corrixir KDEInstallDirsTest.relative_or_absolute_usr, evitar usar as rutas de Qt

### KAuth

- Comprobar o estado de erro tras cada uso de PolKitAuthority

### KBookmarks

- Emitir erros cando falte keditbookmarks (fallo 303830)

### KConfig

- Corrección para CMake 3.9

### KCoreAddons

- Usar FindInotify.cmake para decidir se inotify está dispoñíbel

### KDeclarative

- KKeySequenceItem: permitir gardar Ctrl+Núm+1 como atallo
- Iniciar o arrastre premendo e mantendo nos eventos táctiles (fallo 368698)
- Non confiar en que QQuickWindow entregue QEvent::Ungrab como mouseUngrabEvent (dado que xa non o fai en Qt ≥ 5.8) (fallo 380354)

### Compatibilidade coa versión 4 de KDELibs

- Buscar KEmoticons, que é unha dependencia segundo o config.cmake.in de CMake (fallo 381839)

### KFileMetaData

- Engadir un extractor usando qtmultimedia

### KI18n

- Asegurarse de que se xera o destino tsfiles

### KIconThemes

- Máis detalles sobre despregar temas de iconas en Mac e MSWin
- Cambiar o tamaño predeterminado das iconas do panel a 48

### KIO

- [KNewFileMenu] Agachar o menú «Ligar co dispositivo» se estaría baleiro (fallo 381479)
- Usar KIO::rename en vez de KIO::moveAs en setData (fallo 380898)
- Corrixir a posición do menú despregábel en Wayland
- KUrlRequester: Definir o sinal NOTIFY como textChanged() para a propiedade «text»
- [KOpenWithDialog] Escapar o nome de ficheiro para HTML
- KCoreDirLister::cachedItemForUrl: non crear unha caché se non existía
- Usar «data» como nome de ficheiro ao copiar URL de datos (fallo 379093)

### KNewStuff

- Corrixir unha detección de erro incorrecta de ficheiros de knsrc que faltan
- Expoñer e usar a variábel de tamaño de páxina de Engine
- Permitir usar QXmlStreamReader para ler un ficheiro de rexistro de KNS

### Infraestrutura KPackage

- Engadiuse kpackage-genericqml.desktop

### KTextEditor

- Corrixir o aumento significativo do uso de CPU tras mostrar a barra de ordes de vi (fallo 376504)
- Corrixir os saltos de scrollbar-dragging cando se activa mini-map
- Saltar á posición premida da barra de desprazamento cando se activa o minimapa (fallo 368589)

### KWidgetsAddons

- Actualizar kcharselect-data a Unicode 10.0

### KXMLGUI

- KKeySequenceWidget: permitir gardar Ctrl+Num+1 como atallo (fallo 183458)
- Reverter «Ao construír as xerarquías de menús, subordinar os menús aos seus contedores»
- Reverter «usar transientparent directamente»

### NetworkManagerQt

- WiredSetting: as propiedades de espertar na rede local migráronse a NM 1.0.6
- WiredSetting: migrouse a NM 1.0.6 a propiedade metered de versións posteriores
- Engadir novas propiedades a moitas clases de configuración
- Dispositivo: engadir estatísticas de dispositivos
- Engadir dispositivo de IpTunnel
- WiredDevice: engadir información sobre a versión de NM necesaria para a propiedade s390SubChannels
- TeamDevice: engadir unha nova propiedade de configuración (desde NM 1.4.0)
- Dispositivo con fío: engadir a propiedade s390SubChannels
- Actualizar as introspeccións (NM 1.8.0)

### Infraestrutura de Plasma

- Asegurarse de que o tamaño é definitivo tras showEvent
- Corrixir as marxes e o esquema de cores da icona da área de notificacións de VLC
- Configurar os Containment para ter o foco dentro da vista (fallo 381124)
- xerar a clave vella antes de actualizar enabledborders (fallo 378508)
- mostrar o botón de contrasinal tamén en caso de texto baleiro (fallo 378277)
- Emitir usedPrefixChanged cando o prefixo estea baleiro

### Solid

- cmake: construír a infraestrutura de udisks2 en FreeBSD só cando se active

### Realce da sintaxe

- Salientar os ficheiros .julius como JavaScript
- Haskell: Engadir todos os pragmas da linguaxe como palabras clave
- CMake: OR e AND non se realzan tras expr en () (fallo 360656)
- Makefile: Retirar entradas de palabra clave incorrectas en makefile.xml
- indexador: mellorar o informe de erros
- Actualización da versión do ficheiro de sintaxe de HTML
- Engadíronse modificadores angulares nos atributos de HTML
- Actualizar os datos de referencia da proba en consonancia cos cambios da remisión anterior
- Fallo 376979 - o maior que e o menor que en comentarios de Doxygen rompen o realce de sintaxe

### ThreadWeaver

- Solución temporal para un fallo do compilador MSVC2017

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
