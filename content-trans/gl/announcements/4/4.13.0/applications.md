---
date: '2014-04-16'
description: KDE Ships Applications and Platform 4.13.
hidden: true
title: A versión 4.13 das aplicacións de KDE benefícianse da nova busca semántica
  e inclúen novas funcionalidades
---
A comunidade KDE está orgullosa de anunciar a última gran versión de actualizacións das aplicacións de KDE, que fornece novas funcionalidades e correccións. Kontact, o xestor de información persoal, recibiu unha intensa actividade que afectou positivamente á tecnoloxía de busca semántica de KDE e trouxo novas funcionalidades. O visor de documentos Okular e o editor de textos avanzado Kate recibiron melloras de interface e de funcionalidades. Nas áreas de educación e xogos, introducimos un novo adestrador de idiomas estranxeiros, Artikulate; Marble, o globo de escritorio, permite explorar o Sol, a Lúa, varios planetas, rutas de bicicleta e millas náuticas; Palapeli, a aplicación de quebracabezas jigsaw, adquiriu novas dimensións e capacidades.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE Kontact introduce novas funcionalidades e máis velocidade

A colección Kontact de KDE introduce unha serie de funcionalidades nos seus distintos compoñentes. KMail introduce almacenamento na nube e compatibilidade mellorada con Sieve para filtrar do lado do servidor, agora KNotes pode xerar alarmas e introduce funcionalidades de busca, e aplicáronse moitas melloras na capa de caché de datos de Kontact, acelerando case todas as operacións.

### Compatibilidade con almacenamento na nube

KMail introduce compatibilidade con servizos de almacenamento, para que anexos grandes poidan almacenarse en servizos da nube e incluírse como ligazóns en mensaxes de correo electrónico. Entre os servizos de almacenamento compatíbeis están Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic e hai unha opción xenérica para WebDav. Unha ferramenta, <em>storageservicemanager</em>, axuda a xestionar os ficheiros neses servizos.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Grandes melloras na compatibilidade con Sieve

Os filtros de Sieve, unha tecnoloxía para permitir a KMail xestionar filtros do lado do servidor, agora poden xestionar a compatibilidade con vacacións para varios servidores. A ferramenta KSieveEditor permite aos usuarios editar os filtros de Sieve sen ter que engadir o servidor a Kontact.

### Outros cambios

A barra de filtro rápido ten unha pequena mellora de interface de usuario e benefíciase en gran medida das melloras das capacidades de busca que se incluíron na versión 4.13 da plataforma de desenvolvemento de KDE. A busca é bastante máis rápida e fiábel. O editor introduce un acurtador de URL, estendendo as ferramentas existentes de tradución e fragmentos de texto.

Agora as etiquetas e as anotacións de datos de xestión de información persoal almacénanse en Akonadi. En versións futuras tamén se almacenarán en servidores (en IMAP ou Kolab), permitindo compartir etiquetas entre varios computadores. Akonadi: engadiuse compatibilidade coa API de Google Drive. Permítese buscar con complementos de terceiros (o que significa que os resultados poden recibirse rapidamente) e buscas do lado do servidor (buscar elementos que non indexase un servizo de indexación local).

### KNotes, KAddressbook

Realizouse traballo significativo en KNotes, corrixindo unha serie de fallos e pequenas incomodidades. A posibilidade de poñer alarmas en notas é nova, igual que buscar nas notas. Consulte máis información <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aquí</a>. KAdressbook recibiu a funcionalidade de impresión: máis detalles <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aquí</a>.

### Melloras de rendemento

Kontact performance is noticeably improved in this version. Some improvements are due to the integration with the new version of KDE’s <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Semantic Search</a> infrastructure, and the data caching layer and loading of data in KMail itself have seen significant work as well. Notable work has taken place to improve support for the PostgreSQL database. More information and details on performance-related changes can be found in these links:

- Storage Optimizations: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>sprint report</a>
- speed up and size reduction of database: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>mailing list</a>
- optimization in access of folders: <a href='https://git.reviewboard.kde.org/r/113918/'>review board</a>

### KNotes, KAddressbook

Realizouse traballo significativo en KNotes, corrixindo unha serie de fallos e pequenas incomodidades. A posibilidade de poñer alarmas en notas é nova, igual que buscar nas notas. Consulte máis información <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aquí</a>. KAdressbook recibiu a funcionalidade de impresión: máis detalles <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aquí</a>.

## Okular refina a interface de usuario

This release of the Okular document reader brings a number of improvements. You can now open multiple PDF files in one Okular instance thanks to tab support. There is a new Magnifier mouse mode and the DPI of the current screen is used for PDF rendering, improving the look of documents. A new Play button is included in presentation mode and there are improvements to Find and Undo/Redo actions.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate introduce unha barra de estado mellorada, coincidencia animada entre corchetes e complementos mellorados

The latest version of the advanced text editor Kate introduces <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>animated bracket matching</a>, changes to make <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>AltGr-enabled keyboards work in vim mode</a> and a series of improvements in the Kate plugins, especially in the area of Python support and the <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>build plugin</a>. There is a new, much <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>improved status bar</a> which enables direct actions like changing the indent settings, encoding and highlighting, a new tab bar in each view, code completion support for <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>the D programming language</a> and <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>much more</a>. The team has <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>asked for feedback on what to improve in Kate</a> and is shifting some of its attention to a Frameworks 5 port.

## Varias funcionalidades en todas partes

Konsole trae flexibilidade adicional permitindo que as follas de estilo personalizadas controlen as barras de separadores. Agora os perfís poden almacenar os tamaños desexados de columnas e filas. Consulte máis <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>aquí</a>.

Umbrello permite duplicar diagrama e introduce menús contextuais intelixentes que axustan os seus contidos aos trebellos seleccionados. Tamén se melloraron a funcionalidade de desfacer e as propiedades visual. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introduce a funcionalidade de vistas previas de RAW</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

O mesturador de son KMix introduciu mando a distancia mediante o protocolo de comunicación entre procesos de D-Bus (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>detalles</a>), engadidos ao menú de son e un novo diálogo de configuración (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>detalles</a>), e unha serie de correccións de fallos e pequenas melloras.

Modificouse a interface de busca de Dolphin para aproveitar a nova infraestrutura de busca e aplicáronselle melloras adicionais de rendemento. Para máis información, consulte o <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>resumo do traballo de optimización do ano pasado</a>.

KHelpcenter engade orde alfabética para módulos e reorganización de categorías para facelo máis fácil de usar.

## Xogos e aplicacións educativos

Nesta versión os xogos e aplicacións educativas de KDE recibiron moitas actualizacións. A aplicación de quebracabezas jigsaw de KDE, Palapeli, obtivo <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>novas funcionalidades xeniais</a> que facilitan resolver quebracabezas grandes (de ata 10.000 pezas) para aqueles a quen lles gustan os retos. KNavalBattle mostra a posición das naves inimigas ao rematar a partida para que poida ver onde errou.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

As aplicacións educativas de KDE conseguiron novas funcionalidades. KStars consegue unha interface de scripting mediante D-Bus e pode usar a API de servizos web de astrometry.net para optimizar o uso de memoria. O editor de scripts de Cantor goza agora de realce de sintaxe e o editor agora permite as súas infraestruturas de Scilab e Python 2. A ferramenta educativa de mapas e navegación Marble inclúe agora as posicións do <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sol, a Lúa</a> e <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>varios planetas</a> e permite <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>capturar películas durante as viaxes virtuais</a>. Mellorouse o cálculo de rutas de bicicleta ao engadir compatibilidade con cyclestreets.net. Agora permítense millas náuticas e agora premer un <a href='http://en.wikipedia.org/wiki/Geo_URI'>URI xeográfico</a> abrirá Marble.

#### Instalar aplicacións de KDE

Os programas de KDE, incluídas todas as súas bibliotecas e as súas aplicacións, están dispoñíbeis de maneira gratuíta baixo licenzas de código aberto. Os programas de KDE funcionan en varias configuracións de soporte físico e arquitecturas de CPU como ARM e x86, sistemas operativos e funcionan con calquera xestor de xanelas ou contorno de escritorio. Ademais de Linux e outros sistemas operativos baseados en UNIX pode atopar versións para Microsoft Windows da meirande parte das aplicacións de KDE no sitio web de <a href='http://windows.kde.org'>programas de KDE para Windows</a> e versións para o Mac OS X de Apple no <a href='http://mac.kde.org/'>sitio web de programas de KDE para Mac</a>. En Internet poden atoparse construcións experimentais de aplicacións de KDE para varias plataformas móbiles como MeeGo, MS Windows Mobile e Symbian pero non están mantidas. <a href='http://plasma-active.org'>Plasma Active</a> é unha experiencia de usuario para un maior abanico de dispositivos, como computadores de tableta e outros soportes físicos móbiles.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paquetes

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.13.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Lugares dos paquetes

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Community Wiki</a>.

The complete source code for 4.13.0 may be <a href='/info/4/4.13.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.13.0 are available from the <a href='/info/4/4.13.0#binary'>4.13.0 Info Page</a>.

#### Requisitos do sistema

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
