---
date: '2013-12-18'
hidden: true
title: KDE-program 4.12 med enorma framsteg när det gäller personlig informationshantering
  och förbättringar överallt
---
KDE-gemenskapen är stolt över att tillkännage den senaste större uppdateringen av KDE:s program, som levererar nya funktioner och felrättningar. Utgåvan markerar omfattande förbättringar av KDE:s svit för personlig informationshantering, vilket ger mycket bättre prestanda och många nya funktioner. Kate strömlinjeformar integreringen av Python-insticksprogram samt lägger till initialt stöd för Vim-makron, och spel- och utbildningsprogrammen tillhandahåller ett antal olika nya funktioner.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Kate, den mest avancerade grafiska texteditorn för Linux, har åter erhållit arbete på kodkomplettering, denna gång tillägg av <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>avancerad matchningskod, som hanterar förkortningar och partiell matchning i klasser</a>. Den nya koden kan exempelvis matcha  en typbaserad 'QualIdent' med 'QualifiedIdentifier'. Kate har också fått <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>initialt stöd för Vim-makron</a>. Bäst av allt, dessa förbättringar sipprar också igenom till KDevelop och andra program som använder sig av teknologin från Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Dokumentvisaren Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>tar nu hänsyn till skrivarens hårda marginaler</a>, har stöd för ljud och video i EPub, har bättre sökning, och kan nu hantera fler omvandlingar inklusive EXIF-bildmetadata. I UML-diagramverktyget Umbrello kan nu associationer <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>ritas med olika utläggning</a> och Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>lägger till visuell återkoppling om en grafisk komponent har dokumentation</a>.

Integritetsskyddsverktyget Kgpg visar mer information för användaren och Plånbokshantering, verktyget för att spara lösenord, kan nu <a href='http://www.rusu.info/wp/?p=248'>lagra dem med GPG</a>. Terminal introducerar en ny funktion: Ctrl+klick för att direkt följa webbadresslänkar i konsolens utmatning. Den kan nu också <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>lista processer i avslutningsvarningen</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

Kwebkit lägger till möjlighet att <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatiskt skala innehållet för att motsvara skrivbordets upplösning</a>. Filhanteraren Dolphin introducerar ett antal prestandaförbättringar i sortering och visning av filer, som reducerar minnesanvändning och snabbar upp saker och ting. KRDC introducerar automatisk återanslutning i VNC och Kdialog ger nu tillgång till meddelanderutorna  'detailedsorry' och 'detailederror' för mer informativa konsolskript. Kopete uppdaterar insticksprogrammet för optisk teckenigenkänning och Jabber-protokollet får stöd för XEP-0264: Miniatyrbilder vid filöverföringar. Förutom dessa funktioner låg fokus i huvudsak på att städa koden och rätta kompileringsvarningar.

### Spel- och utbildningsprogramvara

KDE:s spel har fått arbete utfört på diverse områden. Othello är nu <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>baserat på QML och Qt Quick</a>, vilket ger ett snyggare spel med bättre flyt. Knetwalk har också <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>överförts</a> med samma fördelar, samt möjlighet att ställa in ett bräde med egen bredd och höjd. Erövring har nu en ny utmanande AI-spelare vid namn 'Becai'.

En hel del större ändringar har skett i utbildningsprogrammen. Ktouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>lägger till stöd för egna lektioner och flera nya kurser</a>; Kstars har en ny, noggrannare <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>inriktningsmodul för teleskop</a>, du hittar en <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>YouTube video här</a> om de nya funktionerna. Cantor, som erbjuder ett enkelt och kraftfullt gränssnitt för ett antal matematiska bakgrundsprogram, har nu gränssnitt <a href='http://blog.filipesaraiva.info/?p=1171'>för Python2 och Scilab</a>. Läs mer om det kraftfulla bakgrundsprogrammet Scilab <a href='http://blog.filipesaraiva.info/?p=1159'>här</a>. Marble lägger till integrering med ownCloud (anpassning är tillgänglig under Inställningarna) och lägger till stöd för överlagrad återgivning. Kalgebra gör det möjligt att exportera tredimensionella diagram till PDF, vilket ger ett utmärkt sätt att dela med dig av ditt arbete. Sist men inte minst har många fel rättats i KDE:s olika utbildningsprogram.

### Brev, kalender och personlig information

KDE PIM, KDE:s programuppsättning för att hantera e-post, kalendrar och annan personlig information, har fått en hel del arbete utfört.

Med början på e-postprogrammet Kmail, finns det nu <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>stöd för reklamblockering</a> (när HTML är aktiverat) och förbättrat stöd för detektering av bedrägeribrev genom förlängning av avkortade webbadresser.En ny Akonadi-modul vid namn Korgarkivering låter användare arkivera lästa brev i särskilda korgar, och det grafiska gränssnittet för funktionen Skicka senare har städats upp. Kmail drar också nytta av förbättrat stöd för Sieve-filter. Sieve möjliggör filtrering av brev på serversidan, och du kan nu <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>skapa och ändra filtren på servrarna</a> samt <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>konvertera befintliga Kmail-filter till serverfilter</a>. Kmails stöd för mbox <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>har också förbättrats</a>.

I övriga program har flera ändringar gjort användning enklare och angenämare. Ett nytt verktyg har lagts till, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>Editor av kontaktteman</a>, som gör det möjligt att skapa Grantlee-teman i adressboken för att visa kontakter. Adressboken kan nu också visa förhandsgranskningar innan data skrivs ut. För Anteckningar har en del <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>seriöst arbete för att rätta fel</a> utförts. Bloggverktyget Blogilo kan nu hantera översättningar, och det har skett omfattande rättningar och förbättringar i alla KDE:s program för personlig informationshantering.

I den underliggande datalagringen för KDE:s personliga informationshantering har <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>mycket arbete rörande prestanda, stabilitet och skalbarhet</a> utförts, som att fixa <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>stöd för PostgreSQL med senaste Qt 4.8.5</a>. Det finns också ett nytt kommandoradsverktyg, calendarjanitor, som kan söka igenom all kalenderdata för att hitta felaktiga förekomster och som lägger till en felsökningsdialogruta. Några mycket speciella kramar går till Laurent Montel för allt det arbete som han gör med funktioner i KDE:s personliga informationshantering.

#### Installera KDE-program

KDE:s programvara, inklusive alla dess bibliotek och program, är fritt tillgänglig med licenser för öppen källkod. KDE:s programvara kör på diverse hårdvarukonfigurationer och processorarkitekturer, såsom ARM och x86, olika operativsystem och fungerar med alla sorters fönsterhanterare och skrivbordsmiljöer. Förutom Linux och andra UNIX-baserade operativsystem finns det versioner av de flesta KDE-program för Microsoft Windows på <a href='http://windows.kde.org'>KDE:s programvara för Windows</a>, och versioner för Apple Mac OS X på <a href='http://mac.kde.org/'>KDE:s programvara för Mac</a>. Experimentella byggen av KDE-program för diverse mobila plattformar som MeeGo, MS Windows Mobile och Symbian finns på webben, men stöds för närvarande inte. <a href='http://plasma-active.org'>Plasma Aktiv</a> erbjuder användningsmöjlighet på en större omfattning apparater, som läsplattor och annan mobil hårdvara. <br />

KDE:s programvara kan erhållas som källkod och som diverse binärformat från <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> och kan också erhållas på <a href='/download'>Cd-rom</a> eller med något av de <a href='/distributions'>större GNU/Linux- och UNIX-systemen</a> som levereras idag.

##### Paket

Vissa Linux- och UNIX-operativsystemleverantörer har vänligen tillhandahållit binärpaket av 4.12.0 för vissa versioner av sina distributioner, och i andra fall har volontärer i gemenskapen gjort det.

##### Paketplatser

Besök <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Gemenskapens Wiki</a> för en aktuell lista med tillgängliga binärpaket som har kommit till KDE-projektets kännedom.

Fullständig källkod för 4.12.0 kan <a href='/info/4/4.12.0'>laddas ner fritt</a>. Instruktioner om hur man kompilerar och installerar KDE:s programvara 4.12.0 är tillgängliga på <a href='/info/4/4.12.0#binary'>informationssidan om 4.12.0</a>.

#### Systemkrav

För att få ut så mycket som möjligt av dessa utgåvor, rekommenderar vi att använda en aktuell version av Qt, såsom 4.8.4. Det är nödvändigt för att försäkra sig om en stabil och högpresterande upplevelse, eftersom vissa förbättringar som har gjorts av KDE:s programvara har i själva verket skett i det underliggande Qt-ramverket.

För att helt dra nytta av kapaciteten i KDE:s programvara, rekommenderar vi också att använda de senaste grafikdrivrutinerna för systemet, eftersom det avsevärt kan förbättra användarupplevelsen både när det gäller tillvalsfunktioner, och övergripande prestanda och stabilitet.
