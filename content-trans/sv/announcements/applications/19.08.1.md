---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE levererar Program 19.08.1.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE levererar Program 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../19.08.0'>KDE-program 19.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än tjugo registrerade felrättningar omfattar förbättringar av bland annat Kontact, Dolphin, Kdenlive, Konsole och Step.

Förbättringar omfattar:

- Flera regressioner i Terminals flikhantering har rättats
- Dolphin startar återigen riktigt när det är i delat visningsläge
- Att ta bort en mjuk kropp i fysiksimulatorn Step orsakar inte längre en krasch
