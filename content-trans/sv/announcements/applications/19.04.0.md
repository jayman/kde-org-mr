---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE levererar Program 19.04.
layout: application
release: applications-19.04.0
title: KDE levererar KDE-program 19.04.0
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

KDE-gemenskapen är glada att tillkännage utgivningen av KDE Program 19.04.

Vår gemenskap arbetar kontinuerligt på att förbättra programvaran som ingår i vår KDE-programserie. Tillsammans med nya funktioner, förbättrar vi konstruktionen, användbarheten och stabiliteten hos alla våra verktyg, spel och kreativitetsverktyg. Vårt mål är att göra livet enklare för dig genom att göra KDE:s programvara mer njutbar att använda. Vi hoppas att du tycker om alla nya förbättringar och felrättningar som du hittar i 19.04.

## Vad är nytt i KDE-program 19.04

Mer än 150 fel har lösts. Rättningarna implementerar om inaktiverade funktioner, normaliserar genvägar och löser krascher, vilket gör KDE-program vänligare och låter dig bli mer produktiv.

### Filhantering

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> är KDE:s filhanterare. Den ansluter också till nätverkstjänster såsom SSH, FTP och Samba-servrar, och levereras med avancerade verktyg för att söka efter och organisera data.

Nya funktioner:

+ Vi har expanderat stödet för miniatyrbilder, så att Dolphin nu kan visa miniatyrbilder för flera nya filtyper: <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a>-filer, <a href='https://phabricator.kde.org/D18738'>.epub och .fb2 e-böcker</a>, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a>-filer och <a href='https://phabricator.kde.org/D19679'>PCX</a>-filer. Dessutom visar nu miniatyrbilder för textfiler <a href='https://phabricator.kde.org/D19432'>syntaxfärgläggning</a> för texten på miniatyrbilden. </li>
+ Nu kan man välja <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>vilken delad visningsruta som ska stängas</a> när man klickar på knappen 'Stäng delad'. </li>
+ Den här versionen av Dolphin introducerar <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>smartare flikplacering</a>. När man öppnar en katalog under en ny flik, placeras den nya fliken omedelbart till höger om den aktuella, istället för alltid i slutet av flikraden. </li>
+ <a href='https://phabricator.kde.org/D16872'>Att ge objekt etiketter</a> är nu mycket mer praktiskt, eftersom etiketter kan läggas till eller tas bort genom att använda den sammanhangsberoende menyn. </li>
+ Vi har <a href='https://phabricator.kde.org/D18697'>förbättrat standardsorteringens parametrar</a> för vissa ofta använda kataloger. Normalt sorteras nerladdningskatalogen nu enligt datum med gruppering aktiverad, och visningen av senaste dokument (tillgänglig genom att navigera till recentdocuments:/) sorteras enligt datum med listvy vald. </li>

Felrättningar omfattar:

+ När en modern version av SMB-protokollet används, kan man nu <a href='https://phabricator.kde.org/D16299'>upptäcka delade Samba-kataloger</a> för Mac- och Linux-datorer. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>Att arrangera om objekt i platspanelen</a> fungerar nu riktigt igen när vissa objekt är dolda. </li>
+ Efter en ny flik öppnats i Dolphin, får den nya flikens vy automatiskt <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>tangentbordsfokus</a>. </li>
+ Dolphin varnar nu om man försöker avsluta medan <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>terminalpanelen är öppen</a> med ett program som kör i den. </li>
+ Vi har rättat många minnesläckor, vilket förbättrar Dolphins övergripande prestanda.</li>

<a href='https://cgit.kde.org/audiocd-kio.git/'>Ljud-cd I/O-slaven</a> gör det möjligt för andra KDE-program att läsa ljud från cd-skivor och automatiskt konvertera det till andra format.

+ Ljud-cd I/O-slaven stöder nu lagring med <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ Vi har gjort <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>cd-informationstexten</a> verkligt genomskinlig för visning. </li>

### Videoredigering

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

Det här är en milstolpeversion för KDE:s videoeditor. <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> har genomgått en omfattande omskrivning av kärnkoden då mer än 60 %% av de interna delarna har ändrats, för att förbättra dess övergripande arkitektur.

Förbättringar omfattar:

+ Tidslinjen har skrivits om för att utnyttja QML.
+ När ett klipp läggs till på tidslinjen, hamnar ljud och video alltid i separata spår.
+ Tidslinjen stöder nu tangentbordsnavigering: klipp, kompositioner och nyckelbilder kan flyttas med tangentbordet. Dessutom kan själva spårens höjd justeras.
+ I den här versionen av Kdenlive, levereras ljudinspelningen i spåret med en ny funktion för <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>dubbning</a>.
+ Vi har förbättrat kopiera och klistra in: Det fungerar mellan olika projektfönster. Hantering av ersättningsklipp har också förbättrats, eftersom klipp nu kan tas bort individuellt.
+ I version 19.04 har stöd för externa BlackMagic bildskärmar kommit tillbaka, och det finns också nya förinställningshjälplinjer för visning på bildskärmar.
+ Vi har förbättrat hantering av nyckelbilder, vilket ger den ett mer likformigt utseende och arbetsflöde. Titelverktyget har också förbättrats genom att låta justeringsknapparna låsas till säkra zoner, lägga till inställningshjälplinjer och bakgrundsfärger, och visa saknade objekt.
+ Vi har rättat tidslinjens korruptionsfel som felplacerade eller missade klipp, vilken utlöstes när man flyttade en grupp av klipp.
+ Vi rättade JPEG-felet som återgav bilder som vita skärmar på Windows. Vi har också rättat fel som påverkade tagning av skärmbilder på Windows.
+ Förutom allt ovanstående har vi lagt till många små användarförbättringar som gör det enklare och smidigare att använda Kdenlive.

### Kontor

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> är KDE:s dokumentvisare med flera funktioner. Idealisk för att läsa och kommentera PDF-filer, och kan också öppna ODF-filer (som används av LibreOffice och OpenOffice), e-böcker publicerade som ePub-filer, de flesta vanliga seriebokfiler, Postscript-filer, och många fler.

Förbättringar omfattar:

+ För att säkerställa att dokumenten alltid passar perfekt, har vi lagt till <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>skalningsalternativ</a> i Okulars utskriftsdialogruta.
+ Okular stöder nu att visa och verifiera <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>digitala signaturer</a> i PDF-filer.
+ Tack vare bättre integrering mellan olika program, stöder nu Okular redigering av Latex dokuments i <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ Förbättrat stöd för <a href='https://phabricator.kde.org/D18118'>navigering på pekskärmar</a> betyder att du kan gå bakåt ock framåt genom att använda en pekskärm i presentationsläge.
+ Användare som föredrar att manipulera dokument från kommandoraden kan utföra smarta <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>textsökningar</a> med den nya kommandoradsväljaren som låter dig öppna ett dokument och färglägga alla förekomster av ett givet textstycke.
+ Okular visar nu länkar riktigt i <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>Markdown-dokument</a> som spänner över mer än en rad.
+ <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>Trimverktygen</a> har snygga nya ikoner.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>Kmail</a> är KDE:s integritetsskyddande e-postprogram. Kmail är en del av <a href='https://kde.org/applications/office/kontact/'>grupprogramsviten Kontact</a> som stöder alla e-postsystem och låter dig organisera dina brev i en delad virtuell inkorg eller i separata konton (välj själv). Det stöder alla sorters brevkryptering och signering, och låter dig dela data såsom kontakter, mötesdatum, och reseinformation med andra Kontact-program.

Förbättringar omfattar:

+ Dessutom Grammarly! Den här versionen av Kmail levereras med stöd för languagetools (grammatikkontroll) och grammelecte (grammatikkontroll enbart för franska).
+ Telefonnummer i brev detekteras nu och kan direkt ringas via <a href='https://community.kde.org/KDEConnect'>KDE-anslut</a>.
+ Kmail har nu ett alternativ för att <a href='https://phabricator.kde.org/D19189'>direkt starta i systembrickan</a> utan att öppna huvudfönstret.
+ Vi har förbättrat insticksstöd för Markdown.
+ Hämtning av brev via IMAP fastnar inte längre när inloggning misslyckas.
+ Vi har också gjort omfattande rättningar i Kmails gränssnitt Akonadi för att förbättra tillförlitlighet och prestanda.

<a href='https://kde.org/applications/office/korganizer/'>Korganizer</a> är Kontacts kalenderkomponent, som hanterar alla händelser.

+ Återkommande händelser från <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Googles kalender</a> synkroniserar åter korrekt.
+ Fönstret för händelsepåminnelser kommer nu ihåg att det ska <a href='https://phabricator.kde.org/D16247'>visas på alla skrivbord</a>.
+ Vi har moderniserat utseendet för <a href='https://phabricator.kde.org/T9420'>händelsevisning</a>.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> är Kontacts helt nya reseassistent som hjälper dig komma fram till resmålet och ger dig råd på vägen.

- Det finns ett nytt generellt extraheringsverktyg för RCT2 biljetter (t.ex. använda av järnvägsföretag såsom DSB, ÖSB, SBB, NS).
- Detektering och tvetydighet av flygplatsnamn har förbättrats avsevärt.
- Vi har lagt till nya egna extraheringsverktyg för företag tidigare utan stöd (t.ex. BCD Travel, NH Group), och förbättrat format- och språkvariationer för företag som redan stöds (t.ex. SNCF, Easyjet, Booking.com, Hertz).

### Utveckling

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> är KDE:s fullfjädrade texteditor, ideal för programmering tack vara funktioner som flikar, delad visning, syntaxfärgläggning, en inbyggd terminalruta, ordkomplettering, sökning och ersättning med reguljära uttryck, och mycket mer via den flexibla infrastrukturen för  insticksprogram.

Förbättringar omfattar:

- Kate kan nu visa alla osynliga blanktecken, inte bara vissa.
- Du kan enkelt aktivera och inaktivera statisk radbrytning genom att använda det egna menyalternativet per dokument, utan att behöva ändra den allmänna standardinställningen.
- De sammanhangsberoende arkiv- och flikmenyerna inkluderar nu ett antal användbara nya alternativ, såsom byta namn, ta bort, kopiera, öppna katalog med innehåll, kopiera filsökväg, jämför (med en annan öppen fil), och egenskaper.
- Den här versionen av Kate levereras med fler insticksprogram normalt aktiverade, inklusive den populära och användbara terminalfunktionen på plats.
- När Kate avslutas, blir du inte längre tillfrågad om att bekräfta filer som har ändrats på disk av någon annan process, såsom en källkodshanteringsändring.
- Trädvyn av insticksprogram visar nu alla menyalternativ korrekt för git-poster som har diakritiska tecken i namnen.
- När flera filer öppna genom att använda kommandoraden, öppnas filerna i nya flikar i samma ordning som de anges på kommandoraden.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Terminal</a> är KDE:s terminalemulator. Den stöder flikar, genomskinlig bakgrund, delat visningsläge, anpassningsbara färgscheman och snabbtangenter, bokmärken för kataloger och SSH, och många andra funktioner.

Förbättringar omfattar:

+ Flikhantering har fått ett antal förbättringar som hjälper dig vara mer produktiv. Nya flikar kan skapas genom att mittenklicka på tomma delar av flikraden, och det finns också ett alternativ som låter dig stänga flikar genom att mittenklicka på dem. Stängningsknappar visar normalt på flikar, och ikoner visas bara när en profil med en egen ikon används. Sist men inte minst, låter genvägen Ctrl+Tabulator dig snabbt byta mellan den aktuella och föregående flik.
+ En omfattande <a href='https://phabricator.kde.org/D17244'>översyn av användargränssnittet</a> har gjorts för dialogrutan Redigera profil.
+ Färgschemat Breeze används nu som det normala färgschemat i Terminal, och vi har förbättrat dess kontrast och likformighet med det systemomfattande Breeze-temat.
+ Vi har löst problemen när fetstil visas.
+ Konsole visar nu markören med understreck korrekt.
+ Vi har förbättrat visning av <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>rut- och linjetecken</a> samt <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>Emoji-tecken</a>.
+ Genvägar för profilbyte byter nu den aktuella flikens profil istället för att öppna en ny flik med den andra profilen.
+ Inaktiva flikar som får en underrättelse och får rubrikens textfärg ändrad återställer nu färgen till den normala textfärgen för rubriker när fliken aktiveras.
+ Funktionen 'Variera bakgrund för varje flik' fungerar nu när bakgrundens basfärg är mycket mörk eller svart.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> är ett datorbaserat översättningssystem som fokuserar på produktivitet och kvalitetskontroll. Dess mål är programvaruöversättning, men det integrerar också externa konverteringsverktyg för att översätta kontorsdokument.

Förbättringar omfattar:

- Lokalize stöder nu att visa översättningskällor med en egen editor.
- Vi har förbättrat placering av dockningskomponenterna, och sättet som inställningar sparas och återställs.
- Positionen i .po-filer bevaras nu när meddelanden filtreras.
- Vi har rättat ett antal fel i användargränssnittet (utvecklarkommentarer, ändring av reguljära uttryck i omfattande ersättningar, antal inexakta tomma meddelanden, …).

### Verktyg

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> är ett avancerat bildvisnings- och organiseringsprogram med intuitiva och lättanvända redigeringsverktyg.

Förbättringar omfattar:

+ Versionen av Gwenview som levereras med Program 19.04 inkluderar fullständigt <a href='https://phabricator.kde.org/D13901'>stöd för pekskärmar</a>, men gester för att flytta, zooma, panorera, med mera.
+ En annan förbättring tillagd i Gwenview är fullständigt <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>stöd för hög upplösning</a>, vilket gör att bilder ser mycket bra ut på bildskärmar med hög upplösning.
+ Förbättrat stöd för <a href='https://phabricator.kde.org/D14583'>musknapparna bakåt och framåt</a> låter dig navigera mellan bilder genom att trycka på knapparna.
+ Du kan nu använda Gwenview för att öppna bildfiler skapade med <a href='https://krita.org/'>Krita</a>, allas favoritverktyg för digital målning.
+ Gwenview stöder nu stora <a href='https://phabricator.kde.org/D6083'>512 bildpunkters miniatyrbilder</a>, vilket låter dig enklare förhandsgranska dina bilder.
+ Gwenview använder nu den <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>vanliga snabbtangenten Ctrl+L</a> för att ge fokus till webbadressfältet.
+ Du kan nu använda <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>funktionen för filtrera enligt namn</a> med genvägen Ctrl+I, precis som i Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> är Plasmas skärmbildsprogram. Du kan ta hela skrivbord som spänner över flera skärmar, individuella skärmar, fönster, delar av fönster, eller egna områden genom att använda funktionen för rektangelmarkering.

Förbättringar omfattar:

+ Vi har utökat rektangelområdesläget med några nya alternativ. Det kan ställas in för att <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>automatiskt acceptera</a> den uppritade rutan istället för att först be dig justera den. Det finns också ett nytt standardalternativ för att komma ihåg den aktuella markeringsrutan för <a href='https://phabricator.kde.org/D19117'>rektangelområdet</a>, men bara tills programmet avslutas.
+ Du kan ställa in vad som händer när skärmbildsgenvägen används <a href='https://phabricator.kde.org/T9855'>medan Spectacle redan kör</a>.
+ Spectacle låter dig ändra <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>komprimeringsnivån</a> för förstörande bildformat.
+ Spara inställningar visar dig hur <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>filnamnet för en skärmbild</a> ser ut. Du kan också enkelt justera <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>filnamnsmallen</a> som du föredrar genom att helt enkelt klicka på platsmarkörerna.
+ Spectacle visar inte längre både alternativen "Fullskärm (alla bildskärmar)" och "Aktuell skärm" när datorn bara har en skärm.
+ Hjälptexten för rektangulärt område visas nu mitt på huvudskärmen, istället för delat mellan skärmarna.
+ När Spectacle kör på Wayland, inkluderas bara funktionerna som fungerar.

### Spel- och utbildning

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Vår programserie inkluderar mängder av <a href='https://games.kde.org/'>spel</a> och <a href='https://edu.kde.org/'>utbildningsprogram</a>.

<a href='https://kde.org/applications/education/kmplot'>Kmplot</a> är en matematisk funktionsritare. Den har en kraftfull inbyggd tolk. Graferna kan färgläggas och visningen är skalbar, vilket låter dig zooma till nivån du behöver. Användare kan rita upp olika funktioner samtidigt och kombinera dem för att bygga nya funktioner.

+ Du kan nu zooma in genom att hålla nere Ctrl och använda <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>mushjulet</a>.
+ I den här versionen av Kdenlive, levereras ljudinspelningen i spåret med en ny funktion för <a href='https://phabricator.kde.org/D17626'>dubbning</a>.
+ Rotvärdet eller paret (x,y) kan ny kopieras till <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>klippbordet</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> är ett minigolfspel.

+ Vi har återställt <a href='https://phabricator.kde.org/D16978'>ljudstöd</a>.
+ Kolf har konverterats från kdelibs4 med lyckat resultat.
