---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE levererar KDE-program 15.04.2
layout: application
title: KDE levererar KDE-program 15.04.2
version: 15.04.2
---
2:e juni, 2015. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../15.04.0'>KDE-program 15.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 30 registrerade felrättningar omfattar förbättringar av gwenview, kate, kdenlive, kdepim, konsole, marble, kgpg, kig, ktp-call-ui och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av Plasma arbetsrymder 4.11.20, KDE:s utvecklingsplattform 4.14.9 och Kontakt-sviten 4.14.9.
