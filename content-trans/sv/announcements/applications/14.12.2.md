---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE levererar Program 14.12.2.
layout: application
title: KDE levererar KDE-program 14.12.2
version: 14.12.2
---
3:e februari, 2015. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../14.12.0'>KDE-program 14.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av anagramspelet Kanagram, UML-modelleringsverktyget Umbrello, dokumentvisaren Okular och den virtuella jordgloben Marble.

Utgåvan inkluderar också versioner för långtidsunderhåll av Plasma arbetsrymder 4.11.16, KDE:s utvecklingsplattform 4.14.5 och Kontakt-sviten 4.14.5.
