---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Tillägg av kommandobeskrivningar (balooctl)
- Sök också i kataloger med symbolisk länk (fel 333678)

### BluezQt

- Tillhandahåll enhetstyp för lågenergienheter

### Extra CMake-moduler

- Ange QML-rotsökväg som den delade katalogen i prefixet
- Rätta kompatibilitet för ecm_generate_pkgconfig_file med ny cmake
- Registrera bara  alternativen APPLE_* om APPLE

### KActivitiesStats

- Tillägg av förinställningar i testprogrammet
- Flytta objekt på ett riktigt sätt till önskad position
- Synkronisera omordning till andra modellinstanser
- Om ordningen inte är definierad, sortera posterna enligt id

### KDE Doxygen-verktyg

- [Meta] Ändra underhållsansvarig i setup.py

### KAuth

- Gränssnitt för Mac
- Lägg till stöd för att döda ett KAuth::ExecuteJob

### KConfig

- Sanera genvägslistan vid läs eller skriv från kdeglobals
- Undvik meningslösa realloc genom att ta bort anrop till squeeze för tillfällig buffert

### KDBusAddons

- KDBusService: Lägg till åtkomstmetod för DBus-tjänstnamnet som vi registrerade

### KDeclarative

- Använd det nya programmeringsgränssnittet med Qt ≥ 5.8 för att ange scengrafens gränssnitt
- Ställ inte in acceptHoverEvents i DragArea eftersom vi inte använder dem

### KDocTools

- meinproc5: Länka till filerna, inte till biblioteket (fel 377406)

### KFileMetaData

- gör så att PlainTextExtractor motsvarar "text/plain" igen

### KHTML

- Felsida, läs in bilden på ett riktigt sätt (med en riktig webbadress)

### KIO

- Få omdirigering av fjärrwebbadressen file:/// till smb:/// att fungera igen
- Behåll frågekodning när HTTP-proxy används
- Uppdatera användaragenter (Firefox 52 ESR, Chromium 57)
- Hantera, avkorta, visad sträng för webbadresser tilldelad till jobbeskrivningar. Förhindrar stora data: webbadresser från att inkluderas i användargränssnittets underrättelser.
- Lägg till KFileWidget::setSelectedUrl() (fel 376365)
- Rätta metod för att spara i KUrlRequester genom att lägga till setAcceptMode

### KItemModels

- Nämn det nya QSFPM::setRecursiveFiltering(true) som gör KRecursiveFilterProxyModel föråldrad

### KNotification

- Ta inte bort köade underrättelser när tjänsten fd.o startar
- Anpassningar till Mac-plattform

### KParts

- Dokumentation av programmeringsgränssnitt: Rätta saknad anmärkning för att anropa setXMLFile med KParts::MainWindow

### KService

- Rätta terminalmeddelanden 'Not found: ""'

### KTextEditor

- Exponera ytterligare intern View-funktionalitet i det öppna programmeringsgränssnittet
- Spara många tilldelningar för setPen
- Rätta ConfigInterface i KTextEditor::Document
- Tillägg av alternativ för teckensnitt och stavningskontroll i farten i ConfigInterface

### Kwayland

- Lägg till stöd för wl_shell_surface::set_popup och popup_done

### KWidgetsAddons

- Stöd att bygga med Qt utan a11y aktiverad
- Rätta felaktigt storlekstips när animatedShow anropas med ett dolt överliggande objekt (fel 377676)
- Rätta att tecken i KCharSelectTable dras ihop
- Aktivera alla plan i testdialogrutan kcharselect

### NetworkManagerQt

- WiredSetting: Returnera automatisk förhandling även om inaktiverad
- Förhindra att signaler i glib2 definieras av Qt
- WiredSetting: Hastighet och duplex kan bara ställas in med automatisk förhandling avstängd (fel 376018)
- Automatisk förhandling av värde för trådbundna inställningar ska vara falsk

### Plasma ramverk

- [ModelContextMenu] Använd Instantiator istället för Repeater-and-reparent
- [Calendar] Krymp och dra ihop veckonamn som görs med dagdelegering (fel 378020)
- [Icon Item] Låt egenskapen "smooth" faktiskt göra något
- Ställ in implicit storlek från källstorlek för image/SVG webbadresskällor
- Lägg till en ny egenskap i omgivning, för ett redigeringsläge
- Korrigera maskRequestedPrefix när inget prefix används (fel 377893)
- [Menu] Harmonisera placering av openRelative
- De flesta (sammanhangsberoende) menyer har acceleratorer (genvägar med Alt+bokstav) nu (fel 361915)
- Plasma-kontroller baserade på QtQuickControls2
- Hantera applyPrefixes med en tom sträng (fel 377441)
- Ta verkligen bort gamla temacacher
- [Containment Interface] Visa sammanhangsberoende menyer när tangenten "Menu" används
- [Breeze Plasma Theme] Förbättra ikoner för åtgärdsöverlagring (fel 376321)

### Syntaxfärgläggning

- TOML: Rätta färgläggning av strängundantagssekvenser
- Uppdatera Clojure-syntaxfärgläggning
- Några få uppdateringar av OCaml-syntax
- Färglägg *.sbt-filer som scala-kod
- Använd också QML-färgläggning för .qmltypes-filer

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
