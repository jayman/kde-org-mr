---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Kontrollera om en webbadress är en lokal fil på ett riktigt sätt

### Baloo

- Kompileringsrättningar för Windows

### Breeze-ikoner

- Många nya åtgärds- och programikoner.
- Ange erbjudna filändelser enligt ändring i kiconthemes

### Extra CMake-moduler

- Android-driftsättning: Stöd projekt utan någonting i share eller lib/qml (fel 362578)
- Aktiverar KDE_INSTALL_USE_QT_SYS_PATHS om CMAKE_INSTALL_PREFIX Qt5-prefix
- ecm_qt_declare_logging_category: Förbättra felmeddelande vid användning utan att inkludera

### Integrering med ramverk

- Ta bort insticksprogrammet platformtheme eftersom det finns i plasma-integration

### KCoreAddons

- Tillhandahåll ett sätt att inaktivera användning av inotify i KDirWatch
- Rätta KAboutData::applicationData() att initieras från aktuell Q*Application metadata
- Klargör att KRandom inte rekommenderas för kryptografiska ändamål

### KDBusAddons

- KDBusService: Ändra '-' till '_' i objektsökvägar

### KDeclarative

- Krascha inte om det inte finns något openGL-sammanhang

### Stöd för KDELibs 4

- Tillhandahåll en MAXPATHLEN i reserv om ej definierad
- Rätta KDateTime::isValid() för ClockTime-värden (fel 336738)

### KDocTools

- Tillägg av enhet program

### KFileMetaData

- Sammanfoga gren 'externalextractors'
- Rättade externa insticksprogram och tester
- Tillägg av stöd för externa skrivinsticksprogram
- Tillägg av stöd för skrivinsticksprogram
- Tillägg av stöd för externt extraheringsinsticksprogram

### KHTML

- Implementera toString för Uint8ArrayConstructor och liknande
- Sammanfoga flera Coverity-relaterade rättningar
- Använd QCache::insert på ett riktigt sätt
- Rätta några minnesläckor
- Rimlighetskontrollera tolkning av CSS-webbteckensnitt, undvik potentiell minnesläcka
- dom: Lägg till taggprioriteter för taggen 'comment'

### KI18n

- libgettext: Rätta potentiell användning efter frigöring med andra kompilatorer än g++

### KIconThemes

- Använd lämplig omgivning för fält av interna pekare
- Lägg till möjlighet att minska onödig åtkomst av disk, introducerar KDE-Extensions
- Spara viss åtkomst av disk

### KIO

- kurlnavigatortoolbutton.cpp: Använd buttonWidth i paintEvent()
- Ny filmeny: Filtrera bort dubbletter (t.ex. mellan .qrc och systemfiler) (fel 355390)
- Rätta felmeddelanden vid start för inställningsmodulen av kakor
- Ta bort kmailservice5, den kan bara göra skada vid detta tillfälle (fel 354151)
- Rätta KFileItem::refresh() för symboliska länkar. Felaktig storlek, filtyp och rättigheter tilldelades.
- Rätta regression i KFileItem: refresh() som gjorde att filtypen gick förlorad, så att en katalog blev till en fil (fel 353195)
- Ange text i den grafiska komponenten QCheckbox, istället för att använda en separat etikett (fel 245580)
- Aktivera inte den grafiska komponenten för ACL-rättigheter om filen inte ägs (fel 245580)
- Rätta dubbla snedstreck i resultat från KUriFilter när ett namnfilter används
- KUrlRequester: Lägg till signalen textEdited (vidarebefordrad från QLineEdit)

### KItemModels

- Rätta mallsyntax för testfallsgenerering
- Rätta länkning med Qt 5.4 (felaktigt placerad #endif)

### KParts

- Rätta layout av dialogrutan BrowserOpenOrSaveQuestion

### KPeople

- Lägg till en kontroll av att PersonData är giltig

### Kör program

- Rätta metainfo.yaml: KRunner är varken ett verktyg för konverteringshjälp eller avråds från

### KService

- Ta bort för restriktiv maximal stränglängd i KSycoca-databas

### KTextEditor

- Använd riktig teckensyntax '"' istället för '"'
- doxygen.xml: Använd också standardstilen dsAnnotation för "Egna etiketter"(färre hårdkodade färger)
- Lägg till alternativ för att visa ordräknaren
- Förbättrad kontrast av förgrundsfärg för markeringar av sök och ersätt
- Rätta krasch när Kate stängs via D-Bus när utskriftsdialogrutan visas (fel nr. 356813) (fel 356813)
- Cursor::isValid(): Lägg till anmärkning om isValidTextPosition()
- Lägg till API {Cursor, Range}::{toString, static fromString}

### KUnitConversion

- Informera klienten om konverteringsförhållandet inte är känt
- Lägg till valutan ILS (Israeli New Shekel) (fel 336016)

### Ramverket KWallet

- Inaktivera återställning av session för kwalletd5

### KWidgetsAddons

- KNewPasswordWidget: Ta bort storlekstips för distans, vilket alltid ledde till visst tomt utrymme i layouten.
- KNewPasswordWidget: Rätta QPalette när den grafiska komponenten är inaktiverad

### KWindowSystem

- Rätta generering av sökväg till XCB-insticksprogram

### Plasma ramverk

- [QuickTheme] Rätta egenskaper
- highlight/highlightedText från riktig färggrupp
- ConfigModel: Försök inte lösa upp tom källsökväg från paket
- [calendar] Visa bara händelsemarkering på dagrutmönstret, inte månad eller år
- declarativeimports/core/windowthumbnail.h: Rätta varningen -Wreorder
- Läs in ikontema igen på ett riktigt sätt
- Skriv alltid temanamnet till plasmarc, också när standardtemat väljes
- [calendar] Lägg till en markering för dagar som innehåller en händelse
- Lägg till Positiva, Neutrala och Negativa textfärger
- ScrollArea: Rätta varning när contentItem inte är bläddringsbar
- Lägg till en egenskap och metod för att justera menyn mot hörnet på dess visuella överliggande komponent
- Tillåt inställning av en minimal bredd för Menu
- Behåll ordning i lagrad objektlista
- Utöka programmeringsgränssnitt så att menyalternativ kan positioneras (om) vid procedurbaserad infogning
- Bind highlightedText färg i Plasma::Theme
- Rätta frånkoppling av tillhörande program eller webbadresser för Plasma::Applets
- Exponera inte symboler för den privata klassen DataEngineManager
- Lägg till elementet "event" i kalenderns SVG
- SortFilterModel: Invalidera filter när filtrets återanrop ändras

### Sonnet

- Installera verktyget parsetrigrams tool för korskompilering
- hunspell: Läs in och lagra en personlig ordlista
- Stöd för hunspell 1.4
- configwidget: Underrätta om ändrad inställning när ignorerade ord uppdateras
- settings: Spara inte inställningen omedelbart när ignoreringslistan uppdateras
- configwidget: Rätta att spara när ignorerade ord uppdateras
- Rätta problem med misslyckande att spara ignorerat ord (fel 355973)

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
