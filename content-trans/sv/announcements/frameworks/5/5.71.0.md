---
aliases:
- ../../kde-frameworks-5.71.0
date: 2020-06-06
layout: framework
libCount: 70
---
### Baloo

- Lagra bara filnamnstermer en gång

### Breeze-ikoner

- Förbättra noggrannhet för batteriprocent ikoner
- Rätta KML mimetype ikon
- Ändra musikon så den har bättre kontrast i mörka teman (fel 406453)
- Kräv i källkodsbygge (fel 421637)
- Lägg till 48 bildpunkters platsikoner (fel 421144)
- Lägger till en saknad genväg till LibreOffice ikon

### Extra CMake-moduler

- [android] Använd nyare Qt-version i exempel
- [android] Tillåt att APK installationsplats anges
- ECMGenerateExportHeader: lägg till generering av *_DEPRECATED_VERSION_BELATED()
- ECMGeneratePriFile: rättning för att ECM_MKSPECS_INSTALL_DIR är absolut
- ECMGeneratePriFile: gör pri-filer relokerbara
- Undertryck varning om missanpassat namn för paketet find_package_handle_standard_args

### KDE Doxygen-verktyg

- Använd normalt filen logo.png som logotyp, om den finns
- Ändra public_example_dir till public_example_dirs, för att möjliggöra flera kataloger
- Kasta kod för Python2-stöd
- Anpassa länkar till arkiv till invent.kde.org
- Rätta länk till kde.org impressum
- Historiksidor för KDE4 blev infogade i den generella historiksidan
- Laga generering med beroendediagram med Python 3 (ta sönder Py2)
- Exportera metalista till json-fil

### KAuth

- Använd ECMGenerateExportHeader för att bättre hantera programmeringsgränssnitt som avråds från

### KBookmarks

- Använd användargränssnittets markörsammanhang i fler tr() anrop
- [KBookmarkMenu] Tilldela m_actionCollection tidigt för att undvika krasch (fel 420820)

### KCMUtils

- Lägg till X-KDE-KCM-argument som egenskap, läs egenskap vid modulinläsning
- Rätta krasch vid inläsning av ett externt inställningsmodulprogram såsom yast (fel 421566)
- KSettings::Dialogruta: undvik duplicerade poster som använder $XDG_DATA_DIRS i kaskad

### KCompletion

- Omge också dokumentation av programmeringsgränssnitt med KCOMPLETION_ENABLE_DEPRECATED_SINCE, använd per metod
- Använd användargränssnittets markörsammanhang i fler tr() anrop

### KConfig

- Försök inte initiera SaveOptions uppräkningsvärde som avråds från
- Lägg till KStandardShortcut::findByName(const QString&amp;) och avråd från find(const char*)
- Rätta KStandardShortcut::find(const char*)
- Justera namn på internt exporterad metod som föreslås i D29347
- KAuthorized: exportera metod för att läsa in restriktioner igen

### KConfigWidgets

- [KColorScheme] Ta bort duplicerad kod
- Gör så att Header färger återgård till Windows färger först
- Introducera Header färguppsättningen

### KCoreAddons

- Rätta Fel 422291 - Förhandsgranska XMPP webbadresser i KMail (fel 422291)

### KCrash

- Anropa inte gstring landsanpassade saker i kritisk sektion

### KDeclarative

- Skapa funktionerna kcmshell.openSystemSettings() och kcmshell.openInfoCenter()
- Konvertera KKeySequenceItem till QQC2
- Rätta justering av underliggande objekt i GridViewInternal

### KDED

- Rätta suddiga ikoner i namnlistens programmeny genom att lägga till flaggan UseHighDpiPixmaps
- Lägg till systemd användartjänstfil för kded

### KFileMetaData

- [TaglibExtractor] Lägg till stöd för Audible "Enhanced Audio" ljudböcker
- ta hänsyn till flaggan extractMetaData

### KGlobalAccel

- Rätta fel med komponenter som innehåller specialtecken (fel 407139)

### KHolidays

- Uppdatera Taiwanesiska helger
- holidayregion.cpp - tillhandahåll översättningsbara strängar för Tyska områden
- holidays_de-region - ange namnfält för alla Tyska helgfiler
- holiday-de-&lt;region&gt; - där region är ett område i Tyskland
- Lägg till helgfil för DE-BE (Tyskland/Berlin)
- holidays/plan2/holiday_gb-sct_en-gb

### KImageFormats

- Lägg till några rimlighets- och gränskontroller

### KIO

- Introducera KIO::OpenUrlJob, en omskrivning och ersättning av KRun
- KRun: avråd från alla statiska 'run*' metoder, med fullständiga konverteringsinstruktioner
- KDesktopFileActions: avråd från run/runWithStartup, använd OpenUrlJob istället
- Sök efter kded som körtidsberoende
- [kio_http] Tolka FullyEncoded QUrl sökväg med TolerantMode (fel 386406)
- [DelegateAnimationHanlder] Ersätt QLinkedList som avråds från med QList
- RenameDialog: Varna när filstorlekar inte är likadana (fel 421557)
- [KSambaShare] Kontrollera att både smbd och testparm är tillgängliga (fel 341263)
- Places: Använd Solid::Device::DisplayName för DisplayRole (fel 415281)
- KDirModel: rätta regression i hasChildren() för träd med filer som visas (fel 419434)
- file_unix.cpp: när ::rename används som villkor, jämför returvärdet med -1
- [KNewFileMenu] Tillåt att skapa en katalog som heter '~' (fel 377978)
- [HostInfo] Ange QHostInfo::HostNotFound när en värddator inte hittas i DNS cachen (fel 421878)
- [DeleteJob] Rapportera slutliga värden efter avslutning (fel 421914)
- KFileItem: landsanpassa timeString (fel 405282)
- [StatJob] Gör så att mostLocalUrl ignorerar fjärrwebbadresser (ftp, http, etc.) (fel 420985)
- [KUrlNavigatorPlacesSelector] uppdatera bara en gång vid ändringar (fel 393977)
- [KProcessRunner] Använd bara körbart namn för räckvidd
- [knewfilemenu] Visa varning på plats när objekt skapas med inledande eller efterföljande mellanslag (fel 421075)
- [KNewFileMenu] Ta bort redundant slot-parameter
- ApplicationLauncherJob: visa dialogrutan Öppna med om ingen tjänst skickas med
- Säkerställ att programmet finns och är körbart innan vi injicerar kioexec/kdesu/etc
- Rätta webbadress skickad som argument när en .desktop-fil startas (fel 421364)
- [kio_file] Hantera namnbyte av fil 'A' till 'a' på FAT32 filsystem
- [CopyJob] Kontrollera om målkatalog är en symbolisk länk (fel 421213)
- Rätta att tjänstfil som anger 'Kör i terminal' ger felkod 100 (fel 421374)
- kio_trash: lägg till stöd för namnbyte av kataloger i cachen
- Varna om info.keepPassword inte är inställt
- [CopyJob] Kontrollera ledig plats för fjärrwebbadresser innan kopiering och andra förbättringar (fel 418443)
- [kcm trash] Ändra papperskorgens storleksprocent till två decimaler i inställningsmodul
- [CopyJob] Bli av med gammal TODO och använd QFile::rename()
- [LauncherJobs] Skicka beskrivning

### Kirigami

- Lägg till sektionshuvuden i sidorad när alla åtgärderna är expanderbara
- rättning: Vaddering i huvud för överlagringsblad
- Använd bättre förvald färg för Global Drawer när använd som Sidebar
- Rätta felaktig kommentar om hur GridUnit bestäms
- Tillförlitligare klättring av överliggande träd för PageRouter::pushFromObject
- PlaceholderMessage beror på Qt 5.13 även om CMakeLists.txt kräver 5.12
- introducera gruppen Header
- Spela inte stängningsanimering för close() om bladet redan är stängt
- Lägg till komponenten SwipeNavigator
- Introducera komponenten Avatar
- Rätta initiering av skuggningsresurs för statiska byggen
- AboutPage: Lägg till verktygstips
- Säkerställ closestToWhite och closestToBlack
- AboutPage: Lägg till knappar för e-post, webbadress
- Använd alltid färguppsättningen Window för AbstractApplicationHeader (fel 421573)
- Introducera ImageColors
- Rekommendera bättre beräkning av bredd för PlaceholderMessage
- Förbättra programmeringsgränssnitt för PageRouter
- Använd litet teckensnitt för BasicListItem undertitel
- Lägg till stöd för lager i PagePoolAction
- Introducera kontrollen RouterWindow

### KItemViews

- Använd användargränssnittets markörsammanhang i fler tr() anrop

### KNewStuff

- Duplicera inte felmeddelanden i en passiv underrättelse (fel 421425)
- Rätta felaktiga färger i KNS Quick meddelanderuta (fel 421270)
- Markera inte post som avinstallerad om avistallationsskriptet misslyckas (fel 420312)
- KNS: Avråd från metoden isRemote och hantera tolkningsfel riktigt
- KNS: Markera inte post som installerad om installationsskriptet misslyckades
- Rätta att visa uppdateringar när alternativet markeras (fel 416762)
- Rätta uppdatering av automatisk markering (fel 419959)
- Lägg till stöd för KPackage i KNewStuffCore (fel 418466)

### KNotification

- Implementera kontroll av synlighet för låsskärm på Android
- Implementera gruppering av underrättelser på Android
- Visa rich text underrättelsemeddelanden på Android
- Implementera webbadresser genom att använda tips
- Använd användargränssnittets markörsammanhang i fler tr() anrop
- Implementera stöd för underrättelsevikt på Android
- Ta bort kontrollera av underrättelsetjänst och reserv i KPassivePopup

### KQuickCharts

- Enklare avrundningskontroll för bakgrund
- PieChart: Återge ett torus-segment som bakgrund när det inte är en hel cirkel
- PieChart: Lägg till en hjälpfunktion som tar hand om att avrunda torus-segment
- PieChart: Exponera fromAngle/toAngle till skuggning
- Återge alltid cirkeldiagrammets bakgrund
- Ställ alltid in bakgrundsfärg, även om toAngle != 360
- Tillåt start/slut att vara minst 2 bildpunkter från varandra
- Omarbeta implementeringen av PieChart
- Rätta och kommentera funktionen sdf_torus_segment sdf
- Ange inte explicit utjämningsvärde när sdf:er återges
- Provisorisk lösning för avsaknad av fältfunktionsparametrar i GLES2-skuggningar
- Hantera skillnader mellan kärnprofiler och föråldrade profiler i linje- och cirkelskuggningar
- Rätta ett fåtal valideringsfel
- Uppdatera valideringsskriptet för skuggningar med den nya strukturen
- Lägg till ett separat huvud för GLE3-skuggningar
- Ta bort duplicerade skuggningar för kärnprofil, använd istället preprocessor
- exponera både name och shortName
- stöd olika långa/korta beteckningar (fel 421578)
- skydda modell bakom en QPointer
- Lägg till MapProxySource

### Kör program

- Låt inte Kör program vitlista bli bestående i inställning
- Rätta signalerna prepare/teardown för Kör program (fel 420311)
- Detektera lokala filer och kataloger som börjar med ~

### KService

- Lägg till X-KDE-DBUS-Restricted-Interfaces för Application skrivbordspostfält
- Lägg till saknad etikett för kompilatoravrådan för konstruktorn KServiceAction med fem argument

### KTextEditor

- Lägg till .diff i file-changed-diff för att aktivera Mime-detektering på Windows
- rullningslist miniavbildning: prestanda: fördröj uppdatering för inaktiva dokument
- Gör så att text alltid justeras till teckensnittets baslinje
- Återställ "Lagra och hämta fullständig vyinställning i och från sessionsinställning"
- Rätta ändrad radmarkör i kate miniavbildning

### KWidgetsAddons

- Var högljudd om att avråda från KPageWidgetItem::setHeader(tom sträng som inte är null)

### KXMLGUI

- [KMainWindow] Anropa QIcon::setFallbackThemeName (senare) (fel 402172)

### KXmlRpcClient

- Markera KXmlRpcClient som konverteringshjälp

### Plasma ramverk

- PC3: Lägg till avskiljare i verktygsrad
- stöd färggruppen Header
- Implementera rullnings- och dragjustering av värden för SpinBox-kontroll
- Använd font: istället för font.pointSize: om möjligt
- kirigamiplasmastyle: Lägg till implementering av AbstractApplicationHeader
- Undvik potentiell bortkoppling av alla signaler i IconItem (fel 421170)
- Använd litet teckensnitt för ExpandableListItem undertitel
- Lägg till smallFont i Kirigami plasma-stil
- [Plasmoid Heading] Rita bara huvud när det finns SVG i temat

### QQC2StyleBridge

- Lägg till ToolSeparator stiländring
- Ta bort "pressed" från CheckIndicator "on" tillstånd (fel 421695)
- Stöd gruppen Header
- Implementera smallFont i Kirigami-insticksprogram

### Solid

- Lägg till en QString Solid::Device::displayName, använd i enheten Fstab för nätverksmontering (fel 415281)

### Sonnet

- Tillåt överskrida för att inaktivera automatisk språkdetektering (fel 394347)

### Syntaxfärgläggning

- Uppdatera Raku-utökningar i Markdown-block
- Raku: rätta inhägnade kodblock i Markdown
- Tilldela "Identifier" egenskap till öppnar dubbla citationstecken istället av "Comment" (fel 421445)
- Bash: rätta kommentarer efter undantag (fel 418876)
- LaTeX: rätta vikning för end{...} och för områdesmarkeringar BEGIN-END (fel 419125)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
