---
aliases:
- ../../kde-frameworks-5.65.0
date: 2019-12-14
layout: framework
libCount: 70
qtversion: 5.12
---
Ny modul: KQuickCharts, en QtQuick-modul som tillhandahåller diagram med hög prestanda

### Breeze-ikoner

- Bildpunktsjustera färgväljare
- Lägg till nya ikoner för baloo
- Lägg till nya sökikoner för preferenser
- Använd en pipett för färgväljarikoner (fel 403924)
- Lägg till kategoriikon "all applications"

### Extra CMake-moduler

- Städning av EBN extra-cmake-modules överföring
- ECMGenerateExportHeader: lägg till flaggan NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE
- Använd explicit bibliotek för systemd-kataloger
- Lägg till installationskatalog för systemd-enheter
- KDEFrameworkCompilerSettings: aktivera alla Qt och KF avrådningsvarningar

### Integrering med ramverk

- Ange SH_ScrollBar_LeftClickAbsolutePosition villkorligt baserat på inställning av kdeglobals (fel 379498)
- Ange programnamn och version i verktyget knshandler

### KDE Doxygen-verktyg

- Rätta import av moduler med Python3

### KAuth

- Installera .pri-fil för KAuthCore

### KBookmarks

- Avråd från KonqBookmarkMenu och KonqBookmarkContextMenu
- Flytta klasser som bara används av KonqBookmarkMenu tillsammans med den

### KCalendarCore

- Återgå till systemets tidszon när kalender skapas med en ogiltig sådan
- Memory Calendar: undvik duplicering av kod
- Använd QDate som nyckel i mIncidencesForDate i MemoryCalendar
- Hantera förekomster i olika tidszoner i MemoryCalendar

### KCMUtils

- [KCMultiDialog] Ta bort det mesta av den speciella marginalhanteringen. Den görs nu i KPageDialog
- KPluginSelector: använd ny KAboutPluginDialog
- Lägg till skydd för saknad kirigami (fel 405023)
- Inaktivera knappen för att återställa förval om KCModule säger det
- Låt KCModuleProxy ta hand om det förvalda tillståndet
- Gör så att KCModuleQml överensstämmer med signalen defaulted()

### KCompletion

- Strukturera om KHistoryComboBox::insertItems

### KConfig

- Dokumentera inställning av Notifiers
- Skapa bara en sessionsinställning när en session faktiskt återställs
- kwriteconfig: lägg till borttagningsalternativ
- Lägg till KPropertySkeletonItem
- Förbered KConfigSkeletonItem att tillåta att dess privata klass ärvs

### KConfigWidgets

- [KColorScheme] Gör så att ordningen för dekorationsfärger matchar uppräkningstypen DecorationRole
- [KColorScheme] Rätta misstag i NShadeRoles kommentar
- [KColorScheme/KStatefulBrush] Byt ut hårdkodade nummer mot uppräkningsvärden
- [KColorScheme] Lägg till objekt i ColorSet och Role uppräkningstyper för det totala antalet objekt
- Registrera KKeySequenceWidget i KConfigDialogManager
- Justera KCModule att också kanalisera information om förval

### KCoreAddons

- Avråd från KAboutData::fromPluginMetaData, nu när KAboutPluginDialog finns
- Lägg till en beskrivande varning när inotify_add_watch returnerade ENOSPC (fel 387663)
- Lägg till test för felet "fel-414360" det är inte ett ktexttohtml-fel (fel 414360)

### KDBusAddons

- Inkludera programmeringsgränssnitt för att implementera --replace argument generellt

### KDeclarative

- EBN kdeclarative städning av överföringsprotokoll
- Anpassa till ändring i KConfigCompiler
- gör sidhuvud och sidfot synliga när de får innehåll
- stöd qqmlfileselectors
- Tillåt att beteendet för att spara automatiskt inaktiveras i ConfigPropertyMap

### KDED

- Ta bort beroende på kdeinit från kded

### Stöd för KDELibs 4

- ta bort använd kgesturemap från kaction

### KDocTools

- Catalan Works: Lägg till saknade poster

### KI18n

- Sluta avråda från I18N_NOOP2

### KIconThemes

- Avråd från toppnivåmetoden UserIcon, används inte längre

### KIO

- Lägg till nytt protokoll för 7z-arkiv
- [CopyJob] Ta också hänsyn till https för ikonen text-html vid länkning 
- [KFileWidget] Undvik att anropa slotOk direkt efter webbadressen ändrades (fel 412737)
- [kfilewidget] Läs in ikoner enligt namn
- KRun: överskrid inte användarens föredragna program när lokal *.*html och co.-filer öppnas (fel 399020)
- Reparera FTP/HTTP-proxy frågor i fallet med ingen proxy
- Ftp I/O-slav: Rätta skicka ProxyUrls parameter
- [KPropertiesDialog] tillhandahåll ett sätt att visa målet för en symbolisk länk (fel 413002)
- [I/O-slav remote] Lägg till visningsnamn i remote:/ (fel 414345)
- Rätta inställningar av HTTP-proxy (fel 414346)
- [KDirOperator] Lägg till genvägen Bakstreck för åtgärden tillbaka
- När kioslave5 inte kunde hittas i libexec-liknande platser, prova $PATH
- [Samba] Förbättra varningsmeddelande om netbios-namn
- [DeleteJob] Använd en separat arbetstråd för att köra själva I/O-operationen (fel 390748)
- [KPropertiesDialog] Gör också skapad datumsträng valbar av musen (fel 413902)

Avråd från:

- Avråd från KTcpSocket och KSsl* klasser
- Ta bort de sista spåren av KSslError från TCPSlaveBase
- Konvertera ssl_cert_errors metadata från KSslError till QSslError
- Avråd från KTcpSocket överlagring av KSslErrorUiData konstruktor
- [http I/O-slav] använd QSslSocket istället för KTcpSocket (avråds från)
- [TcpSlaveBase] konvertera från KTcpSocket (avråds från) till QSslSocket

### Kirigami

- Rätta marginaler för ToolBarHeader
- Krascha inte när ikonens källa är tom
- MenuIcon: rätta varningar när lådan inte är initierad
- Ta hänsyn till en mnemonisk beteckning för att gå tillbaka till ""
- Rätta att InlineMessage åtgärder alltid placeras i överflödesmeny
- Rätta förvald kortbakgrund (fel 414329)
- Ikon: lös trådhanteringsproblem när källan är http
- rättningar av tangentbordsnavigering
- i18n: extrahera också meddelanden från C++ källor
- Rätta cmake projekt kommandoposition
- Gör QmlComponentsPool till en instans per gränssnitt (fel 414003)
- Ändra ToolBarPageHeader så att beteendet för att dra ihop ikoner från ActionToolBar används
- ActionToolBar: Byt automatiskt till enbart ikon för åtgärder markerade som KeepVisible
- Lägg till en egenskap displayHint i Action
- lägg till dbus-gränssnittet i den statiska versionen
- Återställ "ta hänsyn till draghastighet när en bläddring slutar"
- FormLayout: Rätta beteckningshöjd om bredd läge är falskt
- visa inte normalt greppet om inte fast läge
- Återställ "Säkerställ att GlobalDrawer topContent alltid förblir överst"
- Använd en RowLayout för layout av ToolBarPageHeader
- Centrera vänstra åtgärder vertikalt i ActionTextField (fel 413769)
- återställ dynamisk övervakning av surfplatteläge
- ersätt SwipeListItem
- stöd egenskapen actionsVisible
- starta SwipeListItem konvertering till SwipeDelegate

### KItemModels

- Avråd från KRecursiveFilterProxyModel
- KNumberModel: hantera stepSize 0 snyggt
- Exponera KNumberModel för QML
- Lägg till ny klass KNumberModel som är en modell av tal mellan två värden
- Exponera KDescendantsProxyModel för QML
- Lägg till import av qml för KItemModels

### KNewStuff

- Lägg till några vänliga länkar "rapportera fel här"
- Rätta i18n-syntax för att undvika körtidsfel (fel 414498)
- Omvandla KNewStuffQuick::CommentsModel till en SortFilterProxy för granskningar
- Ställ in i18n-argument korrekt på en gång (fel 414060)
- Funktionerna är @since 5.65, inte 5.64
- Lägg till OBS i skärminspelningar (fel 412320)
- Rätta några felaktiga länkar, uppdatera länkar till https://kde.org/applications/
- Rätta översättningar av $GenericName
- Visa en upptagetindikering "Loading more..." när vydata läses in
- Ge lite mer snygg återmatning i NewStuff::Page medan Engine laddas (fel 413439)
- Lägg till en överlagringskomponent för återmatning av objektaktivitet (fel 413441)
- Visa bara DownloadItemsSheet om det finns mer än ett nerladdningsobjekt (fel 413437)
- Använd pekande hand markören för enkelklickbara delegater (fel 413435)
- Rätta huvudlayouter för komponenterna EntryDetails och Page (fel 413440)

### KNotification

- Gör så att dokumenten reflekterar att setIconName ska föredras över setPixmap om möjligt
- Sökväg för dokumentinställningsfil på Android

### KParts

- Markera att egenskapen BrowserRun::simpleSave avråds från
- BrowserOpenOrSaveQuestion: flytta AskEmbedOrSaveFlags uppräkningstyp från BrowserRun

### KPeople

- Tillåt att utlösa sortering från QML

### kquickcharts

Ny modul. Modulen Quick Charts tillhandahåller en uppsättning diagram som kan användas från QtQuick-program. De är avsedda att användas för både enkel visning av data samt kontinuerlig datavisning med hög volym (ofta kallad plotter). Diagrammen använder ett system som kallas avståndsfält för sin accelererade återgivning, vilket tillhandahåller sätt att använda grafikprocessorn för att återge tvådimensionella former utan kvalitetsförlust.

### KTextEditor

- KateModeManager::updateFileType(): validera lägen och läs in statusradens meny igen
- Verifiera lägen i sessionens inställningsfil
- LGPLv2+ after ok av Svyatoslav Kuzmich
- återställ förformatering av filer

### KTextWidgets

- Avråd från kregexpeditorinterface
- [kfinddialog] Ta bort användning av kregexpeditor insticksprogramsystem

### Kwayland

- [server] Äg inte implementering av dmabuf
- [server] Gör dubbelbuffrade egenskaper i xdg-shell dubbelbuffrade

### KWidgetsAddons

- [KSqueezedTextLabel] Lägg till ikon för åtgärden "Kopiera hela texten"
- Förena KPageDialog marginalhantering in i själva KPageDialog (fel 413181)

### KWindowSystem

- Justera antal efter tillägg av _GTK_FRAME_EXTENTS
- Lägg till stöd för _GTK_FRAME_EXTENTS

### KXMLGUI

- Ta bort oanvänt och felaktigt stöd för KGesture
- Lägg till KAboutPluginDialog, att användas med KPluginMetaData
- Tillåt också att återställningslogik för session när program startas manuellt (fel 413564)
- Lägg till saknad egenskap i KKeySequenceWidget

### Oxygen-ikoner

- Skapa symbolisk länk för microphone till audio-input-microphone på alla storlekar (fel 398160)

### Plasma ramverk

- flytta hantering av bakgrundstips i miniprogram
- använd filväljaren i interceptorn
- mer användning av ColorScope
- övervaka också fönsterändringar
- stöd för att användare tar bort bakgrund och automatisk skugga
- stöd filväljare
- stöd qml-filväljare
- ta bort förflugna qgraphicsview grejer
- ta inte bort och skapa om wallpaperinterface om det inte behövs
- MobileTextActionsToolBar kontrollera om controlRoot är odefinierad innan den används
- Lägg till hideOnWindowDeactivate i PlasmaComponents.Dialog

### Syfte

- inkludera cmake-kommando vi just ska använda

### QQC2StyleBridge

- [TabBar] Använd fönsterfärg istället för knappfärg (fel 413311)
- bind aktiverade egenskaper till vyn som är aktiverade
- [ToolTip] Bastidsgräns för textlängd
- [ComboBox] Gör inte meddelanderuta dämpad
- [ComboBox] Indikera inte fokus när meddelanderuta är öppen
- [ComboBox] Följ focusPolicy

### Solid

- [udisks2] rätta ändringsdetektering av media för externa optiska enheter (fel 394348)

### Sonnet

- Inaktivera ispell gränssnitt med mingw
- Implementera ISpellChecker-gränssnitt för Windows ≥ 8
- Grundläggande stöd för korskompilering för parsetrigrams
- inbädda trigrams.map i delat bibliotek

### Syndikering

- Rätta fel 383381 - Hämta kanalwebbadressen från en YouTube-kanal fungerar inte längre (fel 383381)
- Extrahera kod så vi kan rätta tolkningskod (fel 383381)
- atom har stöd för ikoner (så vi kan använda en specifik ikon i akregator)
- Konvertera som ett verkligt qtest-program

### Syntaxfärgläggning

- Uppdateringar från CMake 3.16 slutlig utgåva
- reStructuredText: Rätta litteraler på plats som färglägger föregående tecken
- rst: Lägg till stöd för fristående hyperlänkar
- JavaScript: flytta nyckelord från TypeScript och andra förbättringar
- JavaScript/TypeScript React: byt namn på syntaxdefinitioner
- Latex: rätta bakstrecksavgränsare i vissa nyckelord (fel 413493)

### ThreadWeaver

- Använd webbadress med transportkryptering

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
