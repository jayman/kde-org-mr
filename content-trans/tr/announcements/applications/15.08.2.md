---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE, KDE Uygulamaları 15.08.2'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 15.08.2'yi Gönderdi
version: 15.08.2
---
October 13, 2015. Today KDE released the second stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen 30'dan fazla hata düzeltmesi arasında ark, gwenview, kate, kbruch, kdelibs, kdepim, lokalize ve umbrello için iyileştirmeler bulunuyor.

This release also includes Long Term Support version of KDE Development Platform 4.14.13.
