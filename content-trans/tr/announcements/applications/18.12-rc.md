---
aliases:
- ../announce-applications-18.12-rc
date: 2018-11-30
description: KDE Uygulamalar 18.12  Sürüm Adayını Gönderdi.
layout: application
release: applications-18.11.90
title: KDE, KDE Uygulamalar 18.12 Sürüm Adayını Gönderdi
version_number: 18.11.90
version_text: 18.12 Release Candidate
---
30 Kasım 2018. Bugün KDE, KDE Uygulamalarının yeni sürümlerinin Sürüm Adayını yayınladı. Bağımlılık ve özellik donmalarıyla birlikte, KDE ekibinin odak noktası artık hataları düzeltmek ve daha fazla parlatmaktır.

Check the <a href='https://community.kde.org/Applications/18.12_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

The KDE Applications 18.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the Release Candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
