---
date: 2013-08-14
hidden: true
title: KDE-ohjelmistoalusta 4.11 parantaa suorituskykyä
---
KDE-ohjelmistoalusta 4 on ollut ominaisuusjäässä (englanniksi feature freeze) 4.9-julkaisusta alkaen. Tässäkin versiossa on siten vain virhekorjauksia ja suorituskykyparannuksia.

The Nepomuk semantic storage and search engine received massive performance improvements, such as a set of read optimizations that make reading data up to six times faster. Indexing has become smarter, being split in two stages. The first stage retrieves general information (such as file type and name) immediately; additional information like media tags, author information, etc. is extracted in a second, somewhat slower stage. Metadata display on newly-created or freshly-downloaded content is now much faster. In addition, the Nepomuk developers improved the backup and restore system. Last but not least, Nepomuk can now also index a variety of document formats including ODF and docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Dolphinin semanttiset ominaisuudet` width="600px">}}

Nepomukin optimoitu tiedon tallennusmuoto ja uudelleenkirjoitettu sähköposti-indeksoia vaatii, että osa kiintolevyn sisällöstä indeksoidaan uudelleen. Tästä johtuen indeksointi kuluttaa jonkin aikaa tavallista enemmän laskentatehoa riippuen uudelleenindeksoitavan sisällön määrästä. Nepomukin tietokanta päivitetään automaattisesti ensimmäisen kirjautumisen yhteydessä.

Ohjelmistoalustaan tehdyt pienet korjaukset <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>löytyvät git-lokista</a>.

#### KDE-kehitysalustan asentaminen

KDE-ohjelmat, mukaan lukien kaikki KDE:n kirjastot ja sovellukset, ovat ilmaisesti saatavilla avoimen lähdekoodin lisenssien mukaisesti. KDE-ohjelmia voi käyttää useilla laitekokoonpanoilla ja suoritinarkkitehtuureilla kuten ARMilla ja x86:lla, useissa käyttöjärjestelmissä, ja ne toimivat kaikenlaisten ikkunointiohjelmien ja työpöytäympäristojen kanssa. Linux- ja UNIX-pohjaisten käyttöjärjestelmien lisäksi useimmista KDE-sovelluksista on saatavilla Microsoft Windows -versiot <a href='http://windows.kde.org'>KDE-ohjelmat Windowsissa</a> -sivustolta ja Apple Mac OS X -versiot <a href='http://mac.kde.org/'>KDE-ohjelmat Macillä</a> -sivustolta. Joistain KDE-sovelluksista on verkossa kokeellisia versioita, jotka toimivat useilla mobiilialustoilla kuten Meegolla, Microsoftin Windows Mobilella ja Symbianilla, mutta niitä ei tällä hetkellä tueta. <a href='http://plasma-active.org'>Plasma Active</a> on käyttökokemus laajemmalle laitekirjolle kuten esimerkiksi tablettitietokoneille ja muille mobiililaitteille.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketit

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Pakettien sijainnit

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Järjestelmävaatimukset

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Tänään tiedotettiin myös seuraavista:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Valmistautuen pitkäaikaiseen ylläpitöön Plasma-työtilat parantaa perustoimintoja edelleen: julkaisussa on mukana jouhevampi tehtäväpalkki, älykkäämpi akkusovelma ja parannettu äänimikseri. KScreenin käyttöönotto tekee useiden näyttöjen hoitamisesta fiksumpaa, ja mittavat suorituskykyparannukset yhdessä pienten käytettävyysparannusten kanssa tekevät käyttökokemuksesta paremman.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

Tätä julkaisua leimaa runsaat parannukset KDE:n PIM-pinossa (henkilökohtaisten tietojen hallinnassa). Niiden ansiosta sen suorituskyky on nyt paljon parempi ja siinä on paljon uusia ominaisuuksia. Kate parantaa Python- ja Javascript-kehittäjien tuottavuutta uusilla liitännäisillä. Dolphin nopeutui, ja opetusohjelmissa on monia uusia ominaisuuksia.
