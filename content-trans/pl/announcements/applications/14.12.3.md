---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE wydało Aplikacje 14.12.3.
layout: application
title: KDE wydało Aplikacje KDE 14.12.3
version: 14.12.3
---
3 marca 2015. Dzisiaj KDE wydało trzecie uaktualnienie stabilizujące <a href='../14.12.0'>Aplikacji KDE 14.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Poza 19 zarejestrowanymi poprawkami błędów uwzględnia ulepszenia do gry Kanagram, modelera UML Umbrello, przeglądarki dokumentów Okular oraz aplikacji do geometrii globu Kig.

To wydanie zawiera także wersje o długoterminowym wsparciu: Przestrzeni Roboczych 4.11.17, Platformę Programistyczną KDE 4.14.6 oraz Pakiet Kontact 4.14.6 .
