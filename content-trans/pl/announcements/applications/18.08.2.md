---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE wydało Aplikacje KDE 18.08.2
layout: application
title: KDE wydało Aplikacje KDE 18.08.2
version: 18.08.2
---
11 października 2018. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../18.08.0'>Aplikacji KDE 18.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than a dozen recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, KCalc, Umbrello, among others.

Wśród ulepszeń znajdują się:

- Dragging a file in Dolphin can no longer accidentally trigger inline renaming
- KCalc again allows both 'dot' and 'comma' keys when entering decimals
- A visual glitch in the Paris card deck for KDE's card games has been fixed
