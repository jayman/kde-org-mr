---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE wydało Aplikacje KDE 18.08.1
layout: application
title: KDE wydało Aplikacje KDE 18.08.1
version: 18.08.1
---
6 wrzesień 2018. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../18.08.0'>Aplikacji KDE 18.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than a dozen recorded bugfixes include improvements to Kontact, Cantor, Gwenview, Okular, Umbrello, among others.

Wśród ulepszeń znajdują się:

- The KIO-MTP component no longer crashes when the device is already accessed by a different application
- Sending mails in KMail now uses the password when specified via password prompt
- Okular now remembers the sidebar mode after saving PDF documents
