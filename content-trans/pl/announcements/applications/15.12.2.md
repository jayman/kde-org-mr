---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE wydało Aplikacje KDE 15.12.2
layout: application
title: KDE wydało Aplikacje KDE 15.12.2
version: 15.12.2
---
16 luty 2015. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../15.12.0'>Aplikacji KDE 15.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 30 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark oraz umbrello.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.17.
