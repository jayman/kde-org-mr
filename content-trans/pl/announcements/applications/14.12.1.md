---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE wydało Aplikacje KDE 14.12.1.
layout: application
title: KDE wydało Aplikacje KDE 14.12.1
version: 14.12.1
---
4 stycznia 2015. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../14.12.0'>Aplikacji KDE 14.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 50 zarejestrowanych poprawek błędów uwzględnia ulepszenia do Ark, modelera UML Umbrello, przeglądarki dokumentów Okular, programu do nauki wymowy Artikulate i klienta zdalnego  pulpitu KRDC.

To wydanie zawiera także wersje o długoterminowym wsparciu: Przestrzeni Roboczych 4.11.15, Platformę Programistyczną KDE 4.14.4 oraz Pakiet Kontact 4.14.4 .
