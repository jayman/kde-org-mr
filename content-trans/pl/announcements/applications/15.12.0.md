---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE wydało Aplikacje 15.12.
layout: application
release: applications-15.12.0
title: KDE wydało Aplikacje KDE 15.12.0
version: 15.12.0
---
16 grudzień 2015. Dzisiaj KDE wydało Aplikacje KDE 15.12.

KDE z niecierpliwością zawiadamia o wydaniu Aplikacji KDE 15.12, uaktualnienia Aplikacji KDE na grudzień 2015. To wydanie dostarcza nowych aplikacji, funkcji, a także poprawki błędów w wielu aplikacjach. Zespół stara się zapewniać najlepszą jakość tych aplikacji, więc liczymy na twoją informację zwrotną.

W tym wydaniu uaktualniliśmy wiele aplikacji tak, aby były zgodne z nowymi<a href='https://dot.kde.org/2013/09/25/frameworks-5'>Szkieletami KDE 5</a>tj. <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> i więcej Gier KDE, nie wspominając już zestawu wtyczek KDE Image Plugin Interface i bibliotek, które je wspierają. Podwyższa to całkowitą liczbę aplikacji używających Szkieletów KDE 5 do 126.

### Widowiskowy nowy dodatek

Po 14 latach bycia częścią KDE, KSnapshot został został wysłany na emeryturę i zastąpiony nową aplikacją robiącą zrzuty ekranu, Spectacle.

Z nowymi możliwościami i całkowicie nowym interfejsem, Spectacle sprawia, że robienie zrzutów ekranu jest tak zintegrowane jakim jeszcze nie było. Poza tym co można było zrobić przy użyciu KSnapshot, Spectacle może robić kompozytowe zrzuty ekranu tj. menu podręczne wraz z nadrzędnymi oknami, lub robić zrzuty ekranu całego ekranu (lub obecnie aktywnego okna) bez konieczności uruchamiania Spectacle. Wystarczy używać skrótów klawiszowych Shift +PrintScreen oraz Meta+PrintScreen.

Zoptymalizowaliśmy także czas uruchomienia aplikacji, aby w pełni zminimalizować dziurę czasową pomiędzy uruchomieniem aplikacji i przechwyceniem obrazu.

### Polerowano wszędzie

Wiele aplikacji zostało znacznie udoskonalonych w tym cyklu, w dodatku do poprawek do stabilności i usuniętych błędów.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, nieliniowy edytor wideo, zyskał wiele w obszarze interfejsu użytkownika. Teraz możesz kopiować i wklejać elementy na twojej osi czasu i łatwo przełączać przezroczystość danej ścieżki. Kolory ikon samoczynnie dostosowują się do głównego wystroju interfejsu, czyniąc je łatwiejszymi w rozróżnianiu.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark może teraz wyświetlać komentarze ZIP`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, program do archiwów może teraz wyświetlać komentarze osadzone w archiwach ZIP i RAR. Ulepszyliśmy wsparcie dla znaków Unicode w nazwach plików w archiwach ZIP, od teraz możesz rozpoznawać uszkodzone archiwa i odzyskiwać z nich tyle danych ile się da. Możesz także otwierać zarchiwizowane pliki w ich domyślnych aplikacjach, poprawiliśmy także działanie przeciągnij-upuść na pulpit, a także podglądy plików XML.

### Tylko gry i zero pracy

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nowy ekran powitalny KSudoku (na Mac OS X)`>}}

Programiści Gier KDE pracowali ciężko przez ostatnie miesiące, aby zoptymalizować nasze gry pod kątem przyjemniejszych i bogatszych wrażeń. W tym obszarze przygotowaliśmy dla naszych użytkowników wiele nowych rzeczy.

W <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, dodaliśmy dwa nowe zestawy poziomów, jeden pozwalający na kopanie przy spadaniu i drugi bez tej możliwości. Dodaliśmy rozwiązania dla kilki istniejących zestawów poziomów. Dla niektórych starszych zestawów poziomów zwiększyliśmy poziom trudności i wyłączyliśmy możliwość kopania przy spadaniu.

W <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, teraz możesz drukować Mathdoku i Zabójcze Sudoku. Nowy układ wielokolumnowy na ekranie powitalnym KSudoku czyni łatwym przeglądanie dostępnych rodzajów łamigłówek; dodatkowo kolumny dopasowują się same, gdy rozmiar okna ulega zmianie. To samo wdrożyliśmy w Palapeli, tak aby można było widzieć więcej zestawów puzzli jigsaw na raz.

Wprowadziliśmy poprawki do stabilności w takich grach jak KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> oraz <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, przez co ogólne wrażenia uległy poprawie. KTuberling, Klickety oraz KNavalBattle zostały także uaktualnione do nowych Szkieletów KDE 5.

### Ważne poprawki

Czy nie denerwuje cię to, że twoja ulubiona aplikacja napotyka usterkę w najmniej odpowiedniej chwili? Nas także to denerwuje, więc pracowaliśmy ciężko, aby usunąć te błędy, lecz na pewno niektóre z nich jeszcze tam są, więc nie zapominaj o ich <a href='https://bugs.kde.org'>zgłoszeniu</a>!

W naszej przeglądarce plików <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, zwiększyliśmy stabilność, a także zwiększyliśmy płynność przewijania. W <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, poprawiliśmy kłopot z tekstem na białych tłach. W <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, usunęliśmy usterkę występującą przy zamykaniu, poza tym oczyściliśmy interfejs i dodaliśmy nowy moduł do czyszczenia pamięci podręcznej.

<a href='https://userbase.kde.org/Kontact'>Pakiet Kontact</a> ma tony nowych funkcji, wiele poprawek i optymalizacji wydajności. W rzeczywistości wprowadzono tyle zmian w tym cyklu, że zdecydowano o podniesieniu numeru wersji do 5.1. Zespół ciężko pracuje i czeka na twoją informację zwrotną na temat wyników tej pracy.

### Idziemy naprzód

Część naszych działań polega także na zmodernizowaniu naszej oferty, tak więc porzuciliśmy kilka aplikacji z Aplikacji KDE i nie będziemy ich wydawać z Aplikacjami KDE 15.12

Porzuciliśmy 4 aplikacje z tego wydania - Amor, KTux, KSnapshot oraz SuperKaramba. Jak wspomniano powyżej, KSnapshot został zastąpiony Spectacle, a Plazma właściwie zastępuje SuperKarambę jako silnik elementów interfejsu. Porzuciliśmy wolnostojące wygaszacze ekranu, ponieważ blokowanie ekranu odbywa się już inaczej na nowoczesnych pulpitach.

Porzuciliśmy także 3 pakiety oprawy graficznej (kde-base-artwork, kde-wallpapers oraz kdeartwork); ich zawartość nie zmieniła się przez bardzo długi czas.

### Pełny dziennik zmian
