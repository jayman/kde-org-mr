---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE wydało Aplikacje KDE 16.04.2
layout: application
title: KDE wydało Aplikacje KDE 16.04.2
version: 16.04.2
---
14 czerwca 2016. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../16.04.0'>Aplikacji KDE 16.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 25 zarejestrowanych poprawek błędów uwzględnia ulepszenia do akonadi, ark, artikulate, dolphin, kdenlive, kdepim oraz inne.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.21.
