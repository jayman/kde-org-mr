---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE publie la version 18.12 des applications de KDE.
layout: application
release: applications-18.12.0
title: KDE publie la version 18.12.0 des applications de KDE.
version: 18.12.0
---
{{% i18n_date %}}

Les applications de KDE 18.12 ont été publiées.

{{%youtube id="ALNRQiQnjpo"%}}

Nous travaillons de façon continue sur l'amélioration des logiciels dont la série d'applications de KDE. Nous espérons que vous trouverez toutes les nouvelles améliorations et les corrections de bogues utiles ! 

## Quoi de neuf dans les applications de KDE 18.12

Plus de 140 corrections de bogues ont été apportées dans les applications dont la suite Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello et bien d'autres !

### Gestion de fichiers

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, le puissant gestionnaire de fichiers de KDE :

- Nouvelle implémentation de MTP le rendant totalement utilisable pour la production
- Très important gain de performances lors de la lecture de fichiers par le protocole « sftp »
- Pour les aperçus par vignette, les cadres et les ombres sont maintenant dessinés pour les fichiers d'images sans aucune transparence, améliorant l'affichage des icônes.
- Nouveaux aperçus de vignettes pour les documents LibreOffice et les applications « Appimage »
- Les fichiers vidéos de taille supérieure à 5 Mo sont maintenant affichés dans les vignettes de dossiers quand le dossier pour les vignettes est activé.
- Lors de la lecture de CD audio, Dolphin est maintenant capable de modifier le débit binaire « CBR » pour l'encodeur MP3 et corriger les horodatages pour FLAC.
- Le menu « Contrôle » de Dolphin affiche maintenant des éléments « Créer de nouveau... » et possède un nouvel élément de menus « Afficher les emplacements masqués ».
- Dolphin se ferme maintenant uniquement quand il ne reste qu'un seul onglet ouvert et que le raccourci de clavier standard « Fermer l'onglet » est utilisé.
- Après le démontage d'un volume à partir du panneau « Emplacements », il est maintenant possible de le monter de nouveau.
- L'affichage des documents récents (disponible par navigation à partir de « recentdocuments:/ » dans Dolphin) affiche maintenant unique les documents courants et filtre automatiquement les URL Internet.
- Dolphin affiche maintenant une alarme avant de vous autoriser à renommer un fichier ou un dossier, de telle façon à ce qu'il devienne immédiatement masqué.
- Il n'est plus possible de tenter de libérer des disques de votre système d'exploitation actif ou de votre dossier personnel à partir du panneau « Emplacement ».

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, la recherche traditionnelle de fichiers de KDE, possède maintenant, une méthode de recherche de métadonnées, reposant sur KFileMetaData.

### Bureau

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, le puissant client de messagerie électronique de KDE :

- KMail peut maintenant afficher une boîte de réception unifiée.
- Nouveau module externe : génération d'un courriel « HTML » à partir du langage « Markdown »
- Utilisation de l'objet comme texte partagé (comme pour un courriel)
- Les courriels en HTML sont maintenant lisibles indépendamment du thème de couleurs en cours d'utilisation.

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, l'afficheur multi-usage de documents de KDE :

- Le nouvel outil d'annotations « Typewriter » pouvant être utilisé pour écrire du texte n'importe où.
- La table hiérarchique de l'affichage de contenus possède maintenant la possibilité de tout développer et de tou replier, ou de même avec une section spécifique.
- Améliorations du comportement du retour à ligne pour les annotations en ligne
- Lors du survol d'un lien avec la souris, l'URL est maintenant tout le temps affiché, afin de pouvoir cliquer dessus, plutôt que seulement dans le mode « Navigation ».
- Les fichiers « ePub » contenant des ressources avec des espaces dans leurs « URL » s'affichent maintenant correctement.

### Développement

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, l'éditeur de texte avancé de KDE :

- Lors de l'utilisation du terminal intégré, ce dernier se synchrone maintenant automatiquement avec le dossier courant selon l'emplacement sur disque du document actif.
- Le terminal intégré de Kate peut maintenant gagner et perdre le focus lors de l'utilisation du raccourci de clavier avec la touche « F4 ».
- Le commutateur intégré d'onglets de Kate affiche maintenant les emplacements complets pour les fichiers nommés de façon identique.
- La numérotation des lignes est proposée maintenant par défaut
- Le module externe de filtrage de texte, utile et puissant, est maintenant activé par défaut et est plus identifiable. 
- L'ouverture d'un document déjà ouvert avec la fonctionnalité d'ouverture rapide fait maintenant revenir à ce document.
- La fonctionnalité d'ouverture rapide n'affiche plus les entrées en doublon.
- Lors de l'utilisation de multiples activités, les fichiers sont maintenant ouverts dans la bonne activité.
- Kate affiche maintenant tous les icônes correctes lors d'une exécution sous GNOME en utilisant un thème d'icônes de GNOME.

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'émulateur de terminal de KDE :

- Konsole prend maintenant totalement en charge les caractères d'émoticônes
- Les icônes inactives des onglets sont maintenant mis en surbrillance quand elles reçoivent un signal de clochette.
- Les deux points ne sont plus considérés comme faisant partie d'un mot pour les besoins de la sélection par double clic, le rendant plus facile pour la sélection d'emplacements et la sortie de « grep ».
- Quand une souris avec des boutons en avant et arrière est connectée, Konsole peut maintenant utiliser ces boutons pour naviguer entre les onglets.
- Konsole possède maintenant un élément de menu pour ré-initialiser la taille de la police dans le profil par défaut, si sa taille a été augmentée ou diminuée.
- Les onglets sont maintenant plus difficiles à détacher accidentellement et peuvent être plus rapidement trier à nouveau avec précision.
- Amélioration de la sélection du clic + « Maj »
- Correction du double clic sur une ligne de texte, plus grande que la largeur de la fenêtre
- La barre de recherche se ferme de nouveau quand vous appuyez sur la touche « Échap ».

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, l'outil de traduction de KDE :

- Masquage des fichiers traduits sur l'onglet « Projet »
- Ajout de la prise en charge de base pour pology, son système de vérification de syntaxe et de glossaire.
- Navigation simplifiée avec un tri des onglets et l'ouverture avec plusieurs onglets
- Correction des erreurs de segmentation provoquées par des accès concurrents aux objets de la base de données
- Correction d'un « glisser / déposer » incohérent
- Correction d'un bogue concernant les raccourcis qui étaient différents selon les éditeurs.
- Améliorations dans le comportement de recherche (La recherche trouvera et affichera les formes plurielles)
- Compatibilité restaurée avec Windows grâce au système de compilation artisanale

### Utilitaires

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, l'afficheur d'images de KDE :

- L'outil de « réduction des yeux rouges » a reçu un ensemble d'améliorations intéressantes d'ergonomie.
- Gwenview affiche maintenant une boîte de dialogue d'avertissement quand vous masquez la barre de menus, vous informant sur comment revenir en arrière.

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>,l'utilitaire de copie d'écran de KDE :

- Spectacle possède maintenant la capacité de numéroter séquentiellement les fichiers de copie d'écran et de revenir par défaut à ce schéma de nommage, si vous effacez le champ de texte correspondant au nom de fichier.
- Correction de l'enregistrement d'images dans un format autres que « .png » lors de l'utilisation de « Enregistrer sous »
- Lors de l'utilisation de Spectacle pour ouvrir une copie d'écran avec une application externe, il est maintenant possible de modifier et d'enregistrer l'image une fois que vous l'avez réalisée.
- Spectacle ouvre maintenant le dossier correct après un clic sur Outils / Ouvrir le dossier des copies d'écran.
- Les horodatages de copies d'écran correspondent maintenant à la création de l'image, et non quand elle a été enregistrée.
- Toutes les options d'enregistrement sont maintenant regroupées dans la page « Enregistrement ».

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, le gestionnaire d'archives de KDE :

- Ajout de la prise en charge du format « Zstandard » (Archives « tar.zst »)
- Correction de l'aperçu de certains fichiers (Par exemple, Open Document), traités comme des archives plutôt que de les ouvrir avec l'application appropriée.

### Mathématiques

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, la calculatrice simple de KDE, possède maintenant un paramètre pour répéter plusieurs fois, le dernier calcul.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, l'interface mathématique de KDE :

- Ajout d'une type d'entrée « Markdown »
- Mise en surbrillance animée de l'entrée courante calculée de la commande.
- L'affichage des entrées de commandes en attente (en file mais pas encore calculées)
- Autoriser le formatage des entrées de commandes (Couleur de fond d'écran, couleur de premier plan, propriétés de polices).
- Permet d'insérer de nouvelles entrées de commandes à un emplacement arbitraire dans la feuille de travail en plaçant le curseur à la position désirée et en commençant la saisie.
- Pour les expressions possédant des commandes multiples, affiche les résultats comme des objets indépendants de résultats dans la feuille de travail.
- Ajout de la prise en charge de l'ouverture de feuilles de travail avec des emplacements relatifs à partir de la console
- Ajout de la prise en charge de l'ouverture multiple de fichiers dans un unique shell « Cantor »
- Modifier la couleur et la police lors d'une demande d'informations additionnelles, afin de mieux séparer l'entrée courante dans une entrée de commandes
- Ajout de raccourcis pour la navigation parmi les feuilles de travail (« CTRL » + Page vers le haut, « CTRL » + Page vers le bas). 
- Ajout d'une action dans le sous-menu « Affichage » pour la ré-initialisation du zoom
- Active le téléchargement des projets Cantor à partir de store.kde.org (A ce moment, le téléchargement ne fonctionne qu'à partir du site Internet).
- Ouvrir la feuille de travail en mode lecture seule si le moteur n'est pas disponible sur le système.

<a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, le traceur de fonctions de KDE a corrigé de nombreux problèmes :

- Correction des noms incorrects des tracés pour les dérivées et les intégrales par rapport à la notation conventionnelle.
- L'exportation « SVG » dans Kmplot fonctionne maintenant correctement.
- La première fonctionnalité dérivée n'a aucune notation 
- Le décochage de la fonction non ciblée masque maintenant son graphe :
- Résolution du plantage de Kmplot lors de l'ouverture de « Modifier les constantes » de façon récursive à partir de l'éditeur de fonctions.
- Résolution du plantage de Kmplot quand le pointeur de souris suit dans le tracé après la suppression de la fonction.
- Vous pouvez maintenant exporter des données de tracé graphique vers un format d'image
