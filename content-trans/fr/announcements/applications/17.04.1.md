---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE publie les applications de KDE en version 17.04.1
layout: application
title: KDE publie les applications de KDE en version 17.04.1
version: 17.04.1
---
11 mai 2017. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../17.04.0'>applications 17.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à kdepim, dolphin, gwenview, kate, kdenlive et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.32 de KDE.
