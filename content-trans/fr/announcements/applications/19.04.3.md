---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE publie la version 19.04.3 des applications.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE publie les applications KDE en version 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la troisième mise à jour de consolidation pour les <a href='../19.04.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 60 corrections de bogues identifiés, portant sur des améliorations de Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular, Umbrello parmi bien d'autres.

Les améliorations incluses sont :

- Konqueror et Kontact ne se plantent plus lors d'une sortie avec « QtWebEngine » en version 5.13.
- La coupure de groupes avec des compositions ne plante plus dans l'éditeur vidéo Kdenlive.
- L'importateur de Python dans le concepteur UML Umbrello gère maintenant les paramètres avec des arguments par défaut.
