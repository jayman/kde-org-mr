---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE publie les applications de KDE en version 16.08.0
layout: application
title: KDE publie les applications de KDE en version 16.08.0
version: 16.08.0
---
18 août 2016 . Aujourd'hui, KDE présente KDE Applications 16.08, avec un éventail impressionnant de mises à niveau lorsqu'il s'agit d'une meilleure facilité d'accès, de l'introduction de nouvelles fonctionnalités de la suppression de quelques problèmes mineurs.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> et KDiskFree ont maintenant été portés sur KDE Frameworks 5 et nous attendons avec impatience vos commentaires et vos impressions sur les nouvelles fonctionnalités introduites avec cette version.

Dans le cadre de l'effort continu pour diviser les bibliothèques de la suite Kontact afin de faciliter leur utilisation par des tiers, le fichier tarball kdepimlibs a été divisé en akonadi-contacts, akonadi-mime et akonadi-notes.

Nous avons abandonné les paquets suivants : kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu et mplayerthumbs. Cela nous aidera à nous concentrer sur le reste du code.

### Pour rester en contact

<a href='https://userbase.kde.org/Kontact'>La suite Kontact</a> a reçu la série habituelle de nettoyages, de corrections de bogues et d'optimisations dans cette version. On notera l'utilisation de QtWebEngine dans divers composants, ce qui permet d'utiliser un moteur de rendu HTML plus moderne. Nous avons également amélioré la prise en charge de VCard4 et ajouté de nouveaux plugins qui peuvent avertir si certaines conditions sont remplies lors de l'envoi d'un e-mail, par exemple vérifier que vous voulez autoriser l'envoi d'e-mails avec une identité donnée, ou vérifier si vous envoyez des e-mails en texte clair, etc.

### Nouvelle version de Marble

<a href='https://marble.kde.org/'>Marble</a> 2.0 inclue dans KDE Applications 16.08 comprend plus de 450 modifications de code, notamment des améliorations de la navigation, du rendu et du rendu vectoriel expérimental des données OpenStreetMap.

### Plus d'archivage

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> peut désormais extraire les fichiers AppImage et .xar ainsi que tester l'intégrité des archives zip, 7z et rar. Il peut également ajouter/modifier les commentaires dans les archives rar.

### Améliorations du terminal

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> a reçu des améliorations concernant les options de rendu et la prise en charge de l'accessibilité.

### Et encore plus !

<a href='https://kate-editor.org'>Kate</a> a reçu des onglets déplaçables. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Plus d'information...</a>

<a href='%1'>KGeography</a> intègre des provinces et des régions du Burkina Faso.

### Contrôle agressif de nuisance

Plus de 120 bogues ont été corrigés dans les applications, y compris la suite Kontact, Ark, Cantor, Dolphin, KCalc, Kdenlive et bien plus !

### Journal complet des changements
