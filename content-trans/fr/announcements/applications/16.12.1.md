---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE publie les applications de KDE en version 16.12.1
layout: application
title: KDE publie les applications de KDE en version 16.12.1
version: 16.12.1
---
12 Janvier 2017. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../16.12.0'>applications 16.12 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Cette version corrige un bogue avec « PERTE DE DONNÉES » dans la ressource « iCal », échouant lors de la création de « std.ics » si non existant.

Plus de 40 corrections de bogues apportent des améliorations à Kdepim, Ark, Gwenview, Kajongg, Okular, Kate, Kdenlive et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.28 de KDE.
