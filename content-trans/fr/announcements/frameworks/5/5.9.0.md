---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nouveau module : module « ModemManagerQt » (Qt wrapper pour ModemManager API). Alternativement, mise à jour vers « Plasma-NM »version 5.3 « bêta » lors de la mise à jour vers « ModemManagerQt » 5.9.0.

### KActivities

- Implémentation de l'oubli d'une ressource
- Correction de compilations
- Ajout d'un module externe pour enregistrer les évènements pour les notifications « KRecentDocument »

### KArchive

- Respect du paramètre « KZip::extraField » aussi lors de l'écriture des entrées centrales d'en-têtes
- Suppression de deux assertions erronées, se produisant quand le disque est plein. Bogue 343214

### KBookmarks

- Correction de la compilation avec Qt 5.5

### KCMUtils

- Utilisation du nouveau système de modules externes reposant sur « json ». Les modules « KCM » sont rechercher dans le dossier « kcms/ ». A partir de maintenant, un fichier de bureau a toujours besoin d'être installé dans le dossier « kservices5/ » pour des raisons de compatibilité.
- Chargement et encapsulation de la version uniquement « QML » de « kcms », lorsque possible

### KConfig

- Correction de l'assertion lors de l'utilisation de « KSharedConfig » dans un destructeur global d'objets.
- kconfig_compiler : ajout de la prise en charge de « CategoryLoggingName » dans les fichiers « *.kcfgc » pour pouvoir générer des appels à « qCDebug(category) »

### KI18n

- Préchargement du catalogue globale de Qt lors de l'utilisation de « i18n() »

### KIconThemes

- L'application « KIconDialog » peut maintenant être affichée en utilisant les méthodes « show() » et « exec() » de « QDialog »
- Correction de « KIconEngine::paint » pour gérer différents « devicePixelRatios »

### KIO

- Activation de « KPropertiesDialog » pour afficher également les informations d'espace libre pour des systèmes de fichiers distants (par exemple « smb »)
- Correction de « KUrlNavigator » pour les fichiers graphiques en haute résolution
- Permission donnée à « KFileItemDelegate » de gérer dans les animations, le « devicePixelRatio » non par défaut.

### KItemModels

- KRecursiveFilterProxyModel : reprise pour émettre les signaux corrects au bon moment
- KDescendantsProxyModel : gestion des déplacements signalés dans le modèle de source.
- KDescendantsProxyModel : correction du comportement lorsque la sélection est faite pendant un réinitialisation.
- KDescendantsProxyModel : permettre la construction et l'utilisation de « KSelectionProxyModel » à partir de « QML ».

### KJobWidgets

- Propager le code d'erreur sur l'interface « D-Bus » pour « JobView »

### KNotifications

- Ajout d'une version de « event() » consommant aucune icône autre que celle par défaut
- Ajout d'une version de « event() » consommant « StandardEvent eventId » et « QString iconName »

### KPeople

- Autoriser les extensions de métadonnées d'actions par utilisation des types prédéfinis
- Correction des modèles non correctement mis à jour après la suppression de contact d'une personne

### KPty

- Afficher si « KPty » a été compilé avec la bibliothèque « utempter »

### KTextEditor

- Ajouter le fichier de coloration syntaxique « kdesrc-buildrc »
- syntaxe : nouvelle prise en charge pour les littéraux binaires pour entiers dans le fichier de coloration syntaxique pour « PHP »

### KWidgetsAddons

- Rendre l'animation «  » plus douce pour les périphériques à haut densité de pixels

### KWindowSystem

- Ajouter une implémentation fictive « Wayland » pour « KWindowSystemPrivate »
- « KWindowSystem::icon » avec « NETWinInfo » non lié à la plate-forme « X11 ».

### KXmlGui

- Préserver le domaine de traduction lors de la fusion des fichiers « .rc  »
- Correction de l'alarme d'exécution « QWidget::setWindowModified » : la fenêtre de titre ne doit pas contenir un caractère « [*] »

### KXmlRpcClient

- Installation des traductions

### Environnement de développement de Plasma

- Correction d'infobulles parasites quand un propriétaire temporaire d'une infobulle a disparu ou devient vide.
- Correction de la barre de tâches, incorrectement positionnée initialement, qui pouvait être visible, par exemple dans Kickoff.
- Les transitions « PageStack » utilisent maintenant les objets « Animator » pour des animations plus douces
- Les transitions « TabGroup » utilisent maintenant les objets « Animator » pour des animations plus douces
- Faire fonctionner « Svg » et « FrameSvg » avec « QT_DEVICE_PIXELRATIO »

### Opaque

- Rafraîchir les propriétés de la batterie sur le signal de reprise

### Modifications dans le système de compilation

- Les modules « Extra CMake Modules » (ECM) sont maintenant gérés en version, comme les environnements de développement de KDE. Maintenant, il est en version 5.9, alors que précédemment, il était en version 1.8.
- De nombreux environnements de développement ont été corrigés pour être utilisables sans rechercher leurs dépendances privées. Par exemple, les applications recherchant un environnement de développement n'utilisent plus que les dépendances publiques, pas celles privées.
- Autoriser la configuration de «  » pour mieux gérer les profils en multi-architecture

### Intégration des environnements de développement

- Correction d'un possible plantage en fermant l'objet « QSystemTrayIcon » (déclenché par exemple par « Trojita »), bogue 343976
- Correction des boîtes de dialogue modales natives dans « QML », bogue 334963

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
