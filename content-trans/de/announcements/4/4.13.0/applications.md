---
date: '2014-04-16'
description: KDE veröffentlicht die Anwendungen und Plattform 4.13
hidden: true
title: Die KDE-Anwendungen Version 4.13 profitieren von der neuen semantischen Suche
  und bringen neue Funktionen
---
Die KDE-Gemeinschaft freut sich, die neueste größere Aktualisierung der KDE-Anwendungen mit neuen Funktionen und Fehlerkorrekturen zu veröffentlichen. Kontact (die Persönliche Informationsverwaltung) hat viel Aktivität gesehen, profitiert von Verbesserungen in KDEs Semantischer Suche und bringt neue Funktionen mit. Der Dokumentbetrachter Okular und der Texteditor Kate erhalten Verbesserungen der Benutzeroberfläche und Funktionen. In den Bereichen Bildung und Spiele gibt es den neuen Sprachtrainer Articulate; Marble (der Schreibtischglobus) erhält Unterstützung für Sonne, Mond, Planeten, Fahrrad-Routen und nautische Meilen. Palapeli erreicht neue Dimensionen durch neuen Funktionen.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## Neue Funktionen und größere Geschwindigkeit für KDE Kontact

KDEs Kontact-Suite bringt eine Reihe neuer Funktionen in den verschiedenen Komponenten. KMail unterstützt Cloud-Speicher und verbessert Sieve-Filterung auf Server-Seite. KNotes kann jetzt Alarme erstellen und erhält eine Suchfunktion. Zudem gibt es viele Verbesserungen an der Daten-Zwischenspeicherschicht in Kontact, die viele Aktionen beschleunigt.

### Unterstützung für Cloud-Speicher

KMail bringt Unterstützung für Speicherdienste, sodass große Anhänge in Cloud-Diensten gespeichert und als Link gesendet werden können. Unterstützt werden Dropbox, Box, KolabServer, YouSendIt, UbuntuOne und Hubic und es gibt eine generische WebDav-Option. Das Werkzeug <em>storageservicemanager</em> hilft bei der Verwaltung der Dateien bei diesen Diensten.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Erheblich verbesserte Unterstützung für Sieve

Sieve-Filter, eine Technik zum handhaben von Filtern auf dem Server, kann jetzt mit Urlaubstagen auf mehreren Servern umgehen. Das Werkzeug KSieveEditor ermöglicht es Benutzern, Sieve-Filter zu bearbeiten, ohne den Server zu Kontact hinzuzufügen.

### Weitere Änderungen

Die Schnellfilterleiste erfuhr kleine Verbesserungen der Oberfläche und profitiert stark von den verbesserten Such-Fähigkeiten der KDE-Entwicklerplattform 4.13. Die Suchfunktion ist nun wesentlich schneller und verlässlicher. Beim Verfassen von E-Mails kann jetzt einen URL-Abkürzer verwenden, um die bestehenden Übersetzungs- und Textbaustein-Funktionen aufzuwerten.

Stichwörter und Anmerkungen an PIM-Daten werden jetzt in Akonadi gespeichert. In zukünftigen Versionen können Sie zudem auf Serverseite (bei IMAP oder Kolab) gespeichert werden, was diese Informationen über mehrere Computer hinweg zur Verfügung stellt. Akonadi: API für Google Drive hinzugefügt. Das Suchen mit Modulen von Drittentwicklern (schnellere Ergebnisse) und serverseitige Suche (nicht lokale Daten können durchsucht werden) werden jetzt unterstützt.

### KNotes, KAddressbook

KNotes hat viele Verbesserungen erfahren. Es wurden viele Fehler und kleine Ungereimtheiten beseitigt. Die Möglichkeit, Erinnerungen für Notizen festzulegen und das Durchsuchen von Notizen sind Neuheiten. Lesen Sie <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>hier</a> mehr darüber. Aus KAdressbook kann nun gedruckt werden: weitere Details finden Sie <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>hier</a>.

### Verbesserungen der Leistung

Kontact performance is noticeably improved in this version. Some improvements are due to the integration with the new version of KDE’s <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Semantic Search</a> infrastructure, and the data caching layer and loading of data in KMail itself have seen significant work as well. Notable work has taken place to improve support for the PostgreSQL database. More information and details on performance-related changes can be found in these links:

- Storage Optimizations: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>sprint report</a>
- speed up and size reduction of database: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>mailing list</a>
- optimization in access of folders: <a href='https://git.reviewboard.kde.org/r/113918/'>review board</a>

### KNotes, KAddressbook

KNotes hat viele Verbesserungen erfahren. Es wurden viele Fehler und kleine Ungereimtheiten beseitigt. Die Möglichkeit, Erinnerungen für Notizen festzulegen und das Durchsuchen von Notizen sind Neuheiten. Lesen Sie <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>hier</a> mehr darüber. Aus KAdressbook kann nun gedruckt werden: weitere Details finden Sie <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>hier</a>.

## Okular mit besserer Benutzeroberfläche

This release of the Okular document reader brings a number of improvements. You can now open multiple PDF files in one Okular instance thanks to tab support. There is a new Magnifier mouse mode and the DPI of the current screen is used for PDF rendering, improving the look of documents. A new Play button is included in presentation mode and there are improvements to Find and Undo/Redo actions.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate bringt eine verbesserte Statuszeile, animierte Hervorhebung zusammengehöriger Klammern sowie erweiterte Module

The latest version of the advanced text editor Kate introduces <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>animated bracket matching</a>, changes to make <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>AltGr-enabled keyboards work in vim mode</a> and a series of improvements in the Kate plugins, especially in the area of Python support and the <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>build plugin</a>. There is a new, much <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>improved status bar</a> which enables direct actions like changing the indent settings, encoding and highlighting, a new tab bar in each view, code completion support for <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>the D programming language</a> and <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>much more</a>. The team has <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>asked for feedback on what to improve in Kate</a> and is shifting some of its attention to a Frameworks 5 port.

## Verschiedene Funktionen im Allgemeinen

Konsole brings some additional flexibility by allowing custom stylesheets to control tab bars. Profiles can now store desired column and row sizes. See more <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>here</a>.

Umbrello makes it possible to duplicate diagrams and introduces intelligent context menus which adjust their contents to the selected widgets. Undo support and visual properties have been improved as well. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introduces RAW preview support</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

The sound mixer KMix introduced remote control via the DBUS inter-process communication protocol (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>details</a>), additions to the sound menu and a new configuration dialog (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>details</a>), and a series of bug fixes and smaller improvements.

Dolphin's search interface has been modified to take advantage of the new search infrastructure and received further performance improvements. For details, read this <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>overview of optimization work during the last year</a>.

Im KHelpcenter gibt es jetzt eine alphabetische Sortierung der Module und neue Kategorien dafür, damit Module leichter gefunden werden können.

## Spiel- und Lernprogramme

KDE's game and educational applications have received many updates in this release. KDE's jigsaw puzzle application, Palapeli, has gained <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>nifty new features</a> that make solving large puzzles (up to 10,000 pieces) much easier for those who are up to the challenge. KNavalBattle shows enemy ship positions after the game ends so that you can see where you went wrong.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

KDE's Educational applications have gained new features. KStars gains a scripting interface via D-BUS and can use the astrometry.net web services API to optimize memory usage. Cantor has gained syntax highlighting in its script editor and its Scilab and Python 2 backends are now supported in the editor. Educational mapping and navigation tool Marble now includes the positions of the <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sun, Moon</a> and <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planets</a> and enables <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>capturing movies during virtual tours</a>. Bicycle routing is improved with the addition of cyclestreets.net support. Nautical miles are now supported and clicking a <a href='http://en.wikipedia.org/wiki/Geo_URI'>Geo URI</a> will now open Marble.

#### KDE-Programme installieren

KDE-Software einschließlich all ihrer Bibliotheken und Anwendungen ist kostenlos unter Open-Source-Lizenzen erhältlich. KDE-Software läuft auf verschiedenen Betriebssystemen, Hardware-Konfigurationen und Prozessorarchitekturen wie z.B. ARM und x86, und es arbeitet mit jeder Art von Fensterverwaltung und Arbeitsumgebung zusammen. Neben Linux und anderen UNIX-basierten Betriebssystemen sind von den meisten KDE-Anwendungen auch Versionen für Microsoft Windows und Apple Mac OS X erhältlich. Sie sind auf den Seiten <a href='http://windows.kde.org'>KDE-Software für Windows</a> bzw. <a href='http://mac.kde.org/'>KDE-Software für den Mac</a> erhältlich. Experimentelle Versionen von KDE-Anwendungen für verschiedene Mobilplattformen wie MeeGo, MS Windows Mobile und Symbian sind im Internet erhältlich, werden zur Zeit aber nicht unterstützt. <a href='http://plasma-active.org'>Plasma Active</a> ist eine Benutzerumgebung für ein großes Spektrum an Geräten wie beispielsweise Tabletts und andere mobile Hardware.

Die KDE-Software kann als Quelltext und in verschiedenen binären Formaten von <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> heruntergeladen und auch auf <a href='/download'>CD-ROM</a> und von vielen großen <a href='http://download.kde.org/stable/%1'>GNU/Linux- und UNIX-Systemen</a> bezogen werden.

##### Pakete

Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete von 4.13.0für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft,

##### Paketquellen

Eine aktuelle Liste aller Binärpakete, von denen das Release-Team in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Community-Wiki</a>.

Der vollständige Quelltext für 4.13.0 kann <a href='/info/4/4.13.0'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren von %1 finden Sie auf der <a href='/info/4/4.13.0#binary'>Infoseite zu 4.13.0</a>.

#### Systemanforderungen

Um den größtmöglichen Nutzen aus den Veröffentlichungen zu ziehen, empfehlen wir, eine aktuelle Version von Qt zu verwenden, etwa 4.8.4. Dies ist nötig, um eine stabile und performante Erfahrung zu bieten, weil einige KDE-Verbesserungen in Wahrheit am darunterliegenden Qt-Framework ansetzen.

Um den vollen Umfang an Fähigkeiten der KDE-Software zu nutzen, empfehlen wir außerdem, den neuesten Grafiktreiber für Ihr System zu verwenden, weil dies den Nutzungseindruck maßgeblich verbessert, sowohl bei wählbaren Funktionen als auch bei der allgemeinen Leistung und Stabilität.
