---
aliases:
- ../announce-4.11.3
date: 2013-11-05
description: KDE veröffentlicht die Plasma-Arbeitsbereiche, Anwendungen und Plattform
  4.11.3
title: KDE veröffentlicht die November-Aktualisierungen der Plasma-Arbeitsbereiche,
  der Anwendungen und Plattform
---
5. November 2013. Heute veröffentlicht KDE Aktualisierungen der Arbeitsbereiche, der Anwendungen und Entwicklerplattform. Dies ist die dritte monatliche Aktualisierung zur Stabilisierung in KDE 4.11. Wie in der Ankündigung zur Veröffentlichung angemerkt, werden die Arbeitsbereiche für die nächsten zwei Jahre aktualisiert. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 120 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für die Fensterverwaltung KWin, die Dateiverwaltung Dolphin, das integrierte Programm Kontact für persönliche Informationsverwaltung, das UML-Programm Umbrello und andere Programme. Es gibt viele Korrekturen zur Stabilisierung und die üblichen Erweiterungen der Übersetzungen.

Eine umfassendere <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.3&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>Liste der Änderungen</a> finden Sie im KDE-Fehlermeldungssystem. Eine genaue Liste der Änderungen für 4.11.3 finden Sie auch in den Git-Protokollen.

Um den Quelltext oder Pakete zur Installation herunterzuladen, gehen Sie zur Seite <a href='/info/4/4.11.3'>Informationen über 4.11.3</a>. Möchten Sie weitere Informationen über der Version von 4.11 der KDE-Arbeitsbereiche, der Anwendungen oder der Entwicklerplattform erhalten, lesen Sie bitte die <a href='/announcements/4.11/'>Hinweise zur Veröffentlichung von 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Der Arbeitsablauf für das spätere Senden in Kontact` width="600px">}}

KDE-Software einschließlich aller Bibliotheken und Anwendungen ist frei unter „Open Source“-Lizenzen verfügbar. KDE-Software kann als Quelltext und in verschiedenen binären Formaten von <a href='http://download.kde.org/stable/4.11.3/'>download.kde.org</a> oder von vielen der <a href='/distributions'>wichtigsten GNU/Linux- und Unix-Distributionen</a> heruntergeladen werden.
