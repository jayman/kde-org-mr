---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE veröffentlicht die KDE-Anwendungen 15.12.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.12.3
version: 15.12.3
---
15. März 2016. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../15.12.0'>KDE-Anwendungen 15.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 15 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für kdepim, Akonadi, Ark, KBlocks, KCalc, KTouch und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
