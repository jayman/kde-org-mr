---
aliases:
- ../announce-applications-17.08-rc
date: 2017-08-04
description: KDE veröffentlicht den Freigabekandidaten der Anwendungen 17.08
layout: application
release: applications-17.07.90
title: KDE veröffentlicht den Freigabekandidaten der KDE-Anwendungen 17.08
---
04. August 2017. Heute veröffentlicht KDE den Freigabekandidaten der neuen Version der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

In den <a href='https://community.kde.org/Applications/17.08_Release_Notes'>Veröffentlichungshinweisen der KDE-Gemeinschaft</a> finden Sie Informationen über neue Quelltextarchive, Portierungen zu KF5 und bekannte Probleme. Eine vollständigere Ankündigung erscheint mit der endgültigen Version.

Es sind sind gründliche Tests für die Veröffentlichung der KDE-Anwendungen 17.08 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Benutzer, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim KDE-Team und installieren Sie den Freigabekandidaten und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.
