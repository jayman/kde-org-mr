---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE veröffentlicht die Anwendungen 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE veröffentlicht die Anwendungen 19.08.2
version: 19.08.2
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../19.08.0'>KDE-Anwendungen 19.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize, Spectacle, among others.

Verbesserungen umfassen:

- High-DPI support got improved in Konsole and other applications
- Switching between different searches in Dolphin now correctly updates search parameters
- KMail can again save messages directly to remote folders
