---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE veröffentlicht die KDE-Anwendungen 17.08.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.08.3
version: 17.04.3
---
13. Juli 2017. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../17.04.0'>KDE-Anwendungen 17.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 25 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für kdepim, Dolphin, Dragonplayer, Kdenlive und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
