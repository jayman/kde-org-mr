---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE veröffentlicht die KDE-Anwendungen 17.08.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.08.2
version: 17.08.2
---
12. Oktober 2017. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../17.08.0'>KDE-Anwendungen 17.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 25 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Dolphin, Gwenview, Kdenlive, Marble und Okular.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.

Verbesserungen umfassen:

- A memory leak and crash in Plasma events plugin configuration was fixed
- Read messages are no longer removed immediately from Unread filter in Akregator
- Gwenview Importer now uses the EXIF date/time
