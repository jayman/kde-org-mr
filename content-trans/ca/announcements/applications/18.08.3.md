---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE distribueix les aplicacions 18.08.3 del KDE
layout: application
title: KDE distribueix les aplicacions 18.08.3 del KDE
version: 18.08.3
---
8 de novembre de 2018. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../18.08.0'>aplicacions 18.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha unes 20 esmenes registrades d'errors que inclouen millores al Kontact, Ark, Dolphin, els jocs del KDE, Kate, Okular i Umbrello, entre d'altres.

Les millores inclouen:

- Es recorda el mode de visualització HTML al KMail, i torna a carregar les imatges externes si es permet
- Ara el Kate recorda la meta-informació (incloent-hi les adreces d'interès) entre sessions d'edició
- El desplaçament automàtic del text a la IU del Telepathy s'ha solucionat amb les versions més noves del QtWebEngine
