---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE distribueix els espais de treball Plasma, les aplicacions i la plataforma
  4.11.
title: Compilació de programari KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Els espais de treball del Plasma 4.11 de KDE` >}} <br />

14 d'agost de 2013. La comunitat KDE anuncia amb satisfacció la darrera actualització major dels espais de treball Plasma, les aplicacions i la plataforma de desenvolupament que aporta noves funcionalitats i esmenes mentre que es prepara la plataforma per a una evolució addicional. Els espais de treball Plasma 4.11 rebran manteniment a llarg termini atès que l'equip se centra en la transició tècnica als Frameworks 5. Per tant, ara es mostra l'últim llançament combinat dels espais de treball, les aplicacions i la plataforma amb el mateix número de versió.<br />

Aquest llançament està dedicat a la memòria de l'<a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul «toolz» Chitnis</a>, un gran campió del programari de codi font lliure i obert de l'Índia. L'Atul va dirigir les conferències Linux Bangalore i FOSS.IN des de 2001 i ambdues eren esdeveniments que van fer història en el panorama FOSS de l'Índia. El KDE de l'Índia va néixer en el primer FOSS.in en el desembre de 2005. Molts col·laboradors indis del KDE van començar en aquests esdeveniments. Va ser gràcies als ànims de l'Atul que el dia del projecte KDE en el FOSS.IN va ser sempre un èxit enorme. L'Atul ens va deixar el 3 de juny després de lluitar una batalla contra el càncer. Resti la seva ànima en pau. Agraïm les seves contribucions a un món millor.

Aquests llançaments estan tots traduïts a 54 idiomes; s'espera afegir més idiomes en els llançaments mensuals menors posteriors d'esmenes d'errors de KDE. L'equip de documentació ha actualitzat 91 manuals d'aplicacions en aquest llançament.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Els espais de treball del Plasma 4.11 continuen refinant l'experiència d'usuari</a>

Preparat per un manteniment a llarg termini, els espais de treball Plasma proporcionen més millores a les funcionalitats bàsiques amb una barra de tasques més suau, un giny de bateria més intel·ligent, i un mesclador de so millorat. La introducció del KScreen aporta una gestió intel·ligent del multimonitor als espais de treball, i les millores de rendiment a gran escala combinades amb petits retocs d'usabilitat milloren l'experiència global de manera agradable.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Les aplicacions del KDE 4.11 fan un pas endavant en la gestió d'informació personal i millores globals</a>

Aquest llançament es distingeix per moltes millores en la pila del KDE PIM, que aporten un rendiment millor i moltes funcionalitats noves. El Kate millora la productivitat dels desenvolupadors en Python i Javascript amb connectors nous. El Dolphin ha esdevingut més ràpid i les aplicacions educatives tenen diverses funcionalitats noves.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> La plataforma KDE 4.11 proporciona un rendiment millor</a>

Aquest llançament de la plataforma KDE 4.11 continua enfocant-se en l'estabilitat. Les funcionalitats noves s'estan implementant amb el nostre futur llançament dels Frameworks 5.0 de KDE, però per al llançament estable s'ha restringit a optimitzar la infraestructura del Nepomuk.

<br />
En actualitzar, doneu una ullada a les <a href='https://community.kde.org/KDE_SC/4.11_Release_Notes'>notes de llançament</a>.<br />

## Difoneu la notícia i vegeu què està ocorrent: etiqueteu amb &quot;KDE&quot;

El KDE anima a tothom a difondre la notícia en la web social. Envieu articles a llocs de notícies i useu canals com delicious, digg, reddit, twitter o identi.ca. Pugeu captures de pantalla a serveis com Facebook, Flickr, ipernity o Picasa, i publiqueu-les en els grups adequats. Creeu vídeos de captures de pantalla i pugeu-los a YouTube, Blip.tv o Vimeo. Si us plau, etiqueteu els articles i el material pujat amb &quot;KDE&quot;. Això farà que sigui més fàcil de trobar i proporcionarà a l'equip de promoció del KDE una manera d'analitzar la cobertura dels llançaments 4.11 del programari KDE.

## Festes de llançament

Com és usual, els membres de la comunitat KDE organitzen festes del llançament per tot el món. Algunes d'aquestes ja s'han programat i altres ho faran ben aviat. Consulteu la <a href='https://community.kde.org/Promo/Events/Release_Parties/4.11'>llista de festes aquí</a>. Tothom hi és benvingut a participar! En elles hi haurà una combinació de companyia interessant i converses inspiradores, així com menjar i beguda. Són una gran oportunitat per saber més sobre què està ocorrent en el KDE, implicar-se en el projecte, o solament per conèixer a altres usuaris i col·laboradors.

Animem a tothom a organitzar les seves pròpies festes. Són divertides d'organitzar i obertes a tothom! Doneu una ullada als <a href='https://community.kde.org/Promo/Events/Release_Parties'>consells sobre com organitzar una d'aquestes festes</a>.

## Sobre aquests anuncis de llançament

Aquests anuncis de llançament han estat preparats per os Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin i altres membres de l'equip de promoció del KDE i de la resta de la comunitat del KDE. Cobreixen el resum dels canvis més importants realitzats en el programari del KDE durant els últims sis mesos.

#### Contribució a KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

El nou <a href='http://jointhegame.kde.org/'>programa de suport de membres</a> de KDE e.V. ara està obert. Per 25&euro; cada tres mesos podreu assegurar que la comunitat internacional KDE continua creixent per fer programari lliure de qualitat mundial.
