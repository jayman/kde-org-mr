---
date: 2013-08-14
hidden: true
title: La plataforma KDE 4.11 proporciona un rendiment millor
---
La plataforma KDE 4 està en congelació de funcionalitats des de l'alliberament 4.9. Per tant, aquesta versió només inclou esmenes d'errors i millores de rendiment.

El motor semàntic del Nepomuk ha rebut grans optimitzacions de rendiment, per exemple, un conjunt d'optimitzacions de lectura ha augmentat la lectura de dades fins a 6 vegades més ràpida. La indexació s'ha fet més intel·ligent, i s'ha dividit en dues etapes. La primera etapa recupera la informació general (com el tipus de fitxer i el nom) immediatament; la informació addicional com les etiquetes multimèdia, la informació de l'autor, etc. s'extreu en una segona etapa més lenta. La visualització de les metadades en contingut creat nou o acabat de descarregar ara també és molt més ràpida. Addicionalment, els desenvolupadors del Nepomuk han millorat el sistema de còpia de seguretat i restauració. També, ara el Nepomuk pot indexar gran varietat de formats de documents, incloent-hi ODT i DOCX.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Funcionalitats semàntiques en acció en el Dolphin` width="600px">}}

El format d'emmagatzematge optimitzat del Nepomuk i la reescriptura de l'indexador de correus requereixen la reindexació de part del contingut del disc dur. Per tant, l'execució de la reindexació consumirà una quantitat anormal de recursos de l'ordinador durant cert període, en funció de la quantitat de contingut que calgui reindexar. En la primera connexió s'executarà la conversió automàtica de la base de dades del Nepomuk.

Hi ha més esmenes petites que <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>es poden trobar en els registres del git</a>.

#### Instal·lació de la plataforma de desenvolupament del KDE

El programari KDE, incloses totes les seves biblioteques i les seves aplicacions, és disponible de franc d'acord amb les llicències de codi font obert (Open Source). El programari KDE s'executa en diverses configuracions de maquinari i arquitectures de CPU com ARM i x86, sistemes operatius i treballa amb qualsevol classe de gestor de finestres o entorn d'escriptori. A banda del Linux i altres sistemes operatius basats en UNIX, podeu trobar versions per al Windows de Microsoft de la majoria d'aplicacions del KDE al lloc web <a href='http://windows.kde.org'>programari de Windows</a> i versions per al Mac OS X d'Apple en el <a href='http://mac.kde.org/'>lloc web del programari KDE per al Mac</a>. Les compilacions experimentals d'aplicacions del KDE per a diverses plataformes mòbils com MeeGo, MS Windows Mobile i Symbian es poden trobar en la web, però actualment no estan suportades. El <a href='http://plasma-active.org'>Plasma Active</a> és una experiència d'usuari per a un espectre ampli de dispositius, com tauletes i altre maquinari mòbil.

El programari KDE es pot obtenir en codi font i en diversos formats executables des de <a href='https://download.kde.org/stable/4.11.0'>download.kde.org</a> i també es pot obtenir en <a href='/download'>CD-ROM</a> o amb qualsevol dels <a href='/distributions'>principals sistemes GNU/Linux i UNIX</a> que es distribueixen avui en dia.

##### Paquets

Alguns venedors de Linux/UNIX OS han proporcionat gentilment paquets executables de 4.11.0 per a algunes versions de la seva distribució, i en altres casos ho han fet els voluntaris de la comunitat. <br />

##### Ubicació dels paquets

Per a una llista actual de paquets executables disponibles dels quals ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>wiki de la comunitat</a>.

El codi font complet per a 4.11.0 es pot <a href='/info/4/4.11.0'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari KDE, versió 4.11.0, estan disponibles a la <a href='/info/4/4.11.0#binary'>pàgina d'informació de la versió 4.11.0</a>.

#### Requisits del sistema

Per tal d'aprofitar al màxim aquestes versions, es recomana utilitzar una versió recent de les Qt, com la 4.8.4. Això és necessari per a assegurar una experiència estable i funcional, ja que algunes millores efectuades en el programari KDE s'ha efectuat realment en la infraestructura subjacent de les Qt.<br /> Per tal de fer un ús complet de les capacitats del programari KDE, també es recomana l'ús dels darrers controladors gràfics per al vostre sistema, ja que aquest pot millorar substancialment l'experiència d'usuari, tant per a les funcionalitats opcionals, com el rendiment i l'estabilitat en general.

## També s'ha anunciat avui:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Els espais de treball del Plasma 4.11 continuen refinant l'experiència d'usuari</a>

Preparat per un manteniment a llarg termini, els espais de treball Plasma proporcionen més millores a les funcionalitats bàsiques amb una barra de tasques més suau, un giny de bateria més intel·ligent, i un mesclador de so millorat. La introducció del KScreen aporta una gestió intel·ligent del multimonitor als espais de treball, i les millores de rendiment a gran escala combinades amb petits retocs d'usabilitat milloren l'experiència global de manera agradable.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Les aplicacions del KDE 4.11 fan un pas endavant en la gestió d'informació personal i millores globals</a>

Aquest llançament es distingeix per moltes millores en la pila del KDE PIM, que aporten un rendiment millor i moltes funcionalitats noves. El Kate millora la productivitat dels desenvolupadors en Python i Javascript amb connectors nous. El Dolphin ha esdevingut més ràpid i les aplicacions educatives tenen diverses funcionalitats noves.
