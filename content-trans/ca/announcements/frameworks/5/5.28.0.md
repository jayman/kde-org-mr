---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Framework nou: syntax-highlighting

Motor de ressaltat de sintaxi per les definicions de sintaxi del Kate

Aquesta és una implementació autònoma del motor de ressaltat de sintaxi del Kate. Està destinat com un bloc de construcció per a editors de text i també com una representació de ressaltat de sintaxi (p. ex. com HTML), permetent tant la integració amb un editor personalitzat, així com una subclasse del QSyntaxHighlighter preparada per usar-se.

### Icones Brisa

- Actualitza les icones d'acció del KStars (error 364981)
- El Brisa fosca es llistava com a Brisa en el fitxer «.themes» incorrectes de l'Arranjament del sistema (error 370213)

### Mòduls extres del CMake

- Fa que KDECMakeSettings funcioni amb KDE_INSTALL_DIRS_NO_DEPRECATED
- No requereix les dependències de vinculació del Python per l'ECM
- Afegeix el mòdul PythonModuleGeneration

### KActivitiesStats

- Ignora l'estat dels enllaços en ordenar els models UsedResources i LinkedResources

### Eines de Doxygen del KDE

- [CSS] Reverteix els canvis fets pel Doxygen 1.8.12
- Afegeix el fitxer «doxygenlayout»
- Actualitza la manera de definir noms de grups

### KAuth

- Assegura que es pot fer més d'una petició
- Assegura que es pot conèixer el progrés llegint la sortida del programa

### KConfig

- Assegura que no es trenca la compilació amb unitats trencades anteriors
- No dona un error fatal quan no s'analitza adequadament el camp Fitxer

### KCoreAddons

- Mostra l'URL incorrecte
- Carrega els avatars d'usuari des de l'AccountsServicePath si existeix (error 370362)

### KDeclarative

- [QtQuickRendererSettings] Esmena que el predeterminat sigui buit en lloc de «fals»

### Compatibilitat amb les KDELibs 4

- Fa que la bandera de França ocupi realment tot el mapa de píxels

### KDocTools

- Soluciona «checkXML5 genera fitxers HTML en el directori de treball per als docbook vàlids» (error 371987)

### KIconThemes

- Permet factors d'escala no enters a «kiconengine» (error 366451)

### KIdleTime

- S'ha desactivat la inundació de la sortida de la consola amb missatges «en espera»

### KImageFormats

- imageformats/kra.h - Sobreescriu «capabilities()» i «create()» del KraPlugin

### KIO

- Esmena el format de la data HTTP enviat pel «kio_http» per tal d'usar sempre el «locale» C (error 372005)
- KACL: Soluciona fuites de memòria detectades per l'ASAN
- Soluciona fuites de memòria a KIO::Scheduler, detectat per l'ASAN
- S'ha eliminat un botó de neteja duplicat (error 369377)
- Soluciona l'edició d'entrades d'inici automàtic quan /usr/local/share/applications no existeix (error 371194)
- [KOpenWithDialog] Oculta la capçalera de la vista en arbre
- Saneja la mida de la memòria intermèdia de noms d'enllaços simbòlics (error 369275)
- Acaba adequadament el «DropJobs» quan no s'ha emès l'activador (error 363936)
- ClipboardUpdater: soluciona una altra fallada en el Wayland (error 359883)
- ClipboardUpdater: soluciona una fallada en el Wayland (error 370520)
- Permet factors d'escala no enters a KFileDelegate (error 366451)
- kntlm: Distingeix entre domini NULL o buit
- No mostra el diàleg de sobreescriptura si el nom del fitxer està buit
- kioexec: usa noms de fitxer més fàcils d'emprar
- Soluciona la propietat del focus si l'URL canvia abans de mostrar el giny
- Gran millora de rendiment en desactivar les vistes prèvies en el diàleg de fitxer (error 346403)

### KItemModels

- Afegeix vinculacions del Python

### KJS

- Exporta «FunctionObjectImp», usat pel depurador del khtml

### KNewStuff

- Separa els rols d'ordenació i els filtres
- Fa possible consultar les entrades instal·lades

### KNotification

- No desreferenciar un objecte que no s'ha referenciat quan la notificació no té cap acció
- El KNotification ja no tornarà a fallar quan s'usi en una QGuiApplication i no s'executi cap servei de notificació (error 370667)
- Soluciona fallades a NotifyByAudio

### Framework del KPackage

- Assegura que se cerquen metadades per «json» i per «desktop»
- Prevenir que es destrueixi Q_GLOBAL_STATIC en tancar l'aplicació
- Soluciona un apuntador penjant («dangling») al KPackageJob (error 369935)
- Elimina l'associació del Discovery a una clau en eliminar la definició
- Genera la icona dins el fitxer «appstream»

### KPty

- Usa «ulog-helper» en el FreeBSD en lloc de «utempter»
- Cerca més intensa del «utempter» usant també el prefix bàsic del CMake
- Solució temporal a la fallada de «find_program ( utempter ...)»
- Usa el camí de l'ECM per cercar binaris «utempter», més fiable que un prefix senzill del CMake

### KRunner

- i18n: gestiona les cadenes dels fitxers «kdevtemplate»

### KTextEditor

- Brisa fosca: Enfosqueix el color de fons de la línia actual per a millorar-ne la llegibilitat (error 371042)
- S'han ordenat les instruccions del Dockerfile
- Brisa (fosc): Fer una mica més clars per millorar la llegibilitat (error 371042)
- Esmena els sagnadors CStyle i C++/boost quan s'activa els parèntesis automàtics (error 370715)
- Afegeix el mode de línia «auto-brackets»
- Soluciona la inserció de text després del final de fitxer (cas rar)
- Soluciona els fitxers de ressaltat amb XML no vàlid
- Maxima: Elimina els valors dels colors en el codi font, esmena l'etiqueta «itemData»
- Afegeix definicions de sintaxi d'OBJ, PLY i STL
- Afegeix un fitxer d'implementació de ressaltat de sintaxi per a Praat

### KUnitConversion

- Unitats tèrmiques i elèctriques noves i funció d'utilitats d'unitats

### Framework del KWallet

- Si no es troba el Gpgmepp, intenta usar el KF5Gpgmepp
- Usa el Gpgmepp del GpgME-1.7.0

### KWayland

- Millora la reubicabilitat de l'exportació del CMake
- [eines] Soluciona la generació de «wayland_pointer_p.h»
- [eines] Genera mètodes «eventQueue» només per a classes globals
- [servidor] Soluciona una fallada en actualitzar la superfície del teclat amb focus
- [servidor] Soluciona una fallada potencial en la creació d'un DataDevice
- [servidor] Assegura que hi ha un DataSource al DataDevice en «setSelection»
- [eines/generador] Millora la destrucció del recurs a la banda del servidor
- Afegeix una petició per tenir el focus a una PlasmaShellSurface del rol plafó
- Afegeix la implementació per ocultar automàticament el plafó a la interfície PlasmaShellSurface
- Permet passar un QIcon genèric a través de la interfície PlasmaWindow
- [servidor] Implementa la propietat de finestra genèrica a QtSurfaceExtension
- [client] Afegeix mètodes per obtenir la ShellSurface des d'una QWindow
- [servidor] Envia els apuntadors als esdeveniments a tots els recursos «wl_pointer» d'un client
- [servidor] No invoca «wl_data_source_send_send» si el DataSource està deslligat
- [servidor] Usa «deleteLater» quan es destrueixi una ClientConnection (error 370232)
- Implementa el funcionament del protocol de l'apuntador relatiu
- [servidor] Cancel·la la selecció prèvia del SeatInterface::setSelection
- [servidor] Envia els esdeveniments de tecla a tots els recursos «wl_keyboard» d'un client

### KWidgetsAddons

- Mou «kcharselect-generate-datafile.py» al subdirectori «src»
- Importa l'script «kcharselect-generate-datafile.py» amb l'historial
- Elimina la secció obsoleta
- Afegeix el dret de còpia Unicode i l'avís de permisos
- Soluciona l'avís: Manca sobreescriptura
- Afegeix els blocs de símbols SMP
- Esmena les referències a «Vegeu també»
- Afegeix els blocs Unicode que mancaven; millora l'ordenació (error 298010)
- Afegeix categories de caràcters al fitxer de dades
- Actualitza les categories Unicode a l'script de generació del fitxer de dades
- Ajusta el fitxer de generació del fitxer de dades per ser capaç d'analitzar els fitxers de dades Unicode 5.2.0
- Esmena l'adaptació posterior per generar traduccions
- Fer que l'script que genera el fitxer de dades per al kcharselect també gravi una traducció fictícia
- Afegeix l'script per a generar el fitxer de dades per al KCharSelect
- Aplicació KCharSelect nova (ara usa el giny «kcharselect» de les «kdelibs»)

### KWindowSystem

- Millora la reubicabilitat de l'exportació del CMake
- Afegeix la implementació per al «desktopFileName» al «NETWinInfo»

### KXMLGUI

- Permet l'ús del nou estil de connexió a «KActionCollection::add<a href="">Acció</a>»

### ModemManagerQt

- Esmena la inclusió de directori en el fitxer «pri»

### NetworkManagerQt

- Esmena la inclusió de directori en el fitxer «pri»
- Soluciona un error del «moc» per usar el Q_ENUMS a un espai de noms, amb la branca 5.8 de les Qt

### Frameworks del Plasma

- Assegura que l'OSD no té cap indicador «Dialog» (error 370433)
- Defineix les propietats del context abans de recarregar el QML (error 371763)
- No torna a analitzar el fitxer de metadades si ja s'ha carregat
- Esmena una fallada a «qmlplugindump» quan no hi ha cap QApplication disponible
- No mostra el menú «Alternatives» de manera predeterminada
- Nou booleà per usar el senyal «activated» com una commutació de «expanded» (error 367685)
- Esmena la construcció de «plasma-framework» amb les Qt 5.5
- [PluginLoader] Usa l'operador&lt;&lt; per «finalArgs» en lloc d'una llista d'inicialització
- Usa «kwayland» per al posicionament de les ombres i els diàlegs
- Icones que encara mancaven i millores a la xarxa
- Mou «availableScreenRect/Region» amunt a AppletInterface
- No carrega les accions dels contenidors per als contenidors incrustats (safates del sistema)
- Actualitzar la visibilitat de l'entrada del menú «Alternatives» de la miniaplicació segons es demani

### Solid

- Esmena l'ordenació inestable dels resultats de la consulta una altra vegada
- Afegeix una opció del CMake per commutar entre els gestors HAL i UDisks al FreeBSD
- Fa que el dorsal UDisks2 compili al FreeBSD (i, possiblement a altres UNIX)
- Windows: No mostra els diàlegs d'error (error 371012)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
