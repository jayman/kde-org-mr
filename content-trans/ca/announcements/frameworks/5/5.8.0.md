---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Frameworks nous:

- KPeople, proporciona accés a tots els contactes i la gent que els manté
- KXmlRpcClient, interacció amb els serveis XMLRPC

### General

- Diverses esmenes de construcció per compilar amb les properes Qt 5.5

### KActivities

- El servei de recursos de puntuació ara està finalitzat

### KArchive

- Evitar l'aturada en fitxers ZIP amb descriptors de dades redundants

### KCMUtils

- Restaurar «KCModule::setAuthAction»

### KCoreAddons

- KPluginMetadata: afegir implementació per claus ocultes

### KDeclarative

- Es prefereix exposar llistes al QML amb el QJsonArray
- No gestionar per defecte «devicePixelRatios» en les imatges
- Exposar «hasUrls» en el «DeclarativeMimeData»
- Permetre configurar als usuaris quantes línies horitzontals es dibuixen

### KDocTools

- Esmenar la construcció al MacOSX en usar Homebrew
- Millora de l'estil dels objectes de medis (imatges,...) en la documentació
- Codificar els caràcters no vàlids en camins usats en els DTD dels XML, evitant errors

### KGlobalAccel

- Activació dels segells de temps definits com una propietat dinàmica als QAction activats.

### KIconThemes

- Esmenar QIcon::fromTheme(xxx, unaAlternativa) que podria no retornar l'alternativa

### KImageFormats

- Fer que el lector d'imatges PSD sigui indiferent al «endian».

### KIO

- Fer obsolet l'UDSEntry::listFields i afegir el mètode UDSEntry::fields que retorna un QVector sense cap conversió costosa.
- Sincronitzar el bookmarkmanager només si el canvi era per aquest procés (error 343735)
- Esmenar l'engegada del servei de D-Bus del kssld5
- Implementar «quota-used-bytes» i «quota-available-bytes» del RFC 4331 per habilitar la informació de l'espai lliure en el «ioslave» http.

### KNotifications

- Retardar la inicialització de l'àudio fins que es necessiti realment
- Esmenar la configuració de les notificacions que no s'apliquen instantàniament
- Esmenar les notificacions d'àudio aturades després de la primera reproducció de fitxer

### KNotifyConfig

- Afegir una dependència opcional del QtSpeech per tornar a habilitar les notificacions pronunciades.

### KService

- KPluginInfo: acceptar llistes de cadenes com a propietats

### KTextEditor

- Afegir una estadística del nombre de paraules en la barra d'estat
- vimode: esmenar una fallada en eliminar la darrera línia en el mode de línia visual

### KWidgetsAddons

- Fer que el KRatingWidget funcioni amb el devicePixelRatio

### KWindowSystem

- KSelectionWatcher i KSelectionOwner es poden usar sense dependre del QX11Info.
- KXMessages es pot usar sense dependre del QX11Info

### NetworkManagerQt

- S'han afegit propietats i mètodes nous des del NetworkManager 1.0.0

#### Frameworks del Plasma

- Esmenar el plasmapkg2 per sistemes traduïts
- Millorar la disposició dels consells
- Possibilitar que els plasmoides carreguin scripts externs al paquet plasma...

### Canvis en el sistema de construcció (extra-cmake-modules)

- Ampliar la macro «ecm_generate_headers» per a permetre també les capçaleres «CamelCase.h»

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
