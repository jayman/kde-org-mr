---
aliases:
- ../../kde-frameworks-5.61.0
date: 2019-08-10
layout: framework
libCount: 70
---
### Baloo

- Enllaç contra «KIOCore» en lloc de «KIOWidgets» als esclaus KIO
- [IndexCleaner] ignora les entrades inexistents dins de la configuració

### BluezQt

- Soluciona la fallada a causa que el punter «q» no s'inicialitza mai
- No s'inclou «bluezqt_dbustypes.h» des de les capçaleres instal·lades

### Icones Brisa

- S'afegeix la icona «user-others» (error 407782)
- Es crea un enllaç simbòlic «edit-none» a «dialog-cancel»
- Se suprimeixen les versions redundants i monocromàtiques de les aplicacions d'Internet
- S'afegeixen les icones «view-pages-*», segons sigui necessari a l'Okular per a la selecció de la disposició de pàgina (error 409082)
- S'empren les fletxes en el sentit horari per a les icones «_refresh_» i «update-*» (error 409914)

### Mòduls extres del CMake

- Android: es permet substituir ANDROID_ARCH i ANDROID_ARCH_ABI com a variables d'entorn
- Es notifica als usuaris quan no estiguin usant KDE_INSTALL_USE_QT_SYS_PATHS sobre «prefix.sh»
- Es proporciona un valor predeterminat a «CMAKE_INSTALL_PREFIX» més sensat
- Es crea el tipus de construcció predeterminada «Debug» en compilar una comprovació del Git

### KActivitiesStats

- S'afegeix el terme «Date» a les estadístiques de «KActivities» per a filtrar la data de l'esdeveniment del recurs

### KActivities

- Se simplifica el codi d'activitat anterior/següent al «kactivities-cli»

### Eines de Doxygen del KDE

- S'han corregit els directoris de comprovació per a «metainfo.yaml» amb caràcters no ASCII amb el Python 2.7
- Registra els noms de camí incorrectes (a través de «repr())» en lloc de bloquejar-se totalment
- Genera sobre la marxa una llista dels fitxers de dades

### KArchive

- KTar::openArchive: no afirma si el fitxer té dos directoris arrel
- KZip::openArchive: no afirma en obrir fitxers trencats

### KCMUtils

- Adapta els canvis a la IU a «KPageView»

### KConfig

- Seguretat: elimina el funcionament de $(...) a les claus de la configuració amb el marcador [$e]
- Inclou la definició per a la classe emprada a la capçalera

### KCoreAddons

- S'afegeix la funció «KFileUtils::suggestName» per a suggerir un nom de fitxer únic

### KDeclarative

- Vista desplaçable: no omple l'element primari amb la vista (error 407643)
- S'introdueix «FallbackTapHandler»
- Servidor intermediari en QML del KRun: s'esmena la confusió entre camí/URL
- Calendari d'esdeveniments: es permet que els connectors mostrin els detalls de l'esdeveniment

### KDED

- Fitxer d'escriptori del kded5: s'utilitza un tipus vàlid (Service) per a suprimir l'avís des del kservice

### Compatibilitat amb les KDELibs 4

- Connector del «Designer»: s'usa consistentment «KF5» en els noms de grup i textos
- No anuncia usant «KPassivePopup»

### KDesignerPlugin

- Exposa el nou «KBusyIndicatorWidget»
- S'elimina la generació del connector del «Designer» per al «KF5WebKit»

### KDE WebKit

- S'usa la vista prèvia de «ECMAddQtDesignerPlugin» en lloc de «KF5DesignerPlugin»
- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### KFileMetaData

- S'obté l'actualització de l'extractor de mobipocket, però es manté desactivat

### KHolidays

- S'afegeixen els dies substituts dels dies festius a Rússia, per al 2019-2020
- S'actualitzen els festius russos

### KIconThemes

- Es restaura «Verifica si el grup &lt; LastGroup, ja que el KIconEffect no gestiona més l'UserGroup»

### KIO

- Es deixa obsolet «suggestName» a favor del de «KCoreAddons»
- Soluciona l'error en introduir el directori en alguns servidors FTP amb configuració regional turca (error 409740)

### Kirigami

- Es renova «Kirigami.AboutPage»
- Per consistència, usa «Units.toolTipDelay» en lloc de valors definits al codi
- Mida adequada per al contingut de la targeta quan la mida de la targeta està restringida
- S'oculta l'ondulació quan no volem que es pugui fer clic sobre els elements
- Es crea la maneta de manera que segueixi l'alçada arbitrària del calaix
- [element d'una llista amb lliscament] Es té en compte la visibilitat de la barra de desplaçament i el factor de forma per a les accions de la maneta i en línia
- S'elimina l'escalat de la unitat de mida de la icona per a «isMobile»
- Mostra sempre el botó enrere a les capes&gt;1
- Oculta les accions amb submenús des del menú més
- Posició predeterminada de «ActionToolBar» a la capçalera
- Una «z» gran per a no aparèixer en els diàlegs
- S'usa l'opacitat per ocultar els botons que no encaixen
- Només afegirà l'espaiador quan s'ompli l'amplada
- Es fa totalment retrocompatible amb «showNavigationButtons» com el booleà
- Més granularitat a «globalToolBar.showNavigationButtons»

### KItemModels

- En David Faure ara és el mantenidor de «KItemModels»
- KConcatenateRowsProxyModel: s'afegeix la nota que les Qt 5.13 proporcionen «QConcatenateTablesProxyModel»

### Framework del KPackage

- S'ofereix el «metadata.json» quan se sol·licitin les metadades del paquet
- PackageLoader: s'usa l'àmbit correcte per al «KCompressionDevice»

### KPeople

- declarativa: s'actualitza la llista d'accions quan es canvia la persona
- declarativa: no cau en fallada quan s'utilitza incorrectament l'API
- Model de persones: S'afegeix el número de telèfon

### KService

- Exposa «X-KDE-Wayland-Interfaces»
- Esmena la construcció del «KService» a l'Android
- KService: s'elimina el concepte trencat de la base de dades global «sycoca»
- S'elimina un codi de supressió molt perillós amb «kbuildsycoca5 --global»
- Esmena la recursió infinita i afirma quan l'usuari no pot llegir la base de dades «sycoca» (p. ex., propietat de l'arrel)
- Es fa obsoleta «KDBusServiceStarter». Tot el seu ús al kdepim ja no existeix, l'activació de D-Bus és una millor solució
- Es permet que «KAutostart» es construeixi usant un camí absolut

### KTextEditor

- Desa i carrega els marges de pàgina
- No persisteix en l'autenticació
- Es torna a assignar la drecera predeterminada «Canvia el mode de l'entrada» perquè no entri en conflicte amb «konsolepart» (error 409978)
- Es fa que el model de compleció de paraules clau retorni «HideListIfAutomaticInvocation» de manera predeterminada
- Mapa en miniatura: no enregistra el clic del botó esquerre del ratolí en els botons amunt/avall
- Es permeten intervals de fins a 1024 hl en lloc de no ressaltar la línia si s'arriba a aquest límit
- Soluciona el plegat de les línies amb la posició final a la columna 0 d'una línia (error 405197)
- Afegeix l'opció per a tractar alguns caràcters també com a «parèntesi automàtic» només quan es tingui una selecció
- S'afegeix una acció per inserir una línia nova sense sagnia (error 314395)
- S'afegeix la configuració per habilitar/inhabilitar arrossegar i deixar anar el text (activat de manera predeterminada)

### KUnitConversion

- S'afegeixen les unitats de dades binàries (bits, kilobytes, kibibytes... yottabytes)

### Framework del KWallet

- Es mou abans la inicialització del «kwalletd» (error 410020)
- S'elimina completament l'agent de migració del KDE 4 (error 400462)

### KWayland

- Empra els protocols de Wayland

### KWidgetsAddons

- S'introdueix el concepte de capçalera i peu de pàgina al «kpageview»
- [Indicador d'ocupat] la durada de fer la coincidència de la versió a l'estil de l'escriptori QQC2
- S'afegeix un diàleg d'avís amb una secció de detalls plegable
- Classe nova «KBusyIndicatorWidget» similar a «BusyIndicator» del «QtQuick»

### KWindowSystem

- [plataformes/XCB] s'usa l'extensió XRES per obtenir el PID real de la finestra (error 384837)
- Adapta «KXMessages» per eliminar «QWidget»

### KXMLGUI

- S'afegeixen espaiadors expansibles com a una opció de personalització per a les barres d'eines
- S'usen icones d'acció monocromàtiques per als botons de «KAboutData»
- S'elimina la connexió «visibilityChanged» a favor de l'existent «eventFilter»

### ModemManagerQt

- Es permet actualitzar el temps d'espera predeterminat de D-Bus a cada interfície

### NetworkManagerQt

- dispositiu: s'inclou «reapplyConnection()» a la interfície

### Frameworks del Plasma

- [estil per al botó de l'eina] s'usa el mateix grup de colors per a l'estat en passar per damunt
- Es maneja el fitxer de colors en un instal·lador de temes fals de Plasma
- S'instal·la el tema de Plasma al «XDG_DATA_DIR» local per a la prova de les icones
- S'aplica el canvi en la durada de l'indicador d'ocupat de «D22646» a l'estil del QQC2
- Es compilen els connectors de l'estructura del paquet a dins del subdirectori esperat
- Es canvia «Highlight» a «ButtonFocus»
- Soluciona l'execució de «dialognativetest» sense instal·lar
- Se cerca el connector de l'altre plasmoide
- [Indicador d'ocupat] la durada de fer la coincidència de la versió a l'estil de l'escriptori QQC2
- S'afegeixen els connectors que manquen a «org.kde.plasma.components 3.0»
- Es refresquen les icones d'actualització i es reinicia per a reflectir les noves versions de les icones Brisa (error 409914)
- «itemMouse» no està definit en el «plasma.components 3.0»
- Empra «clearItems» quan se suprimeix una miniaplicació
- Soluciona la fallada si el «switchSize» s'ajusta durant la configuració inicial
- Millora la memòria cau del connector

### Purpose

- Phabricator: obre automàticament un diff nou en el navegador
- Soluciona l'extracció. Pedaç per en Victor Ryzhykh

### QQC2StyleBridge

- Soluciona la protecció trencada que evita l'estil dels controls lliscants amb valors negatius
- Fa disminuir la velocitat de gir per a l'indicador d'ocupat
- Soluciona l'«Error de tipus» en crear un «TextField» amb focus: cert
- [quadre combinat] s'estableix la política de tancament per a tancar en fer clic a fora, en lloc de només fora del pare (error 408950)
- [botó de selecció de valors] s'estableix el «renderType» (error 409888)

### Solid

- S'assegura que els dorsals de Solid siguin reentrants

### Ressaltat de la sintaxi

- TypeScript: s'esmenen les paraules clau a les expressions condicionals
- S'esmenen els camins del generador i prova de CMake
- S'afegeix suport per a paraules clau de QML addicionals que no formen part de JavaScript
- Actualitza el ressaltat de CMake

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
