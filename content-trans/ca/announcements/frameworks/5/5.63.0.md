---
aliases:
- ../../kde-frameworks-5.63.0
date: 2019-10-14
layout: framework
libCount: 70
---
### Icones Brisa

- Millora la icona del KFloppy (error 412404)
- Afegeix les icones de les accions «format-text-underline-squiggle» (error 408283)
- Afegeix la icona «preferences-desktop-filter» a color (error 406900)
- Afegeix icona d'aplicació per a l'aplicació Kirogi de control de drons
- S'han afegit scripts per crear un tipus de lletra web de totes les icones d'acció del Brisa
- Afegeix les icones «enablefont» i «disablefont» per al KCM del «kfontinst»
- Corregeix les icones «system-reboot» grans que giren en una direcció incoherent (error 411671)

### Mòduls extres del CMake

- Mòdul ECMSourceVersionControl nou
- Corregeix el FindEGL en usar Emscripten
- ECMAddQch: afegeix l'argument INCLUDE_DIRS

### Integració del marc de treball

- Assegura que «winId()» no es crida en ginys no natius (error 412675)

### Kcalendarcore

Mòdul nou, conegut anteriorment com a «kcalcore» a «kdepim»

### KCMUtils

- Suprimeix esdeveniments de ratolí en els KCM que provocaven moviments a les finestres
- Ajusta els marges del «KCMultiDialog» (error 411161)

### KCompletion

- [KComboBox] Desactiva adequadament el completador integrat de les Qt [correcció de regressió]

### KConfig

- Corregeix la generació de propietats que comencen per una lletra majúscula

### KConfigWidgets

- Fa que KColorScheme sigui compatible amb QVariant

### Kcontacts

Mòdul nou, anteriorment formava part del KDE PIM

### KCoreAddons

- Afegeix KListOpenFilesJob

### KDeclarative

- Suprimeix el context de QQmlObjectSharedEngine en sincronitzar amb QQmlObject
- [KDeclarative] Adapta des del QWheelEvent::delta() obsolet a «angleDelta()»

### Compatibilitat amb les KDELibs 4

- Admet el NetworkManager 1.20 i fa que compili realment el dorsal del NM

### KIconThemes

- Fa obsolets els mètodes globals [Small|Desktop|Bar]Icon()

### KImageFormats

- Afegeix fitxers per proves (error 411327)
- xcf: corregeix una regressió en llegir fitxers amb propietats «no implementades»
- xcf: llegeix adequadament la resolució de la imatge
- Adapta el carregador de la imatge HDR (Radiance RGBE) a les Qt5

### KIO

- [Places panel] Renova la secció Desats recentment
- [DataProtocol] Compila sense conversió implícita des d'ASCII
- Considera l'ús dels mètodes del WebDAV com a suficient per assumir WebDAV
- REPORT també admet la capçalera Depth
- Fa reusable la conversió QSslError::SslError &lt;-&gt; KSslError::Error
- Fa obsolet el «ctor» de KSslError::Error del KSslError
- [Windows] Corregeix el llistat del directori pare de C:foo, que és C: i no C:
- Corregeix una fallada en sortir al «kio_file» (error 408797)
- Afegeix els operadors == i != al KIO::UDSEntry
- Substitueix KSslError::errorString per QSslError::errorString
- Treball de moure/copia: omet l'«stat» dels orígens si no es pot escriure al directori de destinació (error 141564)
- Corregeix la interacció amb els executables DOS/Windows a KRun::runUrl
- [KUrlNavigatorPlacesSelector] Identifica adequadament l'acció de tancar/retrocedir (error 403454)
- KCoreDirLister: corregeix una fallada en crear carpetes noves des del «kfilewidget» (error 401916)
- [kpropertiesdialog] Afegeix icones a la secció mida
- Afegeix icones per als menús «Obre amb» i «Accions»
- Evita inicialitzar una variable innecessària
- Mou més funcionalitats de KRun::runCommand/runApplication a KProcessRunner
- [Advanced Permissions] Corregeix els noms de les icones (error 411915)
- [KUrlNavigatorButton] Corregeix l'ús de QString per a no usar [] fora dels límits
- Fa que KSslError mantingui internament un QSslError
- Divideix KSslErrorUiData a partir de KTcpSocket
- Adapta «kpac» a partir del QtScript

### Kirigami

- Sempre posa l'últim element a la memòria cau
- Més z (error 411832)
- Corregeix la versió d'importació al PagePoolAction
- PagePool és al Kirigami 2.11
- Té en compte la velocitat d'arrossegament quan acaba un lliscament
- Corregeix la còpia d'URL al porta-retalls
- Comprova més si l'element real s'està rejerarquitzant
- Implementació bàsica per a les accions de ListItem
- Presenta «cachePages»
- Corregeix la compatibilitat amb les Qt5.11
- Presenta PagePoolAction
- Classe nova: PagePool per gestionar el reciclatge de pàgines després que s'hagin mostrat
- Fa que les barres de pestanyes semblin millors
- Una mica de marge a la dreta (error 409630)
- Reverteix «Compensa les mides de les icones més petites al mòbil a l'ActionButton»
- Fa que els elements de la llista no semblin inactius (error 408191)
- Reverteix «S'elimina l'escalat de la unitat de mida de la icona per a «isMobile»»
- Cal que el client faci Layout.fillWidth (error 411188)
- Afegeix una plantilla per al desenvolupament d'una aplicació Kirigami
- Afegeix un mode per centrar les accions i ometre el títol en usar un estil ToolBar (error 402948)
- Compensa les mides de les icones més petites al mòbil a l'ActionButton
- S'han corregit diversos errors en temps d'execució de propietats no definides
- Corregeix el color de fons de ListSectionHeader per a diversos esquemes de color
- Elimina l'element de contingut personalitzat del separador d'ActionMenu

### KItemViews

- [KItemViews] Adaptació a una API no obsoleta de QWheelEvent

### KJobWidgets

- Neteja aviat els objectes relacionats amb el D-Bus per a evitar penjar-se en sortir del programa

### KJS

- S'han afegit les funcions String del JS «startsWith()», «endsWith()» i «includes()»
- S'ha corregit Date.prototype.toJSON() cridada des d'objectes no-Date

### KNewStuff

- Porta la paritat de funcionalitats del KNewStuffQuick amb KNewStuff(Widgets)

### KPeople

- Reclama l'Android com a una plataforma admesa
- Desplega l'avatar predeterminat via «qrc»
- Empaqueta els fitxers de connectors a l'Android
- Desactiva les peces de D-Bus a l'Android
- Corregeix una fallada en controlar un contacte que s'elimina a PersonData (error 410746)
- Usa tipus qualificats complets als senyals

### KRunner

- Considera els camins UNC com a context de NetworkShare

### KService

- Mou els Passatemps al directori Jocs en lloc del Jocs i joguines (error 412553)
- [KService] Afegeix un constructor de còpia
- [KService] Afegeix «workingDirectory()», fa obsolet «path()»

### KTextEditor

- Intenta evitar defectes visuals a la vista prèvia del text
- Expansió de variables: Usa std::function internament
- QRectF en lloc de QRect soluciona els problemes de retall (error 390451)
- El següent defecte visual de renderització marxa si s'ajusta una mica el retall «rect» (error 390451)
- Evita la màgia de triar el tipus de lletra i desactiva l'antialiàsing (error 390451)
- KadeModeMenuList: corregeix fuites de memòria i altres
- Intenta explorar els tipus de lletra usables, funciona raonablement bé si no s'usen factors d'escalat ximples com 1.1
- Menú de mode de la barra d'estat: reusa el QIcon buit que és compartit de manera implícita
- Exposa KTextEditor::MainWindow::showPluginConfigPage()
- Substitueix QSignalMapper per «lambda»
- KateModeMenuList: usa QString() per a cadenes buides
- KateModeMenuList: afegeix la secció «Millors cerques coincidents» i correccions per al Windows
- Expansió de variables: permet QTextEdits
- Afegeix una drecera de teclat per commutar els mètodes d'entrada al menú d'edició (error 400486)
- Diàleg d'expansió de variables: gestiona adequadament els canvis de selecció i activació d'elements
- Diàleg d'expansió de variables: afegeix l'edició de filtre de línies
- Còpia de seguretat en desar: accepta substitucions de cadenes de data i hora (error 403583)
- Expansió de variables: prefereix un valor de retorn sobre un argument de retorn
- Començament inicial de diàleg de variables
- Usa el nou format de l'API

### Framework del KWallet

- Suport per HiDPI

### KWayland

- Ordena alfabèticament els fitxers a la llista del CMake

### KWidgetsAddons

- Fa configurable el botó D'acord a KMessageBox::sorry/detailedSorry
- [KCollapsibleGroupBox] Corregeix l'avís del QTimeLine::start en temps d'execució
- Millora els noms dels mètodes d'icona del KTitleWidget
- Afegeix «setters» de QIcon per als diàlegs de contrasenya
- [KWidgetAddons] Adaptació a una API no obsoleta de les Qt

### KWindowSystem

- Defineix l'XCB com a requerit si es construeix el dorsal de les X
- Fa menys ús de l'àlies del numerador obsolet NET::StaysOnTop

### KXMLGUI

- Mou l'element «Mode de pantalla completa» del menú d'Arranjament al menú de Vista (error 106807)

### NetworkManagerQt

- ActiveConnection: connecta el senyal «stateChanged()» a la interfície correcta

### Frameworks del Plasma

- Exporta la categoria d'enregistrament «Plasma core lib», afegeix una categoria a un qWarning
- [pluginloader] Usa un enregistrament categoritzat
- Fa «editMode» una propietat global del Corona
- Respecta el factor global de velocitat d'animació
- Instal·la adequadament tot el «plasmacomponent3»
- [Dialog] Aplica el tipus de finestra després de canviar els indicadors
- Canvia la lògica del botó de revelació de contrasenya
- Corregeix una fallada en tancar/retrocedir amb el ConfigLoader de la miniaplicació (error 411221)

### QQC2StyleBridge

- Corregeix diversos errors del sistema de construcció
- Pren els marges del «qstyle»
- [Tab] Corregeix la mida (error 409390)

### Ressaltat de la sintaxi

- Afegeix el ressaltat de sintaxi per al RenPy (.rpy) (error 381547)
- Regla del WordDetect: detecta delimitadors a la vora interna de la cadena
- Ressaltat dels fitxers GeoJSON com si fossin JSON normals
- Afegeix el ressaltat de sintaxi per als subtítols SubRip Text (SRT)
- Corregeix «skipOffset» amb una expressió regular dinàmica (error 399388)
- Bitbake: gestiona l'intèrpret d'ordres i el Python incrustat
- Jam: corregeix un identificador a SubRule
- Afegeix la definició de sintaxi per al Perl6 (error 392468)
- Permet l'extensió «.inl» per al C++, no s'usa en altres fitxers XML de moment (error 411921)
- Admet «*.rej» per al ressaltat de diferències (error 411857)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
