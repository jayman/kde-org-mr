---
aliases:
- ../../kde-frameworks-5.68.0
date: 2020-03-07
layout: framework
libCount: 70
---
### Baloo

- [ModifiedFileIndexer] Verificacions de l'hora correcta per als fitxers nous
- [ModifiedFileIndexer] Omet executar BasicIndexingJob quan no es requereix
- Sincronitza IndexerConfig en sortir (error 417127)
- [FileIndexScheduler] Força l'avaluació de l'«indexerState» després de suspendre/reprendre

### BluezQt

- Corregeix els errors al commit d'adaptació de QRegularExpression

### Icones Brisa

- Afegeix la icona «network-wireless-hotspot»
- Mou les icones del plafó del «telegram» a la categoria d'estat
- [breeze-icons] Afegeix les icones «telegram-desktop» de la safata (error 417583)
- [breeze-icons] Icona nova de «telegram» de 48px
- Afegeix icones «rss» a les accions
- Elimina les icones de «telegram» de 48px
- Revisió per assegurar que la validació no es fa en paral·lel amb la generació
- Logo/icona nou del Yakuake
- Corregeix les incoherències i duplicats a les icones «network-wired/wireless»
- Corregeix els valors de color del text de les icones osd-*
- Només instal·la les icones generades si s'han generat
- Escapa tots els camins per assegurar que funciona el sistema de CI
- Estableix «-e» a l'script generador per tal que es mostrin adequadament els errors
- Construcció: corregeix la construcció quan el prefix d'instal·lació no és escrivible per l'usuari
- Adapta el generador nou de 24x per usar el «bash» en lloc del «sh»
- També genera automàticament els enllaços simbòlics de compatibilitat amb 24@2x
- Genera automàticament les icones monocromes de 24px
- Afegeix les icones que només estan a «actions/24» a «actions/22»
- Estableix l'escala del document a 1,0 per a totes les icones d'«actions/22»
- Afegeix les icones <code>smiley-add</code> noves
- Fa coherents les icones «shape» i «shape-choose» amb les altres icones «-shape»
- Fa coherent la icona «smiley-shape» amb les altres icones «-shape»
- Fa coherents les icones «flower-shape» i «hexagon-shape» amb les altres icones «-shape»
- Substitueix &lt;use/&gt; per &lt;path/&gt; a «muondiscover.svg»
- Afegeix icones d'estat: «data-error», «data-warning», «data-information»
- Afegeix una icona per a «org.kde.Ikona»
- Afegeix la icona del «vvave»
- Afegeix la icona del «puremaps»
- Unifica l'aspecte de totes les icones que contenen 🚫 (signe no)
- Icona nova per al KTimeTracker (error 410708)
- Optimitza les icones KTrip i KDE Itinerary
- Actualitza les icones «travel-familiy»

### Mòduls extres del CMake

- Accepta NDK r20 i les Qt 5.14
- Carrega els fitxers QM a partir dels actius: els URL a l'Android
- Afegeix «ecm_qt_install_logging_categories» i «ecm_qt_export_logging_category»
- ECMGeneratePriFile: corregeix per als usos amb LIB_NAME que no és un nom d'objectiu
- ECMGeneratePriFile: corregeix les configuracions estàtiques

### Integració del marc de treball

- [KStyle] Estableix el color dels KMessageWidgets al correcte a partir de l'esquema de color actual

### KActivities

- Corregeix un problema en cercar els directoris d'inclusió del Boost
- Usa els mètodes exposats de D-Bus per a commutar les activitats a la CLI

### KAuth

- [KAuth] Afegeix la implementació per als detalls d'acció al dorsal del Polkit1
- [policy-gen] Corregeix el codi per usar realment el grup correcte de captura
- Elimina el dorsal del Policykit
- [polkit-1] Simplifica la cerca d'existència de l'acció del Polkit1Backend
- [polkit-1] Retorna un estat d'error a «actionStatus» si hi ha un error
- Calcula KAuthAction::isValid a demanda

### KBookmarks

- Reanomena les accions per a ser coherents

### KCalendarCore

- Actualitza la visibilitat de la memòria cau en canviar la visibilitat de la llibreta de notes

### KCMUtils

- Comprova l'«activeModule» abans d'usar-lo (error 417396)

### KConfig

- [KConfigGui] Neteja la propietat «styleName» del tipus de lletra per als estils de lletra Regular (error 378523)
- Corregeix la generació del codi per a les entrades amb min/max (error 418146)
- KConfigSkeletonItem: permet establir un KconfigGroup per a llegir i escriure elements als grups imbricats
- Corregeix la propietat «is&lt;PropertyName&gt;Immutable» generada
- Afegeix «setNotifyFunction» a KPropertySkeletonItem
- Afegeix un «is&lt;PropertyName&gt;Immutable» per conèixer si una propietat és immutable

### KConfigWidgets

- Canvia «Redisplay» per «Refresh»

### KCoreAddons

- Afegeix el consell que el QIcon també es pot usar com un logo del programa

### KDBusAddons

- Fa obsolet «KDBusConnectionPool»

### KDeclarative

- Exposa el senyal de captura a KeySequenceItem
- Corregeix la mida de la capçalera a «GridViewKCM» (error 417347)
- Permet que la classe derivada ManagedConfigModule registri explícitament KCoreConfigSkeleton
- Permet usar KPropertySkeletonItem al ManagedConfigModule

### KDED

- Afegeix l'opció «--replace» al «kded5»

### Complements de la IGU del KDE

- [UrlHandler] Gestiona l'obertura de documents en línia als mòduls KCM
- [KColorUtils] Canvia l'interval de to de «getHcy()» a [0.0, 1.0]

### KHolidays

- Actualitza els festius japonesos
- holiday_jp_ja - corregeix un error de tecleig del Dia Nacional de la Fundació (error 417498)

### KI18n

- Admet les Qt 5.14 a l'Android

### KInit

- Fa que «kwrapper/kshell» activin el «klauncher5» si cal

### KIO

- [KFileFilterCombo] No afegeix un QMimeType no vàlid al filtre de MIME (error 417355)
- [src/kcms/*] Substitueix un «foreach» (obsolet) per un «for» basat en «range/index»
- KIO::iconNameForUrl(): gestiona el cas d'un fitxer/carpeta sota «trash:/»
- [krun] Implementació compartida de «runService» i «runApplication»
- [krun] Elimina el suport de KToolInvocation de KRun::runService
- Millora KDirModel per evitar mostrar «+» si no hi ha cap subdirectori
- Corregeix l'execució del Konsole al Wayland (error 408497)
- KIO::iconNameForUrl: corregeix la cerca de les icones de protocol del KDE (error 417069)
- Majúscula correcta de l'element «basic link»
- Canvia «AutoSkip» a «Skip All» (error 416964)
- Corregeix una fuita de memòria a KUrlNavigatorPlacesSelector::updateMenu
- File ioslave: atura la copia tan aviat es mati l'«ioslave»
- [KOpenWithDialog] Selecciona automàticament el resultat si el filtre de model només té una coincidència (error 400725)

### Kirigami

- Mostra el consell d'eina amb l'URL complet per al botó d'URL amb el text substituït
- Possibilita tenir barres d'eines retràctils a les pàgines amb desplaçament també per als peus de pàgina
- Corregeix el comportament de PrivateActionToolButton amb «showText» vs «IconOnly»
- Corregeix les ActionToolBar/PrivateActionToolButton en combinació amb l'acció QQC2
- Mou l'element de menú activat sempre dins l'interval
- Vigila els esdeveniments de canvi d'idioma, i els reenvia al motor QML
- Admet les Qt 5.14 a l'Android
- No té OverlaySheets a sota de la capçalera de la pàgina
- Usa la reserva quan la icona falla en carregar
- Enllaços que manquen als fitxers de codi font de la «pagepool»
- Icon: corregeix la renderització de la imatge dels URL amb PPP alt (error 417647)
- No falla quan l'amplada o l'alçada és 0 (error 417844)
- Corregeix els marges a OverlaySheet
- [examples/simplechatapp] Sempre estableix «isMenu» a «true»
- [RFC] Redueix la mida de les capçaleres de nivell 1 i incrementa el farciment esquerre als títols de la pàgina
- Sincronitza adequadament els consells de mida amb la màquina d'estat (error 417351)
- Afegeix la implementació per als connectors «platformtheme» estàtics
- Alinea correctament «headerParent» quan hi ha una barra de desplaçament
- Corregeix el càlcul de l'amplada de la barra de pestanyes
- Afegeix PagePoolAction al fitxer QRC
- Permet l'estil de la barra d'eines al mòbil
- Fa que els documents de l'API reflecteixin que el Kirigami no és només un joc d'eines per a mòbils

### KItemModels

- KRearrangeColumnsProxyModel: desactiva temporalment l'asserció per un error a QTreeView
- KRearrangeColumnsProxyModel: reinici a «setSourceColumns()»
- Mou el SortFilterProxyModel del Plasma al connector KItemModel del QML

### KJS

- Exposa les funcions d'avaluació de la gestió del temps de venciment a l'API pública

### KNewStuff

- Corregeix el clic en el delegat de només miniatura (error 418368)
- Corregeix el desplaçament a la pàgina d'EntryDetails (error 418191)
- No suprimeix CommentsModel per duplicat (error 417802)
- També cobreix el connector del «qtquick» al fitxer instal·lat de categories
- Usa el catàleg correcte de traduccions per mostrar-les
- Corregeix el títol de tancament del diàleg del KNSQuick i la disposició bàsica (error 414682)

### KNotification

- Fa disponible el «kstatusnotifieritem» sense el D-Bus
- Adapta la numeració de les accions a l'Android per a funcionar com al «KNotifications»
- Dona de baixa al Kai-Uwe com a mantenidor del KNotifications
- Elimina sempre l'HTML si el servidor no ho admet
- [android] Emet «defaultActivated» en tocar la notificació

### KPeople

- Corregeix la generació del fitxer «pri»

### KQuickCharts

- No imprimeix els errors quant als rols no vàlids quan no s'ha definit «roleName»
- Usa una plataforma fora de pantalla per a les proves al Windows
- Elimina la baixada del validador «glsl» de l'script de validació
- Corregeix un error de validació al «shader» de la línia del diagrama
- Actualitza el «shader» del perfil del nucli de la línia del diagrama per coincidir amb la compatibilitat
- Afegeix un comentari quant a la comprovació dels límits
- LineChart: afegeix la implementació per a comprovació dels límits min/max de «y»
- Afegeix la funció «sdf_rectangle» a la biblioteca «sdf»
- [linechart] Es protegeix contra la divisió per zero
- Diagrames de línies: redueix el nombre de punts per segment
- No perd punts al final d'un diagrama de línies

### Kross

- Qt5::UiTools no és opcional en aquest mòdul

### KService

- Mecanisme nou de consulta d'aplicacions: KApplicationTrader

### KTextEditor

- Afegeix una opció per a la separació dinàmica dins de paraules
- KateModeMenuList: no superposa la barra de desplaçament

### KWayland

- Afegeix els camins de D-Bus del menú d'aplicació a la interfície «org_kde_plasma_window»
- Registre: no destrueix la crida inversa a la sincronització global
- [surface] Corregeix el desplaçament de la memòria intermèdia en adjuntar memòries intermèdies a les superfícies

### KWidgetsAddons

- [KMessageWidget] Permet que l'estil canviï la paleta
- [KMessageWidget] El dibuixa amb el QPainter en lloc d'usar fulls d'estils
- Redueix lleugerament la mida de la capçalera de nivell 1

### ModemManagerQt

- Elimina la generació del fitxer «pri» del QMake i la instal·lació, trencada actualment

### NetworkManagerQt

- Admet SAE a «securityTypeFromConnectionSetting»
- Elimina la generació del fitxer «pri» del QMake i la instal·lació, trencada actualment

### Icones de l'Oxygen

- Admet «data-error/warning/information» també a les mides 32, 46, 64, 128
- Afegeix l'element d'acció «plugins», per a coincidir les icones del Brisa
- Afegeix icones d'estat: «data-error», «data-warning», «data-information»

### Frameworks del Plasma

- Botons: permet ampliar l'escala de les icones
- Intenta aplicar l'esquema de colors del tema actual a QIcons (error 417780)
- Diàleg: desconnecta dels senyals de la QWindow al destructor
- Corregeix una fuita de memòria a ConfigView i a Dialog
- Corregeix els consells de la mida de la disposició per a les etiquetes de botons
- Assegura que els consells de la mida són enters i parells
- Implementa «icon.width/height» (error 417514)
- Elimina els colors al codi font (error 417511)
- Construeix NullEngine amb KPluginMetaData() (error 417548)
- Redueix lleugerament la mida de la capçalera de nivell 1
- Centra verticalment la icona/imatge del consell d'eina
- Implementa la propietat de visualització dels botons
- No avisa per a les metadades no vàlides de connector (error 412464)
- Els consells d'eina sempre tenen el grup de color normal
- [Tests] Fa que «radiobutton3.qml» usi PC3
- Optimitza el codi en deixar anar fitxers a l'escriptori (error 415917)

### Prison

- Corregeix el fitxer «pri» perquè no falli amb inclusions CamelCase
- Corregeix el fitxer «pri» per a tenir el nom del QMake del QtGui com a dependència

### Purpose

- Reescriu el connector de Nextcloud
- Mata l'acceptació de Twitter

### QQC2StyleBridge

- ScrollView: usa l'alçada de la barra de desplaçament com a farciment inferior, no l'amplada

### Solid

- Corregeix la lògica inversa a IOKitStorage::isRemovable

### Sonnet

- Corregeix una falla de segment en sortir

### Ressaltat de la sintaxi

- Corregeix una manca de memòria a causa de piles de context massa grans
- Actualització general del ressaltat de la sintaxi del CartoCSS
- Afegeix el ressaltat de sintaxi per a les Java Properties
- TypeScript: afegeix camps privats i importacions/exportacions de només tecleig, i diverses correccions
- Afegeix l'ampliació FCMacro del FreeCAD a la definició de ressaltat del Python
- Actualitzacions per al CMake 3.17
- C++: paraula clau «constinit» i sintaxi «std::format» per a cadenes. Millora del format «printf»
- RPM spec: diverses millores
- Ressaltat del Makefile: corregeix el nom de les variables a les condicions «else» (error 417379)
- Afegeix el ressaltat de sintaxi per al Solidity
- Petites millores a diversos fitxers XML
- Ressaltat del Makefile: afegeix les substitucions (error 416685)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
