---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### General

Molts mòduls ara tenen vinculacions del Python.

### Attica

- Afegeix la implementació per a «display_name» a les categories

### Icones Brisa

- Massa canvis a les icones per llistar-los aquí

### Mòduls extres del CMake

- Activa «-Wsuggest-override» per a g++ &gt;= 5.0.0
- Passa «-fno-operator-names» quan s'admet
- ecm_add_app_icon: Ignora en silenci els fitxers SVG quan no s'admeten
- Vinculacions: Moltes correccions d'error i millores

### Integració del marc de treball

- Admetre diverses preguntes del KNSCore usant notificacions

### KArchive

- Esmena perquè KCompressionDevice (cerca) funcioni amb les Qt &gt;= 5.7

### KAuth

- Actualitza la majoria dels exemples, descarta els obsolets

### KConfig

- Soluciona l'enllaçat al Windows: no enllaça «kentrymaptest» amb el «KConfigCore»

### KConfigWidgets

- No fa res a ShowMenubarActionFilter::updateAction si no hi ha cap barra de menús

### KCoreAddons

- Soluciona l'error 363427 - Caràcters insegurs analitzats incorrectament com a part d'un URL (error 363427)
- kformat: Fa possible traduir adequadament dates relatives (error 335106)
- KAboutData: Documenta que l'adreça de correu d'error també pot ser un URL

### KDeclarative

- [IconDialog] Defineix el grup d'icones adequat
- [QuickViewSharedEngine] Usa «setSize» en lloc de «setWidth/setHeight»

### Compatibilitat amb les KDELibs 4

- S'ha sincronitzat el KDE4Defaults.cmake de les «kdelibs»
- Soluciona la verificació HAVE_TRUNC del CMake

### KEmoticons

- KEmoticons: Usa el D-Bus per a notificar als processos en execució dels canvis efectuats al KCM
- KEmoticons: Gran millora de rendiment

### KIconThemes

- KIconEngine: Centra la icona al rectangle requerit

### KIO

- Afegeix KUrlRequester::setMimeTypeFilters
- Soluciona l'anàlisi del llistat de directoris en un servidor FTP específic (error 375610)
- Conserva el grup/propietari en copiar fitxers (error 103331)
- KRun: Fa obsolet «runUrl()» a favor de «runUrl()» amb RunFlags
- KSSL: Assegura que el directori de certificats d'usuari estigui creat abans d'usar (error 342958)

### KItemViews

- Aplica immediatament el filtre a l'intermediari

### KNewStuff

- Fa possible adoptar recursos, principalment per a paràmetres globals del sistema
- No falla en moure al directori temporal en instal·lar
- Fa obsoleta la classe de seguretat
- No bloqueja en executar l'ordre de postinstal·lació (error 375287)
- [KNS] Té en compte el tipus de distribució
- No pregunta si s'ha obtingut el fitxer de /tmp

### KNotification

- Torna a afegir les notificacions de registre als fitxers (error 363138)
- Marca les notificacions no persistents com a transitòries
- Accepta les «accions predeterminades»

### Framework del KPackage

- No genera «appdata» si està marcat com a NoDisplay
- Soluciona el llistat quan el camí requerit és absolut
- Soluciona la gestió d'arxius amb una carpeta en ells (error 374782)

### KTextEditor

- Soluciona la renderització de minimapes per entorns HiDPI

### KWidgetsAddons

- Afegeix mètodes per ocultar accions de revelació de contrasenya
- KToolTipWidget: No prendre la propietat del contingut del giny
- KToolTipWidget: Oculta immediatament si el contingut es destrueix
- Soluciona la substitució del focus a KCollapsibleGroupBox
- Soluciona un avís en destruir un KPixmapSequenceWidget
- Instal·la també les capçaleres «CamelCase» per a les classes des de les capçaleres multiclasse
- KFontRequester: Cerca la coincidència més propera si manca un tipus de lletra (error 286260)

### KWindowSystem

- Permet que la Tabulació sigui modificada per Majúscules (error 368581)

### KXMLGUI

- Informador d'errors: Permet un URL (no només una adreça de correu) per als informes personalitzats
- Omet les dreceres buides en les verificacions d'ambigüitat

### Frameworks del Plasma

- [Interfície de Contenidors] No cal «values()», ja que «contains()» cerca les claus
- Diàleg: Oculta quan el focus canvia a ConfigView amb «hideOnWindowDeactivate»
- [Menú de PlasmaComponents] Afegeix la propietat «maximumWidth»
- Icona que mancava en connectar-se a «openvpn» via una xarxa Bluetooth (error 366165)
- Assegura que es mostra el ListItem activat en passar-hi per sobre
- Fa que totes les alçades de la capçalera del calendari siguin senars (error 375318)
- Soluciona l'estil del color a la icona de xarxa del Plasma (error 373172)
- Defineix «wrapMode» a Text.WrapAnywhere (error 375141)
- Actualitza la icona del KAlarm (error 362631)
- Reenviament correcte de l'estat des de les miniaplicacions al contenidor (error 372062)
- Usa KPlugin per carregar els connectors del Calendari
- Usa el color de ressaltat per al text seleccionat (error 374140)
- [Icon Item] Arrodoneix la mida en carregar-hi un mapa de píxels
- La propietat «portrait» no és important quan no hi ha text (error 374815)
- Soluciona les propietats del «renderType» per a diversos components

### Solid

- Esmena de l'error de recuperació de les propietats de D-Bus (error 345871)
- Tracta la no frase de pas com un error de Solid::UserCanceled

### Sonnet

- Afegeix el fitxer de dades de trigrames grecs
- Soluciona una falla de segment a la generació de trigrames i exposa la constant MAXGRAMS a la capçalera
- Cerca la libhunspell.so no versionada, serà més apropiat de cara al futur

### Ressaltat de la sintaxi

- Ressaltat del C++: actualització a les Qt 5.8

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
