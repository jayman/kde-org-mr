---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE випущено збірку програм KDE 16.08.0
layout: application
title: KDE випущено збірку програм KDE 16.08.0
version: 16.08.0
---
18 серпня 2016 року. Сьогодні командою KDE випущено Програми KDE 16.08 із вагомими оновленнями, які роблять набір програм доступнішим, функціональнішим та позбавленим незначних вад. Отже, Програми KDE у новій версії стали ближчими до досконалого набору програм для вашого комп'ютерного пристрою.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> і KDiskFree портовано на KDE Frameworks 5 і ми чекаємо на ваші відгуки та підказки щодо новітніх можливостей, які реалізовано у цьому випуску.

У межах зусиль з поділу комплексу програм Kontact на бібліотеки для полегшення використання у сторонніх програмах архів kdepimlibs поділено на частини: akonadi-contacts, akonadi-mime та akonadi-notes.

Припинено супровід пакунків kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu та mplayerthumbs. Це надасть нам змогу сконцентрувати зусилля на решті програмного коду.

### Підтримуємо Kontact

У межах цього випуску <a href='https://userbase.kde.org/Kontact'>комплекс програм Kontact</a> звично удосконалено, виправлено вади та оптимізовано код. Примітним є використання QtWebEngine у різних компонентах, що надало змогу скористатися сучаснішим кодом обробки даних з інтернету. Також значно поліпшено підтримку VCard4, а також додано нові додатки, які можуть попереджати про невідповідність повідомлення певним умовам, зокрема виявляти надсилання повідомлень без вказаного профілю, у форматі звичайного тексту тощо.

### Нова версія Marble

Частиною набору програм KDE 16.08 є <a href='https://marble.kde.org/'>Marble</a> 2.0. До коду програми внесено понад 450 змін, зокрема у механізмах навігації, показу даних, та реалізовано експериментальний показ векторних даних OpenStreetMap.

### Ширші можливості з архівування

Нова версія <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> здатна видобувати дані з файлів AppImage та .xar, а також перевіряти цілісність архівів zip, 7z і rar. Також реалізовано можливість додавання і редагування коментарів у архівах rar.

### Поліпшення у терміналі

У <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> удосконалено параметри показу шрифтів та доступність для користувачів із обмеженими можливостями.

### І багато іншого!

У <a href='https://kate-editor.org'>Kate</a> реалізовано пересування вкладок. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Докладніше...</a>

У <a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a> реалізовано карти провінцій і областей Буркіна-Фасо.

### Агресивна боротьба із вадами

Було виправлено понад 120 вад у програмах, зокрема у Kontact Suite, Ark, Cantor, Dolphin, KCalc, Kdenlive!

### Повний список змін
