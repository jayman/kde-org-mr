---
aliases:
- ../announce-applications-17.04-rc
date: 2017-04-07
description: KDE випущено кандидат у випуски Програм 17.04.
layout: application
release: applications-17.03.90
title: KDE випущено близьку до готовності версію програм KDE 17.04
---
7 квітня 2017 року. Сьогодні командою KDE випущено близький до готовності варіант нових версій програм. Від цього моменту заморожено залежності та список можливостей, — команда KDE зосереджує зусилля на виправлені вад та удосконаленні нової версії.

Ознайомитися із даними щодо архівів з кодом, архівів, які тепер засновано на KF5, та списком відомих проблем можна за допомогою <a href='https://community.kde.org/Applications/17.04_Release_Notes'>сторінки нотаток щодо випуску</a>. Повніше оголошення буде зроблено, щойно ми приготуємо остаточний випуск.

Випуск набору програм 17.04 потребує ретельного тестування з метою підтримання та поліпшення якості та зручності у користуванні. Користувачі є надзвичайно важливою ланкою у підтриманні високої якості випусків KDE, оскільки розробникам просто не вистачить часу перевірити всі можливі комбінацій обладнання та налаштувань системи. Ми розраховуємо на вашу допомогу у якомога швидшому виявленні вад, щоб уможливити виправлення цих вад до остаточного випуску. Будь ласка, долучіться до команди тестувальників , встановивши нову версію <a href='https://bugs.kde.org/'>і повідомивши про всі виявлені вади</a>.
