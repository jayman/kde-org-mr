---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE Ships Applications 19.04.
layout: application
release: applications-19.04.0
title: KDE에서 KDE 프로그램 19.04.0 출시
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

The KDE community is happy to announce the release of KDE Applications 19.04.

Our community continuously works on improving the software included in our KDE Application series. Along with new features, we improve the design, usability and stability of all our utilities, games, and creativity tools. Our aim is to make your life easier by making KDE software more enjoyable to use. We hope you like all the new enhancements and bug fixes you'll find in 19.04!

## What's new in KDE Applications 19.04

150개 이상의 버그를 해결했습니다. 수정 사항 중에는 비활성화된 기능을 재구현, 단축키 표준화, 충돌 해결 등이 있으며 KDE 프로그램을 더 친숙하게 사용하고 생산성을 향상시킬 수 있습니다.

### 파일 관리

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> is KDE's file manager. It also connects to network services, such as SSH, FTP, and Samba servers, and comes with advanced tools to find and organize your data.

새로운 기능:

+ We have expanded thumbnail support, so Dolphin can now display thumbnails for several new file types: <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a> files, <a href='https://phabricator.kde.org/D18738'>.epub and .fb2 eBook</a> files, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> files, and <a href='https://phabricator.kde.org/D19679'>PCX</a> files. Additionally, thumbnails for text files now show <a href='https://phabricator.kde.org/D19432'>syntax highlighting</a> for the text inside the thumbnail. </li>
+ You can now choose <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>which split view pane to close</a> when clicking the 'Close split' button. </li>
+ This version of Dolphin introduces <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>smarter tab placement</a>. When you open a folder in a new tab, the new tab will now be placed immediately to the right of the current one, instead of always at the end of the tab bar. </li>
+ <a href='https://phabricator.kde.org/D16872'>Tagging items</a> is now much more practical, as tags can be added or removed using the context menu. </li>
+ We have <a href='https://phabricator.kde.org/D18697'>improved the default sorting</a> parameters for some commonly-used folders. By default, the Downloads folder is now sorted by date with grouping enabled, and the Recent Documents view (accessible by navigating to recentdocuments:/) is sorted by date with a list view selected. </li>

버그 수정 내역:

+ When using a modern version of the SMB protocol, you can now <a href='https://phabricator.kde.org/D16299'>discover Samba shares</a> for Mac and Linux machines. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>Re-arranging items in the Places panel</a> once again works properly when some items are hidden. </li>
+ After opening a new tab in Dolphin, that new tab's view now automatically gains <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>keyboard focus</a>. </li>
+ Dolphin now warns you if you try to quit while the <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>terminal panel is open</a> with a program running in it. </li>
+ We fixed many memory leaks, improving Dolphin's overall performance.</li>

The <a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> allows other KDE applications to read audio from CDs and automatically convert it into other formats.

+ The AudioCD-KIO now supports ripping into <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ We made <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>CD info text</a> really transparent for viewing. </li>

### 동영상 편집

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

This is a landmark version for KDE's video editor. <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> has gone through an extensive re-write of its core code as more than 60%% of its internals has changed, improving its overall architecture.

개선 사항:

+ 타임라인이 QML 기반으로 재작성되었습니다.
+ 클립을 타임라인에 삽입할 때 오디오와 비디오는 항상 별도의 트랙으로 이동합니다.
+ 타임라인에서 키보드 탐색을 지원합니다. 키보드로 클립, 합성 및 키프레임을 이동할 수 있습니다. 또한 트랙 자체의 높이도 조정할 수 있습니다.
+ In this version of Kdenlive, the in-track audio recording comes with a new <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>voice-over</a> feature.
+ 복사/붙여넣기 기능을 개선했습니다. 다른 프로젝트 창간에도 작동합니다. 클립을 개별적으로 삭제할 수 있으므로 프록시 클립 관리도 향상되었습니다.
+ 버전 19.04에서는 외부 BlackMagic 모니터 디스플레이 지원이 돌아왔으며 새로운 모니터 사전 설정 가이드도 있습니다.
+ 키프레임 처리를 개선하여 더욱 일관된 모양과 작업 흐름을 지원합니다. 정렬 단추를 안전 영역으로 스냅하고, 설정 가능한 안내선과 배경색을 추가하고, 누락 된 항목을 표시하는 등의 타이틀 제작 도구를 개선했습니다.
+ 클립 그룹을 이동할 때 트리거된 클립을 잘못 배치하거나 누락시키는 타임라인 손상 버그가 수정되었습니다.
+ Windows에서 이미지를 흰색 화면으로 렌더링하는 JPG 그림 버그를 수정했습니다. 또한 Windows에서 화면 캡처에 영향을 주는 버그를 수정했습니다.
+ Apart from all of the above, we have added many small usability enhancements that will make using Kdenlive easier and smoother.

### 사무용 도구

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> is KDE's multipurpose document viewer. Ideal for reading and annotating PDFs, it can also open ODF files (as used by LibreOffice and OpenOffice), ebooks published as ePub files, most common comic book files, PostScript files, and many more.

개선 사항:

+ To help you ensure your documents are always a perfect fit, we've added <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>scaling options</a> to Okular's Print dialog.
+ Okular now supports viewing and verifying <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>digital signatures</a> in PDF files.
+ Thanks to improved cross-application integration, Okular now supports editing LaTeX documents in <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ Improved support for <a href='https://phabricator.kde.org/D18118'>touchscreen navigation</a> means you will be able to move backwards and forwards using a touchscreen when in Presentation mode.
+ Users who prefer manipulating documents from the command-line will be able to perform smart <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>text search</a> with the new command-line flag that lets you open a document and highlight all occurrences of a given piece of text.
+ Okular now properly displays links in <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>Markdown documents</a> that span more than one line.
+ The <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>trim tools</a> have fancy new icons.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> is KDE's privacy-protecting email client. Part of the <a href='https://kde.org/applications/office/kontact/'>Kontact groupware suite</a>, KMail supports all email systems and allows you to organize your messages into a shared virtual inbox or into separate accounts (your choice). It supports all kinds of message encryption and signing, and lets you share data such as contacts, meeting dates, and travel information with other Kontact applications.

개선 사항:

+ Grammarly, 안녕! 이 KMail 버전은 languagetools(문법 검사기) 및 grammalecte(프랑스어 전용 문법 검사기)를 지원합니다.
+ Phone numbers in emails are now detected and can be dialed directly via <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ KMail now has an option to <a href='https://phabricator.kde.org/D19189'>start directly in system tray</a> without opening the main window.
+ Markdown 플러그인 지원이 개선되었습니다.
+ 로그인에 실패했을 때 IMAP 메일 가져오기가 더 이상 멈추지 않습니다.
+ KMail의 백엔드인 Akonadi에 신뢰성과 성능을 향상시키기는 개선이 이뤄졌습니다.

<a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> is Kontact's calendar component, managing all your events.

+ Recurrent events from <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> are again synchronized correctly.
+ The event reminder window now remembers to <a href='https://phabricator.kde.org/D16247'>show on all desktops</a>.
+ We modernized the look of the <a href='https://phabricator.kde.org/T9420'>event views</a>.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> is Kontact's brand new travel assistant that will help you get to your location and advise you on your way.

- RCT2 승차권에 사용하는 새로운 일반 추출기가 추가되었습니다(예: 덴마크 DSB, 오스트리아 ÖBB, 스위스 SBB, 네덜란드 NS 등 철도 회사에서 사용).
- 공항 이름 감지 및 명확성이 개선되었습니다.
- 이전에 지원하지 않았던 공급자(예: BCD Travel, NH Group) 및 이미 지원하는 공급자(예: SNCF, Easyjet, Booking.com, Hertz)의 형식/언어 파생형을 지원하는 새로운 추출기를 추가했습니다.

### 개발

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> is KDE's full-featured text editor, ideal for programming thanks to features such as tabs, split-view mode, syntax highlighting, a built-in terminal panel, word completion, regular expressions search and substitution, and many more via the flexible plugin infrastructure.

개선 사항:

- Kate에서 모든 보이지 않는 공백 문자를 표시할 수 있습니다.
- 자체 메뉴 항목을 통해서 전역 설정을 바꾸지 않고 정적 자동 줄 바꿈 기능을 문서별로 설정할 수 있습니다.
- 파일 및 탭 콘텍스트 메뉴에 이름 바꾸기, 삭제, 포함하는 폴더 열기, 파일 경로 복사, [다른 열린 파일과] 비교 및 속성과 같은 유용한 작업이 추가되었습니다.
- 이 Kate 버전에서는 더 많은 플러그인을 기본으로 활성화합니다. 인기 있고 유용한 인라인 터미널 기능을 포함합니다.
- Kate를 끝낼 때 리비전 관리 시스템 등 다른 프로세스에서 파일을 변경했음을 확인하는 대화 상자를 표시하지 않습니다.
- 이름에 움라우트가 있는 git 항목의 모든 메뉴 항목을 플러그인의 트리 보기에서 올바르게 표시합니다.
- 명령행을 사용하여 여러 파일을 열 때 명령행에 지정한 순서대로 파일을 새 탭에서 엽니다.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> is KDE's terminal emulator. It supports tabs, translucent backgrounds, split-view mode, customizable color schemes and keyboard shortcuts, directory and SSH bookmarking, and many other features.

개선 사항:

+ 생산성 향상에 도움이 되는 여러 가지 탭 관리 개선 사항이 추가되었습니다. 탭 표시줄의 빈 부분을 마우스 가운데 단추로 클릭하면 새 탭이 열리며, 기존 탭을 마우스 가운데 단추로 클릭했을 때 탭을 닫을 수 있는 옵션도 있습니다. 닫기 단추는 기본적으로 탭에 표시되며, 사용자 정의 아이콘이 있는 프로필을 사용할 때에만 아이콘을 표시합니다. Ctrl+Tab 단축키를 누르면 현재 탭과 이전 탭을 빠르게 전환할 수 있습니다.
+ The Edit Profile dialog received a huge <a href='https://phabricator.kde.org/D17244'>user interface overhaul</a>.
+ Breeze 색 배열을 기본 Konsole 색 배열로 사용하며, 시스템 전역 Breeze 테마와의 대비 및 일관성이 향상되었습니다.
+ 굵은 텍스트를 표시할 때 문제를 해결했습니다.
+ Konsole에 밑줄 스타일의 커서가 올바르게 표시됩니다.
+ We have improved the display of <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>box and line characters</a>, as well as of <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>Emoji characters</a>.
+ 프로필 전환 단축키를 눌렀을 때 프로필을 적용한 다른 탭을 열지 않고 현재 탭의 프로필을 전환합니다.
+ 알림이 수신되어 제목 글자색이 변경된 비활성 탭을 활성화할 때 다시 색상을 일반 탭 제목 글자색으로 재설정합니다.
+ 기본 배경색이 매우 어둡거나 검은색 인 경우 '각각 탭에 대해 다른 배경색 사용' 기능이 작동합니다.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> is a computer-aided translation system that focuses on productivity and quality assurance. It is targeted at software translation, but also integrates external conversion tools for translating office documents.

개선 사항:

- Lokalize에서 사용자 정의 편집기로 번역 원본을 볼 수 있습니다.
- 독 위젯 위치가 개선되었으며 설정이 저장되고 복원되는 방식이 변경되었습니다.
- 메시지를 필터링할 때 .po 파일의 위치가 유지됩니다.
- 다양한 UI 버그(개발자 의견, 대량 바꾸기에서 정규 표현식 전환, 퍼지 빈 메시지 개수 등)를 수정했습니다.

### 유틸리티

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> is an advanced image viewer and organizer with intuitive and easy-to-use editing tools.

개선 사항:

+ The version of Gwenview that ships with Applications 19.04 includes full <a href='https://phabricator.kde.org/D13901'>touchscreen support</a>, with gestures for swiping, zooming, panning, and more.
+ Another enhancement added to Gwenview is full <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>High DPI support</a>, which will make images look great on high-resolution screens.
+ Improved support for <a href='https://phabricator.kde.org/D14583'>back and forward mouse buttons</a> allows you to navigate between images by pressing those buttons.
+ You can now use Gwenview to open image files created with <a href='https://krita.org/'>Krita</a> – everyone’s favorite digital painting tool.
+ Gwenview now supports large <a href='https://phabricator.kde.org/D6083'>512 px thumbnails</a>, allowing you to preview your images more easily.
+ Gwenview now uses the <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>standard Ctrl+L keyboard shortcut</a> to move focus to the URL field.
+ You can now use the <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>Filter-by-name feature</a> with the Ctrl+I shortcut, just like in Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> is Plasma's screenshot application. You can grab full desktops spanning several screens, individual screens, windows, sections of windows, or custom regions using the rectangle selection feature.

개선 사항:

+ We have extended the Rectangular Region mode with a few new options. It can be configured to <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>auto-accept</a> the dragged box instead of asking you to adjust it first. There is also a new default option to remember the current <a href='https://phabricator.kde.org/D19117'>rectangular region</a> selection box, but only until the program is closed.
+ You can configure what happens when the screenshot shortcut is pressed <a href='https://phabricator.kde.org/T9855'>while Spectacle is already running</a>.
+ Spectacle allows you to change the <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>compression level</a> for lossy image formats.
+ Save settings shows you what the <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>filename of a screenshot</a> will look like. You can also easily tweak the <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>filename template</a> to your preferences by simply clicking on placeholders.
+ 컴퓨터에 하나의 화면만 있는 경우 Spectacle에서 더 이상 "전체 화면(모든 모니터)" 및 "현재 화면" 옵션을 표시하지 않습니다.
+ 직사각형 영역 모드에서 도움말 텍스트는 화면간에 분할되지 않고 주 디스플레이의 중간에 나타납니다.
+ Wayland에서 실행할 때 Spectacle은 작동하는 기능만 표시됩니다.

### 게임과 교육

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Our application series includes numerous <a href='https://games.kde.org/'>games</a> and <a href='https://edu.kde.org/'>educational applications</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a>은 수학 함수 플로터입니다. 강력한 파서가 내장되어 있습니다. 그래프를 색칠하고 필요한 크기로 보기를 조정할 수 있습니다. 다른 함수를 동시에 플롯하고 함수를 합쳐서 새로운 함수를 만들 수 있습니다.

+ You can now zoom in by holding down Ctrl and using the <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>mouse wheel</a>.
+ This version of Kmplot introduces the <a href='https://phabricator.kde.org/D17626'>print preview</a> option.
+ Root value or (x,y) pair can now be copied to <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>clipboard</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> is a miniature golf game.

+ We have restored <a href='https://phabricator.kde.org/D16978'>sound support</a>.
+ Kolf가 kdelibs4에서 이식되었습니다.
