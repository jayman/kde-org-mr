---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE에서 KDE 프로그램 17.12.3 출시
layout: application
title: KDE에서 KDE 프로그램 17.12.3 출시
version: 17.12.3
---
March 8, 2018. Today KDE released the third stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Dolphin, Gwenview, JuK, KGet, Okular, Umbrello 등에 25개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- Akregator에서 오류 발생 시 더 이상 feeds.opml 파일을 삭제하지 않음
- Gwenview의 전체 화면 모드에서 파일 이름을 바꾼 후 올바른 파일 이름 적용
- Okular에서 극히 드물게 발생하는 충돌 조건 확인 및 수정
