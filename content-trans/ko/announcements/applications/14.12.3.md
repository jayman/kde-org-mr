---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE Ships Applications 14.12.3.
layout: application
title: KDE에서 KDE 프로그램 14.12.3 출시
version: 14.12.3
---
March 3, 2015. Today KDE released the third stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

애너그램 게임 Kanagram, Umbrello UML 모델러, 문서 뷰어 Okular 및 기하학 프로그램 Kig에 버그 수정 및 기능 개선 19개가 있었습니다.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 and the Kontact Suite 4.14.6.
