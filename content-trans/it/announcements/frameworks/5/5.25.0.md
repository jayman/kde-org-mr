---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Generale

- Qt &gt;= 5.5 is now required

### Attica

- Follow HTTP redirects

### Icone Brezza

- update mail- 16px icons to recognize the icons better
- update mic and audio status icons to have the same layout and size
- New System Settings app icon
- add symbolic status gnome icons
- add gnome 3 symbolic icon support
- Added icons for Diaspora and Vector, see phabricator.kde.org/M59
- New icons for Dolphin and Gwenview
- weather icons are status icons no app icons
- add some links to xliff thanks gnastyle
- add kig icon
- add mimetype icons, krdc icon, other app icons from gnastyle
- add certificate mimetype icon (bug 365094)
- update gimp icons thanks gnastyle (bug 354370)
- globe action icon is now no linked file please use it in digikam
- update labplot icons according to mail 13.07. from Alexander Semke
- add app icons from gnastyle
- add kruler icon from Yuri Fabirovsky
- fix broken svg files thanks fuchs (bug 365035)

### Moduli CMake aggiuntivi

- Fix inclusion when there's no Qt5
- Add a fallback method for query_qmake() when there's no Qt5 installation
- Make sure ECMGeneratePriFile.cmake behaves like the rest of ECM
- Appstream data changed its preferred location

### KActivities

- [KActivities-CLI] commands for starting and stopping an activity
- [KActivities-CLI] setting and getting activity name, icon and description
- Added a CLI application for controlling activities
- Adding scripts to switch to previous and next activities
- Method for inserting into QFlatSet now returns index along with the iterator (bug 365610)
- Adding ZSH functions for stopping and deleting non-current activities
- Added isCurrent property to KActivities::Info
- Using constant iterators when searching for activity

### KDE Doxygen Tools

- Many improvements to the output formatting
- Mainpage.dox has now higher priority than README.md

### KArchive

- Handle multiple gzip streams (bug 232843)
- Assume a directory is a directory, even if the permission bit is set wrong (bug 364071)

### KBookmarks

- KBookmarkGroup::moveBookmark: fix return value when item is already at the right position

### KConfig

- Add DeleteFile and RenameFile standard shortcut

### KConfigWidgets

- Add DeleteFile and RenameFile standard action
- The config page has now scroll bars when needed (bug 362234)

### KCoreAddons

- Install known licenses and find them at runtime (regression fix) (bug 353939)

### KDeclarative

- Actually emit valueChanged

### KFileMetaData

- Check for xattr during config step, otherwise the build might fail (if xattr.h is missing)

### KGlobalAccel

- Use klauncher dbus instead of KRun (bug 366415)
- Launch jumplist actions via KGlobalAccel
- KGlobalAccel: Fix deadlock on exit under Windows

### KHTML

- Support percentage unit in border radius
- Removed prefixed version of background and border radius properties
- Fixes in 4-values shorthand constructor function
- Create string objects only if they will be used

### KIconThemes

- Greatly improve the performance of makeCacheKey, as it is a critical code path in icon lookup
- KIconLoader: reduce number of lookups when doing fallbacks
- KIconLoader: massive speed improvement for loading unavailable icons
- Do not clear search line when switching category
- KIconEngine: Fix QIcon::hasThemeIcon always returning true (bug 365130)

### KInit

- Adapt KInit to Mac OS X

### KIO

- Fix KIO::linkAs() to work as advertised, i.e. fail if dest already exists
- Fix KIO::put("file:///path") to respect the umask (bug 359581)
- Fix KIO::pasteActionText for null dest item and for empty URL
- Add support for undoing symlink creation
- GUI option to configure global MarkPartial for KIO slaves
- Fix MaxCacheSize limited to 99 KiB
- Add clipboard buttons to checksums tab
- KNewFileMenu: fix copying template file from embedded resource (bug 359581)
- KNewFileMenu: Fix creating link to application (bug 363673)
- KNewFileMenu: Fix suggestion of new filename when file already exist in desktop
- KNewFileMenu: ensure fileCreated() is emitted for app desktop files too
- KNewFileMenu: fix creating symlinks with a relative target
- KPropertiesDialog: simplify button box usage, fix behavior on Esc
- KProtocolInfo: refill cache to find newly installed protocols
- KIO::CopyJob: port to qCDebug (with its own area, since this can be quite verbose)
- KPropertiesDialog: add Checksums tab
- Clean url's path before initializing KUrlNavigator

### KItemModels

- KRearrangeColumnsProxyModel: fix assert in index(0, 0) on empty model
- Fix KDescendantsProxyModel::setSourceModel() not clearing internal caches
- KRecursiveFilterProxyModel: fix QSFPM corruption due to filtering out rowsRemoved signal (bug 349789)
- KExtraColumnsProxyModel: implement hasChildren()

### KNotification

- Don't set parent of sublayout manually, silences warning

### Struttura dei pacchetti

- Infer the ParentApp from the PackageStructure plugin
- Let kpackagetool5 generate appstream information for kpackage components
- Make it possible to load metadata.json file from kpackagetool5

### Kross

- Remove unused KF5 dependencies

### KService

- applications.menu: remove references to unused categories
- Always update the Trader parser from yacc/lex sources

### KTextEditor

- Do not ask for overwriting a file twice with native dialogs
- added FASTQ syntax

### KWayland

- [client] Use a QPointer for the enteredSurface in Pointer
- Expose Geometry in PlasmaWindowModel
- Add a geometry event to PlasmaWindow
- [src/server] Verify that surface has a resource before sending pointer enter
- Add support for xdg-shell
- [server] Properly send a selection clear prior to keyboard focus enter
- [server] Handle no XDG_RUNTIME_DIR situation more gracefully

### KWidgetsAddons

- [KCharSelect] Fix crash when searching with no present data file (bug 300521)
- [KCharSelect] Handle characters outside BMP (bug 142625)
- [KCharSelect] Update kcharselect-data to Unicode 9.0.0 (bug 336360)
- KCollapsibleGroupBox: Stop animation in destructor if still running
- Update to Breeze palette (sync from KColorScheme)

### KWindowSystem

- [xcb] Ensure the compositingChanged signal is emitted if NETEventFilter is recreated (bug 362531)
- Add a convenience API to query the windowing system/platform used by Qt

### KXMLGUI

- Fix minimum size hint (cut-off text) (bug 312667)
- [KToggleToolBarAction] Honor action/options_show_toolbar restriction

### NetworkManagerQt

- Default to WPA2-PSK and WPA2-EAP when getting security type from connection settings

### Icone Oxygen

- add application-menu to oxygen (bug 365629)

### Plasma Framework

- Keep compatiable slot createApplet with Frameworks 5.24
- Don't delete gl texture twice in thumbnail (bug 365946)
- Add translation domain to wallpaper QML object
- Don't manually delete applets
- Add a kapptemplate for Plasma Wallpaper
- Templates: register templates in own toplevel category "Plasma/"
- Templates: Update techbase wiki links in READMEs
- Define what Plasma packagestructures extend plasmashell
- support a size for adding applets
- Define Plasma PackageStructure as regular KPackage PackageStructure plugins
- Fix: update wallpaper example Autumn's config.qml to QtQuick.Controls
- Use KPackage to install Plasma Packages
- If we pass a QIcon as an argument to IconItem::Source, use it
- Add overlay support to Plasma IconItem
- Add Qt::Dialog to default flags to make QXcbWindow::isTransient() happy (bug 366278)
- [Breeze Plasma Theme] Add network-flightmode-on/off icons
- Emit contextualActionsAboutToShow before showing the applet's contextualActions menu (bug 366294)
- [TextField] Bind to TextField length instead of text
- [Button Styles] Horizontally center in icon-only mode (bug 365947)
- [Containment] Treat HiddenStatus as low status
- Add kruler system tray icon from Yuri Fabirovsky
- Fix the infamous 'dialogs show up on the Task Manager' bug once more
- fix network wireless available icon with an ? emblem (bug 355490)
- IconItem: Use better approach to disable animation when going from invisible to visible
- Set Tooltip window type on ToolTipDialog through KWindowSystem API

### Solid

- Always update the Predicate parser from yacc/lex sources

### Sonnet

- hunspell: Clean up code for searching for dictionaries, add XDG dirs (bug 361409)
- Try to fix language filter usage of language detection a bit

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
