---
aliases:
- ../announce-applications-15.12-rc
date: 2015-12-03
description: KDE rilascia Applications 15.12 Release Candidate.
layout: application
release: applications-15.11.90
title: KDE rilascia la Release Candidate di KDE Applications 15.12
---
3 dicembre 2015. Oggi KDE ha rilasciato la Release Candidate della nuova versione di KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura dei programmi.

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, KDE Applications 15.12 ha bisogno della qualità e dell'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando la versione <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.
