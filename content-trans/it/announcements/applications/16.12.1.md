---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE rilascia KDE Applications 16.12.1
layout: application
title: KDE rilascia KDE Applications 16.12.1
version: 16.12.1
---
12 gennaio 2017. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../16.12.0'>KDE Applications 16.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Questa versione corregge una perdita di dati nella risorsa iCal che non riusciva a creare std.ics se non esisteva.

Gli oltre 40 errori corretti includono, tra gli altri, miglioramenti a kdepim, Ark, Gwenview, Kajongg, Okular, Kate e Kdenlive.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.28.
