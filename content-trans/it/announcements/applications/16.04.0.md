---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE rilascia KDE Applications 16.04.0
layout: application
title: KDE rilascia KDE Applications 16.04.0
version: 16.04.0
---
20 aprile 2016. KDE introduce oggi una serie impressionante di aggiornamenti in termini di una migliore facilità di accesso, l'introduzione di funzionalità molto utili ed eliminando quei problemi minori, portando KDE Applications un passo più vicino a offrirti la configurazione perfetta per il tuo dispositivo.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> e <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> ora sono stati portati su KDE Frameworks 5 e non vediamo l'ora di ricevere riscontro da parte tua e idee sulle nuove funzionalità introdotte con questa versione. Incoraggiamo anche vivamente il tuo supporto per <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a>,mentre lo accogliamo tra le nostre applicazioni KDE, e il tuo contributo su ciò che ti piacerebbe vedere.

### Aggiunte più recenti di KDE

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

Una nuova applicazione è stata aggiunta alla suite KDE Education. <a href='https://minuet.kde.org'>Minuet</a> è un software di educazione musicale con supporto MIDI completo con controllo di tempo, intonazione e volume, che lo rende adatto sia ai musicisti principianti che a quelli esperti.

Minuet include 44 esercizi di allenamento dell'orecchio per scale, accordi, intervalli e ritmo, consente la visualizzazione dei contenuti musicali sulla tastiera del piano e consente l'integrazione senza soluzione di continuità dei propri esercizi.

### Più aiuto per te

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

KHelpCenter, che in precedenza era distribuito sotto Plasma, è ora distribuito come parte di KDE Applications.

Una massiccia campagna di individuazione e pulizia di bug messa in campo dalla squadra di KHelpCenter ha portato a 49 bug risolti, molti dei quali erano relativi al miglioramento e al ripristino della funzionalità di ricerca in precedenza non funzionale.

La ricerca interna del documento che si basava sul software deprecato ht::/dig è stata sostituita con un nuovo sistema di indicizzazione e ricerca basato su Xapian che porta al ripristino di funzionalità come la ricerca all'interno delle pagine man, delle pagine informative e della documentazione fornita dal software KDE, con la fornitura aggiuntiva di segnalibri per le pagine di documentazione.

Con la rimozione del supporto KDELibs4 e un'ulteriore pulizia del codice e alcuni altri bug minori, la manutenzione del codice è stata anche fornita accuratamente per darti KHelpCenter in una nuova scintillante veste.

### Controllo aggressivo dei parassiti

La suite Kontact aveva risolto un enorme carico di 55 bug; Alcuni dei quali erano correlati a problemi con l'impostazione degli allarmi e nell'importazione di posta Thunderbird, ridimensionando le icone di Skype e Google Talk nella vista del pannello Contatti, soluzioni alternative relative a KMail come importazioni di cartelle, importazioni vCard, apertura degli allegati di posta ODF, inserti URL da Chromium, le differenze del menu dello strumento con l'applicazione avviata come parte di Kontact in contrapposizione a un uso autonomo, Manca la voce di menu «Invia» e poco altro. Il supporto per le fonti RSS portoghesi brasiliane è stato aggiunto insieme alla correzione degli indirizzi per le fonti ungheresi e spagnole.

Le nuove funzionalità includono una riprogettazione dell'editor dei contatti di KAddressbook, un nuovo tema predefinito delle intestazioni di KMail, miglioramenti all'esportatore delle impostazioni e una correzione del supporto Favicon in Akregator. L'interfaccia del compositore di KMail è stata ripulita insieme all'introduzione di un nuovo tema predefinito delle intestazioni di KMail con il tema Grantlee utilizzato per la pagina «Informazioni» in KMail, nonché Kontact e Akregator. Akregator ora utilizza QtWebKit - uno dei principali motori per rendere le pagine web ed eseguire il codice javascript come motore di rendering web e il processo di implementazione del supporto per QtWebEngine in Akregator e altre applicazioni della suite Kontact è già iniziato. Mentre diverse caratteristiche sono state trasformate in estensioni in kdepim-addons, le librerie PIM sono state divise in numerosi pacchetti.

### Archiviazione in profondità

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, il gestore degli archivi, ha ricevuto la correzione di due bug principali in modo che ora Ark avverta l'utente se un'estrazione non riesce a causa della mancanza di spazio sufficiente nella cartella di destinazione e non satura la RAM durante l'estrazione di file enormi tramite trascinamento.

Ark ora include anche una finestra delle proprietà che visualizza informazioni come il tipo di archivio, dimensioni compresse e non compresse, hash crittografici MD5/SHA-1/SHA-256 sull'archivio attualmente aperto.

Ark ora può aprire ed estrarre anche archivi RAR senza utilizzare le utilità RAR non libere e può aprire, estrarre e creare archivi TAR compressi con i formati lzop/lzip/lrzip.

L'interfaccia utente di Ark è stata raffinata riorganizzando la barra dei menu e la barra degli strumenti in modo da migliorare l'usabilità e rimuovere le ambiguità, nonché per salvare lo spazio verticale grazie alla barra di stato ora nascosta per impostazione predefinita.

### Più precisione alle modifiche video

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, l'editor video non lineare ha implementato numerose nuove funzionalità. La titolatrice ora ha una funzionalità di griglia, gradienti, aggiunta di ombra di testo e spaziatura di lettere/linea di regolazione.

La visualizzazione integrata dei livelli audio consente un facile monitoraggio dell'audio nel progetto insieme a nuove sovrapposizioni di monitoraggio video che visualizzano velocità di riproduzione, zone sicure e forme d'onda audio e una barra degli strumenti per cercare marcatori e un monitoraggio dell'ingrandimento.

C'è stata anche una nuova funzionalità di libreria che consente di copiare/incollare sequenze tra progetti e una vista divisa nella sequenza temporale per confrontare e visualizzare gli effetti applicati alla clip con quella senza di loro.

La finestra di rendering è stata riscritta con un'opzione aggiunta per ottenere una codifica più veloce, producendo file di grandi dimensioni e l'effetto di velocità è stato fatto funzionare anche con il suono.

Le curve in keyframe sono state introdotte per alcuni effetti e ora i tuoi effetti scelti più spesso possono essere accessibili rapidamente tramite l'oggetto degli effetti preferiti. Durante l'utilizzo dello strumento Rasoio, ora la linea verticale nella sequenza temporale mostrerà il fotogramma accurato in cui verrà eseguito il taglio e i generatori di clip appena aggiunti ti consentiranno di creare clip e contatori a barre. Oltre a questo, il conteggio dell'utilizzo della clip è stato reintrodotto nel cestino del progetto e anche le miniature audio sono state riscritte per renderle molto più veloci.

Le principali correzioni di bug includono l'arresto anomalo durante l'utilizzo dei titoli (che richiede l'ultima versione MLT), correggendo le corruzioni che si verificano quando il tasso di fotogrammi al secondo del progetto è diversa da 25, l'arresto anomalo alla cancellazione della traccia, la modalità di sovrascrittura problematica nella sequenza temporale e Corruzioni/effetti persi durante l'utilizzo di una localizzazione con una virgola come separatore. Oltre a questi, la squadra ha costantemente lavorato per apportare importanti miglioramenti a stabilità, flusso di lavoro e piccole caratteristiche di usabilità.

### E altro!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, il visore di documenti porta nuove funzionalità sotto forma di larghezza del bordo di annotazione in linea personalizzabile, possibilità di aprire direttamente file incorporati invece di salvarli e anche la visualizzazione di una tabella di marcatori di contenuti quando i collegamenti figli vengono contratti.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> ha introdotto un supporto migliorato per GnuPG 2.1 con una correzione di errori da test automatici e gli aggiornamenti chiari ingombranti causati dalla nuova disposizione della cartella di GnuPG (GNU Privacy Guard). La generazione della chiave ECC è stata aggiunta con la visualizzazione dei dettagli della curva per i certificati ECC. Kleopatra è ora rilasciato come pacchetto separato e il supporto per i file .pfx e .crt è stato incluso. Per risolvere la differenza nel comportamento delle chiavi segrete importate e delle chiavi generate, Kleopatra ora consente di impostare la fiducia del proprietario su Definitiva per le chiavi segrete importate.
