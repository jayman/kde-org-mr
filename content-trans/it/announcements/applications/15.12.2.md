---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE rilascia KDE Applications 15.12.2
layout: application
title: KDE rilascia KDE Applications 15.12.2
version: 15.12.2
---
16 febbraio 2016. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../15.12.0'>KDE Applications 15.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 30 bug corretti includono. tra gli altri, miglioramenti a kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark e umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.17.
