---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE rilascia Applications 14.12.
layout: application
title: KDE rilascia KDE Applications 14.12
version: 14.12.0
---
17 dicembre 2014. Oggi KDE ha rilasciato le applicazioni di KDE 14.12 (KDE Applications 14.12). Questo rilascio porta nuove funzionalità e correzioni di bug per più di cento applicazioni. Molte di queste sono basata su KDE Development Platform 4; alcune sono state convertite al nuovo <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, un insieme di librerie modulari basate su Qt5, l'ultima versione di questo framework famoso per applicazioni multi-piattaforma.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> è nuova in questo rilascio; è una libreria per abilitare il riconoscimento facciale e l'identificazione dei volti nelle fotografie.

Questo rilascio include le prime versioni basate su KDE Frameworks 5 di <a href='%1'>Kate</a> and <a href='%2'>KWrite</a>, <a href='%3'>Konsole</a>, <a href='%4'>Gwenview</a>, <a href='%5'>KAlgebra</a>, <a href='%6'>Kanagram</a>, <a href='%7'>KHangman</a>, <a href='%8'>Kig</a>, <a href='%9'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> e <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Alcune librerie sono anche pronte l'uso di KDE Frameworks 5: analitza e libkeduvocdocument.

La <a href='http://kontact.kde.org'>suite Kontact</a> è ora in supporto a lungo termine (Long Term Support) nella sua versione 4.14, mentre gli sviluppatori usano le loro nuove energie per portarla a KDE Frameworks 5

Alcune delle nuove funzionalità in questo rilascio includono:

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> dispone di una nuova versione Android grazie a KDE Frameworks 5 ed è in grado di <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>stampare i suoi grafici in 3D</a>
+ <a href='http://edu.kde.org/kgeography'>KGeography</a> ha una nuova mappa per Bihar.
+ Il visualizzatore di documenti <a href='http://okular.kde.org'>Okular</a> ha ora il supporto per la ricerca inversa usando latex-synctex nei documenti DVI e alcuni piccoli miglioramenti nel supporto ePub.
+ <a href='http://umbrello.kde.org'>Umbrello</a>, il modellatore UML, ha molte nuove funzionalità, troppo numerose per poter essere elencate qui.

Il rilascio di aprile di KDE Applications 15.04 includerà molte nuove funzionalità, oltre ad un numero maggiore di applicazioni basate sulle librerie moduli KDE Frameworks 5.
