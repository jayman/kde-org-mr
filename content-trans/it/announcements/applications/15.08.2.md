---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE rilascia KDE Applications 15.08.2
layout: application
title: KDE rilascia KDE Applications 15.08.2
version: 15.08.2
---
13 ottobre 2015. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../15.08.0'>KDE Applications 15.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 30 bug corretti includono miglioramenti ad Ark, Gwenview, Kate, KBruch, kdelibs, kdepim, Lokalize e Umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.13.
