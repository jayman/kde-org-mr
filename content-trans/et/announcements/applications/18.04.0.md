---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE toob välja KDE rakendused 18.04.0
layout: application
title: KDE toob välja KDE rakendused 18.04.0
version: 18.04.0
---
19. aprill 2018. KDE laskis välja rakendused 18.04.0.

Me jätkame visalt tööd meie KDE rakendustesse kaasatud tarkvara parandamisel ja täiustamisel ning loodame, et kõik uued omadused ja veaparandused tulevad kasutajatele kasuks!

## What's new in KDE Applications 18.04

### Süsteem

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

The first major release in 2018 of <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, features many improvements to its panels:

- "Asikohtade paneeli" sektsioone saab nüüd soovi korral peita ning võrguasukohtade näitamiseks on uus "Võrgu" sektsioon.
- "Terminalipaneeli" saab nüüd dokkida akna mis tahes küljele ja kui proovid seda avada, ilma et Konsool paigaldatud oleks, näitab Dolphin hoiatust ja abistab paigaldamisel.
- "Teabepaneeli" HiDPI toetust on täiustatud.

Uuendatud on ka kataloogivaadet ja menüüsid:

- Prügikastikataloog näitab nüüd nuppu "Tühjenda prügikast".
- Nimeviitadest sihtkohtade paremaks leidmiseks on lisatud "sihtkoha näitamise" menüükirje
- Gitiga lõimimist on parandatud: giti kataloogide kontekstimenüü sisaldab nüüd ka toiminguid "git log" ja "git merge".

Täiustused sisaldavad muu hulgas:

- Uus kiirklahv annab võimaluse avada filtririba lihtsalt kaldkriipsu (/) klahvi vajutades.
- Nüüd saab fotosid sortida ja korraldada pildi tegemise aja järgi.
- Paljude väikeste failide lohistamine Dolphinis on nüüd kiirem ning kasutajad saavad hulgi nime muutmise toiminguid tagasi võtta.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

To make working on the command line even more enjoyable, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, can now look prettier:

- Värviskeeme saab alla laadida uue kuuma kraami dialoogiga.
- Kerimisriba sulandub paremini kokku aktiivse värviskeemiga.
- Vaikimisi näidatakse kaardiriba ainult vajaduse korral.

Konsooli arendajad sellega ei piirdunud ja on lisanud mitmeid uusi omadusi:

- Lisatud on uus kirjutuskaitstud režiim ja profiili omadus lülitada teksti kopeerimist HTML-vormingus.
- Konsool toetab nüüd Waylandis lohistamismenüüd.
- Mitmed täiustused puudutavad ZMODEM-i protokolli: Konsool suudab nüüd käidelda zmodemi üleslaadiminsnäidikut B01, näitab edenemist andmete ülekandel, nupp Loobu toimib dialoogis nüüd nii, nagu peab, ning paremini on toetatud suurte failide ülekanne neid 1 MB tükkidena mällu lugedes.

Täiustused sisaldavad muu hulgas:

- Hiirerattaga kerimine libinputi vahendusel on ära parandatud, kerimist läbi kogu ajaloo hiirerattaga kerimisel takistatakse.
- Otsingut värskendatakse pärast regulaaravaldise põhjal vastete otsimise valiku muutmist ning kiirklahvi Ctrl + Backspace vajutamisel hakkab Konsool käituma xterm'i moodi.
- "--background-mode" kiirklahv parandati ära.

### Multimeedia

<a href='https://juk.kde.org/'>JuK</a>, KDE's music player, now has Wayland support. New UI features include the ability to hide the menu bar and having a visual indication of the currently playing track. While docking to the system tray is disabled, JuK will no longer crash when attempting to quit via the window 'close' icon and the user interface will remain visible. Bugs regarding JuK autoplaying unexpectedly when resuming from sleep in Plasma 5 and handling the playlist column have also been fixed.

For the 18.04 release, contributors of <a href='https://kdenlive.org/'>Kdenlive</a>, KDE's non-linear video editor, focused on maintenance:

- Klipi suuruse muutmine ei tekita enam kahju hääbumistele ja võtmekaadritele.
- Kasutajad HiDPI monitoride taga võivad nautida eredamaid ikoone.
- Võimalik krahh teatava seadistusega käivitamisel on parandatud.
- MLT peab nüüd olema vähemalt versiooniga 6.6.0 ja parandatud on ühilduvust.

#### Graafika

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Over the last months, contributors of <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer, worked on a plethora of improvements. Highlights include:

- Lisatud on NPRIS kontrollerite toetus, mis lubab nüüd juhtida täisekraanirežiimis esitlusi KDE Connecti abil, klaviatuuri meedianuppudega või meediamängija plasmoidiga.
- Hiirekursori pisipildi kohale viimisel ilmuvad nupud saab nüüd välja lülitada.
- Kärpimistööriist sai mitmeid täiustusi, suutes nüüd seadistuse meelde jätta teisele pildile lülitudes. Valikukasti kuju saab nüüd lukustada tõstu- või Ctrl-klahvi all hoides, samuti saab lukustada parajasti näidatava pildi proportsioonid.
- Täisekraanirežiimist saab nüüd väljuda klahviga Escape ning värvipalett peegeldab parajasti aktiivset värviskeemi. Kui täisekraanireiimis viibides Gwenview'st väljuda, jäetakse seadistus meelde ja rakenduse uuel avamisel käivitatakse see taas täisekraanirežiimis.

Alati on oluline pöörata tähelepanu pisiasjadele, mistõttu Gwenview viimistlemine puudutas järgmisi asju:

- Gwenview näitab viimati kasutatud kataloogide loendis failide asukohti inimsilmale hoomatavamal kujul, näitab sama loendi elementide kontekstimenüüd ning unustab nõutult kõik, kui sisse lülitada "ajaloo keelamise" võimalus.
- Külgribal kataloogile klõpsamine lubab lülituda sirvimis- ja vaaterežiimi vahel ning peab kataloogide vahel liikudes viimati kasutatud režiimi meeles, mis muudab isegi väga suurtes pildikogudes liikumise märgatavalt kiiremaks.
- Liikumist klaviatuuri abil on jätkuvalt viimistletud ja nüüd näidatakse sirvimisrežiimis korralikult klaviatuurifookust.
- "Laiusele mahutamise" võimalus asendati üldisema "täitmise" võimalusega.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Isegi pisikesed täiustused võivad muuta kasutaja elu mõnusamaks:

- Ühtluse huvides suurendatakse nüüd SVG-pilte samamoodi teistega, kui "Pildivaade->Väikeste piltide suurendamine" on sisse lülitatud.
- Pildi muutmise või muudatuste tagasivõtmise järel ei lähe pildivaade ja pisipilt enam sünkroonist välja.
- Piltide nime muutmisel jäetakse failinime laiend nüüd vaikimisi valimata ning "liiguta/kopeeri"lingi" dialoog näitab vaikimisi aktiivset kataloogi.
- Parandatud rohkelt visuaalseid ebakõlasid näiteks URL-ribal, täisekraanirežiimi tööriistaribal ja pisipiltide kohtspikrite animeerimisel. Lisatud on seni puudunud ikoone.
- Viimaks näitab täisekraanirežiim nupp nüüd hiirekursori all otse pildivaadet, mitte enam kataloogi sisu, ning muude seadistuste all saab täpsemalt paika panna ICC värvirenderduse režiimi.

#### Kontoritöö

In <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's universal document viewer, PDF rendering and text extraction can now be cancelled if you have poppler version 0.63 or higher, which means that if you have a complex PDF file and you change the zoom while it's rendering it will cancel immediately instead of waiting for the render to finish.

PDF JavaScripti toetus AFSimple_Calculate on varasemast parem ja kui poppler on vähemalt versiooniga 0.64, toetab Okular PDF JavaScripti  muutmist ka vormide kirjutuskaitstuse korral.

Management of booking confirmation emails in <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, has been significantly enhanced to support train bookings and uses a Wikidata-based airport database for showing flights with correct timezone information. To make things easier for you, a new extractor has been implemented for emails not containing structured booking data.

Täiustused sisaldavad muu hulgas:

- Uus "ekspertülugin" suudab taas näidata kirjade struktuuri.
- Sieve redaktoris lisati plugin kirjade valimiseks Akonadi andmebaasist.
- Tekstiotsingut redaktoris täiustati korralikult regulaaravaldiste toetusega.

#### Tööriistad

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Improving the user interface of <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, was a major focus area:

- Alumine nupurida on põhjalikult ümber korraldatud ning näitab nüüd nuppu seadistusteakna avamiseks ja uut Tööriistade nuppu, mis lubab avada viimati kasutatud ekraanipiltide kataloogi ja käivitada ekraani salvestamise programmi.
- Viimati kasutatud salvestamisviis jäetakse nüüd vaikimisi meelde.
- Akna suurust kohandatakse nüüd vastavalt ekraanipildi proportsioonile, mille tulemuseks on palju meeldivam ja ruumi kokku hoidev pisipilt.
- Seadistusakent on tunduvalt lihtsustatud.

Lisaks on kasutajatel võimalik oma tööd lihtsustada järgmiste uute omadustega:

- Konkreetsest aknast pildi tegemisel lisatakse selle tiitel automaatselt ekraanipildi failinimele.
- Kasutaja saab nüüd valida, kas Spectacle lõpetab pärast salvestamist või kopeerimist automaatselt töö või mitte.

Olulised veaparandused:

- Lohistamine Chromiumi akendesse töötab nüüd oodatud moel.
- Korduv kiirklahvi kasutamine ekraanipildi salvestamiseks ei too enam kaasa hoiatusdialoogi mitmeti mõistetava kiirklahvi kohta.
- Ristkülikukujulise ala ekraanipildi korral saab valiku alumist serva senisest täpsemalt paika panna.
- Ekraani serva puudutavate akende pildistamine on varasemast usaldusväärsem ka siis, kui komposiit ei ole sisse lülitatud.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

With <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, KDE's certificate manager and universal crypto GUI, Curve 25519 EdDSA keys can be generated when used with a recent version of GnuPG. A 'Notepad' view has been added for text based crypto actions and you can now sign/encrypt and decrypt/verify directly in the application. Under the 'Certificate details' view you will now find an export action, which you can use to export as text to copy and paste. What's more, you can import the result via the new 'Notepad' view.

In <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's graphical file compression/decompression tool with support for multiple formats, it is now possible to stop compressions or extractions while using the libzip backend for ZIP archives.

### KDE rakenduste väljalasetega liituvad rakendused

KDE's webcam recorder <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> and backup program <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> will now follow the Applications releases. The instant messenger <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> is also being reintroduced after being ported to KDE Frameworks 5.

### Omaette väljalaske-ajakavale üle läinud rakendused

The hex editor <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> will have its own release schedule after a request from its maintainer.

### Vigadest vabaks

Üle 170 vea parandati sellistes rakendustes, nagu Kontacti komplekt, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, KGpg, Konsool, Okular, Umbrello jt!

### Täielik muudatuste logi
