---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE toob välja KDE rakendused 16.08.0
layout: application
title: KDE toob välja KDE rakendused 16.08.0
version: 16.08.0
---
18. august 2016. KDE laskis täna välja KDE rakendused 16.08 märkimisväärse hulga uuendustega, mis on nüüd veelgi hõlpsamini kasutatavad, veelgi rohkemate võimalustega ja aina vabamad kõigist neist seni nördimust tekitanud pisivigadest, sõnaga, teel aina lähemale just sinu seadmele ideaalselt sobivate rakendusteni.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> and KDiskFree have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release.

Jätkuvate pingutuste käigus tükeldada Kontacti komplekti teegid nende hõlpsamaks kasutamiseks teistes rakendustes tükeldati kdepimlibs pakettideks akonadi-contacts, akonadi-mime ja akonadi-notes.

Me loobusime järgmistest pakettidest: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu ja mplayerthumbs. See aitab meil paremini keskenduda ülejäänud koodile.

### Ikka Kontactis

<a href='https://userbase.kde.org/Kontact'>The Kontact Suite</a> has got the usual round of cleanups, bug fixes and optimizations in this release. Notable is the use of QtWebEngine in various compontents, which allows for a more modern HTML rendering engine to be used. We have also improved VCard4 support as well as added new plugins that can warn if some conditions are met when sending an email, e.g. verifying that you want to allow sending emails with a given identity, or checking if you are sending email as plaintext, etc.

### Marble uus versioon

<a href='https://marble.kde.org/'>Marble</a> 2.0 is part of KDE Applications 16.08 and includes more than 450 code changes including improvements in navigation, rendering and an experimental vector rendering of OpenStreetMap data.

### Rohkem arhiive

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> can now extract AppImage and .xar files as well as testing the integrity of zip, 7z and rar archives. It can also add/edit comments in rar archives

### Terminali täiustused

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> has had improvements regarding font rendering options and accessibility support.

### Ja veel palju rohkem!

<a href='https://kate-editor.org'>Kate</a> got movable tabs. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>More information...</a>

<a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a>, has added provinces and regions maps of Burkina Faso.

### Agressiivne võitlus kõikvõimalike vigadega

Üle 120 vea parandati sellistes rakendustes, nagu Kontacti komplekt, Ark, Cantor, Dolphin, KCalc, Kdenlive jt!

### Täielik muudatuste logi
