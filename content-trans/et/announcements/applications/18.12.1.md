---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE Ships Applications 18.12.1.
layout: application
major_version: '18.12'
title: KDE Ships KDE Applications 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Cantori, Dolphini, JuKi, Kdenlive'i, Konsooli ja Okulari täiustused.

Täiustused sisaldavad muu hulgas:

- Akregator töötab nüüd Qt 5.11 või uuema WebEngine'iga
- Muusikamängijas JuK parandati ära veergude sortimine
- Konsool renderdab kujundite loomiseks kasutatavaid sümboleid jälle korrektselt
