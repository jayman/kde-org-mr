---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE toob välja KDE rakendused 18.04.3
layout: application
title: KDE toob välja KDE rakendused 18.04.3
version: 18.04.3
---
July 12, 2018. Today KDE released the third stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Arki, Cantori, Dolphini, Gwenview ja KMagi täiustused.

Täiustused sisaldavad muu hulgas:

- Taastati ühilduvus IMAP-serveritega, mis ei anna teada oma võimeid
- Ark suudab nüüd lahti pakkida ZIP-arhiive, millel puuduvad korralikud kataloogikirjed
- KNotesi ekraanimärkmed järgivad liigutamisel jälle hiirekursorit
