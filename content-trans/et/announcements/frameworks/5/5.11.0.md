---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### CMake'i lisamoodulid

- ecm_add_tests() uued argumendid (veateade 345797)

### Raamistike lõimimine

- Korrektse initialDirectory kasutamine KDirSelectDialog'is
- Kontrollimine, kas skeem on määratud, kui tühistatakse alustava URL-i väärtus
- Ainult olemasolevate kataloogide arvestamine FileMode::Directory režiimis

### KActivities

(muutuste logi puudub)

### KAuth

- KAUTH_HELPER_INSTALL_ABSOLUTE_DIR muutmine kättesaadavaks kõigile KAuth'i kasutajatele

### KCodecs

- KEmailAddress: ülekoormuse lisamine extractEmailAddress'ile ja firstEmailAddress'ile, mis tagastab veateate.

### KCompletion

- Soovimatu valiku parandamine failinime muutmisel failidialoogis (veateade 344525)

### KConfig

- Krahhi vältimine, kui QWindow::screen() on null
- KConfigGui::setSessionConfig() lisamine (veateade 346768)

### KCoreAddons

- Uus KPluginLoader::findPluginById() API

### KDeclarative

- ConfigModule'i loomise toetus KPluginMetadata'st
- pressAndhold sündmuste parandamine

### KDELibs 4 toetus

- QTemporaryFile'i kasutamine ajutise faili otsese andmise asemel.

### KDocTools

- Tõlgete uuendamine
- customization/ru uuendamine
- Valede linkidega olemite parandamine

### KEmoticons

- Teema puhverdamine lõimimispluginas

### KGlobalAccel

- [käitusaeg] Platvormipõhise koodi liigutamine pluginatesse

### KIconThemes

- KIconEngine::availableSizes() optimeerimine

### KIO

- Do not try to complete users and assert when prepend is non-empty. (bug 346920)
- KPluginLoader::factory() kasutamine KIO::DndPopupMenuPlugin laadimisel
- Tupiku parandamine võrgupuhverserverite kasutamisel (veateade 346214)
- KIO::suggestName parandamine faililaiendite säilitamiseks
- kbuildsycoca4 väljalülitamine sycoca5 uuendamisel.
- KFileWidget: faile ei aktsepteerida kataloogirežiimis
- KIO::AccessManager: järjestikuste QIODevice'ide asünkroonse käitlemise lubamine

### KNewStuff

- Uue meetodi fillMenuFromGroupingNames lisamine
- KMoreTools: paljude uute rühmitamiste lisamine
- KMoreToolsMenuFactory: "git-clients-and-actions" käitlemine
- createMenuFromGroupingNames: URL-i parameeter ei ole enam kohustuslik

### KNotification

- NotifyByExecute'i krahhi parandamine, kui ühtegi vidinat pole määratud (veateade 348510)
- Suletud märguannete käitlemise täiustamine (veateade 342752)
- QDesktopWidget'i kasutamise asendamine QScreen'iga
- Tagamine, et KNotification'it saaks kasutada ka graafilise kasutajaliideseta lõime korral

### Paketiraamistik

- Struktuuri qpointer'i juurdepääsu kaitsmine (veateade 347231)

### KPeople

- QTemporaryFile'i kasutamine /tmp otsese andmise asemel.

### KPty

- tcgetattr &amp; tcsetattr kasutamine, kui võimalik

### Kross

- Krossi moodulite "forms" ja "kdetranslation" laadimise parandamine

### KService

- Administraatori õigustes töötamise korral olemasolevate puhverfailide omanikuõiguse säilitamine (veateade 342438)
- Kaitsmine suutmatuse eest avada voogu (veateade 342438)
- Faili kirjutamisel vigaste õiguste kontrollimise parandamine (veateade 342438)
- ksycoca päringute parandamine x-scheme-handler/* pseudo-MIME tüüpide puhul (veateade 347353)

### KTextEditor

- Kolmanda poole rakendustel/pluginatel oma esiletõstmise XML-failide paigaldamise lubamine kataloogi katepart5/syntax, nagu see käis ka KDE 4.x ajal
- KTextEditor::Document::searchText() lisamine
- KEncodingFileDialog taas kasutusele võtmine (veateade 343255)

### KTextWidgets

- Meetodi lisamine dekoraatorile
- Kohandatud sonneti dekoraatori kasutamise lubamine
- "Leia eelmine" võimaldamine KTextEdit'is.
- speech-to-text toetuse tagasilisamine

### KWidgetsAddons

- KAssistantDialog: abinupu taaslisamine, nagu see oli KDELibs4 versioonis

### KXMLGUI

- Seansihalduse lisamine KMainWindow'le (veateade 346768)

### NetworkManagerQt

- WiMAX-i toetusest loobumine NM 1.2.0 ja uuemates versioonides

### Plasma raamistik

- Kalendrikomponendid näitavad nüüd nädalanumbreid (veateade 338195)
- QtRendering'i kasutamine paroolivälja fontide korral
- AssociatedApplicationManager'i otsingu parandamine, kui on MIME tüüp (veateade 340326)
- Paneeli taustatooni parandamine (veateade 347143)
- Sõnumist "Apleti laadimine nurjus" lahtisaamine
- Võime laadida QML-is kcms'e plasmoidi seadistusaknas
- Enam ei kasutata DataEngineStructure'i aplettides
- libplasma maksimaalne portimine sycoca pealt ära
- [plasmacomponents] SectionScroller järgib ListView.section.criteria-t
- Kerimisribasid enam puuteekraani korral automaatselt ei peideta (veateade 347254)

### Sonnet

- Ühe keskse puhvri kasutamine SpellerPlugins'ites.
- Ajutiste ressursside vähendamine.
- Optimeerimine; sõnaraamatupuhvrit ei tühjendada spelleri objektide kopeerimisel.
- save() väljakutsete optimeerimine kohe 

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
