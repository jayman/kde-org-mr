---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (muutuste logi puudub)

### KConfig

- QML-kõlblike klasside genereerimine kconfigcompiler'i abil

### KCoreAddons

- Uus cmake'i makro kcoreaddons_add_plugin KPluginLoader'i põhiste pluginate hõlpsamaks loomiseks.

### KDeclarative

- Krahhi likvideerimine tekstuuripuhvris.
- ja muud parandused

### KGlobalAccel

- Uue meetodi globalShortcut lisamine, mis hangib globaalsetes seadistustes määratud kiirklahvi.

### KIdleTime

- Kidletime'i krahhi vältimine waylandi platvormil

### KIO

- Lisati KPropertiesDialog::KPropertiesDialog(urls) ja KPropertiesDialog::showDialog(urls).
- Asünkroonne QIODevice-põhine andmete tõmbamine KIO::storedPut ja KIO::AccessManager::put puhul.
- QFile::rename tagastatava väärtuse tingimuste parandamine (veateade 343329)
- KIO::suggestName parandamine paremate nimede pakkumise huvides (veateade 341773)
- kioexec: kurl'i kirjutatava asukoha parandamine (veateade 343329)
- Järjehoidjate salvestamine ainult faili user-places.xbel (veateade 345174)
- RecentDocuments'i kirje topelt, kui kahel failil peaks olema sama nimi
- Parem veateade, kui üksik fail on liiga suur prügikasti viskamiseks (veateade 332692)
- KDirListeri krahhi kõrvaldamine ümbersuunamisel, kui pesa kutsub välja openURL-i

### KNewStuff

- Uus valik klasse nimedega KMoreTools ja sarnased. KMoreTools aitab lisada vihjeid väliste tööriistade kohta, mida võib-olla ei ole isegi veel paigaldatud. Lisaks muudab see pikad menüüd lühemaks, pakkudes välja kasutaja seadistamise võimalusega põhi- ja lisasektsioonid.

### KNotifications

- KNotificationsi parandamine kasutamisel koos Ubuntu NotifyOSD-ga (veateade 345973)
- Märguannete uuendamist ei käivitata samade omaduste määramisel (veateade 345793)
- Lipu LoopSound lisamine, mis lubab märguannetel vajaduse korral esitada heli korduvalt (veateade 346148)
- Krahhi vältimine, kui märguandel pole vidinat

### KPackage

- Funktsiooni KPackage::findPackages lisamine, mis sarnaneb funktsiooniga KPluginLoader::findPlugins

### KPeople

- KService'i asemel pluginate aktualiseerimiseks KPluginFactory kasutamine

### KService

- Kirje asukoha väära tükeldamise parandamine [veateade 344614)

### KWallet

- Migreerimisagent kontrollib nüüd enne alustamist, kas vana turvalaegas on tühi (veateade 346498)

### KWidgetsAddons

- KDateTimeEdit: Parandus, et kasutaja sisendit ka tegelikult arvestataks. Topeltpiirete parandamine.
- KFontRequester: ainult püsisammfontide valimise parandamine

### KWindowSystem

- QX11Infost sõltumise lõpetamine KXUtils::createPixmapFromHandle'is (veateade 346496)
- uus meetod NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Kiirklahvide parandamine teisese kiirklahvi määramisel (veateade 345411)
- Bugzilla toodete/komponentide loendi uuendamine veast teatamisel (veateade 346559)
- Globaalsed kiirklahvid: ka alternatiivse kiirklahvi seadistamise lubamine

### NetworkManagerQt

- Paigaldatud päised korraldatakse nüüd nii nagu teistes raamistikes.

### Plasma raamistik

- PlasmaComponents.Menu toetab nüüd sektsioone
- KPluginLoaderi kasutamine ksycoca asemel C++ andmemootorite laadimisel
- visualParent'i pööramise kaalumine popupPosition'is (veateade 345787)

### Sonnet

- Esiletõstmist ei rakendata, kui ei leita õigekirja kontrollijat. See tekitab lõputu kordamise - rehighlighRequest'i taimer alustab aina uuesti otsast peale.

### Frameworkintegration

- Fix native file dialogs from widgets QFileDialog: ** File dialogs opened with exec() and without parent were opened, but any user-interaction was blocked in a way that no file could be selected nor the dialog closed. ** File dialogs opened with open() or show() with parent were not opened at all.

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
