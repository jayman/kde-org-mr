---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Üldised muutused

- cmake 3.10+ AUTOMOC'i hoiatuste parandused
- Kategoriseeritud logimise laialdasem kasutamine, et silumisväljund üldse vaikimisi välja lülitada (taaslubamiseks tarvitada kdebugsetting'it)

### Baloo

- balooctl olek: kõigi argumentide töötlemine
- Mitmesõnaliste siltide päringu parandus
- Nime muutmise tingimuste lihtsustamine
- Vale UDSEntry näidatava nime parandus

### Breeze'i ikoonid

- Ikooninime parandus: "weather-none" -&gt; "weather-none-available" (veateade 379875)
- Vivaldi ikooni eemaldamine, sest algupärane rakenduseikoon sobib perfektselt breeze'iga (veateade 383517)
- Mõne puuduva MIME lisamine
- Breeze'i ikoonidesse lisati ikoon document-send )veateade 388048)
- albumi esitaja ikooni uuendamine
- labplot-editlayout'i toetuse lisamine
- duplikaatide eemaldamine ja tumeda teema uuendamine
- gnumeric'i breeze-icon'i toetuse lisamine

### CMake'i lisamoodulid

- readelf'i kasutamine projekti sõltuvuste leidmiseks
- INSTALL_PREFIX_SCRIPT'i pakkumine prefiksite hõlpsaks määramiseks

### KActivities

- krahhi vältimine kactivities'is, kui dbus'i ühendust pole saada

### KConfig

- API dokumentatsioon: selgitamine, kuidas kasutada KWindowConfig'it dialoogi konstruktorist
- KDesktopFile::sortOrder() märkimine iganenuks
- KDesktopFile::sortOrder() tulemuse parandus

### KCoreAddons

- CMAKE_AUTOMOC_MACRO_NAMES'i laiendamine ka omaehitustele
- Litsentsivõtmete sobitamine spdx'ga

### KDBusAddons

- Absoluutse asukoha tuvastamise parandus cmake 3.5 korral Linuxis
- cmake'i funktsiooni 'kdbusaddons_generate_dbus_service_file' lisamine

### KDeclarative

- qml'i juhtelemendid juhtimismooduli loomisel

### KDED

- cmake'i funktsiooni 'kdbusaddons_generate_dbus_service_file' (kdbusaddons'ist) kasutamine dbus'i teenusefaili genereerimiseks
- Omaduse used lisamine teenusefaili definitsioonile

### KDELibs 4 toetus

- Kasutaja teavitamine, kui moodulit ei saa registreerida org.kde.kded5 peal, ja väljumine tõrketeatega
- Mingw32 kompileerimise parandus

### KDocTools

- Michael Pyne'i olemi lisamine
- Martin Kolleri olemite lisamine contributor.entities'isse
- debiani kirje parandus
- Debiani olemi lisamine general.entities'isse
- imporditud kbackup'i olemi lisamine
- latex'i olemi lisamine, meil on juba 7 olemit kf5 erinevates index.docbook'ides

### KEmoticons

- skeemi (file://) lisamine. mis on hädavajalik, kui me kasutame seda qml'is ja oleme lisanud kõik

### KFileMetaData

- QtMultimedia-põhise ekstraktori eemaldamine
- Linuxi kontrollimine TagLib'i asemel ja usermetadatawritertest'i ehitamise vältimine Windows'is
- # 6c9111a9 taastamine, kuni on võimalik edukas ehitamine ja linkimine ilma TagLib'ita
- taglib'i sõltuvuse eemaldamine, mida tingis ripakile jäänud kaasatu

### KHTML

- Lõpuks silumisväljundi keelamise lubamine ja selle asemel kategoriseeritud logimise kasutamine

### KI18n

- ts-pmap-compile'i ei kohelda täitmisfailina
- Mälulekke parandus KuitStaticData's
- KI18n: mitme topeltotsingu parandus

### KInit

- Koodi eemaldamine, mida on võimatu kasutada

### KIO

- Properly parse dates in cookies when running in non-English locale (bug 387254)
- [kcoredirlister] alamasukoha loomise parandus
- prügikasti oleku kaajstamine iconNameForUrl'is
- QComboBox'i signaalide edastamine QComboBox'i redigeerimisrea signaalide asemel
- Parandus: veebikiirkorraldused näitasid asukohta, mitte inimesele mõistetavat nime
- TransferJob: parandus, kui readChannelFinished väljastatatakse, enne kui alustamine on välja kutsutud (veateade 386246)
- Krahhi vältimine, eeldatavasti alates Qt 5.10? )veateade 386364)
- KUriFilter: olematute failide korral ei tagastata viga
- Asukohtade loomise parandus
- kfile'i dialoogi teostus, kus me saame lisada kohandatud vidina
- Kontroll, et qWaitForWindowActive ei nurju
- KUriFilter: portimine KServiceTypeTrader'i pealt ära
- API dox: näidisekraanipiltide tiitlites kasutatakse klassinimesid
- API dox: ka KIOWIDGETS'i eksportmakrode käsitlemine QCH loomisel
- KCookieAdvice::AcceptForSession'i käitlemise parandus (veateade 386325)
- Loodi 'GroupHiddenRole' KPlacesModel'ile
- soklitõrke stringi edastamine KTcpSocket'ile
- Koodi ümberkorraldamine ja topeltkoodi eemaldamine kfileplacesview's
- Signaali 'groupHiddenChanged' väljastamine
- Animatsiooni peitmise/näitamise kasutuse ümberkorraldamine KFilePlacesView's
- Kasutaja võib nüüd peita kogu asukohtade rühma KFilePlacesView's (veateade 300247)
- Asukoharühmade peitmise teostus KFilePlacesModel'is (veateade 300247)
- [KOpenWithDialog] liigse KLineEdit'i loomise eemaldamine
- Tagasivõtmise toetuse lisamine BatchRenameJob'ile
- BatchRenameJob'i lisamine KIO'sse
- Parandus: doxygeni koodiplokk ei lõppenud lõpetuskoodiga

### Kirigami

- plingitava hoidmine interaktiivsena
- kohane prefiks ka ikoonidele
- vormi suuruse muutmise parandus
- wheelScrollLines'i lugemine kdeglobals'ist, kui see on olemas
- konfliktide vältimiseks prefiksi lisamine kirigami failidele
- mõningad staatilise linkimise parandused
- plasma stiilide liigutamine plasma-framework'i
- Ühekordsete jutumärkide kasutamine sümbolite + QLatin1Char'i vastendamisel
- FormLayout

### KJobWidgets

- QWindow API pakkumine KJobWidgets:: decorators'ile

### KNewStuff

- Nõudepuhvri suuruse piiramine
- Sama sisemise versiooni nõudmine, mida ehitatakse
- Globaalsete muutujate kasutamise vältimine pärast vabastamist

### KNotification

- [KStatusNotifierItem] vidina asukohta ei "taastata" selle esimesel näitamisel
- Süsteemisalve pärandikoonide asukohtade kasutamine minimeerimis/taastamistoimingutel
- Vasakklõpsu asukoha käitlemine süsteeemisalve pärandikoonidel
- kontekstimenüüd ei muudeta aknaks
- Selgitava kommentaari lisamine
- KNotification'i pluginate loid loomine ja loid laadimine

### KPackage raamistik

- käitusaja puhvri kehtivuse tühistamine paigaldamisel
- eksperimentaalne toetus rcc-failide laadimisele kpackage'is
- kompileerimine Qt 5.7 peal
- Pakettide indekseerimise parandus ja käitusaja puhverdamise lisamine
- uus meetod KPackage::fileUrl()

### KRunner

- [RunnerManager] ThreadWeaveri lõimede arvu ei aeta sassi

### KTextEditor

- Metamärkide vastendamise parandus režiimiridadel
- Tagailanguse parandus, mida põhjustas klahvi Backspace käitumise muutus
- portimine mitteiganenud API peale, nagu juba mitmes kohas (veateade 386823)
- Puuduva kaasatu lisamine std::array'le
- MessageInterface: CenterInView lisamine täiendava positsioonina
- QStringList'i initsialiseerija nimekirja puhastus

### KWalleti raamistik

- Korrektse teenuse täitmisfaili aukoha kasutamine kwalletd dbus'i teenuse paigaldamiseks Win32 peal

### KWayland

- Nimede ebaühtluse parandus
- Liidese loomine serverile dekoratsioonipalettide edastamiseks
- std::bind funktsioonide otsene kaasamine
- [server] meetodi IdleInterface::simulateUserActivity lisamine
- Tagasilanguse parandus, mida põhjustas tagasiühilduvuse toetus andmeallikas
- Andmeseadmehalduri liidese versiooni 3 toetuse lisamine (veateade 386993)
- Testi eksporditud/imporditud objektide kehtivusala
- Tõrke parandus WaylandSurface::testDisconnect'is
- Otsese AppMenu protokolli lisamine
- Genereeritud faili automoc'i võimalusest väljajätmise parandus

### KWidgetsAddons

- Krahhi parandus setMainWindow's wayland'i peal

### KXMLGUI

- API dox: doxygen pannakse taas kõnelema seansiga seotud makrodest ja funktsioonidest
- shortcutedit'i pesa lahutamine vidina hävitamisel (veateade 387307)

### NetworkManagerQt

- 802-11-x: PWD EAP meetodi toetus

### Plasma raamistik

- [Air'i teema] tegumiriba edenemise graafika lisamine (veateade 368215)
- Mallid: stray * eemaldamine litsentsipäistest
- packageurlinterceptor'i muutmine nii noop'iks kui vähegi võimalik
- "Renderdaja tegevust ei lõpetata ja muud hädavajalikku ,kui Svg::setImagePath on välja kutsutud sama argumendiga" tagasivõtmine
- kirigami plasma stiilide liigutamine siia
- haihtuvad kerimisribad mobiilis
- KPackage'i isendi taaskasutamine PluginLoader'i ja Applet'i vahel
- [AppletQuickItem] QtQuick Controls 1 stiili määramine ainult korra mootori kohta
- Plasma::Dialog'is ei määrata aknaikooni
- [RTL] - valitud teksti kohane joondus RTL-i korral (veateade 387415)
- skaleerimisteguri algväärtustamine mis tahes isendile määratud viimase skaleerimisteguri peale
- "skaleerimisteguri algväärtustamine mis tahes isendile määratud viimase skaleerimisteguri peale" tagasivõtmine
- Ei uuendata, kui aluseks oleva FrameSvg ümberjoonistamine on blokitud
- skaleerimisteguri algväärtustamine mis tahes isendile määratud viimase skaleerimisteguri peale
- if'i kontrolli liigutamine #ifdef'i sisse
- [FrameSvgItem] tarbetuid sõlmi ei looda
- Renderdaja tegevust ei lõpetata ja muud hädavajalikku ,kui Svg::setImagePath on välja kutsutud sama argumendiga

### Prison

- Samuti silumissufiksiga qrencode'i otsimine

### QQC2StyleBridge

- lihtsustamine ja ei püüta blokkida hiiresündmusi
- kui wheel.pixelDelta puudub, kasutatakse globaalseid hiireratta kerimisridu
- töölaua kaardiribadel on iga kaardi laius erinev
- tagamine, et poleks ühtegi 0-suurusega vihjet

### Sonnet

- Sisemiste abiliste täitmisfaile ei ekspordita
- Sonnet: vale keele pakkumiste parandus mitmes keeles tekstide korral
- Igivana ja katkise hädalahenduse eemaldamine
- Windowsis ei põhjustata ringlinkimist

### Süntaksi esiletõstmine

- Esiletõstmise indekseerija: hoiatus kontekstilüliti fallthroughContext="#stay" korral
- Esiletõstmise indekseerija: hoiatus tühjade atribuutide korral
- Esiletõstmise indekseerija: tõrgete lubamine
- Esiletõstmise indekseerija: teadaandmine kasutamata ItemData'dest ja puuduvatest ItemData'dest
- Prolog, RelaxNG, RMarkDown: esiletõstmise probleemide parandus
- Haml: vigaste ja kasutamata itemData'de parandus
- ObjectiveC++: topeltkommentaarikontekstide eemaldamine
- Diff, ObjectiveC++: esiletõstmisfailide puhastus ja parandus
- XML (silumine): vale DetectChar'i reegli parandus
- Esiletõstmise indekseerija: kõigi esiletõstmiste konteksti kontrollimise toetus
- Tagasivõtmine: GNUMacros'i lisamine uuesti gcc.xml'ile, mida kasutab isocpp.xml
- email.xml: *.email'i lisamine laiendustesse
- Esiletõstmise indekseerija: lõputute silmuste kontroll
- Esiletõstmise indekseerija: tühjade kontekstinimede ja regulaaravaldiste kontroll
- Olematutele võtmesõnade loenditele viitamise parandus
- Topeltkonteksti lihtsamate juhtumite parandus
- Topelt ItemData'de parandus
- DetectChar'i ja Detect2Chars'i reeglite parandus
- Esiletõstmise indekseerija: võtmesõnade loendite kontroll
- Esiletõstmise indekseerija: hoiatus topeltkonteksti korral
- Esiletõstmise indekseerija: topelt ItemData'de kontroll
- Esiletõstmise indekseerija: DetectChar'i ja Detect2Chars'i kontroll
- ItemData kõigi atribuutide olemasolu valideerimine

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
