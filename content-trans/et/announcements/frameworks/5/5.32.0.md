---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Pesastatud siltide teostus

### Breeze'i ikoonid

- Plasma Vaulti ikoonide lisamine
- Krüptitud ja lahtikrüptitud kataloogide ikoonide nime muutmine
- Torrentide 22px ikooni lisamine
- nm-tray ikoonide lisamine (veateade 374672)
- color-management: defineerimata linkide eemaldamine (veateade 374843)
- system-run on nüüd toiming kuni &lt;= 32px ja 48px rakenduseikoon (veateade 375970)

### CMake'i lisamoodulid

- Inotify tuvastamine
- "Puhaste virtuaalsete funktioonidega klasside automaatne märkimine "Abstract'iks"" tagasivõtmine

### KActivitiesStats

- Etteplaneerimise ja loendist veel puuduva elemendi järjekorra määramise lubamine

### KArchive

- Võimaliku mälulekke parandus, millele osutas "limitedDev"

### KCMUtils

- Võimaliku krahhi parandus QML-i juhtimismoodulis, kui rakenduse palett muutub

### KConfig

- KConfig: KConfigBackend'i eksportimine ja paigaldamine lõpetatakse

### KConfigWidgets

- KColorScheme: vaikimisi rakenduse skeem, kui selle on nääranud KColorSchemeManager (veateade 373764)
- KConfigDialogManager: muutmise signaali saamine metaObject'ist või eriomadusest
- KCModule::setAuthAction'i tõrkekontrolli parandus

### KCoreAddons

- (6) välistamine emotikonide äratundmisest
- KDirWatch: mälulekke parandamine hävitamisel

### KDELibs 4 toetus

- Vea parandamine kfiledialog.cpp-s, mis põhjustas omavidinate kasutamisel krahhi

### KDocTools

- meinproc5: linkimine failide, mitte teegia (veateade 377406)
- KF5::XsltKde staatilise teegi eemaldamine
- Sobiva jagatud teegi eksportimine KDocTools'ile
- Portimine kategoriseeritud logimise peale ja kaasatute puhastamine
- Üheainsa fail ekstraktimise funktsiooni lisamine
- Ehitamise varakult nurjunuks tunnistamine, kui xmllint pole saadaval (veateade 376246)

### KFileMetaData

- kfilemetadata uus hooldaja
- [ExtractorCollection] MIME tüübi päriluse kasutamine pluginate tagastamiseks
- uue omaduse DiscNumber lisamine mitmeplaadiliste albumite helifailidele

### KIO

- Küpsiste juhtimismoodul: nupu "Kustuta" keelamine, kui pole ühtegi aktiivset elementi
- kio_help: uue KDocTools'i eksporditud jagatud teegi kasutamine
- kpac: URL-ide korrastamine enne nende edastamist FindProxyForURL'ile (turvaparandus)
- Võrgu IO-mooduli import plasma-workspace'ist
- kio_trash: tipptaseme failide ja kataloogide nime muutmise teostus
- PreviewJob: kohalike failide vaikimisi maksimumsuuruse eemaldamine
- DropJob: rakenduse toimingute avatud menüüsse lisamise lubamine
- ThumbCreator: DrawFrame'i iganenuks märkimine, nagu arutati https://git.reviewboard.kde.org/r/129921/

### KNotification

- flatpaki portaalide toetuse lisamine
- desktopfilename'i saatmine osana notifyByPopup 'i vihjest
- [KStatusNotifierItem] minimeeritud akna taastamine normaalsena

### KPackage raamistik

- Tihendatud pakettide avamise toetuse lõpetamine

### KTextEditor

- Kasutaja määratud failitüüp jäetakse seansside vahel meelde
- Failitüübi lähtestamine url-i avamisel
- getter'i lisamine word-count'i seadistusväärtusele
- Kooskõlaline teisendamine kursorist koordinaatidesse ja vastupidi
- Failitüübi uuendamine salvestamisel ainult siis, kui asukoht muutub
- EditorConfig'i seadistustefailide toetus (vt üksikasju: http://editorconfig.org/)
- FindEditorConfig'i lisamine ktexteditor'ile
- Parandus: emmetToggleComment'i toiming ei tööta (veateade 375159)
- Algussuurtähe kasutamine redigeerimisväljade pealdistekstides
- :split ja :vsplit tähenduste pööramine tagurpidi, et need sobiksid vi ja Kate toimingutega
- C++11 log2() kasutamine log() / log(2) asemel
- KateSaveConfigTab: eraldaja asetamine kaardi Muu viimase rühma järele, mitte sisse
- KateSaveConfigTab: vale veerise eemaldamine kaardi Muu sisu ümbert
- Piirete seadistamise alamlehekülg: kerimisriba nähtavuse parandus, et liitkast valesse kohta ei satuks

### KWidgetsAddons

- KToolTipWidget: kohtspikri peitmine enterEvent'is, kui hideDelay on null
- Parandus: KEditListWidget kaotas nuppudele klõpsamisel fookuse
- Hanguli silpide dekomponeerimine Hangul Jamos
- KMessageWidget: käitumise parandus kattuvate animatedShow/animatedHide väljakutsete korral

### KXMLGUI

- KConfigi võtmetes ei kasutata längkriipse

### NetworkManagerQt

- Introspektsioonide ja genereeritud failide sünkroonimine NM 1.6.0-ga
- Manager: deviceAdded'i kahekordse väljastamise parandus, kui NM uuesti käivitub

### Plasma raamistik

- vaikevihjete määramine, kui repr ei ekspordi Layout.* (veateade 377153)
- võimaldamine määrata konteinerile expanded=false
- [Menuüü saadaoleva ruumi korrigeerimise täiustus openRelative puhul
- setImagePath'i loogika liigutamine updateFrameData()-sse (veateade 376754)
- IconItem: omaduse roundToIconSize lisamine
- [SliderStyle] elemendi "hint-handle-size" pakkumise lubamine
- Kõigi ühenduste sidumine toiminguga QMenuItem::setAction'is
- [ConfigView] KIOSK-i juhtimismooduli piirangute arvestamine
- Ketramisanimatsiooni keelamise parandus, kui hõivatuse näidikul pudub läbipaistmatus
- [FrameSvgItemMargins] Ei uuendata repaintNeeded'i korral
- Plasma Vaulti rakenduseikoonid
- AppearAnimation'i ja DisappearAnimation'i migreerimine Animators'i
- visualParent'i alumise ja ülemise serva joondamine TopPosedLeftAlignedPopup'i korral
- [ConfigModel] dataChanged'i väljastamine, kui ConfigCategory muutub
- [ScrollViewStyle] omaduse frameVisible hindamine
- [Nupustiilid] Layout.fillHeight kasutamine parent.height'i asemel Layout'is (veateade 375911)
- [ContainmentInterface] ka konteineri kontekstimenüü joondamine paneeliga

### Prison

- Min qt versiooni parandus

### Solid

- Diskette näidatakse nüüd kui Diskette", mitte kui "0 B eemaldatavaid andmekandjaid"

### Süntaksi esiletõstmine

- Rohkemate võtmesõnade lisamine. Võtmesõnade õigekirja kontrolli keelamine
- Rohkemate võtmesõnade lisamine
- Faililaienduse *.RHTML lisamine Ruby on Rails'i esiletõstmisse (veateade 375266)
- SCSS-i ja CSS-i süntaksi esiletõstmise uuendamine (veateade 376005)
- less'i esiletõstmine: uut regiooni alustavate üherealiste kommentaaride parandus
- LaTeX-i esiletõstmine: keskkonna alignat parandus (veateade 373286)

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
