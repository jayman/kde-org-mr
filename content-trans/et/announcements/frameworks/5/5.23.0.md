---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Tõeliselt pakkujate selgitamise võimaldamine meile antud URL-i põhjal
- QDebug'i abiliste pakkumine mõnele Attica klassile
- Absoluutsete URL-ide ümbersuunamise parandus (veateade 354748)

### Baloo

- Tühimärkide kasutamine siltide KIO-moodulis (veateade 349118)

### Breeze'i ikoonid

- CMake'i valiku lisamine binaarse Qt ressursi ehitamiseks kataloogi icons põhjal
- Palju uusi ja uuendatud ikoone
- Ühendamata võrgu ikooni uuendamine, et see erineks rohkem ühendatud võrgu ikoonist (veateade 353369)
- ühendamise ja lahutamise ikooni uuendamine (veateade 358925)
- mõne avatari lisamine plasma-desktop/kcms/useraccount/pics/sources-st
- chromiumi ikooni eemaldamine, sest chromiumi vaikeikoon sobib väga hästi (veateade 363595)
- konsooli ikoonide muutmine heledamaks (veateade 355697)
- kirjaikoonide lisamine thunderbird'ile (veateade 357334)
- avaliku võtme ikooni lisamine (veateade 361366)
- process-working-kde eemaldamine, sest kasutada tuleks konquerori ikoone (veateade 360304)
- krusaderi ikoonide uuendamine vastavalt veateatele 359863
- mikrofoniikoonide nime muutmine D1291 järgi (veateade D1291)
- Mõne skripti MIME tüübi ikooni lisamine (veateade 363040)
- virtuaalse klaviatuuri ja puutepadja sisse-väljalülitamise funktsionaalsuse lisamine OSD-le

### Raamistike lõimimine

- Kasutamata sõltuvuste eemaldamine ja tõlgete käitlemine

### KActivities

- Omaduse runningActivities lisamine Consumer'ile

### KDE Doxygeni tööriistad

- API dokumentatsiooni genereerimise põhjalik ümbertöötamine

### KCMUtils

- QQuickWidget'i kasutamine QML juhtimismoodulites (veateade 359124)

### KConfig

- KAuthorized kontrolli vahelejätmise vältimine

### KConfigWidgets

- Uues laadis ühenduse süntaksi kasutamise lubamine KStandardAction::create()-ga

### KCoreAddons

- Nurjunud plugina näitamine määramishoiatuse märguande korral
- [kshareddatacache] vigase &amp; kasutuse parandus joondamata lugemiste vältimiseks
- Kdelibs4ConfigMigrator: taasparsimise vahelejätmine, kui midagi ei migreeritud
- krandom: testjuhtumi lisamine veateate 362161 lahendamiseks (seemne automaatse loomise nurjumine)

### KCrash

- unixi domeeni pesa asukoha suuruse kontroll enne sellesse kopeerimist

### KDeclarative

- Valitud oleku toetus
- KCMShell'i importi saab nüüd kasutada päringuks, kas juhtimismooduli avamine on tõepoolest lubatud

### KDELibs 4 toetus

- Hoiatus, et KDateTimeParser::parseDateUnicode ei ole veel teostatud
- K4TimeZoneWidget: lipupiltide korrektne asukoht

### KDocTools

- Klahvide levinumate olemite lisamine faili en/user.entities
- man-docbook malli uuendamine
- Raaamatumalli + manuaalilehekülje malli uuendamine + artiklimalli lisamine
- kdoctools_create_handbook väljakutsumine ainult index.docbook'i peal (veateade 357428)

### KEmoticons

- Emojide toetuse lisamine KEmoticon'i + Emoji One ikoonide lisamine
- Kohandatud emotikonisuuruste toetuse lisamine

### KHTML

- Coverity teatatud võimaliku mälulekke parandus ja koodi lihtsustamine
- Kihtide arv määratakse komadega eraldatud väärtuste arvu järgi omaduses 'background image'
- Tausta asukoha parsimise parandus kiirklahvi deklaratsioonis
- Korraliku allika puudumisel uut fontFace'i ei looda

### KIconThemes

- KIconThemes ei sõltu Oxygen'ist (veateade 360664)
- Ikoonide valitud oleku kontseptsioon
- Süsteemsete värvide kasutamine ühevärvilistel ikoonidel

### KInit

- Trügimise vältimine, kui X11 küpsist sisaldaval failil on mõneks ajaks valed õigused
- /tmp/xauth-xxx-_y õiguste parandus

### KIO

- Selgema veateate esitamine, kui KRun(URL)-ile antakse URL ilma skeemita (veateade 363337)
- KProtocolInfo::archiveMimetypes() lisamine
- valitud ikooni režiimi kasutamine faili avamise dialoogi külgribal
- kshorturifilter: tagasilanguse parandus, kui mailto: jäetakse prefiksita, kui e-posti programmi pole paigaldatud

### KJobWidgets

- Korrektse lipu "dialog" määramine edenemisvidina dialoogis

### KNewStuff

- KNS3::DownloadManager'i ei initsialiseerita valede kategooriatega
- KNS3 laiendamine::kirje avalik API

### KNotification

- heli-URL-i loomiseks QUrl::fromUserInput'i kasutamine (veateade 337276)

### KNotifyConfig

- heli-URL-i loomiseks QUrl::fromUserInput'i kasutamine (veateade 337276)

### KService

- Suurtäheliste MIME tüüpidega seostuvate rakenduste parandus
- MIME tüüpide otsinguvõtme muutmine väiketäheliseks, et see oleks tõstutundetu
- Ksycoca märguannete parandus, kui andmebaasi veel ei ole

### KTextEditor

- Vaikimisi kodeeringuna kasutatakse UTF-8 (veateade 362604)
- Vaikestiili "Error" värvi seadistatavuse parandus
- Otsimine ja asendamine: taustavärvi asendamise parandus (tagasilangus versioonis 5.22) (veateade 363441)
- Uus värviskeem Breeze Dark, vt https://kate-editor.org/?post=3745
- KateUndoManager::setUndoRedoCursorOfLastGroup(): Cursor'i edastamine const-referentsina
- sql-postgresql.xml süntaksi esiletõstmise täiustus mitmerealiste funktsioonide kehamit eirates
- Elixiri ja Kotlini süntaksi esiletõstmise lisamine
- VHDL süntaksi esiletõstmine ktexteditor'is: arhitektuurilausetes esinevate funktsioonide toetuse lisamine
- vimode: krahhi vältimine, kui on antud olematu käsu vahemik (veateade 360418)
- Kohane liitsümbolite eemaldamine India kirjade puhul

### KUnitConversion

- Rahavahetuskursside allalaadimise parandus (veateade 345750)

### KWalleti raamistik

- KWalletd migreerimine: vigade käitlemise parandus, välditakse migreerimist iga taaskäivituse korral.

### KWayland

- [klient] PlasmaWindow' ressursiversiooni ei kontrollita
- Algoleku sündmuse lisamine Plasma Window protokolli
- [server] veast teatamine, kui siirdepäring püüab ennast eellaseks muuta
- [server] korralik juhtumi käitlemine, kui PlasmaWindow on seondamata, enne kui klient selle seondab
- [server] destruktori kohane käitlemine SlideInterface'is
- Puutesündmuste toetuse lisamine fakeinput'i protokollis ja liideses
- [server] destruktrori nõude käitlemise standardimine Resources'is
- wl_text_input ja zwp_text_input_v2 liideste teostus
- [server] väljakutse ressursside topeltkustutamise vältimine SurfaceInterface'is
- [server] ressursi nullptr kontrolli lisamine ShellSurfaceInterface'is
- [server] ClientConnection'i, mitte wl_client'i võrdlemine SeatInterface'is
- [server] käitlemise täiustus, kui kliendid ühenduse katkestavad
- server/plasmawindowmanagement_interface.cpp - -Wreorder hoiatuse parandus
- [client] kontekstiviida lisamine PlasmaWindowModel'i ühendustele
- Palju hävitamisega seotud parandusi

### KWidgetsAddons

- Valitud ikooni efekti kasutamine KPageView aktiivsel leheküljel

### KWindowSystem

- [platform xcb] nõude ikooni suuruse arvestamine (veateade 362324)

### KXMLGUI

- Paremklõps rakenduse menüüribal ei võimalda enam möödahiilimist

### NetworkManagerQt

- "WiMAX toetusest loobumine NM 1.2.0+ korral" tagasivõtmine, sest see lõhub ära ABI

### Oxygeni ikoonid

- Ilmaikoonide sünkroonimine Breeze'iga
- Uuendamisikoonide lisamine

### Plasma raamistik

- Cantata süsteemisalve toetuse lisamine (veateade 363784)
- Valitud olek Plasma::Svg'le ja IconItem'ile
- DaysModel: m_agendaNeedsUpdate lähtestamine, kui plugin saadab uusi sündmusi
- Heli- ja võrguikooni uuendamine parema kontrasti tagamiseks (veateade 356082)
- downloadPath(const QString &amp;file) märkimine iganenuks ja downloadPath() eelistamine
- [ikooni pisipildid] eelistatud ikoonisuuruse nõue (veateade 362324)
- Plasmoidid suudavad nüüd öelda, kas vidinad on lukus kasutaja või süsteemiadministraatori seatud piirangu tõttu
- [ContainmentInterface] tühja QMenu'd ei püüta hüpikuna esitada
- Plasma::Svg laaditabeli asendusena SAX-i kasutamine
- [DialogShadows] Puhvri juurdepääs QX11Info::display()-s
- KDE3 air plasma teema ikoonide taastamine
- Valitud värviskeemi taaslaadimine värvide muutmisel

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
