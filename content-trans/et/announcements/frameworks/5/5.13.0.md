---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Uued raamistikud

- KFileMetadata: failide metaandmete ja lahtipakkimise teek
- Baloo: failide indekseerimise ja otsimise raamistik

### Kõiki raamistikke puudutavad muudatused

- Qt versiooni nõue kerkis 5.2 pealt 5.3 peale
- Silumisväljund porditi kategoriseeritud väljundi peale, mis tekitab vaikimisi vähem müra
- Docbook-dokumentatsiooni revideeriti ja uuendati

### Raamistike lõimimine

- Parandati krahh ainult katalooge näitavas failidialoogis
- Qt &lt; 5.4 puhul ei toetuta options()-&gt;initialDirectory() peale

### KDE Doxygeni tööriistad

- kapidox'i skriptide manuaalilehekülgede lisamine ja hooldajate teabe uuendamine failis setup.py

### KBookmarks

- KBookmarkManager: KDirWatch'i kasutamine QFileSystemWatcher'i asemel user-places.xbel loomise tuvastamiseks.

### KCompletion

- HiDPI parandused KLineEdit'is/KComboBox'is
- KLineEDit: kasutajal ei lubata teksti kustutada, kui see on kirjutuskaitstud

### KConfig

- Iganenud API kasutamist ei soovitata
- Iganenud koodi ei genereerita

### KCoreAddons

- Kdelibs4Migration::kdeHome() lisamine juhtudeks, mida ressursid ei kata
- tr() hoiatuse parandamine
- KCoreAddonsi ehitamise parandamine Clang+ARM peal

### KDBusAddons

- KDBusService: aktiivse akna esiletõstmise dokumenteerimine Activate()-s

### KDeclarative

- Iganenud KRun::run väljakutse parandamine
- MouseArea samasugune käitumine filtreeritud alamsündmuste koordinaatide seondamisel
- Algse näoikooni loomise tuvastamine
- Joonise renderdamisel ei värskendata kogu akent (veateade 348385)
- userPaths'i konteksti omaduse lisamine
- Tühi QIconItem ei tekita enam hangumist

### KDELibs 4 toetus

- kconfig_compiler_kf5 liigutati libexec'isse, kasuta findExe testiks selle asemel readconfig5
- KApplication::disableSessionManagement'i (väheoptimeeritud) asenduste dokumenteerimine

### KDocTools

- vigadest teatamise lause muutmine dfaure heakskiidul
- saksa user.entities kohandamine en/user.entities järgi
- general.entities uuendamine: raamistike tähistamise muutminr + plasma rakenduse asemel tootenimeks
- en/user.entities uuendamine
- raamatu- ja manuaalilehekülgede mallide uuendamine
- CMAKE_MODULE_PATH'i kasutamine cmake_install.cmake'is
- VEATEADE: 350799 (veateade 350799)
- general.entities uuendamine
- Nõutavate Perli moodulite otsimine.
- Nimeruumi abimakro paigaldatud makrode failis.
- Võtmenimede tõlgete kohandamine Termcati pakutud standardtõlgetega

### KEmoticons

- Breeze'i teema paigaldamine
- Kemoticos: Breeze'i emotikonide muutmine standardiks Glassi asemel
- Breeze'i emotikonide pakk Uri Herreralt

### KHTML

- Khtml'i kasutamise võimaldamine ka ilma privaatsõltuvusi otsimata

### KIconThemes

- Ajutiste stringiomistamiste eemaldamine
- Teemapuu silumiskirje eemaldamine

### KIdleTime

- Platvormipluginate privaatpäiste paigaldamine.

### KIO

- Ebavajalike QUrl-ümbriste hävitamine

### KItemModels

- Uus puhverserver KExtraColumnsProxyModel, võimaldab lisada olemasolevasse mudelisse veerge.

### KNotification

- Varuvariandina kasutatavate hüpikdialoogide algse Y-asukoha parandamine
- Sõltuvuste vähendamine ja liigutamine 2. kihti
- tundmatute märguandekirjete tabamine (nullptr deref) (veateade 348414)
- Üsna kasutu hoiatusteate eemaldamine

### Paketiraamistik

- subtiitrid, jah, subtiitrid ;)
- kpackagetool: mitteladina väljundteksti parandamine standardväljundis

### KPeople

- Lisati AllPhoneNumbersProperty
- PersonsSortFilterProxyModel'i saab nüüd kasutada QML-is

### Kross

- krosscore: kaamelkirjas päise "KrossConfig" paigaldamine
- PyQt5 peal töötavate Python2 testide parandamine

### KService

- kbuildsycoca --global parandamine
- KToolInvocation::invokeMailer: manuse parandamine mitme manuse olemasolul

### KTextEditor

- vaikimisi logitaseme kaitsmine Qt &lt; 5.4.0 korral, log cat nime parandamine
- Xonotici esiletõstmise lisamine (veateade 342265)
- Groovy esiletõstmise lisamine (veateade 329320)
- J esiletõstmise uuendamine (veateade 346386)
- Kompileerimine MSVC2015-ga
- vähem iconloader'i kasutamist, veel rohkemate pikseldatud ikoonide parandamine
- kõigi nuppude leidmise lubamine/keelamine mustri muutmisel
- Otsimise ja asendamise riba täiustamine
- tarbetu joonlaua eemaldamine
- õhem otsinguriba
- vi: markType01 lipu valesti lugemise parandamine
- Korrektse kvalifitseerimise kasutamine baasmeetodi väljakutsumisel
- Kontrollide eemaldamine, QMetaObject::invokeMethod kaitseb ennast juba selle eest.
- HiDPI probleemide parandamine värvivalijates
- Puhastamine: QMetaObject::invokeMethod on nullptr suhtes turvaline.
- rohkem kommentaare
- viisi muutmine, kuidas liidesed on nulli suhtes turvalised
- vaikimisi ainult väljundi hoiatused ja tõsisemad
- varasemate ülesannete eemaldamine
- QVarLengthArray kasutamine ajutise QVector'i korduse salvestamiseks.
- Rühmapealdiste treppimise kiirparanduse liigutamine ehitamisaega.
- Mõne tõsise probleemi parandamine KateCompletionModeli'i puurežiimis.
- Katkise mudelikujunduse parandamine, mis toetus Qt 4 käitumisele.
- umask'i reeglite järgimine uue faili salvestamisel (veateade 343158)
- mesoni esiletõstmise lisamine
- As Varnish 4.x introduces various syntax changes compared to Varnish 3.x, I wrote additional, separate syntax highlighting files for Varnish 4 (varnish4.xml, varnishtest4.xml).
- HiDPI probleemide parandamine
- vimode: krahhi vältimine, kui käsk &lt;c-e&gt; täidetakse dokumendi lõpus. (veateade 350299)
- QML-i mitmerealiste stringide toetus.
- oors-xml süntaksi parandamine
- CartoSS esiletõstmise lisamine Lukas Sommeri poolt (veateade 340756)
- ujukoma esiletõstmise parandamine, kasutatakse kaasapandud Float'i nagu C-s (veateade 348843
- tükeldamissuunad olid valetpidi (veateade 348845)
- Bug 348317 - [PATCH] Katepart syntax highlighting should recognize u0123 style escapes for JavaScript (bug 348317)
- *.cljs lisamine (veateade 349844)
- GLSL esiletõstmisfaili uuendamine.
- vaikimisi värvide parandamine nende selgemaks eristamiseks

### KTextWidgets

- Vana esiletõstja kustutamine

### KWalleti raamistik

- Windowsi ehituse parandamine
- Hoiatuse näitamine veakoodiga, kui turvalaeka avamine PAM-iga nurjub
- Taustaprogrammi veakoodi tagastamine 1 asemel, kui turvalaeka avamine nurjub
- Taustaprogrammi "tundmatu šifri" muutmine negatiivseks tagastatavaks koodiks
- PAM_KWALLET5_LOGIN jälgimine KWallet5 korral
- Krahhi vältimine, kui MigrationAgent::isEmptyOldWallet() kontroll nurjub
- PAM võib nüüd mooduli kwallet-pam abil KWalleti lukustusest vabastada

### KWidgetsAddons

- Uus API QIconi parameetrite hankimiseks ikoonide määramisel kaardiribale
- KCharSelect: unicode kategooria parandamine ja boundingRect'i kasutamine laiuse arvutamiseks
- KCharSelect: lahtri laiuse parandamine sisu mahutamiseks
- KMultiTabBar'i veerised on nüüd HiDPI ekraanidel korras
- KRuler: rakendamata MigrationAgent::isEmptyOldWallet() iganenuks tunnistamine, kommentaaride puhastamine
- KEditListWidget: veerise eemaldamine, et see paremini joonduks teiste vidinatega

### KWindowSystem

- NETWM andmete lugemise täiustamine (veateade 350173)
- vanemate Qt versioonide kaitse näiteks kio-http's
- Platvormipluginate privaatpäiste paigaldamine.
- Platvormipõhiste komponentide laadimine pluginatena.

### KXMLGUI

- Meetodi KShortcutsEditorPrivate::importConfiguration parandamine

### Plasma raamistik

- Kokkutõmbeliigutusega saab nüüd kalendris lülituda suurendustasemete vahel
- topeltkoodi kommenteerimine icondialog'is
- Liuduri õnara värv oli jäigalt määratud - seda muudeti, et see kasutaks värviskeemi
- QBENCHMARK'i kasutamine jäiga nõude asemel masina jõudluse mõõtmisel
- Kalendris liikumist täiustati oluliselt, võimaldades aasta ja kümnendi ülevaadet
- PlasmaCore.Dialog'il on nüüd omadus 'opacity'
- Raadionupule ruumi tagamine
- Menüü olemasolul ringjat tausta ei näidata
- X-Plasma-NotificationAreaCategory definitsiooni lisamine
- Märguannete ja ekraaniesituse näitamine kõigil töölaudadel
- Kasuliku hoiatuse näitamine, kui korraliku KPluginIfo hankimine nurjus
- Võimaliku lõputu kordamise parandamine PlatformStatus::findLookAndFeelPackage()-s
- software-updates.svgz uueks nimeks sai software.svgz

### Sonnet

- CNake'i bittide lisamine Voikko plugina ehitamise võimaldamiseks.
- Voikko õigekirja kontrollijale Sonnet::Client kogumi lisamine
- Voikko-põhise õigekirja kontrollija lisamine (Sonnet::SpellerPlugin)

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
