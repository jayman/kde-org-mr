---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၄.၂ တင်ပို့ခဲ့သည်
layout: application
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၄.၂ တင်ပို့ခဲ့သည်
version: 18.04.2
---
June 7, 2018. Today KDE released the second stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 25 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular, among others.

တိုးတက်မှုများ -

- Image operations in Gwenview can now be redone after undoing them
- KGpg no longer fails to decrypt messages without a version header
- Exporting of Cantor worksheets to LaTeX has been fixed for Maxima matrices
