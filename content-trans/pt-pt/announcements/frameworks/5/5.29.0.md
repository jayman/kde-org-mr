---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nova plataforma

Esta versão inclui o Prison, uma nova plataforma para a geração de códigos de barras (inclusive códigos QR).

### Geral

Foi adicionado o FreeBSD ao metainfo.yaml em todas as plataformas testadas para funcionar no FreeBSD.

### Baloo

- Melhorias de performance na escrita (4 * rapidez na gravação de dados)

### Ícones do Brisa

- Uso do 'BINARY_ICONS_RESOURCE ON' por omissão
- Adição do tipo MIME 'vnd.rar' para o 'shared-mime-info' 1.7 (erro 372461)
- adição do ícone do Claws (erro 371914)
- Adição do ícone do 'gdrive' em vez de um ícone genérico de nuvem (erro 372111)
- Correcção do erro "list-remove-symbolic a usar uma imagem errada" (erro 372119)
- outras melhorias e adições

### Módulos Extra do CMake

- Salto do teste das interfaces em Python, caso o PyQt não esteja instalado
- Só adicionar o teste se o 'python' for encontrado
- Redução da versão mínima do CMake obrigatória
- Adição do módulo 'ecm_win_resolve_symlinks'

### Integração da Plataforma

- Pesquisa pelo QDBus, necessário pela rotina de tratamento do KPackage para o Appstream
- Possibilidade de o KPackage ter dependências do packagekit &amp; appstream

### KActivitiesStats

- Envio adequado do evento ligado ao recurso

### Ferramentas de Doxygen do KDE

- Adaptação à mudança do quickgit -&gt; cgit
- Correcção de um erro quando o nome do grupo não está definido. Pode à mesma funcionar mal em condições más

### KArchive

- Adição do método errorString() para oferecer informações de erros

### KAuth

- Adição da propriedade 'timeout' (tempo-limite) (erro 363200)

### KConfig

- kconfig_compiler - geração de código com substituições
- Processamento adequado das palavras-chave das funções (erro 371562)

### KConfigWidgets

- Garantia de que as acções do menu recebem o MenuRole pretendido

### KCoreAddons

- KTextToHtml: correcção do erro "[1] added at the end of a hyperlink" (adicionado ao fim de uma ligação) (erro 343275)
- KUser: Só pesquisar por um avatar se o 'loginName' não estiver vazio

### KCrash

- Alinhamento com o KInit e não usar o DISPLAY no Mac
- Não fechar todos os descritores de ficheiros no OS X

### KDesignerPlugin

- src/kgendesignerplugin.cpp - adição de substitutos para o código gerado

### KDESU

- Limpeza do XDG_RUNTIME_DIR nos processos executados com o 'kdesu'

### KFileMetaData

- Pesquisa eficaz da 'libpostproc' do FFMpeg

### KHTML

- java: aplicação dos nomes aos botões correctos
- java: mudança dos nomes na janela de permissões

### KI18n

- Verificação adequada da desigualdade dos ponteiros no 'dngettext' (erro 372681)

### KIconThemes

- Possibilidade de mostrar os ícones de todas as categorias (erro 216653)

### KInit

- Definição das variáveis de ambiente do KLaunchRequest ao iniciar um novo processo

### KIO

- Migração para um registo de dados por categorias
- Correcção da compilação com o SDK do WinXP
- Possibilidade do processamento de códigos de validação em maiúsculas na página 'Códigos de Validação' (erro 372518)
- Nunca limpar a última coluna (data) na janela de ficheiros (erro 312747)
- Importação e actualização dos DocBook's do 'kcontrol' para o código do 'kio' no 'master' do 'kde-runtime'
- [OS X] possibilidade de o lixo do KDE usar o lixo do OS X
- SlaveBase: adição de documentação sobre os ciclos de eventos, notificações e módulos do 'kded'

### KNewStuff

- Adição de uma nova opção de gestão de pacotes ('subdir') ao 'knsrc'
- Consumo dos novos 'signals' de erro (definição de erros das tarefas)
- Tratamento de comportamento estranho com ficheiros que desaparecem quando são acabados de criar
- Instalação eficaz dos ficheiros de inclusão de base, com capitalização CamelCase

### KNotification

- [KStatusNotifierItem] Gravação / reposição da posição da janela ao esconder / mostrar uma janela (erro 356523)
- [KNotification] Permitir a anotação de notificações com URL's

### Plataforma KPackage

- Continuar a instalar o 'metadata.desktop' (erro 372594)
- Carregamento manual dos meta-dados se for passada uma localização absoluta
- Correcção de uma falha potencial se o pacote não for compatível com o Appstream
- Possibilidade de o KPackage saber sobre o X-Plasma-RootPath
- Correcção da geração do ficheiro 'metadata.json'

### KPty

- Mais pesquisas por locais do 'utempter' (incluindo /usr/lib/utempter/)
- Adição da localização da biblioteca para o executável 'utempter' seja encontrado no Ubuntu 16.10

### KTextEditor

- Impedimento de avisos do Qt sobre modos de áreas de transferência não suportados no Mac
- Uso das definições de sintaxe do KF5::SyntaxHighlighting

### KTextWidgets

- Não substituir os ícones da janela com o resultado de uma pesquisa mal-sucedida

### KWayland

- [cliente] Correcção de remoção de referência nula no ConfinedPointer e no LockedPointer
- [cliente] Instalação do pointerconstraints.h
- [servidor] Correcção de regressão no SeatInterface::end/cancelPointerPinchGesture
- Implementação do protocolo PointerConstraints
- [servidor] Redução da sobrecarga do 'pointersForSurface'
- Devolução do SurfaceInterface::size no espaço global do compositor
- [ferramentas/gerador] Geração do enumerado XPTOInterfaceVersion do lado do servidor
- [ferramentas/gerador] Envolvência de todos os argumentos de pedidos 'wl_fixed' no 'wl_fixed_from_double'
- [ferramentas/gerador] Geração da implementação dos pedidos do lado do cliente
- [ferramentas/gerador] Geração das fábricas de recursos do lado do cliente
- [ferramentas/gerador] Geração de chamadas de retorno e atendimento do lado do cliente
- [ferramentas/gerador] Passagem do 'this' como ponteiro 'q' no Client::Resource::Private
- [ferramentas/gerador] Geração do Private::setup(wl_xpto *arg) do lado do cliente
- Implementação do protocolo PointerGestures

### KWidgetsAddons

- Remoção de estoiro no Mac
- Não substituir os ícones com o resultado de uma pesquisa mal-sucedida
- KMessageWidget: correcção do formato quando o 'wordWrap' está activo sem acções
- KCollapsibleGroupBox: não esconder os itens, substituindo sim a política de foco

### KWindowSystem

- [KWindowInfo] Adição do pid() e do desktopFileName()

### Ícones do Oxygen

- Adição do ícone 'application-vnd.rar' (erro 372461)

### Plataforma do Plasma

- Verificação da validade dos meta-dados no 'settingsFileChanged' (erro 372651)
- Não inverter o formato da barra de páginas se estiver na vertical
- Remoção do 'radialGradient4857' (erro 372383)
- [AppletInterface] Nunca retirar o foco do 'fullRepresentation' (erro 372476)
- Correcção do prefixo do ID dos ícones SVG (erro 369622)

### Solid

- winutils_p.h: Reposição da compatibilidade com o SDK do WinXP

### Sonnet

- Pesquisa também pelo hunspell-1.5

### Realce de Sintaxe

- Normalização dos valores dos atributos da licença no XML
- Sincronização das definições de sintaxe do KTextEditor
- Correcção da junção de regiões dobradas

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
