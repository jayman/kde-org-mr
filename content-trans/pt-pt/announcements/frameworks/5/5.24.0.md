---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Alterações gerais

- A lista de plataformas suportadas por cada plataforma é agora mais explícita ainda. O Android foi adicionado à lista de plataformas suportadas em todas as plataformas em que esse seja o caso.

### Baloo

- DocumentUrlDB::del Só dar um erro quando a filha da pasta já existir
- Ignorar pesquisas inválidas que tenham um operador binário sem primeiro argumento

### Ícones do Brisa

- Muitos ícones novos e actualizados
- correcção do erro 364931, onde o ícone de inactividade do utilizador não estava visível (erro 364931)
- Adição de um programa para converter os ficheiros ligados simbolicamente a itens alternativos do 'qrc'

### Módulos Extra do CMake

- Integração dos locais relativos de bibliotecas no APK
- Use o "${BIN_INSTALL_DIR}/data" para o DATAROOTDIR no Windows

### KArchive

- Garantia de que a extracção de um pacote não instala os ficheiros fora da pasta de extracção, por motivos de segurança. Em vez disso, extrai esses ficheiros para o topo da pasta de extracção.

### KBookmarks

- Limpeza do KBookmarkManagerList antes de o qApp sair, para evitar bloqueios com a tarefa do DBus

### KConfig

- Descontinuação do authorizeKAction() em detrimento de authorizeAction()
- Correcção da capacidade de reprodução das compilações, garantindo a codificação em UTF-8

### KConfigWidgets

- KStandardAction::showStatusbar: Devolução da acção pretendida

### KDeclarative

- Tornar o 'epoxy' opcional

### KDED

- [OS X] passagem do 'kded5' para um agente, compilando-o como uma aplicação normal

### Suporte para a KDELibs 4

- Remoção da classe KDETranslator, dado que não existe mais o 'kdeqt.po'
- Documentação da substituição de use12Clock()

### KDesignerPlugin

- Adição do suporte para o KNewPasswordWidget

### KDocTools

- Possibilidade de o KDocTools localizar sempre pelo menos os seus próprios itens instalados
- Uso do CMAKE_INSTALL_DATAROOTDIR para procurar pelos ficheiros Docbook em vez da 'share'
- Actualização do Docbook da página de manual do 'qt5options' para o Qt 5.4
- Actualização do 'docbook' da página de manual do 'kf5options'

### KEmoticons

- Passagem do tema Glass para o 'kde-look'

### KGlobalAccel

- Uso da QGuiApplication em vez da QApplication

### KHTML

- Correcção da aplicação do valor 'inherit' para a propriedade curta 'outline'
- Tratamento dos valores 'initial' e 'inherit' para o raio do contorno
- Ignorar a propriedade, caso seja usada um tamanho/percentagem invaĺida como 'background-size'
- O cssText deverá devolver valores separados por vírgulas para essas propriedades
- Correcção do processamento do 'background-clip' na declaração curta
- Implementação do processamento do 'background-size' na declaração curta
- Marcação das propriedades como definidas ao repetir os padrões
- Correcção da herança das propriedades do fundo
- Correcção da aplicação dos valores 'initial' e 'inherit' da propriedade 'background-size'
- Instalação do ficheiro 'kxmlgui' do khtml num ficheiro de recurso do Qt

### KI18n

- Também pesquisar nos catálogos por versões limpas dos valores na variável de ambiente LANGUAGE
- Correcção do processamento dos valores das variáveis de ambiente WRT 'modifier' e 'codeset', feitos pela ordem errada

### KIconThemes

- Adição do suporte para o carregamento e uso de um tema de ícones automaticamente num ficheiro RCC
- Instalação do tema de ícones de documentos no MacOS e Windows, ver em https://api.kde.org/frameworks/kiconthemes/html/index.html

### KInit

- Permitir a definição de tempo-limite no 'reset_oom_protection', enquanto se espera pelo SIGUSR1

### KIO

- KIO: adição SlaveBase::openPasswordDialogV2 para uma melhor verificação de erros; convém migrar os 'kioslaves' para esta API
- Correcção da janela de abertura de ficheiros do KUrlRequester na pasta errada (erro 364719)
- Correcção de conversões inseguras do KDirModelDirNode*
- Adição de opção do CMake KIO_FORK_SLAVES para definir o valor por omissão
- Filtro do ShortUri: correcção da filtragem de mailto:utilizador@máquina
- Adição do OpenFileManagerWindowJob para realçar um ficheiro dentro de uma pasta
- KRun: adição do método 'runApplication'
- Adição do fornecedor de pesquisa na Soundcloud
- Correcção de um problema de alinhamento com o estilo "macintosh" nativo do OS X

### KItemModels

- Adição do KExtraColumnsProxyModel::removeExtraColumn, que será necessário pelo StatisticsProxyModel

### KJS

- kjs/ConfigureChecks.cmake - definição adequada do HAVE_SYS_PARAM_H

### KNewStuff

- Certificação de que existe um tamanho oferecido (erro 364896)
- Correcção de "A janela de transferência falha quando faltam todas as categorias"

### KNotification

- Correcção da notificação pela barra de tarefas

### KNotifyConfig

- KNotifyConfigWidget: adição do método disableAllSounds() (erro 157272)

### KParts

- Adição de uma opção para desactivar o tratamento dos títulos das janelas pelos KParts
- Adição de um item de menu de doação ao menu de ajuda das aplicações

### Kross

- Correcção do nome do enumerador da QDialogButtonBox "StandardButtons"
- Remoção da primeira tentativa de carregamento da biblioteca, porque será testada à mesma o valor de 'libraryPaths'
- Correcção de um estoiro quando um método exposto no Kross devolve um QVariant com dados não-móveis
- Não usar as conversões à C para 'void*' (erro 325055)

### KRunner

- [QueryMatch] Adição do 'iconName'

### KTextEditor

- Mostrar a Antevisão de Texto da Scrollbar ao fim de 250ms
- esconder a antevisão e outros itens ao deslocar o conteúdo da janela
- definição do pai + área-ferramentas - parece ser necessário para evitar um item no gestor de tarefas no Win10
- Remoção do "KDE-Standard" na lista de codificações
- Dobragem da antevisão activada por omissão
- Evitar um sublinhado tracejado na antevisão &amp; evitar a poluição da 'cache' de disposições de linhas
- Activar sempre a opção "Mostrar uma antevisão do texto dobrado"
- TextPreview: Ajuste do 'grooveRect-height' quando a opção 'scrollPastEnd' estiver activada
- Antevisão da barra de posicionamento: usar um rectângulo de relevo se a barra de posicionamento não usar toda a altura
- Adição do KTE::MovingRange::numberOfLines() tal como acontece no KTE::Range
- Antevisão da dobragem de código: definição a altura da janela para que caibam todas as linhas escondidas
- Adição de opção para desactivar a antevisão do texto dobrado
- Adição da linha de modo 'folding-preview' com um tipo booleano
- ConfigInterface do objecto View: suporte de 'folding-preview' com o tipo booleano
- Adição de propriedade booleana KateViewConfig::foldingPreview() e setFoldingPreview(bool)
- Funcionalidade: Mostrar uma antevisão de texto ao passar sobre um bloco de código dobrado
- KateTextPreview: adição de setShowFoldedLines() e showFoldedLines()
- Adição das linhas de modo 'scrollbar-minimap' [bool] e 'scrollbar-preview' [bool]
- Adição de um mini-mapa da barra de posicionamento por omissão
- Nova funcionalidade: Mostrar uma antevisão do texto ao passar o cursor sobre a barra de posicionamento
- KateUndoGroup::editEnd(): passagem do KTE::Range por referência constante
- Correcção do tratamento do atalho do modo VIM, após algumas alterações de comportamento no Qt 5.5 (erro 353332)
- Parêntesis automáticos: Não inserir um carácter ' no texto
- ConfigInterface: adição da opção de configuração 'scrollbar-minimap' para activar/desactivar o mini-mapa da barra de posicionamento
- Correcção do KTE::View::cursorToCoordinate() quando o elemento da mensagem de topo está visível
- Remodelação da Barra de Comandos Emulada
- Correcção dos artefactos de desenho no deslocamento enquanto as notificações estão visíveis (erro 363220)

### KWayland

- Adição de um evento 'parent_window' à interface Window do Plasma
- Tratamento adequado da destruição de um recurso Pointer/Keyboard/Touch
- [servidor] Remoção de código antigo: KeyboardInterface::Private::sendKeymap
- [servidor] Adição do suporte para a definição da selecção da DataDeviceInterface da área de transferência manualmente
- [servidor] Garantia de que o Resource::Private::get devolve um ponteiro nulo se lhe for passado também um ponteiro nulo
- [servidor] Adição de verificação de recursos no QtExtendedSurfaceInterface::close
- [servidor] Limpeza do ponteiro SurfaceInterface nos objectos referenciados quando for destruído
- [servidor] Correcção de mensagem de erro na interface QtSurfaceExtension
- [servidor] Introdução de um sinal Resource::unbound emitido a partir da rotina de tratamento do 'unbind'
- [servidor] Não emitir um erro ao destruir uma BufferInterface ainda referenciada
- Adição de um pedido de destruição ao 'org_kde_kwin_shadow' e ao 'org_kde_kwin_shadow_manager'

### KWidgetsAddons

- Correcção da leitura dos dados Unihan
- Correcção do tamanho mínimo do KNewPasswordDialog (erro 342523)
- Correcção de construtor ambíguo no MSVC 2015
- Correcção de um problema de alinhamento com o estilo "macintosh" nativo do OS X (erro 296810)

### KXMLGUI

- KXMLGui: Correcção da junção de índices ao remover clientes do 'xmlgui' com acções nos grupos (erro 64754)
- Não avisar acerca do "ficheiro encontrado na localização de compatibilidade", caso não seja encontrado de todo
- Adição de um item de menu de doação ao menu de ajuda das aplicações

### NetworkManagerQt

- Não usar a legenda do PEAP com base na versão do PEAP
- Passagem das verificações da versão do NetworkManager para a execução (para evitar problemas de compilação vs. execução - erro 362736)

### Plataforma do Plasma

- [Calendário] Inverter os botões de setas nas línguas da direita-para-esquerda
- O Plasma::Service::operationDescription() deverá devolver um QVariantMap
- Não incluir contentores incorporados no containmentAt(pos) (erro 361777)
- correcção dos temas de cores para o ícone de reinício do sistema (ecrã de autenticação) (erro 364454)
- desactivação das miniaturas da barra de tarefas com o 'llvmpipe' (erro 363371)
- guarda contra 'applets' inválidas (erro 364281)
- PluginLoader::loadApplet: reposição da compatibilidade para as 'applets' mal instaladas
- correcção da pasta de PLASMA_PLASMOIDS_PLUGINDIR
- PluginLoader: melhoria da mensagem de erro sobre a compatibilidade da versão do 'plugin'
- Correcção de verificação para manter o QMenu no ecrã para os formatos multi-ecrã
- Novo tipo de contentor para a bandeja do sistema

### Solid

- Correcção da verificação se o CPU é válido
- Tratamento da leitura do /proc/cpuinfo para os processadores ARM
- Pesquisa dos CPU's por sub-sistema em vez de controlador

### Sonnet

- Tornar o auxiliar do 'exe' como aplicação não gráfica
- Permitir que o 'nsspellcheck' seja compilado no Mac por omissão

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
