---
aliases:
- ../announce-applications-15.04-beta2
date: '2015-03-12'
description: O KDE Lança as Aplicações do KDE 15.04 Beta 2.
layout: applications
title: O KDE Lança a Segunda Versão 15.04 Beta das Aplicações
---
12 de Março de 2015. Hoje o KDE lançou a segunda das versões beta das novas Aplicações do KDE. Com as dependências e as funcionalidades estabilizadas, o foco da equipa do KDE é agora a correcção de erros e mais algumas rectificações.

Com as diversas aplicações a basearem-se nas Plataformas do KDE 5, as versões 15.04 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa do 4.12, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
