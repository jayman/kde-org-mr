---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: O KDE Lança as Aplicações do KDE 15.12.2
layout: application
title: O KDE Lança as Aplicações do KDE 15.12.2
version: 15.12.2
---
16 de Fevereiro de 2016. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../15.12.0'>Aplicações do KDE 15.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 30 correcções de erros registadas incluem as melhorias nos módulos 'kdelibs' e 'kdepim' e nas aplicações 'kdenlive', 'marble', 'konsole', 'spectacle', 'akonadi', 'ark' e 'umbrello', entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.17.
