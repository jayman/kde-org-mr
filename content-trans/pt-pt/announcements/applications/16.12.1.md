---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: O KDE Lança as Aplicações do KDE 16.12.1
layout: application
title: O KDE Lança as Aplicações do KDE 16.12.1
version: 16.12.1
---
12 de Janeiro de 2017. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../16.12.0'>Aplicações do KDE 16.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Esta versão resolver um erro de PERDA DE DADOS no recurso de iCal, que o impedia de criar o 'std.ics', caso não existisse.

As mais de 40 correcções de erros registadas incluem as melhorias nos módulos 'kdepim', no 'ark', no 'gwenview', no 'kajongg', no 'okular', no 'kate', no 'kdenlive', entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.28.
