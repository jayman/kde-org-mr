---
aliases:
- ../../kde-frameworks-5.47.0
date: 2018-06-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Uitvoeren van zoekopdracht vroeg beëindigen als subterm een lege resultaatset teruggeeft
- Crash vermijden bij lezen van corrupte gegevens uit documenttermen-db (bug 392877)
- tekenreekslijsten als invoer behandelen
- Meer typen broncodebestanden negeren (bug 382117)

### Breeze pictogrammen

- bijgewerkte handvatten en overflow-menu

### Extra CMake-modules

- Android toolchain: toestaan om extra bibliotheken handmatig te specificeren
- Android: geen qml-import-paden definiëren als het leeg is

### KArchive

- zip-bestanden ingebed binnen zip-bestanden behandelen (bug 73821)

### KCMUtils

- [KCModuleQml] uitgeschakelde besturing negeren bij springen met tab

### KConfig

- kcfg.xsd - geen kcfgfile-element vereisen

### KConfigWidgets

- Het "standaard" kleurenschema repareren om opnieuw overeen te komen met Breeze

### KDeclarative

- De kcm-contexteigenschap instellen op de juiste context
- [Plotter] niet renderen als m_node nul is (bug 394283)

### KDocTools

- De lijst met entities van Oekraïne bijwerken
- Entity OSD toevoegen aan general.entites
- Entities CIFS, NFS, Samba, SMB toevoegen aan general.entities
- Falkon, Kirigami, macOS, Solid, USB, Wayland, X11, SDDM toevoegen aan de algemene entities

### KFileMetaData

- controleer dat ffmpeg op de laatste version 3.1 is die de API introduceert die we vereisen
- zoeken naar albumartiest end albumartiesttags in taglibextractor
- popplerextractor: probeer geen titel te raden als er geen is

### KGlobalAccel

- Ga na dat 'ungrab keyboard request' is verwerkt alvorens sneltoets uit te sturen (bug 394689)

### KHolidays

- holiday_es_es - dag van de "Comunidad de Madrid" repareren

### KIconThemes

- Controleer of groep &lt; LastGroup, omdat KIconEffect in elke geval geen UserGroup behandelt

### KImageFormats

- Dubbele MIME-types verwijderen uit json-bestanden

### KIO

- Controleer of bestemming bestaat ook bij plakken van binaire gegevens (bug 394318)
- Auth ondersteuning: de actuele lengte van socketbuffer teruggeven
- Auth ondersteuning: API meer algemeen maken voor delen van bestandsbeschrijving
- Auth ondersteuning: socketbestand aanmaken in runtime-map van gebruiker
- Auth ondersteuning: socket-bestand verwijderen na gebruik
- Auth ondersteuning: taak voor opschonen van socket-bestand verplaatsen naar FdReceiver
- Auth ondersteuning: geen abstracte socket gebruiken in linux om bestandsbeschrijving te delen 
- [kcoredirlister] zo veel als mogelijke url.toString() verwijderen
- KFileItemActions: naar standaard mimetype terugvallen bij alleen selecteren van bestanden (bug 393710)
- KFileItemListProperties::isFile() introduceren
- KPropertiesDialogPlugin kan nu meerdere ondersteunde protocollen specificeren met gebruik van X-KDE-Protocols
- Fragment bewaren bij omleiden van http naar https
- [KUrlNavigator] tabRequested uitsturen wanneer pad in padselectormenu een middenklik krijgt
- Prestatie: de nieuwe implementatie van uds gebruiken
- Omleiden van smb:/ naar smb:// en daarna naar smb:/// niet uitvoeren
- Accepteren met dubbelklik bij dialoog opslaan toestaan (bug 267749)
- Voorbeeld tonen standaard inschakelen in de dialoog van bestand kiezen
- Voorbeeld van bestand verbergen wanneer pictogram te klein is
- i18n: meervoudsvorm opnieuw gebruiken voor plug-in bericht
- Een reguliere dialoog gebruiken in plaats van een dialoog met lijst bij verplaatsen naar prullenbak of verwijderen van een enkel bestand
- Maak de waarschuwingstekst voor verwijderbewerkingen om nadrukkelijker het permanente karakter en onomkeerbaarheid aan te duiden
- Draai terug "Knoppen voor modus weergave in de werkbalk van de dialoog openen/opslaan"

### Kirigami

- action.main meer prominent tonen op de ToolBarApplicationHeader
- Toestaan dat Kirigami gebouwd wordt zonder afhankelijkheid van tabletmodus van KWin
- corrigeer swipefilter bij RnL
- corrigeer grootte wijzigen van contentItem
- --reverse gedrag repareren 
- contextobject delen naar altijd toegang tot i18n toestaan
- zorg ervoor dat tekstballon verborgen is
- ga na om geen ongeldige varianten toe te kennen aan de gevolgde eigenschappen
- geen MouseArea, dropped() signaal behandelen
- geen hover-effect op mobiel
- juiste pictogram overflow-menu-left en right
- Handvat verslepen om items opnieuw te ordenen in een lijstweergave
- Mnemonics op de werkbalkknoppen gebruiken
- Ontbrekende bestanden toegevoegd in QMake's .pri
- [API dox] Kirigami.InlineMessageType -&gt; Kirigami.MessageType repareren
- applicatieheaders in applicatie-item repareren
- Niet toestaan tonen/verbergen van de la wanneer er geen handvat is (bug 393776)

### KItemModels

- KConcatenateRowsProxyModel: invoer op de juiste manier opschonen

### KNotification

- Crashes repareren in NotifyByAudio bij sluiten van toepassingen

### KPackage-framework

- kpackage*install**package: ontbrekende dep tussen .desktop en .json repareren
- verzeker dat paden in rcc nooit afgeleid zijn uit absolute paden

### KRunner

- Proces DBus antwoord in de ::match thread (bug 394272)

### KTextEditor

- Hoofd- kleine letters van titel voor het keuzevak "aantal woorden tonen" niet gebruiken
- Maak van aantal woorden/tekens een globale voorkeur

### KWayland

- Versie van org_kde_plasma_shell-interface verhogen
- "SkipSwitcher" toevoegen aan API
- XDG-uitvoerprotocol toevoegen

### KWidgetsAddons

- [KCharSelect] tabelcelgrootte repareren met Qt 5.11
- [API dox] gebruik van overload verwijderen, resulteert in gebroken docs
- [API dox] doxygen vertellen "e.g." beëindigt niet de zin, gebruik ". "
- [API dox] onnodige escaping in HTML verwijderen
- Niet automatisch de standaard pictogrammen voor elke stijl instellen
- Laat KMessageWidget kloppen met de stijl van Kirigami inlineMessage (bug 381255)

### NetworkManagerQt

- Informatie over onbehandelde eigenschap juist maken in debugbericht
- WirelessSetting: assignedMacAddress eigenschap implementeren

### Plasma Framework

- Slablonen: consistente naamgeving, cataloggusnamen van vertalingen &amp; meer repareren
- [Breeze Plasma Theme] kleopatra-pictogram repareren om kleur van stijlsheet te gebruiken (bug 394400)
- [Dialog] geminimaliseerde dialoog netjes behandelen (bug 381242)

### Omschrijving

- Telegram-integratie verbeteren
- Inwendige arrays als OF beperkingen behandelen in plaats van als EN
- Het mogelijk maken om plug-ins in te perken door aanwezigheid van een bureaubladbestand 
- Het mogelijk maken plug-ins te filteren per uitvoerbaar bestand
- Het geselecteerde apparaat accentueren in de plug-in KDE Connect
- i18n-problemen in frameworks/purpose/plug-ins repareren
- Telegram-plug-in toevoegen
- kdeconnect: melden wanneer het proces niet kan starten (bug 389765)

### QQC2StyleBridge

- Eigenschap van pallet alleen gebruiken bij gebruik van qtquickcontrols 2.4
- Werken met Qt&lt;5.10
- Hoogte van tabbladbalk repareren
- Control.palette gebruiken
- [RadioButton] "control" hernoemen tot "controlRoot"
- Geen expliciete spatiëring op RadioButton/CheckBox instellen
- [FocusRect] handmatige plaatsing gebruiken in plaats van ankers
- Er blijkt dat de flickable in een scrollview het contentItem is
- Focus rect tonen wanneer CheckBox of RadioButton zijn gefocust
- hackachtige reparatie aan detectie van scrollview
- reparent niet naar de flickable naar het muisgebied
- [TabBar] schakel tabbladen om met het muiswiel
- Besturing moet geen children hebben (bug 394134)
- Scroll beperken (bug 393992)

### Accentuering van syntaxis

- Perl6: ondersteuning toevoegen voor extensies .pl6, .p6 of .pm6 (bug 392468)
- DoxygenLua: sluiten van commentaarblokken repareren (bug 394184)
- pgf toevoegen aan de latex-achtige bestandsformaten (zelfde formaat als tikz)
- Postgresql sleutelwoorden toevoegen
- Accentuering voor OpenSCAD
- debchangelog: Cosmic Cuttlefish toevoegen
- cmake: waarschuwing van DetectChar over escaped backslash
- Pony: identifier en sleutelwoord repareren
- Lua: bijgewerkt voor Lua5.3

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
