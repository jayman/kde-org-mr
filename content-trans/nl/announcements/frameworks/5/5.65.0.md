---
aliases:
- ../../kde-frameworks-5.65.0
date: 2019-12-14
layout: framework
libCount: 70
qtversion: 5.12
---
Nieuwe module: KQuickCharts -- een QtQuick module die hoge-prestatiegrafieken levert.

### Breeze pictogrammen

- Kleurenkiezer op pixel uitlijnen
- Nieuwe pictogrammen voor baloo
- Nieuwe voorkeuren voor zoekpictogrammen toevoegen
- Een oogdruppelaar gebruiken voor kleurenkiezerpictogrammen (bug 403924)
- Pictogram voor categorie "Alle toepassingen" toevoegen

### Extra CMake-modules

- EBN extra-cmake-modules voor opschonen van overdracht
- ECMGenerateExportHeader: NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE vlag toevoegen
- lib expliciet gebruiken voor systemd-mappen
- Installatie-map voor systemd-units toevoegen
- KDEFrameworkCompilerSettings: all Qt &amp; KF afkeurwaarschuwingen inschakelen

### Frameworkintegratie

- SH_ScrollBar_LeftClickAbsolutePosition voorwaardelijk instellen gebaseerd op instellingen van kdeglobals (bug 379498)
- Toepassingnaam instellen en versie in het hulpmiddel knshandler

### KDE Doxygen hulpmiddelen

- Module importeren met Python3 repareren

### KAuth

- .pri bestand installeren voor KAuthCore

### KBookmarks

- KonqBookmarkMenu en KonqBookmarkContextMenu afkeuren
- Klassen alleen gebruikt door KonqBookmarkMenu verplaatsen samen hiermee

### KCalendarCore

- Naar systeemtijdzone terugvallen bij aanmaken van agenda met een ongeldige
- Geheugenagenda: duplicatie van code vermijden
- QDate als sleutel in mIncidencesForDate in MemoryCalendar gebruiken
- Incidenten in verschillende tijdzones in MemoryCalendar behandelen

### KCMUtils

- [KCMultiDialog] De meeste behandelingen van speciale marges verwijderen; het wordt nu gedaan in KPageDialog
- KPluginSelector: nieuwe KAboutPluginDialog gebruiken
- Bewaking voor ontbrekende kirigami toevoegen (bug 405023)
- De knop standaard herstellen uitschakelen als de KCModule dat zo aangeeft
- KCModuleProxy laten zorgen voor de standaard status
- KCModuleQml zich laten conformeren aan het defaulted() signaal

### KCompletion

- KHistoryComboBox::insertItems opnieuw programmeren

### KConfig

- Instelling voor documentmelders
- Een sessieconfiguratie alleen aanmaken bij echt herstellen van een sessie
- kwriteconfig: optie verwijderen toevoegen
- KPropertySkeletonItem toevoegen
- KConfigSkeletonItem voorbeelden om erven van zijn private klasse toe te staan

### KConfigWidgets

- [KColorScheme] volgorde van decoratiekleuren overeen laten komen met DecorationRole enumeratie
- [KColorScheme] vergissing in NShadeRoles-commentaar repareren
- [KColorScheme/KStatefulBrush] hard gecodeerde getallen voor enumeratie items omschakelen
- [KColorScheme] items aan ColorSet en Role enumeraties voor het totale aantal items toevoegen
- KKeySequenceWidget aan KConfigDialogManager registreren
- KCModule aanpassen om ook informatie over standaarden te kanaliseren

### KCoreAddons

- KAboutData::fromPluginMetaData afkeuren, er is nu KAboutPluginDialog
- Een beschrijvende waarschuwing toevoegen wanneer inotify_add_watch ENOSPC teruggeeft (bug 387663)
- Test voor bug "bug-414360" toevoegen het is geen ktexttohtml bug (bug 414360)

### KDBusAddons

- API invoegen om generiek --replace argumenten te implementeren

### KDeclarative

- EBN kdeclarative transfer protocol opschoning
- Wijziging in KConfigCompiler aanpassen
- header en footer zichtbaar maken wanneer ze inhoud krijgen
- qqmlfileselectors ondersteunen
- Automatisch opslaangedrag toestaan in ConfigPropertyMap

### KDED

- Afhankelijkheid van kdeinit verwijderen uit kded

### Ondersteuning van KDELibs 4

- ongebruikte kgesturemap uit kaction verwijderen

### KDocTools

- Catalan Works: ontbrekende entities toevoegen

### KI18n

- Maak verouderd ongedaan van I18N_NOOP2

### KIconThemes

- Top-niveau UserIcon-methode afkeuren, wordt niet langer gebruikt

### KIO

- Nieuw protocol voor 7z-archieven toevoegen
- [CopyJob] Bij koppeling maken beschouw ook https voor pictogram text-html
- [KFileWidget] aanropen van slotOk direct na de gewijzigde url vermijden (bug 412737)
- [kfilewidget] pictogrammen met naam laden
- KRun: app met voorkeur van de gebruiker niet overschrijven bij openen van lokale *.*html en co.-bestanden (bug 399020)
- FTP/HTTP proxy querying repareren voor het geval van geen proxy
- Ftp ioslave: ProxyUrls doorgeven van parameter repareren
- [KPropertiesDialog] een manier bieden om het doel van een symbolische koppeling te tonen (bug 413002)
- [Remote ioslave] Displaynaam toevoegen aan remote:/ (bug 414345)
- HTTP proxy instellingen repareren (bug 414346)
- [KDirOperator] sneltoets backspace toevoegen aan actie terug
- Wanneer kioslave5 niet gevonden kan worden in libexec-achtige locaties probeer $PATH
- [Samba] waarschuwingsbericht over netbiosnaam verbeteren
- [DeleteJob] een separate werkerthread gebruiken om actuele IO-operatie uit te voeren (bug 390748)
- [KPropertiesDialog] creatiedatumtekenreeks met de muis te selecteren maken (bug 413902)

Afkeuringen:

- Afgekeurde KTcpSocket en KSsl* klassen
- De laatst sporen van KSslError uit TCPSlaveBase verwijderen
- Metagegevens van ssl_cert_errors overzetten van KSslError naar QSslError
- Overladen KTcpSocket van KSslErrorUiData ctor afkeuren
- [http kio slave] QSslSocket gebruiken in plaats van KTcpSocket (afgekeurd)
- [TcpSlaveBase] uit KTcpSocket (afgekeurd) overgezet naar QSslSocket

### Kirigami

- Marges van ToolBarHeader repareren
- Niet crashen wanneer bron van pictogram leeg is
- MenuIcon: waarschuwingen repareren wanneer de opvangbak niet is geïnitialiseerd
- Rekening houden met een mnemonic label om naar terug te gaan ""
- InlineMessage acties repareren om ze altijd te plaatsen in het overloopmenu
- Standaard achtergrond van kaart repareren (bug 414329)
- Pictogram: probleem met threading oplossen op wanneer de bron http is
- navigatie met het toetsenbord reparaties
- i18n: berichten ook uit C++ broncode extraheren
- Positie van commando in cmake-project repareren
- Er voor zorgen dat QmlComponentsPool één exemplaar per engine is (bug 414003)
- ToolBarPageHeader omschakelen om het invouwgedrag van pictogram uit ActionToolBar te gebruiken
- ActionToolBar: Automatisch naar pictogram-alleen te wijzigen voor acties gemarkeerd KeepVisible
- Een displayHint-eigenschap toevoegen aan Actie
- de dbus-interface toevoegen in de statische versie
- "rekening houden met snelheid van slepen wanneer een flick eindigt" terugdraaien
- FormLayout: labelhoogte repareren als bredemodus onwaar is
- het handvat niet standaard tonen bij niet modaal
- "Verzekeren dat GlobalDrawer topContent altijd bovenaan blijft" terugdraaien
- Een RowLayout gebruiken voor indelen van ToolBarPageHeader
- Verticaal centreren van achtergelaten acties in ActionTextField (bug 413769)
- herstel dynamische bewaking van tabletmodus
- SwipeListItem vervangen
- actionsVisible-eigenschap ondersteunen
- SwipeListItem overzetten naar SwipeDelegate starten

### KItemModels

- KRecursiveFilterProxyModel afkeuren
- KNumberModel: netjes een stepSize van 0 behandelen
- KNumberModel naar QML blootstellen
- Nieuwe klasse KNumberModel toevoegen die een model is van getallen tussen twee waarden
- KDescendantsProxyModel blootstellen aan QML
- qml importeren toevoegen voor KItemModels

### KNewStuff

- Enige vriendelijke koppelingen "bugs hier rapporteren" toevoegen
- i18n-syntaxis repareren om fouten bij uitvoeren te vermijden (bug 414498)
- KNewStuffQuick::CommentsModel omvormen in een SortFilterProxy voor reviews
- i18n-argumenten juist instellen in één doorgang (bug 414060)
- Deze functies zijn @since 5.65, niet 5.64
- OBS toevoegen aan schermopnemers (bug 412320)
- Een aantal gebroken koppelingen repareren, koppelingen bijwerken naar https://kde.org/applications/
- Vertaling van $GenericName repareren
- Een "Meer wordt geladen..." tonen als indicator voor bezig bij laden van te bekijken gegevens
- Enige nettere terugkoppeling geven in NewStuff::Page terwijl de Engine aan het laden is (bug 413439)
- Een overlaycomponent toevoegen voor terugkoppeling van itemactiviteit (bug 413441)
- Alleen DownloadItemsSheet tonen als er meer dan één download item is (bug 413437)
- De aanwijzercursor voor de enkel te klikken gedelegeerden (bug 413435)
- De indelingen van de header voor EntryDetails en Page componenten repareren (bug 413440)

### KNotification

- Laat de documenten weergeven dat setIconName de voorkeur heeft boven setPixmap indien mogelijk
- Configuratiebestandspad van document op Android 

### KParts

- BrowserRun::simpleSave juist markeren as afgekeurd
- BrowserOpenOrSaveQuestion: AskEmbedOrSaveFlags-enum verplaatsen uit BrowserRun

### KPeople

- Sorteren starten vanuit QML toestaan

### kquickcharts

Nieuwe module. De module Quick Charts biedt een set grafieken die gebruikt kunnen worden uit QtQuick-toepassingen. Ze zijn bedoeld om gebruikt te worden voor zowel eenvoudig tonen van gegevens evenals voortdurend tonen van een hoog volume aan gegevens (vaak gerefereerd naar als plotters). De grafieken gebruiken een systeem genaamd "distance fields" voor hun versnelde rendering, die manieren levert voor gebruik van de GPU voor renderen van 2D vormen zonder verlies van kwaliteit.

### KTextEditor

- KateModeManager::updateFileType(): modi valideren en menu herladen van de statusbalk
- Modi verifiëren van het sessieconfiguratiebestand
- LGPLv2+ na ok door Svyatoslav Kuzmich
- pre-format van bestanden herstellen

### KTextWidgets

- kregexpeditorinterface afkeuren
- [kfinddialog] gebruik van kregexpeditor plug-in-systeem verwijderen

### KWayland

- [server] geen eigenaar zijn van dmabuf-implementatie 
- [server] Maak dubbel gebufferde eigenschappen in xdg-shell dubbel gebufferd

### KWidgetsAddons

- [KSqueezedTextLabel] pictogram voor actie "Gehele tekst kopiëren" toevoegen
- Eenheid maken van behandeling van marges in KPageDialog in KPageDialog zelf (bug 413181)

### KWindowSystem

- Telling aanpassen na _GTK_FRAME_EXTENTS toevoeging
- Ondersteuning voor _GTK_FRAME_EXTENTS toegevoegd

### KXMLGUI

- Ondersteuning voor ongebruikte gebroken KGesture laten vallen
- KAboutPluginDialog toevoegen om te worden gebruikt met KPluginMetaData
- Aanroepen van restauratie van sessielogica toestaan wanneer apps handmatig worden gestart (bug 413564)
- Ontbrekende eigenschap toevoegen aan KKeySequenceWidget

### Oxygen-pictogrammen

- Symbolische koppeling maken van microfoon naar audio-input-microphone op alle groottes (bug 398160)

### Plasma Framework

- beheer van backgroundhints verplaatsen naar Applet
- de bestandskiezer gebruiken in de interceptor
- meer gebruik van ColorScope
- ook wijzigingen in venster monitoren
- ondersteuning voor gebruiker die achtergrond en automatische schaduw wil verwijderen
- bestandskiezers ondersteunen
- qml-bestandskiezers ondersteunen
- verdwaalde qgraphicsview zaken verwijderen
- wallpaperinterface niet verwijderen en opnieuw maken indien niet nodig
- MobileTextActionsToolBar controleren als controlRoot ongedefinieerd is alvorens het te gebruiken
- hideOnWindowDeactivate aan PlasmaComponents.Dialog toevoegen

### Omschrijving

- het commando cmake invoegen dat we op het punt staan te gebruiken

### QQC2StyleBridge

- [TabBar] vensterkleur gebruiken in plaats van kleur van knop (bug 413311)
- ingeschakelde eigenschappen binden aan de ingeschakelde weergave
- [ToolTip] timeout baseren op lengte van tekst
- [ComboBox] Pop-up niet dimmen
- [ComboBox] Geef focus niet aan wanneer pop-up is geopend
- [ComboBox] focusPolicy volgen

### Solid

- [udisks2] detectie van mediawijzigingen voor externe optische stations (bug 394348)

### Sonnet

- Backend met ispell met mingw uitschakelen
- ISpellChecker-backend implementeren voor Windows &gt;= 8
- Ondersteuning voor basis kruislings compileren voor parsetrigrams
- trigrams.map inbedden in gedeelde bibliotheek

### Syndication

- Bug 383381 reparatie - ophalen van de feed-URL uit een youtube-kanaal werkt niet langer (bug 383381)
- Code extraheren zodat we ontleden van code kunnen repareren (bug 383381)
- atom heeft ondersteuning van pictogrammen (zodat we specifieke pictogrammen in akregator kunnen gebruiken)
- Als een echte qtest toepassing converteren

### Accentuering van syntaxis

- Bijwerkelementen uit CMake 3.16 uiteindelijke vrijgave
- reStructuredText: accentuering van inline literals voorafgaand aan tekens
- rst: ondersteuning voor alleenstaande hyperlinks toevoegen
- JavaScript: sleutelwoorden verplaatsen van TypeScript en andere verbeteringen
- JavaScript/TypeScript React: syntaxisdefinities hernoemen
- LaTeX: backslash scheidingsteken in sommige sleutelwoorden repareren (bug 413493)

### ThreadWeaver

- URL met transportversleuteling gebruiken

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
