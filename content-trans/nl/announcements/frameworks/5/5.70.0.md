---
aliases:
- ../../kde-frameworks-5.70.0
date: 2020-05-02
layout: framework
libCount: 70
---
### Baloo

- [FileWatch] redundante watchIndexedFolders() slot verwijderen
- [ModifiedFileIndexer] Een opmerking verhelderen
- [FileWatch] bewaken van bijwerken bij configuratie wijzigingen repareren
- [KInotify] overeenkomende paden repareren bij verwijderen van bewaking
- [Extractor] Gecategoriseerde logging gebruiken
- KFileMetaData gebruiken voor XAttr-ondersteuning in plaats van privaat opnieuw implementeren
- Baloo-DBus-signalen toevoegen voor verplaatste of verwijderde bestanden" terugdraaien
- [QML Monitor] Resterende tijd zo spoedig mogelijk tonen
- [FileContentIndexer] status bijwerken en volgorde signaal repareren
- [Monitor] monitorstatus en signaalvolgorde repareren
- [Extractor] voortgangsrapportage repareren
- [Coding] herhaald losmaken en controle op grootte vermijden
- [baloo_file] KAboutData uit baloo_file verwijderen
- [Searchstore] ruimte voor phrase query terms reserveren
- [SearchStore] exacte overeenkomsten in afvragen naar niet-eigenschappen toestaan
- [PhraseAndIterator] tijdelijke arrays verwijderen bij controleren van overeenkomsten
- [Extractor] balanceren tussen modi idle en bezig verbeteren
- [Extractor] monitoren van idle repareren
- [Extractor] IdleStateMonitor wrapperklasse verwijderen
- [OrpostingIterator] overslaan van elementen toestaan, skipTo implementeren
- [PhraseAndIterator] recursieve next() implementatie vervangen
- [AndPostingIterator] recursieve next() implementatie vervangen
- [PostingIterator] Ga na dat skipTo ook werkt voor het eerste element
- newBatchTime-signaal in filecontentindexer hernoemen en exporteren
- [SearchStore] dubbele waarden in opvragen van eigenschappen behandelen
- [AdvancedQueryParser] semantische behandeling van tokens naar SearchStore verplaatsen
- [Inotify] not-so-OptimizedByteArray verwijderen
- [Inotify] dode/gedupliceerde code verwijderen
- [QueryParser] single-use helper vervangen door std::none_of

### Breeze pictogrammen

- Hoekvouw van document naar rechtsboven verplaatsen in twee pictogrammen
- 16px konversatiepictogram toevoegen
- vscode-pictogramnaam corrigeren
- 16px Vvave-pictogram toevoegen
- alligator-pictogram toevoegen
- Pictogrammen preferences-desktop-tablet en preferences-desktop-touchpad toevoegen
- Koppelingen in README.md bijwerken
- build: pad naar bronmap aanhalen
- Bouwen uit een alleen-lezen bronlocatie toestaan
- Pictogrammen vouw-uit/vouw-in om bestaande pictogrammen alles-uitvouwen/alles-invouwen gezelschap te houden
- Auth-sim-locked en auth-sim-missing toevoegen
- Simkaartapparaat-pictogrammen toevoegen
- Rotatie pictogrammen toevoegen
- !6px pictogram Systeeminstellingen toevoegen
- ButtonFocus wijzigen naar Highlight
- Uiterlijk van kcachegrind verbeteren
- Rand van format-border-set-* pictogrammen verwijderen

### Extra CMake-modules

- android: de architectuur in de apk-naam meenemen
- ECMAddQch: gebruik van aanhalingstekens repareren met PREDEFINED in doxygen configuratie
- FindKF5 aanpassen aan strictere controles in nieuwer find_package_handle_standard_args
- ECMAddQch: help doxygen om Q_DECLARE_FLAGS te behandelen, zodat zulke typen docs worden
- Waylandscanner-waarschuwingen repareren
- ECM: poging om KDEInstallDirsTest.relative_or_absolute op Windows te repareren

### KDE Doxygen hulpmiddelen

- Ontbrekende witruimte na "Platform(s):" op de voorpagina repareren
- Gebruik vaan aanhalingstekens voor PREDEFINED items in Doxygile.global repareren
- doxygen helpen bij Q_DECL_EQ_DEFAULT &amp; Q_DECL_EQ_DELETE
- Opvangbak toevoegen op mobiel en code opschonen
- doxygen helpen bij Q_DECLARE_FLAGS, zodat zulke typen gedocumenteerd kunnen worden
- Naar Aether Bootstrap 4 overzetten
- api.kde.org opnieuw doen om meer op Aether te lijken

### KBookmarks

- Actioncollection altijd aanmaken
- [KBookMarksMenu] objectName instellen voor newBookmarkFolderAction

### KCMUtils

- KSettings::Dialog: ondersteuning toevoegen voor KPluginInfos zonder een KService
- Kleine optimalisatie: kcmServices() slechts eenmaal aanroepen
- Waarschuwing over oude stijl KCM naar qDebug afwaarderen, tot KF6
- ecm_setup_qtplugin_macro_names gebruiken

### KConfig

- kconfig_compiler - kconfig-instellingen met subgroep genereren
- Enige waarschuwingen van de compiler repareren
- opslaangedrag naar KEntryMap afdwingen toevoegen
- Standaard sneltoets toevoegen voor "Verborgen bestanden tonen/verbergen" (bug 262551)

### KContacts

- Beschrijving in metainfo.yaml met die in README.md in lijn brengen

### KCoreAddons

- API dox: ulong typedef met Q_PROPERTY(percent) gebruiken om doxygen bug te vermijden
- API dox: Q_DECLARE_FLAGS-based vlaggen documenteren
- Oude KLibFactory typedef markeren als afgekeurd
- [KJobUiDelegate] AutoHandlingEnabled-vlag toevoegen

### KCrash

- Gebruik van klauncher vanuit KCrash laten vallen

### KDeclarative

- De naam van de inhoud van het kcmcontrols-project op de juiste manier geven
- Documentatie kcmcontrols aanpassen
- Methode startCapture toevoegen
- [KeySequenceHelper] Om meta-modifiergedrag heen werken
- Ook het venster in de vernietiger vrijgeven

### KDED

- KToolInvocation::kdeinitExecWait naar QProcess overzetten
- Vertraagde tweede fase laten vallen

### KHolidays

- Vakanties in Nicaragua
- Taiwanese vakantiedagen
- Roemeense vakantiedagen bijgewerkt

### KI18n

- KI18N_WRAP_UI macro: eigenschap SKIP_AUTOUIC instellen op ui-bestand en gen. header

### KIconThemes

- Notitie toevoegen over loadMimeTypeIcon overzetten

### KImageFormats

- Ondersteuning voor moderne Gimp-afbeeldingen/XCF-bestanden toevoegen

### KIO

- [RenameDialog] een pijl toevoegen die de richting van bron naar bestemming aangeeft (bug 268600)
- KIO_SILENT API documenten aan de werkelijkheid aanpassen
- Behandeling van niet vertrouwde programma's naar ApplicationLauncherJob verplaatsen
- Controle op ongeldige service uit KDesktopFileActions naar ApplicationLauncherJob verplaatsen
- Uitvoerbare programma's zonder +x permissie in $PATH detecteren om foutmeldingen te verbeteren (bug 415567)
- Het sjabloon voor eht HTML-bestand meer bruikbaar maken (bug 419935)
- JobUiDelegate constructor toevoegen met AutoErrorHandling-vlag en venster
- Berekening van cachemap repareren bij aan prullenbak toevoegen
- Bestandsprotocol: ga na dat KIO::StatAcl werkt zonder impliciete afhankelijkheid van KIO::StatBasic
- Detailwaarde van KIO::StatRecursiveSize toevoegen zodat kio_trash dit alleen doet op aanvraag
- CopyJob: bij gebruik van stat op de bestemming, StatBasic gebruiken
- [KFileBookMarkHandler] naar nieuw KBookmarkMenu-5.69 overzetten
- KStatusBarOfflineIndicator markeren als afgekeurd
- KLocalSocket vervangen door QLocalSocket
- Crash in uitgavemodus na de waarschuwing over onverwachte afsplitsitems vermijden (bug 390288)
- Docu: noemen van niet bestaand signaal verwijderen
- [renamedialog] KIconLoader gebruik vervangen door QIcon::fromTheme
- kio_trash: grootte, modificatie, toegang en datum voor prullenbak aanmaken:/ (bug 413091)
- [KDirOperator] nieuwe standaard sneltoets "Verborgen bestanden tonen/verbergen" gebruiken (bug 262551)
- Voorbeelden tonen op versleutelde bestandssystemen (bug 411919)
- [KPropertiesDialog] Vervangen van mappictogram op afstand uitschakelen (bug 205954)
- [KPropertiesDialog] QLayout waarschuwing repareren
- API dox: documenteer meer van de standaard eigenschapwaarden van KUrlRequester
- DirectorySizeJob repareren zodat het niet afhangt van volgorde in de lijst
- KRun: toekenning repareren wanneer een toepassing starten mislukt

### Kirigami

- Theme::smallFont introduceren
- BasicListItem meer nuttig maken door het een eigenschap ondertitel te geven
- PageRouterAttached met minder segfaults
- PageRouter: beter zoeken naar ouders van items
- Ongebruikte QtConcurrent uit colorutils verwijderen
- PlaceholderMessage: gebruikt van Plasma-eenheden verwijderen
- PlaceholderMessage toestaan geen tekst te bevatten
- verticaal centreren van werkvelden als ze geen schuifbalk hebben (bug 419804)
- Rekening houden met boven- en ondermarge in standaard cardhoogte
- Verschillende reparaties aan nieuwe Cards (bug 420406)
- Pictogram: pictogramrendering verbeteren op multi-scherm multi-dpi setups
- Fout repareren in PlaceholderMessage: acties zijn uitgeschakeld, niet verborgen
- PlaceholderMessage-component introduceren
- Hotfix: slechte typering in FormLayout arrayfuncties repareren
- Snelle reparatie voor SwipeListItem: Array.prototype.*.call gebruiken
- Snelle reparatie: Array.prototype.some.call in ContextDrawer gebruiken
- Snelle reparatie voor D28666: Array.prototype.*.call gebruiken in plaats van aanroepen van functies op 'list'-objecten
- Ontbrekende m_sourceChanged-variabele toevoegen
- ShadowedRectangle gebruiken voor achtergrond van kaart gebruiken (bug 415526)
- De controle op zichtbaarheid voor ActionToolbar bijwerken door breedte te controleren met minder-"gelijk"
- Een aantal 'triviale' reparaties voor gebroken code
- nooit sluiten wanneer de klik is binnen de inhoud van het vel (bug 419691)
- vel moet onder andere pop-ups zijn (bug 419930)
- PageRouter-component toevoegen
- ColorUtils toevoegen
- Instelling van gescheiden hoekstralen voor ShadowedRectangle toestaan
- De optie STATIC_LIBRARY verwijderen om statische builds te repareren

### KJobWidgets

- KDialogJobUiDelegate(KJobUiDelegate::Flags) constructor toevoegen

### KJS

- UString operator= implementeren om gcc gelukkig te maken
- Compilerwaarschuwing over kopie van niet-triviale gegevens stil maken

### KNewStuff

- KNewStuff: bestandspad en procesaanroep repareren (bug 420312)
- KNewStuff: overzetten uit KRun::runApplication naar KIO::ApplicationLauncherJob
- Vokoscreen door VokoscreenNG vervangen (bug 416460)
- Meer foutrapportage zichtbaar voor gebruiker introduceren voor installaties (bug 418466)

### KNotification

- Bijwerken van meldingen op Android implementeren
- Meldingen met meerdere regels en opgemaakte tekst op Android behandelen
- KNotificationJobUiDelegate(KJobUiDelegate::Flags) constructor toevoegen
- [KNotificationJobUiDelegate] "Mislukt" achtervoegen voor foutmeldingen

### KNotifyConfig

- knotify-config.h consistent gebruiken om door te geven in vlaggen over Canberra/Phonon

### KParts

- StatusBarExtension(KParts::Part *) overladen constructor toevoegen

### KPlotting

- foreach (afgekeurd) overzetten naar range for

### KRunner

- DBus Runner: service-eigenschap om eenmalig acties te vragen toevoegen (bug 420311)
- Een waarschuwing afdrukken als runner incompatibel is met KRunner

### KService

- KPluginInfo::service() afkeuren, omdat de constructor met een KService is afgekeurd

### KTextEditor

- slepen en loslaten aan linkerzijde van widget repareren (bug 420048)
- Complete weergaveconfiguratie opslaan en ophalen in en uit sessieconfiguratie
- Voorbarig overzetten naar niet-vrijgegeven Qt 5.15 terugdraaien die intussen is gewijzigd

### KTextWidgets

- [NestedListHelper] inspringen van selectie repareren, testen toevoegen
- [NestedListHelper] inspringcode verbeteren
- [KRichTextEdit] ga na headings doen niets verkeerd met stack voor ongedaan maken
- [KRichTextEdit] springen bij schuiven wanneer horizontale regel is toegevoegd repareren (bug 195828)
- [KRichTextWidget] oude workaround verwijderen en regressie repareren (commit 1d1eb6f)
- [KRichTextWidget] ondersteuning voor headings toevoegen
- [KRichTextEdit] indrukken van een toets als enkele wijziging in stack met ongedaan maken behandelen (bug 256001)
- [findreplace] zoeken behandelen voor WholeWordsOnly in modus Regex

### KUnitConversion

- Imperial gallon en US pint toevoegen (bug 341072)
- IJslandse kroon toevoegen aan valuta

### KWayland

- [Wayland] Toevoegen aan PlasmaWindowManagement-protocol voor volgorde van vensterstapeling
- [server] Enige onderwater lifecycle signalen toevoegen

### KWidgetsAddons

- [KFontChooser] NoFixedCheckBox DisplayFlag verwijderen, dubbel
- [KFontChooser] nieuwe DisplayFlag toevoegen; hoe vlaggen gebruikt worden wijzigen
- [KFontChooser] styleIdentifier() meer precies maken door styleName van lettertype toe te voegen (bug 420287)
- [KFontRequester] overzetten van QFontDialog naar KFontChooserDialog
- [KMimeTypeChooser] de mogelijkheden toevoegen om de boomstructuurweergave met een QSFPM te filteren (bug 245637)
- [KFontChooser] De code enigszins meer leesbaar maken
- [KFontChooser] een keuzevakje toevoegen om alleen tonen van lettertype met vaste breedte om te schakelen
- Onnodige include verwijderen

### KWindowSystem

- Betekenisvolle waarschuwing afdrukken wanneer er geen QGuiApplication is

### KXMLGUI

- [KRichTextEditor] ondersteuning voor headings toevoegen
- [KKeySequenceWidget] om gedrag van Meta-modifier heen werken

### NetworkManagerQt

- foreach vervangen door range-for

### Plasma Framework

- [PlasmaCore.IconItem] regressie: crash bij wijziging van broncode repareren (bug 420801)
- [PlasmaCore.IconItem] behandeling van broncode voor verschillende types opnieuw ontwerpen
- Tekstspatiëring van tekstballon van applet consistent maken
- [ExpandableListItem] het aanraakvriendelijk maken
- [ExpandableListItem] meer semantisch correcte pictogrammen voor uitvouwen en invouwen
- PC3 BusyIndicator binding loop repareren
- [ExpandableListItem] nieuwe optie showDefaultActionButtonWhenBusy toevoegen
- Afgeronde randen aan plasmoidHeading verwijderen
- [ExpandableListItem] signaal itemCollapsed toevoegen en geen itemExpanded uitsturen indien ingevouwen
- Readmes verklarende status van plasmacomponentversies toevoegen
- [configview] code vereenvoudigen / workaround Qt5.15 crash
- ExpandableListItem aanmaken
- Animatieduur consistent maken met Kirigami waarden

### QQC2StyleBridge

- QQC2 versie detecteren op moment van bouwen met actuele detectie
- [ComboBox] transparante dimmer gebruiken

### Solid

- [Solid] foreach overzetten naar range/index for
- [FakeCdrom] een nieuwe UnknownMediumType enumerator naar MediumType toevoegen
- [FstabWatcher] verlies van fstab bewaker repareren
- [Fstab] deviceAdded niet tweemaal uitsturen bij fstab/mtab wijzigingen

### Accentuering van syntaxis

- debchangelog: Groovy Gorilla toevoegen
- Ondersteuning van taalsyntaxis van Logtalk bijwerken
- TypeScript: de "awaited" type-operator toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
