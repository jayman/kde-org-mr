---
aliases:
- ../../kde-frameworks-5.54.0
date: 2019-01-12
layout: framework
libCount: 70
---
### Attica

- Geef een melding als een standaard leverancier mislukt bij downloaden

### Baloo

- Helper voor typesForMimeType verplaatsen uit BasicIndexingJob naar anonieme naamruimte
- "image/svg" toevoegen als Type::Image aan de BasicIndexingJob
- Formattering van Compact json gebruiken voor opslaan van metagegevens in documenten

### Breeze pictogrammen

- preferences-system-network wijzigen in symlink
- Een Kolf pictogram gebruiken met mooie achtergrond en schaduwen
- Enige gewijzigde pictogrammen kopiëren van Breeze naar Breeze Dark
- YaST en nieuwe pictogrammen voor voorkeur
- Een toepasselijk pictogram voor python-bytecode toevoegen, consistente kleur in python-pictogrammen gebruiken (bug 381051)
- Symbolische koppelingen verwijderen naar python-bytecode in voorbereiding op het worden van eigen pictogrammen
- Maak het pictogram voor het python-mimetype de basis en het pictogram voor de python bytecode er een koppeling naar
- Apparaatpictogrammen voor RJ11 en RJ45 poorten toevoegen
- Ontbrekende scheiding van map toevoegen (bug 401836)
- Juiste pictogram voor Python 3 scripts gebruiken (bug 402367)
- Kleur van netwerk/webpictogrammen wijzigen naar consistente stijl
- Nieuwe naam voor sqlite-bestanden toevoegen, zodat de pictogrammen ook echt verschijnen (bug 402095)
- Pictogrammen voor drive-* voor YaST Partitioner toevoegen
- Pictogram voor privé-weergeven toevoegen (bug 401646)
- Pictogrammen acties met zaklamp toevoegen
- Symbolisme in pictogram voor uit en gedempte status verbeteren (bug 398975)

### Extra CMake-modules

- Zoekmodule voor libphonenumber van Google toevoegen

### KActivities

- De versie in het pkgconfig-bestand repareren

### KDE Doxygen hulpmiddelen

- Doxygen markdown weergave repareren

### KConfig

- Zet escape-teken voor bytes groter of gelijk aan 127 in configuratiebestanden 

### KCoreAddons

- cmake macros: verouderde ECM var in kcoreaddons_add_plugin overzetten naar nieuwe vorm (bug 401888)
- eenheden en voorvoegsels van formatValue vertaalbaar maken

### KDeclarative

- geen scheidingen op mobiel
- root.contentItem in plaats van gewoon contentItem
- De ontbrekende api toevoegen voor multilevel KCM's om de kolommen te besturen

### KFileMetaData

- Laat test op schrijven als mimetype niet wordt ondersteund door de extractor mislukken
- Extractie van ape-schijfnummer repareren
- Extractie van hoes implementeren voor asf-bestanden
- Lijst met ondersteunde mimetypes uitbreiden voor ingebedde extractor van afbeeldingen
- Code van ingebedde extractor van afbeelding herschrijven voor grotere uitbreidbaarheid
- Ontbrekend mimetype wav toevoegen
- Meer tags extraheren uit exif-metagegevens (bug 341162)
- extractie van GPS-hoogte voor exif-gegevens repareren

### KGlobalAccel

- Bouwen van KGlobalAccel met vóóruitgave van Qt 5.13 repareren

### KHolidays

- README.md - basis instructies voor testen vakantiebestanden toevoegen
- verschillende agenda's - syntaxisfouten repareren

### KIconThemes

- ksvg2icns : Qt 5.9+ QTemporaryDir API gebruiken

### KIdleTime

- [xscreensaverpoller] leegmaken na reset van schermbeveiliging

### KInit

- Zachte rlimit gebruiken voor aantal open handels. Dit repareert erg langzaam opstarten van Plasma met de laatste systemd.

### KIO

- "Voorbeeld van bestand verbergen wanneer pictogram te klein is" omdraaien
- Fout tonen in plaats van stil mislukken bij vragen om map aan te maken die al bestaat (bug 400423)
- Het pad voor elk item van de submappen in een hernoemen van de map wijzigen(bug 401552)
- getExtensionFromPatternList reg exp filtering uitbreiden
- [KRun] wanneer gevraagd om koppeling te openen in externe browser, valt terug naar lijst mimeapps. als niets is ingesteld in kdeglobals (bug 100016)
- Maak de functie url openen in tabblad een beetje meer te ontdekken (bug 402073)
- [kfilewidget] geef te bewerken URL navigator terug aan broodkruimelmodus als het focus heeft en alles is geselecteerd en wanneer Ctrl+L wordt ingedrukt
- [KFileItem] controle van isLocal repareren in checkDesktopFile (bug 401947)
- SlaveInterface: speed_timer stoppen nadat een job is afgebroken
- De gebruiker waarschuwen voor een kopieer/verplaats opdracht als de bestandsgrootte uit gaat boven de maximaal mogelijke bestandsgrootte in het FAT32 bestandsysteem (4 GB) (bug 198772)
- Voortdurend de Qt-event-queue in KIO slaves verhogen vermijden
- Ondersteuning voor TLS 1.3 (onderdeel van Qt 5.12)
- [KUrlNavigator] firstChildUrl repareren bij teruggaan uit archief

### Kirigami

- Ga na dat we de QIcon::themeName niet overschrijven wanneer dat niet zou moeten
- Een DelegateRecycler aangekoppeld object introduceren
- marges van gridview repareren rekening houdend met schuifbalken
- Laat AbstractCard.background reageren op AbstractCard.highlighted
- Code in MnemonicAttached vereenvoudigen
- SwipeListItem: toon de pictogrammen altijd als !supportsMouseEvents
- Ga na of needToUpdateWidth overeenkomt met widthFromItem, niet hoogte
- Neem de schuifbalk in rekening voor de ScrollablePage margin (bug 401972)
- Probeer geen opnieuw positioneren van de ScrollView wanneer we een foute hoogte krijgen (bug 401960)

### KNewStuff

- Standaard sorteervolgorde wijzigen in de downloaddialoog naar "Meeste downloads" (bug 399163)
- Melding over de leverancier die niet wordt geladen

### KNotification

- [Android] zachter mislukken bij bouwen met API &lt; 23
- Android backend voor meldingen toevoegen
- Zonder Phonon en D-Bus bouwen op Android

### KService

- applications.menu: ongebruikte categorie X-KDE-Edu-Teaching verwijderen
- applications.menu: &lt;KDELegacyDirs/&gt; verwijderen

### KTextEditor

- Scripten voor Qt 5.12 repareren
- emmet-script repareren door HEX in plaats van OCT getallen in tekenreeksen te gebruiken (bug 386151)
- Gebroken Emmet repareren (bug 386151)
- ViewConfig: optie 'Dynamic Wrap At Static Marker' toevoegen
- opvouwen van regio-einde repareren, beëindigingstoken aan de reeks toevoegen
- lelijk overtekenen met alfa vermijden
- Niet opnieuw woorden markeren toegevoegd/genegeerd in het woordenboek als fout gespeld (bug 387729)
- KTextEditor: actie toevoegen voor statische woordafbreking (bug 141946)
- Actie 'Woordenboekreeksen wissen' niet verbergen
- Niet vragen om bevestiging bij opnieuw laden (bug 401376)
- class Message: initialisatie van inclass-member gebruiken
- Stel KTextEditor::ViewPrivate:setInputMode(InputMode) bloot aan KTextEditor::View
- Prestaties van kleine bewerkingen verbeteren, bijv. grote acties alles vervangen repareren (bug 333517)
- updateView() in visibleRange() alleen aanroepen wanneer endPos() ongeldig is

### KWayland

- Uitleg over gebruik van zowel KDE's ServerDecoration als XdgDecoration toevoegen
- Ondersteuning voor Xdg decoratie
- XDGForeign Client header installaties repareren
- [server] ondersteuning voor aanraakverslepen
- [server] meerdere aanraakinterfaces per client toestaan

### KWidgetsAddons

- [KMessageBox] minimale dialoggrootte repareren wanneer om details wordt verzocht (bug 401466)

### NetworkManagerQt

- Instellingen voor DCB, macsrc, match, tc, ovs-patch en ovs-port toegevoegd

### Plasma Framework

- [Calendar] firstDayOfWeek in MonthView laten zien (bug 390330)
- preferences-system-bluetooth-battery toevoegen aan preferences.svgz

### Omschrijving

- Plug-intype toevoegen voor delen van URL's

### QQC2StyleBridge

- Menu-itembreedte repareren wanneer de gedelegeerde wordt overschreven (bug 401792)
- Indicator voor bezig rechtsom laten draaien
- Keuzevakjes/radioknop naar vierkant forceren

### Solid

- [UDisks2] MediaRemovable gebruiken om te bepalen of medium uitgeworpen kan worden
- Batterijen voor Bluetooth ondersteunen

### Sonnet

- Methode aan BackgroundChecker toevoegen om woord aan sessie toe te voegen
- DictionaryComboBox: woordenboeken met voorkeur van gebruiker bovenaan houden (bug 302689)

### Accentuering van syntaxis

- Ondersteuning voor php-syntaxis bijwerken
- WML: ingebedde Lua-code repareren &amp; nieuwe standaard stijlen gebruiken
- CUDA .cu en .cuh bestanden accentueren als C++
- TypeScript &amp; TS/JS React: detectie van typen verbeteren, drijvende komma repareren &amp; andere verbeteringen/reparaties
- Haskell: leeg commentaar accentueren na 'import'
- WML: oneindige lus repareren in omschakelen van contexten &amp; alleen tags accentueren met geldige namen (bug 402720)
- BrightScript: workaround toevoegen voor QtCreator 'endsub' accentueren, functie/sub invouwen toevoegen
- meer varianten van C-literals voor getallen ondersteunen (bug 402002)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
