---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE stelt KDE Applicaties 16.04.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.04.2 beschikbaar
version: 16.04.2
---
14 juni 2016. Vandaag heeft KDE de tweede update voor stabiliteit vrijgegeven voor <a href='../16.04.0'>KDE Applicaties 16.04</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 25 aangegeven bugreparaties inclusief verbeteringen aan konadi, ark, artikulate, dolphin. kdenlive, kdepim, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.21.
