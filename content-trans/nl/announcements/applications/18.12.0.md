---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE stelt KDE Applicaties 18.12 beschikbaar.
layout: application
release: applications-18.12.0
title: KDE stelt KDE Applicaties 18.12.0 beschikbaar
version: 18.12.0
---
{{% i18n_date %}}

KDE Applications 18.12 is uitgebracht.

{{%youtube id="ALNRQiQnjpo"%}}

We werken voortdurend aan verbetering van de software in onze KDE Application series en we hopen dat u alle nieuwe verbeteringen en reparaties van bugs nuttig vindt!

## Wat is er nieuw in KDE Applicaties 18.12

Meer dan 140 bugs zijn opgelost in toepassingen inclusief de Kontact Suite, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello en meer!

### Bestandsbeheer

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, de krachtige bestandsbeheerder van KDE:

- Nieuwe implementatie van MTP die het volledig bruikbaar maakt voor productie
- Enorme verbetering van prestatie voor het lezen van bestanden over het SFTP-protocol
- Voor voorbeelden met miniaturen worden frames en schaduwen nu alleen getekend voor afbeeldingsbestanden zonder transparantie, waarmee het weergeven voor pictogrammen wordt verbeterd
- Nieuwe voorbeelden met miniaturen voor LibreOffice-documenten en AppImage apps
- Videobestanden groter dan 5 MB worden nu op de mapminiaturen getoond wanneer miniaturen in de map is ingeschakeld
- Bij lezen van audio-cd's is Dolphin nu in staat om de CBR-bitsnelheid te wijzigen voor de MP3-encoder en repareert tijdstippen voor FLAC
- Het besturingsmenu van Dolphin toont nu het item 'Nieuwe aanmaken…' en heeft een nieuw menu-item 'Verborgen plaatsen tonen'
- Dolphin stopt nu wanneer er slechts één tabblad is geopend en de standaard sneltoets (Ctrl+W) voor 'tabblad sluiten' wordt ingedrukt
- Na afkoppelen van een volume uit het paneel Plaatsen, is het nu mogelijk om het opnieuw aan te koppelen
- De weergave Recente documenten (beschikbaar door te bladeren naar recentdocuments:/ in Dolphin) toont nu alleen actuele documenten en filtert automatisch web-URL's uit
- Dolphin toont nu een waarschuwing voor het toestaan om een bestand of map te hernoemen op zo'n manier dat het onmiddellijk wordt verborgen
- Het is niet langer mogelijk om schijven af te koppelen voor uw actieve besturingssysteem of thuismap uit het paneel Plaatsen

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, het traditionele zoekprogramma naar bestanden, van KDE, heeft nu een zoekmethode voor metagegevens gebaseerd op KFileMetaData.

### Kantoor

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, de krachtige e-mailclient van KDE:

- KMail kan nu een geünificeerd postvak in tonen
- Nieuwe plug-in: HTML-e-mail genereren uit Markdown-taal
- Met behulp van Purpose for Sharing Text (als e-mail)
- HTML-e-mails zijn nu te lezen ongeacht welk kleurenschema wordt gebruikt

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, de veelzijdige documentviewer van KDE:

- Nieuw hulpmiddel voor annotatie 'Typewriter' die gebruikt kan worden om tekst overal te schrijven
- De hiërarchische inhoudsopgave heeft nu de mogelijkheid om alles uit en in te klappen of gewoon een specifieke sectie te tonen
- Verbeterd gedrag van regels afbreken in inline annotaties
- Bij zweven met de muis over een koppeling wordt de URL nu getoond op elk moment dat er op geklikt kan worden, in plaats van alleen bij modus Bladeren
- ePub-bestanden met hulpbronnen met spaties in hun URL's worden nu juist getoond

### Ontwikkeling

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, de geavanceerde tekstbewerker van KDE:

- Bij gebruik van de ingebedde terminal wordt deze nu automatisch gesynchroniseerd met de huidige map met de locatie op schijf van het actieve document
- De ingebedde terminal van Kate kan nu focus krijgen en verliezen met de sneltoets F4
- De ingebouwde omschakelaar van tabbladen van Kate toont nu volledige paden voor gelijknamige bestanden
- Regelnummers zijn nu standaard aan
- De ongelooflijk nuttige en krachtige tekstfilterplug-in is nu standaard ingeschakeld en is beter te ontdekken
- Openen van een reeds geopend document met de functie Snel openen schakelt nu terug naar dat document
- De functie Snel openen toont niet langer dubbele items
- Bij gebruik van meerdere activiteiten worden bestanden nu geopend in de juiste activiteit
- Kate toont nu alle juiste pictogrammen bij uitvoeren op GNOME met het pictogramthema van GNOME

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, het terminalprogramma van KDE:

- Konsole ondersteunt nu volledig emoji-tekens
- Inactieve pictogrammen van tabbladen worden nu geaccentueerd wanneer ze een belsignaal ontvangen
- Achterliggende dubbele punten worden niet langer beschouwd als onderdeel van een woord voor het doel van selecteren door dubbel te klikken, waarmee makkelijk gemaakt wordt om paden en uitvoer van 'grep' te selecteren
- Wanneer een muis met knoppen voor achteruit en vooruit wordt ingeplugd, kan Konsole nu deze knoppen gebruiken voor omschakelen tussen tabbladen
- Konsole heeft nu een menu om de tekengrootte te resetten naar de standaard van het profiel, als deze is vergroot of verkleind
- Tabbladen zijn nu moeilijker per ongeluk los te maken en sneller nauwkeurig te ordenen
- Verbeterd gedrag van selectie met shift-klikken
- Dubbel klikken op een tekstregel die over de breedte van het venster gaat is gerepareerd
- De zoekbalk wordt opnieuw gesloten wanneer u op de Escape-toets drukt

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, Het hulpmiddel voor vertalen van KDE:

- Vertaalde bestanden in het tabblad Project verbergen
- Basisondersteuning van pology, het systeem voor controleren van syntaxis en woordenlijst
- Vereenvoudigde navigatie met ordenen van tabbladen en openen van meerdere tabbladen
- Segfaults gerepareerd vanwege concurrerende toegang tot databaseobjecten
- Inconsistent slepen en loslaten verbeterd
- Een bug gerepareerd bij sneltoetsen die verschillen tussen bewerkers
- Verbeterd zoekgedrag (zoeken zal naar meervoudige vormen zoeken en deze tonen)
- Compatibiliteit met Windows herstellen met dank aan het craft-bouwsysteem

### Hulpmiddelen

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, de afbeeldingsviewer van KDE:

- Het hulpmiddel 'Rode ogen reductie' kreeg een aantal mooie bruikbaarheidsverbeteringen
- Gwenview toont nu een waarschuwingsdialoog wanneer u de menubalk verbergt die u vertelt how het terug te krijgen

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, het hulpmiddel voor schermafdrukken van KDE:

- Spectacle heeft nu de mogelijkheid om schermafdrukbestanden sequentieel te nummeren en gebruikt standaard dit schema voor namen als u het tekstveld voor de bestandsnaam wist
- Opslaan van afbeeldingen in een formaat anders dan .png gerepareerd, bij gebruik van Opslaan als…
- Bij gebruik van Spectacle om een schermafdruk in een externe toepassing te openen, is het nu mogelijk de afbeelding te wijzigen en op te slaan, nadat u er klaar mee bent
- Spectacle opent nu de juiste map wanneer u klikt op Hulpmiddelen > Schermafdrukmap openen
- Tijdstempels van schermafdrukken geven nu weer wanneer de afbeelding is aangemaakt, niet wanneer het is opgeslagen
- Alle opties voor opslaan zijn nu aanwezig in de pagina “Opslaan”

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, de archiefbeheerder van KDE:

- Ondersteuning voor het Zstandard-formaat toegevoegd (tar.zst archieven)
- Bekijken van bepaalde bestanden in Ark (bijv. Open Document) als archieven in plaats van ze te openen in de bijbehorende toepassing, gerepareerd

### Wiskunde

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, de eenvoudige rekenmachine van KDE heeft nu een instelling om de laatst berekening meerdere keren te herhalen.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, de mathematische frontend van KDE:

- Markdown type invoer toevoegen
- Geanimeerde accentuering van het nu berekende opdrachtitem
- Visualisatie van opdrachtitems in de wacht (in de wachtrij gezet, maar nog niet worden uitgerekend)
- In staat sellen om opdrachtitems te formatteren (achtergrondkleur, voorgrondkleur, lettertype-eigenschappen)
- In staat stellen om nieuwe opdrachtitems op willekeurige plaatsen in het werkvel in te voegen door de cursor op de gewenste positie te plaatsen en door met typen te starten
- Voor uitdrukkingen met meerdere opdrachten, het resultaat tonen als onafhankelijke resultaatobjecten in het werkvel
- Ondersteuning toevoegen voor het openen van werkvellen via relatieve paden vanaf de console
- Ondersteuning toevoegen voor openen van meerdere bestanden in één Cantor-shell
- De kleur en het lettertype wijzigen voor wanneer gevraagd wordt naar extra informatie om beter onderscheid te maken tussen de gebruikelijke invoer in het opdrachtitem
- Sneltoetsen toegevoegd voor navigatie tussen de werkvellen (Ctrl+PageUp, Ctrl+PageDown)
- Actie toegevoegd in submenu 'Beeld' voor reset van zoomniveau
- Downloaden van Cantor projecten uit store.kde.org inschakelen (op dit moment werkt uploaden alleen vanaf de website)
- Het werkvel in modus alleen-lezen openen als de backend niet beschikbaar is in het systeem

<a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, de plotter van functies van KDE, veel problemen gerepareerd:

- Verkeerde namen van de plots voor afgeleiden en integralen in de conventionele notatie gerepareerd
- SVG exporteren in Kmplot werkt nu op de juiste manier
- Functie eerste afgeleide heeft geen notatie met accent
- Functie unfocused deactiveren verbergt nu zijn grafiek:
- Crash van Kmplot bij recursief openen van 'Constanten bewerken' uit functiebewerker gerepareerd
- Crash van KmPlot na verwijderen van een functie die de muisaanwijzer volgt in de plot gerepareerd
- Getekende gegevens kunt u nu exporteren in elk afbeeldingsformaat
