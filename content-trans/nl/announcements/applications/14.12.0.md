---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE stelt KDE Applicaties 14.12 beschikbaar.
layout: application
title: KDE stelt KDE Applicaties 14.12 beschikbaar
version: 14.12.0
---
17 december 2014. Vandaag heeft KDE Applications 14.12 beschikbaar gesteld. Deze uitgave brengt nieuwe functies en reparaties van bugs aan meer dan honderd toepassingen. De meeste van deze toepassingen zijn gebaseerd op het KDE Development Platform 4; sommige zijn geconverteerd naar het nieuwe <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, een set van in modulaire vorm gebrachte bibliotheken die gebaseerd zijn op Qt5, de nieuwste versie van dit populaire framework voor toepassingen op allerlei platformen.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> is nieuw in deze uitgave; het is een bibliotheek om gezichtsdetectie en -herkenning in foto's in te schakelen.

De uitgave bevat de eerste op KDE Frameworks 5 gebaseerde versies van <a href='http://www.kate-editor.org'>Kate</a> en <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='http://edu.kde.org/kig'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> and <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Enige bibliotheken zijn ook gereed voor gebruik in KDE Frameworks 5: analitza en libkeduvocdocument.

De <a href='http://kontact.kde.org'>Suite Kontact</a> is nu in Lange termijn ondersteuning in de 4.14 versie terwijl ontwikkelaars hun nieuwe energie stoppen om het over te brengen naar KDE Frameworks 5

Enige van de nieuwe functies in deze uitgave omvatten:

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> heeft een nieuwe Android versie dankzij KDE Frameworks 5 en is nu in staat om <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>zijn grafieken in 3D af te drukken</a>
+ <a href='http://edu.kde.org/kgeography'>KGeography</a> heeft een nieuwe kaart voor Bihar.
+ De documentviewer <a href='http://okular.kde.org'>Okular</a> heeft nu ondersteuning voor latex-synctex omgekeerd zoeken in dvi en enige kleine verbeteringen in de ondersteuning van ePub.
+ <a href='http://umbrello.kde.org'>Umbrello</a> --de UML-modeller-- heeft vele nieuwe functies, te veel om hier te noemen.

De uitgave in april van KDE Applications 15.04 zal vele nieuwe functies bevatten, evenals meer toepassingen gebaseerd op het modulaire KDE Frameworks 5.
