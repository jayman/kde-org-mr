---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Novo módulo: ModemManagerQt (interface do Qt para a API do ModemManager). Em alternativa, actualize para a versão Plasma-NM 5.3 Beta ao actualizar para o ModemManagerQt 5.9.0.

### KActivities

- Foi implementado o esquecimento de um recurso
- Correções de compilação
- Adição de um plugin de registro de eventos para as notificações do KRecentDocument

### KArchive

- Respeito da configuração do KZip::extraField também quando gravar os itens no cabeçalho central
- Remoção de duas validações erradas que ocorriam quando o disco estava cheio - erro <a href='https://bugs.kde.org/show_bug.cgi?id=343214'>343214</a>

### KBookmarks

- Correção da compilação com o Qt 5.5

### KCMUtils

- Novo sistema de plugins baseado em JSON. Os KCMs são pesquisados em kcms/. Por enquanto, ainda é necessário instalar um arquivo <i>desktop</i> em kservices5/, por questões de compatibilidade
- Carga e empacotamento da versão apenas em QML do <i>kcms</i>, se possível

### KConfig

- Correção de validação ao usar o KSharedConfig em um destruidor de objetos global.
- kconfig_compiler: adição do suporte para o CategoryLoggingName nos arquivos *.kcfgc, para gerar chamadas do qCDebug(categoria).

### KI18n

- Carregamento prévio do catálogo global do Qt ao usar o i18n()

### KIconThemes

- O KIconDialog pode agora ser mostrado com os métodos normais show() e exec() do QDialog
- Correção do KIconEngine::paint para lidar com <i>devicePixelRatios</i> diferentes

### KIO

- Ativação do KPropertiesDialog para mostrar a informação do espaço livre em disco de sistemas de arquivos remotos (p.ex., smb)
- Correção do KUrlNavigator com imagens de PPP elevados (high DPI)
- Fazer o KFileItemDelegate tratar do <I>devicePixelRatio</I> não predefinidos em animações

### KItemModels

- KRecursiveFilterProxyModel: Remodelação para emitir os sinais corretos no tempo correto
- KDescendantsProxyModel: Lidar com as movimentações informadas pelo modelo de origem.
- KDescendantsProxyModel: Correção do comportamento quando é efetuada uma seleção enquanto se reinicia.
- KDescendantsProxyModel: Permitir a construção e uso do KSelectionProxyModel a partir do QML.

### KJobWidgets

- Propagação do código de erro para a interface D-Bus do JobView

### KNotifications

- Adição de uma versão do event() que não recebe nenhum ícone e usa um padrão
- Adição de uma versão do event() que recebe o <i>StandardEvent eventId</i> e o <i>QString iconName</i>

### KPeople

- Permitir a extensão dos metadados de ação, usando os tipos predefinidos
- Correção da não-atualização correta do modelo após a remoção de um contato do Person

### KPty

- Exposição global se o KPty foi compilado sem a biblioteca <i>utempter</i>

### KTextEditor

- Adição do arquivo de realce do kdesrc-buildrc
- Sintaxe: Adição do suporte para literais inteiros binários no arquivo de realce de PHP

### KWidgetsAddons

- Tornar a animação do KMessageWidget suave com uma elevada proporção de pixels do dispositivo

### KWindowSystem

- Adição de uma implementação de testes do Wayland para o KWindowSystemPrivate
- O KWindowSystem::icon com o NETWinInfo não está associado à plataforma X11.

### KXmlGui

- Preservação do domínio de traduções ao mesclar os arquivos .rc
- Correção do aviso de execução do QWidget::setWindowModified: "O título da janela não contém um item de substituição '[*]'"

### KXmlRpcClient

- Instalação das traduções

### Framework do Plasma

- Correção de dicas inválidas quando o dono temporário da dica desaparecia ou ficava vazio
- Correção da <i>TabBar</i> não ficar inicialmente disposta de forma adequada, que podia ser verificado, por exemplo, no Kickoff
- As transições <i>PageStack</i> agora usam os <i>Animators</i> para animações mais suaves
- As transições <i>TabGroup</i> agora usam os <i>Animators</i> para animações mais suaves
- O Svg,FrameSvg agora funciona com o QT_DEVICE_PIXELRATIO

### Solid

- Atualização das propriedades da bateria ao retornar de uma suspensão

### Alterações no sistema de compilação

- Os Extra CMake Modules (ECM) têm agora o mesmo sistema de número de versões do KDE Frameworks. Sendo assim, agora está n versão 5.9, quando anteriormente era 1.8.
- Muitas plataformas foram corrigidas para poderem ser usadas sem procurar pelas suas dependências privadas. I.e., as aplicações que procurem por uma dada plataforma só precisam das suas dependências públicas, não das privadas.
- Permissão de configuração do SHARE_INSTALL_DIR para lidar melhor com os layouts multi-arquitetura

### Frameworkintegration

- Correção de uma possível falha ao destruir um QSystemTrayIcon (acionado, por exemplo, pelo Trojitá) - erro <a href='https://bugs.kde.org/show_bug.cgi?id=343976'>343976</a>
- Correção das caixas de diálogo de arquivos modais nativas no QML - erro <a href='https://bugs.kde.org/show_bug.cgi?id=334963'>334963</a>

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
