---
aliases:
- ../../kde-frameworks-5.71.0
date: 2020-06-06
layout: framework
libCount: 70
---
### Baloo

- Guardar apenas uma vez os termos do nome do ficheiro

### Ícones Breeze

- Melhorar a precisão do ícones de percentagem da bateria
- Correcção do ícone do tipo MIME KML
- Mudança do ícone do rato para ter um melhor contraste em temas escuros (erro 406453)
- Obrigação de uma compilação dentro-do-código (erro 421637)
- Adição do ícones dos locais a 48px (erro 421144)
- Adição de um atalho de ícone para o LibreOffice em falta

### Módulos extra do CMake

- [android] Uso de uma versão mais recente do Qt no exemplo
- [android] Permitir a definição da localização de instalação do APK
- ECMGenerateExportHeader: adição da geração do *_DEPRECATED_VERSION_BELATED()
- ECMGeneratePriFile: correcção para o ECM_MKSPECS_INSTALL_DIR ser absoluto
- ECMGeneratePriFile: possibilitar aos ficheiros .pri mudarem de posição
- Supressão do aviso de inconsistência do nome do pacote no 'find_package_handle_standard_args'

### Ferramentas Doxygen do KDE

- Uso do ficheiro 'logo.png' como logótipo por omissão se estiver presente
- Mudança do 'public_example_dir' para 'public_example_dirs' para activar várias pastas
- Eliminação de código do suporte para Python2
- Adaptação das ligações ao repositório para invent.kde.org
- Correcção da ligação para o impresso do kde.org
- As páginas de histórico do KDE4 foram reunidas com a página de histórico geral
- Rectificação da geração dos diagramas de dependências com o Python 3 (inviabiliza o Py2)
- Exportação da meta-lista para o ficheiro JSON

### KAuth

- Uso do ECMGenerateExportHeader para gerir melhor a API obsoleta

### KBookmarks

- Uso do contexto de marcação da UI em mais chamadas tr()
- [KBookmarkMenu] Atribuição prévia do 'm_actionCollection' para evitar um estoiro (erro 420820)

### KCMUtils

- Adição do X-KDE-KCM-Args como propriedade, leitura da propriedade no carregamento do módulo
- Correcção de estoiro ao carregar um KCM de uma aplicação externa como o Yast (erro 421566)
- KSettings::Dialog: evitar itens duplicados devido ao uso em cascata da $XDG_DATA_DIRS

### KCompletion

- Envolver também a documentação da API com uma utilização do KCOMPLETION_ENABLE_DEPRECATED_SINCE; por cada método
- Uso do contexto de marcação da UI em mais chamadas tr()

### KConfig

- Não tentar inicializar o valor do enumerado obsoleto SaveOptions
- Adição do KStandardShortcut::findByName(const QString&amp;) e descontinuação do  find(const char*)
- Correcção do KStandardShortcut::find(const char*)
- Ajuste do nome do método exportado a nível interno, como sugerido no D29347
- KAuthorized: exportação do método para recarregar as restrições

### KConfigWidgets

- [KColorSche] Remoção de código duplicado
- Possibilidade de as cores do Header usarem como alternativa as cores do Window em primeiro lugar
- introdução do conjunto de cores do Header

### KCoreAddons

- Correcção do erro 422291 - Antevisão dos URI's de XMPP no KMail (erro 422291)

### KCrash

- Não invocar chamadas localizadas do 'qstring' na secção crítica

### KDeclarative

- Criação das funções kcmshell.openSystemSettings() e kcmshell.openInfoCenter()
- Migração do KKeySequenceItem para o QQC2
- Alinhamento ao pixel dos filhos do GridViewInternal

### KDED

- Correcção de ícones borrados no menu de aplicação, usando a opção UseHighDpiPixmaps
- Adição do ficheiro de serviço do 'systemd' para o 'kded'

### KFileMetaData

- [TaglibExtractor] Adição do suporte para os livros de áudio Audible "Enhanced Audio"
- respeito da opção 'extractMetaData'

### KGlobalAccel

- Correcção de erro com os componentes que contêm caracteres especiais (erro 407139)

### KHolidays

- Actualização dos feriados de Taiwan
- holidayregion.cpp - oferta de textos para tradução das regiões Alemãs
- holidays_de-xpto - definição do campo do nome para todos os ficheiros de feriados alemães
- holiday-de-&lt;xpto&gt; - where foo is a region is Germany
- Adição do ficheiro de feriados de DE-BE (Alemanha/Berlim)
- holidays/plan2/holiday_gb-sct_en-gb

### KImageFormats

- Adição de verificações de limites e sanidade

### KIO

- Introdução do KIO::OpenUrlJob, uma remodelação e substituição do KRun
- KRun: descontinuação de todos os métodos estáticos 'run*' com instruções de migração completas
- KDesktopFileActions: descontinuação do run/runWithStartup, uso em alternativa do OpenUrlJob
- Pesquisa pelo 'kded' como dependência de execução
- [kio_http] Processamento de um FullyEncoded QUrl com o TolerantMode (erro 386406)
- [DelegateAnimationHanlder] Substituição da QLinkedList obsoleta pela QList
- RenameDialog: Avisar quando os tamanhos dos ficheiros não forem iguais (erro 421557)
- [KSambaShare] Validação que tanto o 'smbd' como o 'testparm' estão disponíveis (erro 341263)
- Locais: Uso do Solid::Device::DisplayName para o DisplayRole (erro 415281)
- KDirModel: correcção de regressão no hasChildren() para as árvores com ficheiros visíveis (erro 419434)
- file_unix.cpp: quando o ::rename é usado como condição, comparar o valor devolvido com -1
- [KNewFileMenu] Possibilidade de criar uma pasta chamada '~' (erro 377978)
- [HostInfo] Atribuição do QHostInfo::HostNotFound quando a máquina não consta na 'cache' de DNS (erro 421878)
- [DeleteJob] Comunicação dos resultados após terminar (erro 421914)
- KFileItem: tradução do 'timeString' (erro 405282)
- [StatJob] Make mostLocalUrl ignore remote (ftp, http...etc) URLs (bug 420985)
- [KUrlNavigatorPlacesSelector] actualizar apenas uma vez o menu em caso de alterações (erro 393977)
- [KProcessRunner] Uso apenas do nome do executável no âmbito
- [knewfilemenu] Apresentação de um aviso incorporado ao criar itens com espaços iniciais ou finais (erro 421075)
- [KNewFileMenu] Remoção de parâmetro redundante do 'slot'
- ApplicationLauncherJob: mostrar a janela Abrir Com se não tiver sido passado nenhum serviço
- Validação que o programa existe e é executável antes de ser injectado o 'kioexec/kdesu/etc'
- Correcção de URL passado como argumento ao invocar um ficheiro .desktop (erro 421364)
- [kio_file] Tratamento da mudança de nome do ficheiro 'A' para 'a' nos sistemas de ficheiros FAT32
- [CopyJob] Verificação se a pasta de destino é uma ligação simbólica (erro 421213)
- Correcção do ficheiro de serviço que define o 'Executar num terminal' que dá um código de erro 100 (erro 421374)
- kio_trash: adição do suporte para mudar os nomes das pastas na 'cache'
- Aviso se o info.keepPassword não estiver definido
- [CopyJob] Verificação do espaço livre para URL's remotos antes de copiar e outras melhorias (erro 418443)
- [kcm_trash] Mudança da percentagem do tamanho do lixo para 2 casas decimais
- [CopyJob] Resolução de um POR-FAZER antigo e uso do QFile::rename()
- [LauncherJobs] Emitir a descrição

### Kirigami

- Adição de cabeçalhos de secções na barra lateral quando todas as acções podem ser expandidas
- correcção: Preenchimento do cabeçalho das folhas sobrepostas
- Uso de uma cor predefinida melhor para a Drawer Global quando é usada como Sidebar
- Correcção de comentário incorrecto sobre a forma como se determina a GridUnit
- Circulação mais fiável pelas árvores-mães para o PageRouter::pushFromObject
- O PlaceholderMessage depende no Qt 5.13 mesmo que o CMakeLists.txt obrigue ao 5.12
- introdução de grupos de Header
- Não reproduzir a animação de fecho no close() se a folha já estiver fechada
- Adição do componente SwipeNavigator
- Introdução do componente Avatar
- Correcção da inicialização do recurso de 'shaders' para as compilações estáticas
- AboutPage: Adição de dicas
- Garantia do 'closestToWhite' e do 'closestToBlack'
- AboutPage: Adição de botões para o e-mail, endereço Web
- Usar sempre o conjunto de cores do Window para o AbstractApplicationHeader (erro 421573)
- Introdução do ImageColors
- Recomendação de um cálculo melhor da largura do PlaceholderMessage
- Melhoria da API do PageRouter
- Usto de um tamanho de texto pequeno para o sub-título do BasicListItem
- Adição do suporte para camadas no PagePoolAction
- Introdução do controlo RouterWindow

### KItemViews

- Uso do contexto de marcação da UI em mais chamadas tr()

### KNewStuff

- Não duplicar as mensagens de erro numa notificação passiva (erro 421425)
- Correcção das cores incorrectas na janela de mensagem do KNS Quick (erro 421270)
- Não marcar o item como desinstalado se o programa de desinstalação falhar (erro 420312)
- KNS: Descontinuar o método 'isRemote' e tratar o erro de processamento de forma adequada
- Não marcar o item como instalado se o programa de instalação falhar
- Correcção da apresentação das actualizações quando a opção for seleccionada (erro 416762)
- Correcção da actualização da selecção automática (erro 419959)
- Adição do suporte do KPackage ao KNewStuffCore (erro 418466)

### KNotification

- Implementação do controlo de visibilidade do bloqueio de ecrã no Android
- Implementação do agrupamento de notificações no Android
- Apresentação de mensagens de notificações com texto formatado no Android
- Implementação dos URL's com sugestões
- Uso do contexto de marcação da UI em mais chamadas tr()
- Implementação do suporte para a urgência das notificações no Android
- Remoção das verificações do serviço de notificações e usar o KPassivePopup como alternativa

### KQuickCharts

- Verificação de arredondamento do fundo mais simples
- PieChart: Desenho de um segmento de toro como fundo quando não for um círculo completo
- PieChart: Adição de uma função auxiliar que lida com o arredondamento dos segmentos de toros
- PieChart: Exposição do 'fromAngle/toAngle' ao 'shader'
- Desenhar sempre o fundo do gráfico circular
- Definir sempre a cor de fundo, mesmo quando o 'toAngle != 360'
- Permitir que o início/fim esteja pelo menos a 2px de distância uns dos outros
- Remodelação da implementação do PieChart
- Correcção e comentário da função de SD do 'sdf_torus_segment'
- Não definir de forma explícita a quantidade de suavização ao desenhar os SDF's
- Truque alternativo para a falta de parâmetros de funções de listas nos 'shaders' de GLES2
- Tratamento das diferenças entre os perfis de base e antigos nos sistemas de desenho de linhas/segmentos de círculo
- Correcção de alguns erros de validação
- Actualização do programação da validação do 'shader' com a nova estrutura
- Adição de um cabeçalho separado para os 'shaders' GLES3
- Remoção de 'shaders' duplicados para o perfil de base, usando em alternativa o pré-processador
- exposição tanto do 'name' como do 'shortName'
- suporte para legendas curtas/extensas diferentes (erro 421578)
- guarda do modelo atrás de um QPointer
- Adição do MapProxySource

### KRunner

- Não persistir a lista de permissões de execução na configuração
- Correcção no KRunner dos eventos 'prepare/teardown' (erro 420311)
- Detecção de ficheiros e pastas locais que começam por ~

### KService

- Adição do X-KDE-DBUS-Restricted-Interfaces aos campos do item de 'desktop' do Application
- Adição da marca de descontinuação do compilador para o construtor do KServiceAction com 5 argumentos

### KTextEditor

- Adição do .diff ao 'file-changed-diff' para activar a detecção MIME no Windows
- mini-mapa da barra de deslocamento: atrasar a actualização nos documentos inactivos
- Fazer com que o texto se alinhe sempre com a linha de base do texto
- Reversão da "Configuração das vistas de gravação e carregamento completos com base na configuração da sessão"
- Correcção do marcador de linha modificada no mini-mapa do Kate

### KWidgetsAddons

- Ser insistente sobre o método obsoleto KPageWidgetItem::setHeader(texto-vazio-e-não-nulo)

### KXMLGUI

- [KMainWindow] Invocação do QIcon::setFallbackThemeName (posterior) (erro 402172)

### KXmlRpcClient

- Marcação do KXmlRpcClient como ajuda de migração

### Plasma Framework

- PC3: Adição de separador à barra de ferramentas
- suporte para grupos de cores do Header
- Implementação do ajuste de deslocamento e arrastamento dos valores do controlo SpinBox
- Usar o 'font:' em vez do 'font.pointSize:' sempre que possível
- kirigamiplasmastyle: Adição da implementação do AbstractApplicationHeader
- Impedir um encerramento potencial de todos os sinais no IconItem (erro 421170)
- Uso de tamanho de texto pequeno para o sub-título do ExpandableListItem
- Adição do 'smallFont' ao estilo do Plasma no Kirigami
- [Cabeçalho do Plasmóide Desenho do cabeçalho apenas quando existir um SVG no tema

### QQC2StyleBridge

- Adição de estilos do ToolSeparator
- Remoção do "pressed" do estado "on" do CheckIndicator (erro 421695)
- Suporte para grupos de Header's
- Implementação do 'smallFont' no 'plugin' do Kirigami

### Solid

- Adição de uma QString Solid::Device::displayName, usada no Dispositivo do Fstab para as montagens na rede (erro 415281)

### Sonnet

- Possibilidade de substituição para desactivar a detecção automática da língua (erro 394347)

### Realce de sintaxe

- Actualização das extensões do Raku nos blocos de Markdown
- Raku: correcção dos blocos de código delimitados no Markdown
- Atribuição do atributo "Identifier" às aspas de abertura em vez do "Comment" (erro 421445)
- Bash: correcção dos comentários após os caracteres de escape (erro 418876)
- LaTeX: correcção da dobragem no \end{...} e nos marcadores de região BEGIN-END (erro 419125)

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
