---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: O KDE disponibiliza o KDE Applications 17.04.2
layout: application
title: O KDE disponibiliza o KDE Applications 17.04.2
version: 17.04.2
---
8 de Junho de 2017. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../17.04.0'>Aplicações do KDE 17.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 15 correções de erros registradas incluem melhorias no kdepim, ark, dolphin, gwenview, kdenlive, entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.33.
