---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: O KDE disponibiliza o KDE Applications 16.04.3
layout: application
title: O KDE disponibiliza o KDE Applications 16.04.3
version: 16.04.3
---
12 de Julho de 2016. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../16.04.0'>Aplicações do KDE 16.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 20 correções de erros registradas incluem melhorias no Ark,  Cantor,Kate, KDEPIM, Umbrello entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.22.
